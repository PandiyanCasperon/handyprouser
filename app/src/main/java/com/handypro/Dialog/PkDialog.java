package com.handypro.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.handypro.R;


/**
 * Casperon Technology on 11/27/2015.
 */
public class PkDialog {

    private Context mContext;
    private Button Bt_action, Bt_dismiss;
    private TextView alert_title, alert_message, TextViewWhiteArea;
    private RelativeLayout RelativeLayoutHolderColor;
    private LinearLayout LinearLayoutTitleMessage;
    private RelativeLayout Rl_button;
    private Dialog dialog;
    private View view;
    private boolean isPositiveAvailable = false;
    private boolean isNegativeAvailable = false;


    public PkDialog(Context context) {
        this.mContext = context;

        //--------Adjusting Dialog width-----
        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen

        view = View.inflate(mContext, R.layout.custom_dialog_library, null);
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        alert_title = view.findViewById(R.id.custom_dialog_library_title_textview);
        alert_message = view.findViewById(R.id.custom_dialog_library_message_textview);
        TextViewWhiteArea = view.findViewById(R.id.TextViewWhiteArea);
        Bt_action = view.findViewById(R.id.custom_dialog_library_ok_button);
        Bt_dismiss = view.findViewById(R.id.custom_dialog_library_cancel_button);
        Rl_button = view.findViewById(R.id.custom_dialog_library_button_layout);
        RelativeLayoutHolderColor = view.findViewById(R.id.RelativeLayoutHolderColor);
        LinearLayoutTitleMessage = view.findViewById(R.id.LinearLayoutTitleMessage);
    }


    public void show() {

        /*Enable or Disable positive Button*/
        if (isPositiveAvailable) {
            Bt_action.setVisibility(View.VISIBLE);
        } else {
            Bt_action.setVisibility(View.GONE);
        }

        /*Enable or Disable Negative Button*/
        if (isNegativeAvailable) {
            Bt_dismiss.setVisibility(View.VISIBLE);
        } else {
            Bt_dismiss.setVisibility(View.GONE);
        }

        /*Changing color for Button Layout*/
        if (isPositiveAvailable && isNegativeAvailable) {
            Rl_button.setBackgroundColor(mContext.getResources().getColor(R.color.white_color));
        } else {
            Rl_button.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        }

        dialog.show();
    }


    public void dismiss() {
        dialog.dismiss();
    }


    public void setDialogTitle(String title) {
        alert_title.setText(title);
    }

    public void setChangeOrderDialog() {
        SpannableStringBuilder builders = new SpannableStringBuilder();
        String string1 = "Approval sent to your ";
        SpannableString redSpannable = new SpannableString(string1);
        redSpannable.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.black_color)), 0, string1.length(), 0);
        builders.append(redSpannable);

        String string2 = "Trustz";
        SpannableString whiteSpannable = new SpannableString(string2);
        whiteSpannable.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.dark_blue)), 0, string2.length(), 0);
        builders.append(whiteSpannable);

        String string3 = "Pro ";
        SpannableString redSpannable1 = new SpannableString(string3);
        redSpannable1.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.colorPrimary)), 0, string3.length(), 0);
        builders.append(redSpannable1);

        String string4 = "Craftsman";
        SpannableString whiteSpannable1 = new SpannableString(string4);
        whiteSpannable1.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.black_color)), 0, string4.length(), 0);
        builders.append(whiteSpannable1);
        alert_message.setText(builders, TextView.BufferType.SPANNABLE);
    }

    public void setMultiScheduleErrorDialog(String MobileNumber) {
        RelativeLayoutHolderColor.setBackgroundColor(Color.parseColor("#FBEF5F"));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0, 0, 0, 0);
        LinearLayoutTitleMessage.setLayoutParams(params);

        SpannableStringBuilder builders = new SpannableStringBuilder();
        String string1 = "Due to the nature of your job, please contact ";
        SpannableString redSpannable = new SpannableString(string1);
        redSpannable.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.colorPrimary)), 0, string1.length(), 0);
        builders.append(redSpannable);

        String string2 = "Trustz";
        SpannableString whiteSpannable = new SpannableString(string2);
        whiteSpannable.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, string2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        whiteSpannable.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.dark_blue)), 0, string2.length(), 0);
        builders.append(whiteSpannable);

        String string3 = "Pro ";
        SpannableString redSpannable1 = new SpannableString(string3);
        whiteSpannable.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, string3.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        redSpannable1.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.colorPrimary)), 0, string3.length(), 0);
        builders.append(redSpannable1);

        String string4 = "to schedule your job.";
        SpannableString whiteSpannable1 = new SpannableString(string4);
        whiteSpannable1.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.colorPrimary)), 0, string4.length(), 0);
        builders.append(whiteSpannable1);
        alert_message.setText(builders, TextView.BufferType.SPANNABLE);
        alert_title.setVisibility(View.GONE);
        TextViewWhiteArea.setVisibility(View.VISIBLE);
        TextViewWhiteArea.setText(MobileNumber);
    }


    public void setDialogMessage(String message) {
        alert_message.setText(message);
    }

    public void setDialogMessageInLeft(String message) {
        alert_message.setGravity(Gravity.START);
        alert_message.setText(message);
    }


    public void setCancelOnTouchOutside(boolean value) {
        dialog.setCanceledOnTouchOutside(value);
    }


    /*Action Button for Dialog*/
    public void setPositiveButton(String text, final View.OnClickListener listener) {

        isPositiveAvailable = true;

        Bt_action.setText(text);
        Bt_action.setOnClickListener(listener);
    }

    public void setNegativeButton(String text, final View.OnClickListener listener) {
        isNegativeAvailable = true;

        Bt_dismiss.setText(text);
        Bt_dismiss.setOnClickListener(listener);
    }

}
