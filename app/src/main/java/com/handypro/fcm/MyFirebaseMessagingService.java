package com.handypro.fcm;


import android.app.ActivityManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.handypro.BuildConfig;
import com.handypro.R;
import com.handypro.activities.HomeActivity;
import com.handypro.activities.MoreInfoPage;
import com.handypro.activities.NotificationDB;
import com.handypro.activities.OrderDetailActivity;
import com.handypro.activities.RatingPageActivity;
import com.handypro.activities.Xmpp_PushNotificationPage;
import com.handypro.activities.approveestimatebuilder;
import com.handypro.activities.navigationMenu.ChatActivity;
import com.handypro.activities.navigationMenu.paymentrequest;
import com.handypro.activities.newmoreinfo;
import com.handypro.core.socket.ChatMessageService;
import com.handypro.core.socket.ChatMessageServicech;
import com.handypro.sharedpreference.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by user145 on 8/4/2017.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private NotificationDB mHelper;
    private SQLiteDatabase dataBase;
    private NotificationUtils notificationUtils;
    private String Job_status_message = "", actionnew = "";
    private String value;
    private SharedPreference mySession;
    private String Job_id = "", Title = BuildConfig.App_Name, Message = "You received new notification";

    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);
        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);
        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
    }

    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.apply();

        SharedPreference mySession = new SharedPreference(getApplicationContext());
        mySession.putFCMRegisterId(token);
    }

    public static String getRegistrationId(Context context) {
        final SharedPreferences pref = context.getSharedPreferences(Config.SHARED_PREF, 0);
        String registrationId = pref.getString("FCMId", "");
        if (registrationId.isEmpty()) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("regId", FirebaseInstanceId.getInstance().getToken());
            editor.apply();

            SharedPreference mySession = new SharedPreference(context);
            mySession.putFCMRegisterId(FirebaseInstanceId.getInstance().getToken());

            return FirebaseInstanceId.getInstance().getToken();
        }
        return registrationId;
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        notificationUtils = new NotificationUtils(MyFirebaseMessagingService.this);
        mySession = new SharedPreference(MyFirebaseMessagingService.this);
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        System.out.println("--------FCM Received-------" + remoteMessage);

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    private void handleNotification(String title, String message) {
        Job_status_message = message;
        Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
        pushNotification.putExtra("title", title);
        pushNotification.putExtra("body", message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
    }

    private void handleDataMessage(JSONObject json) {
        System.out.println("Sara response--->fcm-" + json.toString());

        value = json.toString();


        SharedPreference mySession;
        mySession = new SharedPreference(getApplicationContext());
        if (mySession.getLogInStatus() == false) {

        } else {

            try {
                Intent intent = new Intent(getApplicationContext(), ChatMessageService.class);
                startService(intent);

                Intent intentx = new Intent(getApplicationContext(), ChatMessageServicech.class);
                startService(intentx);


            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                JSONObject json1 = new JSONObject(json.toString());
                JSONObject object1 = json1.getJSONObject("data");

                if (object1.has("key0")) {
                    actionnew = object1.getString("action");
                    if (object1.has("uniqueId")) {
                        int okl = 0;
                        String uniqueId = object1.getString("uniqueId");
                        System.out.println("Sara response--->fcm-unique id" + uniqueId);
                        mHelper = new NotificationDB(this);
                        dataBase = mHelper.getWritableDatabase();
                        Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + mHelper.TABLE_NAME + " WHERE uniqueid ='" + uniqueId + "'", null);

                        if (mCursor.moveToFirst()) {
                            do {
                                okl++;
                                System.out.println("Sara response--->fcm-okl" + okl);
                            } while (mCursor.moveToNext());
                        }

                        if (okl == 0) {
                            System.out.println("Sara response--->fcm-record not exists" + mHelper.getProfilesCount());
                            mHelper = new NotificationDB(this);
                            dataBase = mHelper.getWritableDatabase();
                            ContentValues values = new ContentValues();
                            values.put(NotificationDB.KEY_FNAME, json.toString());
                            values.put(NotificationDB.KEY_LNAME, uniqueId);
                            dataBase.insert(NotificationDB.TABLE_NAME, null, values);
                            if (isAppOnForeground(MyFirebaseMessagingService.this)) {
                                commonNotificationCall();
                            } else {
                                if (object1.has("key0")) {
                                    commonTrayMessageNotification(object1);
                                }
                            }
                        } else {
                            System.out.println("Sara response--->fcm-record exists" + mHelper.getProfilesCount());
                        }
                    } else {
                        if (isAppOnForeground(MyFirebaseMessagingService.this)) {
                            commonNotificationCall();
                        } else {
                            if (object1.has("key0")) {
                                commonTrayMessageNotification(object1);
                            }
                        }
/*
                        if (object1.has("key0")) {
                            commonTrayMessageNotification(object1);
                        }*/
                    }
                } else {

                    String checkdataa = object1.getString("action");
                    if (checkdataa.equalsIgnoreCase("chat")) {
                        Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
                        dialogIntent.putExtra("TITLE_INTENT", value);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(dialogIntent);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    private void commonNotificationCall() {
        if (actionnew.equalsIgnoreCase("job_reassign")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("job_accepted")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("admin_notification")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("Task_failed")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("start_off")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("provider_reached")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("job_started")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("job_completed")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("job_user_approval")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("change_order_request")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("estimation_received")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("payment_paid")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("pre_payment_paid")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("requesting_payment")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);

        } else if (actionnew.equalsIgnoreCase("requesting_pre_payment")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        } else if (actionnew.equalsIgnoreCase("referer_credited")) {
            Intent dialogIntent = new Intent(this, Xmpp_PushNotificationPage.class);
            dialogIntent.putExtra("TITLE_INTENT", value);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(dialogIntent);
        }

    }

    private boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        assert activityManager != null;
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    private void commonTrayMessageNotification(JSONObject object1) {
        try {
            if (value.equals("") || value.equals(" ")) {
                actionnew = "chat";
                Message = getResources().getString(R.string.class_xmppPush_message);
                if (mySession.getLogInStatus()) {
                    Title = "Hi " + mySession.getUserDetails().getUserName() + "!";
                }
                commonTrayNotification(Job_id, Title, Message);
            } else {
                if (object1.has("key0")) {
                    Job_id = object1.getString("key0");
                    Message = object1.getString("message");
                    actionnew = object1.getString("action");

                    if (actionnew.equalsIgnoreCase("job_user_approval")) {
                       /* pushnotification_alert_cancel.setVisibility(View.GONE);
                        Textview_Ok.setText(getResources().getString(R.string.class_xmppPush_open));*/
                    } else if (actionnew.equalsIgnoreCase("admin_notification")) {
                        /*pushnotification_alert_cancel.setVisibility(View.GONE);
                        Textview_Ok.setText("CLOSE");*/
                        if (mySession.getLogInStatus()) {
                            Title = "Hi " + mySession.getUserDetails().getUserName() + "!";
                        }
                        commonTrayNotification(Job_id, Title, Message);
                    } else {
                        if (actionnew.equalsIgnoreCase("job_accepted")) {
                            if (Message.contains("accepted")) {
                                Job_status_message = "A Craftsmen has accepted your job. We are Excited To Serve You and Care For Your Home.";
                            }
                            Title = getResources().getString(R.string.congrats);
                            Message = Job_status_message;
                        } else if (actionnew.equalsIgnoreCase("Task_failed")) {
                            Title = getResources().getString(R.string.thanks);
                            Message = Job_status_message;
                        }
                    }

                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.putString("servicetype", "");
                    editor.apply();

                    if (mySession.getLogInStatus()) {
                        Title = "Hi " + mySession.getUserDetails().getUserName() + "!";
                    }
                    commonTrayNotification(Job_id, Title, Message);
                } else {
                    if (mySession.getLogInStatus()) {
                        Title = "Hi " + mySession.getUserDetails().getUserName() + "!";
                    }
                    commonTrayNotification(Job_id, Title, getResources().getString(R.string.class_xmppPush_message));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void commonTrayNotification(String Job_id, String Title, String Message) {
        if (actionnew.equalsIgnoreCase("job_reassign")) {
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "1");
            prefeditor.apply();


            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.putString("servicetype", "");
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), OrderDetailActivity.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "10.00", resultIntent);

        } else if (actionnew.equalsIgnoreCase("Task_failed")) {

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "1");
            prefeditor.apply();


            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.putString("servicetype", "");
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), OrderDetailActivity.class);
            resultIntent.putExtra("message", Message);
            resultIntent.putExtra("gotore", "goo");
            notificationUtils.showNotificationMessage(Title, Message, "10.00", resultIntent);


        } else if (actionnew.equalsIgnoreCase("job_user_approval")) {

            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), newmoreinfo.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "10.00", resultIntent);

        } else if (actionnew.equalsIgnoreCase("start_off")) {

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "1");
            prefeditor.apply();

            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.putString("servicetype", "");
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), OrderDetailActivity.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);

        } else if (actionnew.equalsIgnoreCase("chat")) {
            Intent resultIntent = new Intent(getApplicationContext(), ChatActivity.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);
        } else if (actionnew.equalsIgnoreCase("provider_reached")) {
            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "1");
            prefeditor.apply();


            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.putString("servicetype", "");
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), OrderDetailActivity.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);
        } else if (actionnew.equalsIgnoreCase("job_started")) {

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "1");
            prefeditor.apply();

            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.putString("servicetype", "");
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), OrderDetailActivity.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);
        } else if (actionnew.equalsIgnoreCase("job_completed")) {
            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), newmoreinfo.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);
        } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "1");
            prefeditor.apply();


            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.putString("servicetype", "");
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), OrderDetailActivity.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);


        } else if (actionnew.equalsIgnoreCase("change_order_request")) {


            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "1");
            prefeditor.apply();

            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), MoreInfoPage.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);
        } else if (actionnew.equalsIgnoreCase("estimation_received")) {

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "1");
            prefeditor.apply();


            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), approveestimatebuilder.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);

        } else if (actionnew.equalsIgnoreCase("job_accepted")) {


            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "1");
            prefeditor.apply();

            SharedPreferences showpopup;
            showpopup = getApplicationContext().getSharedPreferences("showpoupup", 0); // 0 - for private mode
            SharedPreferences.Editor editorshowpopup = showpopup.edit();
            editorshowpopup.putString("show", "1");
            editorshowpopup.apply();


            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.putString("servicetype", "");
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), OrderDetailActivity.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);
        } else if (actionnew.equalsIgnoreCase("payment_paid")) {

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "1");
            prefeditor.apply();

            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), RatingPageActivity.class);
            resultIntent.putExtra("message", Message);
            resultIntent.putExtra("JobID", Job_id);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);
        } else if (actionnew.equalsIgnoreCase("pre_payment_paid")) {

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "1");
            prefeditor.apply();


            SharedPreferences showpopup;
            showpopup = getApplicationContext().getSharedPreferences("showpoupup", 0); // 0 - for private mode
            SharedPreferences.Editor editorshowpopup = showpopup.edit();
            editorshowpopup.putString("show", "1");
            editorshowpopup.apply();


            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.putString("servicetype", "");
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), OrderDetailActivity.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);
        } else if (actionnew.equalsIgnoreCase("requesting_payment") || actionnew.equalsIgnoreCase("requesting_pre_payment")) {

            SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
            SharedPreferences.Editor prefeditor = prefjobid.edit();
            prefeditor.putString("page", "1");
            prefeditor.apply();

            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + Job_id);
            editor.apply();

            Intent resultIntent = new Intent(getApplicationContext(), paymentrequest.class);
            resultIntent.putExtra("message", Message);
            resultIntent.putExtra("Type", actionnew);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);

        } else if (actionnew.equalsIgnoreCase("referer_credited")) {
            Intent resultIntent = new Intent(getApplicationContext(), HomeActivity.class);
            resultIntent.putExtra("message", Message);
            notificationUtils.showNotificationMessage(Title, Message, "", resultIntent);
        }
    }
}