package com.handypro.Iconstant;

import com.handypro.BuildConfig;

public interface ServiceConstant {

    /*Since I have implemented the Flavors in app.build.gradle, we can fetch the base URL from there.
        It Mean based on the current Build Type and Build Variant App Base URL will fetch from app.build.gradle*/
    //String Base_Url = "http://192.168.1.35:3009/";// local

    //String Base_Url = "https://book.handypro.com/";
    String Base_Url = "https://beta.handypro.com/";
     //String Base_Url = "https://handypro.casperon.co/";

    String SOCKET_CHAT_URL = Base_Url + "chat";
    String SOCKET_EVENT_NOTIFYURL = Base_Url + "notify";

    String ESTIMATE_URL = Base_Url + "mobile/user/get-job-estimation-category";

    //-------------------------------------------------------
    String loginUrl = Base_Url + "mobile/app/login";
    String Estimateavailability = Base_Url + "mobile/app/show-tasker-schedule";
    String beforephotoapproval = Base_Url + "mobile/app/updatetaskImagestatus";


    String Estimateapproval = Base_Url + "mobile/app/estimation-client-approval";
    String RegisterUrl = Base_Url + "mobile/app/check-user";
    String OtpUrl = Base_Url + "mobile/app/register";
    String Aboutus_Url = Base_Url + "mobile/app/mobile/aboutus";
    String Terms_Conditions_Url = Base_Url + "mobile/app/mobile/termsandconditions";

    String Privacy_Polocy = Base_Url + "mobile/app/mobile/privacypolicy";
    String paybycah_url = Base_Url + "mobile/app/payment/by-cash";
    String paybycheque_url = Base_Url + "mobile/app/payment/by-cheque";
    String BookJob = Base_Url + "mobile/app/user-booking";

    String newBookJob = Base_Url + "mobile/app/user-booking";
    String paymentpageurl = Base_Url + "mobile/app/paymentlist/history";
    String forgot_password_url = Base_Url + "mobile/app/user/reset-password";
    String reset_password_url = Base_Url + "mobile/app/user/update-reset-password";
    String couponcode_url = Base_Url + "mobile/app/payment/couponmob";

    String acceptclickurl = Base_Url + "mobile/user/acceptterms";

    String QUESTION_URL = Base_Url + "mobile/user/getCategoryQA";

    String loyalty_url = Base_Url + "mobile/app/complete_zero_balance";
    String CategoriesUrl = Base_Url + "mobile/app/categories";
    String Mobile_Id_url = Base_Url + "mobile/app/payment/by-gateway";
    String invite_earn_friends_url = Base_Url + "mobile/app/get-invites";
    String Notification_mode = Base_Url + "mobile/user/notification_mode";
    String App_Info = Base_Url + "mobile/app/mobile/appinfo";
    String plumbal_money_url = Base_Url + "mobile/app/get-money-page";
    String plumbal_add_money_url = Base_Url + "mobile/mobile/wallet-recharge/stripe-process";
    String plumbal_money_transaction_url = Base_Url + "mobile/app/get-trans-list";

    String Card_webview_url = Base_Url + "mobile/mobile/stripe-manual-payment-form?mobileId=";

    String bookingconform = Base_Url + "mobile/app/book-job-new";

    String checkcratfman = Base_Url + "mobile/app/check-tasker-availability";

    //Partner Profile URl
    String PROFILEINFO_URL = Base_Url + "mobile/provider/provider-info";

    String GETMESSAGECHAT_URL = Base_Url + "mobile/app/getmessage";
    String User_profile_Url = Base_Url + "mobile/app/getuserprofile";

    String PAYMENT_URL = Base_Url + "mobile/app/user/job-more-info";
    String CLIENTAPPROVE = Base_Url + "mobile/app/client-approval";
    String CHANGEJOB_URL = Base_Url + "mobile/app/user/job-change-info";

    String LOYALITY_URL = Base_Url + "mobile/user/get-loyalty";


    String marketingdata = Base_Url + "mobile/app/knownsource";


    String MyJobsList_Url = Base_Url + "mobile/app/my-jobs-new";
    String MyJobs_Cancel_Reason_Url = Base_Url + "mobile/app/cancellation-reason";
    String MyJobs_Cancel_Url = Base_Url + "mobile/app/cancel-job";
    String MyJobs_Detail_Url = Base_Url + "mobile/user/view-job";
    String Filter_booking_url = Base_Url + "mobile/user/recentuser-list";
    String changePassword_url = Base_Url + "mobile/app/user/change-password";
    String profile_edit_userName_url = Base_Url + "mobile/app/user/change-name";
    String profile_edit_mobileNo_url = Base_Url + "mobile/app/user/change-mobile";
    String profile_edit_photo_url = Base_Url + "mobile/app/user-profile-pic";
    String logout_url = Base_Url + "mobile/app/user/logout";

    String gettingdetails = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";

    //https://maps.googleapis.com/maps/api/geocode/json?key=map_key&address=1600+Amphitheatre+Parkway,+Mountain+View,+CA
    String GetLatLonFromAddress = "https://maps.googleapis.com/maps/api/geocode/json?key=";
    //https://maps.googleapis.com/maps/api/geocode/json?components=postal_code:600027&sensor=false&key=map_key
    String GetLatLonFromZIPCode = "https://maps.googleapis.com/maps/api/geocode/json?key=";

    //newly done by abdul
    String place_search_url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=";

    String GetAddressFrom_LatLong_url = "https://maps.googleapis.com/maps/api/place/details/json?key=";

    String availabledays = Base_Url + "mobile/app/show-all-tasker-schedule";

    String rating_list_url = Base_Url + "mobile/get-rattings-options";
    String rating_submit_url = Base_Url + "mobile/submit-rattings";
    String chat_detail_url = Base_Url + "mobile/chat/chathistory";
    String social_check_url = Base_Url + "mobile/app/mobile/social-fbcheckUser";

    String facebook_register_url = Base_Url + "mobile/app/mobile/social-register";
    String facebook_login_url = Base_Url + "mobile/app/mobile/social-login";

    //Transaction Module URL
    String TRANSACTION_URL = Base_Url + "mobile/app/user-transaction";
    String TRANSACTION_DETAIL_URL = Base_Url + "mobile/app/userjob-transaction";

    //Notification URl
    String NOTIFICATION_URL = Base_Url + "mobile/app/notification";

    //Review Module URL
    String REVIEW_URL = Base_Url + "mobile/app/get-reviews";

//---------------------------Notification Process-----------------------------

    String MODEUPDATE_URL = Base_Url + "mobile/user/notification_mode";

    /**
     * new API calls
     */
    String CategoryDetail = Base_Url + "mobile/user/get-subcategorydetails";
    //----stripe payment--
    String getCardList = Base_Url + "mobile/app/payment/stripe-card-list";
    String AddStripeCard = Base_Url + "mobile/app/payment/stripe-add-card";
    String deleteStripeCard = Base_Url + "mobile/app/payment/stripe-delete-card";
    String makeCardDefault = Base_Url + "mobile/app/payment/stripe-default-card";

    String payusingcard = Base_Url + "mobile/app/payment/stripe-payment-process";

    //mobile/user/pre_amount/list --> Used to get prepayment list details
    String PrepaymentRequestList = Base_Url + "mobile/user/pre_amount/list";
    //mobile/user/pre_amount/update --> Used to update the status of prepayment
    String PrepaymentRequestListUpdate = Base_Url + "mobile/user/pre_amount/update";
    //mobile/user/pre_amount/byGateway -->  Used to generate the Transaction ID
    String PrepaymentRequestForTransactionID = Base_Url + "mobile/user/pre_amount/byGateway";
    //mobile/user/pre_amount/paymentListhistory --> Used to get payment related info PaymentPageActivity when the flow is Prepayment
    String PrepaymentRequestForPaymentRelated = Base_Url + "mobile/user/pre_amount/paymentListhistory";
    //mobile/user/pre_amount/byCash --> Used to make final payment by cash
    String PrepaymentRequestByCash = Base_Url + "mobile/user/pre_amount/byCash";
    //mobile/user/pre_amount/stripe-payment-process --> Used to hit the final payment for card payment.
    String PrepaymentRequestByCard = Base_Url + "mobile/user/pre_amount/stripe-payment-process";
    //mobile/user/pre_amount/by-cheque --> Used to make final payment by cheque
    String PrepaymentRequestByCheque = Base_Url + "mobile/user/pre_amount/by-cheque";
    //mobile/app/client-approval-for-withoutschedule --> Used to approve the estimation with scheduling the craftsman.
    String ApprovalWithoutSchedule = Base_Url + "mobile/app/client-approval-for-withoutschedule";
    //mobile/provider/remove_subitems --> This API is used to delete the craftman created sub items. But we must have at least single sub items.
    String SubItemsDelete = Base_Url + "mobile/user/remove_subitems";

    //Reference Link of Android and iOS.
    String AndroidReferLink = "https://handypro.page.link/playstore";
    String IOSReferLink = "https://itunes.apple.com/us/app/handypro/id1354294771?mt=8";
    //mobile/provider/update_user_email --> This call used to update the user email
    String UPDATE_USER_EMAIL_URL = Base_Url + "mobile/provider/update_user_email";
    //mobile/app/next_availability_tasker_schedule --> This call is used to availability days of given 7 dates.
    String CHECK_NEXT_AVAILABILITY = Base_Url + "mobile/app/next_availability_tasker_schedule";
    //mobile/app/user_address_list
    String GET_USER_SAVED_ADDRESS = Base_Url + "mobile/app/user_address_list";
    //provider-rating --> This call is used to get the craftsman rating. Display under craftsman profile.
    String GET_CRAFTSMAN_RATING = Base_Url + "mobile/provider/provider-rating";
    //mobile/app/campaign_code Get the Campaign from backend based on ZIP code of job.
    String GET_CAMPAIGN_CODE = Base_Url + "mobile/app/campaign_code";
    //mobile/user/getSocialNetworkList this is used to get franchisee mobile number and franchisee social list based on job ID in JOB RATING PAGE
    String GET_RATING_DETAILS = Base_Url + "mobile/user/getSocialNetworkList";


    String GETHMP_DETAILS = Base_Url + "mobile/user/gethmpdetails";
}
