package com.handypro.Iconstant;

/**
 * Created by CAS63 on 3/9/2018.
 */

interface DBConstantValues {
    String CATEGORY_INFO_TABLENAME = "restuarant_info";

    String CATEGORY_ID = "cat_id";

    String CATEGORY_NAME = "cat_name";

    String CATEGORY_ACTIVE_ICON = "cat_icon";

    String CATEGORY_JOB_TYPE = "job_type";

    String CATEGORY_HOURLY_RATE = "hourly_rate";
}
