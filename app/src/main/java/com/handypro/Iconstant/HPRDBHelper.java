package com.handypro.Iconstant;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.handypro.Pojo.SubCategoryPojo;
import com.handypro.sharedpreference.SharedPreference;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/9/2018.
 */

public class HPRDBHelper implements Commonvalues, DBConstantValues {

    private DataBaseHelper myDBHelper;
    private String TAG = HPRDBHelper.class.getSimpleName();
    private int DATABASE_VERSION = 1;
    private SQLiteDatabase myDataBase;
    private Context myContext;
    private SharedPreference mySession;

    public HPRDBHelper(Context aContext) {
        this.myContext = aContext;
        myDBHelper = new DataBaseHelper(aContext);
        mySession = new SharedPreference(aContext);
        myDataBase = myDBHelper.getWritableDatabase();
        open();
    }

    public class DataBaseHelper extends SQLiteOpenHelper {

        public DataBaseHelper(Context context) {

            super(context, DATABASE_NAME, null, DATABASE_VERSION);

        }

        @Override
        public void onCreate(SQLiteDatabase db) {

        }

        @Override
        public void onUpgrade(SQLiteDatabase aDB, int aOldVersion, int aNewVersion) {
            String aAlter = "";

            Log.e(TAG, "onUpgrade called " + aOldVersion + " NEW" + aNewVersion);
            String aInsert = "";

        }

    }

    /**
     * Function to make DB as readable and writable
     */
    private void open() {

        try {
            if (myDataBase == null) {
                myDataBase = myDBHelper.getWritableDatabase();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Function to Close the database
     */
    public void close() {

        try {
            Log.d(TAG, "mySQLiteDatabase Closed");

            // ---Closing the database---
            myDBHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Truncate Table
     *
     * @param aTableName
     */
    public void truncateTable(String aTableName) {
        try {
            String aQuery = "DELETE  FROM " + aTableName;
            myDataBase.execSQL(aQuery);

        } catch (SQLiteException e) {
            Log.e(TAG, e.getMessage().toString());
        }
    }

    /**
     * put the category details in the Database
     *
     * @param aSubCatPojo
     */
    public void putCategoryDetails(SubCategoryPojo aSubCatPojo) {

        Cursor aCursor = null;

        String aQuery = "SELECT * FROM " + CATEGORY_INFO_TABLENAME;

        aCursor = myDataBase.rawQuery(aQuery, null);

        aCursor.moveToFirst();
        if (aCursor.getCount() == 0) {
            ContentValues values = new ContentValues();
            values.put(CATEGORY_ID, aSubCatPojo.getSubCategoryId());
            values.put(CATEGORY_NAME, aSubCatPojo.getSubCategoryName());
            values.put(CATEGORY_ACTIVE_ICON, aSubCatPojo.getSubCategoryActiveIcon());
            values.put(CATEGORY_JOB_TYPE, aSubCatPojo.getSubCategoryJobType());
            values.put(CATEGORY_HOURLY_RATE,aSubCatPojo.getSubCategoryHourlyRate());
            myDataBase.insert(CATEGORY_INFO_TABLENAME, null, values);
        } else {
            //Truncate the Table
            truncateTable(CATEGORY_INFO_TABLENAME);
            ContentValues values = new ContentValues();
            values.put(CATEGORY_ID, aSubCatPojo.getSubCategoryId());
            values.put(CATEGORY_NAME,  aSubCatPojo.getSubCategoryName());
            values.put(CATEGORY_ACTIVE_ICON, aSubCatPojo.getSubCategoryActiveIcon());
            values.put(CATEGORY_JOB_TYPE, aSubCatPojo.getSubCategoryJobType());
            values.put(CATEGORY_HOURLY_RATE,aSubCatPojo.getSubCategoryHourlyRate());
            myDataBase.insert(CATEGORY_INFO_TABLENAME, null, values);
        }

    }


    /**
     * Get the Selected Category details from the Database
     */
    public ArrayList<SubCategoryPojo> getSelectedCategoryDetails() {
        ArrayList<SubCategoryPojo> aSelectedCatArrList = new ArrayList<SubCategoryPojo>();
        try {
            Log.d(TAG, "Get category Info from DB");
            String aQuery = "SELECT * FROM " + CATEGORY_INFO_TABLENAME;

            Cursor aCursor = myDataBase.rawQuery(aQuery, null);
            aCursor.moveToFirst();

            if (aCursor.getCount() > 0) {
                while (!aCursor.isAfterLast()) {
                    SubCategoryPojo aSelectedCategoryPojo = new SubCategoryPojo();

                    aSelectedCategoryPojo.setSubCategoryId(aCursor.getString(aCursor.getColumnIndex(CATEGORY_ID)));
                    aSelectedCategoryPojo.setSubCategoryName(aCursor.getString(aCursor.getColumnIndex(CATEGORY_NAME)));
                    aSelectedCategoryPojo.setSubCategoryActiveIcon(aCursor.getString(aCursor.getColumnIndex(CATEGORY_ACTIVE_ICON)));
                    aSelectedCategoryPojo.setSubCategoryJobType(aCursor.getString(aCursor.getColumnIndex(CATEGORY_JOB_TYPE)));
                    aSelectedCategoryPojo.setSubCategoryHourlyRate(aCursor.getString(aCursor.getColumnIndex(CATEGORY_HOURLY_RATE)));
                    aSelectedCatArrList.add(aSelectedCategoryPojo);
                    aCursor.moveToNext();
                }
            }

            aCursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return aSelectedCatArrList;
    }

}
