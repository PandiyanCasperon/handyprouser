package com.handypro.volley;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.evernote.android.job.JobManager;
import com.handypro.activities.DemoJobCreator;

import io.fabric.sdk.android.Fabric;

public class HAppController extends MultiDexApplication {

    public static final String TAG = HAppController.class.getSimpleName();
    private static HAppController mInstance;
    private int TOTAL_CACHE = 10 * 1024 * 1024;
    private RequestQueue mRequestQueue;

    public static synchronized HAppController getInstance() {
        return mInstance;
    }



    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;
        JobManager.create(this).addJobCreator(new DemoJobCreator()); }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(HAppController.this);
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), TOTAL_CACHE);
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


}