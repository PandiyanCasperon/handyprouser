package com.handypro.Widgets;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.handypro.R;

import net.bohush.geometricprogressview.GeometricProgressView;

/**
 * Created by CAS63 on 2/16/2018.
 */

public class ProgressDialogcreated extends Dialog {
    private GeometricProgressView myProgressView;
    private TextView myMessageTXT;

    public ProgressDialogcreated(Context aContext) {
        super(aContext);

        try {
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.setContentView(R.layout.dialog_progress);
            this.setCancelable(false);
            this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            this.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
            myProgressView = (GeometricProgressView) this
                    .findViewById(R.id.dialog_progressView);
            this.setCanceledOnTouchOutside(false); // need to change false in future


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
