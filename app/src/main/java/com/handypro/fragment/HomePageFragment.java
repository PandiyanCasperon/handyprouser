package com.handypro.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.android.volley.Request;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.gson.Gson;
import com.handypro.BuildConfig;
import com.handypro.Dialog.colorchangedialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.AnswerPojo;
import com.handypro.Pojo.ParentCategorypojo;
import com.handypro.Pojo.ParentQuestionPojo;
import com.handypro.Pojo.SessionParentPojo;
import com.handypro.Pojo.SubCategoryPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreatedforfrontpage;
import com.handypro.activities.DemoSyncJob;
import com.handypro.activities.HomeActivity;
import com.handypro.activities.LocationSearchActivity;
import com.handypro.activities.QuestionAnswerActivity;
import com.handypro.activities.SearchCategoryActivity;
import com.handypro.activities.bookingFlow.SelectedCategoryListActivity;
import com.handypro.activities.login.LoginActivity;
import com.handypro.adapters.HomeCategoryListAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static androidx.appcompat.app.AppCompatActivity.RESULT_OK;

/**
 * Created by CAS61 on 2/9/2018.
 */

public class HomePageFragment extends Fragment {

    public static final int RequestPermissionCode = 7;
    public static ArrayList<ParentCategorypojo> myCategoryArrList;
    public static ArrayList<String> subcatname = new ArrayList<>();
    public static ArrayList<String> subcatid = new ArrayList<>();
    public static ArrayList<String> checkedimage = new ArrayList<>();
    public static ArrayList<String> uncheckedimage = new ArrayList<>();
    public static ArrayList<String> parentcat = new ArrayList<>();
    public static ArrayList<String> subcatmainid = new ArrayList<>();
    public static ArrayList<String> whats_included = new ArrayList<>();
    public static String procrewno = "";
    PlayerView aPlayerView;
    SimpleExoPlayer aSimpleExoPlayer;
    public static String storenextscreen = "0";
    public static LinearLayout activity_shedule_appointment_page_free_estimate_LAY;
    public static TextView activity_shedule_appointment_page_free_estimate_TXT;
    private RelativeLayout fragment_home_page_edit_locationLAY;
    public static ScrollView scrollView;
    private RelativeLayout showme;
    private LinearLayout iamfirst;
    public static String tasker_date = "";
    public static ArrayList<String> mySelectedSubCatIdArrdd;
    private View myView;
    private RelativeLayout myDrawerLAY, myCurrentLocationLAY;
    public static ListView myCategoryListView;
    private ImageView mySupportIMG;
    TextView txt = null;
    public static ArrayList<ParentCategorypojo> mySortedCategoryArrList;
    public static ArrayList<SubCategoryPojo> mySubCategoryArrList;
    public static HomeCategoryListAdapter myAdapter;
    private CustomTextView myWelcomeUserTXT, myRetryLblTXT;
    public static CustomTextView myCurrentLocTXT;
    private TextView mySearchSubCategoryET;
    private CustomButton myHomePageBTN;
    public static RelativeLayout myEmptyCategoryLAY;
    public static ProgressDialogcreatedforfrontpage myDialog;
    public static SharedPreference mySession;
    public static ConnectionDetector myConnectionManager;
    private int PLACE_SEARCH_REQUEST_CODE = 1029;
    private int REFRESHADAPTER = 1031;
    public static int SHEDULE_APPOINTMENT_FLAG = 2011;
    public static String mySelectedLocation = "", myLatSTR = "", myLongSTR = "";
    public static String myZipCodeSTR = "";
    public static ArrayList<String> mySelectedSubCatIdArr;
    private LinearLayout myNoInternetLAY, myMainLAY;
    private MaterialRefreshLayout myRefreshLAY;
    public static Dialog myGetZipCodeDialog;
    public static FragmentActivity myContext;
    public static ArrayList<ParentQuestionPojo> myParentInfoList = new ArrayList<>();
    public static String myCategoryTitleStr = "";
    public static ArrayList<SessionParentPojo> aSessionParentInfoList = null;

    private static void hideKeyboard(EditText myZipCodeET, Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(
                myZipCodeET.getApplicationWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS
        );
    }

    public static void checkedQuestion(final String subCategoryId) {
        //  Log.e("enter", "" + subCategoryId);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("category", subCategoryId);
        jsonParams.put("zipcode", myZipCodeSTR);

        ServiceRequest mRequest = new ServiceRequest(myContext);

        mRequest.makeServiceRequest(ServiceConstant.QUESTION_URL, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("QUESTION_URL", response);
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("1")) {
                        myParentInfoList.clear();
                        JSONObject aResponseObject = object.getJSONObject("response");
                        JSONArray aQuestionArray = aResponseObject.getJSONArray("QA");
                        myCategoryTitleStr = aResponseObject.getString("name");
                        if (aQuestionArray.length() > 0) {
                            for (int i = 0; i < aQuestionArray.length(); i++) {
                                ArrayList<AnswerPojo> aAnswerInfoList = new ArrayList<>();
                                JSONObject aQuestionObject = aQuestionArray.getJSONObject(i);
                                ParentQuestionPojo aParentPojo = new ParentQuestionPojo();
                                aParentPojo.setQuestionId(aQuestionObject.getString("_id"));
                                aParentPojo.setQuestiontitle(aQuestionObject.getString("question"));
                                aParentPojo.setQuestionAnsView(aQuestionObject.getString("ansview"));
                                aParentPojo.setQuestionAnsType(aQuestionObject.getString("anstype"));
                                JSONArray aAnswerArray = aQuestionObject.getJSONArray("answer");
                                if (aAnswerArray.length() > 0) {
                                    for (int y = 0; y < aAnswerArray.length(); y++) {
                                        JSONObject aAnswerObject = aAnswerArray.getJSONObject(y);
                                        AnswerPojo aAnswerPojo = new AnswerPojo();
                                        aAnswerPojo.setAnswerId(aAnswerObject.getString("_id"));
                                        aAnswerPojo.setAnswertitle(aAnswerObject.getString("key"));
                                        aAnswerInfoList.add(aAnswerPojo);
                                    }
                                }
                                aParentPojo.setAnswerpojo(aAnswerInfoList);
                                myParentInfoList.add(aParentPojo);
                            }
                        }
                    }
                    loadQuestionData(myParentInfoList, myCategoryTitleStr, subCategoryId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }

    private static void loadQuestionData(ArrayList<ParentQuestionPojo> aParentInfoList, String aCategoriyTitleStr, String aSubCategoryStr) {
        if (aParentInfoList.size() > 0) {
            Intent aIntent = new Intent(myContext, QuestionAnswerActivity.class);
            aIntent.putExtra("TITLE", aCategoriyTitleStr);
            aIntent.putExtra("SUBCATEGORY_ID", aSubCategoryStr);
            aIntent.putExtra("QUESTION_LIST", new Gson().toJson(aParentInfoList));
            aIntent.putExtra("SELECTED_CATEGORY_LIST", new Gson().toJson(myAdapter.getSelectedSubCatList()));
            aIntent.putExtra("ZIPCODE", myCurrentLocTXT.getText().toString());
            aIntent.putExtra("SUB_CATEGORY_ID", new Gson().toJson(subcatid));
            aIntent.putExtra("SUB_CATEGORY_NAME", new Gson().toJson(subcatname));
            aIntent.putExtra("PARENT_CATEGORY_ID", new Gson().toJson(parentcat));
            aIntent.putExtra("SELECTED_MAIN_ID", new Gson().toJson(subcatmainid));
            aIntent.putExtra("CHECKED_IMAGE", new Gson().toJson(checkedimage));
            aIntent.putExtra("UNCHECKED_IMAGE", new Gson().toJson(uncheckedimage));
            aIntent.putExtra("WHATS_INCLUDED", new Gson().toJson(whats_included));
            aIntent.putExtra("Latitude", myLatSTR);
            aIntent.putExtra("Longitude", myLongSTR);
            myContext.startActivity(aIntent);
        } else {
            if (myAdapter != null) {
                if (!myCurrentLocTXT.getText().toString().isEmpty()) {
                    ArrayList<ParentQuestionPojo> myParentQuestionList = new ArrayList<>();
                    if (myAdapter.getSelectedSubCatList() != null && myAdapter.getSelectedSubCatList().size() > 0) {
                        if (myConnectionManager.isConnectingToInternet()) {
                            aSessionParentInfoList = mySession.getSessionParentDetails();
                            SessionParentPojo aPojo = new SessionParentPojo();
                            aPojo.setSessionParentId(aSubCategoryStr);
                            aPojo.setaPojo(myParentQuestionList);
                            aSessionParentInfoList.add(aPojo);
                            mySession.putSessionParentPojo(aSessionParentInfoList);
                            getCategoryDetailValues(myContext);
                        } else {
                            HNDHelper.showErrorAlert(myContext, myContext.getResources().getString(R.string.nointernet_text));
                        }
                    } else {
                        HNDHelper.showErrorAlert(myContext, myContext.getResources().getString(R.string.categorymissing));
                    }
                } else {
                    showGetZipCodeDialog();
                }
            }
        }
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_home_page, container, false);
        mySession = new SharedPreference(getActivity());
        myDialog = new ProgressDialogcreatedforfrontpage(getActivity());
        myContext = getActivity();

        /*locationUpdatesComponent = new fusedlocationLocationUpdatesComponent(this);
        locationUpdatesComponent.onCreate(getActivity());
        locationUpdatesComponent.onStart();*/

        initializeHeaderView(myView);
        classAndWidgetInitialize(myView);
        getIntentValues();
        PerformRefresh();
        return myView;
    }

    private void PerformRefresh() {
        myRefreshLAY.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FetchCurrentLocation();
                        getData();
                        myRefreshLAY.finishRefresh();
                    }
                }, 750);
            }
        });
    }

    private void initializeHeaderView(View aView) {
        myDrawerLAY = aView.findViewById(R.id.fragment_home_page_LAY_menu);
        if (mySession.getLogInStatus() == false) {
            myDrawerLAY.setVisibility(View.VISIBLE);
        } else {
            myDrawerLAY.setVisibility(View.VISIBLE);
        }
        myDrawerLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mySession.getLogInStatus() == false) {
                    Intent aProfIntent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(aProfIntent);
                } else {
                    HomeActivity.openDrawer();
                }
            }
        });
    }

    private void classAndWidgetInitialize(View aView) {
        myConnectionManager = new ConnectionDetector(getActivity());
        mySelectedSubCatIdArrdd = new ArrayList<>();
        mySelectedSubCatIdArr = new ArrayList<>();
        aSessionParentInfoList = mySession.getSessionParentDetails();
        scrollView = aView.findViewById(R.id.sv);
        showme = aView.findViewById(R.id.showme);
        iamfirst = aView.findViewById(R.id.iamfirst);
         txt = aView.findViewById(R.id.margqueetext);
        LinearLayout margqueetextlay= aView.findViewById(R.id.margqueetextlay);
        txt.setSelected(true);
        getMaintenacestrig();
        margqueetextlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txt.getText().toString().contains("click") || txt.getText().toString().contains("Click"))
                {
                    getMaintenaceData();
                }
            }
        });
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txt.getText().toString().contains("click") || txt.getText().toString().contains("Click"))
                {
                    getMaintenaceData();
                }
            }
        });
        activity_shedule_appointment_page_free_estimate_TXT = aView.findViewById(R.id.activity_shedule_appointment_page_free_estimate_TXT);
        activity_shedule_appointment_page_free_estimate_LAY = aView.findViewById(R.id.activity_shedule_appointment_page_free_estimate_LAY);
        myCategoryListView = aView.findViewById(R.id.fragment_home_page_category_listView);
        myEmptyCategoryLAY = aView.findViewById(R.id.fragment_home_page_no_categoryLAY);
        myCurrentLocationLAY = aView.findViewById(R.id.fragment_home_page_current_locationLAY);
        myCurrentLocTXT = aView.findViewById(R.id.fragment_home_page_current_location_textView);
        fragment_home_page_edit_locationLAY = aView.findViewById(R.id.fragment_home_page_edit_locationLAY);
        myWelcomeUserTXT = aView.findViewById(R.id.fragment_home_page_welcome_user_textView);
        mySearchSubCategoryET = aView.findViewById(R.id.fragment_home_page_ET_search_location);
        myHomePageBTN = aView.findViewById(R.id.fragment_home_page_bottom_next_BTN);
        mySupportIMG = aView.findViewById(R.id.fragment_home_page_support_IMG);
        myNoInternetLAY = aView.findViewById(R.id.fragment_home_page_internetLAY);
        myMainLAY = aView.findViewById(R.id.fragment_home_page_mainLAY);
        myRetryLblTXT = aView.findViewById(R.id.fragment_home_page_retry_TXT);
        myRefreshLAY = aView.findViewById(R.id.fragment_home_page_Swipe_layout);

        SharedPreferences pref = getActivity().getSharedPreferences("firstshow", 0);
        if (pref.getString("showme", "").equals("1")) {
            iamfirst.setBackgroundResource(0);
            showme.setVisibility(View.VISIBLE);
        } else {
            showme.setVisibility(View.GONE);
        }
        iamfirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getActivity().getSharedPreferences("firstshow", 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("showme", "1");
                editor.apply();
                editor.commit();
                if (pref.getString("showme", "").equals("1")) {
                    iamfirst.setBackgroundResource(0);
                    showme.setVisibility(View.VISIBLE);
                } else {
                    showme.setVisibility(View.GONE);
                }
            }
        });

        myWelcomeUserTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mySession.getLogInStatus()) {
                    Intent aProfIntent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(aProfIntent);
                }
            }
        });

        myCurrentLocationLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGetZipCodeDialog();
            }
        });

        fragment_home_page_edit_locationLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aLocationSearchIntent = new Intent(getActivity(), LocationSearchActivity.class);
                startActivityForResult(aLocationSearchIntent, PLACE_SEARCH_REQUEST_CODE);
            }
        });

        mySearchSubCategoryET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (storenextscreen.equalsIgnoreCase("0")) {
                    HNDHelper.showErrorAlert(getActivity(), getResources().getString(R.string.missingsome));
                } else {
                    Intent in = new Intent(getActivity(), SearchCategoryActivity.class);
                    in.putExtra("selectedSubCategories", myAdapter.getSelectedSubCatList());
                    in.putExtra("myZipCodeSTR", myZipCodeSTR);
                    in.putExtra("myLatSTR", myLatSTR);
                    in.putExtra("myLongSTR", myLongSTR);
                    in.putExtra("address", myCurrentLocTXT.getText().toString());
                    Bundle args = new Bundle();
                    args.putSerializable("selectedSubCategories", subcatmainid);
                    args.putSerializable("parentcat", parentcat);
                    args.putSerializable("subcatname", subcatname);
                    args.putSerializable("subcatid", subcatid);
                    args.putSerializable("checkedimage", checkedimage);
                    args.putSerializable("uncheckedimage", uncheckedimage);
                    in.putExtra("BUNDLE", args);
                    startActivityForResult(in, REFRESHADAPTER);
                }
            }
        });
        myHomePageBTN.setText(getResources().getString(R.string.fragment_home_page_next_label));
        activity_shedule_appointment_page_free_estimate_LAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGetZipCodeDialog();
            }
        });
        FetchCurrentLocation();
        setWelcomeUserTXT();
        WidgetOnclick();
        SharedPreferences prefs = getActivity().getSharedPreferences("firstshow", 0);
        if (mySession.GetZIPCode().trim().length() == 0) {
            showGetZipCodeDialog();
        } else {
            myZipCodeSTR = mySession.GetZIPCode();
            myCurrentLocTXT.setText(mySession.GetZIPCode());
        }
        getData();
    }

    private static void getData() {
        if (myConnectionManager.isConnectingToInternet()) {
            getCategoryListValues();
        } else {
            Toast.makeText(myContext, myContext.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    private void getIntentValues() {
        Intent intent = getActivity().getIntent();
        mySelectedSubCatIdArr = (ArrayList<String>) intent.getSerializableExtra("selectedSubCategories");
    }

    private void WidgetOnclick() {
        myRetryLblTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FetchCurrentLocation();
                getData();
            }
        });


        mySupportIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (procrewno != null || procrewno.length() > 2) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + procrewno));
                    startActivity(callIntent);
                }
            }
        });

        myHomePageBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (storenextscreen.equalsIgnoreCase("0")) {
                    HNDHelper.showErrorAlert(getActivity(), getResources().getString(R.string.missingsome));
                } else {
                    if (myAdapter != null) {
                        if (!myCurrentLocTXT.getText().toString().isEmpty()) {
                            if (myAdapter.getSelectedSubCatList() != null && myAdapter.getSelectedSubCatList().size() > 0) {
                                if (myConnectionManager.isConnectingToInternet()) {
                                    getCategoryDetailValues(getActivity());
                                } else {
                                    HNDHelper.showErrorAlert(getActivity(), getResources().getString(R.string.nointernet_text));
                                }
                            } else {
                                HNDHelper.showErrorAlert(getActivity(), getResources().getString(R.string.categorymissing));
                            }
                        } else {
                            showGetZipCodeDialog();
                        }
                    }
                }
            }
        });
    }

    private static void showGetZipCodeDialog() {
        myGetZipCodeDialog = new Dialog(myContext);
        myGetZipCodeDialog.getWindow();
        myGetZipCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myGetZipCodeDialog.setContentView(R.layout.alert_dialog_get_zipcode);
        myGetZipCodeDialog.setCanceledOnTouchOutside(true);
        myGetZipCodeDialog.getWindow().getAttributes().windowAnimations = R.style.animations_photo_Picker;
        myGetZipCodeDialog.show();
        myGetZipCodeDialog.getWindow().setGravity(Gravity.CENTER);

        final CustomEdittext myZipCodeET = myGetZipCodeDialog.findViewById(R.id.alert_dialog_ET_get_zipcode);
        CustomTextView aCancelDialogTXT = myGetZipCodeDialog.findViewById(R.id.alert_dialog_get_zipcode_cancel_TXT);
        CustomTextView aSubmitZipCodeTXT = myGetZipCodeDialog.findViewById(R.id.alert_dialog_get_zipcode_ok_TXT);

        aCancelDialogTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myGetZipCodeDialog.dismiss();
                hideKeyboard(myZipCodeET, myContext.getApplicationContext());
            }
        });

        aSubmitZipCodeTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myZipCodeET.getText() != null && myZipCodeET.getText().toString().length() != 0) {
                    myZipCodeSTR = myZipCodeET.getText().toString().trim();
                    myCurrentLocTXT.setText(myZipCodeSTR);
                    //If user change the new ZIP code means clear the old address from SharedPreferences
                    mySession.SetLocationDetails(myZipCodeSTR, "", "", mySession.GetFullAddress());
                } else {
                    return;
                }
                myGetZipCodeDialog.dismiss();
                if (myConnectionManager.isConnectingToInternet()) {
                    getData();
                } else {
                    Toast.makeText(myContext, myContext.getResources().getString(R.string.fragment_home_connection_error_no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setWelcomeUserTXT() {
        if (mySession.getLogInStatus()) {
            myWelcomeUserTXT.setVisibility(View.VISIBLE);
            myWelcomeUserTXT.setText(getResources().getString(R.string.fragment_home_page_welcome_label).concat(" ").concat(mySession.getUserDetails().getUserName()).concat("!"));
            myWelcomeUserTXT.setTextColor(Color.parseColor("#000000"));
        } else {
            // myWelcomeUserTXT.setText(getResources().getString(R.string.kindlylogin));
            // myWelcomeUserTXT.setTextColor(Color.parseColor("#ff0000"));
            myWelcomeUserTXT.setVisibility(View.VISIBLE);
            myWelcomeUserTXT.setText(getResources().getString(R.string.fragment_home_page_welcome_label).concat(" to ").concat(BuildConfig.App_Name).concat("!"));
            myWelcomeUserTXT.setTextColor(Color.parseColor("#000000"));
        }
    }

    /**
     * perform search subcategories
     *
     * @return
     */


    private void FetchCurrentLocation() {
        if (myConnectionManager.isConnectingToInternet()) {
            myNoInternetLAY.setVisibility(View.GONE);
            myHomePageBTN.setVisibility(View.VISIBLE);
            myMainLAY.setVisibility(View.VISIBLE);
        } else {
            myNoInternetLAY.setVisibility(View.VISIBLE);
            myHomePageBTN.setVisibility(View.GONE);
            myMainLAY.setVisibility(View.GONE);
        }
    }

    private static void getCategoryListValues() {

        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("zipcode", myZipCodeSTR);
        ServiceRequest mRequest = new ServiceRequest(myContext);
        mRequest.makeServiceRequest(ServiceConstant.CategoriesUrl, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("---------Categories response------------" + response);
                Log.e("response", response);
                mySession = new SharedPreference(myContext);
                mySession.setCategoryId("");
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                try {
                    JSONObject object = new JSONObject(response);
                    myCategoryArrList = new ArrayList<>();
                    myCategoryArrList.clear();

                    if (object.getString("status").equals("1")) {
                        storenextscreen = "1";
                        activity_shedule_appointment_page_free_estimate_TXT.setVisibility(View.GONE);
                        activity_shedule_appointment_page_free_estimate_LAY.setVisibility(View.GONE);

                        JSONObject aRespObject = object.getJSONObject("response");
                        if (aRespObject.has("procrew_phoneno")) {
                            procrewno = aRespObject.getString("procrew_phoneno");
                        } else {
                            procrewno = "";
                        }

                        SharedPreferences callprocrew;
                        callprocrew = myContext.getSharedPreferences("callprocrew", 0); // 0 - for private mode
                        SharedPreferences.Editor editorcallprocrew = callprocrew.edit();
                        editorcallprocrew.putString("call", "" + procrewno);
                        editorcallprocrew.apply();
                        parentcat.clear();
                        subcatname.clear();
                        subcatid.clear();
                        checkedimage.clear();
                        uncheckedimage.clear();
                        subcatmainid.clear();
                        whats_included.clear();

                        JSONArray aCategoryArr = aRespObject.getJSONArray("category");
                        for (int i = 0; i < aCategoryArr.length(); i++) {
                            JSONObject aMainObj = aCategoryArr.getJSONObject(i);
                            ParentCategorypojo aParntCatPojo = new ParentCategorypojo();
                            aParntCatPojo.setParentCategory_name(aMainObj.getString("cat_name"));
                            aParntCatPojo.setParentSubMoreCategory_item("");
                            aParntCatPojo.setParentCategoryID(aMainObj.getString("cat_id"));
                            JSONArray aSubCatgryArr = aMainObj.getJSONArray("subcategory");
                            mySubCategoryArrList = new ArrayList<SubCategoryPojo>();
                            for (int a = 0; a < aSubCatgryArr.length(); a++) {
                                JSONObject aSubCatObj = aSubCatgryArr.getJSONObject(a);
                                SubCategoryPojo aSubCatPojo = new SubCategoryPojo();
                                aSubCatPojo.setSubCategoryName(aSubCatObj.getString("cat_name"));
                                aSubCatPojo.setSubCategoryMainName(aMainObj.getString("cat_name"));
                                aSubCatPojo.setSubCategoryActiveIcon(aSubCatObj.getString("active_icon"));
                                aSubCatPojo.setSubCategoryInactiveIcon(aSubCatObj.getString("inactive_icon"));
                                aSubCatPojo.setIsSubCategorySelected("0");
                                aSubCatPojo.setSubCategoryImage(aSubCatObj.getString("image"));
                                aSubCatPojo.setSubCategoryId(aSubCatObj.getString("cat_id"));
                                aSubCatPojo.setJoborestimate("(JOB)");

                                if (aSubCatObj.has("userstatus") && aSubCatObj.getString("userstatus").equals("2")) {
                                    aSubCatPojo.setUserstatus(aSubCatObj.getString("userstatus"));
                                } else {
                                    aSubCatPojo.setUserstatus("1");
                                    mySubCategoryArrList.add(aSubCatPojo);
                                    subcatmainid.add(aSubCatObj.getString("cat_id"));
                                    parentcat.add(aMainObj.getString("cat_name"));
                                    subcatname.add(aSubCatObj.getString("cat_name"));
                                    subcatid.add(aSubCatObj.getString("cat_id"));
                                    checkedimage.add(aSubCatObj.getString("active_icon"));
                                    uncheckedimage.add(aSubCatObj.getString("inactive_icon"));
                                    whats_included.add(aSubCatObj.getString("whats_included"));
                                }
                            }
                            if (mySelectedSubCatIdArr != null && mySelectedSubCatIdArr.size() > 0) {
                                for (int a = 0; a < mySubCategoryArrList.size(); a++) {
                                    for (int b = 0; b < mySelectedSubCatIdArr.size(); b++) {
                                        if (mySubCategoryArrList.get(a).getSubCategoryId().equalsIgnoreCase(mySelectedSubCatIdArr.get(b))) {
                                            mySubCategoryArrList.get(a).setIsSubCategorySelected("1");
                                        }
                                    }
                                }
                            }
                            aParntCatPojo.setSubCategoryListArr(mySubCategoryArrList);
                            aParntCatPojo.setTempSubCategoryListArr(mySubCategoryArrList);
                            myCategoryArrList.add(aParntCatPojo);
                        }
                        mySortedCategoryArrList = new ArrayList<>();
                        mySortedCategoryArrList.clear();
                        mySortedCategoryArrList.addAll(myCategoryArrList);
                        LoadListData();
                    } else {
                        storenextscreen = "0";
                        activity_shedule_appointment_page_free_estimate_TXT.setVisibility(View.VISIBLE);
                        activity_shedule_appointment_page_free_estimate_TXT.setText(object.getString("response"));
                        activity_shedule_appointment_page_free_estimate_LAY.setVisibility(View.VISIBLE);
                        myCategoryListView.setVisibility(View.GONE);
                        myEmptyCategoryLAY.setVisibility(View.VISIBLE);
                        //  HNDHelper.showResponseErrorAlert(getActivity(), object.getString("response"));
                    }
                    postMode(myContext);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private static void LoadListData() {
        if (mySortedCategoryArrList != null) {
            if (mySortedCategoryArrList.size() > 0) {
                myEmptyCategoryLAY.setVisibility(View.GONE);
                myCategoryListView.setVisibility(View.VISIBLE);
                myAdapter = new HomeCategoryListAdapter(mySortedCategoryArrList, myContext);
                myAdapter.notifyDataSetChanged();
                myCategoryListView.setAdapter(myAdapter);
                HNDHelper.setListViewHeightBasedOnChildren(myCategoryListView);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Scroll 1000px down
                        // mScrollView.smoothScrollTo(0, 1000);
                        scrollView.fullScroll(ScrollView.FOCUS_UP);
                    }
                }, 150);
                // scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else {
                myEmptyCategoryLAY.setVisibility(View.VISIBLE);
                myCategoryListView.setVisibility(View.GONE);
            }
        }
    }

    /* *//*
     * enable gps for getting latitude longitude details
     *
     *//*
    private void enableGpsService() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }*/

    /**
     * get current address
     *
     * @param LATITUDE
     * @param LONGITUDE
     * @return
     */
    private String getAddress(double LATITUDE, double LONGITUDE) {
        String strAdd = "";

        try {
            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);


                StringBuilder strReturnedAddress = new StringBuilder();

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                if (returnedAddress.getPostalCode() != null && !returnedAddress.getPostalCode().isEmpty()) {
                    myZipCodeSTR = returnedAddress.getPostalCode();
                } else {
                    myZipCodeSTR = "";
                }

                strAdd = strReturnedAddress.toString();
            } else {
                Log.e("Crnt location address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Crnt location address", "Cannot get Address!");
        }

        return myZipCodeSTR;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("--------------onActivityResult requestCode----------------" + requestCode);
        if (resultCode == RESULT_OK) {
            if (data != null) {
                if ((requestCode == PLACE_SEARCH_REQUEST_CODE)) {

                    mySelectedLocation = data.getStringExtra("Selected_Location");
                    myZipCodeSTR = data.getStringExtra("ZipCode");
                    myLatSTR = data.getStringExtra("Selected_Latitude");
                    myLongSTR = data.getStringExtra("Selected_Longitude");

                    System.out.println("selectedLatitude---------" + myLatSTR);
                    System.out.println("selectedLongitude---------" + myLongSTR);
                    System.out.println("selectedLocation----------" + mySelectedLocation);
                    System.out.println("houseNo-----------" + data.getStringExtra("HouseNo"));
                    System.out.println("city-----------" + data.getStringExtra("City"));
                    System.out.println("postalCode-----------" + data.getStringExtra("ZipCode"));
                    System.out.println("location-----------" + data.getStringExtra("Location"));

                    if (!mySelectedLocation.isEmpty() && !myZipCodeSTR.isEmpty()) {
                        myCurrentLocTXT.setText(myZipCodeSTR);

                        if (myConnectionManager.isConnectingToInternet()) {
                            // getavailanle(ServiceConstant.pincodehint);
                            getCategoryListValues();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }

                        mySession.SetLocationDetails(myZipCodeSTR, myLatSTR, myLongSTR, mySelectedLocation);
                    } else {
                        if (myLatSTR.isEmpty() || myLongSTR.isEmpty()) {
                            myCurrentLocTXT.setText(getResources().getString(R.string.class_homepagefragment_fetching));
                            Toast.makeText(getActivity(), "Unable to your location please try again selecting new location in map.", Toast.LENGTH_SHORT).show();
                        }
                    }

                } else if (requestCode == REFRESHADAPTER) {
                    myAdapter.notifyDataSetChanged();
                    mySession.setNewSelection(true);
                    mySession.setNewsearch(true);
                    Log.e("enter", data.getStringExtra("SUBCAT_ID"));
                    checkedQuestion(data.getStringExtra("SUBCAT_ID"));
                }
            }
        } else if (requestCode == SHEDULE_APPOINTMENT_FLAG) {
            myAdapter.notifyDataSetChanged();
            mySession.setNewSelection(true);
            mySession.setNewsearch(false);
        }

    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + procrewno));
            startActivity(callIntent);
            return true;
        } else {
            return false;
        }
    }

    private void RequestMultiplePermission() {
        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(getActivity(), new String[]
                {
                        Manifest.permission.CALL_PHONE
                }, RequestPermissionCode);

    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RequestPermissionCode) {
            if (grantResults.length > 0) {
                boolean call = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (call) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + procrewno));
                    startActivity(callIntent);
                }
            }
        }
    }

    private static void postMode(Context mContext) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        if (mySession.getUserDetails() != null && mySession.getUserDetails().getUserId() != null) {
            jsonParams.put("user", mySession.getUserDetails().getUserId());
        } else {
            jsonParams.put("user", "");
        }
        jsonParams.put("user_type", "user");
        jsonParams.put("mode", "gcm");
        jsonParams.put("type", "android");

        ServiceRequest mRequest = new ServiceRequest(mContext);
        mRequest.makeServiceRequest(ServiceConstant.Notification_mode, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("1")) {
                        System.out.println("---------mode response------------" + response);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

            }
        });
    }

    public boolean CheckingPermissionIsEnabledOrNot() {


        int call = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);

        return call == PackageManager.PERMISSION_GRANTED;
    }

    public void onResume() {
        super.onResume();
        SharedPreferences prefjobid = getActivity().getSharedPreferences("roomcreated", 0);
        SharedPreferences.Editor prefeditor = prefjobid.edit();
        prefeditor.clear();
        prefeditor.apply();
        if (mySession.getLogInStatus() == false) {
            myDrawerLAY.setVisibility(View.VISIBLE);
        } else {
            try {
                DemoSyncJob.scheduleJob();
            } catch (Exception e) {
                e.printStackTrace();
            }

            myDrawerLAY.setVisibility(View.VISIBLE);
            if (mySession.getLogInStatus() == true) {
                myWelcomeUserTXT.setText(getResources().getString(R.string.fragment_home_page_welcome_label).concat(" ").concat(mySession.getUserDetails().getUserName()).concat("!"));
                myWelcomeUserTXT.setVisibility(View.VISIBLE);
                myWelcomeUserTXT.setTextColor(Color.parseColor("#000000"));
            } else {
                myWelcomeUserTXT.setText(getResources().getString(R.string.kindlylogin));
                myWelcomeUserTXT.setTextColor(Color.parseColor("#ff0000"));
            }
            if (myAdapter != null)
                myAdapter.notifyDataSetChanged();

            try {
                postRequest_ModeChange("gcm");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void postRequest_ModeChange(final String aModeType) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user", mySession.getUserDetails().getUserId());
        jsonParams.put("user_type", "user");
        jsonParams.put("mode", aModeType);
        jsonParams.put("type", "android");

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(ServiceConstant.MODEUPDATE_URL, Request.Method.POST,
                jsonParams, new ServiceRequest.ServiceListener() {
                    @Override
                    public void onCompleteListener(String response) {
                        System.out.println("---------------Mode Response-----------------" + response);
                        String sStatus = "", sResponse = "";
                        try {
                            JSONObject object = new JSONObject(response);
                            sStatus = object.getString("status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (sStatus.equalsIgnoreCase("1")) {
                            //    session.logoutUser();
                            if (aModeType.equalsIgnoreCase("socket")) {
//                                Log.e("MODE UPDATED", "SOCKET");
                                System.out.println("---------------Mode Response-----------------SOCKET");
                            } else {
//                                Log.e("MODE UPDATED", "GCM");
                                System.out.println("---------------Mode Response-----------------FCM");
                            }

//                            if (!socketHandler.getSocketManager().isConnected) {
//                                socketHandler.getSocketManager().disconnect();
//                            }
                        }
                    }

                    @Override
                    public void onErrorListener() {
                    }
                });
    }

    private static void getCategoryDetailValues(Context aContext) {
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParam = new HashMap<>();
        try {
            JSONArray aJsonArr = new JSONArray(myAdapter.getSelectedSubCatList());
            System.out.println("----" + aJsonArr.toString());
            for (int a = 0; a < aJsonArr.length(); a++) {
                jsonParam.put("subcategory[" + a + "]", aJsonArr.getString(a));
            }
            jsonParam.put("zipcode", myCurrentLocTXT.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(ServiceConstant.CategoryDetail, Request.Method.POST, jsonParam, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("---------Categories detail response------------" + response);
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("1")) {
                        JSONObject aRespObj = object.getJSONObject("response");
                        if (object.has("task_date")) {
                            tasker_date = object.getString("task_date");
                        }
                        JSONArray aMainArr = aRespObj.getJSONArray("category");
                        String estimate_text, estimate_count, estimate_hours;
                        StringBuilder nonavailablecat = new StringBuilder();
                        String mynocat = "";
                        int jkx = 0;
                        mySelectedSubCatIdArrdd.clear();
                        for (int a = 0; a < aMainArr.length(); a++) {
                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);
                            String cat_name = aSubCatObj.getString("cat_name");
                            String tasker_availablity = aSubCatObj.getString("tasker_availablity");
                            String cat_id = aSubCatObj.getString("id");
                            if (tasker_availablity.equals("0")) {
                                jkx = 1;
                                nonavailablecat.append("," + cat_name);
                            } else {
                                mySelectedSubCatIdArrdd.add(cat_id);
                            }
                        }

                        if (jkx == 1) {
                            mynocat = nonavailablecat.toString();
                            if (mynocat.startsWith(",")) {
                                mynocat = mynocat.substring(1);
                            }

                            final colorchangedialog mDialog = new colorchangedialog(myContext);
                            mDialog.setDialogTitle(myContext.getResources().getString(R.string.rating_header_sorry_textView));
                            mDialog.setDialogMessage(myContext.getResources().getString(R.string.noavailcat));
                            mDialog.setcatMessage(mynocat);
                            mDialog.setPositiveButton(
                                    myContext.getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();
                                            /*String aSelectedIdSTR = android.text.TextUtils.join(",", mySelectedSubCatIdArrdd);
                                            mySession.setCategoryId(aSelectedIdSTR);
                                            Intent aSheduleAppointmntIntent = new Intent(getActivity(), SheduleAppointmentActivity.class);
                                            aSheduleAppointmntIntent.putExtra("selectedSubCategories", mySelectedSubCatIdArrdd);
                                            aSheduleAppointmntIntent.putExtra("zipCode", myZipCodeSTR);
                                            aSheduleAppointmntIntent.putExtra("Latitude", myLatSTR);
                                            aSheduleAppointmntIntent.putExtra("Longitude", myLongSTR);
                                            aSheduleAppointmntIntent.putExtra("task_date", tasker_date);
                                            mySession.setNewSelection(false);
                                            mySession.setNewsearch(false);
                                            startActivityForResult(aSheduleAppointmntIntent, SHEDULE_APPOINTMENT_FLAG);*/

                                           /* Intent in = new Intent(getActivity(), SelectedCategoryListActivity.class);
                                            in.putExtra("selectedSubCategories", mySelectedSubCatIdArrdd);
                                            in.putExtra("zipCode", myZipCodeSTR);
                                            in.putExtra("Latitude", myLatSTR);
                                            in.putExtra("Longitude", myLongSTR);
                                            in.putExtra("task_date", tasker_date);
                                            Bundle args = new Bundle();
                                            args.putSerializable("SubCategoriesID", subcatmainid);
                                            args.putSerializable("parentcat", parentcat);
                                            args.putSerializable("subcatname", subcatname);
                                            args.putSerializable("subcatid", subcatid);
                                            args.putSerializable("checkedimage", checkedimage);
                                            args.putSerializable("uncheckedimage", uncheckedimage);
                                            args.putSerializable("whats_included", whats_included);
                                            in.putExtra("BUNDLE", args);
                                            mySession.setNewSelection(false);
                                            mySession.setNewsearch(false);
                                            startActivityForResult(in, SHEDULE_APPOINTMENT_FLAG);*/
                                        }
                                    }
                            );
                            mDialog.show();
                        } else {
                            /*String aSelectedIdSTR = android.text.TextUtils.join(",", myAdapter.getSelectedSubCatList());
                            mySession.setCategoryId(aSelectedIdSTR);
                            Intent aSheduleAppointmntIntent = new Intent(getActivity(), SheduleAppointmentActivity.class);
                            aSheduleAppointmntIntent.putExtra("selectedSubCategories", myAdapter.getSelectedSubCatList());
                            aSheduleAppointmntIntent.putExtra("zipCode", myZipCodeSTR);
                            aSheduleAppointmntIntent.putExtra("Latitude", myLatSTR);
                            aSheduleAppointmntIntent.putExtra("Longitude", myLongSTR);
                            aSheduleAppointmntIntent.putExtra("task_date", tasker_date);
                            mySession.setNewSelection(false);
                            mySession.setNewsearch(false);
                            startActivityForResult(aSheduleAppointmntIntent, SHEDULE_APPOINTMENT_FLAG);*/

                            /*Intent in = new Intent(getActivity(), CategorySelectedListActivity.class);
                            in.putExtra("selectedSubCategories", myAdapter.getSelectedSubCatList());
                            in.putExtra("myZipCodeSTR", myZipCodeSTR);
                            in.putExtra("myLatSTR", myLatSTR);
                            in.putExtra("myLongSTR", myLongSTR);
                            in.putExtra("address", myCurrentLocTXT.getText().toString());
                            Bundle args = new Bundle();
                            args.putSerializable("selectedSubCategories", subcatmainid);
                            args.putSerializable("parentcat", parentcat);
                            args.putSerializable("subcatname", subcatname);
                            args.putSerializable("subcatid", subcatid);
                            args.putSerializable("checkedimage", checkedimage);
                            args.putSerializable("uncheckedimage", uncheckedimage);
                            args.putSerializable("whats_included", whats_included);
                            in.putExtra("BUNDLE", args);
                            startActivityForResult(in, SHEDULE_APPOINTMENT_FLAG);*/

                            String aSelectedIdSTR = android.text.TextUtils.join(",", myAdapter.getSelectedSubCatList());
                            mySession.setCategoryId(aSelectedIdSTR);
                            Intent in = new Intent(myContext, SelectedCategoryListActivity.class);
                            in.putExtra("selectedSubCategories", mySelectedSubCatIdArrdd);
                            in.putExtra("zipCode", myZipCodeSTR);
                            in.putExtra("Latitude", myLatSTR);
                            in.putExtra("Longitude", myLongSTR);
                            in.putExtra("task_date", tasker_date);
                            Bundle args = new Bundle();
                            args.putSerializable("SubCategoriesID", subcatmainid);
                            args.putSerializable("parentcat", parentcat);
                            args.putSerializable("subcatname", subcatname);
                            args.putSerializable("subcatid", subcatid);
                            args.putSerializable("checkedimage", checkedimage);
                            args.putSerializable("uncheckedimage", uncheckedimage);
                            args.putSerializable("whats_included", whats_included);
                            in.putExtra("BUNDLE", args);
                            mySession.setNewSelection(false);
                            mySession.setNewsearch(false);
                            myContext.startActivityForResult(in, SHEDULE_APPOINTMENT_FLAG);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    private void GetLatLonFromZIPCode(String zipCode) {
        final Geocoder geocoder = new Geocoder(getContext());
        try {
            List<Address> addresses = geocoder.getFromLocationName(zipCode, 1);
            if (addresses != null && !addresses.isEmpty()) {
                Address address = addresses.get(0);
                // Use the address as needed
                myLatSTR = String.valueOf(address.getLatitude());
                myLongSTR = String.valueOf(address.getLongitude());

                String addressLineOne = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                SharedPreferences getcard;
                getcard = getActivity().getSharedPreferences("zipcode", 0); // 0 - for private mode
                SharedPreferences.Editor editor = getcard.edit();
                editor.putString("code", zipCode);
                editor.putString("Latitude", myLatSTR);
                editor.putString("Longitude", myLongSTR);
                editor.putString("address", zipCode);
                mySelectedLocation = addressLineOne + "," + city + "," + state + "," + country + " - " + zipCode;
                editor.putString("FullAddress", "" + mySelectedLocation);
                editor.apply();

                String message = String.format("Latitude: %f, Longitude: %f",
                        address.getLatitude(), address.getLongitude());
                Log.e("Location ", "" + address.getLatitude() + " " + address.getLongitude());
            } else {
                // Display appropriate message when Geocoder services are not available
                Toast.makeText(getContext(), "Unable to geocode zipcode", Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            // handle exception
        }
    }

    /*@Override
    public void onLocationChanged(Location location) {

        if (mySession.GetZIPCode().trim().length() == 0) {
            myLatitude = location.getLatitude();
            myLongitude = location.getLongitude();
            myLatSTR = String.valueOf(myLatitude);
            myLongSTR = String.valueOf(myLongitude);
            myCurrentLocTXT.setText(getAddress(myLatitude, myLongitude));
            io++;

            if (io == 1) {
                if (myConnectionManager.isConnectingToInternet()) {
                    getData();
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onLocationUpdate(Location location) {

        if (mySession.GetZIPCode().trim().length() == 0) {
            if (myLatitude == 0.0) {
                myLatitude = location.getLatitude();
                myLongitude = location.getLongitude();
                myLatSTR = String.valueOf(myLatitude);
                myLongSTR = String.valueOf(myLongitude);
                myCurrentLocTXT.setText(getAddress(myLatitude, myLongitude));
                if (myConnectionManager.isConnectingToInternet()) {
                    getData();
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }*/


    private void getMaintenacestrig() {
        HashMap<String, String> jsonParams = new HashMap<>();
        ServiceRequest mServiceRequest = new ServiceRequest(getActivity());
        mServiceRequest.makeServiceRequest(ServiceConstant.GETHMP_DETAILS, Request.Method.GET, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                Log.e("hmp", response);
                String Str_status, google_api = "";

                try {
                    JSONObject jobject = new JSONObject(response);

                    if (jobject.getString("status").equalsIgnoreCase("1")) {
                        JSONObject aResponseObject = jobject.getJSONObject("response");
                        if(aResponseObject.has("scrollmsg"))
                        {
                            txt.setText(aResponseObject.getString("scrollmsg"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }
    private void getMaintenaceData() {
        HashMap<String, String> jsonParams = new HashMap<>();
        ServiceRequest mServiceRequest = new ServiceRequest(getActivity());
        mServiceRequest.makeServiceRequest(ServiceConstant.GETHMP_DETAILS, Request.Method.GET, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                Log.e("hmp", response);
                String Str_status, google_api = "";

                try {
                    JSONObject jobject = new JSONObject(response);

                    if (jobject.getString("status").equalsIgnoreCase("1")) {
                        JSONObject aResponseObject = jobject.getJSONObject("response");
                        if (aResponseObject.has("popupdescription")) {
                            showMaintenanceDialog(aResponseObject.getString("popupdescription") + "<style>img{width:100%;height:auto!important;}</style>");
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }


    private void showMaintenanceDialog(String aDescriptionStr) {
        final Dialog aPopDialog = new Dialog(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        aPopDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View aDialogView = inflater.inflate(R.layout.dialog_maintenance, null);
        aPopDialog.setContentView(aDialogView);
        aPopDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        WebView aWebview = (WebView) aPopDialog.findViewById(R.id.dialog_maintenance_WEBVW);
        LinearLayout aCloseLAY = (LinearLayout) aPopDialog.findViewById(R.id.dialog_maintenance_LAY_close);
        CustomButton aSubmitBTN = (CustomButton) aPopDialog.findViewById(R.id.dialog_maintenance_BTN_watchvideo);

        aSubmitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aPopDialog.dismiss();
                getVideoMaintainenceData();
            }
        });
        aCloseLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aPopDialog.dismiss();
            }
        });
        aWebview.loadData(aDescriptionStr, "text/html", "UTF-8");
        aPopDialog.show();
    }

    private void getVideoMaintainenceData() {
        HashMap<String, String> jsonParams = new HashMap<>();
        ServiceRequest mServiceRequest = new ServiceRequest(getActivity());
        mServiceRequest.makeServiceRequest(ServiceConstant.GETHMP_DETAILS, Request.Method.GET, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                Log.e("hmp", response);

                try {
                    JSONObject jobject = new JSONObject(response);

                    if (jobject.getString("status").equalsIgnoreCase("1")) {
                        JSONObject aResponseObject = jobject.getJSONObject("response");
                        if (aResponseObject.has("popupdescription2") && aResponseObject.has("videourl")) {
                            showVideMaintenanceDialog(aResponseObject.getString("popupdescription2"),
                                    aResponseObject.getString("videourl"));
                        } else if (aResponseObject.has("popupdescription2")) {
                            showVideMaintenanceDialog("", aResponseObject.getString("videourl"));
                        } else if (aResponseObject.has("videourl")) {
                            showVideMaintenanceDialog(aResponseObject.getString("popupdescription2"),
                                    "");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }


    private void showVideMaintenanceDialog(String aDescriptionStr, String aVideoURLStr) {

        final Dialog aPopDialog = new Dialog(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        aPopDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View aDialogView = inflater.inflate(R.layout.dialog_watchvideo_maintenance, null);
        aPopDialog.setContentView(aDialogView);
        aPopDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        WebView aWebview = (WebView) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_WEBVW);
        LinearLayout aCloseLAY = (LinearLayout) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_LAY_close);
        CustomButton aSubmitBTN = (CustomButton) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_BTN_watchvideo);
        final ImageView aPlayBTN = (ImageView) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_BTN_play);
        final RelativeLayout aVideoLAY = (RelativeLayout) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_LAY_video);
        final ProgressBar aProgressBar = (ProgressBar) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_PRG);

        aPlayerView = (PlayerView)aPopDialog.findViewById(R.id.player_view);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Uri videoUrl = Uri.parse("https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4");

        LoadControl loadControl = new DefaultLoadControl();
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
        aSimpleExoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(),trackSelector,loadControl);
        DefaultHttpDataSourceFactory factory = new DefaultHttpDataSourceFactory("exoplayer_video");
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        final MediaSource mediaSource = new ExtractorMediaSource(videoUrl,factory,extractorsFactory,null,null);
        aPlayerView.setPlayer(aSimpleExoPlayer);
        aPlayerView.setKeepScreenOn(true);
        aPlayerView.hideController();


        aPlayBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aSimpleExoPlayer.prepare(mediaSource);
                aPlayBTN.setVisibility(View.GONE);
                aPlayerView.showController();
                aSimpleExoPlayer.setPlayWhenReady(true);
            }
        });

        aSimpleExoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == Player.STATE_BUFFERING){
                    aProgressBar.setVisibility(View.VISIBLE);
                }else if (playbackState == Player.STATE_READY){
                    aProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {
            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
            }

            @Override
            public void onPositionDiscontinuity(int reason) {
            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            }

            @Override
            public void onSeekProcessed() {
            }
        });


        /*final ImageView aPauseIMG = (ImageView) aPopDialog.findViewById(R.id.dialog_watchvideo_maintainance_IMG_pause);
        final ImageView aBackwardIMG = (ImageView) aPopDialog.findViewById(R.id.dialog_watchvideo_maintainance_IMG_backward);
        final ImageView aForwardIMG = (ImageView) aPopDialog.findViewById(R.id.dialog_watchvideo_maintainance_IMG_forward);
        final RelativeLayout aController = (RelativeLayout) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_LAY_controller);

        aPlayer = new MediaPlayer();
        final VideoView aVideoView = (VideoView) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_VDVW);
        aVideoView.setVideoPath("https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4");

        aBackwardIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int aPosition = aPlayer.getCurrentPosition() - 5000;
                Log.e("aBackwardIMG", "" + aPosition);
                aVideoView.seekTo(aPosition);

            }
        });
        aPlayBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aPlayBTN.setVisibility(View.GONE);
                aProgressBar.setVisibility(View.VISIBLE);
                aVideoView.start();
                if (aVideoView.isPlaying()) {
                    aProgressBar.setVisibility(View.GONE);
                    aController.setVisibility(View.VISIBLE);
                }
            }
        });

        aPauseIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (aVideoView.isPlaying()) {
                    aVideoView.pause();
                    aPauseIMG.setImageResource(R.drawable.icon_play);
                } else {
                    aVideoView.start();
                    aPauseIMG.setImageResource(R.drawable.icon_pause);
                }
            }
        });

        aForwardIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int aPosition = aPlayer.getCurrentPosition() + 5000;
                Log.e("aForwardIMG", "" + aPosition);
                aVideoView.seekTo(aPosition);
            }
        });

        aVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                aPlayer = mediaPlayer;
                aProgressBar.setVisibility(View.GONE);
                aPlayBTN.setVisibility(View.GONE);
                aController.setVisibility(View.VISIBLE);
            }
        });

        aVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                aController.setVisibility(View.GONE);
                aPlayBTN.setVisibility(View.VISIBLE);
            }
        });*/

        // set the uri for the video view

        final String Base_Url = BuildConfig.SERVER_URL + "user_login/";
        aSubmitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Base_Url));
                startActivity(browserIntent);
            }
        });
        aCloseLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aPopDialog.dismiss();
            }
        });
        aWebview.loadData(aDescriptionStr, "text/html", "UTF-8");
        aPopDialog.show();
    }

}


