package com.handypro.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.WalletTransactionPojo;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.adapters.WalletTransactionAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CAS63 on 3/5/2018.
 */

public class WalletTransactionFragment extends Fragment {
    private CustomTextView myAllTXT, myDebitTXT, myCreditTXT;
    private LinearLayout myAllLAY, myDebitLAY, myCreditLAY, myNoInternetLAY, myEmptyTransLAY;
    private RelativeLayout myMainLAY;
    private ConnectionDetector myConnectionManager;
    private CustomTextView myRetryConnTXT;
    private MaterialRefreshLayout myRefreshLAY;
    private ListView myWalletTransListVw;
    private View myAllView, myCreditView, myDebitView;
    private ArrayList<WalletTransactionPojo> myWalletAllArrList;
    private ArrayList<WalletTransactionPojo> myWalletCreditArrList;
    private ArrayList<WalletTransactionPojo> myWalletDebitArrList;
    private ProgressDialogcreated myDialog;
    private SharedPreference mySession;
    private WalletTransactionAdapter myAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_wallet_transaction, container, false);
        InitClassAndWidgets(rootview);
        WidgetsonClick();
        getData();
        return rootview;

    }

    private void getData() {
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestWalletMoney(ServiceConstant.plumbal_money_transaction_url);
        } else {
            checkInternetConnectivity();
        }
    }

    private void WidgetsonClick() {
        if (myWalletAllArrList.size() > 0) {
            myEmptyTransLAY.setVisibility(View.GONE);
        } else {
            myEmptyTransLAY.setVisibility(View.VISIBLE);
        }

        myAllLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAllView.setVisibility(View.VISIBLE);
                myCreditView.setVisibility(View.INVISIBLE);
                myDebitView.setVisibility(View.INVISIBLE);
                myAllTXT.setTextColor(0xFFF85603);
                myDebitTXT.setTextColor(0xFF818181);
                myCreditTXT.setTextColor(0xFF818181);

                myAdapter = new WalletTransactionAdapter(getActivity(), myWalletAllArrList);
                myWalletTransListVw.setAdapter(myAdapter);
                myAdapter.notifyDataSetChanged();
                if (myWalletAllArrList.size() > 0) {
                    myEmptyTransLAY.setVisibility(View.GONE);
                } else {
                    myEmptyTransLAY.setVisibility(View.VISIBLE);
                }

            }
        });

        myCreditLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCreditView.setVisibility(View.VISIBLE);
                myAllView.setVisibility(View.INVISIBLE);
                myDebitView.setVisibility(View.INVISIBLE);
                myAllTXT.setTextColor(0xFF818181);
                myDebitTXT.setTextColor(0xFF818181);
                myCreditTXT.setTextColor(0xFFF85603);

                myAdapter = new WalletTransactionAdapter(getActivity(), myWalletCreditArrList);
                myWalletTransListVw.setAdapter(myAdapter);
                myAdapter.notifyDataSetChanged();
                if (myWalletCreditArrList.size() > 0) {
                    myEmptyTransLAY.setVisibility(View.GONE);
                } else {
                    myEmptyTransLAY.setVisibility(View.VISIBLE);
                }
            }
        });

        myDebitLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAllView.setVisibility(View.INVISIBLE);
                myCreditView.setVisibility(View.INVISIBLE);
                myDebitView.setVisibility(View.VISIBLE);
                myAllTXT.setTextColor(0xFF818181);
                myDebitTXT.setTextColor(0xFFF85603);
                myCreditTXT.setTextColor(0xFF818181);

                myAdapter = new WalletTransactionAdapter(getActivity(), myWalletDebitArrList);
                myWalletTransListVw.setAdapter(myAdapter);
                myAdapter.notifyDataSetChanged();
                if (myWalletDebitArrList.size() > 0) {
                    myEmptyTransLAY.setVisibility(View.GONE);
                } else {
                    myEmptyTransLAY.setVisibility(View.VISIBLE);
                }
            }
        });

        myRetryConnTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });

        myRefreshLAY.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getData();
                        myRefreshLAY.finishRefresh();
                    }
                }, 750);
            }
        });
    }

    private void postRequestWalletMoney(String url) {
        myDialog = new ProgressDialogcreated(getActivity());
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("type", "all");


        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------post wallet money Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        JSONObject response_Object = object.getJSONObject("response");

                    } else {
                        HNDHelper.showResponseErrorAlert(getActivity(), object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void InitClassAndWidgets(View aView) {
        myWalletAllArrList = new ArrayList<WalletTransactionPojo>();
        myWalletCreditArrList = new ArrayList<WalletTransactionPojo>();
        myWalletDebitArrList = new ArrayList<WalletTransactionPojo>();
        myConnectionManager = new ConnectionDetector(getActivity());
        mySession = new SharedPreference(getActivity());
        myAllLAY = (LinearLayout) aView.findViewById(R.id.fragment_wallet_transaction_all_layout);
        myCreditLAY = (LinearLayout) aView.findViewById(R.id.fragment_wallet_transaction_credits_layout);
        myDebitLAY = (LinearLayout) aView.findViewById(R.id.fragment_wallet_transaction_debit_layout);
        myAllTXT = (CustomTextView) aView.findViewById(R.id.fragment_wallet_transaction_all_textview);
        myDebitTXT = (CustomTextView) aView.findViewById(R.id.fragment_wallet_transaction_debits_textview);
        myCreditTXT = (CustomTextView) aView.findViewById(R.id.fragment_wallet_transaction_credits_textview);
        myWalletTransListVw = (ListView) aView.findViewById(R.id.fragment_wallet_transaction_listview);
        myNoInternetLAY = (LinearLayout) aView.findViewById(R.id.fragment_wallet_transaction_no_internetLAY);
        myMainLAY = (RelativeLayout) aView.findViewById(R.id.fragment_wallet_transaction_transactions_tab_layout);
        myRetryConnTXT = (CustomTextView) aView.findViewById(R.id.fragment_wallet_transaction_retry_TXT);
        myCreditView = (View) aView.findViewById(R.id.fragment_wallet_transaction_list_credit_view);
        myDebitView = (View) aView.findViewById(R.id.fragment_wallet_transaction_list_debit_view);
        myAllView = (View) aView.findViewById(R.id.fragment_wallet_transaction_list_all_view);
        myEmptyTransLAY = (LinearLayout) aView.findViewById(R.id.fragment_wallet_transaction_emptyTrans_LAY);
        myRefreshLAY = (MaterialRefreshLayout) aView.findViewById(R.id.fragment_wallet_transaction_Swipe_layout);

        checkInternetConnectivity();

    }

    private void checkInternetConnectivity() {
        if (myConnectionManager.isConnectingToInternet()) {
            myNoInternetLAY.setVisibility(View.GONE);
            myMainLAY.setVisibility(View.VISIBLE);
        } else {
            myNoInternetLAY.setVisibility(View.VISIBLE);
            myMainLAY.setVisibility(View.GONE);
        }
    }

}
