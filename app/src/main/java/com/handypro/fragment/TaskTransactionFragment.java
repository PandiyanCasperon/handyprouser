package com.handypro.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.TransactionInfoPojo;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.newmoreinfo;
import com.handypro.adapters.TransactionListAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CAS63 on 3/5/2018.
 */

public class TaskTransactionFragment extends Fragment {
    private ListView myTaskListView;
    private LinearLayout myRefreshLAY;
    private LinearLayout myEmptyTransLAY, myMainLAY, myNoInternetLAY;
    private ArrayList<TransactionInfoPojo> myTransArrList;
    private TransactionListAdapter myTransAdapter;
    private ConnectionDetector myConnectionManager;
    private CustomTextView myRetryConnTXT;
    private ProgressDialogcreated myDialog;
    private SharedPreference mySession;
    TextView paymentsuccess;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_task_transaction, container, false);
        InitClassAndWidgets(rootview);
      //  loadData();
        widgetOnClick();
        gettransactionData();
        return rootview;
    }

    private void gettransactionData() {
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestTransactionList(ServiceConstant.TRANSACTION_URL);
        } else {
            checkInternetConnectivity();
        }
    }

    private void postRequestTransactionList(String url) {
        myDialog = new ProgressDialogcreated(getActivity());
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Transaction list Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {


                        JSONObject response_Object = object.getJSONObject("response");
                        JSONArray aJobsArray = response_Object.getJSONArray("jobs");
                        if (aJobsArray.length() > 0) {
                            for (int i = 0; i < aJobsArray.length(); i++) {
                                JSONObject aJsonObject = aJobsArray.getJSONObject(i);
                                TransactionInfoPojo aInfoPojo2 = new TransactionInfoPojo();


                                String service="";
                                JSONArray service_type = aJsonObject.getJSONArray("category_name");
                                for (int b = 0; b < service_type.length(); b++)
                                {
                                    String value=""+service_type.getString(b);
                                    service+=value+",";
                                }

                                if(service.endsWith(","))
                                {
                                    service=service.substring(0,service.length()-1);
                                }



                                aInfoPojo2.setTransactionCategoryName(service);
                                aInfoPojo2.setTransactionDate(aJsonObject.getString("job_date"));
                                aInfoPojo2.setTransactionJobId(aJsonObject.getString("job_id"));

                                aInfoPojo2.setTransactionTotalAmount(mySession.getCurrencySymbol() +aJsonObject.getString("total_amount"));
                                aInfoPojo2.setTransactionTime(aJsonObject.getString("job_time"));


                                myTransArrList.add(aInfoPojo2);
                            }




                        }


                        if(myTransArrList.size()>0)
                        {
                            paymentsuccess.setVisibility(View.GONE);
                        }
                        else
                        {
                            paymentsuccess.setVisibility(View.VISIBLE);
                        }




                    } else {
                        paymentsuccess.setVisibility(View.GONE);
                        HNDHelper.showResponseErrorAlert(getActivity(), object.getString("response"));
                    }

                    if(myTransArrList.size()>0)
                    {
                        myTransAdapter = new TransactionListAdapter(getActivity(), myTransArrList);
                        myTaskListView.setAdapter(myTransAdapter);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void widgetOnClick() {
        myRetryConnTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gettransactionData();
            }
        });

        myTaskListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*Intent aViewDetailIntent = new Intent(getActivity(), ViewTransDetailActivity.class);
                aViewDetailIntent.putExtra("BookingId", myTransArrList.get(position).getTransactionJobId());
                startActivity(aViewDetailIntent);*/

                SharedPreferences pref = getActivity().getSharedPreferences("sendjobid", 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("jobid", myTransArrList.get(position).getTransactionJobId());
                editor.apply();
                editor.commit();
                Intent InfoIntent = new Intent(getActivity(),newmoreinfo.class);
                startActivity(InfoIntent);

            }
        });


    }



    private void InitClassAndWidgets(View aView) {
        myTransArrList = new ArrayList<TransactionInfoPojo>();
        myConnectionManager = new ConnectionDetector(getActivity());
        mySession = new SharedPreference(getActivity());
        myTaskListView = (ListView) aView.findViewById(R.id.fragment_task_transaction_listView);
        myRefreshLAY = (LinearLayout) aView.findViewById(R.id.fragment_task_transaction_Swipe_layout);
        myMainLAY = (LinearLayout) aView.findViewById(R.id.fragment_task_transaction_mainLAY);
        myEmptyTransLAY = (LinearLayout) aView.findViewById(R.id.fragment_task_transaction_emptyTrans_LAY);
        myNoInternetLAY = (LinearLayout) aView.findViewById(R.id.fragment_task_transaction_no_internetLAY);
        myRetryConnTXT = (CustomTextView) aView.findViewById(R.id.fragment_task_transaction_retry_TXT);
        paymentsuccess = (TextView) aView.findViewById(R.id.paymentsuccess);
        checkInternetConnectivity();

        if (myTransArrList.size() == 0) {
            myEmptyTransLAY.setVisibility(View.VISIBLE);
        } else {
            myEmptyTransLAY.setVisibility(View.GONE);
        }
    }

    private void checkInternetConnectivity() {
        if (myConnectionManager.isConnectingToInternet()) {
            myNoInternetLAY.setVisibility(View.GONE);
            myMainLAY.setVisibility(View.VISIBLE);
        } else {
            myNoInternetLAY.setVisibility(View.VISIBLE);
            myMainLAY.setVisibility(View.GONE);
        }
    }
}
