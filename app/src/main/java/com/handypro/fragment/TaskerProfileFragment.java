package com.handypro.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.CraftsmanReviewRatedUsersItem;
import com.handypro.Pojo.TaskerCategoryPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.adapters.CraftsmanReviewAdapter;
import com.handypro.adapters.TaskerCategoryAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by CAS63 on 3/15/2018.
 */

public class TaskerProfileFragment extends Fragment {
    private CustomTextView myTaskerEmailTXT, myTaskerPhnoTXT, myRadiusTXT, myHourlyCostTXT, myMiniCostTXT, myAddressTXT, myWrkLocationTXT;
    private TextView myTaskerUnameTXT;
    private CircleImageView myTaskerProIMG;
    TextView aboutprofile, aboutexp;
    private RecyclerView myCategoryRecyclerView, RecyclerViewReview;
    private LinearLayout myCallBTN;
    private ArrayList<TaskerCategoryPojo> myTskrCatList;
    private TaskerCategoryAdapter myTskrCatgryAdapter;
    private SharedPreference mySession;
    private ConnectionDetector myConnectionManager;
    private ProgressDialogcreated myDialog;
    final int PERMISSION_REQUEST_CODES = 222;
    final int PERMISSION_REQUEST_CODE = 111;
    RatingBar layout_inflate_craft_men_review_ratingBar;
    public static final int RequestPermissionCode = 7;
    int rating;
    private ArrayList<CraftsmanReviewRatedUsersItem> craftsmanReviewRatedUsersItems;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_tasker_profile, container, false);
        InitClassAndWidgets(rootview);

        mySession = new SharedPreference(getActivity());
        myConnectionManager = new ConnectionDetector(getActivity());
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestMyOrderList();

        } else {
            HNDHelper.showErrorAlert(getActivity(), getResources().getString(R.string.nointernet_text));
        }


        return rootview;
    }


    private void InitClassAndWidgets(View aView) {

        myTskrCatList = new ArrayList<TaskerCategoryPojo>();


        aboutprofile = aView.findViewById(R.id.aboutprofile);
        aboutexp = aView.findViewById(R.id.aboutexp);

        layout_inflate_craft_men_review_ratingBar = aView.findViewById(R.id.layout_inflate_craft_men_review_ratingBar);
        myTaskerUnameTXT = aView.findViewById(R.id.fragment_tasker_profile_username_TXT);
        myTaskerEmailTXT = aView.findViewById(R.id.fragment_tasker_profile_email_TXT);
        myTaskerPhnoTXT = aView.findViewById(R.id.fragment_tasker_profile_phone_number_TXT);
        myRadiusTXT = aView.findViewById(R.id.fragment_tasker_profile_radius_TXT);
        myHourlyCostTXT = aView.findViewById(R.id.fragment_tasker_profile_hourly_cost_TXT);
        myMiniCostTXT = aView.findViewById(R.id.fragment_tasker_profile_min_cost_TXT);
        myAddressTXT = aView.findViewById(R.id.fragment_tasker_profile_address_TXT);
        myWrkLocationTXT = aView.findViewById(R.id.fragment_tasker_profile_work_location_TXT);
        myTaskerProIMG = aView.findViewById(R.id.fragment_tasker_profile_profile_imageView);

        myCallBTN = aView.findViewById(R.id.fragment_tasker_profile_BTN_call);

        myCallBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (myTaskerPhnoTXT.getText().toString() != null || myTaskerPhnoTXT.getText().toString().length() > 2) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + myTaskerPhnoTXT.getText().toString()));
                    startActivity(callIntent);
                } else {
                    HNDHelper.showErrorAlert(getActivity(), getResources().getString(R.string.activity_my_orders_error_in_contact_provider));
                }
            }
        });
        myCategoryRecyclerView = aView.findViewById(R.id.fragment_tasker_profile_category_recyclerview);
        RecyclerViewReview = aView.findViewById(R.id.RecyclerViewReview);

    }


    private void setDataInAdapter() {

        myTskrCatgryAdapter = new TaskerCategoryAdapter(getActivity(), myTskrCatList);
        myCategoryRecyclerView.setAdapter(myTskrCatgryAdapter);

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        myCategoryRecyclerView.setLayoutManager(horizontalLayoutManagaer);
        RecyclerViewReview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

    }


    private void postRequestMyOrderList() {
        myDialog = new ProgressDialogcreated(getActivity());
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<>();
        SharedPreferences pref = getActivity().getSharedPreferences("sendtaskerid", 0);
        String provider_id = pref.getString("taskerid", "");


        jsonParams.put("provider_id", provider_id);
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(ServiceConstant.PROFILEINFO_URL, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------full profile----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        JSONObject response_Object = object.getJSONObject("response");


                        String provider_name = response_Object.getString("provider_name");
                        String email = response_Object.getString("email");
                        String mobile_number = response_Object.getString("mobile_number");
                        String image = response_Object.getString("image");
                        if (image.contains("http:") || image.contains("https:")) {

                        } else {
                            image = ServiceConstant.Base_Url + image;
                        }

                        String Working_location = response_Object.getString("Working_location");

                        if (response_Object.has("avg_review")) {
                            String avg_review = response_Object.getString("avg_review");
                            layout_inflate_craft_men_review_ratingBar.setRating(Float.parseFloat(avg_review));
                            layout_inflate_craft_men_review_ratingBar.setVisibility(View.VISIBLE);

                        } else {
                            layout_inflate_craft_men_review_ratingBar.setVisibility(View.GONE);
                        }


                        String address = response_Object.getString("address");
                        String dial_code = response_Object.getString("dial_code");

                        if (response_Object.has("bio")) {
                            String bio = response_Object.getString("bio");
                            if (bio.equalsIgnoreCase("") || bio.equalsIgnoreCase(" ") || bio.equalsIgnoreCase(null) || bio.equalsIgnoreCase("null")) {

                            } else {
                                aboutprofile.setText(bio);
                            }


                        }


                        if (response_Object.has("experience")) {
                            String bio = response_Object.getString("experience");
                            if (bio.equalsIgnoreCase("") || bio.equalsIgnoreCase(" ") || bio.equalsIgnoreCase(null) || bio.equalsIgnoreCase("null")) {

                            } else {
                                aboutexp.setText(bio);
                            }


                        }


                        myTaskerUnameTXT.setText(provider_name);
                        myTaskerEmailTXT.setText(email);

                        myTaskerPhnoTXT.setText(dial_code + mobile_number);

                        myAddressTXT.setText(address);
                        myWrkLocationTXT.setText(Working_location);


                        Picasso.get().load(image)
                                .resize(500, 500)
                                .onlyScaleDown()
                                .placeholder(R.drawable.ic_no_user)
                                .into(myTaskerProIMG);


                        JSONArray aMainArr = response_Object.getJSONArray("category_Details");
                        myTskrCatList.clear();
                        for (int a = 0; a < aMainArr.length(); a++) {

                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);

                            TaskerCategoryPojo aTskerCatPojo = new TaskerCategoryPojo();
                            aTskerCatPojo.setCategoryHourlyRate("");
                            aTskerCatPojo.setCategoryName(aSubCatObj.getString("categoryname"));
                            aTskerCatPojo.setCategoryImage(aSubCatObj.getString("image"));
                            myTskrCatList.add(aTskerCatPojo);


                        }
                        if (aMainArr.length() > 0) {
                            setDataInAdapter();
                        }


                    } else {
                        HNDHelper.showResponseErrorAlert(getActivity(), object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                postRequestMyReviewsList();
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    private void postRequestMyReviewsList() {
        myDialog = new ProgressDialogcreated(getActivity());
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        SharedPreferences pref = getActivity().getSharedPreferences("sendtaskerid", 0);
        String provider_id = pref.getString("taskerid", "");
        jsonParams.put("provider_id", provider_id);


        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(ServiceConstant.GET_CRAFTSMAN_RATING, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println(ServiceConstant.GET_CRAFTSMAN_RATING + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        JSONObject response_Object = object.getJSONObject("response");

                        JSONArray RatedJsonArray = response_Object.getJSONArray("rated_users");
                        craftsmanReviewRatedUsersItems = new ArrayList<>();
                        if (RatedJsonArray.length() > 0) {
                            for (int i = 0; i < RatedJsonArray.length(); i++) {
                                JSONObject CraftsmanReviewJsonObject = RatedJsonArray.getJSONObject(i);
                                craftsmanReviewRatedUsersItems.add(new CraftsmanReviewRatedUsersItem("",
                                        CraftsmanReviewJsonObject.getString("comments"),
                                        CraftsmanReviewJsonObject.getString("user_image"),
                                        CraftsmanReviewJsonObject.getString("rating_time"),
                                        "",
                                        CraftsmanReviewJsonObject.getString("user_name"),
                                        CraftsmanReviewJsonObject.getString("ratings"),
                                        ""));


                            }
                            RecyclerViewReview.setAdapter(new CraftsmanReviewAdapter(getActivity(), craftsmanReviewRatedUsersItems));
                        }
                    } else {
                        HNDHelper.showResponseErrorAlert(getActivity(), object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }


    public boolean CheckingPermissionIsEnabledOrNot() {


        int call = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);

        return call == PackageManager.PERMISSION_GRANTED;
    }

    private void RequestMultiplePermission() {

        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(getActivity(), new String[]
                {

                        Manifest.permission.CALL_PHONE
                }, RequestPermissionCode);

    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == RequestPermissionCode) {
            if (grantResults.length > 0) {

                boolean call = grantResults[0] == PackageManager.PERMISSION_GRANTED;


                if (call) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + myTaskerPhnoTXT.getText().toString()));
                    startActivity(callIntent);
                } else {


                }
            }
        }
    }


}
