package com.handypro.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.ReviewInfoPojo;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.adapters.ReviewsAdapter;
import com.handypro.adapters.TaskerReviewAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CAS63 on 3/15/2018.
 */

public class TaskerReviewFragment extends Fragment {

    private ListView myReviewsListVw;
    private ArrayList<ReviewInfoPojo> myTaskerReviewArrList;
    private TaskerReviewAdapter myTskrRevAdapter;
    private ConnectionDetector myConnectionManager;
    ProgressDialogcreated myDialog;
    LinearLayout activity_reviews_no_revLAY;
    private SharedPreference mySession;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_tasker_review, container, false);
        InitClassAndWidgets(rootview);

        mySession = new SharedPreference(getActivity());

        myConnectionManager = new ConnectionDetector(getActivity());
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestMyOrderList(ServiceConstant.REVIEW_URL);
        } else {
            HNDHelper.showErrorAlert(getActivity(), getResources().getString(R.string.nointernet_text));
        }



        return rootview;
    }



    private void InitClassAndWidgets(View aRootView) {
        myTaskerReviewArrList = new ArrayList<ReviewInfoPojo>();
        activity_reviews_no_revLAY= (LinearLayout) aRootView.findViewById(R.id.activity_reviews_no_revLAY);
        activity_reviews_no_revLAY= (LinearLayout) aRootView.findViewById(R.id.activity_reviews_no_revLAY);
        myReviewsListVw = (ListView) aRootView.findViewById(R.id.activity_reviews_listView);
    }


    private void postRequestMyOrderList( String url) {
        myDialog = new ProgressDialogcreated(getActivity());
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<>();


        jsonParams.put("role", "tasker");
        SharedPreferences pref = getActivity().getSharedPreferences("sendtaskerid", 0);
        String provider_id=pref.getString("taskerid","");
        jsonParams.put("user_id", provider_id);

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------full profile----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    JSONObject aDataObj = object.getJSONObject("data");


                    if (aDataObj.getString("status").equalsIgnoreCase("1")) {
                        JSONObject response_Object = aDataObj.getJSONObject("response");





                        if (response_Object.length() > 0) {
                            JSONArray aJobsArray = response_Object.getJSONArray("reviews");
                            if (aJobsArray.length() > 0) {
                                myTaskerReviewArrList.clear();
                                for (int i = 0; i < aJobsArray.length(); i++) {
                                    JSONObject aJsonObject = aJobsArray.getJSONObject(i);


                                    ReviewInfoPojo aRevInfo = new ReviewInfoPojo();
                                    aRevInfo.setReviewBookingId(aJsonObject.getString("booking_id"));

                                    String service="";
                                    JSONArray service_type = aJsonObject.getJSONArray("category");
                                    for (int b = 0; b < service_type.length(); b++)
                                    {

                                        String value=""+service_type.getString(b);


                                        service+=value+",";
                                    }

                                    if(service.endsWith(","))
                                    {
                                        service=service.substring(0,service.length()-1);
                                    }


                                    aRevInfo.setReviewCategory(service);
                                    aRevInfo.setReviewComments(aJsonObject.getString("comments"));
                                    aRevInfo.setReviewDate(aJsonObject.getString("date"));
                                    aRevInfo.setReviewRating(aJsonObject.getString("rating"));
                                    aRevInfo.setReviewTasker(aJsonObject.getString("user_name"));
                                    aRevInfo.setReviewImage(aJsonObject.getString("user_image"));

                                    myTaskerReviewArrList.add(aRevInfo);
                                }

                                ReviewsAdapter myReviewsAdapter = new ReviewsAdapter(getActivity(), myTaskerReviewArrList);
                                myReviewsListVw.setAdapter(myReviewsAdapter);
                                myReviewsListVw.setVisibility(View.VISIBLE);
                                activity_reviews_no_revLAY.setVisibility(View.GONE);
                            }
                        }




                    } else {

                        myReviewsListVw.setVisibility(View.GONE);
                        activity_reviews_no_revLAY.setVisibility(View.VISIBLE);

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }
}
