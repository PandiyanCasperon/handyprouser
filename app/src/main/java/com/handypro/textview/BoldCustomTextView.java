package com.handypro.textview;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by CAS61 on 11/1/2017.
 */
public class BoldCustomTextView extends AppCompatTextView {

    public BoldCustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public BoldCustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BoldCustomTextView(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Poppins-Bold.ttf");
        setTypeface(tf);
    }
}
