package com.handypro.textview;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatButton;
import android.util.AttributeSet;

/**
 * Created by CAS61 on 11/7/2017.
 */
public class CustomButton extends AppCompatButton {


    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        // TODO Auto-generated constructor stub
    }

    public CustomButton(Context context) {
        super(context);
        init();
        // TODO Auto-generated constructor stub
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        // TODO Auto-generated constructor stub
    }

    private void init() {
        Typeface font_type = Typeface.createFromAsset(getContext().getAssets(), "fonts/Poppins-Bold.ttf");
        setTypeface(font_type);
    }
}
