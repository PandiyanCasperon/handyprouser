package com.handypro.commonvalues;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.handypro.Dialog.PkDialog;
import com.handypro.R;

/**
 * Created by CAS61 on 2/8/2018.
 */

public class HNDHelper {

    /**
     * Display the alert dialog
     *
     * @param aContext
     * @param aErrorMsg
     */
    public static void showErrorAlert(Context aContext, String aErrorMsg) {
        try {

            final PkDialog mDialog = new PkDialog(aContext);
            mDialog.setDialogTitle(aContext.getResources().getString(R.string.success_label));
            mDialog.setDialogMessage(aErrorMsg);
            mDialog.setPositiveButton(
                    aContext.getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );

            mDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Show the response error alert
     *
     * @param aContext
     * @param aErrorMsg
     */
    public static void showResponseErrorAlert(Context aContext, String aErrorMsg) {
        try {

            final PkDialog mDialog = new PkDialog(aContext);
            mDialog.setDialogTitle(aContext.getResources().getString(R.string.dialog_sorry));
            mDialog.setDialogMessage(aErrorMsg);
            mDialog.setPositiveButton("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );

            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void showResponseTitle(String title,Context aContext, String aErrorMsg) {
        try {
            final PkDialog mDialog = new PkDialog(aContext);
            mDialog.setDialogTitle(title);
            mDialog.setDialogMessage(aErrorMsg);
            mDialog.setPositiveButton(aContext.getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );

            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showWhatIncludePopup(String title,Context aContext, String aErrorMsg) {
        try {
            /*final PkDialog mDialog = new PkDialog(aContext);
            mDialog.setDialogTitle(title);
            mDialog.setDialogMessageInLeft(aErrorMsg);
            mDialog.setPositiveButton(aContext.getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );

            mDialog.show();*/

            //--------Adjusting Dialog width-----
            /*DisplayMetrics metrics = aContext.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 0.95);//fill only 95% of the screen*/
            TextView alert_message;
            Button Bt_action;
            View view = View.inflate(aContext, R.layout.custom_dialog_library_what_include, null);
            final AlertDialog.Builder dialog = new AlertDialog.Builder(aContext);
            /*dialog.getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);*/

            alert_message = view.findViewById(R.id.custom_dialog_library_message_textview);
            Bt_action = view.findViewById(R.id.custom_dialog_library_ok_button);

            alert_message.setText(aErrorMsg);
            dialog.setView(view);
            final AlertDialog alertDialog = dialog.show();
            Bt_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Adjust listview height and scrolling
     *
     * @param listView
     *
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    /**
     * Adjust listview height and scrolling
     *
     * @param gridView
     * @param columns
     *
     */
    public static void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if( items > columns ){
            x = items/columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);

    }
}
