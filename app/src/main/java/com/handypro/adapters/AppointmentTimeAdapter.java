package com.handypro.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.handypro.Pojo.AppointmentTimePojo;
import com.handypro.R;

import java.util.ArrayList;

/**
 * Created by CAS63 on 2/21/2018.
 */

public class AppointmentTimeAdapter extends RecyclerView.Adapter<AppointmentTimeAdapter.ViewHolder> {
    private ArrayList<AppointmentTimePojo> myAppointmentTimeArrList;
    private Context myContext;

    public AppointmentTimeAdapter(Context myContext, ArrayList<AppointmentTimePojo> aAppointmentTimeArr) {
        this.myContext = myContext;
        this.myAppointmentTimeArrList = aAppointmentTimeArr;
    }

    @NonNull
    @Override
    public AppointmentTimeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(myContext).inflate(R.layout.newappointment, parent, false);
        return new AppointmentTimeAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.myAppointmntTimeTXT.setText(myAppointmentTimeArrList.get(position).getAppointmentTime());


        if (myAppointmentTimeArrList.get(position).getColourchange().equals("0")) {
            holder.myAppointmntTimeTXT.setTextColor(Color.parseColor("#e8e3e3"));
        } else {

            if (myAppointmentTimeArrList.get(position).getSelected().equals("true") && myAppointmentTimeArrList.get(position).getEnabled().equals("true")) {
                holder.myAppointmntTimeTXT.setTextColor(Color.parseColor("#ff0000"));
            } else if (myAppointmentTimeArrList.get(position).getSelected().equals("false") && myAppointmentTimeArrList.get(position).getEnabled().equals("false")) {
                holder.myAppointmntTimeTXT.setTextColor(Color.parseColor("#FFDCDBDB"));
            } else {
                holder.myAppointmntTimeTXT.setTextColor(Color.parseColor("#285104"));
            }

        }


    }

    @Override
    public int getItemCount() {
        return myAppointmentTimeArrList.size();
    }

    public AppointmentTimePojo getItem(int position) {
        return myAppointmentTimeArrList.get(position);
    }

    public ArrayList<AppointmentTimePojo> getAllTimeArr() {
        return myAppointmentTimeArrList;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView myAppointmntTimeTXT;

        private ViewHolder(View itemView) {
            super(itemView);
            myAppointmntTimeTXT = (TextView) itemView.findViewById(R.id.layout_inflater_appointment_time_list_item_TXT);

        }
    }

}
