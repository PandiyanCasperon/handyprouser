package com.handypro.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.handypro.Pojo.CraftsmanReviewRatedUsersItem;
import com.handypro.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CraftsmanReviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<CraftsmanReviewRatedUsersItem> CraftsmanReviewRatedUsersItem;

    public CraftsmanReviewAdapter(Context context, ArrayList<CraftsmanReviewRatedUsersItem> CraftsmanReviewRatedUsersItem) {
        this.context = context;
        this.CraftsmanReviewRatedUsersItem = CraftsmanReviewRatedUsersItem;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView TextViewUserName, TextViewReviewDate, TextViewReviewText;
        ImageView ImageViewUserImage;
        RatingBar RatingBarUserRating;

        private ViewHolder(View itemView) {
            super(itemView);
            TextViewUserName = itemView.findViewById(R.id.TextViewUserName);
            TextViewReviewDate = itemView.findViewById(R.id.TextViewReviewDate);
            TextViewReviewText = itemView.findViewById(R.id.TextViewReviewText);
            ImageViewUserImage = itemView.findViewById(R.id.ImageViewUserImage);
            RatingBarUserRating = itemView.findViewById(R.id.RatingBarUserRating);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(context).inflate(
                R.layout.recycler_review_view_row, viewGroup, false);
        viewHolder = new ViewHolder(menuItemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        CraftsmanReviewRatedUsersItem craftsmanReviewRatedUsersItem = CraftsmanReviewRatedUsersItem.get(holder.getAdapterPosition());
        viewHolder.TextViewUserName.setText(craftsmanReviewRatedUsersItem.getUserName());
        viewHolder.TextViewReviewDate.setText(craftsmanReviewRatedUsersItem.getRatingTime());
        viewHolder.TextViewReviewText.setText(craftsmanReviewRatedUsersItem.getComments());
        viewHolder.RatingBarUserRating.setRating(Float.parseFloat(craftsmanReviewRatedUsersItem.getRatings()));
        Picasso.get().load(craftsmanReviewRatedUsersItem.getUserImage())
                .resize(500, 500)
                .onlyScaleDown()
                .placeholder(R.drawable.ic_no_user)
                .into(viewHolder.ImageViewUserImage);
    }

    @Override
    public int getItemCount() {
        return CraftsmanReviewRatedUsersItem.size();
    }
}