package com.handypro.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.handypro.Pojo.WalletTransactionPojo;
import com.handypro.R;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/12/2018.
 */

public class WalletTransactionAdapter extends BaseAdapter {

    private ArrayList<WalletTransactionPojo> myTransArrList;
    private LayoutInflater myInflater;
    private Context myContext;

    public WalletTransactionAdapter(Context aContext, ArrayList<WalletTransactionPojo> myDataList) {
        myContext = aContext;
        myInflater = LayoutInflater.from(myContext);
        myTransArrList = myDataList;
    }

    @Override
    public int getCount() {
        return myTransArrList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView myTransTypeIMG;
        private TextView myTransTypeName;
        private TextView myTransPrice, myTitle, myDate, myTransBalance;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = myInflater.inflate(R.layout.layout_inflater_wallet_transaction_list_item, parent, false);
            holder = new ViewHolder();
            holder.myTransTypeName = (TextView) view.findViewById(R.id.layout_inflater_wallet_transaction_list_item_name_TXT);
            holder.myTransTypeIMG = (ImageView) view.findViewById(R.id.layout_inflater_wallet_transaction_list_item_type_IMG);
            holder.myTransPrice = (TextView) view.findViewById(R.id.layout_inflater_wallet_transaction_list_item_price_TXT);
            holder.myTitle = (TextView) view.findViewById(R.id.layout_inflater_wallet_transaction_list_item_title_TXT);
            holder.myDate = (TextView) view.findViewById(R.id.layout_inflater_wallet_transaction_list_item_date_TXT);
            holder.myTransBalance = (TextView) view.findViewById(R.id.layout_inflater_wallet_transaction_list_item_balance_TXT);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }


        if (myTransArrList.get(position).getTransactionType().equalsIgnoreCase("CREDIT")) {
            holder.myTransTypeName.setText(myContext.getResources().getString(R.string.layout_inflater_wallet_transaction_list_item_credit));
            holder.myTransTypeName.setTextColor(0xFF006301);
            holder.myTransTypeIMG.setImageResource(R.drawable.icon_triangle_arrow_right);
        } else {
            holder.myTransTypeName.setText(myContext.getResources().getString(R.string.layout_inflater_wallet_transaction_list_item_debit));
            holder.myTransTypeName.setTextColor(0xFFcc0000);
            holder.myTransTypeIMG.setImageResource(R.drawable.icon_triangle_arrow_right);
        }


        holder.myTransPrice.setText(myTransArrList.get(position).getCurrencySymbol() + myTransArrList.get(position).getTransactionAmount());
        holder.myTitle.setText(myTransArrList.get(position).getTransactionTitle());
        holder.myDate.setText(myTransArrList.get(position).getTransactionDate());
        holder.myTransBalance.setText(myContext.getResources().getString(R.string.layout_inflater_wallet_transaction_list_item_balance) + " " + myTransArrList.get(position).getCurrencySymbol() + myTransArrList.get(position).getBalanceAmount());


        return view;
    }
}

