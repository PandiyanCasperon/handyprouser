package com.handypro.adapters;



/**
 * Created by user127 on 02-04-2018.
 */

import android.graphics.Paint;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.handypro.Pojo.PaymentFareSummeryPojo;
import com.handypro.R;
import com.handypro.commonvalues.HNDHelper;

import java.util.ArrayList;



/**
 * Created by user88 on 12/30/2015.
 */
public class whatsadapter extends BaseAdapter {

    private ArrayList<PaymentFareSummeryPojo> data;
    ArrayList<String> categorynameinarrayo,whatsincludedinarrayo ;
    private LayoutInflater mInflater;
    private AppCompatActivity context;
    private String check;

    public whatsadapter(AppCompatActivity c, ArrayList<String> categorynameinarray, ArrayList<String> whatsincludedinarray) {
        context = c;
        mInflater = LayoutInflater.from(context);
        categorynameinarrayo = categorynameinarray;
        whatsincludedinarrayo= whatsincludedinarray;
    }

    @Override
    public int getCount() {
        return categorynameinarrayo.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {
        private TextView Tv_payment_title,Tv_payment_amount;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;

        if (convertView==null){
            view = mInflater.inflate(R.layout.whatsad, parent, false);
            holder = new ViewHolder();


            holder.Tv_payment_title = (TextView)view.findViewById(R.id.payment_fare_title_textView);
            holder.Tv_payment_amount = (TextView)view.findViewById(R.id.payment_fare_cost_textView);

            view.setTag(holder);

        }else{
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.Tv_payment_title.setText(categorynameinarrayo.get(position));
        if(whatsincludedinarrayo.get(position).equals(""))
        {
            holder.Tv_payment_amount.setVisibility(View.GONE);
        }
        else
        {
            holder.Tv_payment_amount.setVisibility(View.VISIBLE);

            holder.Tv_payment_amount.setPaintFlags(holder.Tv_payment_amount.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }
        holder.Tv_payment_amount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HNDHelper.showErrorAlert(context, whatsincludedinarrayo.get(position));

            }
        });
        //holder.Tv_payment_amount.setText(data.get(position).getPayment_amount());



        return view;
    }
}

