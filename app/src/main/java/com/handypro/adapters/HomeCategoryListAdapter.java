package com.handypro.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.handypro.Pojo.ParentCategorypojo;
import com.handypro.Pojo.SessionParentPojo;
import com.handypro.Pojo.SubCategoryPojo;
import com.handypro.R;
import com.handypro.Widgets.RecyclerItemClickListener;
import com.handypro.fragment.HomePageFragment;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.BoldCustomTextView;
import com.handypro.textview.CustomTextView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by CAS63 on 2/13/2018.
 */

public class HomeCategoryListAdapter extends BaseAdapter {
    private ArrayList<ParentCategorypojo> myCategoryArrList;
    private Context myContext;
    private LayoutInflater mInflater;
    private HomeSubCategoryAdapter mySubCatAdapter;
    private ArrayList<String> mySelectedSubArrList;
    private ArrayList<String> mySelectedSubCatArr;
    private SharedPreference mySession;
    ArrayList<SessionParentPojo> aSessionParentInfoList = null;

    public HomeCategoryListAdapter(ArrayList<ParentCategorypojo> myCategoryArrList, Context myContext) {
        this.myCategoryArrList = myCategoryArrList;
        this.myContext = myContext;
        mInflater = LayoutInflater.from(myContext);
        mySelectedSubCatArr = new ArrayList<>();
        mySession = new SharedPreference(myContext);
        aSessionParentInfoList = mySession.getSessionParentDetails();
        checkForSelectedItems();
    }

    private void checkRemovedItems() {
        ArrayList<String> aSelectedIds = new ArrayList<String>(Arrays.asList(mySession.getCategoryId()
                .split(",")));

        if (aSelectedIds.size() > 0 && mySelectedSubCatArr.size() != 0) {
            ArrayList<String> list = new ArrayList<String>(mySelectedSubCatArr);
            list.removeAll(aSelectedIds);

            for (int b = 0; b < list.size(); b++) {
                for (int p = 0; p < myCategoryArrList.size(); p++) {
                    for (int s = 0; s < myCategoryArrList.get(p).getSubCategoryListArr().size(); s++) {
                        if (list.get(b).equals(myCategoryArrList.get(p).getSubCategoryListArr().get(s).getSubCategoryId())) {
                            myCategoryArrList.get(p).getSubCategoryListArr().get(s).setIsSubCategorySelected("0");
                            showMoreInsSelected(myCategoryArrList.get(p));
                            removeSelectedCatItems(myCategoryArrList.get(p).getSubCategoryListArr().get(s));
                        }
                    }
                }
            }
        }
    }

    private void checkRemovedItems11() {
        ArrayList<String> aSelectedIds = new ArrayList<>(Arrays.asList(mySession.getCategoryId()
                .split(",")));


        if (aSelectedIds.size() > 0) {
            mySelectedSubCatArr.clear();

            for (int p = 0; p < myCategoryArrList.size(); p++) {
                for (int s = 0; s < myCategoryArrList.get(p).getSubCategoryListArr().size(); s++) {
                    if (aSelectedIds.contains(myCategoryArrList.get(p).getSubCategoryListArr().get(s).getSubCategoryId())) {
                        myCategoryArrList.get(p).getSubCategoryListArr().get(s).setIsSubCategorySelected("1");
                        showMoreInsSelected(myCategoryArrList.get(p));

                    } else {
                        myCategoryArrList.get(p).getSubCategoryListArr().get(s).setIsSubCategorySelected("0");
                        showMoreInsSelected(myCategoryArrList.get(p));
                    }
                }
            }

            checkForSelectedItems();

        }
    }

    /**
     * While refreshing adapter to add selected category items
     */
    private void checkForSelectedItems() {
        for (int a = 0; a < HomePageFragment.myCategoryArrList.size(); a++) {
            for (int b = 0; b < HomePageFragment.myCategoryArrList.get(a).getTempSubCategoryListArr().size(); b++) {
                if (HomePageFragment.myCategoryArrList.get(a).getTempSubCategoryListArr().get(b).getIsSubCategorySelected().equals("1") &&
                        !(mySelectedSubCatArr.contains(HomePageFragment.myCategoryArrList.get(a).getTempSubCategoryListArr().get(b).getSubCategoryId()))) {
                    mySelectedSubCatArr.add(HomePageFragment.myCategoryArrList.get(a).getTempSubCategoryListArr().get(b).getSubCategoryId());
                }
            }
        }
    }

    @Override
    public int getCount() {
        return myCategoryArrList.size();
    }

    @Override
    public ParentCategorypojo getItem(int position) {
        return myCategoryArrList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.layout_inflate_category_list_item, parent, false);
            holder.myCategoryTitleTXT = convertView.findViewById(R.id.layout_inflater_category_name_textView);
            holder.myMoreSubCategoryTXT = convertView.findViewById(R.id.layout_inflater_subcategory_more_textView);
            holder.myCategoryRecyclerView = convertView.findViewById(R.id.layout_inflater_category_sublist_recyclerview);
            convertView.setTag(holder);


            RecyclerViewItemClickListener(holder.myCategoryRecyclerView, position, holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (mySession.isNewSelection()) {
            checkRemovedItems();
        }

        if (mySession.isNewSearch()) {
            checkRemovedItems11();
        }

        holder.myCategoryTitleTXT.setText(myCategoryArrList.get(position).getParentCategory_name());
        holder.myMoreSubCategoryTXT.setText(myCategoryArrList.get(position).getParentSubMoreCategory_item());
        showSelectedItems(myCategoryArrList.get(position), holder);

        mySubCatAdapter = new HomeSubCategoryAdapter(myContext, myCategoryArrList.get(position).getSubCategoryListArr());
        holder.myCategoryRecyclerView.setAdapter(mySubCatAdapter);
        setLayoutManager(holder.myCategoryRecyclerView);
        return convertView;
    }

    private void showSelectedItems(ParentCategorypojo aParentCategorypojo, ViewHolder holder) {
        for (int a = 0; a < aParentCategorypojo.getTempSubCategoryListArr().size(); a++) {
            if (aParentCategorypojo.getTempSubCategoryListArr().get(a).getIsSubCategorySelected().equals("1")) {
                AddRemoveSelectedItems((aParentCategorypojo.getTempSubCategoryListArr()), aParentCategorypojo.getParentCategory_name(), holder);
                showMoreInsSelected(aParentCategorypojo);
                break;
            }
        }
    }

    private void RecyclerViewItemClickListener(RecyclerView aCategoryRecyclerView, final int aListViewPos, final ViewHolder holder) {
        aCategoryRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(myContext, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ParentCategorypojo aParentCatPojo = getItem(aListViewPos);
                if (aParentCatPojo.getSubCategoryListArr().get(position).getIsSubCategorySelected().isEmpty() ||
                        aParentCatPojo.getSubCategoryListArr().get(position).getIsSubCategorySelected().equals("0")) {
                    mySession.setNewSelection(false);
                    mySession.setNewsearch(false);
                    aParentCatPojo.getSubCategoryListArr().get(position).setIsSubCategorySelected("1");
                    AddRemoveSelectedItems((myCategoryArrList.get(aListViewPos).getTempSubCategoryListArr()), myCategoryArrList.get(aListViewPos).getParentCategory_name(), holder);
                    HomePageFragment.checkedQuestion(aParentCatPojo.getSubCategoryListArr().get(position).getSubCategoryId());
                } else {
                    mySession.setNewsearch(false);
                    mySession.setNewSelection(false);
                    aParentCatPojo.getSubCategoryListArr().get(position).setIsSubCategorySelected("0");
                    removeSelectedCatItems(aParentCatPojo.getSubCategoryListArr().get(position));
                    if (aSessionParentInfoList != null && aSessionParentInfoList.size() > 0) {
                        for (int h = 0; h < aSessionParentInfoList.size(); h++) {
                            if (aParentCatPojo.getSubCategoryListArr().get(position).getSubCategoryId().equalsIgnoreCase(aSessionParentInfoList.get(h).getSessionParentId())) {
                                aSessionParentInfoList.remove(h);
                            }
                        }
                    }
                    AddRemoveSelectedItems((myCategoryArrList.get(aListViewPos).getTempSubCategoryListArr()), myCategoryArrList.get(aListViewPos).getParentCategory_name(), holder);
                    mySubCatAdapter.notifyDataSetChanged();
                }


//                if (aParentCatPojo.getSubCategoryListArr().get(position).getIsSubCategorySelected().isEmpty() ||
//                        aParentCatPojo.getSubCategoryListArr().get(position).getIsSubCategorySelected().equals("0")) {
//                    mySession.setNewSelection(false);
//                    mySession.setNewsearch(false);
//                    aParentCatPojo.getSubCategoryListArr().get(position).setIsSubCategorySelected("1");
//                    AddRemoveSelectedItems((myCategoryArrList.get(aListViewPos).getTempSubCategoryListArr()), myCategoryArrList.get(aListViewPos).getParentCategory_name(), holder);
//                } else {
//                    aParentCatPojo.getSubCategoryListArr().get(position).setIsSubCategorySelected("0");
//                    AddRemoveSelectedItems((myCategoryArrList.get(aListViewPos).getTempSubCategoryListArr()), myCategoryArrList.get(aListViewPos).getParentCategory_name(), holder);
//                    removeSelectedCatItems(aParentCatPojo.getSubCategoryListArr().get(position));
//                }
                showMoreInsSelected(aParentCatPojo);
                notifyDataSetChanged();
            }
        }));
    }

    /**
     * Add remove selected/unselected sub category items
     *
     * @param aSubCategoryListArr
     * @param aParentCtgryNameSTR
     * @param holder
     */
    private void AddRemoveSelectedItems
    (ArrayList<SubCategoryPojo> aSubCategoryListArr, String aParentCtgryNameSTR, ViewHolder
            holder) {
        mySelectedSubArrList = new ArrayList<>();

        for (int a = 0; a < aSubCategoryListArr.size(); a++) {
            if (aParentCtgryNameSTR.equalsIgnoreCase(holder.myCategoryTitleTXT.getText().toString().trim())) {
                if (aSubCategoryListArr.get(a).getIsSubCategorySelected().equals("1")) {
                    mySelectedSubArrList.add(aSubCategoryListArr.get(a).getSubCategoryName());
                    if (!mySelectedSubCatArr.contains(aSubCategoryListArr.get(a).getSubCategoryId())) {
                        mySelectedSubCatArr.add(aSubCategoryListArr.get(a).getSubCategoryId());
                    }
                }
            }
        }
    }

    private void removeSelectedCatItems(SubCategoryPojo aSubCatPojo) {
        for (int a = 0; a < mySelectedSubCatArr.size(); a++) {
            if (mySelectedSubCatArr.get(a).equals(aSubCatPojo.getSubCategoryId())) {
                mySelectedSubCatArr.remove(a);
            }
        }
    }

    private void setLayoutManager(RecyclerView aRecyclerVw) {
        aRecyclerVw.setLayoutManager(new LinearLayoutManager(myContext, LinearLayoutManager.HORIZONTAL, false));
        aRecyclerVw.setHasFixedSize(true);
    }

    public ArrayList<String> getSelectedSubCatList() {
        return mySelectedSubCatArr;
    }

    private void showMoreInsSelected(ParentCategorypojo aParentCatPojo) {
        ArrayList<String> aSelArr = new ArrayList<>();
        for (int a = 0; a < aParentCatPojo.getSubCategoryListArr().size(); a++) {
            if (aParentCatPojo.getSubCategoryListArr().get(a).getIsSubCategorySelected().equals("1")) {
                aSelArr.add(aParentCatPojo.getSubCategoryListArr().get(a).getSubCategoryName());
            }
        }
        if (aSelArr.size() > 2) {
            int aTempSize = (aSelArr.size()) - 2;
            aParentCatPojo.setParentSubMoreCategory_item(aSelArr.get(0).concat(",").concat(aSelArr.get(1)).concat("+") + String.valueOf(aTempSize)
                    .concat(" ").concat("more"));
        } else {
            aParentCatPojo.setParentSubMoreCategory_item(TextUtils.join(",", aSelArr));
        }
    }

    private class ViewHolder {
        private CustomTextView myMoreSubCategoryTXT;
        private BoldCustomTextView myCategoryTitleTXT;
        private RecyclerView myCategoryRecyclerView = new RecyclerView(new ContextThemeWrapper(myContext, R.style.ScrollbarRecyclerView));
    }
}
