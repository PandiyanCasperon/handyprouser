package com.handypro.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.handypro.Pojo.OrderStatusPojo;
import com.handypro.R;
import com.handypro.textview.CustomTextView;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/22/2018.
 */

public class OrderStatusAdapter extends BaseAdapter {
    private Context myContext;
    private ArrayList<OrderStatusPojo> myOrderStatArrList;
    private LayoutInflater myInflater;

    public OrderStatusAdapter(Context aContext, ArrayList<OrderStatusPojo> aOrderStatArrList) {
        this.myContext = aContext;
        this.myOrderStatArrList = aOrderStatArrList;
        myInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return myOrderStatArrList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {
        private CustomTextView aOrderStatusTXT,myOrderTimingTXT;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = myInflater.inflate(R.layout.layout_inflater_order_status_list_item, parent, false);
            holder = new ViewHolder();
            holder.aOrderStatusTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_order_status_status_textView);
            holder.myOrderTimingTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_order_status_date_textView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.aOrderStatusTXT.setText(myOrderStatArrList.get(position).getOrderTitle());
        holder.myOrderTimingTXT.setText(myOrderStatArrList.get(position).getOrderDate() + "\n" + myOrderStatArrList.get(position).getOrderTime());
        return convertView;
    }
}
