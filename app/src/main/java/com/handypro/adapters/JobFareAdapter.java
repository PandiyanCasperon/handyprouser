package com.handypro.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.handypro.Pojo.JobFarePojo;
import com.handypro.R;
import com.handypro.textview.CustomTextView;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/22/2018.
 */

public class JobFareAdapter extends BaseAdapter {
    private Context myContext;
    private ArrayList<JobFarePojo> myJobFareArrList;
    private LayoutInflater myInflater;

    public JobFareAdapter(Context aContext, ArrayList<JobFarePojo> aJobFareArrList) {
        this.myContext = aContext;
        this.myJobFareArrList = aJobFareArrList;
        myInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return myJobFareArrList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        private CustomTextView myAmountTXT,myTitleTXT;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = myInflater.inflate(R.layout.layout_inflater_job_fare_list_item, parent, false);
            holder = new ViewHolder();
            holder.myAmountTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_job_fare_list_amount_TXT);
            holder.myTitleTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_job_fare_list_title_TXT);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.myAmountTXT.setText(myJobFareArrList.get(position).getJobFareCurrency() + " " + myJobFareArrList.get(position).getJobFareAmount());
        holder.myTitleTXT.setText(myJobFareArrList.get(position).getJobFareTitle());

        return convertView;
    }
}
