package com.handypro.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;

import androidx.fragment.app.FragmentActivity;

import com.handypro.Pojo.AnswerPojo;
import com.handypro.R;
import com.handypro.Widgets.CircularImageView;
import com.handypro.activities.QuestionAnswerActivity;
import com.handypro.textview.CustomTextView;

import java.util.ArrayList;

public class QuestionListAdapter extends BaseAdapter {

    private FragmentActivity myContext;
    private ArrayList<AnswerPojo> myAnswerInfoList;
    private LayoutInflater mInflater;
    private String myQuestionAnsTypeStr = "";

    public QuestionListAdapter(FragmentActivity aContext, ArrayList<AnswerPojo> aAnswerInfoList, String aQuestionAnsType) {
        this.myContext = aContext;
        this.myAnswerInfoList = aAnswerInfoList;
        this.myQuestionAnsTypeStr = aQuestionAnsType;
        mInflater = LayoutInflater.from(myContext);
    }

    @Override
    public int getCount() {
        return myAnswerInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public ArrayList<AnswerPojo> getArrayList() {
        return myAnswerInfoList;
    }

    class ViewHolder {
        private CustomTextView aNameTXT;
        ImageView aCheckbox, aRadio;
    }


    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_inflater_answer_list_item, parent, false);
            holder = new ViewHolder();
            holder.aNameTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_answer_list_item_TXT_title);
            holder.aCheckbox = (ImageView) convertView.findViewById(R.id.layout_inflater_answer_list_item_IMG_checkbox);
            holder.aRadio = (ImageView) convertView.findViewById(R.id.layout_inflater_answer_list_item_IMG_radio);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (myQuestionAnsTypeStr.equalsIgnoreCase("2")) {
            holder.aCheckbox.setVisibility(View.VISIBLE);
            holder.aRadio.setVisibility(View.GONE);
            if (myAnswerInfoList.get(i).isAnswerselected()) {
                holder.aCheckbox.setBackground(myContext.getResources().getDrawable(R.drawable.icon_checked));
            } else {
                holder.aCheckbox.setBackground(myContext.getResources().getDrawable(R.drawable.icon_unchecked));
            }
        } else {
            holder.aRadio.setVisibility(View.VISIBLE);
            holder.aCheckbox.setVisibility(View.GONE);
            if (myAnswerInfoList.get(i).isAnswerselected()) {
                holder.aRadio.setBackground(myContext.getResources().getDrawable(R.drawable.icon_radio_btn));
            } else {
                holder.aRadio.setBackground(myContext.getResources().getDrawable(R.drawable.icon_radio_unchecked));
            }
        }
        holder.aNameTXT.setText(myAnswerInfoList.get(i).getAnswertitle());
        return convertView;
    }
}
