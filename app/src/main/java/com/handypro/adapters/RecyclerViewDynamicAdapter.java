package com.handypro.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.handypro.Pojo.MultiSubItemPojo;
import com.handypro.R;

import java.util.List;

public class RecyclerViewDynamicAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private ProgressDialog progressBar;
    private String FromTheActivity;
    private int PositionForOperation;
    private List<MultiSubItemPojo> MainMultSubItemsList;
    RecyclerViewDynamicAdapterInterface recyclerViewDynamicAdapterInterface;
    public RecyclerViewDynamicAdapter(Context activity, List<MultiSubItemPojo> MainMultSubItemsList,
                                      int PositionForOperation, String FromTheActivity,
                                      RecyclerViewDynamicAdapterInterface recyclerViewDynamicAdapterInterface) {
        this.context = activity;
        this.MainMultSubItemsList = MainMultSubItemsList;
        this.PositionForOperation = PositionForOperation;
        this.FromTheActivity = FromTheActivity;
        this.recyclerViewDynamicAdapterInterface = recyclerViewDynamicAdapterInterface;
    }

    public interface RecyclerViewDynamicAdapterInterface{
        public void Delete(int InnerPosition);
    }


    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView TextViewSubItemTitleAndHours, TextViewSubItemsDescription, TextViewInnerPosition;
        ImageView ImageViewDelete;
        private ViewHolder(View itemView) {
            super(itemView);
            TextViewSubItemTitleAndHours = itemView.findViewById(R.id.TextViewSubItemTitleAndHours);
            TextViewSubItemsDescription = itemView.findViewById(R.id.TextViewSubItemsDescription);
            TextViewInnerPosition = itemView.findViewById(R.id.TextViewInnerPosition);
            ImageViewDelete = itemView.findViewById(R.id.ImageViewDelete);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_add_sub_items, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;

        if (PositionForOperation == MainMultSubItemsList.get(holder.getAdapterPosition()).getPositionForOperation()){

            if(!MainMultSubItemsList.get(holder.getAdapterPosition()).getStatus().equals("User Rejected")){
                if (FromTheActivity.equals("ApproveEstimateBuilder")){
                    switch (MainMultSubItemsList.get(holder.getAdapterPosition()).getStatus()) {
                        case "User Accepted":
                            viewHolder.ImageViewDelete.setImageResource(R.drawable.ticked);
                            break;
                        case "User Rejected":
                            viewHolder.ImageViewDelete.setImageResource(R.drawable.cancelmark);
                            break;
                        default:
                            if (MainMultSubItemsList.size() > 1) {
                                viewHolder.ImageViewDelete.setVisibility(View.VISIBLE);
                                viewHolder.ImageViewDelete.setImageResource(R.drawable.ic_delete);
                            } else {
                                viewHolder.ImageViewDelete.setVisibility(View.GONE);
                            }
                            break;
                    }
                }
                if (MainMultSubItemsList.get(holder.getAdapterPosition()).getName().length() > 20){
                    viewHolder.TextViewSubItemTitleAndHours.setText(MainMultSubItemsList.get(holder.getAdapterPosition()).getName().substring(0, 20) + "... - " + MainMultSubItemsList.get(holder.getAdapterPosition()).getHours() + " Hours");
                }else {
                    viewHolder.TextViewSubItemTitleAndHours.setText(MainMultSubItemsList.get(holder.getAdapterPosition()).getName() + " - " + MainMultSubItemsList.get(holder.getAdapterPosition()).getHours() + " Hours");
                }

                viewHolder.TextViewSubItemsDescription.setText(MainMultSubItemsList.get(holder.getAdapterPosition()).getDescription());

                viewHolder.ImageViewDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        recyclerViewDynamicAdapterInterface.Delete( MainMultSubItemsList.get(holder.getAdapterPosition()).getInnerPosition());
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return MainMultSubItemsList.size();
    }

}
