package com.handypro.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.handypro.R;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomTextView;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by CAS61 on 2/9/2018.
 */

public class HomeMenuListAdapter extends BaseAdapter {

    private Context myContext;
    private String[] myTitles;
    private int[] myIcons;
    private LayoutInflater mInflater;
    private SharedPreference mySession;
    private Animation myTXTAnim;

    public HomeMenuListAdapter(Context aContext, String[] aTitles, int[] aIcons) {
        this.myContext = aContext;
        this.myTitles = aTitles;
        this.myIcons = aIcons;
        mInflater = LayoutInflater.from(myContext);
        mySession = new SharedPreference(myContext);
    }

    @Override
    public int getCount() {
        return myTitles.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {
        private CustomTextView myTitleTXT, myUserProfileName;
        private ImageView myIMGIcon;
        private RelativeLayout myUserprofileLAY;
        private LinearLayout myNormalLAY;
        private CircleImageView myUserProfileIMG;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.layout_inflate_menu_list_item, parent, false);
            holder.myTitleTXT = convertView.findViewById(R.id.layout_inflate_menu_list_item_TXT_title);
            holder.myIMGIcon = convertView.findViewById(R.id.layout_inflate_menu_list_item_IMG_icon);
            holder.myUserprofileLAY = convertView.findViewById(R.id.layout_inflate_menu_list_item_home_LAY_profile);
            holder.myUserProfileIMG = convertView.findViewById(R.id.layout_inflate_menu_list_item_user_profileIMG);
            holder.myUserProfileName = convertView.findViewById(R.id.layout_inflate_menu_list_item_user_profile_name_TXT);
            holder.myNormalLAY = convertView.findViewById(R.id.layout_inflate_menu_list_item_home_LAY_normal);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        myTXTAnim = AnimationUtils.loadAnimation(myContext, R.anim.nav_show_text);

        if (position == 0) {
            holder.myUserprofileLAY.setVisibility(View.VISIBLE);
            holder.myNormalLAY.setVisibility(View.GONE);

            if (mySession.getUserDetails() != null) {
                holder.myUserProfileName.setText(mySession.getUserDetails().getUserName());
                if (mySession.getUserDetails().getUserProfileImage() != null && !mySession.getUserDetails().getUserProfileImage().isEmpty())
                    /*Picasso.with(myContext)
                            .load(*//*ServiceConstant.Base_Url + *//*mySession.getUserDetails().getUserProfileImage())
                            .resize(500, 500)
                            .placeholder(R.drawable.ic_no_user)
                            .error(R.drawable.ic_no_user)
                            .into(holder.myUserProfileIMG);*/

                    Picasso.get()
                            .load(/*ServiceConstant.Base_Url + */mySession.getUserDetails().getUserProfileImage())
                            .resize(500, 500)
                            .placeholder(R.drawable.ic_no_user)
                            .error(R.drawable.ic_no_user)
                            .into(holder.myUserProfileIMG);
                else
                    holder.myUserProfileIMG.setImageResource(R.drawable.ic_no_user);
            }
        } else {
            holder.myUserprofileLAY.setVisibility(View.GONE);
            holder.myNormalLAY.setVisibility(View.VISIBLE);

            holder.myTitleTXT.setText(myTitles[position]);
            holder.myTitleTXT.startAnimation(myTXTAnim);
            holder.myIMGIcon.setImageResource(myIcons[position]);
        }

        return convertView;
    }
}
