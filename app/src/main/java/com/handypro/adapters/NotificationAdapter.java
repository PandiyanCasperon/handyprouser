package com.handypro.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;

import com.handypro.Pojo.NotificationInfoPojo;
import com.handypro.R;
import com.handypro.textview.CustomTextView;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/5/2018.
 */

public class NotificationAdapter extends BaseExpandableListAdapter {
    private Context myContext;
    private ArrayList<NotificationInfoPojo> myNotificationInfoArrList;

    public NotificationAdapter(Context aContext, ArrayList<NotificationInfoPojo> aNotificationList) {
        this.myContext = aContext;
        this.myNotificationInfoArrList = aNotificationList;
    }

    @Override
    public int getGroupCount() {
        return myNotificationInfoArrList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return myNotificationInfoArrList.get(groupPosition).getNotificationMessageInfo().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return myNotificationInfoArrList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) myContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.layout_inflater_notification_parent_list_item, null);
        }

        CustomTextView aCategoryTXT = (CustomTextView) convertView
                .findViewById(R.id.layout_inflate_notification_parent_list_item_TXT_title);

        ImageView aArrowIMG = (ImageView) convertView
                .findViewById(R.id.layout_inflate_notification_parent_list_item_IMG_arrow);

        aCategoryTXT.setText(myNotificationInfoArrList.get(groupPosition).getNotificationBookingId()
                + " - " + myNotificationInfoArrList.get(groupPosition).getNotificationCategory());
        if (isExpanded) {
            aArrowIMG.setImageResource(R.drawable.icon_triangle_arrow_down);
        } else {
            aArrowIMG.setImageResource(R.drawable.icon_triangle_arrow_right);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) myContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.layout_inflater_notification_child_list_item, null);
        }

        CustomTextView aChildCategoryTXT = (CustomTextView) convertView
                .findViewById(R.id.layout_inflater_notification_child_list_item_TXT_title);

        CustomTextView aTimeTXT = (CustomTextView) convertView
                .findViewById(R.id.layout_inflater_notification_child_list_item_TXT_time);


        if(myNotificationInfoArrList.get(groupPosition).getNotificationMessageInfo().get(childPosition).getNotificationMessage().contains("to Serve You and Care for YourHome"))
        {
            aChildCategoryTXT.setText(myContext.getResources().getString(R.string.accejob));
        }
        else
        {
            aChildCategoryTXT.setText(myNotificationInfoArrList.get(groupPosition).getNotificationMessageInfo().get(childPosition).getNotificationMessage());
        }







        aTimeTXT.setText(myNotificationInfoArrList.get(groupPosition).getNotificationMessageInfo().get(childPosition).getNotificationMessageCreatedAt());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

