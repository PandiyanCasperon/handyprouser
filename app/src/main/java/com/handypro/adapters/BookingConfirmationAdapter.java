package com.handypro.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.handypro.Pojo.ParentQuestionPojo;
import com.handypro.Pojo.SessionParentPojo;
import com.handypro.Pojo.SubCategoryPojo;
import com.handypro.R;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.BoldCustomTextView;
import com.handypro.textview.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/1/2018.
 */

public class BookingConfirmationAdapter extends BaseAdapter {
    private Context myContext;
    private ArrayList<SubCategoryPojo> mySubCatArrList;
    private LayoutInflater myInflater;
    private SharedPreference mySession;
    ArrayList<SessionParentPojo> aSessionParentInfoList = null;

    public BookingConfirmationAdapter(Context aContext, ArrayList<SubCategoryPojo> aSubCatArrList) {
        this.myContext = aContext;
        this.mySubCatArrList = aSubCatArrList;
        myInflater = LayoutInflater.from(aContext);
        mySession = new SharedPreference(myContext);
        aSessionParentInfoList = mySession.getSessionParentDetails();
    }

    @Override
    public int getCount() {
        return mySubCatArrList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = myInflater.inflate(R.layout.layout_inflate_booking_confirm_list_item, parent, false);
            holder.mySubCategoryImageView = convertView.findViewById(R.id.layout_inflater_booking_confirm_profile_imageView);
            holder.mySubCategoryNameTXT = convertView.findViewById(R.id.layout_inflater_booking_confirm_name_textView);
            holder.mySubCategoryAmountTXT = convertView.findViewById(R.id.layout_inflater_booking_confirm_amount_textView);
            holder.myBaseLaborOnlyTXT = convertView.findViewById(R.id.layout_inflater_booking_confirm_list_base_labor_onlyTXT);
            holder.myAnswerTXT = convertView.findViewById(R.id.layout_inflater_booking_confirm_TXT_question);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.mySubCategoryNameTXT.setText(mySubCatArrList.get(position).getSubCategoryName());

        String aFinalSelectedStr = "";
        if (aSessionParentInfoList != null && aSessionParentInfoList.size() > 0) {
            for (int h = 0; h < aSessionParentInfoList.size(); h++) {
                if (mySubCatArrList.get(position).getSubCategoryId().equalsIgnoreCase(aSessionParentInfoList.get(h).getSessionParentId())) {
                    for (int u = 0; u < aSessionParentInfoList.get(h).getaPojo().size(); u++) {
                        String aSelectedStr = "";
                        for (int n = 0; n < aSessionParentInfoList.get(h).getaPojo().get(u).getAnswerpojo().size(); n++) {
                            if (aSessionParentInfoList.get(h).getaPojo().get(u).getAnswerpojo().get(n).isAnswerselected()) {
                                if (aSelectedStr.equalsIgnoreCase("")) {
                                    aSelectedStr = aSessionParentInfoList.get(h).getaPojo().get(u).getAnswerpojo().get(n).getAnswertitle();
                                } else {
                                    aSelectedStr = aSelectedStr + "," + aSessionParentInfoList.get(h).getaPojo().get(u).getAnswerpojo().get(n).getAnswertitle();
                                }
                            }
                        }
                        ParentQuestionPojo aParentQuestionPojo = aSessionParentInfoList.get(h).getaPojo().get(u);
                        String aAnswerStr = aParentQuestionPojo.getQuestionAnsView();
                        // Log.e("Split111", aAnswerStr.replace("{{answer}}", aSelectedStr));
                        if (aFinalSelectedStr.equalsIgnoreCase("")) {
                            aFinalSelectedStr = aAnswerStr.replace("{{answer}}", aSelectedStr);
                        } else {
                            aFinalSelectedStr = aFinalSelectedStr + "\n" + aAnswerStr.replace("{{answer}}", aSelectedStr);
                        }
                        Log.e("aFinalSelectedStr", aFinalSelectedStr);
                        holder.myAnswerTXT.setVisibility(View.VISIBLE);
                        holder.myAnswerTXT.setText(aFinalSelectedStr);
                    }
                }
            }
        } else {
            holder.myAnswerTXT.setVisibility(View.GONE);
        }

        /*Picasso.with(myContext)
                .load(*//*ServiceConstant.Base_Url +*//* mySubCatArrList.get(position).getSubCategoryActiveIcon())
                .resize(100, 100)
                .into(holder.mySubCategoryImageView);*/

        Picasso.get().load(mySubCatArrList.get(position).getSubCategoryActiveIcon())
                .resize(100, 100).into(holder.mySubCategoryImageView);

        if (mySubCatArrList.get(position).getSubCategoryJobType().equalsIgnoreCase(myContext.getResources().getString(R.string.activity_shedule_appointment_estimate_label))) {

            holder.mySubCategoryAmountTXT.setText(myContext.getResources().getString(R.string.adapter_booknig_confirmation_estimate));
        } else {

            holder.mySubCategoryAmountTXT.setText(mySubCatArrList.get(position).getSubCategoryHourlyRate());
        }


        if (mySubCatArrList.get(position).getJoborestimate().length() > 4) {
            holder.myBaseLaborOnlyTXT.setVisibility(View.VISIBLE);
        } else {
            holder.myBaseLaborOnlyTXT.setVisibility(View.GONE);
        }

        holder.myBaseLaborOnlyTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HNDHelper.showWhatIncludePopup("What's Include", myContext, mySubCatArrList.get(position).getJoborestimate());
            }
        });

        return convertView;
    }

    private class ViewHolder {
        private CustomTextView mySubCategoryAmountTXT, myBaseLaborOnlyTXT;
        ImageView mySubCategoryImageView;
        BoldCustomTextView mySubCategoryNameTXT;
        CustomTextView myAnswerTXT;
    }
}
