package com.handypro.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.handypro.R;

import java.util.ArrayList;

/**
 * Created by CAS63 on 2/20/2018.
 */

public class LocationSearchAdapter extends BaseAdapter {

    private ArrayList<String> data;
    private LayoutInflater mInflater;
    private Context context;

    public LocationSearchAdapter(Context c, ArrayList<String> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView name;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.layout_inflater_location_search, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.layout_inflater_location_search_textview);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.name.setText(data.get(position));

        return view;
    }
}


