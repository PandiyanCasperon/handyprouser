package com.handypro.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.handypro.Pojo.TaskerCategoryPojo;
import com.handypro.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/15/2018.
 */

public class TaskerCategoryAdapter extends RecyclerView.Adapter<TaskerCategoryAdapter.ViewHolder> {
    private ArrayList<TaskerCategoryPojo> myTskrCatgryArrList;
    private Context myContext;

    public TaskerCategoryAdapter(Context myContext, ArrayList<TaskerCategoryPojo> aTskrCatgryArr) {
        this.myContext = myContext;
        this.myTskrCatgryArrList = aTskrCatgryArr;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(myContext).inflate(R.layout.layout_inflater_tasker_category_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.mySubcategoryTXT.setText(myTskrCatgryArrList.get(position).getCategoryName());

        /*Picasso.with(myContext)
                .load(*//*ServiceConstant.Base_Url + *//*myTskrCatgryArrList.get(position).getCategoryImage())
                .into(holder.mysubCatgryIMG);*/

        Picasso.get().load(myTskrCatgryArrList.get(position).getCategoryImage())
                .into(holder.mysubCatgryIMG);

    }

    @Override
    public int getItemCount() {
        return myTskrCatgryArrList.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder {

        TextView mySubcategoryTXT;
        ImageView mysubCatgryIMG;

        private ViewHolder(View itemView) {
            super(itemView);

            mySubcategoryTXT = (TextView) itemView.findViewById(R.id.layout_inflater_tasker_category_name_TXT);
            mysubCatgryIMG = (ImageView) itemView.findViewById(R.id.layout_inflater_task_category_imageVw);

        }
    }

}

