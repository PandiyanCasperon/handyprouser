package com.handypro.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.handypro.Pojo.TransactionInfoPojo;
import com.handypro.R;
import com.handypro.textview.CustomTextView;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/5/2017.
 */
public class TransactionListAdapter extends BaseAdapter {
    private Context myContext;
    private ArrayList<TransactionInfoPojo> myTransactionInfoList;
    private LayoutInflater mInflater;

    public TransactionListAdapter(Context aContext, ArrayList<TransactionInfoPojo> aTransactionInfoList) {
        this.myContext = aContext;
        this.myTransactionInfoList = aTransactionInfoList;
        mInflater = LayoutInflater.from(myContext);
    }

    @Override
    public int getCount() {
        return myTransactionInfoList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {
        private CustomTextView aJobIdTXT, aPriceTXT, aCategoryTXT, myDateTXT;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_inflate_transaction_list_item, parent, false);
            holder = new ViewHolder();

            holder.aJobIdTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflate_transaction_list_item_TXT_job_id);
            holder.aPriceTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflate_transaction_list_item_TXT_price);
            holder.aCategoryTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflate_transaction_list_item_TXT_category);
            holder.myDateTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflate_transaction_list_item_TXT_date);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.aJobIdTXT.setText("JOB ID:"+myTransactionInfoList.get(position).getTransactionJobId());
        holder.aCategoryTXT.setText("SERVICE TYPE:"+myTransactionInfoList.get(position).getTransactionCategoryName());
        holder.aPriceTXT.setText(myTransactionInfoList.get(position).getTransactionTotalAmount());
        holder.myDateTXT.setText(myTransactionInfoList.get(position).getTransactionDate().replace("/",".") + "  " + myTransactionInfoList.get(position).getTransactionTime());
        return convertView;
    }
}
