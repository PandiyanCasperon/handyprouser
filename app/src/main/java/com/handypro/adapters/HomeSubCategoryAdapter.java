package com.handypro.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.handypro.Pojo.ParentCategorypojo;
import com.handypro.Pojo.SubCategoryPojo;
import com.handypro.R;
import com.handypro.textview.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by CAS63 on 2/13/2018.
 */

public class HomeSubCategoryAdapter extends RecyclerView.Adapter<HomeSubCategoryAdapter.ViewHolder> {
    private ArrayList<SubCategoryPojo> mySubCategoryArrList;
    private Context myContext;

    HomeSubCategoryAdapter(Context myContext, ArrayList<SubCategoryPojo> aSubCategoryListArr) {
        this.myContext = myContext;
        this.mySubCategoryArrList = aSubCategoryListArr;
    }

    @NonNull
    @Override
    public HomeSubCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(myContext).inflate(R.layout.layout_inflater_sub_category_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HomeSubCategoryAdapter.ViewHolder holder, int position) {
        holder.mySubCategoryNameTXT.setText(mySubCategoryArrList.get(position).getSubCategoryName());
        holder.mySubCategoryMainNameTXT.setText(mySubCategoryArrList.get(position).getJoborestimate());
        if (mySubCategoryArrList.get(position).getIsSubCategorySelected().equals("1")) {
            Picasso.get()
                    .load(/*ServiceConstant.Base_Url +*/ mySubCategoryArrList.get(position).getSubCategoryActiveIcon())
                    .resize(100, 100)
                    .into(holder.mySubCategoryImageView);
        } else {
            Picasso.get().load(/*ServiceConstant.Base_Url +*/ mySubCategoryArrList.get(position).getSubCategoryInactiveIcon())
                    .resize(100, 100)
                    .into(holder.mySubCategoryImageView);
        }
        if (position == (getCount() - 1)) {
            holder.aViewLine.setVisibility(View.GONE);
        } else {
            holder.aViewLine.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mySubCategoryArrList.size();
    }

    public void updateInfo(ArrayList<SubCategoryPojo> subCategoryListArr) {
        this.mySubCategoryArrList = subCategoryListArr;
        notifyDataSetChanged();
    }

    public ArrayList<SubCategoryPojo> getArrayList() {
        return mySubCategoryArrList;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mySubCategoryImageView;
        CustomTextView mySubCategoryNameTXT, mySubCategoryMainNameTXT;
        View aViewLine;
        RelativeLayout RelativeLayoutMainHolder;

        private ViewHolder(View itemView) {
            super(itemView);
            mySubCategoryImageView = itemView.findViewById(R.id.layout_inflate_sub_category_IMG);
            mySubCategoryNameTXT = itemView.findViewById(R.id.layout_inflate_menu_sub_category_TXT_title);
            mySubCategoryMainNameTXT = itemView.findViewById(R.id.layout_inflate_menu_main_category_TXT_title);
            aViewLine = itemView.findViewById(R.id.layout_inflate_menu_main_category_view_line);
            RelativeLayoutMainHolder = itemView.findViewById(R.id.RelativeLayoutMainHolder);
        }
    }

    private int getCount() {
        return mySubCategoryArrList.size();
    }
}
