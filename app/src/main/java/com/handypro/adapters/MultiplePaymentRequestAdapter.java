package com.handypro.adapters;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.User_pre_amount_list;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;

import org.json.JSONObject;

public class MultiplePaymentRequestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private User_pre_amount_list[] user_pre_amount_list_main;
    private ProgressDialog progressBar;
    private String currency, Job_id;
    private SelectedPrepaymentListID selectedPrepaymentListID;
    private List<User_pre_amount_list> MultiplePrepaymentSelectedFromAdapterList;
    private ProgressDialogcreated myDialog;
    public MultiplePaymentRequestAdapter(Context activity, User_pre_amount_list[] user_pre_amount_list_main, String currency, String Job_id, SelectedPrepaymentListID selectedPrepaymentListID) {
        this.context = activity;
        this.user_pre_amount_list_main = user_pre_amount_list_main;
        this.currency = currency;
        this.selectedPrepaymentListID = selectedPrepaymentListID;
        this.Job_id = Job_id;
    }

    public interface SelectedPrepaymentListID{
        void SelectedList(List<User_pre_amount_list> List);
        void RefreshTheList();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView TextViewAmount, TextViewDateTime, TextViewStatus;
        CheckBox CheckBoxSelectPayment;
        ImageView ImageViewReject;
        private ViewHolder(View itemView) {
            super(itemView);
            TextViewAmount = (TextView) itemView.findViewById(R.id.TextViewAmount);
            TextViewDateTime = (TextView) itemView.findViewById(R.id.TextViewDateTime);
            TextViewStatus = (TextView) itemView.findViewById(R.id.TextViewStatus);
            CheckBoxSelectPayment = (CheckBox) itemView.findViewById(R.id.CheckBoxSelectPayment);
            ImageViewReject = (ImageView) itemView.findViewById(R.id.ImageViewReject);
            MultiplePrepaymentSelectedFromAdapterList = new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_multiple_payment_request_row, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);
        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.TextViewAmount.setText(currency+user_pre_amount_list_main[holder.getAdapterPosition()].getPre_amount() + " - " + user_pre_amount_list_main[holder.getAdapterPosition()].getPayment_status());
        String temp = user_pre_amount_list_main[holder.getAdapterPosition()].getDate().split("T")[0];
        DateToString(StringToDate(temp, "yyyy-MM-dd"), "MM-dd-yyyy");
        viewHolder.TextViewDateTime.setText(DateToString(StringToDate(temp, "yyyy-MM-dd"), "MM-dd-yyyy")+ " "+user_pre_amount_list_main[holder.getAdapterPosition()].getTime());
        //Based on the status of the payment, I'm setting the color for amount
        switch (user_pre_amount_list_main[holder.getAdapterPosition()].getPayment_status()){
            case "Pending":
                viewHolder.TextViewAmount.setTextColor(Color.parseColor("#FFA500"));
                break;
            case "Accepted":
                viewHolder.TextViewAmount.setTextColor(Color.parseColor("#00FF00"));
                viewHolder.CheckBoxSelectPayment.setEnabled(false);
                viewHolder.ImageViewReject.setVisibility(View.GONE);
                break;
            case "Rejected":
                viewHolder.TextViewAmount.setTextColor(Color.parseColor("#FF0000"));
                break;
        }
        viewHolder.TextViewStatus.setText(user_pre_amount_list_main[holder.getAdapterPosition()].getPayment_status());
        viewHolder.CheckBoxSelectPayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                //If checkbox is selected then adding the selected values to List(MultiplePrepaymentSelectedFromAdapterList)
                if (isChecked){
                    User_pre_amount_list user_pre_amount_list = new User_pre_amount_list();
                    user_pre_amount_list.set_id(user_pre_amount_list_main[holder.getAdapterPosition()].get_id());
                    user_pre_amount_list.setPre_amount(user_pre_amount_list_main[holder.getAdapterPosition()].getPre_amount());
                    user_pre_amount_list.setPayment_status(user_pre_amount_list_main[holder.getAdapterPosition()].getPayment_status());
                    MultiplePrepaymentSelectedFromAdapterList.add(user_pre_amount_list);
                    //Here I'm using the interface to send the selected value to the activity.
                    selectedPrepaymentListID.SelectedList(MultiplePrepaymentSelectedFromAdapterList);
                }
                //Once again selected value is deselected means removing it from List(MultiplePrepaymentSelectedFromAdapterList)
                else {
                    if (MultiplePrepaymentSelectedFromAdapterList.size()!=0){
                        for (int i = 0; i < MultiplePrepaymentSelectedFromAdapterList.size(); i++){
                            if (MultiplePrepaymentSelectedFromAdapterList.get(i).get_id().equals(user_pre_amount_list_main[holder.getAdapterPosition()].get_id())){
                                MultiplePrepaymentSelectedFromAdapterList.remove(i);
                                break;
                            }
                        }
                        //Here I'm using the interface to send the selected value to the activity.
                        selectedPrepaymentListID.SelectedList(MultiplePrepaymentSelectedFromAdapterList);
                    }
                }
            }
        });
        viewHolder.ImageViewReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user_pre_amount_list_main[holder.getAdapterPosition()].getPayment_status().equals("Pending")){
                    //If user select the reject payment means create the alert dialog
                    new AlertDialog.Builder(context)
                            .setTitle("Reject")
                            .setMessage("Are you sure want to reject this prepayment?")

                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Continue with delete operation
                                    startLoading();
                                    HashMap<String, String> params = new HashMap<>();
                                    params.put("job_id", Job_id);
                                    params.put("est[" + 0 + "][prepayment_id]", user_pre_amount_list_main[holder.getAdapterPosition()].get_id());
                                    params.put("est[" + 0 + "][prepayment_status]", "Rejected");
                                    ServiceRequest mservicerequest = new ServiceRequest(context);
                                    mservicerequest.makeServiceRequest(ServiceConstant.PrepaymentRequestListUpdate, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                                        @Override
                                        public void onCompleteListener(String response) {
                                            stopLoading();
                                            Log.e("pre_amount/list", response);
                                            try {
                                                JSONObject jobject = new JSONObject(response);
                                                if (jobject.has("status") && jobject.getString("status").equals("1")){
                                                    //If we get the response as success means refresh the list to get updated value from server
                                                    //So I'm using the interface to do it.
                                                    selectedPrepaymentListID.RefreshTheList();
                                                }else {
                                                    Toast.makeText(context, "Something went wrong! Try again", Toast.LENGTH_SHORT).show();
                                                }
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }

                                        }

                                        @Override
                                        public void onErrorListener() {
                                            stopLoading();
                                        }
                                    });
                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(R.mipmap.handy_app_icon)
                            .show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return user_pre_amount_list_main.length;
    }

    //Function used to convert the Date to String.
    private String DateToString(Date date, String Format){
        return new SimpleDateFormat(Format, Locale.getDefault()).format(date);
    }

    //Function used to convert String to Date.
    private Date StringToDate(String date, String Format){
        Date OutPutDate = null;
        try {
            OutPutDate =  new SimpleDateFormat(Format, Locale.getDefault()).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return OutPutDate;
    }

    //Function to start the loading the Dialog.
    private void startLoading() {
        myDialog = new ProgressDialogcreated(context);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
    }

    //Function to stop the loading the Dialog.
    private void stopLoading() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if( myDialog != null)
                {
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                }
            }
        }, 500);
    }
}
