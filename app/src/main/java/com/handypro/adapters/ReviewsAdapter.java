package com.handypro.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;

import com.handypro.Pojo.ReviewInfoPojo;
import com.handypro.R;
import com.handypro.Widgets.CircularImageView;
import com.handypro.textview.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/5/2018.
 */

public class ReviewsAdapter extends BaseAdapter {

    private Context myContext;
    private ArrayList<ReviewInfoPojo> myReviewInfoList;
    private LayoutInflater mInflater;


    public ReviewsAdapter(Context aContext, ArrayList<ReviewInfoPojo> aReviewInfoList) {
        this.myContext = aContext;
        this.myReviewInfoList = aReviewInfoList;
        mInflater = LayoutInflater.from(myContext);
    }

    @Override
    public int getCount() {
        return myReviewInfoList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {
        private CustomTextView aNameTXT, aJobIdTXT, aCommentsTXT, aViewImageTXT, aTimeTXT, aCategoryTXT;
        private CircularImageView aTaskerIMG;
        private RatingBar aRatingBar;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_inflater_review_list_item, parent, false);
            holder = new ViewHolder();
            holder.aNameTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_review_list_item_TXT_name);
            holder.aJobIdTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_review_list_item_TXT_Jobid);
            holder.aCommentsTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_review_list_item_TXT_comments);
            holder.aTaskerIMG = (CircularImageView) convertView.findViewById(R.id.layout_inflater_review_list_item_IMG_profile);
            holder.aRatingBar = (RatingBar) convertView.findViewById(R.id.layout_inflater_review_list_item_RGB);
            holder.aCategoryTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_review_list_item_TXT_category);
            holder.aViewImageTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_review_list_item_TXT_viewimage);
            holder.aTimeTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_review_list_item_TXT_time);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.aNameTXT.setText(myReviewInfoList.get(position).getReviewTasker());
        holder.aRatingBar.setRating(Float.parseFloat(myReviewInfoList.get(position).getReviewRating()));
        holder.aCommentsTXT.setText(myReviewInfoList.get(position).getReviewComments());
        holder.aJobIdTXT.setText(myReviewInfoList.get(position).getReviewBookingId() + "");
        holder.aCategoryTXT.setText(myReviewInfoList.get(position).getReviewCategory());
        holder.aTimeTXT.setText(myReviewInfoList.get(position).getReviewDate());

        Picasso.get()
              .load(String.valueOf(myReviewInfoList.get(position).getReviewImage()))
               .placeholder(R.drawable.icon_placeholder)
              .into(holder.aTaskerIMG);


        return convertView;
    }
}

