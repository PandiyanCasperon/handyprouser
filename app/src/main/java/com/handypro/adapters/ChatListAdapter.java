package com.handypro.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.handypro.Pojo.ChatInfoPojo;
import com.handypro.R;
import com.handypro.textview.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by CAS63 on 3/5/2018.
 */

public class ChatListAdapter extends BaseAdapter {
    private ArrayList<ChatInfoPojo> myInfoList;
    private Context myContext;
    private LayoutInflater mInflater;

    public ChatListAdapter(Context aContext, ArrayList<ChatInfoPojo> aInfoList) {
        this.myContext = aContext;
        mInflater = LayoutInflater.from(myContext);
        this.myInfoList = aInfoList;

    }

    @Override
    public int getCount() {
        return myInfoList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {
        private CustomTextView aOrderIdTXT, aTaskerNameTXT, aJobcategoryTXT, aDateTXT;
        private CircleImageView aTaskerIMG;
        ImageView green_dot;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_inflater_chat_list_item, parent, false);
            holder = new ViewHolder();
            holder.aOrderIdTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflate_chat_list_item_job_id);
            holder.aTaskerNameTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflate_chat_list_item_username);
            holder.aTaskerIMG = (CircleImageView) convertView.findViewById(R.id.layout_inflate_chat_list_item_IMG);
            holder.aJobcategoryTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflate_chat_list_item_job_category);
            holder.aDateTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflate_chat_list_item_date);
            holder.green_dot = (ImageView) convertView.findViewById(R.id.green_dot);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        if(myInfoList.get(position).getstatus().equalsIgnoreCase("1")){
//            holder.green_dot.setVisibility(View.VISIBLE);
//        }else{
//            holder.green_dot.setVisibility(View.GONE);
//        }

        holder.aOrderIdTXT.setText(myContext.getResources().getString(R.string.jobbid) + myInfoList.get(position).getMessageBookingId());
        holder.aTaskerNameTXT.setText(myContext.getResources().getString(R.string.craftman) + myInfoList.get(position).getMessageTaskerNameId());
        holder.aJobcategoryTXT.setText(myInfoList.get(position).getCategory());
        holder.aDateTXT.setText(myInfoList.get(position).getMessagedate().replace("/", "."));

        if (myInfoList.get(position).getMessageTaskerImageId().contains("http")) {
            Picasso.get()
                    .load(myInfoList.get(position).getMessageTaskerImageId())
                    .error(R.drawable.icon_placeholder)
                    .fit()
                    .into(holder.aTaskerIMG);
        } else {
            Picasso.get()
                    .load(/*ServiceConstant.Base_Url +*/myInfoList.get(position).getMessageTaskerImageId())
                    .error(R.drawable.icon_placeholder)
                    .fit()
                    .into(holder.aTaskerIMG);
        }

        return convertView;
    }
}

