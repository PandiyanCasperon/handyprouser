package com.handypro.core.socket;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Messenger;

import com.handypro.activities.NotificationDB;
import com.handypro.activities.Xmpp_PushNotificationPage;
import com.handypro.sharedpreference.SharedPreference;

import org.json.JSONObject;

import java.util.ArrayList;

public class SocketHandler {
    public static String user;
    private static SocketHandler instance;
    String value = "";
    private Context context;
    private SocketManager manager;
    private ArrayList<Messenger> listenerMessenger;
    private String Job_status_message = "", actionnew = "";
    public SocketManager.SocketCallBack callBack = new SocketManager.SocketCallBack() {
        boolean isChat = false;

        @Override
        public void onSuccessListener(Object response) {
            if (response instanceof JSONObject) {
                JSONObject jsonObject = (JSONObject) response;
                value = jsonObject.toString();
                NotificationDB mHelper;
                SQLiteDatabase dataBase;


                SharedPreference mySession;
                mySession = new SharedPreference(context);
                if (mySession.getLogInStatus() == false) {

                } else {
                    try {
                        JSONObject json1 = new JSONObject(value);
                        JSONObject object1 = json1.getJSONObject("data");
                        if (object1.has("key0")) {


                            if (json1.has("uniqueId")) {

                                int okl = 0;
                                String uniqueId = json1.getString("uniqueId");
                                System.out.println("Socket response--->socket-unique id" + uniqueId);
                                mHelper = new NotificationDB(context);
                                dataBase = mHelper.getWritableDatabase();
                                Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + mHelper.TABLE_NAME + " WHERE uniqueid ='" + uniqueId + "'", null);

                                if (mCursor.moveToFirst()) {
                                    do {
                                        okl++;
                                        System.out.println("Socket response--->socket-okl" + okl);
                                    } while (mCursor.moveToNext());
                                }

                                if (okl == 0) {
                                    System.out.println("Socket response--->socket-record not exists" + mHelper.getProfilesCount());
                                    mHelper = new NotificationDB(context);
                                    dataBase = mHelper.getWritableDatabase();
                                    ContentValues values = new ContentValues();
                                    values.put(NotificationDB.KEY_FNAME, value);
                                    values.put(NotificationDB.KEY_LNAME, uniqueId);
                                    dataBase.insert(NotificationDB.TABLE_NAME, null, values);
                                    commonnotification();
                                } else {
                                    System.out.println("Socket response--->socket-record exists" + mHelper.getProfilesCount());
                                }
                            } else if (object1.has("uniqueId")) {
                                int okl = 0;
                                String uniqueId = object1.getString("uniqueId");
                                System.out.println("Socket response--->socket-unique id" + uniqueId);
                                mHelper = new NotificationDB(context);
                                dataBase = mHelper.getWritableDatabase();
                                Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + mHelper.TABLE_NAME + " WHERE uniqueid ='" + uniqueId + "'", null);

                                if (mCursor.moveToFirst()) {
                                    do {
                                        okl++;
                                        System.out.println("Socket response--->socket-okl" + okl);
                                    } while (mCursor.moveToNext());
                                }

                                if (okl == 0) {
                                    System.out.println("Socket response--->socket-record not exists" + mHelper.getProfilesCount());
                                    mHelper = new NotificationDB(context);
                                    dataBase = mHelper.getWritableDatabase();
                                    ContentValues values = new ContentValues();
                                    values.put(NotificationDB.KEY_FNAME, value);
                                    values.put(NotificationDB.KEY_LNAME, uniqueId);
                                    dataBase.insert(NotificationDB.TABLE_NAME, null, values);
                                    commonnotification();
                                } else {
                                    System.out.println("Socket response--->socket-record exists" + mHelper.getProfilesCount());
                                }
                            } else {
                                commonnotification();
                            }

                        } else {
                            String checkdataa = object1.getString("action");
                            if (checkdataa.equalsIgnoreCase("chat")) {
                                Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                                dialogIntent.putExtra("TITLE_INTENT", value);
                                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(dialogIntent);
                            }
                        }

                    } catch (Exception e) {
                    }

                }


            }
        }
    };

    private SocketHandler(Context context) {
        this.context = context;
        this.manager = new SocketManager(callBack, context);
        this.listenerMessenger = new ArrayList<Messenger>();

    }

    public static SocketHandler getInstance(Context context) {
        if (instance == null) {
            instance = new SocketHandler(context);
        }
        return instance;
    }

    public void addChatListener(Messenger messenger) {
        listenerMessenger.add(messenger);
    }

    public SocketManager getSocketManager() {
        SharedPreference session = new SharedPreference(context);
        String mCurrentUserId = session.getUserDetails().getUserId();
        manager.setTaskId(mCurrentUserId);
        return manager;
    }

    private void commonnotification() {


        try {
            JSONObject json1 = new JSONObject(value);
            JSONObject object1 = json1.getJSONObject("data");
            if (object1.has("key0")) {
                System.out.println("Socket response--->socket-" + value);
                actionnew = object1.getString("action");

                if (object1.has("message")) {
                    Job_status_message = object1.getString("message");
                } else {
                    Job_status_message = "";
                }

                if (actionnew.equalsIgnoreCase("job_reassign")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("job_accepted")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("Task_failed")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("start_off")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("provider_reached")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("job_started")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("admin_notification")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("job_completed")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("job_user_approval")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);

                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("change_order_request")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("estimation_received")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("payment_paid")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("pre_payment_paid")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("requesting_payment")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("requesting_pre_payment")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                } else if (actionnew.equalsIgnoreCase("referer_credited")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                }
            } else {
                String checkdataa = object1.getString("action");
                if (checkdataa.equalsIgnoreCase("chat")) {
                    Intent dialogIntent = new Intent(context, Xmpp_PushNotificationPage.class);
                    dialogIntent.putExtra("TITLE_INTENT", value);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
