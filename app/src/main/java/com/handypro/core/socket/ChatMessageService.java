package com.handypro.core.socket;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.Nullable;

import com.handypro.sharedpreference.SharedPreference;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by user145 on 4/7/2017.
 */
public class ChatMessageService extends Service {

    static ChatMessageSocketManager manager;
    static public ChatMessageService service;
    private ActiveSocketDispatcher dispatcher;
    public static Context context;
    public static String task_id = "";
    public static String tasker_id = "";
    private String mCurrentUserId;
    //  private MessageDBHelper messageDB;
    int i=0;
    public static final long INTERVAL=5000;//variable to execute services every 10 second
    private Handler mHandler=new Handler(); // run on another Thread to avoid crash
    private Timer mTimer=null; // timer handling
    private SharedPreference mySession;

    @Override
    public void onCreate() {
        super.onCreate();

        System.out.println("Running service----------->oncreate");

        i=0;
        if(mTimer!=null)
            mTimer.cancel();
        else
            mTimer=new Timer(); // recreate new timer
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(),0,INTERVAL);//
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class TimeDisplayTimerTask extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    i++;
                    mySession = new SharedPreference(ChatMessageService.this);
                    if(mySession.getLogInStatus()==false)
                    {

                    }
                    else
                    {
                        SocketHandler.getInstance(ChatMessageService.this).getSocketManager().connect();
                    }
                }
            });
        }
    }

}
