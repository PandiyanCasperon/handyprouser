package com.handypro.core.socket;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;

import com.handypro.Iconstant.ServiceConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.HashSet;

import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class SocketManager {

    private AppCompatActivity activity;
    private SocketCallBack callBack;
    public static final String EVENT_NEW_MESSAGE = "notification";
    private HashSet<Object> uniqueBucket;
    public boolean isConnected;
    private SocketConnectCallBack location;
    public static final String JOIN_NETWORK = "join network";
    public static final String NETWORK_CREATED = "network created";
    public static final String ROOM_STRING_SWITCH = "switch room";
    public static final String EVENT_LOCATION = "tasker tracking";
    private Context context;
    public static String Userid = "";


    public static interface SocketCallBack {
        void onSuccessListener(Object response);
    }

    public static interface SocketConnectCallBack {
        void onSuccessListener(Object response);
    }


    public SocketManager(SocketCallBack callBack, Context mContext) {
        this.callBack = callBack;
        this.context = mContext;

    }


    public void setTaskId(String userid) {
        Userid = userid;
    }

    private Socket mSocket;
    {
        try {

            Manager.Options options=new Manager.Options();
            // options.transports=new String[]{ip};
            options.reconnectionAttempts= Integer.MAX_VALUE;
            options.reconnectionDelay=0;
            options.reconnection= true;
            options.timeout= 20000;
            options.transports =  new String[]{"polling","websocket"};



            mSocket = IO.socket(ServiceConstant.SOCKET_EVENT_NOTIFYURL);
            mSocket.io().reconnection(true);





           /* mSocket = IO.socket(ServiceConstant.SOCKET_EVENT_NOTIFYURL,options);
            mSocket.io().reconnection(true);*/
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void connect()
    {
        if(isConnected==true)
        {
//            Log.d("SOCKET MANAGER", "Socket running..");

        }
        else
        {
//            Log.d("SOCKET MANAGER", "Connecting..");
            try
            {
                mSocket.off(EVENT_NEW_MESSAGE, onNewMessage);
                mSocket.on(EVENT_NEW_MESSAGE, onNewMessage);
                mSocket.on(Socket.EVENT_CONNECT, onConnectMessage);
                mSocket.on(Socket.EVENT_DISCONNECT, onDisconnectMessage);
                mSocket.on(Socket.EVENT_PING,onPingEvent);
                mSocket.on(Socket.EVENT_PONG,onPongEvent);
                mSocket.on(NETWORK_CREATED, onNetworkJoinListener);
                mSocket.connect();
            }
            catch (Exception e)
            {
            }
        }


    }

    private Emitter.Listener onPongEvent = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
//            Log.d("SOCKET LETSCHAT MANAGER", "PONG>>");



        }
    };

    private Emitter.Listener onPingEvent = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
//            Log.d("SOCKET LETSCHAT MANAGER", "PING>>");



        }
    };


    private Emitter.Listener onNetworkJoinListener = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
//            Log.d("SOCKET MANAGER", "Joined To Network");

        }
    };


    private Emitter.Listener onConnectMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
//            Log.d("SOCKET MANAGER", "Connected" + Userid);
            JSONObject object = new JSONObject();
            if (Userid != null && Userid.length() > 0) {
                try {
                    object.put("user", Userid);
                    createRoom(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            isConnected = true;
        }
    };

    private Emitter.Listener onDisconnectMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
//            Log.d("SOCKET MANAGER", "DISCONNECTED");
            isConnected = false;
        }
    };

    public void disconnect() {
        try {
            mSocket.off(EVENT_NEW_MESSAGE, onNewMessage);
            mSocket.disconnect();
        } catch (Exception e) {
        }
    }


    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
//            Log.d("SOCKET MANAGER", "onNewMessage = " + args[0]);

            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (callBack != null) {
                            callBack.onSuccessListener(args[0]);
                        }
                    }
                });
            } else {
                if (callBack != null) {
                    callBack.onSuccessListener(args[0]);
                }
            }
        }
    };

    public void createRoom(Object userID) {
        if (userID == null) {
            return;
        }
        mSocket.emit(JOIN_NETWORK, userID);

    }

}
