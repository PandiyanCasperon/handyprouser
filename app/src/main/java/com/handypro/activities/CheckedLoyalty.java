package com.handypro.activities;

import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.handypro.R;

public class CheckedLoyalty {

    private Context mContext;
    private LinearLayout bacceptcall, brejectcall;
    private TextView alert_title;

    private Dialog dialog;
    private View view;
    private boolean isPositiveAvailable = false;
    private boolean isNegativeAvailable = false;


    public CheckedLoyalty(Context context) {
        this.mContext = context;

        //--------Adjusting Dialog width-----
        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen

        view = View.inflate(mContext, R.layout.checkdialogg, null);
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        alert_title = view.findViewById(R.id.custom_dialog_library_title_textview);


        brejectcall = view.findViewById(R.id.brejectcall);
        bacceptcall = view.findViewById(R.id.bacceptcall);


    }


    public void show() {

        dialog.show();
    }


    public void dismiss() {
        dialog.dismiss();
    }


    public void setDialogTitle(String title) {
        alert_title.setText(title);
    }

    public void setDialogTitle(CharSequence builders, TextView.BufferType type) {
        alert_title.setText(builders, type);
    }


    public void setCancelOnTouchOutside(boolean value) {
        dialog.setCanceledOnTouchOutside(value);
    }


    /*Action Button for Dialog*/
    public void setPositiveButton(String text, final View.OnClickListener listener) {

        isPositiveAvailable = true;


        bacceptcall.setOnClickListener(listener);
    }

    public void setNegativeButton(String text, final View.OnClickListener listener) {
        isNegativeAvailable = true;


        brejectcall.setOnClickListener(listener);
    }

}

