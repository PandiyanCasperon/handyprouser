package com.handypro.activities.bookingFlow;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.ParentQuestionPojo;
import com.handypro.Pojo.SessionParentPojo;
import com.handypro.Pojo.SubCategoryPojo;
import com.handypro.R;
import com.handypro.Widgets.undismisprogress;
import com.handypro.activities.AppointmentConfirmationPage;
import com.handypro.activities.HomeActivity;
import com.handypro.activities.LocationSearchActivity;
import com.handypro.activities.OrderDetailActivity;
import com.handypro.Pojo.acceptimage;
import com.handypro.activities.FirstTimePrivacy;
import com.handypro.activities.listhei;
import com.handypro.activities.login.LoginActivity;
import com.handypro.activities.navigationMenu.CardListActivity;
import com.handypro.activities.navigationMenu.bookimageadapter;
import com.handypro.activities.BookingConfirmationWithCraftsmanActivity;
import com.handypro.adapters.BookingConfirmationAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.core.socket.ChatMessageService;
import com.handypro.core.socket.ChatMessageServicech;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by CAS63 on 3/1/2018.
 */

public class BookingConfirmationPreviewActivity extends AppCompatActivity {
    private ArrayList<SubCategoryPojo> mySubCatArrList;
    private listhei mybookingCnfrmListVw;
    private RelativeLayout myBackLAY;
    LinearLayout downlay;
    String apartmentno="";
    TextView totalfare;
    String approx_taskhours = "1";
    String task_id = "";
    private BookingConfirmationAdapter myAdapter;
    private ConnectionDetector myConnectionManager;
    private SharedPreference mySession;
    //private ProgressDialogcreated myDialog;
    private TextView myTotalFareTXT, TextViewJobAddress, TextViewJobLocationTitle;
    private CustomButton mySubmitBookingBTN;
    String bookingid = "";
    RecyclerView beforerecycler_view;
    private ArrayList<String> mySelectedSubCatIdArr;
    String storeavailablecategoryid = "", tryv = "1";
    String categornamedis;
    private int SHEDULE_APPOINTMENT_FLAG = 2024;
    String maxtry = "";
    ImageView editcategory;
    TextView TimeValue, jobdetailsvalue, DateValue;
    int openbox = 0;
    TextView youruploadedimage;
    private undismisprogress myDialog;
    Handler handler = new Handler();
    private int PLACE_SEARCH_REQUEST_CODE = 1029;
    ArrayList<SessionParentPojo> mySessionParentInfoList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_confirm);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("showpoupup", 0); // 0 - for private mode
        SharedPreferences.Editor editor = getcard.edit();
        editor.putString("show", "0");
        editor.apply();

        classAndWidgetsInit();
        mySelectedSubCatIdArr = new ArrayList<String>();
        if (myConnectionManager.isConnectingToInternet()) {
            getCategoryDetailValues(BookingConfirmationPreviewActivity.this);
        } else {
            HNDHelper.showErrorAlert(BookingConfirmationPreviewActivity.this, getResources().getString(R.string.nointernet_text));
        }
    }

    private void classAndWidgetsInit() {
        mySession = new SharedPreference(BookingConfirmationPreviewActivity.this);
        mySessionParentInfoList = mySession.getSessionParentDetails();
        myConnectionManager = new ConnectionDetector(BookingConfirmationPreviewActivity.this);

        mySubCatArrList = new ArrayList<SubCategoryPojo>();
        TimeValue = findViewById(R.id.TimeValue);
        DateValue = findViewById(R.id.DateValue);
        jobdetailsvalue = findViewById(R.id.jobdetailsvalue);
        beforerecycler_view = findViewById(R.id.beforerecycler_view);
        youruploadedimage = findViewById(R.id.youruploadedimage);
        totalfare = findViewById(R.id.totalfare);
        mybookingCnfrmListVw = findViewById(R.id.activity_booking_confirm_listView);
        myBackLAY = findViewById(R.id.activity_booking_confirm_LAY_parent);
        myTotalFareTXT = findViewById(R.id.activity_booking_confirm_total_fare_TXT);
        mySubmitBookingBTN = findViewById(R.id.activity_booking_confirm_bottom_BTN);
        downlay = findViewById(R.id.downlay);
        editcategory = findViewById(R.id.editcategory);
        TextViewJobAddress = findViewById(R.id.TextViewJobAddress);
        TextViewJobLocationTitle = findViewById(R.id.TextViewJobLocationTitle);



        /*File fileTarget = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + BookingConfirmationActivity.this.getApplicationContext().getPackageName()
                + "/Files/Compressed");

        File[] files = fileTarget.listFiles();
        List<acceptimage> movieList = new ArrayList<>();
        movieList.clear();
        if (files != null) {
            for (File file : files) {
                acceptimage movie = new acceptimage(file.getAbsolutePath());
                movieList.add(movie);
            }
        }*/
        List<acceptimage> movieList = new ArrayList<>();
        SharedPreferences jobdetailpage;
        jobdetailpage = getApplicationContext().getSharedPreferences("jobdetailpage", 0);
        String taskimage = jobdetailpage.getString("taskimage", "");
        JSONArray aJsonArr = null;
        try {
            aJsonArr = new JSONArray(taskimage);
            for (int a = 0; a < aJsonArr.length(); a++) {
                acceptimage movie = new acceptimage(aJsonArr.getString(a));
                movieList.add(movie);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (movieList.size() > 0) {
            beforerecycler_view.setVisibility(View.VISIBLE);
            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
            bookimageadapter mAdapter = new bookimageadapter(movieList, getApplicationContext());
            beforerecycler_view.setLayoutManager(layoutManager);
            beforerecycler_view.setAdapter(mAdapter);
        } else {
            beforerecycler_view.setVisibility(View.GONE);
            youruploadedimage.setVisibility(View.GONE);
        }

        SharedPreferences pickupdate;
        pickupdate = getApplicationContext().getSharedPreferences("scheduleapp", 0);
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "EEEE MMMM  dd, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.getDefault());

        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(pickupdate.getString("date", ""));
            str = outputFormat.format(date);

            String s = pickupdate.getString("time", "");
            DateFormat f1 = new SimpleDateFormat("HH:mm", Locale.getDefault()); //HH for hour of the day (0 - 23)
            Date d = f1.parse(s);
            DateFormat f2 = new SimpleDateFormat("hh:mm a", Locale.getDefault());
            assert d != null;

            SpannableStringBuilder builders = new SpannableStringBuilder();
            SpannableString redSpannable = new SpannableString(str);
            redSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.darkcolor_1)), 0, str.length(), 0);
            builders.append(redSpannable);

            SpannableStringBuilder builders1 = new SpannableStringBuilder();
            SpannableString whiteSpannable = new SpannableString(f2.format(d).toLowerCase() + " ");
            whiteSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.darkcolor_1)), 0, (f2.format(d).toLowerCase() + " ").length(), 0);
            builders1.append(whiteSpannable);

            String string3 = "(One Hour Arrival Time)";
            SpannableString redSpannable1 = new SpannableString(string3);
            redSpannable1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, string3.length(), 0);
            builders1.append(redSpannable1);

            TimeValue.setText(builders);
            DateValue.setText(builders1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        jobdetailpage = getApplicationContext().getSharedPreferences("jobdetailpage", 0);
        jobdetailsvalue.setText("Description: " + jobdetailpage.getString("title", "") + "\n" + "What’s important to you: " + jobdetailpage.getString("description", ""));

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        editcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("beforedismiss", 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("before", "1");
                editor.apply();
                finish();
            }
        });
        if (!mySession.getLogInStatus()) {
            mySubmitBookingBTN.setText(getResources().getString(R.string.class_booking_confirmation_login));
        } else {
            mySubmitBookingBTN.setText(getResources().getString(R.string.activity_add_photo_upload_confirm));
        }

        mySubmitBookingBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mySession.getLogInStatus()) {
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("logintoconform", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("lo", "1");
                    editor.apply();

                    Intent aProfIntent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(aProfIntent);
                } else {
                    if (myConnectionManager.isConnectingToInternet()) {

                        if (!mySession.GetGPSZIPCode().isEmpty() || !mySession.GetFullAddress().isEmpty()) {
                            if (mySession.GetZIPCode().equals(mySession.GetGPSZIPCode())) {

                                final Dialog myGetZipCodeDialog = new Dialog(BookingConfirmationPreviewActivity.this);
                                myGetZipCodeDialog.getWindow();
                                myGetZipCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                myGetZipCodeDialog.setContentView(R.layout.alert_dialog_zipcode_confirm_job);
                                myGetZipCodeDialog.setCanceledOnTouchOutside(true);
                                myGetZipCodeDialog.getWindow().getAttributes().windowAnimations = R.style.animations_photo_Picker;
                                myGetZipCodeDialog.show();
                                myGetZipCodeDialog.getWindow().setGravity(Gravity.CENTER);

                                TextView TextViewYes = myGetZipCodeDialog.findViewById(R.id.TextViewYes);
                                TextView TextViewNo = myGetZipCodeDialog.findViewById(R.id.TextViewNo);
                                TextView TextViewAddress = myGetZipCodeDialog.findViewById(R.id.TextViewAddress);

                                TextViewAddress.setText(mySession.GetFullAddress());

                                TextViewNo.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        myGetZipCodeDialog.dismiss();
                                        Intent aLocationSearchIntent = new Intent(BookingConfirmationPreviewActivity.this, LocationSearchActivity.class);
                                        startActivityForResult(aLocationSearchIntent, PLACE_SEARCH_REQUEST_CODE);
                                    }
                                });

                                TextViewYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        myGetZipCodeDialog.dismiss();
                                        SharedPreferences prefdd = getApplicationContext().getSharedPreferences("termsconditionn", MODE_PRIVATE);
                                        if (prefdd.getString("termsandconditions", "").equals("1")) {
                                            Dialog dialog = new Dialog(BookingConfirmationPreviewActivity.this);
                                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            dialog.setContentView(R.layout.requestpage);
                                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                            dialog.setCancelable(false);
                                            dialog.setCanceledOnTouchOutside(false);
                                            postRequestConfirmBooking(BookingConfirmationPreviewActivity.this, dialog);
                                        } else {
                                            SharedPreferences getcard;
                                            getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
                                            String myzipcode = getcard.getString("zipcode", "");

                                            Intent intent = new Intent(BookingConfirmationPreviewActivity.this, FirstTimePrivacy.class);
                                            intent.putExtra("url", myzipcode);
                                            intent.putExtra("type", "reg");
                                            intent.putExtra("classs", "book");
                                            intent.putExtra("header", getResources().getString(R.string.activity_signup_TXT_signup_terms_use));
                                            startActivity(intent);
                                        }
                                    }
                                });
                            } else {
                                final Dialog myGetZipCodeDialog = new Dialog(BookingConfirmationPreviewActivity.this);
                                myGetZipCodeDialog.getWindow();
                                myGetZipCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                myGetZipCodeDialog.setContentView(R.layout.alert_dialog_error_zipcode_confirm_job);
                                myGetZipCodeDialog.setCanceledOnTouchOutside(true);
                                myGetZipCodeDialog.getWindow().getAttributes().windowAnimations = R.style.animations_photo_Picker;
                                myGetZipCodeDialog.show();
                                myGetZipCodeDialog.getWindow().setGravity(Gravity.CENTER);

                                TextView TextViewProceed = myGetZipCodeDialog.findViewById(R.id.TextViewProceed);
                                TextView TextViewCancel = myGetZipCodeDialog.findViewById(R.id.TextViewCancel);
                                TextView TextViewAddress = myGetZipCodeDialog.findViewById(R.id.TextViewAddress);
                                TextViewAddress.setText(getResources().getString(R.string.alert_dialog_address_dont_match));

                                TextViewCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        myGetZipCodeDialog.dismiss();
                                    }
                                });

                                TextViewProceed.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        myGetZipCodeDialog.dismiss();
                                        Intent aLocationSearchIntent = new Intent(BookingConfirmationPreviewActivity.this, LocationSearchActivity.class);
                                        startActivityForResult(aLocationSearchIntent, PLACE_SEARCH_REQUEST_CODE);
                                    }
                                });
                            }
                        } else {
                            final Dialog addressEmptyDialog = new Dialog(BookingConfirmationPreviewActivity.this);
                            addressEmptyDialog.getWindow();
                            addressEmptyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            addressEmptyDialog.setContentView(R.layout.alert_dialog_error_zipcode_confirm_job);
                            addressEmptyDialog.setCanceledOnTouchOutside(true);
                            addressEmptyDialog.getWindow().getAttributes().windowAnimations = R.style.animations_photo_Picker;
                            addressEmptyDialog.show();
                            addressEmptyDialog.getWindow().setGravity(Gravity.CENTER);

                            TextView TextViewProceed = addressEmptyDialog.findViewById(R.id.TextViewProceed);
                            TextView TextViewCancel = addressEmptyDialog.findViewById(R.id.TextViewCancel);
                            TextView TextViewAddress = addressEmptyDialog.findViewById(R.id.TextViewAddress);
                            TextViewAddress.setText(getResources().getString(R.string.alert_dialog_address_empty_address));

                            TextViewCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    addressEmptyDialog.dismiss();
                                }
                            });

                            TextViewProceed.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    addressEmptyDialog.dismiss();
                                    Intent aLocationSearchIntent = new Intent(BookingConfirmationPreviewActivity.this, LocationSearchActivity.class);
                                    startActivityForResult(aLocationSearchIntent, PLACE_SEARCH_REQUEST_CODE);
                                }
                            });
                        }

                    } else {
                        if (myDialog.isShowing()) {
                            myDialog.dismiss();
                        }
                        HNDHelper.showErrorAlert(BookingConfirmationPreviewActivity.this, getResources().getString(R.string.nointernet_text));
                    }
                }
            }
        });
    }

    private void setDataInAdapter() {
        mybookingCnfrmListVw.setEnabled(false);
        myAdapter = new BookingConfirmationAdapter(BookingConfirmationPreviewActivity.this, mySubCatArrList);
        mybookingCnfrmListVw.setAdapter(myAdapter);
    }

    private void postRequestConfirmBooking(final Context aContext, final Dialog dialog) {
        myDialog = new undismisprogress(aContext);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        mySubmitBookingBTN.setEnabled(false);
        mySubmitBookingBTN.setClickable(false);

        HashMap<String, String> params = new HashMap<>();
        SharedPreferences jobdetailpage;
        jobdetailpage = getApplicationContext().getSharedPreferences("jobdetailpage", 0); // 0 - for private mode
        String title = jobdetailpage.getString("title", "");
        String description = jobdetailpage.getString("description", "");
   //     params.put("instruction", title);
        params.put("description", description);
        params.put("booking_id", bookingid);
        params.put("booking_source", "app");
        SharedPreferences getcardss;
        getcardss = getApplicationContext().getSharedPreferences("storecraftmanid", 0); // 0 - for private mode

        SharedPreferences getcards;
        getcards = getApplicationContext().getSharedPreferences("selectresponse", 0);
        String laterornow = getcards.getString("laterornow", "");
        if (!laterornow.equals("now")) {
            params.put("taskerid", getcardss.getString("taskerid", ""));
        }
        params.put("try", "" + tryv);

        if (storeavailablecategoryid.endsWith(",")) {
            storeavailablecategoryid = storeavailablecategoryid.substring(0, storeavailablecategoryid.length() - 1);
        }

        if (storeavailablecategoryid.contains(",")) {
            String[] catva = storeavailablecategoryid.split(",");
            System.out.println("----arraysize" + catva.length);
            mySelectedSubCatIdArr = new ArrayList<String>();
            Collections.addAll(mySelectedSubCatIdArr, catva);
            try {
                JSONArray aJsonArrs = new JSONArray(mySelectedSubCatIdArr);
                for (int a = 0; a < aJsonArrs.length(); a++) {
                    params.put("subcategory[" + a + "]", aJsonArrs.getString(a));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            mySelectedSubCatIdArr = new ArrayList<String>();
            mySelectedSubCatIdArr.add(storeavailablecategoryid);
            try {
                JSONArray aJsonArrs = new JSONArray(mySelectedSubCatIdArr);
                for (int a = 0; a < aJsonArrs.length(); a++) {
                    params.put("subcategory[" + a + "]", aJsonArrs.getString(a));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (mySessionParentInfoList.size() > 0) {
            for (int d = 0; d < mySessionParentInfoList.size(); d++) {
                params.put("question[" + d + "][id]", mySessionParentInfoList.get(d).getSessionParentId());
                for (int u = 0; u < mySessionParentInfoList.get(d).getaPojo().size(); u++) {
                    String aSelectedStr = "";
                    for (int n = 0; n < mySessionParentInfoList.get(d).getaPojo().get(u).getAnswerpojo().size(); n++) {
                        if (mySessionParentInfoList.get(d).getaPojo().get(u).getAnswerpojo().get(n).isAnswerselected()) {
                            if (aSelectedStr.equalsIgnoreCase("")) {
                                aSelectedStr = mySessionParentInfoList.get(d).getaPojo().get(u).getAnswerpojo().get(n).getAnswertitle();
                            } else {
                                aSelectedStr = aSelectedStr + "," + mySessionParentInfoList.get(d).getaPojo().get(u).getAnswerpojo().get(n).getAnswertitle();
                            }
                        }
                    }
                    params.put("question[" + d + "][answer][" + u + "]", mySessionParentInfoList.get(d).getaPojo().get(u).getQuestionAnsView().replace("{{answer}}",
                            aSelectedStr));
                }
            }
        }

        SharedPreferences pickup;
        pickup = getApplicationContext().getSharedPreferences("scheduleapp", 0); // 0 - for private mode
        params.put("pickup_date", pickup.getString("date", ""));
        params.put("pickup_time", pickup.getString("time", ""));

        SharedPreferences zip;
        zip = getApplicationContext().getSharedPreferences("zipcode", 0);
        params.put("lat", zip.getString("Latitude", ""));
        params.put("long", zip.getString("Longitude", ""));
        params.put("zipcode", zip.getString("code", ""));
        params.put("FullAddress", zip.getString("FullAddress", ""));
        params.put("user_id", mySession.getUserDetails().getUserId());
        params.put("marketing", jobdetailpage.getString("marketing", ""));
        String taskimage = jobdetailpage.getString("taskimage", "");
        JSONArray aJsonArr = null;
        try {
            aJsonArr = new JSONArray(taskimage);
            for (int a = 0; a < aJsonArr.length(); a++) {
                System.out.println("----taskimages" + aJsonArr.getString(a));
                Bitmap src = BitmapFactory.decodeFile(aJsonArr.getString(a));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                src.compress(Bitmap.CompressFormat.PNG, 100, baos);
                //  byte[] data = baos.toByteArray();
                String imgString = Base64.encodeToString(getBytesFromBitmap(src),
                        Base64.NO_WRAP);
                params.put("taskimages[" + a + "]", imgString);
                System.out.println("----imgString" + imgString);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(ServiceConstant.bookingconform, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("mobile/app/book-job-new", response);
                System.out.println("-------------get Confirm Category list Response----------------" + response);
                String booklaterornow = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        mySession.putSessionParentPojo(new ArrayList<SessionParentPojo>());
                        JSONObject aRespObj = object.getJSONObject("response");
                        booklaterornow = aRespObj.getString("task_type");

                        if (booklaterornow.equalsIgnoreCase("later")) {
                            task_id = aRespObj.getString("task_id");
                            String idforbooking = aRespObj.getString("task_id");
                            SharedPreferences getcard;
                            getcard = getApplicationContext().getSharedPreferences("selectresponse", 0); // 0 - for private mode
                            SharedPreferences.Editor editor = getcard.edit();
                            editor.putString("res", "" + response);
                            editor.putString("bookingid", "" + idforbooking);
                            editor.apply();
                            if (myDialog.isShowing()) {
                                myDialog.dismiss();
                            }
                            SharedPreferences prefss = getApplicationContext().getSharedPreferences("categorynams", 0); // 0 - for private mode
                            SharedPreferences.Editor editors = prefss.edit();
                            editors.putString("names", "" + categornamedis);
                            editors.apply();
                            SharedPreferences getcardss;
                            getcardss = getApplicationContext().getSharedPreferences("storecraftmanid", 0); // 0 - for private mode
                            String taskerid = getcardss.getString("taskerid", "");
                            postRequestConfirmBooking(BookingConfirmationPreviewActivity.this, taskerid);

                        }
                        if (booklaterornow.equalsIgnoreCase("now")) {
                            if (object.has("jobstatus")) {
                                String idforbooking = object.getString("jobstatus");
                                int jobstate = Integer.parseInt(idforbooking);
                                if (jobstate >= 2) {
                                    SharedPreferences showpopup;
                                    showpopup = getApplicationContext().getSharedPreferences("showpoupup", 0); // 0 - for private mode
                                    SharedPreferences.Editor editorshowpopup = showpopup.edit();
                                    editorshowpopup.putString("show", "1");
                                    editorshowpopup.apply();
                                }
                            }
                            String idforbooking = aRespObj.getString("bookingid");
                            String next_try = aRespObj.getString("next_try");
                            bookingid = idforbooking;
                            tryv = next_try;
                            String response_time;
                            int responsetimer = 1000;
                            if (aRespObj.has("response_time")) {
                                if (aRespObj.getString("response_time").length() >= 1) {
                                    //  response_time=aRespObj.getString("response_time")+"000";
                                    response_time = "1000";
                                } else {
                                    response_time = "1000";
                                }
                            } else {
                                response_time = "1000";
                            }
                            responsetimer = Integer.parseInt(response_time);
                           /* if (myDialog.isShowing()) {
                                myDialog.dismiss();
                            }*/
                            if (aRespObj.has("maxtry")) {
                                maxtry = aRespObj.getString("maxtry");
                                if (myConnectionManager.isConnectingToInternet()) {
                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            //Do something after 15second
                                            Dialog dialog = new Dialog(BookingConfirmationPreviewActivity.this);
                                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            dialog.setContentView(R.layout.requestpage);
                                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                            dialog.setCancelable(false);
                                            dialog.setCanceledOnTouchOutside(false);
                                            postRequestConfirmBooking(BookingConfirmationPreviewActivity.this, dialog);
                                        }
                                    }, responsetimer);
                                } else {
                                    HNDHelper.showErrorAlert(BookingConfirmationPreviewActivity.this, getResources().getString(R.string.nointernet_text));
                                }
                            } else {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                String bookingid = aRespObj.getString("bookingid");
                            }
                        }
                    } else {
                        JSONObject aRespObj = object.getJSONObject("response");
                        JSONObject datas = object.getJSONObject("datas");
                        String statustext = "";

                        if (aRespObj.has("card_status")) {
                            statustext = aRespObj.getString("status_text");
                            Intent intent = new Intent(getApplicationContext(), CardListActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        } else {
                            if (aRespObj.has("status_text")) {
                                statustext = aRespObj.getString("status_text");
                            } else {
                                statustext = "Tasker not available";
                            }

                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }

                            if (maxtry.equalsIgnoreCase("")) {
                                HNDHelper.showResponseErrorAlert(aContext, getResources().getString(R.string.class_booking_confirmation_craftman));
                            } else {
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("categorynams", 0); // 0 - for private mode
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("names", "" + categornamedis);
                                editor.apply();

                                Intent aSheduleAppointmntIntent = new Intent(BookingConfirmationPreviewActivity.this, AppointmentConfirmationPage.class);
                                aSheduleAppointmntIntent.putExtra("IntentMessage", getResources().getString(R.string.class_booking_confirmation_message));
                                aSheduleAppointmntIntent.putExtra("IntentJobID", bookingid);
                                aSheduleAppointmntIntent.putExtra("approx_taskhours", approx_taskhours);
                                aSheduleAppointmntIntent.putExtra("IntentServiceType", getResources().getString(R.string.class_booking_confirmation_cost));
                                aSheduleAppointmntIntent.putExtra("totalfare", myTotalFareTXT.getText().toString());
                                SharedPreferences pickup;
                                pickup = getApplicationContext().getSharedPreferences("scheduleapp", 0); // 0 - for private mode
                                aSheduleAppointmntIntent.putExtra("IntentOrderDate", pickup.getString("date", ""));
                                aSheduleAppointmntIntent.putExtra("IntentTime", pickup.getString("time", ""));
                                SharedPreferences getcard;
                                getcard = getApplicationContext().getSharedPreferences("jobdetailpage", 0); // 0 - for private mode
                                aSheduleAppointmntIntent.putExtra("IntentDescription", getcard.getString("description", ""));

                                JSONObject JSONObject_taskdata = datas.getJSONObject("bookdata");
                                aSheduleAppointmntIntent.putExtra("task_title", JSONObject_taskdata.getString("task_title"));
                                aSheduleAppointmntIntent.putExtra("task_description", JSONObject_taskdata.getString("task_description"));
                                aSheduleAppointmntIntent.putExtra("task_images", datas.getJSONObject("bookdata").getJSONObject("taskimages").getJSONArray("user").toString());
                                startActivityForResult(aSheduleAppointmntIntent, SHEDULE_APPOINTMENT_FLAG);
                            }
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                mySubmitBookingBTN.setEnabled(true);
                mySubmitBookingBTN.setClickable(true);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void getCategoryDetailValues(Context aContext) {
        myDialog = new undismisprogress(BookingConfirmationPreviewActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParam = new HashMap<>();
        try {
            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("scheduleapp", 0); // 0 - for private mode
            String categoryvalueid = getcard.getString("catgeoryid", "");
            if (categoryvalueid.contains(",")) {
                String[] catva = categoryvalueid.split(",");
                mySelectedSubCatIdArr = new ArrayList<String>();
                Collections.addAll(mySelectedSubCatIdArr, catva);
                try {
                    JSONArray aJsonArr = new JSONArray(mySelectedSubCatIdArr);
                    for (int a = 0; a < aJsonArr.length(); a++) {
                        jsonParam.put("subcategory[" + a + "]", aJsonArr.getString(a));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                mySelectedSubCatIdArr = new ArrayList<String>();
                mySelectedSubCatIdArr.add(categoryvalueid);
                try {
                    JSONArray aJsonArr = new JSONArray(mySelectedSubCatIdArr);
                    for (int a = 0; a < aJsonArr.length(); a++) {
                        jsonParam.put("subcategory[" + a + "]", aJsonArr.getString(a));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            SharedPreferences zipcode;
            zipcode = getApplicationContext().getSharedPreferences("zipcode", 0);
            String code = zipcode.getString("code", "");
            jsonParam.put("zipcode", code);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(ServiceConstant.CategoryDetail, Request.Method.POST, jsonParam, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("---------Categories detail response------------" + response);
                Log.e("response", response);
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("1")) {
                        if (mySession.GetFullAddress().isEmpty() || mySession.GetFullAddress().equals("Address")) {
                            TextViewJobLocationTitle.setText(getResources().getText(R.string.job_location_zip_code));
                            TextViewJobAddress.setText(mySession.GetZIPCode());
                        } else {
                            TextViewJobLocationTitle.setText(getResources().getText(R.string.job_location));
                            TextViewJobAddress.setText(mySession.GetFullAddress());
                        }
                        JSONObject aRespObj = object.getJSONObject("response");
                        JSONArray aMainArr = aRespObj.getJSONArray("category");
                        if (aRespObj.has("approx_taskhours")) {
                            approx_taskhours = aRespObj.getString("approx_taskhours");
                        }
                        if (aRespObj.getString("overall_fare").equals("0")) {
                            myTotalFareTXT.setVisibility(View.GONE);
                            totalfare.setVisibility(View.GONE);
                            myTotalFareTXT.setText("0");
                        } else {
                            myTotalFareTXT.setVisibility(View.VISIBLE);
                            totalfare.setVisibility(View.VISIBLE);
                            if (mySession.getLogInStatus() == false) {
                                myTotalFareTXT.setText("$" + aRespObj.getString("overall_fare"));
                            } else {
                                myTotalFareTXT.setText(mySession.getCurrencySymbol() + aRespObj.getString("overall_fare"));
                            }
                        }
                        categornamedis = "";
                        for (int a = 0; a < aMainArr.length(); a++) {
                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);
                            if (aSubCatObj.getString("availability").equals("1")) {
                                SubCategoryPojo aSubCatPojo = new SubCategoryPojo();
                                aSubCatPojo.setSubCategoryJobType(aSubCatObj.getString("job_type"));
                                storeavailablecategoryid += aSubCatObj.getString("id") + ",";
                                categornamedis += aSubCatObj.getString("cat_name") + ",";
                                aSubCatPojo.setJoborestimate(aSubCatObj.getString("whatsincluded"));
                                aSubCatPojo.setSubCategoryName(aSubCatObj.getString("cat_name"));
                                aSubCatPojo.setSubCategoryId(aSubCatObj.getString("id"));

                                aSubCatPojo.setSubCategoryActiveIcon(aSubCatObj.getString("active_icon"));
                                if (aSubCatObj.has("fare")) {
                                    if (aSubCatObj.getJSONObject("fare").has("jobamount")) {
                                        if (mySession.getLogInStatus() == false) {
                                            aSubCatPojo.setSubCategoryHourlyRate("$" + aSubCatObj.getJSONObject("fare").getString("jobamount"));
                                        } else {
                                            aSubCatPojo.setSubCategoryHourlyRate(mySession.getCurrencySymbol() + aSubCatObj.getJSONObject("fare").getString("jobamount"));
                                        }
                                    } else {
                                        if (mySession.getLogInStatus() == false) {
                                            aSubCatPojo.setSubCategoryHourlyRate("$" + aSubCatObj.getJSONObject("fare").getString("estimateamount"));
                                        } else {
                                            aSubCatPojo.setSubCategoryHourlyRate(mySession.getCurrencySymbol() + aSubCatObj.getJSONObject("fare").getString("estimateamount"));
                                        }
                                    }
                                }
                                mySubCatArrList.add(aSubCatPojo);
                            }
                        }
                        if (storeavailablecategoryid.length() < 4) {
                            downlay.setVisibility(View.GONE);
                            HNDHelper.showErrorAlert(BookingConfirmationPreviewActivity.this, getResources().getString(R.string.catinpincode));
                        } else {
                            downlay.setVisibility(View.VISIBLE);
                            setDataInAdapter();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mySession.getLogInStatus()) {
            mySubmitBookingBTN.setText(getResources().getString(R.string.class_booking_confirmation_login));
        } else {
            mySubmitBookingBTN.setText(getResources().getString(R.string.activity_job_detail_BTN_confirm));
        }
        handler.post(timedTask);
        if (mySession.getLogInStatus() == false) {

        } else {
            try {
                Intent intent = new Intent(BookingConfirmationPreviewActivity.this, ChatMessageServicech.class);
                startService(intent);
                Intent intenzt = new Intent(BookingConfirmationPreviewActivity.this, ChatMessageService.class);
                startService(intenzt);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Runnable timedTask = new Runnable() {
        public void run() {
            SharedPreferences showpoupup = getApplicationContext().getSharedPreferences("showpoupup", 0);
            String show = showpoupup.getString("show", "");
            if (show.equalsIgnoreCase("1")) {
                if (openbox == 1) {
                } else {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("beforedismiss", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("before", "0");
                    editor.apply();
                    openbox = 1;
                    try {
                        handler.removeCallbacks(timedTask);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }
            handler.postDelayed(timedTask, 2000);
        }
    };

    private void postRequestConfirmBooking(final Context aContext, String taskerid) {
        myDialog = new undismisprogress(aContext);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("booking_source", "app");
        jsonParams.put("taskid", task_id);
        jsonParams.put("taskerid", taskerid);
        jsonParams.put("apartmentno", apartmentno);

        SharedPreferences zip;
        zip = getApplicationContext().getSharedPreferences("zipcode", 0);
        jsonParams.put("location", zip.getString("address", ""));
        jsonParams.put("tasklat", zip.getString("Latitude", ""));
        jsonParams.put("tasklng", zip.getString("Longitude", ""));

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(ServiceConstant.BookJob, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Confirm Booking Response----------------" + response);
                mySubmitBookingBTN.setEnabled(true);
                mySubmitBookingBTN.setClickable(true);
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        JSONObject insideres = new JSONObject(object.getString("response"));
                        SharedPreferences pickup;
                        pickup = getApplicationContext().getSharedPreferences("scheduleapp", 0); // 0 - for private mode

                        Intent aSheduleAppointmntIntent = new Intent(BookingConfirmationPreviewActivity.this, BookingConfirmationWithCraftsmanActivity.class);
                        aSheduleAppointmntIntent.putExtra("IntentMessage", getResources().getString(R.string.class_booking_confirmation_confirmed));
                        aSheduleAppointmntIntent.putExtra("IntentJobID", insideres.getString("job_id"));
                        aSheduleAppointmntIntent.putExtra("approx_taskhours", approx_taskhours);
                        aSheduleAppointmntIntent.putExtra("IntentOrderDate", pickup.getString("date", ""));
                        aSheduleAppointmntIntent.putExtra("IntentTime", pickup.getString("time", ""));
                        aSheduleAppointmntIntent.putExtra("IntentServiceType", insideres.getString("note"));
                        aSheduleAppointmntIntent.putExtra("IntentDescription", insideres.getString("description"));
                        aSheduleAppointmntIntent.putExtra("Intenttell", insideres.getString("description"));
                        aSheduleAppointmntIntent.putExtra("totalfare", myTotalFareTXT.getText().toString());

                        JSONObject JSONObject_taskdata = insideres.getJSONObject("taskdata");
                        aSheduleAppointmntIntent.putExtra("task_title", JSONObject_taskdata.getString("task_title"));
                        aSheduleAppointmntIntent.putExtra("task_description", JSONObject_taskdata.getString("task_description"));
                        aSheduleAppointmntIntent.putExtra("task_images", JSONObject_taskdata.getJSONObject("taskimages").getJSONArray("user").toString());

                        SharedPreferences zip;
                        zip = getApplicationContext().getSharedPreferences("storecraftmanid", 0);
                        aSheduleAppointmntIntent.putExtra("taskername", zip.getString("taskername", ""));
                        aSheduleAppointmntIntent.putExtra("taskerimage", zip.getString("taskerimage", ""));
                        aSheduleAppointmntIntent.putExtra("taskerreviews", zip.getString("taskerreviews", ""));
                        aSheduleAppointmntIntent.putExtra("taskexperience", zip.getString("taskexperience", ""));
                        aSheduleAppointmntIntent.putExtra("taskeraddress", zip.getString("taskeraddress", ""));
                        aSheduleAppointmntIntent.putExtra("taskerkm", zip.getString("taskerkm", ""));
                        aSheduleAppointmntIntent.putExtra("taskeroverallreviews", zip.getString("taskeroverallreviews", ""));
                        startActivityForResult(aSheduleAppointmntIntent, SHEDULE_APPOINTMENT_FLAG);
                        if (myDialog.isShowing()) {
                            myDialog.dismiss();
                        }
                    } else {
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                mySubmitBookingBTN.setEnabled(true);
                mySubmitBookingBTN.setClickable(true);
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("--------------onActivityResult requestCode----------------" + requestCode);
        if (data != null) {

            if ((requestCode == PLACE_SEARCH_REQUEST_CODE)) {
                if (resultCode == RESULT_OK) {
                    String myLatSTR, myLongSTR;
                    String mySelectedLocation = data.getStringExtra("Selected_Location");
                    final String myZipCodeSTR = data.getStringExtra("ZipCode");
                    myLatSTR = data.getStringExtra("Selected_Latitude");
                    myLongSTR = data.getStringExtra("Selected_Longitude");
                    apartmentno =  data.getStringExtra("apartmetno");

                    System.out.println("selectedLatitude---------" + myLatSTR);
                    System.out.println("selectedLongitude---------" + myLongSTR);
                    System.out.println("selectedLocation----------" + mySelectedLocation);
                    System.out.println("houseNo-----------" + data.getStringExtra("HouseNo"));
                    System.out.println("city-----------" + data.getStringExtra("City"));
                    System.out.println("postalCode-----------" + data.getStringExtra("ZipCode"));
                    System.out.println("location-----------" + data.getStringExtra("Location"));


                    if (mySelectedLocation != null && !mySelectedLocation.isEmpty() && myZipCodeSTR != null && !myZipCodeSTR.isEmpty()) {
//                        mySession.SetLocationDetails(myZipCodeSTR, myLatSTR, myLongSTR, mySelectedLocation);
                        mySession.SetGPSZIPCode(myZipCodeSTR);
                        TextViewJobLocationTitle.setText(getResources().getText(R.string.job_location));
                        TextViewJobAddress.setText(apartmentno+" "+mySelectedLocation);
                        if (!mySession.GetZIPCode().equals(mySession.GetGPSZIPCode())) {
                            final Dialog myGetZipCodeDialog = new Dialog(BookingConfirmationPreviewActivity.this);
                            myGetZipCodeDialog.getWindow();
                            myGetZipCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            myGetZipCodeDialog.setContentView(R.layout.alert_dialog_error_zipcode_confirm_job);
                            myGetZipCodeDialog.setCanceledOnTouchOutside(true);
                            myGetZipCodeDialog.getWindow().getAttributes().windowAnimations = R.style.animations_photo_Picker;
                            myGetZipCodeDialog.show();
                            myGetZipCodeDialog.getWindow().setGravity(Gravity.CENTER);

                            TextView TextViewProceed = myGetZipCodeDialog.findViewById(R.id.TextViewProceed);
                            TextView TextViewCancel = myGetZipCodeDialog.findViewById(R.id.TextViewCancel);
                            TextView TextViewAddress = myGetZipCodeDialog.findViewById(R.id.TextViewAddress);

                            TextViewAddress.setText(getResources().getString(R.string.reselected_address_not_match));
                            TextViewCancel.setText(getResources().getString(R.string.selected_address));

                            TextViewCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    myGetZipCodeDialog.dismiss();
                                    Intent aLocationSearchIntent = new Intent(BookingConfirmationPreviewActivity.this, LocationSearchActivity.class);
                                    startActivityForResult(aLocationSearchIntent, PLACE_SEARCH_REQUEST_CODE);
                                }
                            });

                            TextViewProceed.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    myGetZipCodeDialog.dismiss();
                                    mySession.SetZIPCode(myZipCodeSTR);
                                    mySession.SetFlowFrom("JobConfirmPage");
                                    Intent aLocationSearchIntent = new Intent(BookingConfirmationPreviewActivity.this, HomeActivity.class);
                                    startActivityForResult(aLocationSearchIntent, PLACE_SEARCH_REQUEST_CODE);
                                }
                            });
                        } else {
                            if(apartmentno.equals(""))
                            {
                                mySession.SetLocationDetails(myZipCodeSTR, myLatSTR, myLongSTR, mySelectedLocation);

                            }
                            else
                            {
                                mySession.SetLocationDetails(myZipCodeSTR, myLatSTR, myLongSTR, apartmentno+" "+mySelectedLocation);

                            }
                          }
                    } else {
                        if (myLatSTR.isEmpty() || myLongSTR.isEmpty()) {
                            TextViewJobLocationTitle.setText(getResources().getText(R.string.job_location_zip_code));
                            TextViewJobAddress.setText(mySession.GetZIPCode());
                            Toast.makeText(BookingConfirmationPreviewActivity.this, "Unable to load your location please try again selecting new location in map.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }
    }
}
