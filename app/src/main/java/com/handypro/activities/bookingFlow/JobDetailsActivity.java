package com.handypro.activities.bookingFlow;

/**
 * Created by user127 on 19-02-2018.
 */

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.icu.text.DisplayContext;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.ParentQuestionPojo;
import com.handypro.Pojo.SessionParentPojo;
import com.handypro.Pojo.SubCategoryPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreatedforfrontpage;
import com.handypro.activities.MyGridView;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.TypefaceSpan;
import com.handypro.utils.CustomCompressClass;
import com.handypro.volley.ServiceRequest;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.Picasso;
import com.widget.CustomTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static com.handypro.Iconstant.ServiceConstant.GetLatLonFromZIPCode;

/**
 * Created by user127 on 16-02-2018.
 */

public class JobDetailsActivity extends AppCompatActivity {

    private static final int PICK_CAMERA_IMAGE = 1;
    private static final int PICK_GALLERY_IMAGE = 2;
    private static final int CAMERA_PERMISSION_CODE = 100;
    private static final int STORAGE_PERMISSION_CODE = 101;
    final int PERMISSION_REQUEST_CODE = 111;
    private static final String IMAGE_DIRECTORY_NAME = "Handypro";
    ImageView imageView;
    LinearLayout addphoto;
    int count = 1;
    MyGridView gridview;
    TextView done;
    LinearLayout beforename, LinearLayoutCampaign;
    MaterialSpinner MaterialSpinnerCampaign;
    EditText title, description;
    Typeface tf;
    TextView titlecolo;
    private GridViewAdapter gridAdapter;
    private List<String> imageItems;
    File compressedImage;
    private Uri fileUri; // file url to store image/video
    private String myZipCodeSTR, myLatitude, myLongitude, myTaskerDate;
    private ArrayList<String> mySelectedSubCatIdArr;
    private ProgressDialogcreatedforfrontpage myDialog;
    private CustomCompressClass mViewModel;
    SharedPreference mySession;
    BroadcastReceiver receiver;
    private CustomTextView myAnswerTXT;
    private ArrayList<SessionParentPojo> aSessionParentInfoList = null;
    private String aFinalSelectedStr = "", myScreenTypeStr = "";
    private String map_key = "";
    String cancellationhint = "";
    ArrayList<SubCategoryPojo> mySubCategryArrList = new ArrayList<>();
    private String mySelectedTimeSTR = "";
    private String totalday = "";

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_details_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        tf = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Medium.ttf");
        File dir = new File(getExternalFilesDir("") + "/Compressed");

        Intent intent = getIntent();
        mySelectedSubCatIdArr = (ArrayList<String>) intent.getSerializableExtra("selectedSubCategories");
        myLatitude = intent.getStringExtra("Latitude");
        myLongitude = intent.getStringExtra("Longitude");
        myZipCodeSTR = intent.getStringExtra("zipCode");
        myTaskerDate = intent.getStringExtra("task_date");
        myScreenTypeStr = intent.getStringExtra("SCREENTYPE");

        if (myScreenTypeStr.equalsIgnoreCase("BOOKNOW")) {
            Type listOfdoctorType = new TypeToken<List<SubCategoryPojo>>() {
            }.getType();
            mySubCategryArrList = new Gson().fromJson(intent.getStringExtra("CATERORY_LIST"), listOfdoctorType);
            mySelectedTimeSTR = intent.getStringExtra("SELECTED_DATE");
            totalday = intent.getStringExtra("TOTAL_DAY");
        }


        SharedPreferences pref = getApplicationContext().getSharedPreferences("AppInfo", MODE_PRIVATE);
        byte[] data = Base64.decode(pref.getString("code", ""), Base64.DEFAULT);
        try {
            map_key = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                new File(dir, children[i]).delete();
            }
        }

        myDialog = new ProgressDialogcreatedforfrontpage(JobDetailsActivity.this);
        mySession = new SharedPreference(this);
        //location where photo saved
        imageItems = new ArrayList<>();
        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        done = findViewById(R.id.done);
        titlecolo = findViewById(R.id.titlecolo);
        beforename = findViewById(R.id.beforename);
        imageView = findViewById(R.id.imageView);
        gridview = findViewById(R.id.gridview);
        addphoto = findViewById(R.id.addphoto);
        LinearLayoutCampaign = findViewById(R.id.LinearLayoutCampaign);
        MaterialSpinnerCampaign = findViewById(R.id.MaterialSpinnerCampaign);
        myAnswerTXT = findViewById(R.id.job_details_main_TXT_answer);

        MaterialSpinnerCampaign.setTypeface(tf);

        /*Spannable word = new SpannableString("To ");
        word.setSpan(new ForegroundColorSpan(Color.BLACK), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        titlecolo.setText(word);

        Spannable wordTwo = new SpannableString("Serve You ");
        wordTwo.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        titlecolo.append(wordTwo);

        Spannable wordThree = new SpannableString("better, tell us");
        wordThree.setSpan(new ForegroundColorSpan(Color.BLACK), 0, wordThree.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        titlecolo.append(wordThree);*/

        gridview.setEnabled(false);
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item, imageItems);
        gridview.setAdapter(gridAdapter);
        getImages();

        aSessionParentInfoList = mySession.getSessionParentDetails();
        if (aSessionParentInfoList != null && aSessionParentInfoList.size() > 0) {
            myAnswerTXT.setVisibility(View.VISIBLE);
            for (int h = 0; h < aSessionParentInfoList.size(); h++) {
                for (int u = 0; u < aSessionParentInfoList.get(h).getaPojo().size(); u++) {
                    String aSelectedStr = "";
                    for (int n = 0; n < aSessionParentInfoList.get(h).getaPojo().get(u).getAnswerpojo().size(); n++) {
                        if (aSessionParentInfoList.get(h).getaPojo().get(u).getAnswerpojo().get(n).isAnswerselected()) {
                            if (aSelectedStr.equalsIgnoreCase("")) {
                                aSelectedStr = aSessionParentInfoList.get(h).getaPojo().get(u).getAnswerpojo().get(n).getAnswertitle();
                            } else {
                                aSelectedStr = aSelectedStr + "," + aSessionParentInfoList.get(h).getaPojo().get(u).getAnswerpojo().get(n).getAnswertitle();
                            }
                        }
                    }
                    ParentQuestionPojo aParentQuestionPojo = aSessionParentInfoList.get(h).getaPojo().get(u);
                    String aAnswerStr = aParentQuestionPojo.getQuestionAnsView();
                    // Log.e("Split111", aAnswerStr.replace("{{answer}}", aSelectedStr));
                    if (aFinalSelectedStr.equalsIgnoreCase("")) {
                        aFinalSelectedStr = "\u2022 " + aAnswerStr.replace("{{answer}}", aSelectedStr);
                    } else {
                        aFinalSelectedStr = aFinalSelectedStr + "\n\u2022 " + aAnswerStr.replace("{{answer}}", aSelectedStr);
                    }
                    //  Log.e("aFinalSelectedStr", aFinalSelectedStr);
                }
            }
            myAnswerTXT.setText(aFinalSelectedStr);
        }

        if (aFinalSelectedStr.equalsIgnoreCase("")) {
            myAnswerTXT.setVisibility(View.VISIBLE);
            myAnswerTXT.setText(getResources().getString(R.string.txt_nothing_show));
        }

        /*if (mySession.GetFlowFrom().equals("JobConfirmPage")) {

        }*/

        SharedPreferences beforephoto = getApplicationContext().getSharedPreferences("jobdetailpage", 0); // 0 - for private mode
        //  title.setText(beforephoto.getString("title", ""));
        //  description.setText(beforephoto.getString("description", ""));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final PkDialog mDialog = new PkDialog(JobDetailsActivity.this);
                mDialog.setDialogTitle("SELECT");
                mDialog.setDialogMessage("Choose to add photo ?");
                mDialog.setPositiveButton(
                        getResources().getString(R.string.cameraword), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();

                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                                fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                                // start the image capture Intent
                                startActivityForResult(intent, PICK_CAMERA_IMAGE);


                            }
                        }
                );
                mDialog.setNegativeButton(
                        getResources().getString(R.string.galleryword), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();

                                Intent i = new Intent(
                                        Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(i, PICK_GALLERY_IMAGE);
                            }
                        }
                );

                mDialog.show();
            }
        });

        title.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("TextWatcherTest", "afterTextChanged:\t" + s.toString());
                SharedPreferences getcard;
                getcard = getApplicationContext().getSharedPreferences("jobdetailpage", 0); // 0 - for private mode
                SharedPreferences.Editor editor = getcard.edit();
                editor.putString("title", s.toString());
                editor.apply();
            }
        });

        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("TextWatcherTest", "afterTextChanged:\t" + s.toString());
                SharedPreferences getcard;
                getcard = getApplicationContext().getSharedPreferences("jobdetailpage", 0); // 0 - for private mode
                SharedPreferences.Editor editor = getcard.edit();
                editor.putString("description", s.toString());
                editor.apply();
            }
        });

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getImages();
                gridAdapter.notifyDataSetChanged();
            }
        };

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (title.getText().toString().equalsIgnoreCase("")) {
                    Animation shake = AnimationUtils.loadAnimation(JobDetailsActivity.this,
                            R.anim.shake);
                    title.startAnimation(shake);
                    SpannableString s = new SpannableString(getResources().getString(R.string.class_before_photos_about));
                    s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    title.setError(s);
                } else if (description.getText().toString().equalsIgnoreCase("")) {

                    Animation shake = AnimationUtils.loadAnimation(JobDetailsActivity.this,
                            R.anim.shake);
                    description.startAnimation(shake);
                    SpannableString s = new SpannableString(getResources().getString(R.string.class_before_photos_important));
                    s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    description.requestFocus();
                    description.setError(s);
                } else if (MaterialSpinnerCampaign.getText().toString().equals("Marketing")) {
                    HNDHelper.showErrorAlert(JobDetailsActivity.this, getResources().getString(R.string.campaign_error));
                } else {

                    if (myScreenTypeStr.equalsIgnoreCase("BOOKNOW")) {

                        postRequestConfirmBooking(JobDetailsActivity.this);

                    } else {

                        JSONArray taskimage = new JSONArray();
                        for (int jk = 0; jk < imageItems.size(); jk++) {
                            if (imageItems.get(jk).equalsIgnoreCase("nophoto")) {

                            } else {
                                taskimage.put(imageItems.get(jk));
                            }
                        }
                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("jobdetailpage", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        //  editor.putString("title", title.getText().toString());
                        editor.putString("description", description.getText().toString());
                        editor.putString("taskimage", "" + taskimage);
                        editor.putString("marketing", MaterialSpinnerCampaign.getText().toString());
                        editor.apply();

                        // Toast.makeText(getApplicationContext(),""+getcard.getString("taskimage",""),Toast.LENGTH_LONG).show();

                    /*SharedPreferences getcards;
                    getcards = getApplicationContext().getSharedPreferences("selectresponse", 0);
                    String laterornow = getcards.getString("laterornow", "");
                    if (laterornow.equals("now")) {
                        *//*Intent intent = new Intent(beforephotos.this, BookingConfirmationActivity.class);
                        startActivity(intent);*//*

                        Intent aSheduleAppointmntIntent = new Intent(beforephotos.this, SheduleAppointmentActivity.class);
                        aSheduleAppointmntIntent.putExtra("selectedSubCategories", mySelectedSubCatIdArr);
                        aSheduleAppointmntIntent.putExtra("zipCode", myZipCodeSTR);
                        aSheduleAppointmntIntent.putExtra("Latitude", myLatitude);
                        aSheduleAppointmntIntent.putExtra("Longitude", myLongitude);
                        aSheduleAppointmntIntent.putExtra("task_date", myTaskerDate);
                        startActivity(aSheduleAppointmntIntent);
                    } else {
                        Intent intent = new Intent(beforephotos.this, choosecraftmanbefore.class);
                        startActivity(intent);
                    }*/

                        Intent aSheduleAppointmntIntent = new Intent(JobDetailsActivity.this, ScheduleAppointmentActivity.class);
                        aSheduleAppointmntIntent.putExtra("selectedSubCategories", mySelectedSubCatIdArr);
                        aSheduleAppointmntIntent.putExtra("zipCode", myZipCodeSTR);
                        aSheduleAppointmntIntent.putExtra("Latitude", myLatitude);
                        aSheduleAppointmntIntent.putExtra("Longitude", myLongitude);
                        aSheduleAppointmntIntent.putExtra("task_date", myTaskerDate);
                        startActivity(aSheduleAppointmntIntent);
                    }
                }
            }
        });
        beforename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();

            }
        });
        addphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PkDialog mDialog = new PkDialog(JobDetailsActivity.this);
                mDialog.setDialogTitle(getResources().getString(R.string.activity_reviews_select));
                mDialog.setDialogMessage(getResources().getString(R.string.class_before_photos_choose));
                mDialog.setPositiveButton(
                        getResources().getString(R.string.cameraword), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();

                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                                fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                                // start the image capture Intent
                                startActivityForResult(intent, PICK_CAMERA_IMAGE);


                            }
                        }
                );
                mDialog.setNegativeButton(
                        getResources().getString(R.string.galleryword), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();

                                Intent i = new Intent(
                                        Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(i, PICK_GALLERY_IMAGE);
                            }
                        }
                );

                mDialog.show();

            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParam = new HashMap<>();
        SharedPreferences zip;
        zip = getApplicationContext().getSharedPreferences("zipcode", 0);
        jsonParam.put("zipcode", zip.getString("code", ""));
        ServiceRequest mRequest = new ServiceRequest(JobDetailsActivity.this);
        mRequest.makeServiceRequest(ServiceConstant.GET_CAMPAIGN_CODE, Request.Method.POST, jsonParam, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("---------Categories detail response------------" + response);
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("1")) {
                        JSONObject result = object.getJSONObject("result");
                        JSONArray adsource = result.getJSONArray("adsource");
                        if (adsource.length() > 0) {
                            String[] campaign_code = new String[adsource.length() + 1];
                            campaign_code[0] = "Marketing";
                            int array_index = 1;
                            for (int a = 0; a < adsource.length(); a++, array_index++) {
                                JSONObject text = adsource.getJSONObject(a);
                                campaign_code[array_index] = text.getString("text");
                            }
                            MaterialSpinnerCampaign.setItems(campaign_code);
                        }

                    } else {
                        LinearLayoutCampaign.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                    LinearLayoutCampaign.setVisibility(View.GONE);
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                LinearLayoutCampaign.setVisibility(View.GONE);
            }
        });
    }


    private void postRequestConfirmBooking(final Context aContext) {

        HashMap<String, String> params = new HashMap<>();


        mySelectedSubCatIdArr.clear();
        for (int i = 0; i < mySubCategryArrList.size(); i++) {
            if (mySubCategryArrList.get(i).getAvailability().equalsIgnoreCase("1")) {
                mySelectedSubCatIdArr.add(mySubCategryArrList.get(i).getSubCategoryId());
            }
        }
        try {
            JSONArray aJsonArrs = new JSONArray(mySelectedSubCatIdArr);
            for (int a = 0; a < aJsonArrs.length(); a++) {
                params.put("subcategory[" + a + "]", aJsonArrs.getString(a));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        params.put("pickup_date", totalday);
        params.put("pickup_time", mySelectedTimeSTR);

        SharedPreferences zip;
        zip = getApplicationContext().getSharedPreferences("zipcode", 0);
        if (zip.getString("Latitude", "").trim().isEmpty() || zip.getString("Longitude", "").trim().isEmpty()) {
            GetLatLonFromZIPCode(zip.getString("code", ""));
        } else {
            params.put("lat", zip.getString("Latitude", ""));
            params.put("long", zip.getString("Longitude", ""));
            params.put("zipcode", zip.getString("code", ""));

            ServiceRequest mRequest = new ServiceRequest(aContext);
            mRequest.makeServiceRequest(ServiceConstant.checkcratfman, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {


                    System.out.println("-------------get Confirm Category list Response----------------" + response);

                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getString("status").equalsIgnoreCase("1")) {

                            SharedPreferences getcards;
                            getcards = getApplicationContext().getSharedPreferences("selectresponse", 0); // 0 - for private mode
                            SharedPreferences.Editor editorgetcards = getcards.edit();
                            editorgetcards.putString("res", "" + response);
                            editorgetcards.putString("laterornow", "" + object.getString("task_type"));
                            editorgetcards.apply();

                            SharedPreferences getcard;
                            getcard = getApplicationContext().getSharedPreferences("scheduleapp", 0); // 0 - for private mode
                            SharedPreferences.Editor editor = getcard.edit();
                            editor.putString("date", "" + totalday);
                            editor.putString("time", "" + mySelectedTimeSTR);
                            editor.putString("catgeoryid", "" + mySession.getCategoryId());
                            editor.apply();

                            final SharedPreferences pref = getApplicationContext().getSharedPreferences("beforedismiss", 0);
                            String dataavaialble = pref.getString("before", "0");
                            if (dataavaialble.equalsIgnoreCase("1")) {

                                final PkDialog mDialog = new PkDialog(JobDetailsActivity.this);
                                mDialog.setDialogTitle(getResources().getString(R.string.activity_job_detail_TXT_title));
                                mDialog.setDialogMessage(getResources().getString(R.string.class_schedule_appoitment_stored));
                                mDialog.setPositiveButton(
                                        getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (cancellationhint.length() < 2) {

                                                    Intent aSelectCrftsMnIntent = new Intent(JobDetailsActivity.this, BookingConfirmationPreviewActivity.class);
                                                    startActivity(aSelectCrftsMnIntent);
                                                    mDialog.dismiss();
                                                } else {
                                                    mDialog.dismiss();
                                                    try {

                                                        final PkDialog mDialog = new PkDialog(JobDetailsActivity.this);
                                                        mDialog.setDialogMessage(cancellationhint);
                                                        mDialog.setPositiveButton(
                                                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        mDialog.dismiss();
                                                                        Intent aSelectCrftsMnIntent = new Intent(JobDetailsActivity.this, BookingConfirmationPreviewActivity.class);
                                                                        startActivity(aSelectCrftsMnIntent);
                                                                    }
                                                                }
                                                        );
                                                        mDialog.setNegativeButton(
                                                                getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        mDialog.dismiss();
                                                                    }
                                                                }
                                                        );

                                                        mDialog.show();

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }


                                            }
                                        }
                                );

                                mDialog.setNegativeButton(
                                        getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {


                                                if (cancellationhint.length() < 2) {

                                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("beforedismiss", 0); // 0 - for private mode
                                                    SharedPreferences.Editor editor = pref.edit();
                                                    editor.putString("before", "0");
                                                    editor.apply();

                                                    Intent intent = new Intent(JobDetailsActivity.this, ChooseCraftsmanActivity.class);
                                                    startActivity(intent);
                                                    mDialog.dismiss();
                                                } else {
                                                    mDialog.dismiss();
                                                    try {

                                                        final PkDialog mDialog = new PkDialog(JobDetailsActivity.this);
                                                        mDialog.setDialogMessage(cancellationhint);
                                                        mDialog.setPositiveButton(
                                                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        mDialog.dismiss();
                                                                        SharedPreferences pref = getApplicationContext().getSharedPreferences("beforedismiss", 0); // 0 - for private mode
                                                                        SharedPreferences.Editor editor = pref.edit();
                                                                        editor.putString("before", "0");
                                                                        editor.apply();

                                                                        /*File dir = new File(Environment.getExternalStorageDirectory()
                                                                                + "/Android/data/"
                                                                                + SheduleAppointmentActivity.this.getApplicationContext().getPackageName()
                                                                                + "/Files/Compressed");

                                                                        if (dir.isDirectory()) {
                                                                            String[] children = dir.list();
                                                                            for (String child : children) {
                                                                                new File(dir, child).delete();
                                                                            }
                                                                        }*/

                                                                        /*Intent aSelectCrftsMnIntent = new Intent(SheduleAppointmentActivity.this, beforephotos.class);
                                                                        startActivity(aSelectCrftsMnIntent);*/

                                                                        Intent intent = new Intent(JobDetailsActivity.this, ChooseCraftsmanActivity.class);
                                                                        startActivity(intent);
                                                                    }
                                                                }
                                                        );
                                                        mDialog.setNegativeButton(
                                                                getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        mDialog.dismiss();
                                                                    }
                                                                }
                                                        );

                                                        mDialog.show();

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }

                                                }


                                            }
                                        }
                                );

                                mDialog.show();
                            } else {


                                if (cancellationhint.length() < 2) {
                                    SharedPreferences.Editor editors = pref.edit();
                                    editors.putString("before", "0");
                                    editors.apply();

                                    Intent intent = new Intent(JobDetailsActivity.this, ChooseCraftsmanActivity.class);
                                    startActivity(intent);
                                } else {
                                    try {

                                        final PkDialog mDialog = new PkDialog(JobDetailsActivity.this);
                                        mDialog.setDialogMessage(cancellationhint);
                                        mDialog.setPositiveButton(
                                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        mDialog.dismiss();
                                                        SharedPreferences.Editor editors = pref.edit();
                                                        editors.putString("before", "0");
                                                        editors.apply();

                                                        Intent intent = new Intent(JobDetailsActivity.this, ChooseCraftsmanActivity.class);
                                                        startActivity(intent);
                                                    }
                                                }
                                        );
                                        mDialog.setNegativeButton(
                                                getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        mDialog.dismiss();
                                                    }
                                                }
                                        );

                                        mDialog.show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }


                            }
                        } else {
                            HNDHelper.showResponseErrorAlert(JobDetailsActivity.this, object.getString("response"));
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block

                        e.printStackTrace();
                    }


                }

                @Override
                public void onErrorListener() {
                }
            });
        }

    }
    private void GetLatLonFromZIPCode(final String ZIPCode) {
        ServiceRequest mRequest = new ServiceRequest(this);
        mRequest.makeServiceRequest(ServiceConstant.GetLatLonFromZIPCode + map_key + "&sensor=false&components=postal_code:" + (ZIPCode), Request.Method.GET, null, new ServiceRequest.ServiceListener() {


            @Override
            public void onCompleteListener(String response) {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                try {
                    System.out.println("ServiceConstant.GetLatLonFromZIPCode + map_key + \"&sensor=false&components=postal_code:\" + (ZIPCode)"+ServiceConstant.GetLatLonFromZIPCode + map_key + "&sensor=false&components=postal_code:" +(ZIPCode));

                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        String myStatus = object.getString("status");
                        JSONArray place_object = object.getJSONArray("results");
                        if (myStatus.equalsIgnoreCase("OK")) {

                            if (place_object.length() > 0) {
                                JSONObject address_object = place_object.getJSONObject(0);
                                JSONObject geometryObj = address_object.getJSONObject("geometry");
                                JSONObject location = geometryObj.getJSONObject("location");
                                mySession.SetLatLong(location.getString("lat"), location.getString("lng"));
                                postRequestConfirmBooking(JobDetailsActivity.this);
                            } else {
                                Toast.makeText(JobDetailsActivity.this, "Unable to convert ZIP code location, Try again later", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(JobDetailsActivity.this, "Unable to convert ZIP code location, Try again later", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(JobDetailsActivity.this, "Unable to convert ZIP code location, Try again later", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }
        });
    }

    private List<String> getImages() {

        File fileTarget = new File(getExternalFilesDir("") + "/Compressed");

        File[] files = fileTarget.listFiles();

        imageItems.clear();

        SharedPreference mySession = new SharedPreference(this);
        if (mySession.GetFlowFrom().equals("JobConfirmPage")) {
            SharedPreferences beforephoto = getApplicationContext().getSharedPreferences("jobdetailpage", 0); // 0 - for private mode
            String taskimage = beforephoto.getString("taskimage", "");
            JSONArray aJsonArr = null;
            try {
                aJsonArr = new JSONArray(taskimage);
                for (int a = 0; a < aJsonArr.length(); a++) {
                    imageItems.add(aJsonArr.getString(a));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (files != null) {
            for (File file : files) {
                imageItems.add(file.getAbsolutePath());
            }
        }
        imageItems.add("nophoto");
        gridAdapter.notifyDataSetChanged();
        return imageItems;
    }

    protected void onResume() {
        super.onResume();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("beforedismiss", 0);
        String dataavaialble = pref.getString("before", "0");
        if (dataavaialble.equalsIgnoreCase("1")) {
            finish();
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.handypro.files.selected");
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    //Remove this section if you don't want camera code (Start)
    //Got new image taken from camera intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_CAMERA_IMAGE) {
                String picturePath = fileUri.getPath();
                try {
                    File actualImage = new File(picturePath);
                    CustomCompressClass customCompressClass = new CustomCompressClass();
                    customCompressClass.constructor(this, actualImage);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*getImages();
                gridAdapter.notifyDataSetChanged();*/
            } else if (requestCode == PICK_GALLERY_IMAGE) {

                String picturePath;
                Uri selectedImageUri = data.getData();
                picturePath = getPath(getApplicationContext(), selectedImageUri);
                try {
                    File actualImage = new File(picturePath);

                    CustomCompressClass customCompressClass = new CustomCompressClass();
                    customCompressClass.constructor(this, actualImage/*, new geocoderHandler()*/);
                    /*File TempCompressedFile = customCompressClass.getCustomCompressClassCompressedFile();
                    if (TempCompressedFile.isFile()) {
                        Toast.makeText(this, "Created", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Fail", Toast.LENGTH_SHORT).show();
                    }*/

                    /*File folder = getExternalFilesDir("Compressed");
                    boolean success = true;
                    if (!folder.exists()) {
                        success = new File(getExternalFilesDir(""), "Compressed").isFile();
                    }
                    if (success) {



                        compressedImage = new Compressor(this)
                                .setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(75)
                                .setCompressFormat(Bitmap.CompressFormat.WEBP)
                                .setDestinationDirectoryPath(getExternalFilesDir("") + "/Compressed")
                                .compressToFile(actualImage);
                    } else {
                        Toast.makeText(this, "Unable to create folder to save the image.", Toast.LENGTH_SHORT).show();
                    }*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*getImages();
                gridAdapter.notifyDataSetChanged();*/
            }
        }
    }


    //Remove this section if you don't want camera code (End)

    public class GridViewAdapter extends ArrayAdapter {
        private Context context;
        private List<String> data = new ArrayList<String>();
        private int resourceId;
        private LayoutInflater inflater;

        GridViewAdapter(Context context, int resourceId, List<String> data) {
            super(context, resourceId, data);
            this.resourceId = resourceId;
            this.context = context;
            this.data = data;
            inflater = LayoutInflater.from(context);

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            GridViewAdapter.ViewHolder holder = null;
            if (row == null) {
                row = inflater.inflate(resourceId, parent, false);
                holder = new GridViewAdapter.ViewHolder();
                holder.image = row.findViewById(R.id.image);
                holder.aaddimage = row.findViewById(R.id.aaddimage);
                holder.close = row.findViewById(R.id.close);
                row.setTag(holder);
            } else {
                holder = (GridViewAdapter.ViewHolder) row.getTag(); //Easy to recycle view
            }


            if (data.get(position).equalsIgnoreCase("nophoto")) {
                holder.close.setVisibility(View.GONE);
                holder.aaddimage.setVisibility(View.VISIBLE);
                holder.image.setVisibility(View.GONE);

            } else {
                holder.close.setVisibility(View.VISIBLE);
                holder.image.setVisibility(View.VISIBLE);
                holder.aaddimage.setVisibility(View.GONE);
               /* Glide.with(context)
                        .load("file://" + data.get(position))
                        .fitCenter()
                        .centerCrop()
                        .into(holder.image);*/

                /*Picasso.with(context).load("file://" + data.get(position))
                        .resize(160, 160)
                        .placeholder(R.drawable.icon_placeholder)
                        .into(holder.image);*/

                Picasso.get().load("file://" + data.get(position))
                        .resize(160, 160)
                        .placeholder(R.drawable.icon_placeholder).into(holder.image);
            }


            holder.aaddimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!checkWriteExternalStoragePermission() || !checkcamerapermission()) {
                        requestPermission();}
                    else{


                    final PkDialog mDialog = new PkDialog(JobDetailsActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.activity_reviews_select));
                    mDialog.setDialogMessage(getResources().getString(R.string.class_before_photos_choose));
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.cameraword), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();

                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                                    // start the image capture Intent
                                    startActivityForResult(intent, PICK_CAMERA_IMAGE);


                                }
                            }
                    );
                    mDialog.setNegativeButton(
                            getResources().getString(R.string.galleryword), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();

                                    Intent i = new Intent(
                                            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(i, PICK_GALLERY_IMAGE);
                                }
                            }
                    );

                    mDialog.show();
                }  }
           });


            holder.close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // String imagePath =   parent.getAdapter().getItem(position).toString();

                    final PkDialog mDialog = new PkDialog(JobDetailsActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.class_before_photos_delete));
                    mDialog.setDialogMessage(getResources().getString(R.string.class_before_photos_sure));
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    File fdelete = new File(imageItems.get(position));
                                    if (fdelete.exists()) {
                                        if (fdelete.delete()) {

                                        } else {

                                        }
                                    }
                                    getImages();
                                    gridAdapter.notifyDataSetChanged();

                                /*gridAdapter = new GridViewAdapter(beforephotos.this, R.layout.grid_item, imageItems);
                                gridview.setAdapter(gridAdapter);*/
                                    mDialog.dismiss();
                                }
                            }
                    );
                    mDialog.setNegativeButton(
                            getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            }
                    );

                    mDialog.show();

                }
            });


            return row;
        }

        //I prefer to have Holder to keep all controls
        //So that I can recycle easily in getView
        class ViewHolder {
            ImageView image, close, aaddimage;
        }


    }





   /* public class JobDetailImageAdapter extends BaseAdapter {
        private List<String> data = new ArrayList<String>();
        private int resourceId;
        private LayoutInflater inflater ;
        private Context myContext;

        public JobDetailImageAdapter(Context context, int resourceId, List<String> data) {

            this.resourceId = resourceId;
            this.myContext = context;
            this.data = data;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return data.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(myContext).inflate(R.layout.layout_inflater_photo_grid_item, parent, false);
            }
            ImageView aPhotoGridSingle = (ImageView) convertView.findViewById(R.id.layout_inflater_photo_grid_IMG);
            ImageView aPhotoCloseIMG = (ImageView) convertView.findViewById(R.id.layout_inflater_photo_grid_cancel_IMG);
            RelativeLayout aPhotoGridLAY = (RelativeLayout) convertView.findViewById(R.id.layout_inflater_photo_grid_cancel_IMG_LAY);

            if (position == data.size()) {
                aPhotoCloseIMG.setVisibility(View.GONE);
                aPhotoGridSingle.setImageResource(R.drawable.icon_add_image);
                aPhotoGridSingle.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                return convertView;
            }
            else
            {
                Glide.with(myContext)
                        .load("file://" + data.get(position))
                        .fitCenter()
                        .centerCrop()
                        .into(aPhotoGridSingle);
                aPhotoGridSingle.setScaleType(ImageView.ScaleType.FIT_XY);
                aPhotoCloseIMG.setVisibility(View.VISIBLE);
            }





            aPhotoGridLAY.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        new MaterialDialog.Builder(myContext)
                                .content(myContext.getResources().getString(R.string.activity_job_detail_remove_photo))
                                .positiveText(myContext.getResources().getString(R.string.dialog_ok))
                                .negativeText(myContext.getResources().getString(R.string.dialog_cancel))
                                .positiveColor(myContext.getResources().getColor(R.color.colorAccent))
                                .negativeColor(myContext.getResources().getColor(R.color.darkcolor_3))
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        data.remove(position);
                                        notifyDataSetChanged();
                                    }
                                })
                                .show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            return convertView;
        }

    }*/


    class geocoderHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                if (msg.getData().containsKey("Status") && msg.getData().getString("Status").equals("Success")) {
                    Toast.makeText(JobDetailsActivity.this, "Created", Toast.LENGTH_SHORT).show();
                    getImages();
                    gridAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(JobDetailsActivity.this, "Fail", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }
    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkcamerapermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
    }

//    public void requestPermission(String permission, int requestCode)
//    {
//
//        // Checking if permission is not granted
//        if (!checkWriteExternalStoragePermission() || !checkcamerapermission()) {
//            requestPermission();
//        }
//        else {
//
//            final PkDialog mDialog = new PkDialog(JobDetailsActivity.this);
//            mDialog.setDialogTitle(getResources().getString(R.string.activity_reviews_select));
//            mDialog.setDialogMessage(getResources().getString(R.string.class_before_photos_choose));
//            mDialog.setPositiveButton(
//                    getResources().getString(R.string.cameraword), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            mDialog.dismiss();
//
//                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//                            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
//
//                            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//
//                            // start the image capture Intent
//                            startActivityForResult(intent, PICK_CAMERA_IMAGE);
//
//
//                        }
//                    }
//            );
//            mDialog.setNegativeButton(
//                    getResources().getString(R.string.galleryword), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            mDialog.dismiss();
//
//                            Intent i = new Intent(
//                                    Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                            startActivityForResult(i, PICK_GALLERY_IMAGE);
//                        }
//                    }
//            );
//
//            mDialog.show();
//
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {


            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                final PkDialog mDialog = new PkDialog(JobDetailsActivity.this);
                mDialog.setDialogTitle(getResources().getString(R.string.activity_reviews_select));
                mDialog.setDialogMessage(getResources().getString(R.string.class_before_photos_choose));
                mDialog.setPositiveButton(
                        getResources().getString(R.string.cameraword), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();

                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                                fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                                // start the image capture Intent
                                startActivityForResult(intent, PICK_CAMERA_IMAGE);


                            }
                        }
                );
                mDialog.setNegativeButton(
                        getResources().getString(R.string.galleryword), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();

                                Intent i = new Intent(
                                        Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(i, PICK_GALLERY_IMAGE);
                            }
                        }
                );

                mDialog.show();

            }            } else {

            if (!checkWriteExternalStoragePermission() || !checkcamerapermission()) {
                requestPermission();}
            }


        }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
