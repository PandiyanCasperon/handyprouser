package com.handypro.activities.bookingFlow;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.github.badoualy.datepicker.DatePickerTimeline;
import com.github.badoualy.datepicker.MonthView;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.AppointmentTimePojo;
import com.handypro.Pojo.SubCategoryPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.Widgets.RecyclerItemClickListener;
import com.handypro.activities.BaseActivity;
import com.handypro.activities.MyGridView;
import com.handypro.adapters.AppointmentTimeAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by CAS63 on 2/21/2018.
 */

public class ScheduleAppointmentActivity extends BaseActivity {

    public static final int RequestPermissionCode = 7;
    private ProgressDialogcreated myDialogs;
    private RelativeLayout showme;
    private LinearLayout iamfirst;
    private int saveoveralladdvalue = 0;
    String overall_job_count = "1";
    LinearLayout myEstimateInfoLAY;
    String procrewno = "";
    TextView hideselect;
    CustomTextView higlighttext;
    String totalday;
    LinearLayout notimeavaiable;
    int estimatecount = 0;
    int estimatehour = 0;
    String cancellationhint = "", good_estimation = "";
    ArrayList<String> taskname = new ArrayList<String>();
    ArrayList<String> taskhours = new ArrayList<String>();
    String storeavailablecategoryid = "";
    int firstpo = 0;
    TextView showerrormessage;
    int saveposva = 0;
    int jko = 0;
    DatePickerTimeline timeline;
    private Button ButtonNextAvailability;
    private int myDateInt, myMonthInt, myCounterInt;
    private String myDateSTR = "", myLatitude = "", myLongitude = "", myZipCodeSTR = "",
            myMinuteStr = "", myApprxTimeSTR = "", mySelectedTimeSTR = "", myTaskerDate = "", map_key = "";
    private ArrayList<AppointmentTimePojo> myTimeArrList;
    private RecyclerView myTimeRecyclerView;
    private AppointmentTimeAdapter myAdapter;
    private RelativeLayout myBackLAY;
    private ArrayList<String> mySelectedSubCatIdArr;
    private SelectedSubCategoryAdapter mySubCatAdapter;
    private MyGridView mySubCatGridVw;
    private CustomButton myContinueBookingBTN;
    private ConnectionDetector myConnectionManager;
    private SharedPreference mySession;
    private ArrayList<SubCategoryPojo> mySubCategryArrList;
    private CustomTextView myApprxmtTimeTXT, myNoCraftsMenTXT, myCounterTXT;
    private int myIntTimePos = 0, addvalue = 0;
    private String myBookingIDStr = "", Job_Type = "";
    private ProgressDialogcreated myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shedule_appointment);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        myDialog = new ProgressDialogcreated(ScheduleAppointmentActivity.this);
        myDialogs = new ProgressDialogcreated(ScheduleAppointmentActivity.this);
        storeavailablecategoryid = "";
        SharedPreferences callprocrew;
        callprocrew = getApplicationContext().getSharedPreferences("callprocrew", 0); // 0 - for private mode
        procrewno = callprocrew.getString("call", "");
        SharedPreferences pref = getApplicationContext().getSharedPreferences("AppInfo", MODE_PRIVATE);
        byte[] data = Base64.decode(pref.getString("code", ""), Base64.DEFAULT);
        try {
            map_key = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        taskname.clear();
        taskhours.clear();
        initClassAndWidgets();


        getIntentValues();

        clickListener();
        myCalInit();
        recyclerViewItemClick();


        onGridItemClick();
    }


    private void recyclerViewAdapter() {

        myAdapter = new AppointmentTimeAdapter(ScheduleAppointmentActivity.this, myTimeArrList);

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(ScheduleAppointmentActivity.this, LinearLayoutManager.VERTICAL, false);
        myTimeRecyclerView.setLayoutManager(horizontalLayoutManagaer);
        myTimeRecyclerView.setAdapter(myAdapter);

        myTimeRecyclerView.addOnScrollListener(new CustomScrollListener());

        if (myTimeArrList.size() > 0) {
            myTimeRecyclerView.scrollToPosition(saveposva);
        }


    }

    private void myCalInit() {
//        myCalendar.setSelectedDate(new Date());
        myDateSTR = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
//        myCalendar.setDateSelected(new Date(), true);
//        myCalendar.state().edit()
//                .setMinimumDate(new Date())
//                .setFirstDayOfWeek(Calendar.MONDAY)
//                .commit();
    }


    private void recyclerViewItemClick() {
        myTimeRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(ScheduleAppointmentActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                AppointmentTimePojo aAppTimePojo = myAdapter.getItem(position);
                if (aAppTimePojo.getColourchange().equals("0")) {

                    HNDHelper.showResponseErrorAlert(ScheduleAppointmentActivity.this, getResources().getString(R.string.class_schedule_appoitment_selected));
                    mySelectedTimeSTR = "";
                    myContinueBookingBTN.setTextColor(getResources().getColor(R.color.darkcolor_1));

                } else {
                    if (aAppTimePojo.getSelected().equals("false")) {
                        checkOtherItems();
                        aAppTimePojo.setSelected("true");
                        mySelectedTimeSTR = aAppTimePojo.getsendtime();
                        myContinueBookingBTN.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        aAppTimePojo.setSelected("false");
                        mySelectedTimeSTR = "";
                        myContinueBookingBTN.setTextColor(getResources().getColor(R.color.darkcolor_1));
                    }
                }


                myAdapter.notifyDataSetChanged();


            }
        }));


    }

    private void onGridItemClick() {
        mySubCatGridVw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == mySubCategryArrList.size()) {
                    onBackPressed();
                    finish();
                } else {
                    return;
                }
            }
        });
    }

    private void checkOtherItems() {
        for (int a = 0; a < myAdapter.getAllTimeArr().size(); a++) {
            if (myAdapter.getAllTimeArr().get(a).getSelected().equals("true")) {
                myAdapter.getAllTimeArr().get(a).setSelected("false");
                break;
            }
        }
    }

    private void clickListener() {

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                finish();

            }
        });


        myContinueBookingBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mySelectedTimeSTR.length() < 2) {
                    HNDHelper.showResponseErrorAlert(ScheduleAppointmentActivity.this, getResources().getString(R.string.bookingtime));
                } else if (totalday.length() < 2) {
                    final PkDialog mDialog = new PkDialog(ScheduleAppointmentActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.rating_header_sorry_textView));
                    mDialog.setDialogMessage(getResources().getString(R.string.class_schedule_appoitment_booking_date));
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            }
                    );

                    mDialog.show();
                } else if (mySession.getCategoryId().length() < 2) {
                    HNDHelper.showResponseErrorAlert(ScheduleAppointmentActivity.this, getResources().getString(R.string.choosecategory));
                } else {
                    if (myConnectionManager.isConnectingToInternet()) {
                        postRequestConfirmBooking(ScheduleAppointmentActivity.this);
                    } else {
                        HNDHelper.showResponseErrorAlert(ScheduleAppointmentActivity.this, getResources().getString(R.string.nointernet_text));
                    }
                }


            }
        });
    }


    private void AddItemsInTimeArr(String aSelectedSTR, String aTime, String sendtime, String colourchange) {

        AppointmentTimePojo aTimePojo = new AppointmentTimePojo();
        aTimePojo.setSelected(aSelectedSTR);
        aTimePojo.setAppointmentTime(aTime);
        aTimePojo.setsendtime(sendtime);
        aTimePojo.setEnabled("true");
        aTimePojo.setColourchange(colourchange);
        mySelectedTimeSTR = "";
        myContinueBookingBTN.setTextColor(getResources().getColor(R.color.darkcolor_1));
        myTimeArrList.add(aTimePojo);
    }


    private void initClassAndWidgets() {
        myTimeArrList = new ArrayList<AppointmentTimePojo>();
        mySubCategryArrList = new ArrayList<SubCategoryPojo>();
        myConnectionManager = new ConnectionDetector(ScheduleAppointmentActivity.this);
        mySession = new SharedPreference(ScheduleAppointmentActivity.this);
        // Toast.makeText(getApplicationContext(),mySession.getCurrencySymbol() ,Toast.LENGTH_LONG).show();
        notimeavaiable = findViewById(R.id.notimeavaiable);
        higlighttext = findViewById(R.id.activity_shedule_appointment_page_free_estimate_TXT);
//        myCalendar = (MaterialCalendarView) findViewById(R.id.activity_shedule_appointment_page_calendarview);
        myTimeRecyclerView = findViewById(R.id.activity_shedule_appointment_recyclerview);
        myBackLAY = findViewById(R.id.activity_shedule_appoinment_LAY_back);
        myEstimateInfoLAY = findViewById(R.id.activity_shedule_appointment_page_free_estimate_LAY);
        mySubCatGridVw = findViewById(R.id.activity_shedule_appointment_gridView);
        myContinueBookingBTN = findViewById(R.id.activity_shedule_appointment_bottom_continue_BTN);
        myApprxmtTimeTXT = findViewById(R.id.activity_shedule_appointment_page_apprxm_time_TXT);
        myNoCraftsMenTXT = findViewById(R.id.activity_shedule_appointment_no_craftsmen_TXT);
        myCounterTXT = findViewById(R.id.activity_shedule_appointment_counter_TXT);
        showerrormessage = findViewById(R.id.showerrormessage);
        ButtonNextAvailability = findViewById(R.id.ButtonNextAvailability);

        showme = findViewById(R.id.showme);
        iamfirst = findViewById(R.id.iamfirst);


        hideselect = findViewById(R.id.hideselect);
        hideselect.setVisibility(View.VISIBLE);
        myEstimateInfoLAY.setVisibility(View.GONE);


        SharedPreferences pref = getApplicationContext().getSharedPreferences("secondshow", 0);

        if (pref.getString("sshowme", "").equals("1")) {
            iamfirst.setBackgroundResource(0);
            showme.setVisibility(View.VISIBLE);

        } else {
            showme.setVisibility(View.GONE);
        }

        iamfirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("secondshow", 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("sshowme", "1");
                editor.apply();
                editor.commit();

                if (pref.getString("sshowme", "").equals("1")) {
                    iamfirst.setBackgroundResource(0);
                    showme.setVisibility(View.VISIBLE);
                } else {
                    showme.setVisibility(View.GONE);
                }
            }
        });


        myCounterTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (procrewno != null || procrewno.length() > 2) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + procrewno));
                    startActivity(callIntent);

                }
            }
        });


        timeline = findViewById(R.id.timeline);
        timeline.setDateLabelAdapter(new MonthView.DateLabelAdapter() {
            @Override
            public CharSequence getLabel(Calendar calendar, int index) {
                return (calendar.get(Calendar.MONTH) + 1) + "/" + (calendar.get(Calendar.YEAR) % 2000);
            }
        });


        Calendar calendar = Calendar.getInstance();
        int thisYear = calendar.get(Calendar.YEAR);
        final int thisMonth = calendar.get(Calendar.MONTH);
        int thisDay = calendar.get(Calendar.DAY_OF_MONTH);


        String month = "" + (thisMonth + 1);
        String day = "" + thisDay;

        if ((thisMonth + 1) <= 9) {
            month = "0" + (thisMonth + 1);
        }
        if (thisDay <= 9) {
            day = "0" + thisDay;
        }

        totalday = month + "/" + day + "/" + thisYear;

        timeline.setFirstVisibleDate(thisYear, thisMonth, thisDay);
        timeline.setLastVisibleDate(thisYear, thisMonth + 2, thisDay);

        timeline.setOnDateSelectedListener(new DatePickerTimeline.OnDateSelectedListener() {
            @Override
            public void onDateSelected(int year, int month, int day, int index) {

                String monthss = "" + (month + 1);
                String dayss = "" + day;

                if ((month + 1) <= 9) {
                    monthss = "0" + (month + 1);
                }
                if (day <= 9) {
                    dayss = "0" + day;
                }

                totalday = monthss + "/" + dayss + "/" + year;
                  /*  myTimeArrList.clear();
                    checktime.clear();*/

                if (myConnectionManager.isConnectingToInternet()) {
                    mySelectedTimeSTR = "";
                    myContinueBookingBTN.setTextColor(getResources().getColor(R.color.darkcolor_1));

                    /*if (myTaskerDate != null || myTaskerDate.length() < 0 || myTaskerDate == "") {
                        totalday = myTaskerDate;
                    }*/

                    PostRequest(saveoveralladdvalue);
                } else {
                    HNDHelper.showResponseErrorAlert(ScheduleAppointmentActivity.this, getResources().getString(R.string.nointernet_text));
                }
            }
        });


        ButtonNextAvailability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!myDialog.isShowing()) {
                    myDialog.show();
                }

                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("overall_taskhours", "" + saveoveralladdvalue);
                SharedPreferences zipcode;
                zipcode = getApplicationContext().getSharedPreferences("zipcode", 0);
                String code = zipcode.getString("code", "");
                jsonParams.put("zipcode", code);
                if (mySession.getCategoryId().equals("")) {
                    System.out.println("--------------Otp url-------------------" + mySession.getCategoryId());
                } else {
                    System.out.println("--------------Otp url-------------------no" + mySession.getCategoryId());
                    if (mySession.getCategoryId().contains(",")) {
                        String[] categoryi = mySession.getCategoryId().split(",");
                        for (int j = 0; j < categoryi.length; j++) {
                            jsonParams.put("subcategory[" + j + "]", categoryi[j]);
                        }
                    } else {
                        jsonParams.put("subcategory[" + 0 + "]", mySession.getCategoryId());
                    }
                }
//                String[] days = new String[7];
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(StringToDate(totalday, "MM/dd/yyyy"));
                for (int i = 0; i < 7; i++) {
                    calendar.add(Calendar.DATE, 1);
//                    days[i] = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).format(calendar.getTime());
                    jsonParams.put("date[" + i + "]", new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).format(calendar.getTime()));
//                    Log.e("Date -->", days[i]);
                }

                ServiceRequest mRequest = new ServiceRequest(ScheduleAppointmentActivity.this);
                mRequest.makeServiceRequest(ServiceConstant.CHECK_NEXT_AVAILABILITY, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
                    @Override
                    public void onCompleteListener(String response) {
                        System.out.println("---------CHECK_NEXT_AVAILABILITY------------" + response);

                        if (myDialog.isShowing()) {
                            myDialog.dismiss();
                        }
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object.getString("status").equals("1")) {
                                Log.e(("Next Date"), object.getString("next_avail_date"));
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(StringToDate(object.getString("next_avail_date"), "MM/dd/yyyy"));
                                timeline.setSelectedDate(
                                        calendar.get(Calendar.YEAR),
                                        calendar.get(Calendar.MONTH),
                                        calendar.get(Calendar.DATE)
                                );
                                totalday = object.getString("next_avail_date");
                            } else if (object.getString("status").equals("0")) {
                                Log.e(("Next Date"), object.getString("result"));
                                HNDHelper.showResponseErrorAlert(ScheduleAppointmentActivity.this, object.getString("result"));
                                totalday = object.getString("till_date");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorListener() {

                        if (myDialog.isShowing()) {
                            myDialog.dismiss();
                        }

                    }
                });
            }
        });

    }

    private void getIntentValues() {
        Intent intent = getIntent();
        mySelectedSubCatIdArr = (ArrayList<String>) intent.getSerializableExtra("selectedSubCategories");
        System.out.println("-------------------------selected category arr size" + mySelectedSubCatIdArr.size());
        myLatitude = intent.getStringExtra("Latitude");
        myLongitude = intent.getStringExtra("Longitude");
        myZipCodeSTR = intent.getStringExtra("zipCode");
        myTaskerDate = intent.getStringExtra("task_date");

//        Toast.makeText(this, "Tasker Date :" + myTaskerDate, Toast.LENGTH_SHORT).show();
        System.out.println("Tasker Date : " + myTaskerDate);
        GetAndloadData();

    }

    private void loadDataInAdapter() {
        mySubCatAdapter = new SelectedSubCategoryAdapter(mySubCategryArrList, ScheduleAppointmentActivity.this);
        mySubCatGridVw.setAdapter(mySubCatAdapter);
        HNDHelper.setGridViewHeightBasedOnChildren(mySubCatGridVw, 3);

    }


    private String changedateFormat(String aSelectedTimeSTR) {
        Date dateObj = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            dateObj = sdf.parse(aSelectedTimeSTR);
            System.out.println(dateObj);
            System.out.println(new SimpleDateFormat("H:mm").format(dateObj));
        } catch (final ParseException e) {
            e.printStackTrace();
        }

        return String.valueOf(new SimpleDateFormat("H:mm").format(dateObj));

    }

    private void GetAndloadData() {
        if (myConnectionManager.isConnectingToInternet()) {
            getCategoryDetailValues(ScheduleAppointmentActivity.this);
        } else {
            HNDHelper.showResponseErrorAlert(ScheduleAppointmentActivity.this, getResources().getString(R.string.nointernet_text));
        }
    }

    private void getCategoryDetailValues(Context aContext) {

        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParam = new HashMap<>();

        try {
            JSONArray aJsonArr = new JSONArray(mySelectedSubCatIdArr);
            System.out.println("----" + aJsonArr.toString());
            for (int a = 0; a < aJsonArr.length(); a++) {
                jsonParam.put("subcategory[" + a + "]", aJsonArr.getString(a));
            }
            SharedPreferences zipcode;
            zipcode = getApplicationContext().getSharedPreferences("zipcode", 0);
            String code = zipcode.getString("code", "");

            jsonParam.put("zipcode", code);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(ServiceConstant.CategoryDetail, Request.Method.POST, jsonParam, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("---------Categories detail response------------" + response);

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("1")) {
                        JSONObject aRespObj = object.getJSONObject("response");


                        JSONArray aMainArr = aRespObj.getJSONArray("category");
                        String estimate_text, estimate_count, estimate_hours;

                        if (aRespObj.has("overall_est_count")) {
                            estimate_count = aRespObj.getString("overall_est_count");
                            estimatecount = Integer.parseInt(estimate_count);
                        } else {
                            estimatecount = 0;
                        }

                        if (aRespObj.has("overall_job_count")) {
                            overall_job_count = aRespObj.getString("overall_job_count");

                        } else {
                            overall_job_count = "1";
                        }

                        if (aRespObj.has("cancellationMessage")) {
                            cancellationhint = aRespObj.getString("cancellationMessage");

                        } else {
                            cancellationhint = "";
                        }


                        if (aRespObj.has("good_estimation")) {
                            good_estimation = aRespObj.getString("good_estimation");

                        } else {
                            good_estimation = "";
                        }

                        if (aRespObj.has("procrew_phoneno")) {

                            SharedPreferences callprocrew;
                            callprocrew = getApplicationContext().getSharedPreferences("callprocrew", 0); // 0 - for private mode
                            SharedPreferences.Editor editorcallprocrew = callprocrew.edit();
                            editorcallprocrew.putString("call", "" + aRespObj.getString("procrew_phoneno"));
                            editorcallprocrew.apply();

                        }

                        if (aRespObj.has("estimate_hours")) {
                            estimate_hours = aRespObj.getString("estimate_hours");
                            estimatehour = Integer.parseInt(estimate_hours);
                        } else {
                            estimatehour = 0;
                        }


                        for (int a = 0; a < aMainArr.length(); a++) {
                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);
                            SubCategoryPojo aSubCatPojo = new SubCategoryPojo();

                            taskname.add(aSubCatObj.getString("id"));


                            aSubCatPojo.setSubCategoryName(aSubCatObj.getString("cat_name"));
                            aSubCatPojo.setEstimatetext(aSubCatObj.getString("cat_name"));


                            aSubCatPojo.setSubCategoryImage(aSubCatObj.getString("image"));
                            aSubCatPojo.setSubCategoryActiveIcon(aSubCatObj.getString("active_icon"));
                            aSubCatPojo.setSubCategoryInactiveIcon(aSubCatObj.getString("inactive_icon"));
                            aSubCatPojo.setSubCategoryId(aSubCatObj.getString("id"));
                            aSubCatPojo.setAvailability(aSubCatObj.getString("availability"));
                            if (aSubCatObj.getString("availability").equals("1")) {


                                if (aSubCatObj.has("fare")) {

                                    if (aSubCatObj.getJSONObject("fare").has("jobamount")) {
                                        aSubCatPojo.setSubCategoryHours(aSubCatObj.getJSONObject("fare").getString("jobhours"));
                                        if (!mySession.getLogInStatus()) {
                                            aSubCatPojo.setSubCategoryHourlyRate("$" + aSubCatObj.getJSONObject("fare").getString("jobamount"));
                                        } else {
                                            aSubCatPojo.setSubCategoryHourlyRate(mySession.getCurrencySymbol() + aSubCatObj.getJSONObject("fare").getString("jobamount"));
                                        }

                                    } else {
                                        aSubCatPojo.setSubCategoryHours("0");
                                        aSubCatPojo.setSubCategoryHourlyRate("");
                                    }
                                }
                            } else {
                                aSubCatPojo.setSubCategoryHours("0");
                                aSubCatPojo.setSubCategoryHourlyRate("");
                            }
                            aSubCatPojo.setSubCategoryJobType(aSubCatObj.getString("job_type"));
                            aSubCatPojo.setJoborestimate(aSubCatObj.getString("whatsincluded"));


                            mySubCategryArrList.add(aSubCatPojo);
                            //  mySession.putCurrencySymbol(CurrencySymbolConverter.getCurrencySymbol(aSubCatObj.getString("currency")));

                        }

                        /*The following function will enable the selected category in GridView
                         * due to the client flow change, I'm disabling it.*/
//                        loadDataInAdapter();


                        estimate_text = "";
                        for (int hj = 0; hj < mySubCategryArrList.size(); hj++) {

                            if (mySubCategryArrList.get(hj).getSubCategoryJobType().equalsIgnoreCase("Estimate")) {
                                String valueforadd = mySubCategryArrList.get(hj).getEstimatetext();
                                if (valueforadd.equalsIgnoreCase("") || valueforadd.equalsIgnoreCase(" ") || valueforadd == null || valueforadd.equalsIgnoreCase("null")) {

                                } else {
                                    estimate_text += valueforadd + ",";
                                }

                            }

                        }

                        if (estimate_text.length() > 2) {
                            if (estimate_text.endsWith(",")) {
                                estimate_text = estimate_text.substring(0, estimate_text.length() - 1);
                            }

                            higlighttext.setText(estimate_text + " " + getResources().getString(R.string.class_schedule_appoitment_booking_requires));
                            higlighttext.setVisibility(View.VISIBLE);
                            myEstimateInfoLAY.setVisibility(View.GONE);
                        } else {
                            myEstimateInfoLAY.setVisibility(View.GONE);
                            higlighttext.setVisibility(View.GONE);
                        }

                        addvalue = 0;

                        for (int hj = 0; hj < mySubCategryArrList.size(); hj++) {
                            int valueforadd = Integer.parseInt(mySubCategryArrList.get(hj).getSubCategoryHours());
                            addvalue = addvalue + valueforadd;
                        }

                        if (estimatecount == 0) {

                        } else {
                            addvalue = addvalue + estimatehour;
                        }
                        if (addvalue == 0) {
                            myApprxmtTimeTXT.setText("");
                            myApprxmtTimeTXT.setVisibility(View.GONE);
                            saveoveralladdvalue = addvalue;
                        } else {
                            myApprxmtTimeTXT.setText(getResources().getString(R.string.class_schedule_appoitment_approx) + " " + addvalue + " " + getResources().getString(R.string.class_schedule_appoitment_hours));
                            saveoveralladdvalue = addvalue;

                        }

                        if (myConnectionManager.isConnectingToInternet()) {
                            mySelectedTimeSTR = "";
                            myContinueBookingBTN.setTextColor(getResources().getColor(R.color.darkcolor_1));
                            if (myTaskerDate != null || myTaskerDate.length() > 4) {
                                totalday = myTaskerDate;

                                if (totalday.contains("/")) {

                                    String[] dateArray = totalday.split("\\/");
                                    String day = dateArray[1];
                                    String month = dateArray[0];
                                    String year = dateArray[2];

                                    if (day.contains("0")) {
                                        day = day.replace("0", "");
                                    }

                                    if (month.contains("0")) {
                                        month = month.replace("0", "");
                                    }

                                    int intDay = Integer.parseInt(day);
                                    int intMonth = ((Integer.parseInt(month)) - 1);
                                    int intYear = Integer.parseInt(year);

                                    System.out.println("Task Date" + day + month + year);

                                    timeline.setSelectedDate(intYear, intMonth, intDay);
                                }
                            }
                            PostRequest(addvalue);
                        } else {
                            HNDHelper.showResponseErrorAlert(ScheduleAppointmentActivity.this, getResources().getString(R.string.nointernet_text));
                        }
                    } else {
                        if (object.getString("status").equals("0")) {

                            if (object.has("errors")) {
                                String strError = object.getString("errors");
                                HNDHelper.showResponseErrorAlert(ScheduleAppointmentActivity.this, strError);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent data = new Intent();
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, data);
        } else {
            getParent().setResult(Activity.RESULT_OK, data);
        }
        mySession.setNewSelection(true);
        finish();
    }

    private void postRequestConfirmBooking(final Context aContext) {

        Log.e("mySelectedTimeSTR", mySelectedTimeSTR);
        myDialogs.show();

        HashMap<String, String> params = new HashMap<>();
        mySelectedSubCatIdArr.clear();
        for (int i = 0; i < mySubCategryArrList.size(); i++) {
            if (mySubCategryArrList.get(i).getAvailability().equalsIgnoreCase("1")) {
                mySelectedSubCatIdArr.add(mySubCategryArrList.get(i).getSubCategoryId());
            }
        }
        try {
            JSONArray aJsonArrs = new JSONArray(mySelectedSubCatIdArr);
            for (int a = 0; a < aJsonArrs.length(); a++) {
                params.put("subcategory[" + a + "]", aJsonArrs.getString(a));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        params.put("pickup_date", totalday);
        params.put("pickup_time", mySelectedTimeSTR);

        SharedPreferences zip;
        zip = getApplicationContext().getSharedPreferences("zipcode", 0);
        if (zip.getString("Latitude", "").trim().isEmpty() || zip.getString("Longitude", "").trim().isEmpty()) {
            GetLatLonFromZIPCode(zip.getString("code", ""));
        } else {
            params.put("lat", zip.getString("Latitude", ""));
            params.put("long", zip.getString("Longitude", ""));
            params.put("zipcode", zip.getString("code", ""));

            ServiceRequest mRequest = new ServiceRequest(aContext);
            mRequest.makeServiceRequest(ServiceConstant.checkcratfman, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {


                    System.out.println("-------------get Confirm Category list Response----------------" + response);

                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getString("status").equalsIgnoreCase("1")) {

                            if (myDialogs.isShowing()) {
                                myDialogs.dismiss();
                            }

                            SharedPreferences getcards;
                            getcards = getApplicationContext().getSharedPreferences("selectresponse", 0); // 0 - for private mode
                            SharedPreferences.Editor editorgetcards = getcards.edit();
                            editorgetcards.putString("res", "" + response);
                            editorgetcards.putString("laterornow", "" + object.getString("task_type"));
                            editorgetcards.apply();

                            SharedPreferences getcard;
                            getcard = getApplicationContext().getSharedPreferences("scheduleapp", 0); // 0 - for private mode
                            SharedPreferences.Editor editor = getcard.edit();
                            editor.putString("date", "" + totalday);
                            editor.putString("time", "" + mySelectedTimeSTR);
                            editor.putString("catgeoryid", "" + mySession.getCategoryId());
                            editor.apply();

                            final SharedPreferences pref = getApplicationContext().getSharedPreferences("beforedismiss", 0);
                            String dataavaialble = pref.getString("before", "0");
                            if (dataavaialble.equalsIgnoreCase("1")) {

                                final PkDialog mDialog = new PkDialog(ScheduleAppointmentActivity.this);
                                mDialog.setDialogTitle(getResources().getString(R.string.activity_job_detail_TXT_title));
                                mDialog.setDialogMessage(getResources().getString(R.string.class_schedule_appoitment_stored));
                                mDialog.setPositiveButton(
                                        getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (cancellationhint.length() < 2) {

                                                    Intent aSelectCrftsMnIntent = new Intent(ScheduleAppointmentActivity.this, BookingConfirmationPreviewActivity.class);
                                                    startActivity(aSelectCrftsMnIntent);
                                                    mDialog.dismiss();
                                                } else {
                                                    mDialog.dismiss();
                                                    try {

                                                        final PkDialog mDialog = new PkDialog(ScheduleAppointmentActivity.this);
                                                        mDialog.setDialogMessage(cancellationhint);
                                                        mDialog.setPositiveButton(
                                                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        mDialog.dismiss();
                                                                        Intent aSelectCrftsMnIntent = new Intent(ScheduleAppointmentActivity.this, BookingConfirmationPreviewActivity.class);
                                                                        startActivity(aSelectCrftsMnIntent);


                                                                    }
                                                                }
                                                        );
                                                        mDialog.setNegativeButton(
                                                                getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        mDialog.dismiss();
                                                                    }
                                                                }
                                                        );

                                                        mDialog.show();

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }


                                            }
                                        }
                                );

                                mDialog.setNegativeButton(
                                        getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {


                                                if (cancellationhint.length() < 2) {


                                                    /*File dir = new File(Environment.getExternalStorageDirectory()
                                                            + "/Android/data/"
                                                            + SheduleAppointmentActivity.this.getApplicationContext().getPackageName()
                                                            + "/Files/Compressed");

                                                    if (dir.isDirectory()) {
                                                        String[] children = dir.list();
                                                        for (String child : children) {
                                                            new File(dir, child).delete();
                                                        }
                                                    }*/


                                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("beforedismiss", 0); // 0 - for private mode
                                                    SharedPreferences.Editor editor = pref.edit();
                                                    editor.putString("before", "0");
                                                    editor.apply();


                                                    /*Intent aSelectCrftsMnIntent = new Intent(SheduleAppointmentActivity.this, beforephotos.class);
                                                    startActivity(aSelectCrftsMnIntent);*/

                                                    Intent intent = new Intent(ScheduleAppointmentActivity.this, ChooseCraftsmanActivity.class);
                                                    startActivity(intent);
                                                    mDialog.dismiss();
                                                } else {
                                                    mDialog.dismiss();
                                                    try {

                                                        final PkDialog mDialog = new PkDialog(ScheduleAppointmentActivity.this);
                                                        mDialog.setDialogMessage(cancellationhint);
                                                        mDialog.setPositiveButton(
                                                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        mDialog.dismiss();
                                                                        SharedPreferences pref = getApplicationContext().getSharedPreferences("beforedismiss", 0); // 0 - for private mode
                                                                        SharedPreferences.Editor editor = pref.edit();
                                                                        editor.putString("before", "0");
                                                                        editor.apply();

                                                                        /*File dir = new File(Environment.getExternalStorageDirectory()
                                                                                + "/Android/data/"
                                                                                + SheduleAppointmentActivity.this.getApplicationContext().getPackageName()
                                                                                + "/Files/Compressed");

                                                                        if (dir.isDirectory()) {
                                                                            String[] children = dir.list();
                                                                            for (String child : children) {
                                                                                new File(dir, child).delete();
                                                                            }
                                                                        }*/

                                                                        /*Intent aSelectCrftsMnIntent = new Intent(SheduleAppointmentActivity.this, beforephotos.class);
                                                                        startActivity(aSelectCrftsMnIntent);*/

                                                                        Intent intent = new Intent(ScheduleAppointmentActivity.this, ChooseCraftsmanActivity.class);
                                                                        startActivity(intent);
                                                                    }
                                                                }
                                                        );
                                                        mDialog.setNegativeButton(
                                                                getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        mDialog.dismiss();
                                                                    }
                                                                }
                                                        );

                                                        mDialog.show();

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }

                                                }


                                            }
                                        }
                                );

                                mDialog.show();
                            } else {


                                if (cancellationhint.length() < 2) {
                                    SharedPreferences.Editor editors = pref.edit();
                                    editors.putString("before", "0");
                                    editors.apply();

                                    /*File dir = new File(Environment.getExternalStorageDirectory()
                                            + "/Android/data/"
                                            + SheduleAppointmentActivity.this.getApplicationContext().getPackageName()
                                            + "/Files/Compressed");

                                    if (dir.isDirectory()) {
                                        String[] children = dir.list();
                                        for (String child : children) {
                                            new File(dir, child).delete();
                                        }
                                    }*/


                                    /*Intent aSelectCrftsMnIntent = new Intent(SheduleAppointmentActivity.this, beforephotos.class);
                                    startActivity(aSelectCrftsMnIntent);*/

                                    Intent intent = new Intent(ScheduleAppointmentActivity.this, ChooseCraftsmanActivity.class);
                                    startActivity(intent);
                                } else {
                                    try {

                                        final PkDialog mDialog = new PkDialog(ScheduleAppointmentActivity.this);
                                        mDialog.setDialogMessage(cancellationhint);
                                        mDialog.setPositiveButton(
                                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        mDialog.dismiss();
                                                        SharedPreferences.Editor editors = pref.edit();
                                                        editors.putString("before", "0");
                                                        editors.apply();

                                                        /*File dir = new File(Environment.getExternalStorageDirectory()
                                                                + "/Android/data/"
                                                                + SheduleAppointmentActivity.this.getApplicationContext().getPackageName()
                                                                + "/Files/Compressed");

                                                        if (dir.isDirectory()) {
                                                            String[] children = dir.list();
                                                            for (String child : children) {
                                                                new File(dir, child).delete();
                                                            }
                                                        }*/

                                                       /* Intent aSelectCrftsMnIntent = new Intent(SheduleAppointmentActivity.this, beforephotos.class);
                                                        startActivity(aSelectCrftsMnIntent);*/

                                                        Intent intent = new Intent(ScheduleAppointmentActivity.this, ChooseCraftsmanActivity.class);
                                                        startActivity(intent);
                                                    }
                                                }
                                        );
                                        mDialog.setNegativeButton(
                                                getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        mDialog.dismiss();
                                                    }
                                                }
                                        );

                                        mDialog.show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }


                            }
                        } else {
                            if (myDialogs.isShowing()) {
                                myDialogs.dismiss();
                            }
                            HNDHelper.showResponseErrorAlert(ScheduleAppointmentActivity.this, object.getString("response"));
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block

                        e.printStackTrace();
                    }


                }

                @Override
                public void onErrorListener() {
                    if (myDialogs.isShowing()) {
                        myDialogs.dismiss();
                    }
                }
            });
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RequestPermissionCode) {
            if (grantResults.length > 0) {

                boolean call = grantResults[0] == PackageManager.PERMISSION_GRANTED;


                if (call) {

                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + procrewno));
                    startActivity(callIntent);
                }
            }
        }
    }

    private void PostRequest(int hours) {

        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        System.out.println("--------------Otp url-------------------" + ServiceConstant.availabledays + " hours----" + hours);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("date", totalday);

        jsonParams.put("overall_taskhours", "" + hours);

        SharedPreferences zipcode;
        zipcode = getApplicationContext().getSharedPreferences("zipcode", 0);
        String code = zipcode.getString("code", "");
        jsonParams.put("zipcode", code);

        SharedPreferences zip;
        zip = getApplicationContext().getSharedPreferences("zipcode", 0);
        jsonParams.put("lat", zip.getString("Latitude", ""));
        jsonParams.put("long", zip.getString("Longitude", ""));

        if (mySession.getCategoryId().equals("")) {
            System.out.println("--------------Otp url-------------------" + mySession.getCategoryId());
        } else {
            System.out.println("--------------Otp url-------------------no" + mySession.getCategoryId());
            if (mySession.getCategoryId().contains(",")) {
                String[] categoryi = mySession.getCategoryId().split(",");
                for (int j = 0; j < categoryi.length; j++) {
                    jsonParams.put("subcategory[" + j + "]", categoryi[j]);
                }
            } else {
                jsonParams.put("subcategory[" + 0 + "]", mySession.getCategoryId());
            }
        }


        ServiceRequest mRequest = new ServiceRequest(ScheduleAppointmentActivity.this);
        mRequest.makeServiceRequest(ServiceConstant.availabledays, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                System.out.println("--------------Otp reponse-------------------" + response);
                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    myTimeArrList.clear();

                    if (Sstatus.equalsIgnoreCase("1")) {
                        String todaydate = "";
                        if (object.has("today")) {
                            todaydate = object.getString("today");
                        }


                        hideselect.setVisibility(View.VISIBLE);
                        if (overall_job_count.equals("0") && totalday.equals(todaydate)) {
                            showerrormessage.setText(getResources().getString(R.string.noestimate));
                            hideselect.setVisibility(View.GONE);
                            myTimeRecyclerView.setVisibility(View.VISIBLE);
                            notimeavaiable.setVisibility(View.VISIBLE);
                            mySelectedTimeSTR = "";
                            myContinueBookingBTN.setTextColor(getResources().getColor(R.color.darkcolor_1));
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(new Date());
                            calendar.add(Calendar.DATE, 1);
                            timeline.setSelectedDate(
                                    calendar.get(Calendar.YEAR),
                                    calendar.get(Calendar.MONTH),
                                    calendar.get(Calendar.DATE)
                            );
                            Job_Type = "free estimate";
                            myApprxmtTimeTXT.setText(getResources().getString(R.string.class_schedule_appoitment_your) + " " + Job_Type + " " +
                                    getResources().getString(R.string.class_schedule_appoitment_will_take) + " " + addvalue + " " + getResources().getString(R.string.class_schedule_appoitment_hours));
                        } else {
                            myTimeRecyclerView.setVisibility(View.VISIBLE);
                            mySelectedTimeSTR = "";
                            myContinueBookingBTN.setTextColor(getResources().getColor(R.color.darkcolor_1));
                            JSONArray aMainArr = object.getJSONArray("all_slots_array");
                            if (aMainArr.length() > 0) {
                                notimeavaiable.setVisibility(View.GONE);
                                ButtonNextAvailability.setVisibility(View.GONE);
                            } else {
                                // showerrormessage.setText("There are no available times on this day.Please select some other day.");
                                showerrormessage.setText(getResources().getString(R.string.class_schedule_appoitment_categories));
                                notimeavaiable.setVisibility(View.VISIBLE);
                                ButtonNextAvailability.setVisibility(View.VISIBLE);
                            }
                            saveposva = 0;
                            int hj = 0;
                            for (int a = 0; a < aMainArr.length(); a++) {

                                JSONObject aSubCatObj = aMainArr.getJSONObject(a);
                                if (aSubCatObj.getString("available").equals("1")) {
                                    if (hj == 0) {
                                        saveposva = a;
                                        hj++;
                                    }
                                    AddItemsInTimeArr("false", aSubCatObj.getString("slot"), aSubCatObj.getString("from"), aSubCatObj.getString("available"));
                                }
                            }
                        }

                    } else {
                        myTimeArrList.clear();

                        if (object.has("errors")) {
                            //showerrormessage.setText(object.getString("errors"));
                            showerrormessage.setText(getResources().getString(R.string.class_schedule_appoitment_no_booking));
                        } else {
                            showerrormessage.setText(getResources().getString(R.string.class_schedule_appoitment_no_booking));
                        }

                        myTimeRecyclerView.setVisibility(View.VISIBLE);
                        notimeavaiable.setVisibility(View.VISIBLE);
                        mySelectedTimeSTR = "";
                        myContinueBookingBTN.setTextColor(getResources().getColor(R.color.darkcolor_1));
                    }
                    recyclerViewAdapter();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    public class SelectedSubCategoryAdapter extends BaseAdapter {
        private ArrayList<SubCategoryPojo> mySelectedSubCatArrList;
        private Context myContext;
        private SharedPreference mySession;
        private ArrayList<String> mySubCatIdList;

        private SelectedSubCategoryAdapter(ArrayList<SubCategoryPojo> mySelectedSubCatArrList, Context myContext) {
            this.mySelectedSubCatArrList = mySelectedSubCatArrList;
            this.myContext = myContext;
            mySession = new SharedPreference(myContext);
            mySubCatIdList = new ArrayList<String>();
            AddIdsToArr();
        }

        private void AddIdsToArr() {
            for (int a = 0; a < mySelectedSubCatArrList.size(); a++) {
                if (mySelectedSubCatArrList.get(a).getAvailability().equals("1")) {
                    mySubCatIdList.add(mySelectedSubCatArrList.get(a).getSubCategoryId());
                }
            }
        }

        @Override
        public int getCount() {
            return mySelectedSubCatArrList.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(myContext).inflate(R.layout.layout_inflater_selected_sub_category_grid_item, parent, false);
            }
            ImageView aSubCatPhotoGridSingle = convertView.findViewById(R.id.layout_inflate_selected_sub_category_grid_IMG);
            CustomTextView aSubCatNameTXT = convertView.findViewById(R.id.layout_inflate_selected_sub_category_grid_TXT_title);
            CustomTextView aJobAmount = convertView.findViewById(R.id.layout_inflater_selected_sub_category_grid_amount_TXT);
            LinearLayout myAddMoreLAY = convertView.findViewById(R.id.layout_inflater_selected_sub_category_add_more_LAY);
            LinearLayout myNormalLAY = convertView.findViewById(R.id.layout_inflater_sub_category_mainLAY);
            TextView aCategoryTypeTXT = convertView.findViewById(R.id.layout_inflater_selected_sub_category_grid_job_type_TXT);
            LinearLayout aRemoveLAY = convertView.findViewById(R.id.layout_inflate_selected_sub_category_grid_remove_LAY);
            LinearLayout myMainLAY = convertView.findViewById(R.id.layout_inflater_selected_sub_category_grid_item_mainLAY);
            myAddMoreLAY.setVisibility(View.GONE);

            if (position == mySelectedSubCatArrList.size()) {
                myNormalLAY.setVisibility(View.GONE);
                myAddMoreLAY.setVisibility(View.VISIBLE);
                aRemoveLAY.setVisibility(View.GONE);
                aJobAmount.setVisibility(View.GONE);
                aCategoryTypeTXT.setVisibility(View.GONE);
                return convertView;
            }

            if (mySelectedSubCatArrList.get(position).getJoborestimate().length() < 4) {
                aCategoryTypeTXT.setText("");
            } else {
                aCategoryTypeTXT.setText(getResources().getString(R.string.class_schedule_appoitment_included));
                // hint.setVisibility(View.VISIBLE);
            }

            /*Picasso.with(myContext)
                    .load(*//*ServiceConstant.Base_Url +*//* mySelectedSubCatArrList.get(position).getSubCategoryActiveIcon())
                    .into(aSubCatPhotoGridSingle);*/

            Picasso.get().load(mySelectedSubCatArrList.get(position).getSubCategoryActiveIcon())
                    .placeholder(R.drawable.icon_placeholder).into(aSubCatPhotoGridSingle);

            aSubCatNameTXT.setText(mySelectedSubCatArrList.get(position).getSubCategoryName());
            aJobAmount.setText(mySelectedSubCatArrList.get(position).getSubCategoryHourlyRate());

          /*  aCategoryTypeTXT.setText(mySelectedSubCatArrList.get(position).getSubCategoryJobType());
            aCategoryTypeTXT.setTextColor(Color.parseColor("#575757"));*/

            if (mySelectedSubCatArrList.get(position).getAvailability().equals("0")) {
                myMainLAY.setBackground(myContext.getResources().getDrawable(R.drawable.rect_bg_grey));
                aCategoryTypeTXT.setText(myContext.getResources().getString(R.string.layout_inflater_selected_sub_category_not_avail_TXT));
                aCategoryTypeTXT.setTextColor(Color.parseColor("#F85603"));
                aJobAmount.setVisibility(View.GONE);
                //aRemoveLAY.setEnabled(false);
            } else {
                myMainLAY.setBackground(myContext.getResources().getDrawable(R.drawable.rectanglr_border_bg));
            }

            aRemoveLAY.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<String> aSelectedIds = new ArrayList<String>(Arrays.asList(mySession.getCategoryId()
                            .split(",")));
                    for (int a = 0; a < aSelectedIds.size(); a++) {
                        if (mySelectedSubCatArrList != null && mySelectedSubCatArrList.size() > 0) {
                            if (mySelectedSubCatArrList.get(position).getSubCategoryId().equals(aSelectedIds.get(a))) {
                                showRemoveAlert(mySelectedSubCatArrList.get(position), a, position, aSelectedIds);
                            }
                        }
                    }
                }
            });

            myMainLAY.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (mySelectedSubCatArrList.get(position).getJoborestimate().length() < 4) {

                    } else {
                        HNDHelper.showWhatIncludePopup(getResources().getString(R.string.class_schedule_appoitment_included), myContext, mySelectedSubCatArrList.get(position).getJoborestimate());
                    }
                }
            });
            return convertView;
        }

        private void showRemoveAlert(final SubCategoryPojo aSubCategoryPojo, final int idPos, final int ArraylstPos, final ArrayList<String> aSelectedIds) {
            try {

                final PkDialog mDialog = new PkDialog(myContext);
                mDialog.setDialogTitle(getResources().getString(R.string.layout_inflater_selected_sub_category_remove_TXT));
                mDialog.setDialogMessage(myContext.getResources().getString(R.string.layout_inflater_selected_sub_category_remove_item_query) + " " + aSubCategoryPojo.getSubCategoryName() + "?");
                mDialog.setPositiveButton(
                        myContext.getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                aSelectedIds.remove(idPos);
                                mySelectedSubCatArrList.remove(ArraylstPos);


                                for (int a = 0; a < mySubCatIdList.size(); a++) {
                                    if (aSubCategoryPojo.getSubCategoryId().equals(mySubCatIdList.get(a))) {
                                        mySubCatIdList.remove(aSubCategoryPojo.getSubCategoryId());

                                        if (aSubCategoryPojo.getSubCategoryJobType().equalsIgnoreCase("Estimate")) {
                                            estimatecount = estimatecount - 1;
                                        }

                                        int addvalue = 0;
                                        for (int hj = 0; hj < mySubCategryArrList.size(); hj++) {

                                            String hourr = mySubCategryArrList.get(hj).getSubCategoryHours();
                                            if (mySubCategryArrList.get(hj).getSubCategoryHours().equalsIgnoreCase("") || mySubCategryArrList.get(hj).getSubCategoryHours().equalsIgnoreCase(" ")) {
                                                hourr = "0";
                                            }

                                            int valueforadd = Integer.parseInt(hourr);
                                            addvalue = addvalue + valueforadd;
                                        }

                                        if (estimatecount == 0) {
                                            addvalue = addvalue;
                                        } else {
                                            addvalue = addvalue + estimatehour;
                                        }


                                        if (addvalue == 0) {
                                            saveoveralladdvalue = addvalue;
                                            myApprxmtTimeTXT.setText("");
                                            myApprxmtTimeTXT.setVisibility(View.GONE);

                                        } else {
                                            saveoveralladdvalue = addvalue;
                                            /*myApprxmtTimeTXT.setText(getResources().getString(R.string.class_schedule_appoitment_approx) + " " + addvalue + " " + getResources().getString(R.string.class_schedule_appoitment_hours));*/
                                            myApprxmtTimeTXT.setText(getResources().getString(R.string.class_schedule_appoitment_your) + " " + Job_Type + " " +
                                                    getResources().getString(R.string.class_schedule_appoitment_will_take) + " " + addvalue + " " + getResources().getString(R.string.class_schedule_appoitment_hours));

                                        }

                                        String estimate_text = "";
                                        for (int hj = 0; hj < mySubCategryArrList.size(); hj++) {
                                            if (mySubCategryArrList.get(hj).getSubCategoryJobType().equalsIgnoreCase("Estimate")) {
                                                String valueforadd = mySubCategryArrList.get(hj).getEstimatetext();
                                                if (valueforadd.equalsIgnoreCase("") || valueforadd.equalsIgnoreCase(" ") || valueforadd == null || valueforadd.equalsIgnoreCase("null")) {
                                                } else {
                                                    estimate_text += valueforadd + ",";
                                                }
                                            }
                                        }

                                        if (estimate_text.length() > 2) {
                                            if (estimate_text.endsWith(",")) {
                                                estimate_text = estimate_text.substring(0, estimate_text.length() - 1);
                                            }

                                            higlighttext.setText(estimate_text + " " + getResources().getString(R.string.class_schedule_appoitment_free));
                                            higlighttext.setVisibility(View.VISIBLE);
                                            myEstimateInfoLAY.setVisibility(View.GONE);
                                        } else {
                                            myEstimateInfoLAY.setVisibility(View.GONE);
                                            higlighttext.setVisibility(View.GONE);
                                        }

                                    }
                                }
                                notifyDataSetChanged();
                                String aSelectedIdSTR = android.text.TextUtils.join(",", aSelectedIds);
                                mySession.setCategoryId(aSelectedIdSTR);

                                if (myConnectionManager.isConnectingToInternet()) {
                                    mySelectedTimeSTR = "";
                                    myContinueBookingBTN.setTextColor(getResources().getColor(R.color.darkcolor_1));
                                    PostRequest(saveoveralladdvalue);
                                } else {
                                    HNDHelper.showResponseErrorAlert(ScheduleAppointmentActivity.this, getResources().getString(R.string.nointernet_text));
                                }


                                mDialog.dismiss();
                            }
                        }
                );
                mDialog.setNegativeButton(
                        myContext.getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        }
                );

                mDialog.show();


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class CustomScrollListener extends RecyclerView.OnScrollListener {

        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            switch (newState) {
                case RecyclerView.SCROLL_STATE_IDLE:
                    System.out.println("The RecyclerView is not scrolling");
                    break;
                case RecyclerView.SCROLL_STATE_DRAGGING:
                    System.out.println("Scrolling now");
                    break;
                case RecyclerView.SCROLL_STATE_SETTLING:
                    System.out.println("Scroll Settling");

                    break;

            }

        }

        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            if (dx > 0) {
                System.out.println("Scrolled Right");
            } else if (dx < 0) {
                System.out.println("Scrolled Left");
            } else {
                System.out.println("No Horizontal Scrolled");

            }

            if (dy > 0) {
                System.out.println("Scrolled Downwards");

            } else if (dy < 0) {
                System.out.println("Scrolled Upwards");

            } else {
                System.out.println("No Vertical Scrolled");

            }
        }
    }

    //Function used to convert the Date to String.
    private String DateToString(Date date, String Format) {
        return new SimpleDateFormat(Format, Locale.getDefault()).format(date);
    }

    //Function used to convert String to Date.
    private Date StringToDate(String date, String Format) {
        Date OutPutDate = null;
        try {
            OutPutDate = new SimpleDateFormat(Format, Locale.getDefault()).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return OutPutDate;
    }

    private void GetLatLonFromZIPCode(String ZIPCode) {
        ServiceRequest mRequest = new ServiceRequest(this);
        mRequest.makeServiceRequest(ServiceConstant.GetLatLonFromZIPCode + map_key + "&sensor=false&components=postal_code:" + (ZIPCode), Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------LatLong  reponse-------------------" + response);

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        String myStatus = object.getString("status");
                        JSONArray place_object = object.getJSONArray("results");
                        if (myStatus.equalsIgnoreCase("OK")) {

                            if (place_object.length() > 0) {
                                JSONObject address_object = place_object.getJSONObject(0);
                                JSONObject geometryObj = address_object.getJSONObject("geometry");
                                JSONObject location = geometryObj.getJSONObject("location");
                                mySession.SetLatLong(location.getString("lat"), location.getString("lng"));
                                postRequestConfirmBooking(ScheduleAppointmentActivity.this);
                            } else {
                                Toast.makeText(ScheduleAppointmentActivity.this, "Unable to convert ZIP code location, Try again later", Toast.LENGTH_SHORT).show();
                            }
                        } else {

                            Toast.makeText(ScheduleAppointmentActivity.this, "Unable to convert ZIP code location, Try again later", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ScheduleAppointmentActivity.this, "Unable to convert ZIP code location, Try again later", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }
        });
    }

}