package com.handypro.activities.bookingFlow;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.ParentQuestionPojo;
import com.handypro.Pojo.SessionParentPojo;
import com.handypro.Pojo.SubCategoryPojo;
import com.handypro.Pojo.WorldPopulation;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.BoldCustomTextView;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class SelectedCategoryListActivity extends AppCompatActivity {

    private ArrayList<String> mySelectedSubCatIdArr;
    private String myZipCodeSTR, myLatitude, myLongitude, myTaskerDate;
    private ArrayList<WorldPopulation> arraylist = new ArrayList<>();
    private ListView listview;
    private ArrayList<SubCategoryPojo> mySubCategryArrList;
    private ArrayList<SubCategoryPojo> mySubCategryArrayList;

    private ArrayList<Object> parentcat;
    private ListViewAdapter adapter;
    private Button myHomePageBTN;
    private ConnectionDetector myConnectionManager;
    private ProgressDialogcreated myDialog;
    private SharedPreference mySession;
    private RelativeLayout myBackLAY;
    private LinearLayout LinearLayoutMainAddMoreLAY;
    ArrayList<SessionParentPojo> aSessionParentInfoList = null;
    private CustomTextView myBookNowTimeTXT;
    private LinearLayout myBookNowTimeLAY;
    private CustomButton myBookLaterBTN;
    private int addvalue = 0;
    int estimatehour = 0;
    int estimatecount = 0;
    private String mySelectedTimeSTR = "";
    String totalday = "";
    private int myDateCount = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_selected_list);

        mySubCategryArrayList = new ArrayList<>();
        Intent intent = getIntent();
        myConnectionManager = new ConnectionDetector(this);
        myDialog = new ProgressDialogcreated(this);
        mySession = new SharedPreference(this);
        mySelectedSubCatIdArr = (ArrayList<String>) intent.getSerializableExtra("selectedSubCategories");
        myLatitude = intent.getStringExtra("Latitude");
        myLongitude = intent.getStringExtra("Longitude");
        myZipCodeSTR = intent.getStringExtra("zipCode");
        myTaskerDate = intent.getStringExtra("task_date");

        Bundle args = intent.getBundleExtra("BUNDLE");
        ArrayList<Object> subCategoriesID = (ArrayList<Object>) args.getSerializable("SubCategoriesID");
        parentcat = (ArrayList<Object>) args.getSerializable("parentcat");
        aSessionParentInfoList = mySession.getSessionParentDetails();

        listview = findViewById(R.id.listview);
        myHomePageBTN = findViewById(R.id.fragment_home_page_bottom_next_BTN);
        myBackLAY = findViewById(R.id.activity_shedule_appoinment_LAY_back);
        LinearLayoutMainAddMoreLAY = findViewById(R.id.LinearLayoutMainAddMoreLAY);
        myBookNowTimeTXT = findViewById(R.id.fragment_home_page_bottom_next_TXT_booknow_time);
        myBookNowTimeLAY = findViewById(R.id.fragment_home_page_bottom_next_LAY_booknow);
        myBookLaterBTN = findViewById(R.id.fragment_home_page_bottom_next_BTN_booklater);

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String aSelectedIdSTR = android.text.TextUtils.join(",", mySelectedSubCatIdArr);
                mySession.setCategoryId(aSelectedIdSTR);
                mySession.setNewsearch(true);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        myBookNowTimeLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent aSelectCrftsMnIntent = new Intent(SelectedCategoryListActivity.this, JobDetailsActivity.class);
                aSelectCrftsMnIntent.putExtra("SCREENTYPE", "BOOKNOW");
                aSelectCrftsMnIntent.putExtra("selectedSubCategories", mySelectedSubCatIdArr);
                aSelectCrftsMnIntent.putExtra("zipCode", myZipCodeSTR);
                aSelectCrftsMnIntent.putExtra("Latitude", myLatitude);
                aSelectCrftsMnIntent.putExtra("Longitude", myLongitude);
                aSelectCrftsMnIntent.putExtra("task_date", myTaskerDate);
                aSelectCrftsMnIntent.putExtra("CATERORY_LIST", new Gson().toJson(mySubCategryArrayList));
                aSelectCrftsMnIntent.putExtra("SELECTED_DATE", mySelectedTimeSTR);
                aSelectCrftsMnIntent.putExtra("TOTAL_DAY", totalday);

                startActivity(aSelectCrftsMnIntent);
            }
        });
        LinearLayoutMainAddMoreLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myBackLAY.performClick();
            }
        });

        myHomePageBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent aSelectCrftsMnIntent = new Intent(SelectedCategoryListActivity.this, JobDetailsActivity.class);
                aSelectCrftsMnIntent.putExtra("selectedSubCategories", mySelectedSubCatIdArr);
                aSelectCrftsMnIntent.putExtra("SCREENTYPE", "PICKUPDATE");
                aSelectCrftsMnIntent.putExtra("zipCode", myZipCodeSTR);
                aSelectCrftsMnIntent.putExtra("Latitude", myLatitude);
                aSelectCrftsMnIntent.putExtra("Longitude", myLongitude);
                aSelectCrftsMnIntent.putExtra("task_date", myTaskerDate);
                startActivity(aSelectCrftsMnIntent);
            }
        });
        getCategoryDetailValues(SelectedCategoryListActivity.this);

        myBookLaterBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mySession.putSessionParentPojo(new ArrayList<SessionParentPojo>());
                mySelectedSubCatIdArr.clear();
                String aSelectedIdSTR = android.text.TextUtils.join(",", mySelectedSubCatIdArr);
                mySession.setCategoryId(aSelectedIdSTR);
                mySession.setNewsearch(true);
                finish();
            }
        });
        if (myConnectionManager.isConnectingToInternet()) {
            if (!myDialog.isShowing()) {
                myDialog.show();
            }
            HashMap<String, String> jsonParam = new HashMap<>();

            try {
                JSONArray aJsonArr = new JSONArray(mySelectedSubCatIdArr);
                System.out.println("----" + aJsonArr.toString());
                for (int a = 0; a < aJsonArr.length(); a++) {
                    jsonParam.put("subcategory[" + a + "]", aJsonArr.getString(a));
                }
                SharedPreferences zipcode;
                zipcode = getApplicationContext().getSharedPreferences("zipcode", 0);
                String code = zipcode.getString("code", "");

                jsonParam.put("zipcode", code);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ServiceRequest mRequest = new ServiceRequest(SelectedCategoryListActivity.this);
            mRequest.makeServiceRequest(ServiceConstant.CategoryDetail, Request.Method.POST, jsonParam, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("---------Categories detail response------------" + response);

                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }

                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getString("status").equals("1")) {
                            JSONObject aRespObj = object.getJSONObject("response");
                            JSONArray aMainArr = aRespObj.getJSONArray("category");
                            mySubCategryArrList = new ArrayList<>();
                            for (int a = 0; a < aMainArr.length(); a++) {
                                JSONObject aSubCatObj = aMainArr.getJSONObject(a);
                                SubCategoryPojo aSubCatPojo = new SubCategoryPojo();

                                /*for (int i = 0; i < SubCategoriesID.size(); i++) {
                                    if (SubCategoriesID.get(i).toString().equals(aSubCatObj.getString("id"))) {
                                        aSubCatPojo.setSubCategoryMainName(parentcat.get(i).toString());
                                    }
                                }*/

                                aSubCatPojo.setSubCategoryMainName(aSubCatObj.getJSONObject("parent").getString("name"));
                                aSubCatPojo.setSubCategoryName(aSubCatObj.getString("cat_name"));
                                aSubCatPojo.setSubCategoryActiveIcon(aSubCatObj.getString("active_icon"));
                                aSubCatPojo.setSubCategoryId(aSubCatObj.getString("id"));
                                aSubCatPojo.setJoborestimate(aSubCatObj.getString("whatsincluded"));
                                aSubCatPojo.setSubCategoryJobType(aSubCatObj.getString("job_type"));
                                if (aSubCatObj.getString("job_type").equalsIgnoreCase("Job")) {
                                    aSubCatPojo.setAmount(aSubCatObj.getJSONObject("fare").getString("total_fare"));
                                } else {
                                    aSubCatPojo.setAmount("");
                                }
                                mySubCategryArrList.add(aSubCatPojo);
                            }
                            // Pass results to ListViewAdapter Class
                            adapter = new ListViewAdapter(SelectedCategoryListActivity.this, mySubCategryArrList);


                            // Binds the Adapter to the ListView
                            listview.setAdapter(adapter);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onErrorListener() {
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                }
            });

        } else {
            HNDHelper.showResponseErrorAlert(SelectedCategoryListActivity.this, getResources().getString(R.string.nointernet_text));
        }


    }


    private void getCategoryDetailValues(Context aContext) {

        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParam = new HashMap<>();

        try {
            JSONArray aJsonArr = new JSONArray(mySelectedSubCatIdArr);
            System.out.println("----" + aJsonArr.toString());
            for (int a = 0; a < aJsonArr.length(); a++) {
                jsonParam.put("subcategory[" + a + "]", aJsonArr.getString(a));
            }
            SharedPreferences zipcode;
            zipcode = getApplicationContext().getSharedPreferences("zipcode", 0);
            String code = zipcode.getString("code", "");

            jsonParam.put("zipcode", code);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(ServiceConstant.CategoryDetail, Request.Method.POST, jsonParam, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("---------Categories detail response------------" + response);
                Log.e("cate", response);
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("1")) {
                        JSONObject aRespObj = object.getJSONObject("response");
                        String estimate_count, estimate_hours;
                        JSONArray aMainArr = aRespObj.getJSONArray("category");

                        for (int a = 0; a < aMainArr.length(); a++) {
                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);
                            SubCategoryPojo aSubCatPojo = new SubCategoryPojo();

                            aSubCatPojo.setSubCategoryName(aSubCatObj.getString("cat_name"));
                            aSubCatPojo.setEstimatetext(aSubCatObj.getString("cat_name"));

                            aSubCatPojo.setSubCategoryImage(aSubCatObj.getString("image"));
                            aSubCatPojo.setSubCategoryActiveIcon(aSubCatObj.getString("active_icon"));
                            aSubCatPojo.setSubCategoryInactiveIcon(aSubCatObj.getString("inactive_icon"));
                            aSubCatPojo.setSubCategoryId(aSubCatObj.getString("id"));
                            aSubCatPojo.setAvailability(aSubCatObj.getString("availability"));
                            if (aSubCatObj.getString("availability").equals("1")) {

                                if (aSubCatObj.has("fare")) {

                                    if (aSubCatObj.getJSONObject("fare").has("jobamount")) {
                                        aSubCatPojo.setSubCategoryHours(aSubCatObj.getJSONObject("fare").getString("jobhours"));
                                        if (!mySession.getLogInStatus()) {
                                            aSubCatPojo.setSubCategoryHourlyRate("$" + aSubCatObj.getJSONObject("fare").getString("jobamount"));
                                        } else {
                                            aSubCatPojo.setSubCategoryHourlyRate(mySession.getCurrencySymbol() + aSubCatObj.getJSONObject("fare").getString("jobamount"));
                                        }

                                    } else {
                                        aSubCatPojo.setSubCategoryHours("0");
                                        aSubCatPojo.setSubCategoryHourlyRate("");
                                    }
                                }
                            } else {
                                aSubCatPojo.setSubCategoryHours("0");
                                aSubCatPojo.setSubCategoryHourlyRate("");
                            }
                            aSubCatPojo.setSubCategoryJobType(aSubCatObj.getString("job_type"));
                            aSubCatPojo.setJoborestimate(aSubCatObj.getString("whatsincluded"));
                            mySubCategryArrayList.add(aSubCatPojo);

                        }

                        if (aRespObj.has("overall_est_count")) {
                            estimate_count = aRespObj.getString("overall_est_count");
                            estimatecount = Integer.parseInt(estimate_count);
                        } else {
                            estimatecount = 0;
                        }
                        if (aRespObj.has("estimate_hours")) {
                            estimate_hours = aRespObj.getString("estimate_hours");
                            estimatehour = Integer.parseInt(estimate_hours);
                        } else {
                            estimatehour = 0;
                        }
                        addvalue = 0;

                        for (int hj = 0; hj < mySubCategryArrayList.size(); hj++) {
                            int valueforadd = Integer.parseInt(mySubCategryArrayList.get(hj).getSubCategoryHours());
                            addvalue = addvalue + valueforadd;
                        }

                        if (estimatecount == 0) {
                        } else {
                            addvalue = addvalue + estimatehour;
                        }
                        PostRequest(addvalue);
                    } else {
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }
        });
    }


    private void PostRequest(final int hours) {


        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        int thisYear = calendar.get(Calendar.YEAR);
        final int thisMonth = calendar.get(Calendar.MONTH);
        int thisDay = calendar.get(Calendar.DAY_OF_MONTH);

        String month = "" + (thisMonth + 1);
        String day = "" + thisDay;

        if ((thisMonth + 1) <= 9) {
            month = "0" + (thisMonth + 1);
        }
        if (thisDay <= 9) {
            day = "0" + thisDay;
        }

        totalday = month + "/" + day + "/" + thisYear;


        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("date", totalday);

        jsonParams.put("overall_taskhours", "" + hours);

        SharedPreferences zipcode;
        zipcode = getApplicationContext().getSharedPreferences("zipcode", 0);
        String code = zipcode.getString("code", "");
        jsonParams.put("zipcode", code);

        SharedPreferences zip;
        zip = getApplicationContext().getSharedPreferences("zipcode", 0);
        jsonParams.put("lat", zip.getString("Latitude", ""));
        jsonParams.put("long", zip.getString("Longitude", ""));

        if (mySession.getCategoryId().equals("")) {
        } else {
            if (mySession.getCategoryId().contains(",")) {
                String[] categoryi = mySession.getCategoryId().split(",");
                for (int j = 0; j < categoryi.length; j++) {
                    jsonParams.put("subcategory[" + j + "]", categoryi[j]);
                }
            } else {
                jsonParams.put("subcategory[" + 0 + "]", mySession.getCategoryId());
            }
        }

        ServiceRequest mRequest = new ServiceRequest(SelectedCategoryListActivity.this);
        mRequest.makeServiceRequest(ServiceConstant.availabledays, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("response", response);
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                String Sstatus = "", aDateStr = "", aTimeStr = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject aRegObject = object.getJSONObject("req.body");
                        if (aRegObject.length() > 0) {
                            SimpleDateFormat spf = null;
                            Date newDate = null;
                            try {
                                spf = new SimpleDateFormat("MM/dd/yyyy");
                                newDate = spf.parse(aRegObject.getString("date"));
                                spf = new SimpleDateFormat("MMM dd");
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            aDateStr = spf.format(newDate);
                        }
                        JSONArray aSlotArray = object.getJSONArray("all_slots_array");

                        if(aSlotArray.length()==0){
                            myBookNowTimeLAY.setVisibility(View.GONE);
                        }

                        for (int y = 0; y < aSlotArray.length(); y++) {
                            JSONObject aSlotObject = aSlotArray.getJSONObject(y);
                            if (aSlotObject.getString("available").equalsIgnoreCase("1")) {
                                aTimeStr = aSlotObject.getString("slot");
                                mySelectedTimeSTR = aSlotObject.getString("from");
                                break;
                            }
                        }
                        myBookNowTimeTXT.setText(aDateStr + ", " + aTimeStr);

                    } else if (Sstatus.equalsIgnoreCase("0")) {
                        myDateCount = myDateCount + 1;
                        postnextday(hours);

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void postnextday(int hours) {

        if (myDateCount == 7) {
            myBookNowTimeLAY.setVisibility(View.GONE);
            return;
        }
        String totalday = "";
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, myDateCount);
        int thisYear = calendar.get(Calendar.YEAR);
        final int thisMonth = calendar.get(Calendar.MONTH);
        int thisDay = calendar.get(Calendar.DAY_OF_MONTH);

        String month = "" + (thisMonth + 1);
        String day = "" + thisDay;

        if ((thisMonth + 1) <= 9) {
            month = "0" + (thisMonth + 1);
        }
        if (thisDay <= 9) {
            day = "0" + thisDay;
        }

        totalday = month + "/" + day + "/" + thisYear;

        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("date", totalday);

        jsonParams.put("overall_taskhours", "" + hours);

        SharedPreferences zipcode;
        zipcode = getApplicationContext().getSharedPreferences("zipcode", 0);
        String code = zipcode.getString("code", "");
        jsonParams.put("zipcode", code);

        SharedPreferences zip;
        zip = getApplicationContext().getSharedPreferences("zipcode", 0);
        jsonParams.put("lat", zip.getString("Latitude", ""));
        jsonParams.put("long", zip.getString("Longitude", ""));

        if (mySession.getCategoryId().equals("")) {
        } else {
            if (mySession.getCategoryId().contains(",")) {
                String[] categoryi = mySession.getCategoryId().split(",");
                for (int j = 0; j < categoryi.length; j++) {
                    jsonParams.put("subcategory[" + j + "]", categoryi[j]);
                }
            } else {
                jsonParams.put("subcategory[" + 0 + "]", mySession.getCategoryId());
            }
        }

        ServiceRequest mRequest = new ServiceRequest(SelectedCategoryListActivity.this);
        mRequest.makeServiceRequest(ServiceConstant.availabledays, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("response", response);
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                String Sstatus = "", aDateStr = "", aTimeStr = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject aRegObject = object.getJSONObject("req.body");
                        if (aRegObject.length() > 0) {
                            SimpleDateFormat spf = null;
                            Date newDate = null;
                            try {
                                spf = new SimpleDateFormat("MM/dd/yyyy");
                                newDate = spf.parse(aRegObject.getString("date"));
                                spf = new SimpleDateFormat("MMM dd");
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            aDateStr = spf.format(newDate);
                        }

                        JSONArray aSlotArray = object.getJSONArray("all_slots_array");

                        if(aSlotArray.length()==0){
                            myBookNowTimeLAY.setVisibility(View.GONE);
                        }
                        for (int y = 0; y < aSlotArray.length(); y++) {
                            JSONObject aSlotObject = aSlotArray.getJSONObject(y);

                            if (aSlotObject.getString("available").equalsIgnoreCase("1")) {
                                aTimeStr = aSlotObject.getString("slot");
                                mySelectedTimeSTR = aSlotObject.getString("from");
                                break;
                            }
                        }

                        myBookNowTimeTXT.setText(aDateStr + "," + aTimeStr);
                    } else if (Sstatus.equalsIgnoreCase("0")) {

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    public class ListViewAdapter extends BaseAdapter {

        // Declare Variables
        Context mContext;
        LayoutInflater inflater;
        private ArrayList<SubCategoryPojo> subCategoryPojosArrayList;
        private ArrayList<WorldPopulation> fullarray;
        ArrayList<SessionParentPojo> aSessionParentInfoList = null;
        private SharedPreference mySession;

        ListViewAdapter(Context context, ArrayList<SubCategoryPojo> subCategoryPojosArrayList) {
            mContext = context;
            this.subCategoryPojosArrayList = subCategoryPojosArrayList;
            inflater = LayoutInflater.from(mContext);
            mySession = new SharedPreference(mContext);
            aSessionParentInfoList = mySession.getSessionParentDetails();
        }

        public class ViewHolder {
            TextView SubCategoryName, MainCategoryName, TextViewWhatsInclude, TextViewAmount, questionTXT;
            ImageView showimage, ImageViewRemove;
            LinearLayout LinearLayoutAddMoreLAY, aTotalLAY;
            CustomTextView aYouSaveTXT, aRegisterTXT;
            BoldCustomTextView aFreeEstimateTXT, aTotalAmtTXT;
        }

        @Override
        public int getCount() {
            return subCategoryPojosArrayList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View view, ViewGroup parent) {
            final ListViewAdapter.ViewHolder holder;
            if (view == null) {
                holder = new ListViewAdapter.ViewHolder();
                view = inflater.inflate(R.layout.listview_selected_item, null);
                // Locate the TextViews in listview_item.xml
                holder.showimage = view.findViewById(R.id.layout_inflate_sub_category_IMG);
                holder.ImageViewRemove = view.findViewById(R.id.ImageViewRemove);
                holder.SubCategoryName = view.findViewById(R.id.SubCategoryName);
                holder.MainCategoryName = view.findViewById(R.id.MainCategoryName);
                holder.TextViewWhatsInclude = view.findViewById(R.id.TextViewWhatsInclude);
                holder.LinearLayoutAddMoreLAY = view.findViewById(R.id.LinearLayoutAddMoreLAY);
                holder.TextViewAmount = view.findViewById(R.id.TextViewAmount);
                holder.questionTXT = view.findViewById(R.id.listview_selected_item_TXT_question);

                holder.aTotalLAY = view.findViewById(R.id.listview_selected_item_LAY_total);
                holder.aRegisterTXT = view.findViewById(R.id.listview_selected_item_TXT_register);
                holder.aYouSaveTXT = view.findViewById(R.id.listview_selected_item_TXT_yousave);
                holder.aTotalAmtTXT = view.findViewById(R.id.listview_selected_item_TXT_total_amt);
                holder.aFreeEstimateTXT = view.findViewById(R.id.listview_selected_item_TXT_free_estimate);

                view.setTag(holder);
            } else {
                holder = (ListViewAdapter.ViewHolder) view.getTag();
            }
            holder.LinearLayoutAddMoreLAY.setVisibility(View.GONE);
            final SubCategoryPojo subCategoryPojo = subCategoryPojosArrayList.get(position);

            //  bookingtimecolor
            Picasso.get().load(/*ServiceConstant.Base_Url +*/ subCategoryPojo.getSubCategoryActiveIcon())
                    .resize(120, 120)
                    .into(holder.showimage);

            if (subCategoryPojo.getSubCategoryJobType().equalsIgnoreCase("Job")) {
                holder.aTotalLAY.setVisibility(View.VISIBLE);
                holder.aFreeEstimateTXT.setVisibility(View.GONE);
                holder.aTotalAmtTXT.setText("Total" + " " + mySession.getCurrencySymbol() + subCategoryPojosArrayList.get(position).getAmount());
                float rCtWt = Float.parseFloat(subCategoryPojosArrayList.get(position).getAmount());
                holder.aYouSaveTXT.setText("you save " + " " + mySession.getCurrencySymbol() + Math.round(((rCtWt * 20) / 100)));
                float aTotal = ((rCtWt * 20) / 100) + Float.parseFloat(subCategoryPojosArrayList.get(position).getAmount());
                holder.aRegisterTXT.setText("Reg." + " " + mySession.getCurrencySymbol() + Math.round(aTotal));
                holder.aRegisterTXT.setPaintFlags(holder.aRegisterTXT.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                holder.aTotalLAY.setVisibility(View.GONE);
                holder.aFreeEstimateTXT.setVisibility(View.VISIBLE);
            }

            String aFinalSelectedStr = "";

            if (aSessionParentInfoList != null && aSessionParentInfoList.size() > 0) {
                for (int h = 0; h < aSessionParentInfoList.size(); h++) {
                    if (subCategoryPojo.getSubCategoryId().equalsIgnoreCase(aSessionParentInfoList.get(h).getSessionParentId())) {
                        for (int u = 0; u < aSessionParentInfoList.get(h).getaPojo().size(); u++) {
                            String aSelectedStr = "";
                            for (int n = 0; n < aSessionParentInfoList.get(h).getaPojo().get(u).getAnswerpojo().size(); n++) {
                                if (aSessionParentInfoList.get(h).getaPojo().get(u).getAnswerpojo().get(n).isAnswerselected()) {
                                    if (aSelectedStr.equalsIgnoreCase("")) {
                                        aSelectedStr = aSessionParentInfoList.get(h).getaPojo().get(u).getAnswerpojo().get(n).getAnswertitle();
                                    } else {
                                        aSelectedStr = aSelectedStr + "," + aSessionParentInfoList.get(h).getaPojo().get(u).getAnswerpojo().get(n).getAnswertitle();
                                    }
                                }
                            }
                            ParentQuestionPojo aParentQuestionPojo = aSessionParentInfoList.get(h).getaPojo().get(u);
                            String aAnswerStr = aParentQuestionPojo.getQuestionAnsView();
                            // Log.e("Split111", aAnswerStr.replace("{{answer}}", aSelectedStr));
                            if (aFinalSelectedStr.equalsIgnoreCase("")) {
                                aFinalSelectedStr = aAnswerStr.replace("{{answer}}", aSelectedStr);
                            } else {
                                aFinalSelectedStr = aFinalSelectedStr + "\n" + aAnswerStr.replace("{{answer}}", aSelectedStr);
                            }
                            //  Log.e("aFinalSelectedStr", aFinalSelectedStr);
                            holder.questionTXT.setVisibility(View.VISIBLE);
                            holder.questionTXT.setText(aFinalSelectedStr);
                        }
                    }
                }
                if (aFinalSelectedStr.equalsIgnoreCase("")) {
                    holder.questionTXT.setVisibility(View.GONE);
                }
            } else {
                holder.questionTXT.setVisibility(View.GONE);
            }

            // Set the results into TextViews
            holder.SubCategoryName.setText(subCategoryPojo.getSubCategoryName());
            holder.MainCategoryName.setText(subCategoryPojo.getSubCategoryMainName());

            if (subCategoryPojo.getJoborestimate().isEmpty()) {
                holder.TextViewWhatsInclude.setVisibility(View.GONE);
            } else {
                holder.TextViewWhatsInclude.setVisibility(View.VISIBLE);
            }

            holder.TextViewWhatsInclude.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HNDHelper.showWhatIncludePopup(getResources().getString(R.string.class_schedule_appoitment_included),
                            mContext, subCategoryPojo.getJoborestimate());
                }
            });

            if (position == subCategoryPojosArrayList.size() - 1) {
                Log.e("Position", "True");
                holder.LinearLayoutAddMoreLAY.setVisibility(View.VISIBLE);
            } else {
                Log.e("Position", "False");
            }

            if (subCategoryPojosArrayList.get(position).getAmount().isEmpty())
                holder.TextViewAmount.setText("");
            else
                holder.TextViewAmount.setText(mySession.getCurrencySymbol() + subCategoryPojosArrayList.get(position).getAmount());

            holder.LinearLayoutAddMoreLAY.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String aSelectedIdSTR = android.text.TextUtils.join(",", mySelectedSubCatIdArr);
                    mySession.setCategoryId(aSelectedIdSTR);
                    mySession.setNewsearch(true);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

            holder.ImageViewRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final PkDialog mDialog = new PkDialog(mContext);
                    mDialog.setDialogTitle(getResources().getString(R.string.layout_inflater_selected_sub_category_remove_TXT));
                    mDialog.setDialogMessage(mContext.getResources().getString(R.string.layout_inflater_selected_sub_category_remove_item_query) + " " + subCategoryPojo.getSubCategoryName() + "?");
                    mDialog.setPositiveButton(
                            mContext.getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (subCategoryPojosArrayList.size() - 1 == 0) {
                                        LinearLayoutMainAddMoreLAY.setVisibility(View.VISIBLE);
                                        myHomePageBTN.setVisibility(View.GONE);
                                        listview.setVisibility(View.GONE);
                                    }
                                    if (aSessionParentInfoList != null && aSessionParentInfoList.size() > 0) {
                                        for (int h = 0; h < aSessionParentInfoList.size(); h++) {
                                            if (subCategoryPojo.getSubCategoryId().equalsIgnoreCase(aSessionParentInfoList.get(h).getSessionParentId())) {
                                                aSessionParentInfoList.remove(h);
                                            }
                                        }
                                    }
                                    mySession.putSessionParentPojo(aSessionParentInfoList);
                                    mySelectedSubCatIdArr.remove(position);
                                    subCategoryPojosArrayList.remove(position);
                                    notifyDataSetChanged();
                                    String aSelectedIdSTR = android.text.TextUtils.join(",", mySelectedSubCatIdArr);
                                    mySession.setCategoryId(aSelectedIdSTR);
                                    mDialog.dismiss();
                                }
                            });

                    mDialog.setNegativeButton(
                            mContext.getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            }
                    );
                    mDialog.show();
                }
            });
            return view;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        String aSelectedIdSTR = android.text.TextUtils.join(",", mySelectedSubCatIdArr);
        mySession.setCategoryId(aSelectedIdSTR);
        mySession.setNewsearch(true);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
