package com.handypro.activities;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.ListView;

import com.handypro.Pojo.JobFarePojo;
import com.handypro.R;
import com.handypro.adapters.JobFareAdapter;
import com.handypro.textview.BoldCustomTextView;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/22/2018.
 */

public class JobFairActivity extends AppCompatActivity {
    private ListView myJobfrListVw;
    private BoldCustomTextView myJobIdTXT;
    private ArrayList<JobFarePojo> myJobFareArrList;
    private JobFareAdapter myJobFrAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_fair);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInitialize();
        loadDummyData();
    }

    private void loadDummyData() {
        myJobIdTXT.setText("HND 901283");
        JobFarePojo aJbFrPojo = new JobFarePojo();

        aJbFrPojo.setJobFareAmount("100");
        aJbFrPojo.setJobFareCurrency("$");
        aJbFrPojo.setJobFareTitle("Base Price");

        JobFarePojo aJbFrPojo1 = new JobFarePojo();

        aJbFrPojo1.setJobFareAmount("1 hour");
        aJbFrPojo1.setJobFareCurrency("");
        aJbFrPojo1.setJobFareTitle("Total Hours");

        JobFarePojo aJbFrPojo2 = new JobFarePojo();

        aJbFrPojo2.setJobFareAmount("70");
        aJbFrPojo2.setJobFareCurrency("$");
        aJbFrPojo2.setJobFareTitle("Hourly Rate");

        JobFarePojo aJbFrPojo3 = new JobFarePojo();

        aJbFrPojo3.setJobFareAmount("10");
        aJbFrPojo3.setJobFareCurrency("$");
        aJbFrPojo3.setJobFareTitle("Service Taax");

        JobFarePojo aJbFrPojo4 = new JobFarePojo();

        aJbFrPojo4.setJobFareAmount("20");
        aJbFrPojo4.setJobFareCurrency("$");
        aJbFrPojo4.setJobFareTitle("Miscellaneaous amount");

        JobFarePojo aJbFrPojo5 = new JobFarePojo();

        aJbFrPojo5.setJobFareAmount("20");
        aJbFrPojo5.setJobFareCurrency("$");
        aJbFrPojo5.setJobFareTitle("Miscellaneaous amount");

        myJobFareArrList.add(aJbFrPojo);
        myJobFareArrList.add(aJbFrPojo1);
        myJobFareArrList.add(aJbFrPojo2);
        myJobFareArrList.add(aJbFrPojo3);
        myJobFareArrList.add(aJbFrPojo4);
        myJobFareArrList.add(aJbFrPojo5);

        loadDataInAdapter();
    }

    private void loadDataInAdapter() {
        myJobFrAdapter = new JobFareAdapter(JobFairActivity.this,myJobFareArrList);
        myJobfrListVw.setAdapter(myJobFrAdapter);
    }

    private void classAndWidgetInitialize() {
        myJobFareArrList = new ArrayList<JobFarePojo>();
        myJobfrListVw = (ListView) findViewById(R.id.activity_job_fair_detail_listView);
        myJobIdTXT = (BoldCustomTextView) findViewById(R.id.activity_job_fair_TXT_job_id);
    }
}
