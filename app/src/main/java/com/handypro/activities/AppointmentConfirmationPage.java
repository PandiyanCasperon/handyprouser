package com.handypro.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.handypro.Pojo.acceptimage;
import com.handypro.R;
import com.handypro.activities.navigationMenu.bookimageadapter;
import com.handypro.sharedpreference.SharedPreference;

import org.json.JSONArray;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AppointmentConfirmationPage extends AppCompatActivity {

    private RelativeLayout Rl_back;
    private ImageView Im_backIcon;
    private TextView Tv_headerTitle, notifitext, TextViewAddress, youruploadedimage;
    private TextView Tv_title, Tv_orderId, Tv_date, Tv_serviceType, Tv_description, timearrival;
    private Button Bt_done;
    private String sMessage = "", sOrderId = "", sDate = "", sServiceType = "", sDescription = "", approx_taskhours = "", task_title = "", task_description = "", task_images = "";
    int converttohours = 1;
    private String getMessage = "";
    private SharedPreference mySession;
    TextView totalamount, amounttotal;
    private RecyclerView beforerecycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.appointment_confirmation_page);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mySession = new SharedPreference(AppointmentConfirmationPage.this);
        mySession.setCategoryId("");

        SharedPreferences prefss = getApplicationContext().getSharedPreferences("logintoconform", 0); // 0 - for private mode
        SharedPreferences.Editor editorss = prefss.edit();
        editorss.putString("lo", "1");
        editorss.apply();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("beforedismiss", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("before", "0");
        editor.apply();

        initializeHeaderBar();
        initialize();


        Bt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();


            }
        });

        Rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();


            }
        });
    }

    private void initializeHeaderBar() {
        LinearLayout headerBar = findViewById(R.id.headerBar_layout);
        Rl_back = headerBar.findViewById(R.id.headerBar_left_layout);
        Im_backIcon = headerBar.findViewById(R.id.headerBar_imageView);
        Tv_headerTitle = headerBar.findViewById(R.id.headerBar_title_textView);

        Tv_headerTitle.setText(getResources().getString(R.string.appointment_confirmation_label_title) + " " + mySession.getUserDetails().getUserName() + "!");
        Im_backIcon.setImageResource(R.drawable.icon_back_arrow);
    }

    private void initialize() {
        Tv_title = findViewById(R.id.appointment_confirmation_title_textView);
        Tv_orderId = findViewById(R.id.appointment_confirmation_booking_id_textView);
        Tv_date = findViewById(R.id.appointment_confirmation_booking_date_textView);
        Tv_serviceType = findViewById(R.id.appointment_confirmation_booking_service_textView);
        Tv_description = findViewById(R.id.appointment_confirmation_booking_description_textView);
        TextViewAddress = findViewById(R.id.TextViewAddress);
        Bt_done = findViewById(R.id.appointment_confirmation_page_done_button);

        notifitext = findViewById(R.id.notifitext);

        TextViewAddress.setText(mySession.GetFullAddress());

        SpannableStringBuilder builders = new SpannableStringBuilder();
        String red = "You will be notified ";
        SpannableString redSpannable = new SpannableString(red);
        redSpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, red.length(), 0);
        builders.append(redSpannable);
        String white = getResources().getString(R.string.longexcited);
        SpannableString whiteSpannable = new SpannableString(white);
        whiteSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, white.length(), 0);
        builders.append(whiteSpannable);
        notifitext.setText(builders, TextView.BufferType.SPANNABLE);


        SpannableStringBuilder builders2 = new SpannableStringBuilder();
        String mes = "If not, you will be notified ";
        SpannableString redSpannable1 = new SpannableString(mes);
        redSpannable1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, mes.length(), 0);
        builders2.append(redSpannable1);
        String mes1 = "of the next available date";
        SpannableString whiteSpannable1 = new SpannableString(mes1);
        whiteSpannable1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, mes1.length(), 0);
        builders2.append(whiteSpannable1);
        Tv_title.setText(builders2, TextView.BufferType.SPANNABLE);

        totalamount = findViewById(R.id.totalamount);
        amounttotal = findViewById(R.id.amounttotal);
        timearrival = findViewById(R.id.timearrival);
        beforerecycler_view = findViewById(R.id.beforerecycler_view);
        youruploadedimage = findViewById(R.id.youruploadedimage);

        Intent intent = getIntent();
        sMessage = intent.getStringExtra("IntentMessage");
        sOrderId = intent.getStringExtra("IntentJobID");
        sDate = intent.getStringExtra("IntentOrderDate");
        sServiceType = intent.getStringExtra("IntentServiceType");
        sDescription = intent.getStringExtra("IntentDescription");

        approx_taskhours = intent.getStringExtra("approx_taskhours");

        task_title = intent.getStringExtra("task_title");
        task_description = intent.getStringExtra("task_description");
        task_images = intent.getStringExtra("task_images");

        if (approx_taskhours.matches("[0-9]+")) {
            converttohours = Integer.parseInt(approx_taskhours);
        }

        String tot = intent.getStringExtra("totalfare");
        String time = intent.getStringExtra("IntentTime");

        if (tot.equals("0")) {
            totalamount.setVisibility(View.GONE);
            amounttotal.setVisibility(View.GONE);
        } else {
            totalamount.setVisibility(View.VISIBLE);
            amounttotal.setVisibility(View.VISIBLE);

            amounttotal.setText(tot);
        }


        //Tv_title.setText(sMessage);
        Tv_orderId.setText(sOrderId);

        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "EEEE MMMM  dd, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(sDate);
            str = outputFormat.format(date);

            String s = time;
            DateFormat f1 = new SimpleDateFormat("HH:mm"); //HH for hour of the day (0 - 23)
            Date d = f1.parse(s);
            DateFormat f2 = new SimpleDateFormat("hh:mm a");
            f2.format(d).toLowerCase(); // "12:18am"


            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            String currentDateandTime = sdf.format(d);
            Date datze = sdf.parse(currentDateandTime);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(datze);
            calendar.add(Calendar.HOUR, converttohours);

            SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
            String formattedDate = df.format(calendar.getTime());


            Tv_date.setText(str);
            timearrival.setText(getResources().getString(R.string.arrivaltime) + " " + f2.format(d).toUpperCase() + " - " + formattedDate);


        } catch (ParseException e) {
            e.printStackTrace();
        }


        SharedPreferences prefss = getApplicationContext().getSharedPreferences("categorynams", 0); // 0 - for private mode
        String catename = prefss.getString("names", "");

        if (catename.endsWith(",")) {
            catename = catename.substring(0, catename.length() - 1);
        }

        Tv_serviceType.setText(catename);

        Tv_description.setText("About To Do List: " + task_title + "\n" + "Most Important: " + task_description);
        List<acceptimage> movieList = new ArrayList<>();
        JSONArray aJsonArr = null;
        try {
            aJsonArr = new JSONArray(task_images);
            for (int a = 0; a < aJsonArr.length(); a++) {
                acceptimage movie = new acceptimage(aJsonArr.getString(a));
                movieList.add(movie);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (movieList.size() > 0) {
            beforerecycler_view.setVisibility(View.VISIBLE);
            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
            bookimageadapter mAdapter = new bookimageadapter(movieList, getApplicationContext());
            beforerecycler_view.setLayoutManager(layoutManager);
            beforerecycler_view.setAdapter(mAdapter);
        } else {
            beforerecycler_view.setVisibility(View.GONE);
            youruploadedimage.setVisibility(View.GONE);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    //-----------------Move Back on pressed phone back button------------------

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            return true;
        }
        return false;
    }


}
