package com.handypro.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.navigationMenu.Paymentcarddetails;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.CurrencySymbolConverter;
import com.handypro.volley.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class PaymentPageActivity extends AppCompatActivity {

    ImageView card, cash, pay, wallet;
    TextView add_coupon1;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    LinearLayout coupon;
    String couponcode;
    EditText add_coupon;
    String creditname = "";
    String payname = "";
    Button Apply_coupon;
    RelativeLayout Rl_book;
    TextView amount;
    String User_ID = "", Job_ID = "";
    private String sUserID = "", sJobID = "";
    private String sPaymentCode = "code";
    /*final int REQUEST_PAYPAL_PAYMENT=80;*/
    String currencyCode = "USD";
    String totalAmount = "33";
    private ServiceRequest mRequest;
    String totalamount;
    String mobileId;
    private SharedPreference sharedPreference;

    String jobid = "", franchisee_id = "";
    String USER_ID = "";
    TextView jobtextid;
    TextView cardtext;
    TextView paypaltext;
    String cashname = "";
    String walletname = "";
    TextView cashtext;
    TextView wallettext, acceptterms;
    private ProgressDialogcreated myDialog;
    ArrayList<String> array = new ArrayList<>();
    private static final String CONFIG_CLIENT_ID = "AQV_DV7Ist83_P8aREy8kzEtL1tAsk_eri7ydgsaiNBEHKcnx-lnq3LVJkdqiSxtxMG2keYwCU7r4gfm";
    String str_partiallyPaid = "";
    String Json_TaskId = "";
    private static final int REQUEST_PAYPAL_PAYMENT = 29;
    // String Url="http://45.55.241.170:3002/mobile/app/payment/by-gateway";

    CheckBox checkid;
    String loyaltymoney = "0", loyaltymoneystatus = "0", Flow = "", TotalPrepaymentAmount = "";
    private String select_payment = "false";
    LinearLayout layout_two;
    private ArrayList<String> IdList;
    private ArrayList<String> ValueList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_page);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            loyaltymoney = null;
            loyaltymoneystatus = null;
        } else {
            loyaltymoney = extras.getString("loyaltymoney");
            loyaltymoneystatus = extras.getString("loyaltymoneystatus");
        }


        if (getIntent().hasExtra("Type")) {
            Flow = getIntent().getStringExtra("Type");
            IdList = getIntent().getStringArrayListExtra("IdList");
            ValueList = getIntent().getStringArrayListExtra("ValueList");
            TotalPrepaymentAmount = String.valueOf(getIntent().getDoubleExtra("TotalPrepaymentAmount", 0.0));
        }

        Initialize();
        clickListners();

    }

    private void Initialize() {
        sharedPreference = new SharedPreference(this);
//        HashMap<String, String> task = sharedPreference.gettaskid();
//        taskid = task.get(SharedPreference.TASK_ID);
//
//        HashMap<String, String> job = sharedPreference.getjob();
//        jobid = job.get(SharedPreference.JOB_ID);
//
//        HashMap<String, String> user = sharedPreference.getUserDetails();
        User_ID = sharedPreference.getUserDetails().getUserId();


        SharedPreferences pref;
        pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
        Job_ID = pref.getString("jobid", "");


        cd = new ConnectionDetector(PaymentPageActivity.this);
        isInternetPresent = cd.isConnectingToInternet();

        layout_two = (LinearLayout) findViewById(R.id.layout_two);
        if (isInternetPresent) {
            if (Flow.equals("")) {
                startLoading();
                MakePayment_WebView_MobileID(ServiceConstant.Mobile_Id_url);
            } else if (Flow.equals("requesting_pre_payment")) {
                startLoading();
                MakePayment_WebView_MobileID(ServiceConstant.PrepaymentRequestForTransactionID);
            }

            // paymentrequest(ServiceConstant.paymentpageurl);
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_no_internet_message), Toast.LENGTH_LONG).show();
        }


        acceptterms = (TextView) findViewById(R.id.acceptterms);
        wallettext = (TextView) findViewById(R.id.Wallet_text);
        cashtext = (TextView) findViewById(R.id.Cash_text);
        cardtext = (TextView) findViewById(R.id.Card_text);
        paypaltext = (TextView) findViewById(R.id.Nte_text);
        amount = (TextView) findViewById(R.id.amounttext);
        jobtextid = (TextView) findViewById(R.id.jobidtext);
        card = (ImageView) findViewById(R.id.Card);
        cash = (ImageView) findViewById(R.id.Cash);
        pay = (ImageView) findViewById(R.id.pay);
        wallet = (ImageView) findViewById(R.id.Wallet);
        add_coupon1 = (TextView) findViewById(R.id.add_coupon);

        coupon = (LinearLayout) findViewById(R.id.Linear_Coupon);
        add_coupon = (EditText) findViewById(R.id.coupon_edit);
        Apply_coupon = (Button) findViewById(R.id.coupon_apply);
        Rl_book = (RelativeLayout) findViewById(R.id.linearlayout_footer_additem_final1);

        acceptterms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetPresent) {


                    // paymentPost(PaymentPageActivity.this, ServiceConstant.TERMS);


                    Intent intent = new Intent(PaymentPageActivity.this, CommonWebView.class);
                    intent.putExtra("url", franchisee_id);
                    intent.putExtra("header", getResources().getString(R.string.class_payment_page_terms));
                    intent.putExtra("type", "pay");
                    startActivity(intent);
                } else {
                    HNDHelper.showErrorAlert(PaymentPageActivity.this, getResources().getString(R.string.nointernet_text));

                }
            }
        });


        System.out.println("-------------User_ID in payment page----------------" + User_ID);
        System.out.println("-------------Jod_ID in payment page----------------" + Job_ID);

        Apply_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                couponcode = add_coupon.getText().toString();
                couponcode(ServiceConstant.couponcode_url);

            }
        });


//------------------------------------------------URL----------------------------------


        //------------------------------------------------URL----------------------------------

        sharedPreference = new SharedPreference(PaymentPageActivity.this);


        checkid = (CheckBox) findViewById(R.id.check_id);

        add_coupon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (coupon.getVisibility() == View.VISIBLE) {
                    coupon.setVisibility(View.GONE);
                } else {
                    coupon.setVisibility(View.VISIBLE);
                }
            }
        });
        Rl_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkid.isChecked()) {
                    if (select_payment.equalsIgnoreCase("false")) {


                        Toast.makeText(PaymentPageActivity.this, getResources().getString(R.string.payment_option_toast), Toast.LENGTH_SHORT).show();

                    } else if (select_payment.equalsIgnoreCase("card")) {
                        Intent intent = new Intent(PaymentPageActivity.this, Paymentcarddetails.class);
                        if (Flow.equals("requesting_pre_payment")) {
                            intent.putExtra("TotalPrepaymentAmount", TotalPrepaymentAmount);
                            intent.putExtra("Type", "requesting_pre_payment");
                            intent.putStringArrayListExtra("IdList", IdList);
                            intent.putStringArrayListExtra("ValueList", ValueList);
                        }
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                    } else if (select_payment.equalsIgnoreCase("cash")) {
                        if (Flow.equals("")) {
                            MakePayment_Cash(ServiceConstant.paybycah_url);
                        } else if (Flow.equals("requesting_pre_payment")) {
                            MakePayment_Cash(ServiceConstant.PrepaymentRequestByCash);
                        }
                    } else if (select_payment.equalsIgnoreCase("wallet")) {

                        if (Flow.equals("")) {
                            MakePayment_Wallet(ServiceConstant.paybycheque_url);
                        } else if (Flow.equals("requesting_pre_payment")) {
                            MakePayment_Wallet(ServiceConstant.PrepaymentRequestByCheque);
                        }
                    }
                } else {
                    Toast.makeText(PaymentPageActivity.this, getResources().getString(R.string.terms_condition_toast), Toast.LENGTH_SHORT).show();

                }
            }

        });


        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkid.isChecked()) {
                    // clickButtons((ImageView) v);
                    select_payment = "card";

                    clickButtons((ImageView) v);

                } else {
                    Toast.makeText(PaymentPageActivity.this, getResources().getString(R.string.terms_condition_toast), Toast.LENGTH_LONG).show();
                }


            }
        });

        cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkid.isChecked()) {
                    // clickButtons((ImageView) v);
                    select_payment = "cash";

                    clickButtons((ImageView) v);

                } else {
                    Toast.makeText(PaymentPageActivity.this, getResources().getString(R.string.terms_condition_toast), Toast.LENGTH_LONG).show();
                }


            }
        });


        wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkid.isChecked()) {
                    select_payment = "wallet";

                    clickButtons((ImageView) v);

                } else {
                    Toast.makeText(PaymentPageActivity.this, getResources().getString(R.string.terms_condition_toast), Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private void clickButtons(ImageView btn) {
        if (btn == card) {
            Log.e("card", "TAG  " + card.getTag());
            if (card.getTag() == "card_inactive") {
                card.setImageResource(R.drawable.card_active);
                cash.setImageResource(R.drawable.cash);
                pay.setImageResource(R.drawable.paypal);
                wallet.setImageResource(R.drawable.wallet);

                card.setTag("card_active");
                cash.setTag("cash_inactive");
                pay.setTag("pay_inactive");
                wallet.setTag("wallet_inactive");

                Log.e("card", "IF  " + card.getTag());

            } else {
                card.setImageResource(R.drawable.card);
                card.setTag("card_inactive");
                select_payment = "false";
                Log.e("card", "ELSE " + card.getTag());
            }
        } else if (btn == cash) {
            Log.e("cash", "TAG  " + cash.getTag());
            if (cash.getTag() == "cash_inactive") {
                cash.setImageResource(R.drawable.cash_active);
                card.setImageResource(R.drawable.card);
                pay.setImageResource(R.drawable.paypal);
                wallet.setImageResource(R.drawable.wallet);

                cash.setTag("cash_active");
                card.setTag("card_inactive");
                pay.setTag("pay_inactive");
                wallet.setTag("wallet_inactive");

                Log.e("cash", "IF  " + cash.getTag());

            } else {
                cash.setImageResource(R.drawable.cash);
                cash.setTag("cash_inactive");
                select_payment = "false";
                Log.e("cash", "ELSE " + cash.getTag());
            }
        } else if (btn == pay) {
            Log.e("pay", "TAG  " + pay.getTag());
            if (pay.getTag() == "pay_inactive") {
                pay.setImageResource(R.drawable.paypal_active);
                cash.setImageResource(R.drawable.cash);
                card.setImageResource(R.drawable.card);
                wallet.setImageResource(R.drawable.wallet);

                pay.setTag("pay_active");
                card.setTag("card_inactive");
                cash.setTag("cash_inactive");
                wallet.setTag("wallet_inactive");

                Log.e("pay", "IF  " + pay.getTag());

            } else {
                pay.setImageResource(R.drawable.paypal);
                pay.setTag("pay_inactive");
                select_payment = "false";
                Log.e("pay", "ELSE " + pay.getTag());
            }
        } else if (btn == wallet) {
            Log.e("wallet", "TAG  " + wallet.getTag());
            if (wallet.getTag() == "wallet_inactive") {
                wallet.setImageResource(R.drawable.card_active);
                cash.setImageResource(R.drawable.cash);
                pay.setImageResource(R.drawable.paypal);
                card.setImageResource(R.drawable.card);

                wallet.setTag("wallet_active");
                card.setTag("card_inactive");
                cash.setTag("cash_inactive");
                pay.setTag("pay_inactive");


                Log.e("wallet", "IF  " + wallet.getTag());

            } else {
                wallet.setImageResource(R.drawable.card);
                wallet.setTag("wallet_inactive");
                select_payment = "false";
                Log.e("wallet", "ELSE " + wallet.getTag());
            }
        }


    }


    private void clickListners() {
//        Card_IV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Card_IV.setImageResource(R.drawable.card_active);
//                Cash_IV.setImageResource(R.drawable.cash);
//                paypal_IV.setImageResource(R.drawable.paypal);
//                Wallet_IV.setImageResource(R.drawable.wallet);
//                Selected_Mode = "card";
//            }
//        });
//        Cash_IV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Card_IV.setImageResource(R.drawable.card);
//                Cash_IV.setImageResource(R.drawable.cash_active);
//                paypal_IV.setImageResource(R.drawable.paypal);
//                Wallet_IV.setImageResource(R.drawable.wallet);
//                Selected_Mode = "cash";
//            }
//        });
//        Wallet_IV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Wallet_IV.setImageResource(R.drawable.wallet_active);
//                Card_IV.setImageResource(R.drawable.card);
//                Cash_IV.setImageResource(R.drawable.cash);
//                paypal_IV.setImageResource(R.drawable.paypal);
//                Selected_Mode = "wallet";
//            }
//        });
//        paypal_IV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Wallet_IV.setImageResource(R.drawable.wallet);
//                Card_IV.setImageResource(R.drawable.card);
//                Cash_IV.setImageResource(R.drawable.cash);
//                paypal_IV.setImageResource(R.drawable.paypal_active);
//                Selected_Mode = "paypal";
//            }
//        });
//
//        makePayment_RL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (check_id.isChecked()) {
//                    if (Selected_Mode.equalsIgnoreCase("nil")) {
//                        Toast.makeText(PaymentPageActivity.this, getResources().getString(R.string.payment_option_toast), Toast.LENGTH_SHORT).show();
//                    } else {
//                        if (Selected_Mode.equalsIgnoreCase("card")) {
//                            Toast.makeText(PaymentPageActivity.this, "card process", Toast.LENGTH_SHORT).show();
////                            Intent intent = new Intent(PaymentPageActivity.this, Card_list_Details.class);
////                            intent.putExtra("USER_Id", UserID);
////                            intent.putExtra("JOB_Id", Booking_ID);
////                            intent.putExtra("Mobile_Id", mobileId);
////                            intent.putExtra("task_id", Task_ID);
//
////                            System.out.println("-------------chan ,mobilr_id----------------" + mobileId);
////                            startActivity(intent);
////                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                        } else if (Selected_Mode.equalsIgnoreCase("cash")) {
//                            if (cd.isConnectingToInternet()) {
//                                MakePayment_Cash(ServiceConstant.paybycah_url);
//                            }
//                            Toast.makeText(PaymentPageActivity.this, "cash process", Toast.LENGTH_SHORT).show();
//                        } else if (Selected_Mode.equalsIgnoreCase("paypal")) {
//                            if (cd.isConnectingToInternet()) {
//                                paypalmethod(ServiceConstant.paypalurl);
//                            }
//                            Toast.makeText(PaymentPageActivity.this, "paypal process", Toast.LENGTH_SHORT).show();
//                        } else {
//                            if (cd.isConnectingToInternet()) {
//                                MakePayment_Wallet(ServiceConstant.paybywallet_url);
//                            }
//
//                            Toast.makeText(PaymentPageActivity.this, "wallet process", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                } else {
//                    Toast.makeText(PaymentPageActivity.this, getResources().getString(R.string.terms_condition_toast), Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
    }


    private void alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(PaymentPageActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (str_partiallyPaid.equalsIgnoreCase("partially paid")) {
                    str_partiallyPaid = "";
                    paymentrequest(ServiceConstant.paymentpageurl);
                    mDialog.dismiss();
                } else {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.show();
    }

    /*private void partially_payment_alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(PaymentPageActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkid.setChecked(false);
                paymentrequest(ServiceConstant.paymentpageurl);
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }*/

    //-----------------------MakePayment WebView-MobileID Post Request by card payment-----------------
    private void MakePayment_WebView_MobileID(String Url) {
        // startLoading();

        // System.out.println("=========Abdul==user================>"+JOBID);
        // System.out.println("==========Abdul=====user==========>"+USERID);
        System.out.println("-------------MakePayment WebView-MobileID Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", User_ID);
        jsonParams.put("job_id", Job_ID);
        jsonParams.put("gateway", sPaymentCode);
        if (Flow.equals("requesting_pre_payment")) {
            jsonParams.put("pre_amount", TotalPrepaymentAmount);
        }

        mRequest = new ServiceRequest(PaymentPageActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------MakePayment WebView-MobileID Response----------------" + response);
                stopLoading();

                String sStatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        mobileId = object.getString("mobile_id");
                        System.out.println("=============mobileId===============>" + mobileId);
                        /*Intent intent = new Intent(PaymentPage.this, PaymentWebView.class);
                        intent.putExtra("MobileID", mobileId);
                        intent.putExtra("JobID", sJobID);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter, R.anim.exit);*/

                        if (Flow.equals("")) {
                            paymentrequest(ServiceConstant.paymentpageurl);
                        } else if (Flow.equals("requesting_pre_payment")) {
                            paymentrequest(ServiceConstant.PrepaymentRequestForPaymentRelated);
                        }


                    } else {
                        //  String sResponse = object.getString("response");
                        //alert(getResources().getString(R.string.action_sorry), sResponse);
                    }

                } catch (JSONException e) {
                    stopLoading();
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // stopLoading();
                stopLoading();
            }

            @Override
            public void onErrorListener() {
                // stopLoading();
                stopLoading();
            }
        });
    }
//-----------------------MakePayment Wallet Post Request-----------------


    //-----------------------MakePayment Cash Post Request-----------------


    private void MakePayment_Wallet(String Url) {
        startLoading();

        System.out.println("-------------MakePayment Cash Url----------------" + Url);
        sUserID = User_ID;
        sJobID = Job_ID;
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", sUserID);
        jsonParams.put("job_id", sJobID);

        if (Flow.equals("requesting_pre_payment")) {
            for (int i = 0; i < IdList.size(); i++) {
                jsonParams.put("prepayment_id[" + i + "]", IdList.get(i));
            }
        }

        System.out.println("user_id------------" + sUserID);
        System.out.println("job_id------------" + sJobID);

        mRequest = new ServiceRequest(PaymentPageActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------MakePayment Cash Response----------------" + response);

                String sStatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {

                        stopLoading();
                        final PkDialog mDialog = new PkDialog(PaymentPageActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.view_profile_sucssslabel_call));
                        mDialog.setDialogMessage(getResources().getString(R.string.make_payment_cash_plumber_confirm_label));
                        mDialog.setPositiveButton(getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                finish();
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                                String jobb = pref.getString("jobid", "");

                                SharedPreferences getcard;
                                getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                                SharedPreferences.Editor editor = getcard.edit();
                                editor.putString("jobid", "" + jobb);
                                editor.putString("servicetype", "");
                                editor.commit();

                                Intent intent = new Intent(PaymentPageActivity.this, RatingPageActivity.class);
                                intent.putExtra("JobID", jobb);
                                startActivity(intent);
                                finish();

                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            }
                        });
                        mDialog.show();


                    } else if (sStatus.equalsIgnoreCase("11")) {

                    } else {
                        stopLoading();
                        String sResponse = object.getString("response");
                        alert(getResources().getString(R.string.alert_label_success), sResponse.replace("tasker", "Craftsman"));
                    }

                } catch (JSONException e) {
                    stopLoading();
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                stopLoading();
            }
        });
    }


    private void MakePayment_Cash(String Url) {
        startLoading();

        System.out.println("-------------MakePayment Cash Url----------------" + Url);
        sUserID = User_ID;
        sJobID = Job_ID;
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", sUserID);
        jsonParams.put("job_id", sJobID);

        if (Flow.equals("requesting_pre_payment")) {
            for (int i = 0; i < IdList.size(); i++) {
                jsonParams.put("prepayment_id[" + i + "]", IdList.get(i));
            }
        }

        System.out.println("user_id------------" + sUserID);
        System.out.println("job_id------------" + sJobID);

        mRequest = new ServiceRequest(PaymentPageActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------MakePayment Cash Response----------------" + response);

                String sStatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {

                        stopLoading();
                        final PkDialog mDialog = new PkDialog(PaymentPageActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.view_profile_sucssslabel_call));
                        mDialog.setDialogMessage(getResources().getString(R.string.make_payment_cash_plumber_confirm_label));
                        mDialog.setPositiveButton(getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                finish();
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                                String jobb = pref.getString("jobid", "");

                                SharedPreferences getcard;
                                getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                                SharedPreferences.Editor editor = getcard.edit();
                                editor.putString("jobid", "" + jobb);
                                editor.putString("servicetype", "");
                                editor.apply();

                                Intent intent = new Intent(PaymentPageActivity.this, RatingPageActivity.class);
                                intent.putExtra("JobID", jobb);
                                startActivity(intent);
                                finish();

                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            }
                        });
                        mDialog.show();


                    } else {
                        stopLoading();
                        String sResponse = object.getString("response");
                        alert(getResources().getString(R.string.alert_label_success), sResponse.replace("tasker", "Craftsman"));
                    }

                } catch (JSONException e) {
                    stopLoading();
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                stopLoading();
            }
        });
    }


    //-------------------------------------------------------------------------paybal Url-------------------------------------------


    //----------------------------------------------------------------------Paypal Url--------------------------------------------------

//---------------------------------------------------------------------Coupon code url----------------------------------------------------------//


    private void couponcode(final String Url) {
        startLoading();


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", User_ID);
        jsonParams.put("code", couponcode);
        jsonParams.put("booking_id", Job_ID);

        System.out.println("user_id------------" + sUserID);
        System.out.println("job_id------------" + sJobID);

        mRequest = new ServiceRequest(PaymentPageActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Msarase----------------" + response);

                String sStatus = "";
                String successmsg = "", discount = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {


                        successmsg = object.getString("response");
                        discount = object.getString("discount");

                        if (Flow.equals("")) {
                            MakePayment_WebView_MobileID(ServiceConstant.Mobile_Id_url);
                        } else if (Flow.equals("requesting_pre_payment")) {
                            MakePayment_WebView_MobileID(ServiceConstant.PrepaymentRequestForTransactionID);
                        }




                       /* successmsg = object.getString("response");


                        stopLoading();
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                        String jobb=pref.getString("jobid","");

                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", ""+jobb);
                        editor.putString("servicetype", "");
                        editor.commit();

                        Intent intent = new Intent(PaymentPageActivity.this, RatingPage.class);
                        intent.putExtra("JobID",jobb);
                        startActivity(intent);
                        finish();

                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/


                    } else {
                        stopLoading();
                        String sResponse = object.getString("response");
                        alert(getResources().getString(R.string.action_sorry), sResponse);
                    }


                } catch (JSONException e) {
                    stopLoading();
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                stopLoading();
            }
        });
    }

    private void zerobalance(final String Url) {
        startLoading();


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", User_ID);
        // jsonParams.put("code", couponcode);
        jsonParams.put("booking_id", Job_ID);

        System.out.println("user_id------------" + sUserID);
        System.out.println("job_id------------" + sJobID);

        mRequest = new ServiceRequest(PaymentPageActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Msarase----------------" + response);

                String sStatus = "";
                String successmsg = "", discount = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {

                        Intent intent = new Intent(PaymentPageActivity.this, RatingPageActivity.class);
                        intent.putExtra("JobID", Job_ID);
                        startActivity(intent);
                        finish();

                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


                        //  successmsg = object.getString("response");
                        // discount = object.getString("discount");

                        //  MakePayment_WebView_MobileID(ServiceConstant.Mobile_Id_url);




                       /* successmsg = object.getString("response");


                        stopLoading();
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                        String jobb=pref.getString("jobid","");

                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", ""+jobb);
                        editor.putString("servicetype", "");
                        editor.commit();

                        Intent intent = new Intent(PaymentPageActivity.this, RatingPage.class);
                        intent.putExtra("JobID",jobb);
                        startActivity(intent);
                        finish();

                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/


                    } else {
                        stopLoading();
                        String sResponse = object.getString("response");
                        alert(getResources().getString(R.string.action_sorry), sResponse);
                    }


                } catch (JSONException e) {
                    stopLoading();
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                stopLoading();
            }
        });
    }


//--------------------------------------------------------Coupon code url--------------------------------------------------------------------//

    private void paymentrequest(String Url) {
        //startLoading();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", User_ID);
        jsonParams.put("job_id", Job_ID);

        jsonParams.put("loyaltystatus", loyaltymoneystatus);
        jsonParams.put("loyaltymoney", loyaltymoney);

        if (Flow.equals("requesting_pre_payment")) {
            jsonParams.put("pre_amount", TotalPrepaymentAmount);
        }
//        jsonParams.put("pre_amount", "50");


        SharedPreferences pref = getApplicationContext().getSharedPreferences("donatetips", MODE_PRIVATE);
        String tips = pref.getString("tipsamt", "");
        String donateamt = pref.getString("donateamt", "");

        if (tips.length() >= 1) {
            jsonParams.put("extra[tips]", tips);
        }

        if (donateamt.length() >= 1) {
            jsonParams.put("extra[donate]", donateamt);
        }


        System.out.println("user_id------------" + User_ID);
        System.out.println("job_id------------" + Job_ID);
        System.out.println("Url------------" + Url);

        mRequest = new ServiceRequest(PaymentPageActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                stopLoading();


                System.out.println("-------------MakePayment Cash Response----------------" + response);
                //  stopLoading();
                String sStatus = "";
                String successmsg = "", discount = "";
                String Str_status = "", Str_response = "", Str_currency = "", Str_rideid = "", Str_action = "", currency = "";
                String Job_id = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_response = object.getString("response");
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        totalamount = response_object.getString("balancetotal");


                        if (totalamount.equalsIgnoreCase("0") || totalamount.equalsIgnoreCase("0.0") || totalamount.equalsIgnoreCase("") || totalamount.equalsIgnoreCase(" ")) {
                            layout_two.setVisibility(View.GONE);
                        } else {
                            layout_two.setVisibility(View.VISIBLE);
                        }


                        if (response_object.has("franchisee_id")) {
                            franchisee_id = response_object.getString("franchisee_id");
                        } else {
                            franchisee_id = "";
                        }


                        if (response_object.length() > 0) {
                            //  String ScurrencyCode = response_object.getString("infso");
                            JSONObject infoobject = response_object.getJSONObject("info");
                            Job_id = infoobject.getString("job_id");
                            Json_TaskId = infoobject.getString("task_id");
                            currency = infoobject.getString("currency");


//                            String currencyCode = CurrencySymbolConverter.getCurrencySymbol(currency);
                            jobtextid.setText(Job_id);
                            add_coupon.setText("");

                            JSONArray payment = response_object.getJSONArray("payment");
                            array.clear();
                            for (int i = 0; i < payment.length(); i++) {
                                JSONObject ob = payment.getJSONObject(i);
                                String name = ob.getString("name");
                                String code = ob.getString("code");
                                array.add(name);
                                array.add(code);

                            }

                            String currencyCode = CurrencySymbolConverter.getCurrencySymbol(currency);

                            Double tt = Double.parseDouble(totalamount);
                            DecimalFormat df = new DecimalFormat("#.##");
                            String dx = df.format(tt);
                            tt = Double.valueOf(dx);

                            amount.setText(currencyCode + tt);

                            coupon.setVisibility(View.GONE);


                        }

                        if (totalamount.equals("0") || totalamount.equals("0.0") || totalamount.equals("") || totalamount.equals(" ")) {
                            //  Toast.makeText(PaymentPageActivity.this, "loyality", Toast.LENGTH_SHORT).show();
                            zerobalance(ServiceConstant.loyalty_url);
                            stopLoading();
                        } else {
                            //Toast.makeText(PaymentPageActivity.this, getResources().getString(R.string.payment_option_toast), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        stopLoading();
                        // stopLoading();
                        String sResponse = object.getString("response");
                        alert(getResources().getString(R.string.action_sorry), sResponse);
                    }

                    for (int i = 0; i < array.size(); i++) {
//                        String name = array.get(i);
                        String code = array.get(i);
                        if (code.equalsIgnoreCase("stripe")) {
                            creditname = code;

                        } else if (code.equalsIgnoreCase("Pay by PayPal")) {
                            payname = code;

                        } else if (code.equalsIgnoreCase("cash")) {
                            cashname = code;
                        } else if (code.equalsIgnoreCase("cheque")) {
                            walletname = code;
                        }


                    }


                    if (creditname.equalsIgnoreCase("stripe")) {
                        card.setVisibility(View.VISIBLE);
                        cardtext.setVisibility(View.VISIBLE);
                        creditname = "";
                    } else {
                        card.setVisibility(View.GONE);
                        cardtext.setVisibility(View.GONE);
                    }

                    if (payname.equalsIgnoreCase("Pay by PayPal")) {
                        pay.setVisibility(View.VISIBLE);
                        paypaltext.setVisibility(View.VISIBLE);
                        payname = "";

                    } else {
                        pay.setVisibility(View.GONE);
                        paypaltext.setVisibility(View.GONE);
                    }

                    if (cashname.equalsIgnoreCase("cash")) {
                        cash.setVisibility(View.VISIBLE);
                        cashtext.setVisibility(View.VISIBLE);
                        cashname = "";
                    } else {
                        cash.setVisibility(View.GONE);
                        cashtext.setVisibility(View.GONE);
                    }


                    if (walletname.equalsIgnoreCase("cheque")) {
                        wallet.setVisibility(View.VISIBLE);
                        wallettext.setVisibility(View.VISIBLE);
                        walletname = "";
                    } else {
                        wallet.setVisibility(View.GONE);
                        wallettext.setVisibility(View.GONE);
                    }


                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    stopLoading();


                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                // stopLoading();
                stopLoading();
            }
        });


//---------------------------------------------paymentrequest url------------------------------------------------------------------//


//--------------------------------------------Payment request url-----------------------------------------------------------------------//


    }

    private void startLoading() {
        myDialog = new ProgressDialogcreated(PaymentPageActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
    }

    private void stopLoading() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (myDialog != null) {
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                }


            }
        }, 500);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    private void paymentPost(Context mContext, String url) {


        myDialog = new ProgressDialogcreated(PaymentPageActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();

        System.out.println("payment1---------------------------" + url + "?franchisee_id=" + franchisee_id);
        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url + "?franchisee_id=" + franchisee_id, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "", Str_jobDescription = "", Str_NeedPayment = "", misc_approval_status = "", Str_Currency = "", Str_BtnGroup = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {

                    } else {
                        Str_response = jobject.getString("response");
                        HNDHelper.showResponseErrorAlert(PaymentPageActivity.this, Str_response);
                    }

                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }


                myDialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                myDialog.dismiss();
            }
        });
    }


}
