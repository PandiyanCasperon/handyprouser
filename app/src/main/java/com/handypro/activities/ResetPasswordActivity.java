package com.handypro.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.login.SignupActivity;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by CAS63 on 2/16/2018.
 */

public class ResetPasswordActivity extends AppCompatActivity {
    private CustomTextView myShowPwdTXT;
    private CustomEdittext myEmailET, myPasswrdET;
    private CustomButton mySubmitBTN;
    private ConnectionDetector myConnectionManager;
    private String myVerfCodeSTR = "";
    private ProgressDialogcreated myDialog;
    private RelativeLayout myBackLAY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInitialize();
        getIntentValues();
        clickListeners();

    }

    private void getIntentValues() {
        Intent intent = getIntent();
        myVerfCodeSTR = intent.getStringExtra("Intent_verificationCode");
    }

    private void classAndWidgetInitialize() {
        myConnectionManager = new ConnectionDetector(ResetPasswordActivity.this);

        myEmailET = (CustomEdittext) findViewById(R.id.activity_reset_password_ET_email);
        myPasswrdET = (CustomEdittext) findViewById(R.id.activity_reset_password_ET_password);
        myShowPwdTXT = (CustomTextView) findViewById(R.id.activity_reset_password_show_txtview);
        mySubmitBTN = (CustomButton) findViewById(R.id.activity_reset_password_BTN);
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_reset_password_otp_verification_LAY_bsck);

    }

    private void clickListeners() {

        myShowPwdTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowHidePwd();
            }
        });

        mySubmitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValues();
            }
        });

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void checkValues() {
        if (myEmailET.getText().toString().length() == 0) {
            HNDHelper.showErrorAlert(ResetPasswordActivity.this, getResources().getString(R.string.error_empty_emails));
        } else if(!SignupActivity.isValidEmail(myEmailET.getText().toString())){
            HNDHelper.showErrorAlert(ResetPasswordActivity.this, getResources().getString(R.string.error_enter_valid_email));
        }else if (myPasswrdET.getText().toString().length() == 0) {
            HNDHelper.showErrorAlert(ResetPasswordActivity.this, getResources().getString(R.string.error_empty_password));
        } else {
            submitValues();
        }
    }

    private void submitValues() {
        if (myConnectionManager.isConnectingToInternet()) {
            postResetPwdRequest(ResetPasswordActivity.this, ServiceConstant.reset_password_url);

        } else {
            HNDHelper.showErrorAlert(ResetPasswordActivity.this, getResources().getString(R.string.nointernet_text));
        }
    }

    private void postResetPwdRequest(Context aContext, String url) {

        myDialog = new ProgressDialogcreated(ResetPasswordActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", myEmailET.getText().toString());
        jsonParams.put("password", myPasswrdET.getText().toString());
        jsonParams.put("reset", myVerfCodeSTR);

        System.out.println("email-------" + myEmailET.getText().toString());
        System.out.println("password-------" + myPasswrdET.getText().toString());
        System.out.println("reset-------" + myVerfCodeSTR);


       ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Reset Password response-------------------" + response);

                String sStatus = "", sMessage = "";
                try {

                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    sMessage = object.getString("response");
                    if (sStatus.equalsIgnoreCase("1")) {

                        final PkDialog mDialog = new PkDialog(ResetPasswordActivity.this);
                        mDialog.setDialogMessage(sMessage);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                        onBackPressed();
                                        finish();
                                    }
                                }
                        );
                        mDialog.show();

                    } else {
                        HNDHelper.showResponseErrorAlert(ResetPasswordActivity.this, sMessage);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(myEmailET.getWindowToken(), 0);

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    private void ShowHidePwd() {
        String str_text = myShowPwdTXT.getText().toString();
        if (str_text.equals("Show")) {
            // hide password
            myPasswrdET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            myShowPwdTXT.setText(getResources().getString(R.string.class_ratingpage_hide));
        } else if (str_text.equals("Hide")) {
            // show password
            myPasswrdET.setTransformationMethod(PasswordTransformationMethod.getInstance());
            myShowPwdTXT.setText(getResources().getString(R.string.class_ratingpage_show));
        }
    }

}
