package com.handypro.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.MyOrdersPojo;
import com.handypro.Pojo.OrderStatusPojo;
import com.handypro.Pojo.acceptimage;
import com.handypro.Pojo.afterimage;
import com.handypro.Pojo.joblistpojo;
import com.handypro.R;
import com.handypro.Widgets.CircularImageView;
import com.handypro.Widgets.ExpandableHeightListView;
import com.handypro.Widgets.ParallaxScrollView;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.navigationMenu.MyOrdersActivity;
import com.handypro.activities.navigationMenu.paymentrequest;
import com.handypro.adapters.AcceptImageAdapter;
import com.handypro.adapters.AfterImageAdapter;
import com.handypro.adapters.OrderStatusAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.VISIBLE;

/**
 * Created by CAS63 on 3/14/2018.
 */

public class OrderDetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    public static final int RequestPermissionCode = 7;
    private static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;
    String jobinstruction, description;
    RelativeLayout acceptlay, acceptlay2;
    String job_status, reschedule = "0";
    List<acceptimage> movieList = new ArrayList<>();
    List<beforeimage> beforemovieList = new ArrayList<>();
    List<afterimage> aftermovieList = new ArrayList<>();
    LinearLayout changeorderrequest;
    TextView activity_order_detail_TXT_title, servicetype, activity_order_detail_location_textView, activity_order_detail_date_and_time_textView;
    CircularImageView activity_order_detail_response_viewprofile_imageView;
    ImageView img1, img2, img3;
    TextView name, email;
    RatingBar activity_order_detail_response_viewratingBar;
    LinearLayout estimate;
    String showmore = "0";
    String before_status = "";
    String before_array = "";
    TextView jobtext;
    String after_status = "";
    String after_array = "";
    ExpandableHeightListView activity_my_orders_listView, activity_my_helper_listView;
    TextView showtaskersallocated;
    Handler handler = new Handler();
    String pagehasresponse = "";
    String procrewno;
    TextView hidehint;
    SupportMapFragment mapFragment;
    LinearLayout reschedulecalendar, bacceptcall, brejectcall, LinearLayoutEstimation;
    SwipeRefreshLayout swipeRefreshLayout;
    String approvalstatus = "";
    String phototype = "";
    ParallaxScrollView activity_order_detail_scrollView;
    ShakingBell shakingBell;
    listhei joblistmore;
    String subcategoryre, lattre, longre, zipcodere, taskhrsre, task_idre;
    Joblistadapter myOrderAdapterss;
    LinearLayout lotjob;
    Bundle extras;
    Intent intentd;
    String loadreschedule;
    private RelativeLayout myBackLAY, myResponseLAY, myDetailLAY;
    private CustomButton myBottomStatusBTN;
    private double doubLatitude, doubLongitude;
    private ExpandableHeightListView myExpListVw;
    private OrderStatusAdapter myOrderStatAdapter;
    private ArrayList<OrderStatusPojo> myOrderStatArrList;
    private CustomTextView myTaskerEmailTXT, myTaskerNameTXT;
    private ConnectionDetector myconnectionManager;
    private SharedPreference mySession;
    private RatingBar myTaskerRatingBar;
    private CircularImageView myTaskerIMG;
    private LinearLayout myChatLAY, myViewProfileLAY;
    private ConnectionDetector myConnectionManager;
    private ProgressDialogcreated myDialog;
    private ArrayList<MyOrdersPojo> myOrdersArrList;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private ArrayList<joblistpojo> myjobList;
    private boolean EnablePreAmountRequest = false;
    private String myServiceTypeStr = "";
    private Runnable timedTask = new Runnable() {
        public void run() {

            shakingBell.shake(1);

            // TODO Auto-generated method stub
            SharedPreferences showpoupup = getApplicationContext().getSharedPreferences("reload", 0);
            String show = showpoupup.getString("page", "");
            if (show.equalsIgnoreCase("1")) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("reload", 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("page", "0");
                editor.commit();
                editor.apply();

                myConnectionManager = new ConnectionDetector(OrderDetailActivity.this);
                if (myConnectionManager.isConnectingToInternet()) {
                    approvalstatus = "";
                    phototype = "";
                    postRequestMyOrderList(OrderDetailActivity.this, ServiceConstant.MyJobs_Detail_Url);
                } else {
                    HNDHelper.showErrorAlert(getApplicationContext(), getResources().getString(R.string.nointernet_text));
                }
            }

            handler.postDelayed(timedTask, 2000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        myjobList = new ArrayList<joblistpojo>();


        classAndWidgetInitialize();
        OnClicks();
        initializeMap();
        myConnectionManager = new ConnectionDetector(OrderDetailActivity.this);
        if (myConnectionManager.isConnectingToInternet()) {
            approvalstatus = "";
            phototype = "";
            postRequestMyOrderList(OrderDetailActivity.this, ServiceConstant.MyJobs_Detail_Url);
        } else {
            HNDHelper.showErrorAlert(getApplicationContext(), getResources().getString(R.string.nointernet_text));
        }

        handler.post(timedTask);


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                myConnectionManager = new ConnectionDetector(OrderDetailActivity.this);
                if (myConnectionManager.isConnectingToInternet()) {

                    swipeRefreshLayout.setRefreshing(true);
                    approvalstatus = "";
                    phototype = "";
                    postRequestMyOrderList(OrderDetailActivity.this, ServiceConstant.MyJobs_Detail_Url);

                } else {
                    final PkDialog mDialog = new PkDialog(OrderDetailActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.action_no_internet_title));
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setDialogMessage(getResources().getString(R.string.no_internet));
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                            finish();
                        }
                    });
                    mDialog.show();
                }
            }
        });


    }

    private void classAndWidgetInitialize() {
        mySession = new SharedPreference(OrderDetailActivity.this);
        myOrderStatArrList = new ArrayList<OrderStatusPojo>();

        lotjob = findViewById(R.id.lotjob);


        showtaskersallocated = findViewById(R.id.showtaskersallocated);
        activity_my_orders_listView = findViewById(R.id.activity_my_orders_listView);
        activity_my_helper_listView = findViewById(R.id.activity_my_helper_listView);
        hidehint = findViewById(R.id.hidehint);
        shakingBell = findViewById(R.id.shakeBell);
        shakingBell.shake(1);
        acceptlay = findViewById(R.id.jobaccept);
        acceptlay2 = findViewById(R.id.acceptlay);
        jobtext = findViewById(R.id.jobtext);
        reschedulecalendar = findViewById(R.id.reschedulecalendar);
        changeorderrequest = findViewById(R.id.changeorderrequest);
        LinearLayoutEstimation = findViewById(R.id.LinearLayoutEstimation);

        bacceptcall = findViewById(R.id.bacceptcall);
        brejectcall = findViewById(R.id.brejectcall);


        lotjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(OrderDetailActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.fullscreendialog);


                joblistmore = dialog.findViewById(R.id.jobrecycler_view);
                myOrderAdapterss = new Joblistadapter(OrderDetailActivity.this, myjobList);
                joblistmore.setEnabled(false);
                joblistmore.setAdapter(myOrderAdapterss);


                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                dialog.getWindow().setAttributes(lp);
                dialog.show();

            }
        });


        bacceptcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approvalstatus = "1";
                phototype = "1";
                sendrequest(OrderDetailActivity.this, ServiceConstant.beforephotoapproval);
            }
        });

        brejectcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approvalstatus = "2";
                phototype = "1";
                sendrequest(OrderDetailActivity.this, ServiceConstant.beforephotoapproval);
            }
        });


        activity_order_detail_scrollView = findViewById(R.id.activity_order_detail_scrollView);


        //    showimage= (LinearLayout) findViewById(R.id.showimage);

        swipeRefreshLayout = findViewById(R.id.myjob_ongoing_swipe_refresh_layout);

        activity_order_detail_scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                int scrollY = activity_order_detail_scrollView.getScrollY();
                if (scrollY == 0) swipeRefreshLayout.setEnabled(true);
                else swipeRefreshLayout.setEnabled(false);

            }
        });


        estimate = findViewById(R.id.estimate);


        /*showimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                showdialogphoto();



            }


        });
*/
        reschedulecalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences pref = getApplicationContext().getSharedPreferences("newuserreschedule", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();

                editor.putString("task_idre", task_idre);
                editor.putString("jobid", activity_order_detail_TXT_title.getText().toString());
                editor.putString("subcat", subcategoryre);
                editor.putString("latre", lattre);
                editor.putString("longre", longre);
                editor.putString("zipcodere", zipcodere);
                editor.putString("overalltaskhrsre", taskhrsre);

                editor.putString("jobinstruction", jobinstruction);
                editor.putString("description", description);
                editor.putString("servicetype", myServiceTypeStr);


                editor.apply();
                editor.commit();
                Intent InfoIntent = new Intent(OrderDetailActivity.this, CraftsmanRescheduleActivity.class);
                startActivity(InfoIntent);

            }
        });
        hidehint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences pref = getApplicationContext().getSharedPreferences("newuserreschedule", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();

                editor.putString("task_idre", task_idre);
                editor.putString("jobid", activity_order_detail_TXT_title.getText().toString());
                editor.putString("subcat", subcategoryre);
                editor.putString("latre", lattre);
                editor.putString("longre", longre);
                editor.putString("zipcodere", zipcodere);
                editor.putString("overalltaskhrsre", taskhrsre);

                editor.putString("jobinstruction", jobinstruction);
                editor.putString("description", description);
                editor.putString("servicetype", myServiceTypeStr);


                editor.apply();
                editor.commit();
                Intent InfoIntent = new Intent(OrderDetailActivity.this, CraftsmanRescheduleActivity.class);
                startActivity(InfoIntent);

            }
        });


        changeorderrequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("jobid", activity_order_detail_TXT_title.getText().toString());
                editor.apply();
                editor.commit();
                Intent InfoIntent = new Intent(OrderDetailActivity.this, MoreInfoPage.class);
                startActivity(InfoIntent);
                finish();

            }
        });

        estimate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try {

                    final PkDialog mDialog = new PkDialog(OrderDetailActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                    mDialog.setDialogMessage(getResources().getString(R.string.class_orderdetail_activity_connect));
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                    if (procrewno != null || procrewno.length() > 2) {
                                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                        callIntent.setData(Uri.parse("tel:" + procrewno));
                                        startActivity(callIntent);
                                    } else {
                                        HNDHelper.showResponseErrorAlert(getApplicationContext(), getResources().getString(R.string.class_orderdetail_activity_contacted));
                                    }
                                }
                            }
                    );
                    mDialog.setNegativeButton(
                            getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            }
                    );
                    mDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        servicetype = findViewById(R.id.servicetype);
        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode

        activity_order_detail_response_viewratingBar = findViewById(R.id.activity_order_detail_response_viewratingBar);
        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);

        name = findViewById(R.id.activity_order_detail_response_viewprofileName_textView);
        email = findViewById(R.id.activity_order_detail_response_viewprofileEmail_textView);

        activity_order_detail_response_viewprofile_imageView = findViewById(R.id.activity_order_detail_response_viewprofile_imageView);
        activity_order_detail_date_and_time_textView = findViewById(R.id.activity_order_detail_date_and_time_textView);
        activity_order_detail_location_textView = findViewById(R.id.activity_order_detail_location_textView);
        activity_order_detail_TXT_title = findViewById(R.id.activity_order_detail_TXT_title);
        myBackLAY = findViewById(R.id.activity_order_detail_LAY_back);


        myExpListVw = findViewById(R.id.activity_order_detail_status_listView);
        myBottomStatusBTN = findViewById(R.id.activity_order_detail_bottom_BTN);
        myTaskerRatingBar = findViewById(R.id.activity_order_detail_response_viewratingBar);
        myTaskerNameTXT = findViewById(R.id.activity_order_detail_response_viewprofileName_textView);
        myTaskerEmailTXT = findViewById(R.id.activity_order_detail_response_viewprofileEmail_textView);
        myTaskerIMG = findViewById(R.id.activity_order_detail_response_viewprofile_imageView);
        myChatLAY = findViewById(R.id.activity_order_detail_response_viewchat_layout);
        myViewProfileLAY = findViewById(R.id.activity_order_detail_response_viewviewProfile_layout);
        myResponseLAY = findViewById(R.id.myJob_detail_responseView_RelativeLayout);
        myDetailLAY = findViewById(R.id.activity_order_detail_View_RelativeLayout);

        myExpListVw.setExpanded(true);

    }

    private void OnClicks() {
        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MyOrdersActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        myBottomStatusBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(),myBottomStatusBTN.getText().toString(),Toast.LENGTH_LONG).show();
                if (myBottomStatusBTN.getText().toString().contains(getResources().getString(R.string.activity_my_orders_label_cancelled))) {
                    HNDHelper.showResponseErrorAlert(getApplicationContext(), getResources().getString(R.string.class_orderdetail_activity_cancelled));
                } else if (myBottomStatusBTN.getText().toString().contains(getResources().getString(R.string.activity_payment_request_paynow))) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("jobid", activity_order_detail_TXT_title.getText().toString());
                    editor.apply();
                    Intent InfoIntent = new Intent(OrderDetailActivity.this, paymentrequest.class);
                    startActivity(InfoIntent);
                } else if (myBottomStatusBTN.getText().toString().equalsIgnoreCase(getResources().getString(R.string.class_orderdetail_activity_moreinfo)) || myBottomStatusBTN.getText().toString().equalsIgnoreCase(getResources().getString(R.string.class_orderdetail_activity_job_cancelled))) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("jobid", activity_order_detail_TXT_title.getText().toString());
                    editor.apply();
                    Intent InfoIntent = new Intent(OrderDetailActivity.this, newmoreinfo.class);
                    startActivity(InfoIntent);
                }
            }
        });

        myChatLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aChatIntent = new Intent(OrderDetailActivity.this, ChatPage.class);
                startActivity(aChatIntent);
            }
        });

        myViewProfileLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aTskerProfileIntent = new Intent(OrderDetailActivity.this, TaskerProfileFragActivity.class);
                startActivity(aTskerProfileIntent);
            }
        });

        LinearLayoutEstimation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences getcard;
                getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode


                SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("jobid", getcard.getString("jobid", ""));
                editor.apply();
                editor.commit();
                Intent in = new Intent(OrderDetailActivity.this, approveestimatebuilder.class);
                startActivity(in);
            }
        });
    }

    private void initializeMap() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.activity_order_detail_mapView);
        mapFragment.getMapAsync(OrderDetailActivity.this);
    }

    private void setMarker(final Double lat, final Double lon) {

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                doubLatitude = lat;
                doubLongitude = lon;
                // Move the camera to last position with a zoom level
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(doubLatitude, doubLongitude)).zoom(17).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                // adding marker
                MarkerOptions marker = new MarkerOptions().position(new LatLng(doubLatitude, doubLongitude));
                googleMap.addMarker(marker);

            }
        });


    }

    //-----------Check Google Play Service--------
    private boolean CheckPlayService() {
        final int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(OrderDetailActivity.this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        }
        return true;
    }

    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        runOnUiThread(new Runnable() {
            public void run() {
                final Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionStatusCode, OrderDetailActivity.this, REQUEST_CODE_RECOVER_PLAY_SERVICES);
                if (dialog == null) {
                    Toast.makeText(OrderDetailActivity.this, getResources().getString(R.string.activity_order_detail_incompatible_to_create_map), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void loadDataInAdapter() {

        myOrderStatAdapter = new OrderStatusAdapter(OrderDetailActivity.this, myOrderStatArrList);
        myExpListVw.setAdapter(myOrderStatAdapter);
    }

    private void postRequestMyOrderList(final Context aContext, String url) {
        myDialog = new ProgressDialogcreated(OrderDetailActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        movieList.clear();
        beforemovieList.clear();
        aftermovieList.clear();

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        System.out.println("-------------full details----------------" + mySession.getUserDetails().getUserId());

        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode

        jsonParams.put("job_id", getcard.getString("jobid", ""));


        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------full details----------------" + response);
                swipeRefreshLayout.setRefreshing(false);
                pagehasresponse = response;
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        JSONObject response_Object = object.getJSONObject("response");


                        JSONObject info_Object = response_Object.getJSONObject("info");
                        jobinstruction = info_Object.getString("instructions");
                        description = info_Object.getString("job_description");

                        EnablePreAmountRequest = object.has("EnablePreAmountRequest") && object.getString("EnablePreAmountRequest").equals("1");


                        if (info_Object.has("franchisee_no")) {
                            procrewno = info_Object.getString("franchisee_no");
                        } else {
                            procrewno = "";
                        }


                        JSONArray taskimage_type = info_Object.getJSONArray("user_taskimages");
                        for (int b = 0; b < taskimage_type.length(); b++) {
                            String value = "" + taskimage_type.getString(b);

                            if (value.startsWith("http://") || value.startsWith("https://")) {
                                value = value;
                            } else {
                                value = ServiceConstant.Base_Url + value;
                            }
                            acceptimage movie = new acceptimage(value);
                            movieList.add(movie);
                        }
                        JSONArray partnersarray = response_Object.getJSONArray("partners");

                        String provider_name, provider_image;
                        JSONObject provider_Object = info_Object.getJSONObject("provider");
                        if (provider_Object.has("provider_name")) {


                            JSONArray beforetaskimage_type = info_Object.getJSONArray("before_taskimages");
                            before_array = "" + beforetaskimage_type.length();

                            for (int b = 0; b < beforetaskimage_type.length(); b++) {
                                String value = "" + beforetaskimage_type.getString(b);

                                if (value.startsWith("http://") || value.startsWith("https://")) {
                                    value = value;
                                } else {
                                    value = ServiceConstant.Base_Url + value;
                                }
                                beforeimage beforemovie = new beforeimage(value);
                                beforemovieList.add(beforemovie);
                            }


                            if (info_Object.has("multipleschedules")) {
                                JSONArray multipleschedules = info_Object.getJSONArray("multipleschedules");
                                if (multipleschedules.length() > 0) {

                                    myjobList.clear();
                                    //  JSONArray joblistcategory = object2.getJSONArray("helper_category");
                                    for (int b1 = 0; b1 < multipleschedules.length(); b1++) {

                                        JSONObject getarrayvalue = multipleschedules.getJSONObject(b1);
                                        String date = "" + getarrayvalue.getString("date");
                                        String starttime = "" + getarrayvalue.getString("starttime");
                                        String endtime = "" + getarrayvalue.getString("endtime");
                                        String schedulestatus = "" + getarrayvalue.getString("schedulestatus");

                                        String schedule_id = "" + getarrayvalue.getString("_id");


                                        joblistpojo aOrderPojo = new joblistpojo();

                                        aOrderPojo.setOrderId(date);
                                        aOrderPojo.setOrderServiceType("Job time : " + starttime + " to " + endtime);
                                        aOrderPojo.setOrderStatus(schedulestatus);


                                        aOrderPojo.setOrderBookingDate(schedule_id);


                                        myjobList.add(aOrderPojo);
                                    }

                                    lotjob.setVisibility(View.VISIBLE);
                                } else {
                                    lotjob.setVisibility(View.GONE);
                                }

                            } else {
                                lotjob.setVisibility(View.GONE);
                            }

                            JSONArray aftertaskimage_type = info_Object.getJSONArray("after_taskimages");
                            after_array = "" + aftertaskimage_type.length();

                            for (int b = 0; b < aftertaskimage_type.length(); b++) {
                                String value = "" + aftertaskimage_type.getString(b);

                                if (value.startsWith("http://") || value.startsWith("https://")) {
                                    value = value;
                                } else {
                                    value = ServiceConstant.Base_Url + value;
                                }
                                afterimage beforemovie = new afterimage(value);
                                aftermovieList.add(beforemovie);
                            }


                            before_status = info_Object.getString("before_status");
                            after_status = info_Object.getString("after_status");


                            if (partnersarray.length() == 0) {

                                myOrdersArrList = new ArrayList<MyOrdersPojo>();
                                myOrdersArrList.clear();

                                String job_id = info_Object.getString("job_id");

                                provider_name = provider_Object.getString("provider_name");
                                provider_image = provider_Object.getString("provider_image");
                                if (provider_image.contains("https://") || provider_image.contains("http://")) {
                                } else {
                                    provider_image = ServiceConstant.Base_Url + provider_image;
                                }
                                String job_status = getResources().getString(R.string.class_orderdetail_activity_job_status) + " " + info_Object.getString("job_status");
                                String service = "";


                                JSONArray service_type = info_Object.getJSONArray("tasker_category");

                                if (service_type.length() == 0) {
                                    service_type = info_Object.getJSONArray("job_type");
                                }

                                for (int b = 0; b < service_type.length(); b++) {
                                    String value = "" + service_type.getString(b);
                                    service += value + ",";
                                }
                                if (service.endsWith(",")) {
                                    service = service.substring(0, service.length() - 1);
                                }

                                String overall_est_count = info_Object.getString("provide_estimation");
                                String tasker = provider_Object.getString("provider_id");

                                MyOrdersPojo aOrderPojo = new MyOrdersPojo();
                                aOrderPojo.setOrderId(job_id);
                                aOrderPojo.setOrderServiceType(service);
                                aOrderPojo.setTaskname(provider_name);
                                aOrderPojo.setUserphoto(provider_image);
                                aOrderPojo.setOrderStatus(job_status);
                                aOrderPojo.setEstimationcount(overall_est_count);
                                aOrderPojo.setTaskerid(tasker);
                                myOrdersArrList.add(aOrderPojo);

                                showtaskersallocated.setText(getResources().getString(R.string.activity_order_detail_label_request_craftman));
                                activity_my_orders_listView.setExpanded(true);
                                Craftmanlistadapter myOrderAdapter = new Craftmanlistadapter(OrderDetailActivity.this, myOrdersArrList);
                                activity_my_orders_listView.setAdapter(myOrderAdapter);


                                activity_my_helper_listView.setExpanded(true);
                                Craftmanlistadapter myOrderAdapters = new Craftmanlistadapter(OrderDetailActivity.this, myOrdersArrList);
                                activity_my_helper_listView.setAdapter(myOrderAdapters);
                            } else {
                                myOrdersArrList = new ArrayList<MyOrdersPojo>();
                                myOrdersArrList.clear();
                                String job_id = info_Object.getString("job_id");
                                provider_name = provider_Object.getString("provider_name");
                                provider_image = provider_Object.getString("provider_image");
                                String provider_id = provider_Object.getString("provider_id");
                                if (provider_image.contains("https://") || provider_image.contains("http://")) {
                                } else {
                                    provider_image = ServiceConstant.Base_Url + provider_image;
                                }
                                String job_status = getResources().getString(R.string.class_orderdetail_activity_job_status) + " " + info_Object.getString("job_status");
                                String service = "";
                                JSONArray service_type = info_Object.getJSONArray("tasker_category");
                                if (service_type.length() == 0) {
                                    service_type = info_Object.getJSONArray("job_type");
                                }
                                for (int b = 0; b < service_type.length(); b++) {
                                    String value = "" + service_type.getString(b);
                                    service += value + ",";
                                }
                                if (service.endsWith(",")) {
                                    service = service.substring(0, service.length() - 1);
                                }
                                String overall_est_count = info_Object.getString("provide_estimation");
                                MyOrdersPojo aOrderPojo = new MyOrdersPojo();
                                aOrderPojo.setOrderId(job_id);
                                aOrderPojo.setOrderServiceType(service);
                                aOrderPojo.setTaskname(provider_name);
                                aOrderPojo.setUserphoto(provider_image);
                                aOrderPojo.setEstimationcount(overall_est_count);
                                aOrderPojo.setTaskerid(provider_id);
                                aOrderPojo.setOrderStatus(job_status);
                                myOrdersArrList.add(aOrderPojo);
                                for (int b1 = 0; b1 < partnersarray.length(); b1++) {
                                    JSONObject getarrayvalue = partnersarray.getJSONObject(b1);
                                    String jobiddd = "" + getarrayvalue.getString("job_id");
                                    String orderstatus = getResources().getString(R.string.class_orderdetail_activity_job_status) + " " + getarrayvalue.getString("job_status");
                                    String cattype = "";
                                    JSONArray cattypearray = getarrayvalue.getJSONArray("category_name");
                                    for (int b = 0; b < cattypearray.length(); b++) {
                                        String value = "" + cattypearray.getString(b);
                                        cattype += value + ",";
                                    }
                                    if (cattype.endsWith(",")) {
                                        cattype = cattype.substring(0, cattype.length() - 1);
                                    }
                                    String tasknamee = getarrayvalue.getString("tasker_name");
                                    String taskphotoo = getarrayvalue.getString("tasker_image");
                                    if (taskphotoo.startsWith("http:") || taskphotoo.startsWith("https:")) {
                                    } else {
                                        taskphotoo = ServiceConstant.Base_Url + taskphotoo;
                                    }
                                    String est_count = "" + getarrayvalue.getString("overall_est_count");
                                    String taskerid = "" + getarrayvalue.getString("tasker");
                                    String alreadyinside = "0";
                                    for (int jk = 0; jk < myOrdersArrList.size(); jk++) {
                                        System.out.println("Checking tasker id--->" + myOrdersArrList.get(jk).getTaskerid());
                                        if (taskerid.equalsIgnoreCase(myOrdersArrList.get(jk).getTaskerid())) {
                                            alreadyinside = "1";
                                        }
                                    }
                                    if (alreadyinside.equalsIgnoreCase("0")) {
                                        MyOrdersPojo aOrderPojo1 = new MyOrdersPojo();
                                        aOrderPojo1.setOrderId(jobiddd);
                                        aOrderPojo1.setOrderServiceType(cattype);
                                        aOrderPojo1.setTaskname(tasknamee);
                                        aOrderPojo1.setUserphoto(taskphotoo);
                                        aOrderPojo1.setOrderStatus(orderstatus);
                                        aOrderPojo1.setEstimationcount(est_count);
                                        aOrderPojo1.setTaskerid(taskerid);
                                        myOrdersArrList.add(aOrderPojo1);
                                    }
                                }

                                showtaskersallocated.setText(myOrdersArrList.size() + " " + getResources().getString(R.string.class_orderdetail_activity_craftman));
                                activity_my_orders_listView.setExpanded(true);
                                Craftmanlistadapter myOrderAdapter = new Craftmanlistadapter(OrderDetailActivity.this, myOrdersArrList);
                                activity_my_orders_listView.setAdapter(myOrderAdapter);


                                activity_my_helper_listView.setExpanded(true);
                                Craftmanlistadapter myOrderAdapters = new Craftmanlistadapter(OrderDetailActivity.this, myOrdersArrList);
                                activity_my_helper_listView.setAdapter(myOrderAdapters);

                            }

                        } else {
                            if (info_Object.has("withoutscheduling") && info_Object.getString("withoutscheduling").equals("1")) {
                                LinearLayoutEstimation.setVisibility(VISIBLE);
                            } else {
                                LinearLayoutEstimation.setVisibility(View.GONE);
                            }

                            showtaskersallocated.setText(getResources().getString(R.string.class_orderdetail_activity_craftman_allocation));
                            jobinstruction = info_Object.getString("instructions");
                            description = info_Object.getString("job_description");


                            for (int b = 0; b < taskimage_type.length(); b++) {
                                String value = "" + taskimage_type.getString(b);

                                if (value.startsWith("http://") || value.startsWith("https://")) {
                                    value = value;
                                } else {
                                    value = ServiceConstant.Base_Url + value;
                                }
                                acceptimage movie = new acceptimage(value);
                                movieList.add(movie);
                            }


                            JSONArray beforetaskimage_type = info_Object.getJSONArray("before_taskimages");
                            before_array = "" + beforetaskimage_type.length();

                            for (int b = 0; b < beforetaskimage_type.length(); b++) {
                                String value = "" + beforetaskimage_type.getString(b);

                                if (value.startsWith("http://") || value.startsWith("https://")) {
                                    value = value;
                                } else {
                                    value = ServiceConstant.Base_Url + value;
                                }
                                beforeimage beforemovie = new beforeimage(value);
                                beforemovieList.add(beforemovie);
                            }


                            JSONArray aftertaskimage_type = info_Object.getJSONArray("after_taskimages");
                            after_array = "" + aftertaskimage_type.length();

                            for (int b = 0; b < aftertaskimage_type.length(); b++) {
                                String value = "" + aftertaskimage_type.getString(b);

                                if (value.startsWith("http://") || value.startsWith("https://")) {
                                    value = value;
                                } else {
                                    value = ServiceConstant.Base_Url + value;
                                }
                                afterimage beforemovie = new afterimage(value);
                                aftermovieList.add(beforemovie);
                            }
                        }
                        String need_payment = info_Object.getString("need_payment");
                        showmore = need_payment;
                        String job_id = info_Object.getString("job_id");
                        reschedule = info_Object.getString("reschedule");


                        estimate.setVisibility(View.VISIBLE);


                        activity_order_detail_TXT_title.setText(job_id);

                        String remainder = "";
                        if (info_Object.has("remainder")) {
                            if (info_Object.getString("remainder").length() < 6) {

                            } else {
                                remainder = getResources().getString(R.string.thanks) + " " + getResources().getString(R.string.rescheduledates1);
                            }

                        }

                        if (remainder.length() < 6) {
                            hidehint.setVisibility(View.GONE);
                            reschedulecalendar.setVisibility(View.GONE);
                        } else {
                            reschedulecalendar.setVisibility(View.VISIBLE);
                            hidehint.setVisibility(View.VISIBLE);
                            hidehint.setText(remainder);
                        }

                        String booking_address = info_Object.getString("booking_address");
                        activity_order_detail_location_textView.setText(booking_address);
                        String dateandtime = info_Object.getString("date").replace("/", ".") + " " + info_Object.getString("time");
                        activity_order_detail_date_and_time_textView.setText(dateandtime);
                        job_status = info_Object.getString("job_status");

                        String service = "";
                        JSONArray service_type = info_Object.getJSONArray("job_type");
                        for (int b = 0; b < service_type.length(); b++) {
                            String value = "" + service_type.getString(b);
                            service += value + ",";
                        }

                        if (service.endsWith(",")) {
                            service = service.substring(0, service.length() - 1);
                        }
                        myServiceTypeStr = service;

                        JSONArray aCategoryArray = info_Object.getJSONArray("catdata");
                        String aCreateStr = "", aFinalDisplyStr = "";
                        if (aCategoryArray.length() > 0) {
                            for (int r = 0; r < aCategoryArray.length(); r++) {
                                String aAnswerStr = "";
                                JSONObject aCategoryObject = aCategoryArray.getJSONObject(r);
                                aCreateStr = "♦ " + aCategoryObject.getString("name");
                                JSONArray aAnswerArray = aCategoryObject.getJSONArray("answer");
                                if (aAnswerArray.length() > 0) {
                                    for (int b = 0; b < aAnswerArray.length(); b++) {
                                        if (aAnswerStr.equalsIgnoreCase("")) {
                                            aAnswerStr = "\t\t\t\u2022 " + aAnswerArray.getString(b);
                                        } else {
                                            aAnswerStr = aAnswerStr + "\n\t\t\t\u2022 " + aAnswerArray.getString(b);
                                        }
                                    }
                                    aFinalDisplyStr = aFinalDisplyStr + aCreateStr + "\n" + aAnswerStr + "\n";
                                } else {
                                    aFinalDisplyStr = aFinalDisplyStr + aCreateStr + "\n";
                                }
                            }
                        }
                        servicetype.setText(aFinalDisplyStr);
                        myBottomStatusBTN.setVisibility(View.VISIBLE);
                        String btn_group = info_Object.getString("btn_group");
                        if (showmore.equalsIgnoreCase("no")) {
                            if (btn_group.equalsIgnoreCase("8")) {
                                myBottomStatusBTN.setText(getResources().getString(R.string.class_orderdetail_activity_job_cancelled));
                            } else {
                                if (info_Object.has("misc_approval_status")) {
                                    String misc_approval_status = info_Object.getString("misc_approval_status");
                                    if (misc_approval_status.equalsIgnoreCase("1")) {
                                        myBottomStatusBTN.setText(getResources().getString(R.string.class_orderdetail_activity_moreinfo));
                                        changeorderrequest.setVisibility(View.VISIBLE);
                                    } else if (misc_approval_status.equalsIgnoreCase("2")) {
                                        myBottomStatusBTN.setText(getResources().getString(R.string.class_orderdetail_activity_moreinfo));
                                        changeorderrequest.setVisibility(View.VISIBLE);
                                    } else {
                                        myBottomStatusBTN.setText(getResources().getString(R.string.class_orderdetail_activity_moreinfo));
                                        changeorderrequest.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    myBottomStatusBTN.setText(getResources().getString(R.string.class_orderdetail_activity_moreinfo));
                                }

                            }
                        } else {
                            if (btn_group.equalsIgnoreCase("8")) {
                                myBottomStatusBTN.setText(getResources().getString(R.string.class_orderdetail_activity_job_cancelled));
                            } else if (btn_group.equalsIgnoreCase("6")) {
                                myBottomStatusBTN.setText(getResources().getString(R.string.activity_payment_request_paynow));
                            } else {
                                if (info_Object.has("misc_approval_status")) {
                                    String misc_approval_status = info_Object.getString("misc_approval_status");
                                    if (misc_approval_status.equalsIgnoreCase("1")) {
                                        myBottomStatusBTN.setText(getResources().getString(R.string.activity_order_detail_label_request_payment));
                                    } else if (misc_approval_status.equalsIgnoreCase("2")) {
                                        myBottomStatusBTN.setText(getResources().getString(R.string.activity_order_detail_label_request_payment));
                                    } else {
                                        myBottomStatusBTN.setText(getResources().getString(R.string.class_orderdetail_activity_moreinfo));
                                        changeorderrequest.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    myBottomStatusBTN.setText(getResources().getString(R.string.activity_order_detail_label_request_payment));
                                }

                            }

                        }
                        String lat = info_Object.getString("lat");
                        String lng = info_Object.getString("lng");

                        lattre = lat;
                        longre = lng;

                        if (info_Object.has("zipcode")) {
                            zipcodere = info_Object.getString("zipcode");
                        }
                        if (info_Object.has("overall_taskhours")) {
                            taskhrsre = info_Object.getString("overall_taskhours");
                        }
                        if (info_Object.has("task_id")) {
                            task_idre = info_Object.getString("task_id");
                        }


                        String subcategoryid = "";
                        JSONArray subcategoryidarray = info_Object.getJSONArray("subcategory");
                        for (int b = 0; b < subcategoryidarray.length(); b++) {
                            String value = "" + subcategoryidarray.getString(b);
                            subcategoryid += value + ",";
                        }

                        if (subcategoryid.endsWith(",")) {
                            subcategoryre = subcategoryid.substring(0, subcategoryid.length() - 1);
                        }


                        if (lat.length() > 3) {
                            Double latd = Double.parseDouble(lat);
                            Double lngd = Double.parseDouble(lng);

                            setMarker(latd, lngd);
                        }
                        myDetailLAY.setVisibility(View.VISIBLE);
                        myResponseLAY.setVisibility(View.GONE);
                        JSONArray aMainArr = response_Object.getJSONArray("timeline");
                        myOrderStatArrList.clear();
                        for (int a = 0; a < aMainArr.length(); a++) {
                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);
                            OrderStatusPojo aOrderStat1 = new OrderStatusPojo();
                            aOrderStat1.setOrderTitle(aSubCatObj.getString("title"));
                            aOrderStat1.setOrderDate(aSubCatObj.getString("date").replace("/", "."));
                            aOrderStat1.setOrderTime(aSubCatObj.getString("time"));
                            myDetailLAY.setVisibility(View.VISIBLE);
                            myResponseLAY.setVisibility(View.GONE);
                            myOrderStatArrList.add(aOrderStat1);
                        }
                        loadDataInAdapter();

                        if (job_status.equalsIgnoreCase("Onprogress")) {
                            img1.setBackgroundResource(R.drawable.icon_tick_checked);
                            img2.setBackgroundResource(R.drawable.icon_empty_unchecked);
                            img3.setBackgroundResource(R.drawable.icon_empty_unchecked);
                        } else if (job_status.equalsIgnoreCase("Completed") || job_status.equalsIgnoreCase("Request Payment")) {
                            img1.setBackgroundResource(R.drawable.icon_tick_checked);
                            img2.setBackgroundResource(R.drawable.icon_tick_checked);
                            img3.setBackgroundResource(R.drawable.icon_tick_checked);
                        } else {
                            img1.setBackgroundResource(R.drawable.icon_empty_unchecked);
                            img2.setBackgroundResource(R.drawable.icon_tick_checked);
                            img3.setBackgroundResource(R.drawable.icon_empty_unchecked);
                        }


                        SharedPreferences prefjobids = getApplicationContext().getSharedPreferences("opend", 0);
                        String oppp = prefjobids.getString("opendialog", "");
                        if (oppp.equals("1")) {
                            showdialogphoto();
                            SharedPreferences.Editor prefeditors = prefjobids.edit();
                            prefeditors.putString("opendialog", "0");
                            prefeditors.apply();
                            prefeditors.commit();
                        }


                        mainjobmethod();


                    } else {
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }


                intentd = getIntent();

                if (intentd.hasExtra("gotore")) {

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("newuserreschedule", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();

                    editor.putString("task_idre", task_idre);
                    editor.putString("jobid", activity_order_detail_TXT_title.getText().toString());
                    editor.putString("subcat", subcategoryre);
                    editor.putString("latre", lattre);
                    editor.putString("longre", longre);
                    editor.putString("zipcodere", zipcodere);
                    editor.putString("overalltaskhrsre", taskhrsre);


                    editor.putString("jobinstruction", jobinstruction);
                    editor.putString("description", description);
                    editor.putString("servicetype", servicetype.getText().toString());


                    editor.apply();
                    editor.commit();

                    Intent InfoIntent = new Intent(OrderDetailActivity.this, CraftsmanRescheduleActivity.class);
                    startActivity(InfoIntent);
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {


            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            Intent intent = new Intent(getApplicationContext(), MyOrdersActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
        SharedPreferences.Editor prefeditor = prefjobid.edit();
        prefeditor.putString("page", "0");
        prefeditor.apply();
        prefeditor.commit();


    }

    protected void onPause() {
        super.onPause();
        try {
            handler.removeCallbacks(timedTask);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    protected void onStop() {
        super.onStop();
    }

    protected void onStart() {
        super.onStart();
        if (pagehasresponse.length() > 3) {
            handler.post(timedTask);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        if (CheckPlayService()) {
            // Changing map type
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            // Showing / hiding your current location
            googleMap.setMyLocationEnabled(false);
            // Enable / Disable zooming controls
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            // Enable / Disable my location button
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            // Enable / Disable Compass icon
            googleMap.getUiSettings().setCompassEnabled(false);
            // Enable / Disable Rotate gesture
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            // Enable / Disable zooming functionality
            googleMap.getUiSettings().setZoomGesturesEnabled(true);


            //---------Hiding the bottom layout after success request--------
            googleMap.getUiSettings().setAllGesturesEnabled(false);
        } else {
            HNDHelper.showErrorAlert(OrderDetailActivity.this, getResources().getString(R.string.activity_order_detail_unable_to_create_map));
        }
    }

    private void choosetime() {
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(OrderDetailActivity.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String addzerotohours = "" + hourOfDay;

                        String addzerotomin = "" + minute;

                        if (hourOfDay <= 9) {
                            addzerotohours = "0" + hourOfDay;
                        }

                        if (minute <= 9) {
                            addzerotomin = "0" + minute;
                        }

                        // selectedtime=addzerotohours + ":" + addzerotomin;


                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public boolean CheckingPermissionIsEnabledOrNot() {


        int call = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);

        return call == PackageManager.PERMISSION_GRANTED;
    }

    private void RequestMultiplePermission() {

        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(OrderDetailActivity.this, new String[]
                {

                        Manifest.permission.CALL_PHONE
                }, RequestPermissionCode);

    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {

            case RequestPermissionCode:

                if (grantResults.length > 0) {

                    boolean call = grantResults[0] == PackageManager.PERMISSION_GRANTED;


                    if (call) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + procrewno));
                        startActivity(callIntent);
                    } else {


                    }
                }

                break;
        }
    }

    private void sendrequest(final Context aContext, String url) {


        myDialog = new ProgressDialogcreated(OrderDetailActivity.this);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", mySession.getUserDetails().getUserId());
        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode

        params.put("job_id", getcard.getString("jobid", ""));

        params.put("status", "" + approvalstatus);
        params.put("position", "" + phototype);


        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------get Confirm Category list Response----------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    String message = object.getString("response");
                    if (object.getString("status").equalsIgnoreCase("1")) {


                        if (phototype.equalsIgnoreCase("1")) {

                            if (approvalstatus.equalsIgnoreCase("1")) {
                                before_status = "1";
                            } else if (approvalstatus.equalsIgnoreCase("2")) {
                                before_status = "2";
                            } else {
                                before_status = "0";
                            }

                        } else if (phototype.equalsIgnoreCase("2")) {

                            if (approvalstatus.equalsIgnoreCase("1")) {
                                after_status = "1";
                            } else if (approvalstatus.equalsIgnoreCase("2")) {
                                after_status = "2";
                            } else {
                                after_status = "0";
                            }
                        }


                        mainjobmethod();


                        final PkDialog mDialog = new PkDialog(OrderDetailActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();

                    } else {
                        final PkDialog mDialog = new PkDialog(OrderDetailActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.action_sorry));
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();
                    }
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block


                    e.printStackTrace();
                }


            }


            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void showdialogphoto() {


        RecyclerView recyclerView, beforerecyclerView, afterrecycler_view;
        TextView tellabout, whatsimportant, beforetext;
        LinearLayout userphoto, beforephoto, afterphoto;


        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottomdialog_layout, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(OrderDetailActivity.this);


        if (dialog.isShowing()) {

        } else {

            dialog.setContentView(modalbottomsheet);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);

            tellabout = modalbottomsheet.findViewById(R.id.tellabout);
            whatsimportant = modalbottomsheet.findViewById(R.id.whatsimportant);
            beforetext = modalbottomsheet.findViewById(R.id.beforetext);


            tellabout.setText(description);
            whatsimportant.setText(jobinstruction);

            userphoto = modalbottomsheet.findViewById(R.id.userphoto);
            beforephoto = modalbottomsheet.findViewById(R.id.beforephoto);
            afterphoto = modalbottomsheet.findViewById(R.id.afterphoto);


            recyclerView = modalbottomsheet.findViewById(R.id.recycler_view);
            beforerecyclerView = modalbottomsheet.findViewById(R.id.beforerecycler_view);
            afterrecycler_view = modalbottomsheet.findViewById(R.id.afterrecycler_view);


            if (movieList.size() > 0) {
                userphoto.setVisibility(View.VISIBLE);
                LinearLayoutManager layoutManager
                        = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                AcceptImageAdapter mAdapter = new AcceptImageAdapter(movieList, getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(mAdapter);
            } else {
                userphoto.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
            }


            if (beforemovieList.size() > 0) {
                beforephoto.setVisibility(View.VISIBLE);
                LinearLayoutManager layoutManager
                        = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                beforeimageadapter mAdapter = new beforeimageadapter(beforemovieList, getApplicationContext());
                beforerecyclerView.setLayoutManager(layoutManager);
                beforerecyclerView.setAdapter(mAdapter);
            } else {
                beforephoto.setVisibility(View.GONE);
                beforerecyclerView.setVisibility(View.GONE);
            }


            if (aftermovieList.size() > 0) {
                afterphoto.setVisibility(View.VISIBLE);
                LinearLayoutManager layoutManager
                        = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                AfterImageAdapter mAdapter = new AfterImageAdapter(aftermovieList, getApplicationContext());
                afterrecycler_view.setLayoutManager(layoutManager);
                afterrecycler_view.setAdapter(mAdapter);
            } else {
                afterphoto.setVisibility(View.GONE);
                afterrecycler_view.setVisibility(View.GONE);
            }


            dialog.show();
        }


    }

    private void mainjobmethod() {
        if (job_status.equalsIgnoreCase("Completed")) {
            bacceptcall.setVisibility(View.GONE);
            brejectcall.setVisibility(View.GONE);

        } else {
            if (!after_array.equalsIgnoreCase("0")) {
                bacceptcall.setVisibility(View.GONE);
                brejectcall.setVisibility(View.GONE);


            } else {
                if (before_status.equalsIgnoreCase("0") || before_status.equalsIgnoreCase("")) {
                    jobtext.setText(getResources().getString(R.string.class_orderdetail_activity_job_conform));
                    bacceptcall.setVisibility(View.VISIBLE);
                    brejectcall.setVisibility(View.VISIBLE);
                } else if (before_status.equalsIgnoreCase("1")) {

                    jobtext.setText(getResources().getString(R.string.class_orderdetail_activity_job_request));
                    bacceptcall.setVisibility(View.GONE);
                    brejectcall.setVisibility(View.GONE);
                } else if (before_status.equalsIgnoreCase("2")) {
                    jobtext.setText(getResources().getString(R.string.class_orderdetail_activity_job_rejected));
                    bacceptcall.setVisibility(View.VISIBLE);
                    brejectcall.setVisibility(View.GONE);
                }

            }
        }


        /*if (beforemovieList.size() > 0) {
            acceptlay.setVisibility(View.VISIBLE);
            acceptlay2.setVisibility(View.VISIBLE);

        } else {
            acceptlay.setVisibility(View.GONE);
            acceptlay2.setVisibility(View.GONE);

        }*/


    }

    public class Craftmanlistadapter extends BaseAdapter {
        private ArrayList<MyOrdersPojo> myOrderArrList;
        private LayoutInflater myInflater;
        private Context myContext;

        public Craftmanlistadapter(Context aContext, ArrayList<MyOrdersPojo> aOrderArrList) {
            this.myOrderArrList = aOrderArrList;
            this.myContext = aContext;
            myInflater = LayoutInflater.from(aContext);
        }

        @Override
        public int getCount() {
            return myOrderArrList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            Craftmanlistadapter.ViewHolder holder;
            if (convertView == null) {
                holder = new Craftmanlistadapter.ViewHolder();
                convertView = myInflater.inflate(R.layout.crfatmanlistlayout, parent, false);
                holder.estimatelayout = convertView.findViewById(R.id.estimatelayout);
                holder.LinearLayoutPayment = convertView.findViewById(R.id.LinearLayoutPayment);
                holder.jobid = convertView.findViewById(R.id.details_orderid);
                holder.imag = convertView.findViewById(R.id.imag);
                holder.jobtype = convertView.findViewById(R.id.jobtype);
                holder.username = convertView.findViewById(R.id.username);
                holder.jobstatus = convertView.findViewById(R.id.jobstatus);
                convertView.setTag(holder);

            } else {
                holder = (Craftmanlistadapter.ViewHolder) convertView.getTag();
            }


            holder.jobtype.setText(myOrderArrList.get(position).getOrderServiceType());
            holder.username.setText(myOrderArrList.get(position).getTaskname());
            holder.jobstatus.setText(myOrderArrList.get(position).getOrderStatus());

            holder.imag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("sendtaskerid", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("taskerid", myOrderArrList.get(position).getTaskerid());
                    editor.apply();
                    editor.commit();


                    Intent in = new Intent(OrderDetailActivity.this, TaskerProfileFragActivity.class);
                    startActivity(in);
                }
            });

            holder.jobid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("jobid", myOrderArrList.get(position).getOrderId());
                    editor.apply();
                    editor.commit();
                    Intent in = new Intent(OrderDetailActivity.this, approveestimatebuilder.class);
                    startActivity(in);
                }
            });

            holder.estimatelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("jobid", myOrderArrList.get(position).getOrderId());
                    editor.apply();
                    editor.commit();
                    Intent in = new Intent(OrderDetailActivity.this, approveestimatebuilder.class);
                    startActivity(in);

                }
            });

            if (myOrderArrList.get(position).getEstimationcount().equalsIgnoreCase("0")) {
                holder.estimatelayout.setVisibility(View.GONE);
                holder.jobid.setVisibility(View.GONE);
            } else {
                holder.estimatelayout.setVisibility(View.VISIBLE);
            }


            if (myOrderArrList.get(position).getUserphoto().contains("http")) {

                Picasso.get().load(myOrderArrList.get(position).getUserphoto())
                        .resize(500, 500)
                        .onlyScaleDown()
                        .placeholder(R.drawable.ic_no_user)
                        .into(holder.imag);
            } else {
                Picasso.get().load(/*ServiceConstant.Base_Url +*/ myOrderArrList.get(position).getUserphoto())
                        .resize(500, 500)
                        .onlyScaleDown()
                        .placeholder(R.drawable.ic_no_user)
                        .into(holder.imag);
            }

            if (EnablePreAmountRequest) {
                holder.LinearLayoutPayment.setVisibility(VISIBLE);
            } else {
                holder.LinearLayoutPayment.setVisibility(View.GONE);
            }

            holder.LinearLayoutPayment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (activity_order_detail_TXT_title.getText().toString().length() > 0) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("jobid", activity_order_detail_TXT_title.getText().toString());
                        editor.apply();
                        editor.apply();
                    }
                    Intent intent = new Intent(OrderDetailActivity.this, paymentrequest.class);
                    intent.putExtra("Type", "requesting_pre_payment");
                    startActivity(intent);
                }
            });


            return convertView;
        }

        private class ViewHolder {
            LinearLayout estimatelayout, LinearLayoutPayment;
            private TextView jobid, jobtype, username, jobstatus;
            private CircleImageView imag;
        }


    }

    public class Joblistadapter extends BaseAdapter {
        private ArrayList<joblistpojo> myOrderArrList;
        private LayoutInflater myInflater;
        private Context myContext;

        public Joblistadapter(Context aContext, ArrayList<joblistpojo> aOrderArrList) {
            this.myOrderArrList = aOrderArrList;
            this.myContext = aContext;
            myInflater = LayoutInflater.from(aContext);
        }

        @Override
        public int getCount() {
            return myOrderArrList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            Joblistadapter.ViewHolder holder;
            if (convertView == null) {
                holder = new Joblistadapter.ViewHolder();
                convertView = myInflater.inflate(R.layout.joblistadapter, parent, false);

                holder.jobid = convertView.findViewById(R.id.details_orderid);
                holder.imag = convertView.findViewById(R.id.imag);
                holder.jobtype = convertView.findViewById(R.id.jobtype);
                holder.username = convertView.findViewById(R.id.username);

                holder.jobstatus = convertView.findViewById(R.id.jobstatus);
                convertView.setTag(holder);

            } else {
                holder = (Joblistadapter.ViewHolder) convertView.getTag();
            }

            holder.jobid.setText(myOrderArrList.get(position).getOrderId());
            holder.jobtype.setText(myOrderArrList.get(position).getOrderServiceType());
            holder.jobstatus.setText(myOrderArrList.get(position).getOrderStatus());

            if (myOrderArrList.get(position).getOrderStatus().equals("2")) {
                holder.username.setText(getResources().getString(R.string.jobcompleted));
            } else if (myOrderArrList.get(position).getOrderStatus().equals("1")) {
                holder.username.setText(getResources().getString(R.string.completejobb));
            } else {
                holder.username.setText(getResources().getString(R.string.startjobb));
            }


            return convertView;
        }

        private class ViewHolder {

            private TextView jobid, jobtype, username, jobstatus;
            private CircleImageView imag;

        }


    }

}
