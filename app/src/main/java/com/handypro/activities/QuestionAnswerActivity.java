package com.handypro.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.handypro.Dialog.colorchangedialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.AnswerPojo;
import com.handypro.Pojo.ParentQuestionPojo;
import com.handypro.Pojo.SessionParentPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreatedforfrontpage;
import com.handypro.activities.bookingFlow.SelectedCategoryListActivity;
import com.handypro.adapters.QuestionListAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.BoldCustomTextView;
import com.handypro.textview.CustomButton;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class QuestionAnswerActivity extends AppCompatActivity implements View.OnClickListener {

    private BoldCustomTextView myTitleTXT, myQuestionTitleTXT;
    private String myTitleStr = "";
    private ArrayList<ParentQuestionPojo> myParentQuestionList = null;
    private ListView myQuestionLV;
    private RelativeLayout myBacKLAY;
    private QuestionListAdapter myListAdapter;
    private int myPosition = 0;
    private CustomButton myNextBTN;
    private boolean isSelected = false;
    private BoldCustomTextView myPercentageTXT;
    private String mySubCategoryIdStr = "";
    private int myCount = 0;
    private ArrayList<String> mySelectedSubCategoryInfoList = null;
    private String myZipCodeStr = "";
    private ConnectionDetector myConnectionManager;
    private ProgressDialogcreatedforfrontpage myDialog;
    private String tasker_date = "";
    private ArrayList<String> mySelectedSubCatIdArrdd;
    private SharedPreference mySession;
    public static int SHEDULE_APPOINTMENT_FLAG = 2011;
    public static ArrayList<String> subcatmainid = new ArrayList<>();
    public static ArrayList<String> parentcat = new ArrayList<>();
    public static ArrayList<String> subcatname = new ArrayList<>();
    public static ArrayList<String> subcatid = new ArrayList<>();
    public static ArrayList<String> checkedimage = new ArrayList<>();
    public static ArrayList<String> uncheckedimage = new ArrayList<>();
    public static ArrayList<String> whats_included = new ArrayList<>();
    private String myLatSTR = "", myLongSTR = "";
    private ArrayList<SessionParentPojo> aSessionParentInfoList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_answer);
        getIntentValues();
        classAndWidgetInitialize();
    }

    private void getIntentValues() {
        if (getIntent() != null) {
            myTitleStr = getIntent().getExtras().getString("TITLE");
            mySubCategoryIdStr = getIntent().getExtras().getString("SUBCATEGORY_ID");

            myZipCodeStr = getIntent().getExtras().getString("ZIPCODE");

            myLatSTR = getIntent().getExtras().getString("Latitude");
            myLongSTR = getIntent().getExtras().getString("Longitude");


            mySelectedSubCategoryInfoList = new Gson().fromJson(
                    getIntent().getExtras().getString("SELECTED_CATEGORY_LIST"), new TypeToken<ArrayList<String>>() {
                    }.getType());

            subcatmainid = new Gson().fromJson(
                    getIntent().getExtras().getString("SELECTED_MAIN_ID"), new TypeToken<ArrayList<String>>() {
                    }.getType());

            parentcat = new Gson().fromJson(
                    getIntent().getExtras().getString("PARENT_CATEGORY_ID"), new TypeToken<ArrayList<String>>() {
                    }.getType());

            subcatname = new Gson().fromJson(
                    getIntent().getExtras().getString("SUB_CATEGORY_NAME"), new TypeToken<ArrayList<String>>() {
                    }.getType());

            subcatid = new Gson().fromJson(
                    getIntent().getExtras().getString("SUB_CATEGORY_ID"), new TypeToken<ArrayList<String>>() {
                    }.getType());

            checkedimage = new Gson().fromJson(
                    getIntent().getExtras().getString("CHECKED_IMAGE"), new TypeToken<ArrayList<String>>() {
                    }.getType());

            uncheckedimage = new Gson().fromJson(
                    getIntent().getExtras().getString("UNCHECKED_IMAGE"), new TypeToken<ArrayList<String>>() {
                    }.getType());

            whats_included = new Gson().fromJson(
                    getIntent().getExtras().getString("WHATS_INCLUDED"), new TypeToken<ArrayList<String>>() {
                    }.getType());

            myParentQuestionList = new Gson().fromJson(
                    getIntent().getExtras().getString("QUESTION_LIST"), new TypeToken<ArrayList<ParentQuestionPojo>>() {
                    }.getType());

        }
    }

    private void classAndWidgetInitialize() {
        mySelectedSubCatIdArrdd = new ArrayList<>();
        mySession = new SharedPreference(QuestionAnswerActivity.this);
        myConnectionManager = new ConnectionDetector(QuestionAnswerActivity.this);
        myTitleTXT = (BoldCustomTextView) findViewById(R.id.activity_question_answer_TXT_title);
        myBacKLAY = (RelativeLayout) findViewById(R.id.activity_question_answer_LAY_back);
        myNextBTN = (CustomButton) findViewById(R.id.activity_question_answer_BTN_submit);

        myQuestionTitleTXT = (BoldCustomTextView) findViewById(R.id.activity_question_answer_TXT_question_title);
        myPercentageTXT = (BoldCustomTextView) findViewById(R.id.activity_question_answer_TXT_percentage);

        myQuestionLV = (ListView) findViewById(R.id.activity_question_answer_LV_qustion);
        aSessionParentInfoList = mySession.getSessionParentDetails();
        clickListener();
        setValues();
    }

    private void clickListener() {
        myBacKLAY.setOnClickListener(this);
        myNextBTN.setOnClickListener(this);

    }

    private void setValues() {
        myTitleTXT.setText(myTitleStr);
        myQuestionTitleTXT.setText(myParentQuestionList.get(myPosition).getQuestiontitle());
        myListAdapter = new QuestionListAdapter(QuestionAnswerActivity.this, myParentQuestionList.get(myPosition).getAnswerpojo(), myParentQuestionList.get(myPosition).getQuestionAnsType());
        myQuestionLV.setAdapter(myListAdapter);
        myQuestionLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ArrayList<AnswerPojo> aPojoList = myListAdapter.getArrayList();
                AnswerPojo aAnswerPojo = aPojoList.get(i);
                if (myParentQuestionList.get(myPosition).getQuestionAnsType().equalsIgnoreCase("2")) {
                    if (aAnswerPojo.isAnswerselected()) {
                        aAnswerPojo.setAnswerselected(false);
                    } else {
                        aAnswerPojo.setAnswerselected(true);
                    }
                } else {
                    for (int k = 0; k < aPojoList.size(); k++) {
                        aPojoList.get(k).setAnswerselected(false);
                    }
                    aAnswerPojo.setAnswerselected(true);
                    aPojoList.set(i, aAnswerPojo);
                    movenxtonvalue();
                }
                myListAdapter.notifyDataSetChanged();


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        double aPercentage = Double.parseDouble(String.valueOf(myPosition + 1)) / Double.parseDouble(String.valueOf(myParentQuestionList.size()));
                        int aFinaltotal = (int) (aPercentage * 100);
                        myPercentageTXT.setText(String.valueOf(aFinaltotal) + "%");
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_question_answer_LAY_back:
                if (myPosition == 0) {
                    finish();
                } else {
                    myPosition = myPosition - 1;
                    myQuestionTitleTXT.setText(myParentQuestionList.get(myPosition).getQuestiontitle());
                    myListAdapter = new QuestionListAdapter(QuestionAnswerActivity.this,
                            myParentQuestionList.get(myPosition).getAnswerpojo(), myParentQuestionList.get(myPosition).getQuestionAnsType());
                    myQuestionLV.setAdapter(myListAdapter);
                }

                break;

            case R.id.activity_question_answer_BTN_submit:
                isSelected = false;
                for (int u = 0; u < myParentQuestionList.get(myPosition).getAnswerpojo().size(); u++) {
                    if (myParentQuestionList.get(myPosition).getAnswerpojo().get(u).isAnswerselected()) {
                        isSelected = true;
                    }
                }

                if (!isSelected) {
                    Toast.makeText(QuestionAnswerActivity.this, "Please select the answer", Toast.LENGTH_SHORT).show();
                } else {
                    navigatenextValues();
                }
                break;
        }
    }

    private void navigatenextValues() {
        myCount = myCount + 1;
        if (myParentQuestionList.size() == myCount) {
            if (myConnectionManager.isConnectingToInternet()) {
                SessionParentPojo aPojo = new SessionParentPojo();
                aPojo.setSessionParentId(mySubCategoryIdStr);
                aPojo.setaPojo(myParentQuestionList);
                aSessionParentInfoList.add(aPojo);
                mySession.putSessionParentPojo(aSessionParentInfoList);
                getCategoryDetailValues(QuestionAnswerActivity.this);
            } else {
                HNDHelper.showErrorAlert(QuestionAnswerActivity.this, getResources().getString(R.string.nointernet_text));
            }
        } else {
            myPosition = myPosition + 1;
            myQuestionTitleTXT.setText(myParentQuestionList.get(myPosition).getQuestiontitle());
            myListAdapter = new QuestionListAdapter(QuestionAnswerActivity.this,
                    myParentQuestionList.get(myPosition).getAnswerpojo(), myParentQuestionList.get(myPosition).getQuestionAnsType());
            myQuestionLV.setAdapter(myListAdapter);
        }
    }


    private void getCategoryDetailValues(Context aContext) {
        myDialog = new ProgressDialogcreatedforfrontpage(QuestionAnswerActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParam = new HashMap<>();
        try {
            JSONArray aJsonArr = new JSONArray(mySelectedSubCategoryInfoList);
            System.out.println("----" + aJsonArr.toString());
            for (int a = 0; a < aJsonArr.length(); a++) {
                jsonParam.put("subcategory[" + a + "]", aJsonArr.getString(a));
            }
            jsonParam.put("zipcode", myZipCodeStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(ServiceConstant.CategoryDetail, Request.Method.POST, jsonParam, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("1")) {
                        JSONObject aRespObj = object.getJSONObject("response");
                        if (object.has("task_date")) {
                            tasker_date = object.getString("task_date");
                        }
                        JSONArray aMainArr = aRespObj.getJSONArray("category");
                        String estimate_text, estimate_count, estimate_hours;
                        StringBuilder nonavailablecat = new StringBuilder();
                        String mynocat = "";
                        int jkx = 0;
                        mySelectedSubCatIdArrdd.clear();
                        for (int a = 0; a < aMainArr.length(); a++) {
                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);
                            String cat_name = aSubCatObj.getString("cat_name");
                            String tasker_availablity = aSubCatObj.getString("tasker_availablity");
                            String cat_id = aSubCatObj.getString("id");
                            if (tasker_availablity.equals("0")) {
                                jkx = 1;
                                nonavailablecat.append("," + cat_name);
                            } else {
                                mySelectedSubCatIdArrdd.add(cat_id);
                            }
                        }

                        if (jkx == 1) {
                            mynocat = nonavailablecat.toString();
                            if (mynocat.startsWith(",")) {
                                mynocat = mynocat.substring(1);
                            }

                            final colorchangedialog mDialog = new colorchangedialog(QuestionAnswerActivity.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.rating_header_sorry_textView));
                            mDialog.setDialogMessage(getResources().getString(R.string.noavailcat));
                            mDialog.setcatMessage(mynocat);
                            mDialog.setPositiveButton(
                                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();
                                        }
                                    }
                            );
                            mDialog.show();
                        } else {

                            String aSelectedIdSTR = android.text.TextUtils.join(",", mySelectedSubCategoryInfoList);
                            mySession.setCategoryId(aSelectedIdSTR);
                            Intent in = new Intent(QuestionAnswerActivity.this, SelectedCategoryListActivity.class);
                            in.putExtra("selectedSubCategories", mySelectedSubCatIdArrdd);
                            in.putExtra("zipCode", myZipCodeStr);
                            in.putExtra("Latitude", myLatSTR);
                            in.putExtra("Longitude", myLongSTR);
                            in.putExtra("task_date", tasker_date);
                            Bundle args = new Bundle();
                            args.putSerializable("SubCategoriesID", subcatmainid);
                            args.putSerializable("parentcat", parentcat);
                            args.putSerializable("subcatname", subcatname);
                            args.putSerializable("subcatid", subcatid);
                            args.putSerializable("checkedimage", checkedimage);
                            args.putSerializable("uncheckedimage", uncheckedimage);
                            args.putSerializable("whats_included", whats_included);
                            in.putExtra("BUNDLE", args);
                            mySession.setNewSelection(false);
                            mySession.setNewsearch(false);
                            startActivityForResult(in, SHEDULE_APPOINTMENT_FLAG);
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    private void movenxtonvalue()
    {
        isSelected = false;
        for (int u = 0; u < myParentQuestionList.get(myPosition).getAnswerpojo().size(); u++) {
            if (myParentQuestionList.get(myPosition).getAnswerpojo().get(u).isAnswerselected()) {
                isSelected = true;
            }
        }

        if (!isSelected) {
            Toast.makeText(QuestionAnswerActivity.this, "Please select the answer", Toast.LENGTH_SHORT).show();
        } else {
            navigatenextValues();
        }
    }
}
