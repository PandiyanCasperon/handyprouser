package com.handypro.activities.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.UserInfoPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.HomeActivity;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by CAS63 on 2/19/2018.
 */

public class FacebookotpActivity extends AppCompatActivity implements View.OnClickListener {
    private String Str_username = "", user_first_name = "", user_last_name = "", Str_referralCode = "", Str_email = "", Str_phone = "", Str_countryCode = "", Str_gcmId = "", Str_otp_Status = "", Str_otp = "",
            profile_image = "", fb_id = "", marketing = "";
    private CustomEdittext myOTPET;
    private CustomButton mySubmitOTPBtn;
    private ConnectionDetector myConnectionManager;
    private SharedPreference mySession;
    private RelativeLayout myBackLAY;
    private ProgressDialogcreated myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_otp);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInit();
        getIntentValues();
    }

    private void getIntentValues() {

        Intent intent = getIntent();
        Str_username = intent.getStringExtra("UserName");
        user_first_name = intent.getStringExtra("user_first_name");
        user_last_name = intent.getStringExtra("user_last_name");
        Str_email = intent.getStringExtra("Email");
        Str_phone = intent.getStringExtra("Phone");
        Str_countryCode = intent.getStringExtra("CountryCode");
        Str_gcmId = intent.getStringExtra("GcmID");
        Str_otp_Status = intent.getStringExtra("Otp_Status");
        Str_otp = intent.getStringExtra("Otp");
        profile_image = intent.getStringExtra("profileimage");
        fb_id = intent.getStringExtra("MediaId");
        marketing = intent.getStringExtra("marketing");
        Str_referralCode = intent.getStringExtra("ReferralCode");
        // if (Str_otp_Status.equalsIgnoreCase("development")) {
        myOTPET.setText(Str_otp);
//        } else {
//            myOTPET.setText("");
//        }

    }

    private void classAndWidgetInit() {
        myConnectionManager = new ConnectionDetector(FacebookotpActivity.this);
        mySession = new SharedPreference(FacebookotpActivity.this);

        myOTPET = (CustomEdittext) findViewById(R.id.activity_facebook_otp_verification_entered_otp_ET);
        mySubmitOTPBtn = (CustomButton) findViewById(R.id.activity_facebook_otp_verification_BTN_send);
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_facebook_otp_verification_LAY_back);

        myBackLAY.setOnClickListener(this);
        mySubmitOTPBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_facebook_otp_verification_LAY_back:
                finish();
                break;

            case R.id.activity_facebook_otp_verification_BTN_send:

                if (myOTPET.getText().toString().length() == 0) {
                    HNDHelper.showErrorAlert(FacebookotpActivity.this, getResources().getString(R.string.otp_label_alert_otp));
                } else if (!Str_otp.equals(myOTPET.getText().toString())) {
                    HNDHelper.showErrorAlert(FacebookotpActivity.this, getResources().getString(R.string.otp_label_alert_invalid));
                } else {
                    submitValues();
                }
                break;
        }
    }

    private void submitValues() {
        if (myConnectionManager.isConnectingToInternet()) {
            postRegisterRequest(FacebookotpActivity.this);

        } else {
            HNDHelper.showErrorAlert(FacebookotpActivity.this, getResources().getString(R.string.nointernet_text));
        }
    }

    private void postRegisterRequest(Context aContext) {

        myDialog = new ProgressDialogcreated(FacebookotpActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email_id", Str_email);

        jsonParams.put("user_name", Str_username);
        jsonParams.put("country_code", Str_countryCode);
        jsonParams.put("phone", Str_phone);
        jsonParams.put("unique_code", Str_referralCode);
        jsonParams.put("gcm_id", Str_gcmId);
        jsonParams.put("fb_id", fb_id);
        jsonParams.put("first_name", "");
        jsonParams.put("last_name", "");
        jsonParams.put("prof_pic", profile_image);

        jsonParams.put("marketing", marketing);


        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
        String line1 = getcard.getString("line1", "");
        String line2 = getcard.getString("line2", "");
        String city = getcard.getString("city", "");
        String state = getcard.getString("state", "");
        String zipcode = getcard.getString("zipcode", "");
        String country = getcard.getString("country", "");
        String formatted_address = getcard.getString("formatted_address", "");

        String Selected_Latitude = getcard.getString("Selected_Latitude", "");
        String Selected_Longitude = getcard.getString("Selected_Longitude", "");


        jsonParams.put("address[" + "line1" + "]", line1);
        jsonParams.put("address[" + "line2" + "]", line2);
        jsonParams.put("address[" + "city" + "]", city);
        jsonParams.put("address[" + "state" + "]", state);
        jsonParams.put("address[" + "zipcode" + "]", zipcode);
        jsonParams.put("address[" + "country" + "]", country);
        jsonParams.put("address[" + "formatted_address" + "]", formatted_address);

        jsonParams.put("location[" + "lat" + "]", Selected_Latitude);
        jsonParams.put("location[" + "lng" + "]", Selected_Longitude);

        jsonParams.put("zipcode", zipcode);

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(ServiceConstant.facebook_register_url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------otp response------------" + response);

                String Str_status = "", Str_message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");


                    if (Str_status.equalsIgnoreCase("1")) {

                        UserInfoPojo aUInfoPojo = new UserInfoPojo();

                        aUInfoPojo.setUserName(object.getString("user_name"));
                        aUInfoPojo.setUserId(object.getString("user_id"));
                        aUInfoPojo.setUserEmail(object.getString("email"));
                        aUInfoPojo.setUserProfileImage(object.getString("prof_pic"));
                        aUInfoPojo.setUserCountryCode(object.getString("country_code"));
                        aUInfoPojo.setUserPhoneNumber(object.getString("phone_number"));
                        aUInfoPojo.setUserWalletAmount(object.getString("wallet_amount"));
                        aUInfoPojo.setSocketKey(object.getString("soc_key"));
                        aUInfoPojo.setUserCurrencyCode(object.getString("currency"));

                        mySession.putUserDetails(aUInfoPojo);
                        mySession.setLoginStatus(true);


                        Intent intent = new Intent(FacebookotpActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();

                    }
                    if (Str_status.equalsIgnoreCase("3")) {
                        if (myDialog.isShowing()) {
                            myDialog.dismiss();
                        }
                        submitValues();
                    }

                    if (Str_status.equalsIgnoreCase("0")) {

                        if (myDialog.isShowing()) {
                            myDialog.dismiss();
                        }

                        if (object.has("errors")) {
                            Str_message = object.getString("errors");
                            HNDHelper.showResponseErrorAlert(FacebookotpActivity.this, Str_message);
                        } else {
                            Str_message = object.getString("message");
                            HNDHelper.showResponseErrorAlert(FacebookotpActivity.this, Str_message);
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });


    }
}
