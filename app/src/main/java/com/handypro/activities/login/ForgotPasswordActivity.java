package com.handypro.activities.login;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.ForgotPasswordOtpActivity;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.HideSoftKeyboard;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by CAS61 on 2/6/2018.
 */

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private CustomButton myResetBTN;
    private CustomEdittext myEmailET;
    private RelativeLayout myBackLAY;
    private ConnectionDetector myConnectionManager;
    private ProgressDialogcreated myDialog;
    String mySmsStatusSTR = "", myVerfCodeSTR = "", myEmailAddrsSTR = "";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        HideSoftKeyboard.setupUI(
                ForgotPasswordActivity.this.getWindow().getDecorView(),
                ForgotPasswordActivity.this);
        initializeHeaderView();
        classAndWidgetInitialize();
    }

    private void initializeHeaderView() {
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_forgot_password_LAY_back);
        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void classAndWidgetInitialize() {
        myConnectionManager = new ConnectionDetector(ForgotPasswordActivity.this);
        myResetBTN = (CustomButton) findViewById(R.id.activity_forgot_password_BTN_login);
        myEmailET = (CustomEdittext) findViewById(R.id.activity_forgot_password_ET_password);
        clickListener();
    }

    private void clickListener() {
        myResetBTN.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_forgot_password_BTN_login:
                checkValues();
                break;
        }
    }

    private void checkValues() {
        if (getEditTextValue(myEmailET).length() == 0) {
            HNDHelper.showErrorAlert(ForgotPasswordActivity.this, getResources().getString(R.string.error_empty_forgot));
        }
        else {
            getValues();
        }
    }

    private void getValues() {
        if (myConnectionManager.isConnectingToInternet()) {
            //submitValues();
            postRequestForgotPassword(ForgotPasswordActivity.this, ServiceConstant.forgot_password_url);
        } else {
            HNDHelper.showErrorAlert(ForgotPasswordActivity.this, getResources().getString(R.string.nointernet_text));
        }
    }

    private void postRequestForgotPassword(Context aContext, String url) {
        myDialog = new ProgressDialogcreated(ForgotPasswordActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", myEmailET.getText().toString());
        System.out.println("-----------email--------------" + myEmailET.getText().toString());

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("---------forgot password response------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("1")) {

                        mySmsStatusSTR = object.getString("sms_status");
                        myVerfCodeSTR = object.getString("verification_code");
                        myEmailAddrsSTR = object.getString("email_address");

                        try {

                            final PkDialog mDialog = new PkDialog(ForgotPasswordActivity.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                            mDialog.setDialogMessage(getResources().getString(R.string.activity_forgot_password_email_label_success));
                            mDialog.setPositiveButton(
                                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();
                                            Intent i = new Intent(ForgotPasswordActivity.this, ForgotPasswordOtpActivity.class);
                                            i.putExtra("Intent_Otp_Status", mySmsStatusSTR);
                                            i.putExtra("Intent_verificationCode", myVerfCodeSTR);
                                            i.putExtra("Intent_email", myEmailAddrsSTR);
                                            startActivity(i);
                                            finish();



                                        }
                                    }
                            );
                            mDialog.show();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else {
                        HNDHelper.showResponseErrorAlert(ForgotPasswordActivity.this, object.getString("response"));
                    }

                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private String getEditTextValue(EditText aEditText) {
        return aEditText.getText().toString().trim();
    }

}
