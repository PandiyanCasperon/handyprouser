package com.handypro.activities.login;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.handypro.Dialog.PkDialog;
import com.handypro.Dialog.internetmissing;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.UserInfoPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.HomeActivity;
import com.handypro.core.Facebook.DialogError;
import com.handypro.core.Facebook.Facebook;
import com.handypro.core.Facebook.FacebookError;
import com.handypro.core.Facebook.Util;
import com.handypro.core.socket.ChatMessageService;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;
import com.handypro.textview.CustomTextView;
import com.handypro.textview.TypefaceSpan;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.CurrencySymbolConverter;
import com.handypro.utils.HideSoftKeyboard;
import com.handypro.volley.ServiceRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CAS61 on 2/5/2018.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    public static AppCompatActivity logIn;
    TextView mySkipHomePageBTN;
    String getuserid = "";
    Typeface tf;
    Dialog dialog;
    //--------Handler Method------------
    Runnable dialogFacebookRunnable = new Runnable() {
        @Override
        public void run() {
            dialog = new Dialog(LoginActivity.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();

            TextView dialog_title = dialog.findViewById(R.id.custom_loading_textview);
            dialog_title.setText(getResources().getString(R.string.loading));
        }
    };
    private CustomTextView myForgotPasswordTXT, myRegisterTXT, backarrow, activity_login_TXT_title;
    private CustomButton myLoginBTN, myFacebookBTN;
    private CustomEdittext myEmailET, myPasswordET;
    private ConnectionDetector myConnectionManager;
    private SharedPreference mySession;
    private ProgressDialogcreated myDialog;
    private SharedPreferences mPrefs;
    private String APP_ID;
    private Facebook facebook;
    private CallbackManager myCalBackManager;
    private String myFbUserIdSTR = "", myFbUserNameSTR = "", myFbUserImageSTR = "", myFbUserEmailSTR = "";
    private ArrayList<String> mySelectedSubCatIdArr;
    ArrayList<String> testArray;

    /**
     * Check the valid email or not
     *
     * @param target
     * @return
     */
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        SharedPreferences prefhh = getApplicationContext().getSharedPreferences("termsconditionn", MODE_PRIVATE);
        SharedPreferences.Editor editorss = prefhh.edit();
        editorss.putString("termsandconditions", "0");
        editorss.apply();
        editorss.commit();


        APP_ID = getResources().getString(R.string.facebook_app_id);
        facebook = new Facebook(APP_ID);


        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
        SharedPreferences.Editor editora = getcard.edit();
        editora.clear();
        editora.apply();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("beforedismiss", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("before", "0");
        editor.commit();


        logIn = LoginActivity.this;
        HideSoftKeyboard.setupUI(
                LoginActivity.this.getWindow().getDecorView(),
                LoginActivity.this);
        classAndWidgetInitialize();
        getIntentValues();
        initFaceBookSdk();
    }

    private void getIntentValues() {
        Intent intent = getIntent();
        mySelectedSubCatIdArr = (ArrayList<String>) intent.getSerializableExtra("selectedSubCategories");
    }

    private void classAndWidgetInitialize() {
        mySession = new SharedPreference(LoginActivity.this);
        myCalBackManager = CallbackManager.Factory.create();

        tf = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Medium.ttf");

        myConnectionManager = new ConnectionDetector(LoginActivity.this);
        myEmailET = findViewById(R.id.activity_login_ET_email);
        myPasswordET = findViewById(R.id.activity_login_ET_password);
        myForgotPasswordTXT = findViewById(R.id.activity_login_TXT_forgot_password);
        myRegisterTXT = findViewById(R.id.activity_login_TXT_register);
        myLoginBTN = findViewById(R.id.activity_login_BTN_login);
        myFacebookBTN = findViewById(R.id.activity_login_BTN_facebook);
        mySkipHomePageBTN = findViewById(R.id.activity_login_BTN_skip);
        backarrow = findViewById(R.id.backarrow);
        activity_login_TXT_title = findViewById(R.id.activity_login_TXT_title);


        clickListener();
    }

    private void clickListener() {
        myRegisterTXT.setOnClickListener(this);
        myForgotPasswordTXT.setOnClickListener(this);
        myLoginBTN.setOnClickListener(this);
        myFacebookBTN.setOnClickListener(this);

        backarrow.setOnClickListener(this);
        activity_login_TXT_title.setOnClickListener(this);
    }

    private void initFaceBookSdk() {
        FacebookSdk.sdkInitialize(LoginActivity.this);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.activity_login_TXT_register:
                Intent aSignupIntent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(aSignupIntent);
                finish();
                break;
            case R.id.activity_login_TXT_forgot_password:
                Intent aIntent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(aIntent);
                break;

            case R.id.activity_login_BTN_login:
                checkValues();
                break;

            case R.id.activity_login_BTN_facebook:
                logoutFromFacebook();
                loginToFacebook();
                break;

            case R.id.backarrow:
                finish();
                break;

            case R.id.activity_login_TXT_title:
                finish();
                break;


        }
    }

    private void loginToFacebook() {
        System.out.println("---------------facebook login1-----------------------");
        mPrefs = getSharedPreferences("CASPreferences", Context.MODE_PRIVATE);
        String access_token = mPrefs.getString("access_token", null);
        long expires = mPrefs.getLong("access_expires", 0);

        if (access_token != null) {
            facebook.setAccessToken(access_token);
        }


        System.out.println("---------------facebook expires-----------------------" + expires);

        if (expires != 0) {
            facebook.setAccessExpires(expires);
        }

        System.out.println("---------------facebook isSessionValid-----------------------" + facebook.isSessionValid());
        //   if (!facebook.isSessionValid()) {

        facebook.authorize(LoginActivity.this,
                new String[]{"email"},
                new Facebook.DialogListener() {

                    @Override
                    public void onCancel() {
                        // Function to handle cancel event
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.class_approve_estimate_failed), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onComplete(Bundle values) {
                        // Function to handle complete event
                        // Edit Preferences and update facebook acess_token
                        SharedPreferences.Editor editor = mPrefs.edit();
                        editor.putString("access_token",
                                facebook.getAccessToken());
                        editor.putLong("access_expires", facebook.getAccessExpires());
                        editor.commit();
                        String accessToken = facebook.getAccessToken();
                        System.out.println("Token----------------" + accessToken);
                        Handler handler = new Handler();
                        handler.post(dialogFacebookRunnable);

                        String accessToken1 = facebook.getAccessToken();

                        JsonRequest("https://graph.facebook.com/me?fields=id,name,picture,email&access_token=" + accessToken1);
                    }

                    @Override
                    public void onError(DialogError error) {
                        // Function to handle error
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.class_approve_estimate_failed), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFacebookError(FacebookError fberror) {
                        // Function to handle Facebook errors
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.class_approve_estimate_failed), Toast.LENGTH_LONG).show();
                    }
                });
        // }
//
//            LoginManager.getInstance().logInWithReadPermissions(this,
//                    Arrays.asList("public_profile", "user_friends", "email"));
//            LoginManager.getInstance().registerCallback(myCalBackManager,
//                    new FacebookCallback<LoginResult>() {
//                        @Override
//                        public void onSuccess(LoginResult loginResult) {
//                            GetFacebookData(loginResult);
//                        }
//
//                        @Override
//                        public void onCancel() {
//                        }
//
//                        @Override
//                        public void onError(FacebookException exception) {
//                            System.out.println("-----exception--" + exception);
//                        }
//                    });


    }

    private void logoutFromFacebook() {
        Util.clearCookies(LoginActivity.this);
        SharedPreferences.Editor editor = getSharedPreferences("CASPreferences", Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
    }

    private void checkValues() {
        if (getEditTextValue(myEmailET).length() == 0) {
            myEmailET.requestFocus();

            Animation shake = AnimationUtils.loadAnimation(LoginActivity.this,
                    R.anim.shake);
            myEmailET.startAnimation(shake);
            SpannableString s = new SpannableString(getResources().getString(R.string.class_login_activity_enter_username));
            s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            myEmailET.setError(s);

        } else if (getEditTextValue(myPasswordET).length() == 0) {
            myPasswordET.requestFocus();

            Animation shake = AnimationUtils.loadAnimation(LoginActivity.this,
                    R.anim.shake);
            myPasswordET.startAnimation(shake);
            SpannableString s = new SpannableString(getResources().getString(R.string.class_login_activity_enter_password));
            s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            myPasswordET.setError(s);

        } else {
            getValues();
        }
    }

    private void getValues() {
        if (myConnectionManager.isConnectingToInternet()) {
            postLoginRequest(LoginActivity.this, ServiceConstant.loginUrl);

        } else {
            final internetmissing mDialog = new internetmissing(LoginActivity.this);
            mDialog.setPositiveButton(
                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );

            mDialog.show();

        }
    }

    /**
     * Get the Edit text values
     *
     * @param aEditText
     * @return
     */
    private String getEditTextValue(EditText aEditText) {
        return aEditText.getText().toString().trim();
    }

    /**
     * Login Post Request
     *
     * @param mContext
     * @param url
     */
    private void postLoginRequest(Context mContext, String url) {
        myDialog = new ProgressDialogcreated(LoginActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", myEmailET.getText().toString());
        jsonParams.put("password", myPasswordET.getText().toString());


        jsonParams.put("deviceToken", "");


        if (mySession.getFCMId().equals("") || mySession.getFCMId().equals(" ")) {
            jsonParams.put("gcm_id", "Avgd");
        } else {
            jsonParams.put("gcm_id", mySession.getFCMId());
        }


        System.out.println("-----------email--------------" + myEmailET.getText().toString());
        System.out.println("-----------password--------------" + myPasswordET.getText().toString());
        System.out.println("------------deviceToken-------------");
        System.out.println("------------FCM_id-------------" + mySession.getFCMId());

        ServiceRequest mRequest = new ServiceRequest(mContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------login response------------" + response);

                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equals("1")) {

                        UserInfoPojo aUInfoPojo = new UserInfoPojo();

                        aUInfoPojo.setUserName(object.getString("user_name"));
                        aUInfoPojo.setUserId(object.getString("user_id"));
                        getuserid = object.getString("user_id");
                        aUInfoPojo.setUserEmail(object.getString("email"));
                        aUInfoPojo.setUserProfileImage(object.getString("user_image"));
                        aUInfoPojo.setUserCountryCode(object.getString("country_code"));
                        aUInfoPojo.setUserPhoneNumber(object.getString("phone_number"));
                        aUInfoPojo.setUserWalletAmount(object.getString("wallet_amount"));
                        aUInfoPojo.setSocketKey(object.getString("soc_key"));

                        if (object.has("termsandconditions")) {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("termsconditionn", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("termsandconditions", object.getString("termsandconditions"));
                            editor.apply();
                            editor.commit();
                        }

                        if (object.has("zipcode")) {
                            SharedPreferences getcard;
                            getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
                            SharedPreferences.Editor editor = getcard.edit();
                            editor.putString("zipcode", object.getString("zipcode"));
                            editor.apply();
                            editor.commit();
                        }


                        mySession.putUserDetails(aUInfoPojo);
                        mySession.setLoginStatus(true);
                        mySession.putCurrencySymbol(CurrencySymbolConverter.getCurrencySymbol(object.getString("currency")));


                        try {

                            Intent intent = new Intent(LoginActivity.this, ChatMessageService.class);
                            startService(intent);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        SharedPreferences prefd = getApplicationContext().getSharedPreferences("logintoconform", 0);
                        String wheretogo = prefd.getString("lo", "");
                        if (wheretogo.equalsIgnoreCase("1")) {
                           /* Intent intent = new Intent(LoginActivity.this, BookingConfirmationActivity.class);
                            startActivity(intent);*/
                            finish();
                        } else {
                            /*Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            intent.putExtra("selectedSubCategories", mySelectedSubCatIdArr);
                            startActivity(intent);*/
                            finish();
                        }


                        postMode(getApplicationContext(), ServiceConstant.Notification_mode);

                    } else {

                        final PkDialog mDialog = new PkDialog(LoginActivity.this);
                        mDialog.setDialogTitle("Error");
                        if (object.has("message")) {

                            mDialog.setDialogMessage(object.getString("message"));
                            mDialog.setPositiveButton(
                                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();
                                        }
                                    }
                            );

                        } else {

                            mDialog.setDialogMessage("Failed to login");
                            mDialog.setPositiveButton(
                                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();
                                        }
                                    }
                            );

                        }

                        mDialog.show();

                    }

                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void JsonRequest(final String Url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        ServiceRequest mRequest = new ServiceRequest(LoginActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------access token reponse-------------------" + response);
                String sMediaId = "";


                try {

                    JSONObject object = new JSONObject(response);
                    System.out.println("---------facebook profile------------" + response);


                    sMediaId = object.getString("id");
                    myFbUserIdSTR = object.getString("id");
                    myFbUserImageSTR = "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large";
                    myFbUserNameSTR = object.getString("name");
                    myFbUserNameSTR = myFbUserNameSTR.replaceAll("\\s+", "");


                    if (object.has("email")) {
                        myFbUserEmailSTR = object.getString("email");
                    } else {
                        myFbUserEmailSTR = "";
                    }
                    System.out.println("-------sMediaId------------------" + myFbUserEmailSTR);
                    System.out.println("-------email------------------" + myFbUserEmailSTR);
                    System.out.println("-----------------userid-------------------------------" + myFbUserIdSTR);
                    System.out.println("----------------profile_image-----------------" + myFbUserImageSTR);
                    System.out.println("-----------username----------" + myFbUserNameSTR);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                //post execute
                dialog.dismiss();

                postRequestFaceBookLogin(LoginActivity.this, ServiceConstant.facebook_login_url);
                //  PostRequest_facebook(ServiceConstant.social_check_url);
                //  PostRequest_facebook_login(ServiceConstant.facebook_login_url);
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void GetFacebookData(final LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {

                            Log.e("fb_response", object.toString(1));
                            String aProfileIMG = "https://graph.facebook.com/" + response.getJSONObject().getString("id") + "/picture?type=large";

                            //postRequestFaceBookLogin(LoginActivity.this, ServiceConstant.facebook_login_url, object, aProfileIMG);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

        Bundle bundle = new Bundle();
        bundle.putString("fields", "id,email,first_name,last_name,gender, birthday");
        request.setParameters(bundle);
        request.executeAsync();
    }

    private void postRequestFaceBookLogin(Context aContext, String url) {
        myDialog = new ProgressDialogcreated(LoginActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        //            myFbUserNameSTR = aObject.getString("first_name") + " " + aObject.getString("last_name");
//            myFbUserEmailSTR = aObject.getString("email");
//            myFbUserIdSTR = aObject.getString("id");
//            myFbUserImageSTR = aProfileIMG;
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("fb_id", myFbUserIdSTR);
        jsonParams.put("deviceToken", "");
        jsonParams.put("gcm_id", mySession.getFCMId());
        jsonParams.put("email_id", myFbUserEmailSTR);
        jsonParams.put("prof_pic", myFbUserImageSTR);

        Log.i("jsonparam", "" + jsonParams);

        System.out.println("-----------media_id 1------------" + myFbUserIdSTR);
        System.out.println("-----------deviceToken 1------------" + "");
        System.out.println("-----------FCM_id 1------------" + mySession.getFCMId());
        System.out.println("-----------email 1------------" + myFbUserEmailSTR);
        System.out.println("---------------pro image------" + myFbUserImageSTR);

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------facebook login response------------" + response);

                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equals("1")) {

                        UserInfoPojo aUInfoPojo = new UserInfoPojo();

                        aUInfoPojo.setUserName(object.getString("user_name"));
                        aUInfoPojo.setUserId(object.getString("user_id"));
                        aUInfoPojo.setUserEmail(object.getString("email"));
                        aUInfoPojo.setUserProfileImage(object.getString("user_image"));
                        aUInfoPojo.setUserCountryCode(object.getString("country_code"));
                        aUInfoPojo.setUserPhoneNumber(object.getString("phone_number"));
                        aUInfoPojo.setUserWalletAmount(object.getString("wallet_amount"));
                        aUInfoPojo.setSocketKey(object.getString("soc_key"));

                        mySession.putCurrencySymbol(CurrencySymbolConverter.getCurrencySymbol(object.getString("currency")));

                        mySession.putUserDetails(aUInfoPojo);
                        mySession.setLoginStatus(true);

                        HomeActivity.myHomeActivity.finish();
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();

                    } else {

                        Intent aFBRegIntent = new Intent(LoginActivity.this, FacebookRegisterActivity.class);
                        aFBRegIntent.putExtra("userName", myFbUserNameSTR);
                        aFBRegIntent.putExtra("userImage", myFbUserImageSTR);
                        aFBRegIntent.putExtra("userId", myFbUserIdSTR);
                        aFBRegIntent.putExtra("userEmail", myFbUserEmailSTR);
                        aFBRegIntent.putExtra("media", myFbUserIdSTR);
                        startActivity(aFBRegIntent);
                    }

                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("---------------------------" + requestCode + resultCode + data);

        facebook.authorizeCallback(requestCode, resultCode, data);

        myCalBackManager.onActivityResult(requestCode, resultCode, data);
    }


    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(LoginActivity.this,
                R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(
                Color.parseColor("#cc0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    private void postMode(Context mContext, String url) {


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user", getuserid);
        jsonParams.put("user_type", "user");
        jsonParams.put("mode", "gcm");
        jsonParams.put("type", "android");


        ServiceRequest mRequest = new ServiceRequest(mContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equals("1")) {

                        System.out.println("---------mode response------------" + response);

                    } else {


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

            }
        });
    }
}
