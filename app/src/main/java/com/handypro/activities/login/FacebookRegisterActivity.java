package com.handypro.activities.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.CommonWebView;
import com.handypro.activities.PrivacyPolicyPageActivity;
import com.handypro.activities.RegisterLocationSearchActivity;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.core.Facebook.Util;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by CAS63 on 2/19/2018.
 */

public class FacebookRegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private String myUserNameSTR = "", myUserIdSTR = "", myUserEmailSTR = "",
            myUserMediaSTR = "", myUserImgSTR = "", MainFirstName = "", MainLastName = "";
    private CustomEdittext activity_facebook_register_first_name, activity_facebook_register_last_name,
            activity_facebook_register_user_name, myEmailET, myPasswordET, myMobileNumberET, activity_facebook_register_ET_refercodee;
    private RelativeLayout myBackLay;
    private CustomButton myRegisterBTN;
    TextView myTermsTXT;
    private ConnectionDetector myConnectionManager;
    private CustomTextView myCountryCodeTXT;
    private SharedPreference mySession;
    private ProgressDialogcreated myDialog;
    private CountryPicker picker;
    TextView address;
    LinearLayout showmarketing;
    MaterialSpinner materialamount;
    EditText typemarketing;
    TextView privacypolicy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_register);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInit();
        getIntentValues();
        openPicker();
    }

    private void classAndWidgetInit() {
        myConnectionManager = new ConnectionDetector(FacebookRegisterActivity.this);
        mySession = new SharedPreference(FacebookRegisterActivity.this);
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_label));

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Medium.ttf");
        materialamount = findViewById(R.id.materialamount);
        materialamount.setTypeface(tf);
        privacypolicy = findViewById(R.id.privacypolicy);
        myTermsTXT = findViewById(R.id.activity_signup_TXT_terms_of_use);
        typemarketing = findViewById(R.id.typemarketing);
        showmarketing = findViewById(R.id.showmarketing);
        activity_facebook_register_first_name = findViewById(R.id.activity_facebook_register_first_name);
        activity_facebook_register_last_name = findViewById(R.id.activity_facebook_register_last_name);
        activity_facebook_register_user_name = findViewById(R.id.activity_facebook_register_user_name);

        myEmailET = findViewById(R.id.activity_facebook_register_ET_email_address);
        myBackLay = findViewById(R.id.activity_facebook_register_LAY_back);
        myRegisterBTN = findViewById(R.id.activity_facebook_register_BTN_register);
        myPasswordET = findViewById(R.id.activity_facebook_register_ET_password);
        activity_facebook_register_ET_refercodee = findViewById(R.id.activity_facebook_register_ET_refercodee);

        address = findViewById(R.id.activity_facebook_register_ET_referal_code);
        myMobileNumberET = findViewById(R.id.activity_facebook_register_ET_phone_number);
        myCountryCodeTXT = findViewById(R.id.activity_facebook_register_ET_phone_code);
        myBackLay.setOnClickListener(this);
        myRegisterBTN.setOnClickListener(this);
        myCountryCodeTXT.setOnClickListener(this);
        address.setOnClickListener(this);
        myPasswordET.requestFocus();

        privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SharedPreferences getcard;
                getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
                String zipcode = getcard.getString("zipcode", "");

                Intent intent = new Intent(FacebookRegisterActivity.this, PrivacyPolicyPageActivity.class);
                intent.putExtra("url", zipcode);
                intent.putExtra("header", getResources().getString(R.string.privacypolicy));
                startActivity(intent);
            }
        });

        myTermsTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = ServiceConstant.Terms_Conditions_Url;

                SharedPreferences getcard;
                getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
                String zipcode = getcard.getString("zipcode", "");

                Intent intent = new Intent(FacebookRegisterActivity.this, CommonWebView.class);
                intent.putExtra("url", zipcode);
                intent.putExtra("type", "reg");
                intent.putExtra("header", getResources().getString(R.string.activity_signup_TXT_signup_terms_use));
                startActivity(intent);
            }
        });

        activity_facebook_register_first_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                MainFirstName = editable.toString();
                updateUserName(MainFirstName, MainLastName);
            }
        });

        activity_facebook_register_last_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                MainLastName = editable.toString();
                updateUserName(MainFirstName, MainLastName);
            }
        });
    }

    void updateUserName(String FirstName, String LastName) {
        activity_facebook_register_user_name.setText(FirstName + " " + LastName);
    }

    private void getIntentValues() {
        Intent intent = getIntent();
        myUserIdSTR = intent.getStringExtra("userId");
        myUserNameSTR = intent.getStringExtra("userName");
        myUserEmailSTR = intent.getStringExtra("userEmail");
        myUserMediaSTR = intent.getStringExtra("media");
        myUserImgSTR = intent.getStringExtra("userImage");

        if (!myUserNameSTR.equalsIgnoreCase("")) {
            activity_facebook_register_first_name.setText(myUserNameSTR);
        }
        if (!myUserEmailSTR.equalsIgnoreCase("")) {
            myEmailET.setText(myUserEmailSTR);
        }

    }

    private void openPicker() {
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                myCountryCodeTXT.setText(dialCode);
                if (myCountryCodeTXT.getText().length() > 0) {
                    myMobileNumberET.setError(null);
                }

                myMobileNumberET.requestFocus();


                // close keyboard
                InputMethodManager mgr_username = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr_username.hideSoftInputFromWindow(myMobileNumberET.getWindowToken(), 0);

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_facebook_register_LAY_back:

                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(myBackLay.getWindowToken(), 0);

                logoutFromFacebook();

                onBackPressed();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();
                break;


            case R.id.activity_facebook_register_BTN_register:
                checkValues();
                break;

            case R.id.activity_facebook_register_ET_referal_code:
                Intent intent1 = new Intent(FacebookRegisterActivity.this, RegisterLocationSearchActivity.class);
                startActivity(intent1);
                break;

            case R.id.activity_signup_ET_phone_code:
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(myEmailET.getWindowToken(), 0);

                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;

        }
    }

    private void checkValues() {
        if (getEditTextValue(activity_facebook_register_first_name).length() == 0) {
            activity_facebook_register_first_name.requestFocus();
            HNDHelper.showErrorAlert(FacebookRegisterActivity.this, getResources().getString(R.string.class_signup_enter_first_name));
        } else if (getEditTextValue(activity_facebook_register_last_name).length() == 0) {
            activity_facebook_register_last_name.requestFocus();
            HNDHelper.showErrorAlert(FacebookRegisterActivity.this, getResources().getString(R.string.class_signup_enter_last_name));
        }else if (getEditTextValue(activity_facebook_register_user_name).length() == 0) {
            activity_facebook_register_user_name.requestFocus();
            HNDHelper.showErrorAlert(FacebookRegisterActivity.this, getResources().getString(R.string.class_signup_enter_user_name));
        } else if (getEditTextValue(myEmailET).length() == 0) {
            myEmailET.requestFocus();
            HNDHelper.showErrorAlert(FacebookRegisterActivity.this, getResources().getString(R.string.error_empty_emails));
        } else if (!SignupActivity.isValidEmail(getEditTextValue(myEmailET))) {
            myEmailET.requestFocus();
            HNDHelper.showErrorAlert(FacebookRegisterActivity.this, getResources().getString(R.string.error_enter_valid_email));
        } else if (getEditTextValue(myPasswordET).length() == 0) {
            myPasswordET.requestFocus();
            HNDHelper.showErrorAlert(FacebookRegisterActivity.this, getResources().getString(R.string.error_empty_password));
        } else if (getEditTextValue(myMobileNumberET).length() == 0) {
            myMobileNumberET.requestFocus();
            HNDHelper.showErrorAlert(FacebookRegisterActivity.this, getResources().getString(R.string.error_empty_mobile_number));
        } else if (address.getText().toString().length() == 0) {

            HNDHelper.showErrorAlert(FacebookRegisterActivity.this, getResources().getString(R.string.error_empty_mobile_address));
        } else {

            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
            String zipcode = getcard.getString("zipcode", "");
            if (zipcode.length() < 2) {
                HNDHelper.showErrorAlert(FacebookRegisterActivity.this, "Please choose address to continue");
            } else {
                getValues();
            }
        }
    }

    private void getValues() {
        if (myConnectionManager.isConnectingToInternet()) {
            postFBRegisterCheckRequest(FacebookRegisterActivity.this, ServiceConstant.social_check_url);
        } else {
            HNDHelper.showErrorAlert(FacebookRegisterActivity.this, getResources().getString(R.string.nointernet_text));
        }
    }

    private void postFBRegisterCheckRequest(Context aContext, String url) {


        myDialog = new ProgressDialogcreated(FacebookRegisterActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email_id", myEmailET.getText().toString());
        jsonParams.put("user_name", activity_facebook_register_user_name.getText().toString());
        jsonParams.put("user_first_name", activity_facebook_register_first_name.getText().toString());
        jsonParams.put("user_last_name", activity_facebook_register_last_name.getText().toString());
        jsonParams.put("country_code", myCountryCodeTXT.getText().toString());
        jsonParams.put("phone", myMobileNumberET.getText().toString());

        jsonParams.put("referal_code", activity_facebook_register_ET_refercodee.getText().toString());
        jsonParams.put("deviceToken", "");
        jsonParams.put("gcm_id", mySession.getFCMId());


        System.out.println("email-----------" + myEmailET.getText().toString());
        System.out.println("user_name-----------" + activity_facebook_register_first_name.getText().toString());
        System.out.println("country_code-----------" + myCountryCodeTXT.getText().toString());
        System.out.println("phone_number-----------" + myMobileNumberET.getText().toString());
        System.out.println("FCM_id-----------" + mySession.getFCMId());


        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------register response------------" + response);

                String Sstatus = "", Smessage = "";
                String otp = "", otp_status = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        otp = object.getString("otp");
                        otp_status = object.getString("otp_status");


                        Intent intent = new Intent(getApplicationContext(), FacebookotpActivity.class);
                        intent.putExtra("Otp_Status", otp_status);
                        intent.putExtra("Otp", otp);
                        intent.putExtra("UserName", activity_facebook_register_user_name.getText().toString());
                        intent.putExtra("user_first_name", activity_facebook_register_first_name.getText().toString());
                        intent.putExtra("user_last_name", activity_facebook_register_last_name.getText().toString());
                        intent.putExtra("Email", myEmailET.getText().toString());
                        intent.putExtra("Phone", myMobileNumberET.getText().toString());
                        intent.putExtra("ReferralCode", activity_facebook_register_ET_refercodee.getText().toString());
                        intent.putExtra("CountryCode", myCountryCodeTXT.getText().toString());
                        intent.putExtra("GcmID", mySession.getFCMId());
                        intent.putExtra("MediaId", myUserMediaSTR);
                        intent.putExtra("profileimage", myUserImgSTR);

                        if (typemarketing.getText().toString().equals("") || typemarketing.getText().toString().equals(" ")) {
                            intent.putExtra("marketing", materialamount.getText().toString());
                        } else {
                            intent.putExtra("marketing", typemarketing.getText().toString());
                        }


                        startActivity(intent);
                        finish();

                    } else {
                        HNDHelper.showResponseErrorAlert(FacebookRegisterActivity.this, Smessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });


    }


    private void logoutFromFacebook() {
        Util.clearCookies(FacebookRegisterActivity.this);
        // your sharedPrefrence
        SharedPreferences.Editor editor = getSharedPreferences("CASPreferences", Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
    }

    /**
     * Get the Edit text values
     *
     * @param aEditText
     * @return
     */
    private String getEditTextValue(EditText aEditText) {
        return aEditText.getText().toString().trim();
    }


    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
        String line1 = getcard.getString("line1", "");
        String line2 = getcard.getString("line2", "");
        String city = getcard.getString("city", "");
        String state = getcard.getString("state", "");
        String zipcode = getcard.getString("zipcode", "");
        String country = getcard.getString("country", "");
        String formatted_address = getcard.getString("formatted_address", "");


        String Selected_Latitude = getcard.getString("Selected_Latitude", "");
        String Selected_Longitude = getcard.getString("Selected_Longitude", "");

        if (zipcode.length() < 2) {

        } else {

            address.setText(formatted_address);
            getdata(FacebookRegisterActivity.this, ServiceConstant.marketingdata, zipcode);
        }

    }


    private void getdata(Context mContext, String url, String zipcode) {


        myDialog = new ProgressDialogcreated(FacebookRegisterActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("zipcode", zipcode);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {

                        JSONObject info_Object = jobject.getJSONObject("response");

                        JSONArray sources = info_Object.getJSONArray("sources");
                        String[] country = new String[sources.length()];
                        for (int b = 0; b < sources.length(); b++) {
                            country[b] = sources.getString(b);

                        }


                        materialamount.setItems(country);
                        showmarketing.setVisibility(View.VISIBLE);
                        typemarketing.setVisibility(View.GONE);
                        typemarketing.setText("");
                        materialamount.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                            @Override
                            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {

                            }
                        });


                    } else {
                        showmarketing.setVisibility(View.GONE);
                        typemarketing.setVisibility(View.VISIBLE);
                        typemarketing.setText("");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                myDialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                myDialog.dismiss();
            }
        });
    }
}
