package com.handypro.activities.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.handypro.Dialog.PkDialog;
import com.handypro.Dialog.internetmissing;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.CommonWebView;
import com.handypro.activities.OTPScreenActivity;
import com.handypro.activities.PrivacyPolicyPageActivity;
import com.handypro.activities.RegisterLocationSearchActivity;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;
import com.handypro.textview.CustomTextView;
import com.handypro.textview.TypefaceSpan;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.HideSoftKeyboard;
import com.handypro.volley.ServiceRequest;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by CAS61 on 2/6/2018.
 */

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    private CustomEdittext activity_signup_first_name, activity_signup_last_name, activity_signup_user_name, myEmailET,appartmentnumber;
    private CustomEdittext myPasswordET, myMobileNumberET, activity_signup_refreallcode;
    TextView myReferralET;
    private CustomTextView myCountryCodeTXT, myTermsTXT, myLoginTXT;
    private CustomButton myRegisterBTN;
    private RelativeLayout myBackLAY;
    private ConnectionDetector myConnectionManager;
    private SharedPreference mySession;
    private ProgressDialogcreated myDialog;
    public static AppCompatActivity SignUp;
    private String myCheckClass = "", MainFirstName = "", MainLastName = "";
    private CountryPicker picker;
    Typeface tf;
    Animation shake;
    MaterialSpinner materialamount;
    LinearLayout showmarketing;
    EditText typemarketing;
    TextView privacypolicy;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
        SharedPreferences.Editor editor = getcard.edit();
        editor.clear();
        editor.commit();
        editor.apply();


        SignUp = SignupActivity.this;
        HideSoftKeyboard.setupUI(
                SignupActivity.this.getWindow().getDecorView(),
                SignupActivity.this);
        initializeHeaderView();
        classAndWidgetInitialize();
        getIntentValues();
        openPicker();
    }

    private void openPicker() {
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                myCountryCodeTXT.setText(dialCode);
                if (myCountryCodeTXT.getText().length() > 0) {
                    myMobileNumberET.setError(null);
                }

                myMobileNumberET.requestFocus();


                // close keyboard
                InputMethodManager mgr_username = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr_username.hideSoftInputFromWindow(myMobileNumberET.getWindowToken(), 0);

            }
        });
    }

    private void getIntentValues() {
        Intent intent = getIntent();
        if (intent != null) {
            myCheckClass = intent.getStringExtra("IntentClass");
        }
    }

    private void initializeHeaderView() {
        myBackLAY = findViewById(R.id.activity_signup_LAY_back);
        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aSignupIntent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(aSignupIntent);
                finish();
            }
        });
    }

    private void classAndWidgetInitialize() {
        mySession = new SharedPreference(SignupActivity.this);
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_label));

        shake = AnimationUtils.loadAnimation(SignupActivity.this,
                R.anim.shake);
        tf = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Medium.ttf");

        materialamount = findViewById(R.id.materialamount);
        materialamount.setTypeface(tf);

        privacypolicy = findViewById(R.id.privacypolicy);

        typemarketing = findViewById(R.id.typemarketing);

        showmarketing = findViewById(R.id.showmarketing);

        myConnectionManager = new ConnectionDetector(SignupActivity.this);
        activity_signup_user_name = findViewById(R.id.activity_signup_user_name);
        activity_signup_first_name = findViewById(R.id.activity_signup_first_name);
        activity_signup_last_name = findViewById(R.id.activity_signup_last_name);
        myEmailET = findViewById(R.id.activity_signup_ET_email_address);
        appartmentnumber = findViewById(R.id.appartmentnumber);
        myPasswordET = findViewById(R.id.activity_signup_ET_password);
        myMobileNumberET = findViewById(R.id.activity_signup_ET_phone_number);
        activity_signup_refreallcode = findViewById(R.id.activity_signup_refreallcode);
        myReferralET = findViewById(R.id.activity_signup_ET_referal_code);
        myCountryCodeTXT = findViewById(R.id.activity_signup_ET_phone_code);
        myTermsTXT = findViewById(R.id.activity_signup_TXT_terms_of_use);
        myLoginTXT = findViewById(R.id.activity_signup_TXT_login);
        myRegisterBTN = findViewById(R.id.activity_signup_BTN_register);
        clickListener();
        privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SharedPreferences getcard;
                getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
                String zipcode = getcard.getString("zipcode", "");

                Intent intent = new Intent(SignupActivity.this, PrivacyPolicyPageActivity.class);
                intent.putExtra("url", zipcode);
                intent.putExtra("header", getResources().getString(R.string.privacypolicy));
                startActivity(intent);
            }
        });


    }

    private void clickListener() {
        myLoginTXT.setOnClickListener(this);
        myTermsTXT.setOnClickListener(this);
        myRegisterBTN.setOnClickListener(this);
        myCountryCodeTXT.setOnClickListener(this);
        myReferralET.setOnClickListener(this);

        activity_signup_first_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                MainFirstName = editable.toString();
                updateUserName(MainFirstName, MainLastName);
            }
        });

        activity_signup_last_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                MainLastName = editable.toString();
                updateUserName(MainFirstName, MainLastName);
            }
        });
    }

    void updateUserName(String FirstName, String LastName) {
        activity_signup_user_name.setText(FirstName + " " + LastName);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.activity_signup_TXT_terms_of_use:
                String url = ServiceConstant.Terms_Conditions_Url;

                SharedPreferences getcard;
                getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
                String zipcode = getcard.getString("zipcode", "");

                Intent intent = new Intent(SignupActivity.this, CommonWebView.class);
                intent.putExtra("url", zipcode);
                intent.putExtra("type", "reg");
                intent.putExtra("header", getResources().getString(R.string.activity_signup_TXT_signup_terms_use));
                startActivity(intent);

//                Intent aJobDetailIntent = new Intent(SignupActivity.this, JobDetailActivity.class);
//                startActivity(aJobDetailIntent);
                break;

            case R.id.activity_signup_ET_referal_code:
                Intent intent1 = new Intent(SignupActivity.this, RegisterLocationSearchActivity.class);
                startActivity(intent1);
                break;


            case R.id.activity_signup_TXT_login:
                Intent aSignupIntent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(aSignupIntent);
                finish();
                break;


            case R.id.activity_signup_BTN_register:
                checkValues();
                break;

            case R.id.activity_signup_ET_phone_code:
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(myEmailET.getWindowToken(), 0);

                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;
        }
    }

    private void checkValues() {
        if (getEditTextValue(activity_signup_first_name).length() == 0) {
            activity_signup_first_name.requestFocus();

            activity_signup_first_name.startAnimation(shake);
            SpannableString s = new SpannableString(getResources().getString(R.string.class_signup_enter_first_name));
            s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            activity_signup_first_name.setError(s);

        } else if (getEditTextValue(activity_signup_last_name).length() == 0) {
            activity_signup_last_name.requestFocus();

            activity_signup_last_name.startAnimation(shake);
            SpannableString s = new SpannableString(getResources().getString(R.string.class_signup_enter_last_name));
            s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            activity_signup_last_name.setError(s);

        }else if (getEditTextValue(activity_signup_user_name).length() == 0) {
            activity_signup_user_name.requestFocus();

            activity_signup_user_name.startAnimation(shake);
            SpannableString s = new SpannableString(getResources().getString(R.string.class_signup_enter_last_name));
            s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            activity_signup_user_name.setError(s);

        } else if (getEditTextValue(myEmailET).length() == 0) {
            myEmailET.requestFocus();
            myEmailET.startAnimation(shake);
            SpannableString s = new SpannableString(getResources().getString(R.string.class_signup_enter_emaiid));
            s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            myEmailET.setError(s);
        } else if (!isValidEmail(getEditTextValue(myEmailET))) {
            myEmailET.requestFocus();
            myEmailET.startAnimation(shake);
            SpannableString s = new SpannableString(getResources().getString(R.string.class_signup_enter_valid_emaiid));
            s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            myEmailET.setError(s);
        } else if (getEditTextValue(myPasswordET).length() == 0) {
            myPasswordET.requestFocus();
            myPasswordET.startAnimation(shake);
            SpannableString s = new SpannableString(getResources().getString(R.string.error_empty_password));
            s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            myPasswordET.setError(s);
        } else if (getEditTextValue(myMobileNumberET).length() == 0) {
            myMobileNumberET.requestFocus();
            myMobileNumberET.startAnimation(shake);
            SpannableString s = new SpannableString(getResources().getString(R.string.class_signup_enter_mobileno));
            s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            myMobileNumberET.setError(s);
        } else if (myCountryCodeTXT.length() == 0) {
            final PkDialog mDialog = new PkDialog(SignupActivity.this);
            mDialog.setDialogTitle(getResources().getString(R.string.class_signup_code));
            mDialog.setDialogMessage(getResources().getString(R.string.class_signup_choose_code));
            mDialog.setPositiveButton(
                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );

            mDialog.show();

        } else {


            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
            String zipcode = getcard.getString("zipcode", "");
            if (zipcode.length() < 2) {
                HNDHelper.showErrorAlert(SignupActivity.this, getResources().getString(R.string.class_signup_choose_address));
            } else {
                getValues();
            }


        }
    }

    private void getValues() {
        if (myConnectionManager.isConnectingToInternet()) {
            postRegisterRequest(SignupActivity.this);
        } else {
            final internetmissing mDialog = new internetmissing(SignupActivity.this);
            mDialog.setPositiveButton(
                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );

            mDialog.show();
        }
    }


    /**
     * Register Post Request
     *
     * @param mContext
     */

    private void postRegisterRequest(final Context mContext) {
        myDialog = new ProgressDialogcreated(SignupActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", myEmailET.getText().toString());
        jsonParams.put("password", myPasswordET.getText().toString());
        jsonParams.put("user_name", activity_signup_user_name.getText().toString());
        jsonParams.put("user_first_name", activity_signup_first_name.getText().toString());
        jsonParams.put("user_last_name", activity_signup_last_name.getText().toString());
        jsonParams.put("country_code", myCountryCodeTXT.getText().toString());
        jsonParams.put("phone_number", myMobileNumberET.getText().toString());
        jsonParams.put("referal_code", activity_signup_refreallcode.getText().toString());
        jsonParams.put("deviceToken", "");
        jsonParams.put("gcm_id", mySession.getFCMId());

        System.out.println("email-----------" + myEmailET.getText().toString());
        System.out.println("password-----------" + myPasswordET.getText().toString());
        System.out.println("user_name-----------" + activity_signup_first_name.getText().toString());
        System.out.println("country_code-----------" + myCountryCodeTXT.getText().toString());
        System.out.println("phone_number-----------" + myMobileNumberET.getText().toString());
        System.out.println("FCM_id-----------" + mySession.getFCMId());

        ServiceRequest mRequest = new ServiceRequest(mContext);
        mRequest.makeServiceRequest(ServiceConstant.RegisterUrl, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("---------register response------------" + response);
                String aStatusSTR = "";
                try {
                    JSONObject object = new JSONObject(response);
                    aStatusSTR = object.getString("status");

                    if (aStatusSTR.equalsIgnoreCase("1")) {

                        String pushNotificationKey = object.getString("key");
                        String otpStatus = object.getString("otp_status");
                        String otp = object.getString("otp");


                        Intent aIntent = new Intent(SignupActivity.this, OTPScreenActivity.class);
                        aIntent.putExtra("UserName", activity_signup_user_name.getText().toString());
                        aIntent.putExtra("user_first_name", activity_signup_first_name.getText().toString());
                        aIntent.putExtra("user_last_name", activity_signup_last_name.getText().toString());
                        aIntent.putExtra("Email", myEmailET.getText().toString());
                        aIntent.putExtra("Password", myPasswordET.getText().toString());
                        aIntent.putExtra("Phone", myMobileNumberET.getText().toString());
                        aIntent.putExtra("CountryCode", myCountryCodeTXT.getText().toString());
                        aIntent.putExtra("ReferralCode", activity_signup_refreallcode.getText().toString());
                        aIntent.putExtra("GcmID", pushNotificationKey);
                        aIntent.putExtra("appartmentnumber", appartmentnumber.getText().toString());
                        if (typemarketing.getText().toString().equals("") || typemarketing.getText().toString().equals(" ")) {
                            aIntent.putExtra("marketing", materialamount.getText().toString());
                        } else {
                            aIntent.putExtra("marketing", typemarketing.getText().toString());
                        }


                        aIntent.putExtra("Otp_Status", otpStatus);
                        aIntent.putExtra("Otp", otp);
                        aIntent.putExtra("IntentClass", myCheckClass);
//                        aIntent.putExtra("firstname",Et_firstname.getText().toString());
//                        aIntent.putExtra("lastname",Et_lastname.getText().toString());
                        startActivity(aIntent);
                        finish();

                    } else {

                        final PkDialog mDialog = new PkDialog(SignupActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.class_signup_error));
                        mDialog.setDialogMessage(object.getString("errors"));
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                    }
                                }
                        );

                        mDialog.show();

                    }
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }


    /**
     * Get the Edit text values
     *
     * @param aEditText
     * @return
     */
    private String getEditTextValue(EditText aEditText) {
        return aEditText.getText().toString().trim();
    }

    /**
     * Check the valid email or not
     *
     * @param target
     * @return
     */
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    @Override
    protected void onResume() {
        super.onResume();


        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
        String line1 = getcard.getString("line1", "");
        String line2 = getcard.getString("line2", "");
        String city = getcard.getString("city", "");
        String state = getcard.getString("state", "");
        String zipcode = getcard.getString("zipcode", "");
        String country = getcard.getString("country", "");
        String formatted_address = getcard.getString("formatted_address", "");


        String Selected_Latitude = getcard.getString("Selected_Latitude", "");
        String Selected_Longitude = getcard.getString("Selected_Longitude", "");

        if (zipcode.length() < 2) {

        } else {

            myReferralET.setText(formatted_address);


            getdata(SignupActivity.this, ServiceConstant.marketingdata, zipcode);


        }


    }


    private void getdata(Context mContext, String url, String zipcode) {


        myDialog = new ProgressDialogcreated(SignupActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("zipcode", zipcode);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {

                        JSONObject info_Object = jobject.getJSONObject("response");

                        JSONArray sources = info_Object.getJSONArray("sources");
                        String[] country = new String[sources.length()];
                        for (int b = 0; b < sources.length(); b++) {
                            country[b] = sources.getString(b);

                        }

                        if (sources.length() == 0) {
                            showmarketing.setVisibility(View.GONE);
                            typemarketing.setVisibility(View.VISIBLE);
                            typemarketing.setText("");
                        } else {
                            materialamount.setItems(country);
                            showmarketing.setVisibility(View.VISIBLE);
                            typemarketing.setVisibility(View.GONE);
                            materialamount.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                                @Override
                                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {

                                }
                            });
                        }


                    } else {
                        typemarketing.setVisibility(View.VISIBLE);
                        showmarketing.setVisibility(View.GONE);
                        typemarketing.setText("");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                myDialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                myDialog.dismiss();
            }
        });
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {


            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
