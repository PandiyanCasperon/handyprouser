package com.handypro.activities;

/**
 * Created by user127 on 02-04-2018.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.handypro.Dialog.PkDialog;
import com.handypro.Dialog.internetmissing;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.newpojoforchangeorder;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.CurrencySymbolConverter;
import com.handypro.volley.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.logging.SocketHandler;

import androidx.appcompat.app.AppCompatActivity;


/**
 * Created by user88 on 1/8/2016.
 */
public class MoreInfoPage extends AppCompatActivity {
    private ConnectionDetector cd;
    private Context context;
    SharedPreferences pref;
    LinearLayout hideamountded;
    private ProgressDialogcreated myDialog;
    private Boolean isInternetPresent = false;
    private boolean show_progress_status = false;
    private Handler mHandler;
    int approval_status = 0;
    String deductedamountfromserver = "0", source = "0", deductionde = "";
    private String provider_id = "";
    private listhei fare_listview;
    TextView ammoutde, TextViewChangeOrderHint;
    private TextView Tv_JobId;
    private String asyntask_name = "normal";

    String changeorderint = "0";

    PaymentFareSummeryAdapter adapter;
    private ArrayList<newpojoforchangeorder> farelist;
    private SharedPreference mySession;

    TextView Tvpaymentfare_jobId;
    private String Job_id = "";
    private boolean isPaymetFare = false;
    LinearLayout layout_fare_summery2;

    private RelativeLayout Rl_layout_back;
    private Button Bt_Request_payment, Bt_Receivecash;
    private SocketHandler socketHandler;
    TextView hintshow, Tvpaymentfare_job_description, textView_payment_fare_cost;
    JSONArray miscall;
    TextView grandt, amounded, amountdedreason;

    Double addingingvalue = 0.0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_info);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mySession = new SharedPreference(MoreInfoPage.this);

        pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
        Job_id = pref.getString("jobid", "");
        initialize();


        Rl_layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent InfoIntent = new Intent(MoreInfoPage.this, OrderDetailActivity.class);
                startActivity(InfoIntent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }

    private void initialize() {

        cd = new ConnectionDetector(MoreInfoPage.this);
        mHandler = new Handler();

        isInternetPresent = cd.isConnectingToInternet();

        // get user data from session
        amounded = findViewById(R.id.amounded);
        amountdedreason = findViewById(R.id.amountdedreason);

        farelist = new ArrayList<newpojoforchangeorder>();
        grandt = findViewById(R.id.grandt);
        hintshow = findViewById(R.id.hintshow);
        Tvpaymentfare_jobId = findViewById(R.id.Tvpaymentfare_jobId);
        layout_fare_summery2 = findViewById(R.id.layout_fare_summery2);
        fare_listview = findViewById(R.id.cancelreason_listView);
        Tv_JobId = findViewById(R.id.paymentfare_jobId_);
        hideamountded = findViewById(R.id.hideamountded);
        Tvpaymentfare_job_description = findViewById(R.id.Tvpaymentfare_job_description);
        ammoutde = findViewById(R.id.ammoutde);
        TextViewChangeOrderHint = findViewById(R.id.TextViewChangeOrderHint);
//        Tvpaymentfare_job_description.setPaintFlags(Tvpaymentfare_job_description.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        Bt_Request_payment = findViewById(R.id.Bt_faresummery_requestpaymet);
        Bt_Receivecash = findViewById(R.id.Bt_faresummery_receivecash);
        Rl_layout_back = findViewById(R.id.layout_jobfare_back);
        textView_payment_fare_cost = findViewById(R.id.textView_payment_fare_cost);

        Bt_Request_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isInternetPresent) {
                    approval_status = 1;

                    accepthit(MoreInfoPage.this, ServiceConstant.CLIENTAPPROVE, 1);

                } else {
                    final internetmissing mDialog = new internetmissing(MoreInfoPage.this);
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            }
                    );

                    mDialog.show();
                }
            }
        });

        Bt_Receivecash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isInternetPresent) {
                    approval_status = 1;
                    accepthit(MoreInfoPage.this, ServiceConstant.CLIENTAPPROVE, 2);

                } else {
                    final internetmissing mDialog = new internetmissing(MoreInfoPage.this);
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            }
                    );

                    mDialog.show();
                }
            }
        });


        if (isInternetPresent) {
            paymentPost(MoreInfoPage.this, ServiceConstant.CHANGEJOB_URL);

        } else {
            final internetmissing mDialog = new internetmissing(MoreInfoPage.this);
            mDialog.setPositiveButton(
                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );

            mDialog.show();
        }

    }


    //----------------------Post method for Payment Fare------------
    private void paymentPost(Context mContext, String url) {


        myDialog = new ProgressDialogcreated(MoreInfoPage.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();

        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("job_id", Job_id);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "", Str_jobDescription = "", Str_NeedPayment = "", misc_approval_status = "", Str_Currency = "", Str_BtnGroup = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject object = jobject.getJSONObject("response");
                        JSONObject object2 = object.getJSONObject("job");
                        Str_jobDescription = object2.getString("job_summary");
                        Str_NeedPayment = object2.getString("need_payment");
                        Str_Currency = object2.getString("currency");

                        if (object2.has("amount_to_reduce")) {

                            if (object2.isNull("amount_to_reduce")) {

                            } else {
                                String amount_to_reduce = object2.getString("amount_to_reduce");
                                JSONObject amount_to_reduceobj = new JSONObject(amount_to_reduce);
                                deductedamountfromserver = amount_to_reduceobj.getString("price");

                                source = amount_to_reduceobj.getString("source");

                                if (amount_to_reduceobj.has("description")) {
                                    deductionde = amount_to_reduceobj.getString("description");
                                    if (deductionde.equals("0") || deductionde.equals("")) {

                                    } else {
                                        amountdedreason.setText("Deduction reason:" + amount_to_reduceobj.getString("description"));
                                    }

                                } else {

                                }

                            }


                        }


                        ammoutde.setText("-" + mySession.getCurrencySymbol() + deductedamountfromserver);


                        misc_approval_status = object2.getString("misc_approval_status");

                        // Currency currencycode = Currency.getInstance(getLocale(Str_Currency));

                        String currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);

                        Str_BtnGroup = object2.getString("btn_group");

                        JSONArray jarry = object2.getJSONArray("billing");

                        miscall = object.getJSONArray("miscArray");
                        if (miscall.length() > 0) {
                            for (int kk = 0; kk < miscall.length(); kk++)
                            {
                                JSONObject miscallobj = miscall.getJSONObject(kk);
                                String status = miscallobj.getString("status");
                                if(status.equals("0"))
                                {
                                    Bt_Request_payment.setVisibility(View.VISIBLE);
                                }
                            }

                        }
                        if (jarry.length() > 0) {

                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);
                                newpojoforchangeorder pojo = new newpojoforchangeorder();

                                String title = jobjects_amount.getString("title");
                                String amount = jobjects_amount.getString("amount");


                                if (amount.equalsIgnoreCase("null") || amount.equalsIgnoreCase(null)) {
                                } else {

                                    if (jobjects_amount.has("type")) {
                                        pojo.setEstimatefrom("1");
                                        if (jobjects_amount.getString("status").equals("0")) {
                                            pojo.setAccept("1");
                                            boolean correct = amount.matches("[-+]?[0-9]*\\.?[0-9]+");
                                            if (correct == true) {
                                                Double comingvalue = Double.parseDouble(amount);
                                                addingingvalue = addingingvalue + comingvalue;

                                            }

                                        } else {
                                            if (jobjects_amount.getString("status").equals("1")) {
                                                boolean correct = amount.matches("[-+]?[0-9]*\\.?[0-9]+");
                                                if (correct == true) {
                                                    Double comingvalue = Double.parseDouble(amount);
                                                    addingingvalue = addingingvalue + comingvalue;
                                                }
                                            }
                                            pojo.setAccept(jobjects_amount.getString("status"));
                                        }

                                    } else {

                                        if (jobjects_amount.getString("title").equalsIgnoreCase("Total amount")) {
                                            boolean correct = amount.matches("[-+]?[0-9]*\\.?[0-9]+");
                                            if (correct == true) {
                                                Double comingvalue = Double.parseDouble(amount);
                                                addingingvalue = addingingvalue + comingvalue;
                                            }
                                        }

                                        pojo.setEstimatefrom("0");
                                        pojo.setAccept("2");
                                    }

                                    if (jobjects_amount.getString("title").equalsIgnoreCase("Grand Total")) {

                                    } else {
                                        if (jobjects_amount.getString("title").equals("Total amount")) {
                                            textView_payment_fare_cost.setText(mySession.getCurrencySymbol() + jobjects_amount.getString("amount"));
                                        } else {
                                            pojo.setPayment_title(jobjects_amount.getString("title"));
                                            pojo.setPaymentHideStatus(jobjects_amount.getString("status"));
                                            if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                                pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                            } else {
                                                pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                            }

                                            if (jobjects_amount.has("description")) {
                                                if (jobjects_amount.getString("description").length() < 2) {
                                                    pojo.setPaymentdesc("");
                                                } else {
                                                    pojo.setPaymentdesc(jobjects_amount.getString("description"));
                                                }
                                            } else {
                                                pojo.setPaymentdesc("");
                                            }
                                            farelist.add(pojo);
                                        }
                                    }
                                }
                            }
                            show_progress_status = true;
                        } else {
                            show_progress_status = false;
                        }


                    } else {
                        Str_response = jobject.getString("response");
                    }

                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("1")) {


                    Double subtractd = Double.parseDouble(deductedamountfromserver);

                    DecimalFormat df = new DecimalFormat("#.##");
                    addingingvalue = Double.valueOf(df.format(addingingvalue));

                    if (subtractd == 0.0 || subtractd == 0) {
                        hideamountded.setVisibility(View.GONE);
                    } else {
                        hideamountded.setVisibility(View.VISIBLE);
                    }


                    Double dedam = addingingvalue - subtractd;
                    amounded.setText(mySession.getCurrencySymbol() + dedam);
                    if (amounded.getText().toString().endsWith(".0") || amounded.getText().toString().endsWith(".5") || amounded.getText().toString().endsWith(".1") || amounded.getText().toString().endsWith(".2") || amounded.getText().toString().endsWith(".3") || amounded.getText().toString().endsWith(".4") || amounded.getText().toString().endsWith(".6") || amounded.getText().toString().endsWith(".7") || amounded.getText().toString().endsWith(".8") || amounded.getText().toString().endsWith(".9")) {
                        amounded.setText(mySession.getCurrencySymbol() + dedam + "0");
                    }

                    grandt.setText(mySession.getCurrencySymbol() + "" + addingingvalue);
                    if (grandt.getText().toString().endsWith(".0") || grandt.getText().toString().endsWith(".5") || grandt.getText().toString().endsWith(".1") || grandt.getText().toString().endsWith(".2") || grandt.getText().toString().endsWith(".3") || grandt.getText().toString().endsWith(".4") || grandt.getText().toString().endsWith(".6") || grandt.getText().toString().endsWith(".7") || grandt.getText().toString().endsWith(".8") || grandt.getText().toString().endsWith(".9")) {
                        grandt.setText(mySession.getCurrencySymbol() + addingingvalue + "0");
                    }

                    System.out.println();
                    Tvpaymentfare_jobId.setVisibility(View.VISIBLE);

                    Tv_JobId.setText(Job_id);
                    System.out.println();


                    if (Str_BtnGroup.equalsIgnoreCase("6")) {
                        //completed
                        Bt_Request_payment.setVisibility(View.GONE);
                        TextViewChangeOrderHint.setVisibility(View.GONE);
                        Bt_Receivecash.setVisibility(View.GONE);
                    } else {
                        if (misc_approval_status.equalsIgnoreCase("0")) {
                            hintshow.setVisibility(View.VISIBLE);
                            hintshow.setText(getResources().getString(R.string.class_moreinfo_waiting));
                           // Bt_Request_payment.setVisibility(View.VISIBLE);
                            TextViewChangeOrderHint.setVisibility(View.VISIBLE);
                            layout_fare_summery2.setVisibility(View.VISIBLE);
                            //    Bt_Receivecash.setVisibility(View.VISIBLE);
                            changeorderint = "0";
                        } else if (misc_approval_status.equalsIgnoreCase("1")) {
                            hintshow.setVisibility(View.VISIBLE);
                            hintshow.setText(getResources().getString(R.string.class_moreinfo_accepted));
                          //  Bt_Request_payment.setVisibility(View.GONE);
                            TextViewChangeOrderHint.setVisibility(View.GONE);
                            Bt_Receivecash.setVisibility(View.GONE);
                            changeorderint = "1";
                            layout_fare_summery2.setVisibility(View.VISIBLE);
                        } else if (misc_approval_status.equalsIgnoreCase("2")) {
                            hintshow.setVisibility(View.VISIBLE);
                            hintshow.setText(getResources().getString(R.string.class_moreinfo_rejected));
                         //   Bt_Request_payment.setVisibility(View.GONE);
                            TextViewChangeOrderHint.setVisibility(View.GONE);
                            Bt_Receivecash.setVisibility(View.GONE);
                            changeorderint = "1";
                            layout_fare_summery2.setVisibility(View.VISIBLE);
                        }
                    }

                    fare_listview.setEnabled(false);
                    adapter = new PaymentFareSummeryAdapter(MoreInfoPage.this, R.layout.newcheckbox, farelist);
                    fare_listview.setAdapter(adapter);

//                    if (Str_NeedPayment.equalsIgnoreCase("1")) {
//                        Rl_layout_farepayment_methods.setVisibility(View.VISIBLE);
//                    } else {
//                        Rl_layout_farepayment_methods.setVisibility(View.GONE);
//                    }


                } else {
                    HNDHelper.showResponseErrorAlert(MoreInfoPage.this, Str_response);

                }

                myDialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                myDialog.dismiss();
            }
        });
    }

    private void accepthit(Context mContext, String url, final int sttaus) {


        myDialog = new ProgressDialogcreated(MoreInfoPage.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();

        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("job_id", Job_id);


        jsonParams.put("amount_to_reduce[price]", deductedamountfromserver);
        jsonParams.put("amount_to_reduce[source]", source);
        jsonParams.put("amount_to_reduce[status]", "1");
        jsonParams.put("amount_to_reduce[description]", deductionde);


        int jo = 0;
        for (int jk = 0; jk < farelist.size(); jk++) {
            String accept = "" + farelist.get(jk).getAccept();
            String estimate = "" + farelist.get(jk).getEstimatefrom();
            if (estimate.equalsIgnoreCase("1") && accept.equalsIgnoreCase("1")) {
                jo = 1;
            }
        }


        if (jo == 1) {
            jsonParams.put("approval_status", "1");
        } else {
            jsonParams.put("approval_status", "2");
        }


        for (int jk = 0; jk < farelist.size(); jk++) {
            String accept = "" + farelist.get(jk).getAccept();
            String estimate = "" + farelist.get(jk).getEstimatefrom();
            String title = "" + farelist.get(jk).getPayment_title();
            String amounst = "" + farelist.get(jk).getPayment_amount();
            String desc = "" + farelist.get(jk).getPaymentdesc();

            if (estimate.equalsIgnoreCase("1")) {
                jsonParams.put("misc[" + jk + "][title]", title);
                jsonParams.put("misc[" + jk + "][amount]", amounst);
                jsonParams.put("misc[" + jk + "][description]", desc);
                if (accept.equals("1")) {
                    jsonParams.put("misc[" + jk + "][status]", "1");
                } else {
                    jsonParams.put("misc[" + jk + "][status]", "2");
                }

            }

        }


        try {
            if (miscall.length() > 0) {
                for (int i = 0; i < miscall.length(); i++) {
                    JSONObject jobjects_amount = miscall.getJSONObject(i);
                    jsonParams.put("miscArray[" + i + "][price]", jobjects_amount.getString("price"));
                    jsonParams.put("miscArray[" + i + "][description]", jobjects_amount.getString("description"));
                    jsonParams.put("miscArray[" + i + "][name]", jobjects_amount.getString("name"));
                    jsonParams.put("miscArray[" + i + "][source]", jobjects_amount.getString("source"));
                    jsonParams.put("miscArray[" + i + "][status]", jobjects_amount.getString("status"));

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "", Str_jobDescription = "", Str_NeedPayment = "", Str_Currency = "", Str_BtnGroup = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {

                        Str_response = jobject.getString("response");
                        if (sttaus == 1) {
                            changeorderint = "1";
                            hintshow.setVisibility(View.VISIBLE);
                            hintshow.setText(getResources().getString(R.string.approvalsenttotasker));
                            Bt_Request_payment.setVisibility(View.GONE);
                            TextViewChangeOrderHint.setVisibility(View.GONE);
                            Bt_Receivecash.setVisibility(View.GONE);
                            final PkDialog mDialog = new PkDialog(MoreInfoPage.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                            mDialog.setChangeOrderDialog();
                            mDialog.setPositiveButton(
                                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent InfoIntent = new Intent(MoreInfoPage.this, OrderDetailActivity.class);
                                            startActivity(InfoIntent);
                                            finish();
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                            mDialog.dismiss();
                                        }
                                    }
                            );

                            mDialog.show();

                        } else {
                            changeorderint = "0";
                            hintshow.setVisibility(View.VISIBLE);
                            hintshow.setText(getResources().getString(R.string.approvalsenttotasker));
                            Bt_Request_payment.setVisibility(View.VISIBLE);
                            TextViewChangeOrderHint.setVisibility(View.VISIBLE);
                            Bt_Receivecash.setVisibility(View.GONE);
                            HNDHelper.showResponseTitle("Success", MoreInfoPage.this, Str_response);
                        }


                    } else {
                        Str_response = jobject.getString("response");
                        HNDHelper.showResponseTitle("Sorry", MoreInfoPage.this, Str_response);
                    }


                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }


                myDialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                myDialog.dismiss();
            }
        });
    }

    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {

        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // your code
            Intent InfoIntent = new Intent(MoreInfoPage.this, OrderDetailActivity.class);
            startActivity(InfoIntent);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    public class PaymentFareSummeryAdapter extends ArrayAdapter<newpojoforchangeorder> {

        private ArrayList<newpojoforchangeorder> data;
        private LayoutInflater mInflater;
        private Context context;
        private int layoutResourceId;
        private String check;
        Typeface tf;


        PaymentFareSummeryAdapter(Context context, int layoutResourceId, ArrayList<newpojoforchangeorder> items) {
            super(context, layoutResourceId, items);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = items;
            tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Poppins-Regular.ttf");

        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            PaymentFareSummeryAdapter.Holder holder = null;
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new PaymentFareSummeryAdapter.Holder();
            holder.bean = data.get(position);
            holder.description = row.findViewById(R.id.description);
            holder.Tv_payment_title = row.findViewById(R.id.payment_fare_title_textView);
            holder.Tv_payment_amount = row.findViewById(R.id.payment_fare_cost_textView);
            holder.deletecraftman = row.findViewById(R.id.deletecraftman);

            holder.headingdesc = row.findViewById(R.id.headingdesc);

            row.setTag(holder);
            setupItem(holder, position);


            return row;
        }

        private void setupItem(final PaymentFareSummeryAdapter.Holder holder, final int position) {

            if (changeorderint.equalsIgnoreCase("1")) {
                holder.deletecraftman.setEnabled(false);
            } else {
                holder.deletecraftman.setEnabled(true);
            }
            if (data.get(position).getPaymentHideStatus().equals("1")) {
                holder.deletecraftman.setChecked(true);
                holder.deletecraftman.setEnabled(false);
                holder.deletecraftman.setVisibility(View.VISIBLE);
            }
            else if (data.get(position).getPaymentHideStatus().equals("2")) {
                holder.deletecraftman.setVisibility(View.INVISIBLE);
            }
            else if (data.get(position).getPaymentHideStatus().equals("0")) {
                holder.deletecraftman.setChecked(false);
                holder.deletecraftman.setEnabled(true);
                holder.deletecraftman.setClickable(true);
                holder.deletecraftman.setVisibility(View.VISIBLE);
            } else {
                holder.deletecraftman.setChecked(false);
                holder.deletecraftman.setEnabled(true);
                holder.deletecraftman.setClickable(true);
                holder.deletecraftman.setVisibility(View.VISIBLE);
            }

            holder.deletecraftman.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (((CheckBox) v).isChecked()) {
                        // perform logic

                        holder.bean.setAccept("1");
                        boolean correct = holder.bean.getPayment_amount().matches("[-+]?[0-9]*\\.?[0-9]+");
                        if (correct == true) {
                            Double comingvalue = Double.parseDouble(holder.bean.getPayment_amount());
                            Double minusvalue = addingingvalue + comingvalue;
                            addingingvalue = minusvalue;

                            DecimalFormat df = new DecimalFormat("#.##");
                            minusvalue = Double.valueOf(df.format(minusvalue));

                            Double detamt = Double.parseDouble(deductedamountfromserver);
                            Double dedam = minusvalue - detamt;
                            amounded.setText(mySession.getCurrencySymbol() + dedam);

                            if (amounded.getText().toString().endsWith(".0") || amounded.getText().toString().endsWith(".5") || amounded.getText().toString().endsWith(".1") || amounded.getText().toString().endsWith(".2") || amounded.getText().toString().endsWith(".3") || amounded.getText().toString().endsWith(".4") || amounded.getText().toString().endsWith(".6") || amounded.getText().toString().endsWith(".7") || amounded.getText().toString().endsWith(".8") || amounded.getText().toString().endsWith(".9")) {
                                amounded.setText(mySession.getCurrencySymbol() + dedam + "0");
                            }


                            grandt.setText(mySession.getCurrencySymbol() + minusvalue);
                            if (grandt.getText().toString().endsWith(".0") || grandt.getText().toString().endsWith(".5") || grandt.getText().toString().endsWith(".1") || grandt.getText().toString().endsWith(".2") || grandt.getText().toString().endsWith(".3") || grandt.getText().toString().endsWith(".4") || grandt.getText().toString().endsWith(".6") || grandt.getText().toString().endsWith(".7") || grandt.getText().toString().endsWith(".8") || grandt.getText().toString().endsWith(".9")) {
                                grandt.setText(mySession.getCurrencySymbol() + minusvalue + "0");
                            }
                        }
                    } else {

                        holder.bean.setAccept("2");
                        boolean correct = holder.bean.getPayment_amount().matches("[-+]?[0-9]*\\.?[0-9]+");
                        if (correct == true) {
                            Double comingvalue = Double.parseDouble(holder.bean.getPayment_amount());
                            Double minusvalue = addingingvalue - comingvalue;
                            addingingvalue = minusvalue;

                            DecimalFormat df = new DecimalFormat("#.##");
                            minusvalue = Double.valueOf(df.format(minusvalue));

                            Double detamt = Double.parseDouble(deductedamountfromserver);
                            Double dedam = minusvalue - detamt;
                            amounded.setText(mySession.getCurrencySymbol() + dedam);

                            if (amounded.getText().toString().endsWith(".0") || amounded.getText().toString().endsWith(".5") || amounded.getText().toString().endsWith(".1") || amounded.getText().toString().endsWith(".2") || amounded.getText().toString().endsWith(".3") || amounded.getText().toString().endsWith(".4") || amounded.getText().toString().endsWith(".6") || amounded.getText().toString().endsWith(".7") || amounded.getText().toString().endsWith(".8") || amounded.getText().toString().endsWith(".9")) {
                                amounded.setText(mySession.getCurrencySymbol() + dedam + "0");
                            }


                            grandt.setText(mySession.getCurrencySymbol() + minusvalue);
                            if (grandt.getText().toString().endsWith(".0") || grandt.getText().toString().endsWith(".5") || grandt.getText().toString().endsWith(".1") || grandt.getText().toString().endsWith(".2") || grandt.getText().toString().endsWith(".3") || grandt.getText().toString().endsWith(".4") || grandt.getText().toString().endsWith(".6") || grandt.getText().toString().endsWith(".7") || grandt.getText().toString().endsWith(".8") || grandt.getText().toString().endsWith(".9")) {
                                grandt.setText(mySession.getCurrencySymbol() + minusvalue + "0");
                            }

                        }
                    }

                    notifyDataSetChanged();
                }
            });


            holder.Tv_payment_title.setText(holder.bean.getPayment_title());


            holder.Tv_payment_amount.setText(mySession.getCurrencySymbol() + holder.bean.getPayment_amount());


            if (holder.bean.getEstimatefrom().equals("1")) {
                Log.e("accpet",holder.bean.getAccept());
                if (holder.bean.getAccept().equals("1")) {
                    holder.deletecraftman.setChecked(true);
                } else if (holder.bean.getAccept().equals("0")) {
                    holder.deletecraftman.setChecked(false);
                } else {
                    holder.deletecraftman.setChecked(false);

                }
                holder.deletecraftman.setVisibility(View.VISIBLE);
            } else {
                holder.deletecraftman.setVisibility(View.INVISIBLE);
            }

            if (holder.bean.getPaymentdesc() == null || holder.bean.getPaymentdesc().equals("") || holder.bean.getPaymentdesc().equals(" ")) {
                holder.description.setVisibility(View.GONE);
                holder.headingdesc.setVisibility(View.GONE);
            } else {
                holder.headingdesc.setVisibility(View.VISIBLE);
                holder.description.setVisibility(View.VISIBLE);

                String first = holder.bean.getPaymentdesc();
                holder.description.setText(first);

            }

        }


        public class Holder {
            private TextView Tv_payment_title, Tv_payment_amount, description, headingdesc;
            CheckBox deletecraftman;
            newpojoforchangeorder bean;
        }


    }


}
