package com.handypro.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.handypro.Dialog.PkDialog;
import com.handypro.Dialog.internetmissing;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.UserInfoPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;
import com.handypro.textview.TypefaceSpan;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.CurrencySymbolConverter;
import com.handypro.volley.ServiceRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by CAS63 on 2/16/2018.
 */

public class OTPScreenActivity extends AppCompatActivity {
    private String Str_username = "", user_first_name = "", user_last_name = "", str_marketing = "", Str_email = "", Str_password = "",
            Str_phone = "", Str_countryCode = "", Str_referralCode = "", Str_gcmId = "", Str_otp_Status = "", Str_otp = "",Str_appartmentnumber;
    private CustomEdittext myOTPET;
    private RelativeLayout myBackLAY;
    private CustomButton mySubmitBTN;
    private ConnectionDetector myConnectionManager;
    private SharedPreference mySession;
    private ProgressDialogcreated myDialog;
    Typeface tf;
    Animation shake;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInitialize();
        getIntentValues();
        clickListeners();
    }

    private void clickListeners() {
        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(myBackLAY.getWindowToken(), 0);

                finish();
            }
        });

        mySubmitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myOTPET.getText().toString().length() == 0) {

                    myOTPET.startAnimation(shake);
                    SpannableString s = new SpannableString(getResources().getString(R.string.otp_label_alert_otp));
                    s.setSpan(new TypefaceSpan(tf), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    myOTPET.setError(s);

                } else if (!Str_otp.equals(myOTPET.getText().toString())) {

                    final PkDialog mDialog = new PkDialog(OTPScreenActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.class_signup_error));
                    mDialog.setDialogMessage(getResources().getString(R.string.otp_label_alert_invalid));
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            }
                    );

                    mDialog.show();

                } else {
                    submitValues();
                }
            }
        });
    }

    private void submitValues() {
        if (myConnectionManager.isConnectingToInternet()) {
            postRegisterRequest(OTPScreenActivity.this);

        } else {

            final internetmissing mDialog = new internetmissing(OTPScreenActivity.this);
            mDialog.setPositiveButton(
                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );

            mDialog.show();
        }
    }

    private void postRegisterRequest(Context aContext) {
        myDialog = new ProgressDialogcreated(OTPScreenActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email", Str_email);
        jsonParams.put("password", Str_password);
        jsonParams.put("user_name", Str_username);
        jsonParams.put("user_first_name", user_first_name);
        jsonParams.put("user_last_name", user_last_name);
        jsonParams.put("country_code", Str_countryCode);
        jsonParams.put("phone_number", Str_phone);
        jsonParams.put("unique_code", Str_referralCode);
        jsonParams.put("gcm_id", Str_gcmId);
        jsonParams.put("marketing", str_marketing);


        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
        String line1 = getcard.getString("line1", "");
        String line2 = getcard.getString("line2", "");
        String city = getcard.getString("city", "");
        String state = getcard.getString("state", "");
        String zipcode = getcard.getString("zipcode", "");
        String country = getcard.getString("country", "");
        String formatted_address = getcard.getString("formatted_address", "");

        String Selected_Latitude = getcard.getString("Selected_Latitude", "");
        String Selected_Longitude = getcard.getString("Selected_Longitude", "");


        jsonParams.put("address[" + "line1" + "]", line1);
        jsonParams.put("address[" + "line2" + "]", line2);
        jsonParams.put("address[" + "city" + "]", city);
        jsonParams.put("address[" + "state" + "]", state);
        jsonParams.put("address[" + "zipcode" + "]", zipcode);
        jsonParams.put("address[" + "country" + "]", country);
        jsonParams.put("address[" + "formatted_address" + "]", formatted_address);
        jsonParams.put("address[" + "unit_number" + "]", Str_appartmentnumber);

        jsonParams.put("location[" + "lat" + "]", Selected_Latitude);
        jsonParams.put("location[" + "lng" + "]", Selected_Longitude);

        jsonParams.put("zipcode", zipcode);

        Log.e("Res", jsonParams.toString());

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(ServiceConstant.OtpUrl, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------otp response------------" + response);

                String Str_status = "", Str_message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {


                        UserInfoPojo aUInfoPojo = new UserInfoPojo();

                        aUInfoPojo.setUserName(object.getString("user_name"));
                        aUInfoPojo.setUserId(object.getString("user_id"));
                        aUInfoPojo.setUserEmail(object.getString("email"));
                        aUInfoPojo.setUserProfileImage(object.getString("user_image"));
                        aUInfoPojo.setUserCountryCode(object.getString("country_code"));
                        aUInfoPojo.setUserPhoneNumber(object.getString("phone_number"));
                        aUInfoPojo.setUserWalletAmount(object.getString("wallet_amount"));
                        aUInfoPojo.setSocketKey(object.getString("soc_key"));
                        aUInfoPojo.setUserCurrencyCode(object.getString("currency"));
                        aUInfoPojo.setUserReferalCode(object.getString("referal_code"));
                        aUInfoPojo.setUserCategoryId(object.getString("category"));

                        mySession.putUserDetails(aUInfoPojo);
                        mySession.setLoginStatus(true);
                        mySession.putCurrencySymbol(CurrencySymbolConverter.getCurrencySymbol(object.getString("currency")));


                        SharedPreferences prefd = getApplicationContext().getSharedPreferences("logintoconform", 0);
                        String wheretogo = prefd.getString("lo", "");
                        if (wheretogo.equalsIgnoreCase("1")) {
                           /* Intent intent = new Intent(getApplicationContext(), BookingConfirmationActivity.class);
                            startActivity(intent);*/
                            finish();
                        } else {
                            /*Intent intent = new Intent(OTPScreenActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                            SignupActivity.SignUp.finish();
                            LoginActivity.logIn.finish();*/

                            finish();
                        }


                    }

                    if (Str_status.equalsIgnoreCase("3")) {
                        if (myDialog.isShowing()) {
                            myDialog.dismiss();
                        }
                        submitValues();
                    }

                    if (Str_status.equalsIgnoreCase("0")) {

                        Str_message = object.getString("message");

                        final PkDialog mDialog = new PkDialog(OTPScreenActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.class_signup_error));
                        mDialog.setDialogMessage(Str_message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                    }
                                }
                        );

                        mDialog.show();


                    }
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    private void getIntentValues() {
        Intent intent = getIntent();
        str_marketing = intent.getStringExtra("marketing");
        Str_username = intent.getStringExtra("UserName");
        user_first_name = intent.getStringExtra("user_first_name");
        user_last_name = intent.getStringExtra("user_last_name");
        Str_email = intent.getStringExtra("Email");
        Str_password = intent.getStringExtra("Password");
        Str_phone = intent.getStringExtra("Phone");
        Str_countryCode = intent.getStringExtra("CountryCode");
        Str_referralCode = intent.getStringExtra("ReferralCode");
        Str_gcmId = intent.getStringExtra("GcmID");
        Str_otp_Status = intent.getStringExtra("Otp_Status");
        Str_otp = intent.getStringExtra("Otp");


        Str_appartmentnumber = intent.getStringExtra("appartmentnumber");


       if(Str_appartmentnumber.isEmpty()){
           Str_appartmentnumber="";
       }

       else {
           Str_appartmentnumber = intent.getStringExtra("appartmentnumber");
           mySession.setAppartmentnumber(Str_appartmentnumber);
       }

        if (Str_otp_Status.equalsIgnoreCase("development")) {
            myOTPET.setText(Str_otp);
        } else {
            myOTPET.setText("");
        }

        shake = AnimationUtils.loadAnimation(OTPScreenActivity.this,
                R.anim.shake);
        tf = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Medium.ttf");
    }

    private void classAndWidgetInitialize() {
        myConnectionManager = new ConnectionDetector(OTPScreenActivity.this);
        mySession = new SharedPreference(OTPScreenActivity.this);

        myOTPET = (CustomEdittext) findViewById(R.id.activity_otp_verification_entered_otp_ET);
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_otp_verification_LAY_back);
        mySubmitBTN = (CustomButton) findViewById(R.id.activity_otp_verification_BTN_send);

    }

}
