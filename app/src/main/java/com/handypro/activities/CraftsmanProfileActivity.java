package com.handypro.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.TaskerCategoryPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.adapters.TaskerCategoryAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by CAS63 on 3/15/2018.
 */

public class CraftsmanProfileActivity extends AppCompatActivity {
    final int PERMISSION_REQUEST_CODES = 222;
    final int PERMISSION_REQUEST_CODE = 111;
    private CustomTextView myTaskerUnameTXT, myTaskerEmailTXT, myTaskerPhnoTXT, myRadiusTXT, myHourlyCostTXT, myMiniCostTXT, myAddressTXT, myWrkLocationTXT;
    private CircleImageView myTaskerProIMG;
    private RecyclerView myCategoryRecyclerView;
    private LinearLayout myCallBTN;
    private ArrayList<TaskerCategoryPojo> myTskrCatList;
    private TaskerCategoryAdapter myTskrCatgryAdapter;
    private SharedPreference mySession;
    private ConnectionDetector myConnectionManager;
    private ProgressDialogcreated myDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_tasker_profile);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        myTskrCatList = new ArrayList<>();
        myTaskerUnameTXT = findViewById(R.id.fragment_tasker_profile_username_TXT);
        myTaskerEmailTXT = findViewById(R.id.fragment_tasker_profile_email_TXT);
        myTaskerPhnoTXT = findViewById(R.id.fragment_tasker_profile_phone_number_TXT);
        myRadiusTXT = findViewById(R.id.fragment_tasker_profile_radius_TXT);
        myHourlyCostTXT = findViewById(R.id.fragment_tasker_profile_hourly_cost_TXT);
        myMiniCostTXT = findViewById(R.id.fragment_tasker_profile_min_cost_TXT);
        myAddressTXT = findViewById(R.id.fragment_tasker_profile_address_TXT);
        myWrkLocationTXT = findViewById(R.id.fragment_tasker_profile_work_location_TXT);
        myTaskerProIMG = findViewById(R.id.fragment_tasker_profile_profile_imageView);

        myCallBTN = findViewById(R.id.fragment_tasker_profile_BTN_call);

        myCallBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (myTaskerPhnoTXT.getText().toString().length() > 2) {

                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + myTaskerPhnoTXT.getText().toString()));
                    startActivity(callIntent);
                } else {
                    HNDHelper.showErrorAlert(CraftsmanProfileActivity.this, getResources().getString(R.string.activity_my_orders_error_in_contact_provider));
                }
            }
        });
        myCategoryRecyclerView = findViewById(R.id.fragment_tasker_profile_category_recyclerview);

        mySession = new SharedPreference(CraftsmanProfileActivity.this);
        myConnectionManager = new ConnectionDetector(CraftsmanProfileActivity.this);
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestMyOrderList();
            postRequestMyReviewsList();
        } else {
            HNDHelper.showErrorAlert(CraftsmanProfileActivity.this, getResources().getString(R.string.nointernet_text));
        }


    }


    private void setDataInAdapter() {

        myTskrCatgryAdapter = new TaskerCategoryAdapter(CraftsmanProfileActivity.this, myTskrCatList);
        myCategoryRecyclerView.setAdapter(myTskrCatgryAdapter);

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(CraftsmanProfileActivity.this, LinearLayoutManager.HORIZONTAL, false);
        myCategoryRecyclerView.setLayoutManager(horizontalLayoutManagaer);

    }


    private void postRequestMyOrderList() {
        myDialog = new ProgressDialogcreated(CraftsmanProfileActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        SharedPreferences pref = CraftsmanProfileActivity.this.getSharedPreferences("sendtaskerid", 0);
        String provider_id = pref.getString("taskerid", "");
        jsonParams.put("provider_id", provider_id);


        ServiceRequest mRequest = new ServiceRequest(CraftsmanProfileActivity.this);
        mRequest.makeServiceRequest(ServiceConstant.PROFILEINFO_URL, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------full profile----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        JSONObject response_Object = object.getJSONObject("response");


                        String provider_name = response_Object.getString("provider_name");
                        String email = response_Object.getString("email");
                        String mobile_number = response_Object.getString("mobile_number");
                        String image = response_Object.getString("image");
                        if (image.contains("http:") || image.contains("https:")) {

                        } else {
                            image = ServiceConstant.Base_Url + image;
                        }

                        String Working_location = response_Object.getString("Working_location");
                        String address = response_Object.getString("address");
                        String dial_code = response_Object.getString("dial_code");

                        myTaskerUnameTXT.setText(provider_name);
                        myTaskerEmailTXT.setText(email);

                        myTaskerPhnoTXT.setText(dial_code + mobile_number);

                        myAddressTXT.setText(address);
                        myWrkLocationTXT.setText(Working_location);


                        /*Picasso.with(CraftsmanProfileActivity.this).load(image)
                                .resize(500, 500)
                                .onlyScaleDown()
                                .placeholder(R.drawable.ic_no_user)
                                .into(myTaskerProIMG);*/

                        Picasso.get().load(image)
                                .resize(500, 500)
                                .onlyScaleDown()
                                .placeholder(R.drawable.ic_no_user).into(myTaskerProIMG);


                        JSONArray aMainArr = response_Object.getJSONArray("category_Details");
                        myTskrCatList.clear();
                        for (int a = 0; a < aMainArr.length(); a++) {

                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);

                            TaskerCategoryPojo aTskerCatPojo = new TaskerCategoryPojo();
                            aTskerCatPojo.setCategoryHourlyRate("");
                            aTskerCatPojo.setCategoryName(aSubCatObj.getString("categoryname"));
                            aTskerCatPojo.setCategoryImage(aSubCatObj.getString("image"));
                            myTskrCatList.add(aTskerCatPojo);


                        }
                        if (aMainArr.length() > 0) {
                            setDataInAdapter();
                        }


                    } else {
                        HNDHelper.showResponseErrorAlert(CraftsmanProfileActivity.this, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    private void postRequestMyReviewsList() {
        myDialog = new ProgressDialogcreated(CraftsmanProfileActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        SharedPreferences pref = CraftsmanProfileActivity.this.getSharedPreferences("sendtaskerid", 0);
        String provider_id = pref.getString("taskerid", "");
        jsonParams.put("provider_id", provider_id);


        ServiceRequest mRequest = new ServiceRequest(CraftsmanProfileActivity.this);
        mRequest.makeServiceRequest(ServiceConstant.GET_CRAFTSMAN_RATING, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------full profile----------------" + response);

                /*try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        JSONObject response_Object = object.getJSONObject("response");


                        String provider_name = response_Object.getString("provider_name");
                        String email = response_Object.getString("email");
                        String mobile_number = response_Object.getString("mobile_number");
                        String image = response_Object.getString("image");
                        if (image.contains("http:") || image.contains("https:")) {

                        } else {
                            image = ServiceConstant.Base_Url + image;
                        }

                        String Working_location = response_Object.getString("Working_location");
                        String address = response_Object.getString("address");
                        String dial_code = response_Object.getString("dial_code");

                        myTaskerUnameTXT.setText(provider_name);
                        myTaskerEmailTXT.setText(email);

                        myTaskerPhnoTXT.setText(dial_code + mobile_number);

                        myAddressTXT.setText(address);
                        myWrkLocationTXT.setText(Working_location);


                        Picasso.with(CraftsmanProfileActivity.this).load(image)
                                .resize(500, 500)
                                .onlyScaleDown()
                                .placeholder(R.drawable.ic_no_user)
                                .into(myTaskerProIMG);


                        JSONArray aMainArr = response_Object.getJSONArray("category_Details");
                        myTskrCatList.clear();
                        for (int a = 0; a < aMainArr.length(); a++) {

                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);

                            TaskerCategoryPojo aTskerCatPojo = new TaskerCategoryPojo();
                            aTskerCatPojo.setCategoryHourlyRate("");
                            aTskerCatPojo.setCategoryName(aSubCatObj.getString("categoryname"));
                            aTskerCatPojo.setCategoryImage(aSubCatObj.getString("image"));
                            myTskrCatList.add(aTskerCatPojo);


                        }
                        if (aMainArr.length() > 0) {
                            setDataInAdapter();
                        }


                    } else {
                        HNDHelper.showResponseErrorAlert(CraftsmanProfileActivity.this, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }*/
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(CraftsmanProfileActivity.this, Manifest.permission.CALL_PHONE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(CraftsmanProfileActivity.this, Manifest.permission.READ_PHONE_STATE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(CraftsmanProfileActivity.this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + myTaskerPhnoTXT.getText().toString()));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(callIntent);
                }
                break;


            case PERMISSION_REQUEST_CODES:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + myTaskerPhnoTXT.getText().toString()));
                    startActivity(callIntent);
                }
                break;

        }
    }

}
