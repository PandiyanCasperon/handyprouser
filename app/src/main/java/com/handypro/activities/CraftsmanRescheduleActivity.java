package com.handypro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.github.badoualy.datepicker.DatePickerTimeline;
import com.github.badoualy.datepicker.MonthView;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.AppointmentTimePojo;
import com.handypro.Pojo.CraftsManDetailPojo;
import com.handypro.Pojo.DataModel;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.Widgets.RecyclerItemClickListener;
import com.handypro.Widgets.undismisprogress;
import com.handypro.activities.navigationMenu.estimateacceptdb;
import com.handypro.adapters.AppointmentTimeAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.BoldCustomTextView;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by CAS63 on 2/21/2018.
 */

public class CraftsmanRescheduleActivity extends AppCompatActivity {


    String status = "0";
    int notscrolling = 0;
    private estimateacceptdb mHelper;
    private SQLiteDatabase dataBase;
    ArrayList<String> checktime = new ArrayList<String>();
    private SharedPreference mySession;


    int saveposva = 0;
    //    private MaterialCalendarView myCalendar;
    final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    final SimpleDateFormat formatter1 = new SimpleDateFormat("dd");
    final SimpleDateFormat formatter2 = new SimpleDateFormat("MM");
    private int myDateInt, myMonthInt, myCounterInt;
    private String myDateSTR = "", myLatitude = "", myLongitude = "", myZipCodeSTR = "", myMinuteStr = "", myApprxTimeSTR = "", mySelectedTimeSTR = "";
    private SelectCraftsmenAdapter craftmyAdapter;
    private ArrayList<CraftsManDetailPojo> myCraftsManArrList;
    private RecyclerView myTimeRecyclerView;
    private AppointmentTimeAdapter myAdapter;
    private LinearLayout myBackLAY;
    ArrayList<DataModel> dataModels;
    ProgressDialogcreated myDialog;

    LinearLayout activity_shedule_appointment_recyclerviewLAY;
    private int myIntTimePos = 0;
    public static boolean myCurrDateSelected = true;
    private String myBookingIDStr = "";
    TextView higlighttext;
    String totalday;
    int estimatecount = 0;
    int estimatehour = 0;
    private ArrayList<AppointmentTimePojo> myTimeArrList;
    private Boolean isInternetPresent = false;

    private ConnectionDetector cd;
    TextView donetoreschedule;
    ArrayList<String> taskname = new ArrayList<String>();
    ArrayList<String> taskhours = new ArrayList<String>();
    private listhei myCraftsMenDetailListView;
    JSONObject reshedulejsonobj;
    TextView activity_select_craft_men_TXT_title, whats, tellabout, whatsimportant, viewmoreinfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userreschedule);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        myCraftsManArrList = new ArrayList<CraftsManDetailPojo>();
        cd = new ConnectionDetector(CraftsmanRescheduleActivity.this);
        mHelper = new estimateacceptdb(this);
        mySession = new SharedPreference(CraftsmanRescheduleActivity.this);
        dataModels = new ArrayList<>();
        taskname.clear();
        taskhours.clear();
        initClassAndWidgets();

        clickListener();
        myCalInit();
        recyclerViewItemClick();


    }


    private void recyclerViewItemClick() {
        myTimeRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(CraftsmanRescheduleActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                AppointmentTimePojo aAppTimePojo = myAdapter.getItem(position);
                if (aAppTimePojo.getColourchange().equals("0")) {
                    HNDHelper.showErrorAlert(CraftsmanRescheduleActivity.this, getResources().getString(R.string.class_schedule_appoitment_selected));
                    mySelectedTimeSTR = "";
                    donetoreschedule.setEnabled(false);
                    donetoreschedule.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.darkcolor_1));

                } else {
                    if (aAppTimePojo.getSelected().equals("false")) {
                        checkOtherItems();
                        aAppTimePojo.setSelected("true");
                        mySelectedTimeSTR = aAppTimePojo.getsendtime();
                        donetoreschedule.setEnabled(true);
                        donetoreschedule.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
                    } else {
                        aAppTimePojo.setSelected("false");
                        mySelectedTimeSTR = "";
                        donetoreschedule.setEnabled(false);
                        donetoreschedule.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.darkcolor_1));
                    }
                }
                myAdapter.notifyDataSetChanged();
            }
        }));


    }

    private void checkOtherItems() {
        for (int a = 0; a < myAdapter.getAllTimeArr().size(); a++) {
            if (myAdapter.getAllTimeArr().get(a).getSelected().equals("true")) {
                myAdapter.getAllTimeArr().get(a).setSelected("false");
                System.out.println("----entered");
                break;
            }
        }
    }

    private void recyclerViewAdapter() {

        myAdapter = new AppointmentTimeAdapter(CraftsmanRescheduleActivity.this, myTimeArrList);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(CraftsmanRescheduleActivity.this, LinearLayoutManager.HORIZONTAL, false);
        myTimeRecyclerView.setLayoutManager(horizontalLayoutManagaer);
        myTimeRecyclerView.setAdapter(myAdapter);
        myTimeRecyclerView.addOnScrollListener(new CustomScrollListener());
        if (myTimeArrList.size() > 0) {
            myTimeRecyclerView.scrollToPosition(saveposva);
        }
    }

    private void myCalInit() {
//        myCalendar.setSelectedDate(new Date());
        myDateSTR = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
//        myCalendar.setDateSelected(new Date(), true);
//        myCalendar.state().edit()
//                .setMinimumDate(new Date())
//                .setFirstDayOfWeek(Calendar.MONDAY)
//                .commit();
    }


    private void setDataInAdapter() {
        myCraftsMenDetailListView.setEnabled(false);
        craftmyAdapter = new SelectCraftsmenAdapter(CraftsmanRescheduleActivity.this, myCraftsManArrList);
        myCraftsMenDetailListView.setAdapter(craftmyAdapter);
    }

    private void clickListener() {

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }


    private void AddItemsInTimeArr(String aSelectedSTR, String aTime, String sendtime, String colourchange) {

        AppointmentTimePojo aTimePojo = new AppointmentTimePojo();
        aTimePojo.setSelected(aSelectedSTR);
        aTimePojo.setAppointmentTime(aTime);
        aTimePojo.setsendtime(sendtime);
        aTimePojo.setEnabled("true");
        aTimePojo.setColourchange(colourchange);
        mySelectedTimeSTR = "";
        donetoreschedule.setEnabled(false);
        donetoreschedule.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.darkcolor_1));
        myTimeArrList.add(aTimePojo);
    }


    private void initClassAndWidgets() {


        final SharedPreferences pref = getApplicationContext().getSharedPreferences("newuserreschedule", MODE_PRIVATE);

        myTimeArrList = new ArrayList<AppointmentTimePojo>();
        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
        String estimatehours = Job_idpref.getString("estimatehours", "");
        higlighttext = (TextView) findViewById(R.id.activity_shedule_appointment_page_free_estimate_TXT);
        higlighttext.setText(getResources().getString(R.string.activity_schedule_appoitment_select_craft));
        activity_shedule_appointment_recyclerviewLAY = (LinearLayout) findViewById(R.id.activity_shedule_appointment_recyclerviewLAY);
        whats = (TextView) findViewById(R.id.whats);
        tellabout = (TextView) findViewById(R.id.tellabout);
        whatsimportant = (TextView) findViewById(R.id.whatsimportant);
        viewmoreinfo = (TextView) findViewById(R.id.viewmoreinfo);

        viewmoreinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences prefa = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefa.edit();
                editor.putString("jobid", pref.getString("jobid", ""));
                editor.apply();
                editor.commit();
                Intent InfoIntent = new Intent(CraftsmanRescheduleActivity.this, newmoreinfo.class);
                startActivity(InfoIntent);
            }
        });


        String jobinstruction = pref.getString("jobinstruction", "");
        String description = pref.getString("description", "");
        String servicetype = pref.getString("servicetype", "");

        whats.setText(servicetype);
        tellabout.setText("Instruction:" + jobinstruction);
        whatsimportant.setText("Description:" + description);


        myTimeRecyclerView = (RecyclerView) findViewById(R.id.activity_shedule_appointment_recyclerview);
        myBackLAY = (LinearLayout) findViewById(R.id.activity_shedule_appoinment_LAY_back);
        activity_select_craft_men_TXT_title = (TextView) findViewById(R.id.activity_select_craft_men_TXT_title);
        donetoreschedule = (TextView) findViewById(R.id.donetoreschedule);
        myCraftsMenDetailListView = (listhei) findViewById(R.id.activity_select_craft_men_listView);
        donetoreschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mySelectedTimeSTR.length() < 2) {
                    HNDHelper.showErrorAlert(CraftsmanRescheduleActivity.this, getResources().getString(R.string.bookingtime));

                } else if (totalday.length() < 2) {
                    HNDHelper.showErrorAlert(CraftsmanRescheduleActivity.this, getResources().getString(R.string.bookingdate));
                } else {

                    sendrequest(CraftsmanRescheduleActivity.this, ServiceConstant.checkcratfman);
                    //  HNDHelper.showErrorAlert(CraftsmanRescheduleActivity.this, getResources().getString(R.string.nointernet_text));
                }
            }
        });


        DatePickerTimeline timeline = (DatePickerTimeline) findViewById(R.id.timeline);
        timeline.setDateLabelAdapter(new MonthView.DateLabelAdapter() {
            @Override
            public CharSequence getLabel(Calendar calendar, int index) {
                return Integer.toString(calendar.get(Calendar.MONTH) + 1) + "/" + (calendar.get(Calendar.YEAR) % 2000);
            }
        });


        Calendar calendar = Calendar.getInstance();
        int thisYear = calendar.get(Calendar.YEAR);
        final int thisMonth = calendar.get(Calendar.MONTH);
        int thisDay = calendar.get(Calendar.DAY_OF_MONTH);


        String month = "" + (thisMonth + 1);
        String day = "" + thisDay;

        if ((thisMonth + 1) <= 9) {
            month = "0" + (thisMonth + 1);
        }
        if (thisDay <= 9) {
            day = "0" + thisDay;
        }

        totalday = month + "/" + day + "/" + thisYear;

        PostRequest();


        timeline.setFirstVisibleDate(thisYear, thisMonth, thisDay);
        timeline.setLastVisibleDate(thisYear, thisMonth + 2, thisDay);
        timeline.setOnDateSelectedListener(new DatePickerTimeline.OnDateSelectedListener() {
            @Override
            public void onDateSelected(int year, int month, int day, int index) {
                String monthss = "" + (month + 1);
                String dayss = "" + day;

                if ((month + 1) <= 9) {
                    monthss = "0" + (month + 1);
                }
                if (day <= 9) {
                    dayss = "0" + day;
                }

                totalday = monthss + "/" + dayss + "/" + year;
              /*  myTimeArrList.clear();
                checktime.clear();*/

                if (cd.isConnectingToInternet()) {
                    mySelectedTimeSTR = "";

                    PostRequest();
                } else {
                    HNDHelper.showErrorAlert(CraftsmanRescheduleActivity.this, getResources().getString(R.string.nointernet_text));
                }

              /*  Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                String currentdate = df.format(c);
                //   Toast.makeText(SheduleAppointmentActivity.this,currentdate,Toast.LENGTH_SHORT).show();
                if(totalday.equalsIgnoreCase(currentdate))
                {
                    currentdatetime();
                    //  Toast.makeText(SheduleAppointmentActivity.this,"current"+totalday,Toast.LENGTH_SHORT).show();
                }
                else
                {
                    someotherdate();
                    // Toast.makeText(SheduleAppointmentActivity.this,totalday,Toast.LENGTH_SHORT).show();
                }*/


            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent data = new Intent();
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, data);
        } else {
            getParent().setResult(Activity.RESULT_OK, data);
        }

        finish();
    }

























   /* private void paymentPost(Context mContext, String url) {

        myDialog = new ProgressDialogcreated(CraftsmanRescheduleActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();


        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("taskid", taskid);
        jsonParams.put("taskerid", taskerid);
        jsonParams.put("location", location);
        jsonParams.put("tasklat", tasklat);
        jsonParams.put("tasklng", tasklong);
        jsonParams.put("reshedule_count", reshedule_count);

        for(int i = 0; i<reshedulejsonobj.length(); i++)
        {
            try {
                jsonParams.put("reshedule["+reshedulejsonobj.names().getString(i)+"]", ""+reshedulejsonobj.get(reshedulejsonobj.names().getString(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // Log.e("Payment", "Key = " + reshedulejsonobj.names().getString(i) + " value = " + reshedulejsonobj.get(reshedulejsonobj.names().getString(i)));
        }



        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);


               try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {

                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


                    } else {


                        final PkDialog mDialog = new PkDialog(CraftsmanRescheduleActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.rating_header_sorry_textView));
                        mDialog.setDialogMessage(object.getString("response"));
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }





                 if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }*/


    private void sendrequest(final Context aContext, String url) {


        myDialog = new ProgressDialogcreated(CraftsmanRescheduleActivity.this);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> params = new HashMap<>();


        SharedPreferences pref = getApplicationContext().getSharedPreferences("newuserreschedule", MODE_PRIVATE);
        String code = pref.getString("jobid", "");

        String subcat = pref.getString("subcat", "");
        String latre = pref.getString("latre", "");
        String longre = pref.getString("longre", "");
        String zipcodere = pref.getString("zipcodere", "");


        params.put("zipcode", zipcodere);
        params.put("lat", latre);
        params.put("long", longre);


        if (subcat.contains(",")) {
            String[] subcatsplit = subcat.split(",");
            for (int j = 0; j < subcatsplit.length; j++) {
                params.put("subcategory[" + j + "]", subcatsplit[j]);
            }
        } else {
            params.put("subcategory[" + 0 + "]", subcat);
        }


        params.put("pickup_date", totalday);
        params.put("pickup_time", mySelectedTimeSTR);

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------get Confirm Category list Response----------------" + response);
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equalsIgnoreCase("1")) {


                        String inputPattern = "MM/dd/yyyy";
                        String outputPattern = "EEEE MMMM  dd, yyyy";
                        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                        Date date = null;
                        String str = null;

                        try {
                            date = inputFormat.parse(totalday);
                            str = outputFormat.format(date);

                            String s = mySelectedTimeSTR;
                            DateFormat f1 = new SimpleDateFormat("HH:mm"); //HH for hour of the day (0 - 23)
                            Date d = f1.parse(s);
                            DateFormat f2 = new SimpleDateFormat("hh:mm a");
                            f2.format(d).toLowerCase(); // "12:18am"
                            activity_select_craft_men_TXT_title.setText(getResources().getString(R.string.bookde) + "\n" + str + " " + f2.format(d).toLowerCase());
                            activity_select_craft_men_TXT_title.setVisibility(View.VISIBLE);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        JSONArray aMainArr = object.getJSONArray("taskers");
                        myCraftsMenDetailListView.setVisibility(View.VISIBLE);

                        myCraftsManArrList.clear();

                        for (int a = 0; a < aMainArr.length(); a++) {
                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);
                            CraftsManDetailPojo aCrftMnPojo = new CraftsManDetailPojo();
                            aCrftMnPojo.setCraftsManProfileName(aSubCatObj.getString("name"));
                            aCrftMnPojo.setCraftsManProfileImage(aSubCatObj.getString("avatar"));
                            aCrftMnPojo.setTaskerid(aSubCatObj.getString("_id"));
                            aCrftMnPojo.setCraftsManAddress(aSubCatObj.getString("about"));
                            aCrftMnPojo.setCraftsManJobRadius(aSubCatObj.getString("distance") + aSubCatObj.getString("distanceby"));
                            aCrftMnPojo.setCraftsManReviewAvrg(aSubCatObj.getString("avg_review"));
                            aCrftMnPojo.setCraftsManReviewCount(aSubCatObj.getString("total_review") + " REVIEWS");
                            aCrftMnPojo.setCraftsManYearOfExperience(aSubCatObj.getString("experience"));
                            myCraftsManArrList.add(aCrftMnPojo);

                        }

                        if (aMainArr.length() == 0) {

                            setDataInAdapter();

                            final PkDialog mDialog = new PkDialog(CraftsmanRescheduleActivity.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.rating_header_sorry_textView));
                            mDialog.setDialogMessage(getResources().getString(R.string.class_users_schedule_craftman));
                            mDialog.setPositiveButton(
                                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            mDialog.dismiss();
                                        }
                                    }
                            );
                            mDialog.show();
                        } else {
                            setDataInAdapter();
                        }

                    } else {
                        activity_select_craft_men_TXT_title.setVisibility(View.GONE);
                        myCraftsMenDetailListView.setVisibility(View.GONE);

                        myCraftsManArrList.clear();

                        String message = object.getString("response");
                        final PkDialog mDialog = new PkDialog(CraftsmanRescheduleActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.rating_header_sorry_textView));
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();
                    }
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block


                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }


    public class SelectCraftsmenAdapter extends BaseAdapter {
        private Context myContext;
        private ArrayList<CraftsManDetailPojo> myCraftMenList;
        private LayoutInflater myInflater;

        public SelectCraftsmenAdapter(Context myContext, ArrayList<CraftsManDetailPojo> myCraftMenList) {
            this.myContext = myContext;
            this.myCraftMenList = myCraftMenList;
            myInflater = LayoutInflater.from(myContext);
        }

        @Override
        public int getCount() {
            return myCraftMenList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {
            private CustomTextView myCraftManReviewCountTXT, myCraftManExperienceTXT, myCraftManAddressTXT, myCraftManJobRadiusTXT;
            RatingBar myCraftManAvrgReviewRatingBAR;
            CircleImageView myCraftProfileImageView;
            BoldCustomTextView myCraftManNameTXT;
            CustomButton mySelectCraftManBTN;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            SelectCraftsmenAdapter.ViewHolder holder;
            if (convertView == null) {
                holder = new SelectCraftsmenAdapter.ViewHolder();
                convertView = myInflater.inflate(R.layout.layout_inflate_craft_men_list_item, parent, false);

                holder.myCraftManReviewCountTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_craft_men_review_count_textView);
                holder.myCraftManExperienceTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_craft_men_experience_textView);
                holder.myCraftManAddressTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_craft_men_address_textView);
                holder.myCraftManJobRadiusTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_craft_men_job_radius_textView);
                holder.myCraftManAvrgReviewRatingBAR = (RatingBar) convertView.findViewById(R.id.layout_inflate_craft_men_review_ratingBar);
                holder.myCraftProfileImageView = (CircleImageView) convertView.findViewById(R.id.layout_inflater_craft_men_profile_imageView);
                holder.myCraftManNameTXT = (BoldCustomTextView) convertView.findViewById(R.id.layout_inflater_craft_men_name_textView);
                holder.mySelectCraftManBTN = (CustomButton) convertView.findViewById(R.id.layout_inflater_craft_men_select_button);

                convertView.setTag(holder);
            } else {
                holder = (SelectCraftsmenAdapter.ViewHolder) convertView.getTag();
            }

            holder.myCraftManNameTXT.setText(myCraftMenList.get(position).getCraftsManProfileName());
            holder.myCraftManJobRadiusTXT.setText(myCraftMenList.get(position).getCraftsManJobRadius());
            holder.myCraftManAddressTXT.setText(myCraftMenList.get(position).getCraftsManAddress());
            holder.myCraftManExperienceTXT.setText(myCraftMenList.get(position).getCraftsManYearOfExperience());
            holder.myCraftManReviewCountTXT.setText(myCraftMenList.get(position).getCraftsManReviewCount());

            holder.mySelectCraftManBTN.setText(getResources().getString(R.string.connnform));
            if (URLUtil.isValidUrl(myCraftMenList.get(position).getCraftsManProfileImage())) {
                /*Picasso.with(myContext).load(*//*ServiceConstant.Base_Url + *//*myCraftMenList.get(position).getCraftsManProfileImage())
                        .resize(100, 100)
                        .placeholder(R.drawable.icon_placeholder)
                        .into(holder.myCraftProfileImageView);*/

                Picasso.get().load(myCraftMenList.get(position).getCraftsManProfileImage())
                        .resize(100, 100)
                        .placeholder(R.drawable.icon_placeholder).into(holder.myCraftProfileImageView);
            }


            holder.myCraftProfileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("sendtaskerid", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("taskerid", myCraftMenList.get(position).getTaskerid());
                    editor.apply();
                    editor.commit();

                    Intent in = new Intent(CraftsmanRescheduleActivity.this, TaskerProfileFragActivity.class);
                    startActivity(in);

                }
            });

            holder.myCraftManAvrgReviewRatingBAR.setRating(Float.parseFloat(myCraftMenList.get(position).getCraftsManReviewAvrg()));

            holder.mySelectCraftManBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String taskerid = myCraftMenList.get(position).getTaskerid();
                    //  paymentPost(CraftsmanRescheduleActivity.this, ServiceConstant.BookJob);

                    postRequestConfirmBookinga(CraftsmanRescheduleActivity.this, ServiceConstant.newBookJob, taskerid);

                }
            });
            return convertView;
        }
    }


    private void postRequestConfirmBookinga(final Context aContext, String url, String taskerid) {
        final undismisprogress myDialogds = new undismisprogress(aContext);
        if (!myDialogds.isShowing()) {
            myDialogds.show();
        }
        HashMap<String, String> jsonParams = new HashMap<>();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("newuserreschedule", MODE_PRIVATE);
        final String code = pref.getString("jobid", "");
        final String task_idre = pref.getString("task_idre", "");

        String latre = pref.getString("latre", "");
        String longre = pref.getString("longre", "");
        String zipcodere = pref.getString("zipcodere", "");

        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("booking_source", "app");
        jsonParams.put("taskid", task_idre);
        jsonParams.put("taskerid", taskerid);


        jsonParams.put("pickup_date", totalday);
        jsonParams.put("pickup_time", mySelectedTimeSTR);


        jsonParams.put("location", zipcodere);
        jsonParams.put("tasklat", latre);
        jsonParams.put("tasklng", longre);


        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Confirm Booking Response----------------" + response);

                try {

                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {


                        if (myDialogds.isShowing()) {
                            myDialogds.dismiss();
                        }


                        final PkDialog mDialog = new PkDialog(CraftsmanRescheduleActivity.this);
                        mDialog.setDialogTitle("CONGRATULATIONS!");
                        mDialog.setDialogMessage("Your Job Has Been Successfully Scheduled.\n\nWe are Excited To Serve You And Care For Your Home");
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                        SharedPreferences getcard;
                                        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                                        SharedPreferences.Editor editor = getcard.edit();
                                        editor.putString("jobid", "" + code);
                                        editor.putString("servicetype", "");
                                        editor.commit();
                                        editor.apply();

                                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                                        startActivity(intent);
                                        finish();
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    }
                                }
                        );
                        mDialog.show();


                    } else {

                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialogds.isShowing()) {
                    myDialogds.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialogds.isShowing()) {
                    myDialogds.dismiss();
                }
            }
        });

    }


    private void PostRequest() {

        myDialog = new ProgressDialogcreated(CraftsmanRescheduleActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("date", totalday);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("newuserreschedule", MODE_PRIVATE);
        String code = pref.getString("jobid", "");

        String subcat = pref.getString("subcat", "");
        String latre = pref.getString("latre", "");
        String longre = pref.getString("longre", "");
        String zipcodere = pref.getString("zipcodere", "");
        String overalltaskhrsre = pref.getString("overalltaskhrsre", "");

        jsonParams.put("overall_taskhours", "" + overalltaskhrsre);
        jsonParams.put("zipcode", zipcodere);
        jsonParams.put("lat", latre);
        jsonParams.put("long", longre);


        if (subcat.contains(",")) {
            String[] subcatsplit = subcat.split(",");
            for (int j = 0; j < subcatsplit.length; j++) {
                jsonParams.put("subcategory[" + j + "]", subcatsplit[j]);
            }
        } else {
            jsonParams.put("subcategory[" + 0 + "]", subcat);
        }


        ServiceRequest mRequest = new ServiceRequest(CraftsmanRescheduleActivity.this);
        mRequest.makeServiceRequest(ServiceConstant.availabledays, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                activity_shedule_appointment_recyclerviewLAY.setVisibility(View.VISIBLE);
                System.out.println("--------------Otp reponse-------------------" + response);
                String Sstatus;
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    myTimeArrList.clear();
                    checktime.clear();
                    if (Sstatus.equalsIgnoreCase("1")) {
                        activity_shedule_appointment_recyclerviewLAY.setVisibility(View.VISIBLE);
                        mySelectedTimeSTR = "";
                        donetoreschedule.setEnabled(false);
                        donetoreschedule.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.darkcolor_1));
                        JSONArray aMainArr = object.getJSONArray("all_slots_array");

                        saveposva = 0;
                        int hj = 0;
                        for (int a = 0; a < aMainArr.length(); a++) {


                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);
                            if (aSubCatObj.getString("available").equals("1")) {
                                if (hj == 0) {
                                    saveposva = a;
                                    hj++;
                                }

                            }

                            AddItemsInTimeArr("false", aSubCatObj.getString("slot"), aSubCatObj.getString("from"), aSubCatObj.getString("available"));
                        }

                        if (aMainArr.length() == 0) {
                            activity_shedule_appointment_recyclerviewLAY.setVisibility(View.GONE);
                            activity_select_craft_men_TXT_title.setText("");
                            if (myCraftsManArrList != null) {
                                myCraftsManArrList.clear();
                                myCraftsMenDetailListView.setEnabled(false);
                                craftmyAdapter = new SelectCraftsmenAdapter(CraftsmanRescheduleActivity.this, myCraftsManArrList);
                                myCraftsMenDetailListView.setAdapter(craftmyAdapter);
                            }
                            HNDHelper.showErrorAlert(CraftsmanRescheduleActivity.this, getResources().getString(R.string.class_schedule_appoitment_no_booking));
                        }


                    } else {
                        activity_shedule_appointment_recyclerviewLAY.setVisibility(View.GONE);
                        activity_select_craft_men_TXT_title.setText("");

                        if (myCraftsManArrList != null) {
                            myCraftsManArrList.clear();
                            myCraftsMenDetailListView.setEnabled(false);
                            craftmyAdapter = new SelectCraftsmenAdapter(CraftsmanRescheduleActivity.this, myCraftsManArrList);
                            myCraftsMenDetailListView.setAdapter(craftmyAdapter);
                        }
                        myTimeArrList.clear();
                        checktime.clear();

                        if (object.has("errors")) {
                            //showerrormessage.setText(object.getString("errors"));
                            HNDHelper.showErrorAlert(CraftsmanRescheduleActivity.this, object.getString("errors"));

                        } else {
                            HNDHelper.showErrorAlert(CraftsmanRescheduleActivity.this, getResources().getString(R.string.class_schedule_appoitment_no_booking));
                        }


                        activity_shedule_appointment_recyclerviewLAY.setVisibility(View.VISIBLE);

                        mySelectedTimeSTR = "";
                        donetoreschedule.setEnabled(false);
                        donetoreschedule.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.darkcolor_1));

                    }


                    recyclerViewAdapter();


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }


    public class CustomScrollListener extends RecyclerView.OnScrollListener {
        private CustomScrollListener() {
        }

        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            switch (newState) {
                case RecyclerView.SCROLL_STATE_IDLE:
                    System.out.println("The RecyclerView is not scrolling");
                    notscrolling = 1;
                    break;
                case RecyclerView.SCROLL_STATE_DRAGGING:
                    System.out.println("Scrolling now");
                    notscrolling = 0;
                    break;
                case RecyclerView.SCROLL_STATE_SETTLING:
                    System.out.println("Scroll Settling");

                    break;

            }

        }

        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            if (dx > 0) {
                System.out.println("Scrolled Right");
                notscrolling = 0;
            } else if (dx < 0) {
                System.out.println("Scrolled Left");
                notscrolling = 0;
            } else {
                System.out.println("No Horizontal Scrolled");

            }

            if (dy > 0) {
                System.out.println("Scrolled Downwards");

            } else if (dy < 0) {
                System.out.println("Scrolled Upwards");

            } else {
                System.out.println("No Vertical Scrolled");

            }
        }
    }
}