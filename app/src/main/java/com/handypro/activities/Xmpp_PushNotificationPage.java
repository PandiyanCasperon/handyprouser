package com.handypro.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.handypro.Dialog.PkDialog;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.navigationMenu.ChatActivity;
import com.handypro.activities.navigationMenu.paymentrequest;
import com.handypro.core.socket.SocketHandler;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.volley.ServiceRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


/**
 * Created by user88 on 1/11/2016.
 */
public class Xmpp_PushNotificationPage extends AppCompatActivity {
    private TextView Message_Tv, Textview_Ok, Textview_alert_header, jobid;
    final Handler handler = new Handler();
    private RelativeLayout Rl_layout_alert_ok, pushnotification_alert_cancel;
    String Job_status_message = "", actionnew = "";
    String value = "";
    String Job_id = "";
    String approvalstatus, phototype;
    String username = "";
    private SharedPreference mySession;
    TextView workwait;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pushnotification);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mySession = new SharedPreference(getApplicationContext());

        try {
            SharedPreference mySession = new SharedPreference(Xmpp_PushNotificationPage.this);
            if (!mySession.getLogInStatus()) {
            } else {
                SocketHandler.getInstance(Xmpp_PushNotificationPage.this).getSocketManager().connect();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        MediaPlayer mPlayer = MediaPlayer.create(Xmpp_PushNotificationPage.this, R.raw.notifysound);
        if (mPlayer.isPlaying()) {
            mPlayer.pause();
        } else {
            mPlayer.start();
        }
        initialize();

    }

    private void notificationmethod() {

        String lastvalue = "";
        if (getIntent() != null && getIntent().hasExtra("TITLE_INTENT")) {
            value = getIntent().getStringExtra("TITLE_INTENT");
        }


        SQLiteDatabase dataBase;
        NotificationDB mHelper = new NotificationDB(this);
        dataBase = mHelper.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + NotificationDB.TABLE_NAME, null);

        if (mCursor.moveToLast()) {
            do {
                lastvalue = mCursor.getString(mCursor.getColumnIndex(NotificationDB.KEY_FNAME));
                System.out.println("last inserted record--->fcm-okl" + lastvalue);
            } while (mCursor.moveToNext());
        }


        if (lastvalue.equals("")) {
            if (lastvalue.equalsIgnoreCase(value)) {
                value = lastvalue;
            } else {
                value = lastvalue;

            }
            commonuichange();
        } else {
            commonuichange();
        }
    }

    private void commonuichange() {
        try {
            if (value.equals("") || value.equals(" ")) {
                pushnotification_alert_cancel.setVisibility(View.GONE);
                Textview_Ok.setText(getResources().getString(R.string.class_xmppPush_open));
                actionnew = "chat";
                jobid.setText("");
                Message_Tv.setText(getResources().getString(R.string.class_xmppPush_message));
            } else {
                JSONObject json = new JSONObject(value);
                JSONObject object1 = json.getJSONObject("data");
                if (object1.has("key0")) {
                    Job_id = object1.getString("key0");
                    Job_status_message = object1.getString("message");
                    actionnew = object1.getString("action");

                    if (actionnew.equalsIgnoreCase("job_user_approval")) {
                        pushnotification_alert_cancel.setVisibility(View.GONE);
                        Textview_Ok.setText(getResources().getString(R.string.class_xmppPush_open));
                    } else if (actionnew.equalsIgnoreCase("admin_notification")) {
                        pushnotification_alert_cancel.setVisibility(View.GONE);
                        Textview_Ok.setText("CLOSE");
                    } else {
                        if (actionnew.equalsIgnoreCase("job_accepted")) {
                            if (Job_status_message.contains("accepted")) {
                                Job_status_message = "A Craftsmen has accepted your job. We are Excited To Serve You and Care For Your Home.";
                            }
                            Textview_alert_header.setText(getResources().getString(R.string.congrats));
                        } else if (actionnew.equalsIgnoreCase("Task_failed")) {
                            Textview_alert_header.setText(getResources().getString(R.string.thanks));
                            workwait.setVisibility(View.VISIBLE);
                        }
                        pushnotification_alert_cancel.setVisibility(View.GONE);
                        Textview_Ok.setText(getResources().getString(R.string.class_xmppPush_open));
                    }

                    if (actionnew.equalsIgnoreCase("referer_credited")) {
                        jobid.setText("");
                        Message_Tv.setText("" + Job_status_message);
                        Textview_alert_header.setText("");
                    } else {
                        jobid.setText("" + Job_id);
                        Message_Tv.setText("" + Job_status_message);
                        //  Textview_alert_header.setText(getResources().getString(R.string.class_xmppPush_notification_id));
                    }
                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.putString("servicetype", "");
                    editor.apply();
                } else {
                    pushnotification_alert_cancel.setVisibility(View.GONE);
                    Textview_Ok.setText(getResources().getString(R.string.class_xmppPush_open));
                    actionnew = object1.getString("action");
                    jobid.setText("");
                    Message_Tv.setText(getResources().getString(R.string.class_xmppPush_message));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void initialize() {
        workwait = findViewById(R.id.workwait);
        pushnotification_alert_cancel = findViewById(R.id.pushnotification_alert_cancel);
        Textview_Ok = findViewById(R.id.pushnotification_alert_ok_textview);
        Message_Tv = findViewById(R.id.pushnotification_alert_messge_textview);
        Textview_alert_header = findViewById(R.id.pushnotification_alert_messge_label);
        Rl_layout_alert_ok = findViewById(R.id.pushnotification_alert_ok);
        jobid = findViewById(R.id.jobid);

        if (mySession.getLogInStatus()) {
            username = mySession.getUserDetails().getUserName();
            Textview_alert_header.setText("Hi " + username + "!");

        }
        notificationmethod();
        Rl_layout_alert_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (actionnew.equalsIgnoreCase("job_reassign")) {
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("page", "1");
                    prefeditor.apply();


                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.putString("servicetype", "");
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                    startActivity(intent);
                    finish();

                } else if (actionnew.equalsIgnoreCase("Task_failed")) {

                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("page", "1");
                    prefeditor.apply();


                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.putString("servicetype", "");
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                    intent.putExtra("gotore", "goo");
                    startActivity(intent);
                    finish();


                } else if (actionnew.equalsIgnoreCase("admin_notification")) {

                    finish();
                } else if (actionnew.equalsIgnoreCase("job_user_approval")) {

                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), newmoreinfo.class);
                    startActivity(intent);
                    finish();

                } else if (actionnew.equalsIgnoreCase("start_off")) {

                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("page", "1");
                    prefeditor.apply();

                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.putString("servicetype", "");
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                    startActivity(intent);
                    finish();

                } else if (actionnew.equalsIgnoreCase("chat")) {
                    Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("provider_reached")) {
                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("page", "1");
                    prefeditor.apply();


                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.putString("servicetype", "");
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("job_started")) {

                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("page", "1");
                    prefeditor.apply();

                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.putString("servicetype", "");
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("job_completed")) {
                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), newmoreinfo.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("job_rescheduled")) {

                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("page", "1");
                    prefeditor.apply();


                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.putString("servicetype", "");
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                    startActivity(intent);
                    finish();

                } else if (actionnew.equalsIgnoreCase("change_order_request")) {


                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("page", "1");
                    prefeditor.apply();

                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), MoreInfoPage.class);
                    startActivity(intent);
                    finish();


                } else if (actionnew.equalsIgnoreCase("estimation_received")) {

                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("page", "1");
                    prefeditor.apply();


                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), approveestimatebuilder.class);
                    startActivity(intent);
                    finish();

                } else if (actionnew.equalsIgnoreCase("job_accepted")) {


                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("page", "1");
                    prefeditor.apply();

                    SharedPreferences showpopup;
                    showpopup = getApplicationContext().getSharedPreferences("showpoupup", 0); // 0 - for private mode
                    SharedPreferences.Editor editorshowpopup = showpopup.edit();
                    editorshowpopup.putString("show", "1");
                    editorshowpopup.apply();


                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.putString("servicetype", "");
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("payment_paid")) {

                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("page", "1");
                    prefeditor.apply();

                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.apply();

                    Intent intent = new Intent(Xmpp_PushNotificationPage.this, RatingPageActivity.class);
                    intent.putExtra("JobID", Job_id);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("pre_payment_paid")) {

                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("page", "1");
                    prefeditor.apply();


                    SharedPreferences showpopup;
                    showpopup = getApplicationContext().getSharedPreferences("showpoupup", 0); // 0 - for private mode
                    SharedPreferences.Editor editorshowpopup = showpopup.edit();
                    editorshowpopup.putString("show", "1");
                    editorshowpopup.apply();


                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.putString("servicetype", "");
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                    startActivity(intent);
                    finish();
                } else if (actionnew.equalsIgnoreCase("requesting_payment") || actionnew.equalsIgnoreCase("requesting_pre_payment")) {

                    SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                    SharedPreferences.Editor prefeditor = prefjobid.edit();
                    prefeditor.putString("page", "1");
                    prefeditor.apply();

                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + Job_id);
                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), paymentrequest.class);
                    intent.putExtra("Type", actionnew);
                    startActivity(intent);
                    finish();

                } else if (actionnew.equalsIgnoreCase("referer_credited")) {
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacksAndMessages(null);
    }

    private void sendrequest(final Context aContext, String url) {


        final ProgressDialogcreated myDialog = new ProgressDialogcreated(Xmpp_PushNotificationPage.this);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> params = new HashMap<>();
        SharedPreference mySession;
        mySession = new SharedPreference(Xmpp_PushNotificationPage.this);
        params.put("user_id", mySession.getUserDetails().getUserId());


        params.put("job_id", Job_id);

        params.put("status", "" + approvalstatus);
        params.put("position", "" + phototype);


        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------get Confirm Category list Response----------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    String message = object.getString("response");
                    if (object.getString("status").equalsIgnoreCase("1")) {


                        final PkDialog mDialog = new PkDialog(Xmpp_PushNotificationPage.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {


                                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                                        prefeditor.putString("page", "1");
                                        prefeditor.apply();
                                        prefeditor.commit();

                                        SharedPreferences prefjobids = getApplicationContext().getSharedPreferences("opend", 0);
                                        SharedPreferences.Editor prefeditors = prefjobids.edit();
                                        prefeditors.putString("opendialog", "1");
                                        prefeditors.apply();
                                        prefeditors.commit();


                                        SharedPreferences getcard;
                                        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                                        SharedPreferences.Editor editor = getcard.edit();
                                        editor.putString("jobid", "" + Job_id);
                                        editor.putString("servicetype", "");
                                        editor.commit();
                                        editor.apply();

                                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                                        startActivity(intent);
                                        finish();

                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();

                    } else {
                        final PkDialog mDialog = new PkDialog(Xmpp_PushNotificationPage.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.dialog_sorry));
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();
                    }
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block


                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }
}
