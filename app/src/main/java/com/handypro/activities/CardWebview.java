package com.handypro.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.activities.navigationMenu.MyOrdersActivity;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.ConnectionDetector;


public class CardWebview extends AppCompatActivity {

    RelativeLayout back;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SharedPreference sharedPreference;
    private RelativeLayout Rl_back;
    private ImageView Im_backIcon;
    private TextView Tv_headerTitle;
    private WebView webview;
    private ProgressBar progressBar;
    private String sMobileID = "";
    private String UserID = "";
    private String sJobID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_webview);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initialize();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PkDialog mDialog = new PkDialog(CardWebview.this);
                mDialog.setDialogTitle(getResources().getString(R.string.plumbal_webView_label_cancel_transaction));
                mDialog.setDialogMessage(getResources().getString(R.string.plumbal_webView_label_cancel_transaction_proceed));
                mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();

                        // close keyboard
//                        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                        mgr.hideSoftInputFromWindow(Rl_back.getWindowToken(), 0);

                        Intent intent = new Intent(getApplicationContext(), MyOrdersActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("EXIT", true);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }
                });
                mDialog.setNegativeButton(getResources().getString(R.string.action_cancel), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }
        });

    }

    private void initialize() {
        cd = new ConnectionDetector(CardWebview.this);
        isInternetPresent = cd.isConnectingToInternet();
        sharedPreference = new SharedPreference(CardWebview.this);

        webview = (WebView) findViewById(R.id.cabily_money_webview);

        progressBar = (ProgressBar) findViewById(R.id.cabily_money_webview_progressbar);

        // Enable Javascript to run in WebView
        webview.getSettings().setJavaScriptEnabled(true);
        back = (RelativeLayout) findViewById(R.id.cabily_money_webview_header_back_layout);
        // Allow Zoom in/out controls
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setDomStorageEnabled(true);

        // Zoom out the best fit your screen
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);

        // get user data from session
//        HashMap<String, String> user = sessionManager.getUserDetails();
//        UserID = user.get(SharedPreference.KEY_USER_ID);
        UserID = sharedPreference.getUserDetails().getUserId();

        Intent i = getIntent();
        //  sMobileID = i.getStringExtra("MobileID");
        sJobID = i.getStringExtra("JOB_Id");
        sMobileID = i.getStringExtra("Mobile_Id");

        startWebView(ServiceConstant.Card_webview_url + sMobileID);

        System.out.println("sMobileID-----------" + sMobileID);

        System.out.println("paymentwebview----------" + ServiceConstant.Card_webview_url + sMobileID);

        webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100 && progressBar.getVisibility() == ProgressBar.GONE) {
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                }
                progressBar.setProgress(progress);

                if (progress == 100) {
                    progressBar.setVisibility(ProgressBar.GONE);
                }
            }
        });

//        finishReceiver = new RefreshReceiver();
//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction("com.package.finish.Cardpage");
//
//        registerReceiver(finishReceiver, intentFilter);
    }


//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        unregisterReceiver(finishReceiver);
//    }

    private void startWebView(String url) {
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            @Override
            public void onLoadResource(WebView view, String url) {
            }

//            @Override
//            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//                final AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//                builder.setMessage("SSL Error");
//                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        handler.proceed();
//                    }
//                });
//                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        handler.cancel();
//                    }
//                });
//                final AlertDialog dialog = builder.create();
//                dialog.show();
//            }

            @Override
            public void onPageFinished(WebView view, String url) {

                System.out.println("------------url-$$$$$------" + url);

                try {

                    if (url.contains("pay-completed/bycard")) {

                        alertPaySuccess(getResources().getString(R.string.action_success), getResources().getString(R.string.make_payment_webView_label_transaction_success));
                        //finishMethod();
                    }
//                    else if (url.contains("paypalsucess")) {
//                        //webview.clearHistory();
//
//                        alertPaySuccess(getResources().getString(R.string.action_success), getResources().getString(R.string.make_payment_webView_label_transaction_success));
////                        Intent broadcastIntent = new Intent();
////                        broadcastIntent.setAction("com.package.ACTION_CLASS_CARDLIST_REFRESH");
////                        sendBroadcast(broadcastIntent);
////                        finishMethod();
//                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });

        //Load url in webView
        webview.loadUrl(url);
    }

    //--------------Alert Method-----------
    private void alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(CardWebview.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    //--------------Alert Pay Transaction Method-----------
    private void alertPay(String title, String alert) {

        final PkDialog mDialog = new PkDialog(CardWebview.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                Intent intent = new Intent(getApplicationContext(), MyOrdersActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        mDialog.show();

    }

    //--------------Alert Pay Transaction Success Method-----------
    private void alertPaySuccess(String title, String alert) {

//        Intent broadcastIntent = new Intent();
//        broadcastIntent.setAction("com.package.ACTION_CLASS_MY_JOBS_REFRESH");
//        broadcastIntent.putExtra("status","completed");
//        sendBroadcast(broadcastIntent);

        final PkDialog mDialog = new PkDialog(CardWebview.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCancelOnTouchOutside(false);

        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                System.out.println("rat----------------");

                Intent intent = new Intent(CardWebview.this, RatingPageActivity.class);
                intent.putExtra("JobID", sJobID);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


            }
        });
        mDialog.show();
    }

    public void finishMethod() {
        //----changing button of cabily money page----
        // Cardlist.changeButton();
        finish();
        onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        if (webview.canGoBack()) {
            webview.goBack();
        } else {
            // Let the system handle the back button
            super.onBackPressed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

            final PkDialog mDialog = new PkDialog(CardWebview.this);
            mDialog.setDialogTitle(getResources().getString(R.string.plumbal_webView_label_cancel_transaction));
            mDialog.setDialogMessage(getResources().getString(R.string.plumbal_webView_label_cancel_transaction_proceed));
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();

                    Intent intent = new Intent(getApplicationContext(), MyOrdersActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


                    // close keyboard
//                    InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                    mgr.hideSoftInputFromWindow(Rl_back.getWindowToken(), 0);

                    onBackPressed();
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                }
            });
            mDialog.setNegativeButton(getResources().getString(R.string.action_cancel), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();

            return true;
        }
        return false;
    }

}
