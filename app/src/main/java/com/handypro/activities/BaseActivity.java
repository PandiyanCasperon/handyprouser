package com.handypro.activities;

/**
 * Created by user127 on 31-05-2018.
 */

import android.content.ComponentCallbacks2;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    protected void onStart() {
        super.onStart();
       // Toast.makeText(getApplicationContext(),"App In Foreground",Toast.LENGTH_LONG).show();

        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("showintent", 0); // 0 - for private mode
        SharedPreferences.Editor editor = getcard.edit();
        editor.putString("shoo", "1");
        editor.commit();
        editor.apply();

    }



    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
         //   Toast.makeText(getApplicationContext(),"App In background",Toast.LENGTH_LONG).show();
            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("showintent", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("shoo", "0");
            editor.commit();
            editor.apply();
        }
    }
}