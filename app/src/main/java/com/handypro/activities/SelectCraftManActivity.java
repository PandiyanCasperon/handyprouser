package com.handypro.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.CraftsManDetailPojo;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.BoldCustomTextView;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by CAS63 on 2/12/2018.
 */

public class SelectCraftManActivity extends AppCompatActivity {

    private ListView myCraftsMenDetailListView;
    private SelectCraftsmenAdapter myAdapter;
    private ArrayList<CraftsManDetailPojo> myCraftsManArrList;
    private RelativeLayout myBackLAY;
    private MaterialRefreshLayout myRefreshLAY;
    private ConnectionDetector myConnectionManager;
    private SharedPreference mySession;
    private ProgressDialogcreated myDialog;
    private LinearLayout myNoInternetLAY;
    String overallresponse,bookingid;
    private int SHEDULE_APPOINTMENT_FLAG = 2033;
    String taskername,taskerreviews,taskeraddress,taskerkm,taskeroverallreviews,taskerstar,taskerimage,taskexperience;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_craft_men);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initClassAndWidgets();
        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("selectresponse", 0); // 0 - for private mode
        overallresponse=getcard.getString("res","");
        bookingid=getcard.getString("bookingid","");
       // getData();
        loadCraftManData();
    }

    private void initClassAndWidgets() {
        myConnectionManager = new ConnectionDetector(SelectCraftManActivity.this);
        mySession = new SharedPreference(SelectCraftManActivity.this);
        myCraftsMenDetailListView = (ListView) findViewById(R.id.activity_select_craft_men_listView);
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_select_craft_men_LAY_back);
        myRefreshLAY = (MaterialRefreshLayout) findViewById(R.id.activity_select_craft_men_Swipe_layout);
        myNoInternetLAY = (LinearLayout) findViewById(R.id.activity_select_craft_men_page_internetLAY);
        myNoInternetLAY.setVisibility(View.GONE);


        clickListeners();
        PerformRefresh();
    }

    private void clickListeners() {
        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }



    private void setDataInAdapter() {
        myAdapter = new SelectCraftsmenAdapter(SelectCraftManActivity.this, myCraftsManArrList);
        myCraftsMenDetailListView.setAdapter(myAdapter);
    }

    private void PerformRefresh() {
        myRefreshLAY.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadCraftManData();
                        myRefreshLAY.finishRefresh();
                    }
                }, 750);
            }
        });
    }


    private void loadCraftManData() {


        try {
            JSONObject object = new JSONObject(overallresponse);
            if (object.getString("status").equalsIgnoreCase("1"))
            {
                JSONObject aRespObj = object.getJSONObject("response");

                JSONObject datas = object.getJSONObject("datas");
                JSONArray aMainArr = datas.getJSONArray("tasker");

                myCraftsManArrList = new ArrayList<CraftsManDetailPojo>();
                for (int a = 0; a < aMainArr.length(); a++) {

                    JSONObject aSubCatObj = aMainArr.getJSONObject(a);

                    CraftsManDetailPojo aCrftMnPojo = new CraftsManDetailPojo();
                    aCrftMnPojo.setCraftsManProfileName(aSubCatObj.getString("name"));
                    aCrftMnPojo.setCraftsManProfileImage(aSubCatObj.getString("avatar"));

                    aCrftMnPojo.setTaskerid(aSubCatObj.getString("_id"));
                    aCrftMnPojo.setCraftsManAddress(aSubCatObj.getString("about"));
                    aCrftMnPojo.setCraftsManJobRadius(aSubCatObj.getString("distance")+aSubCatObj.getString("distanceby"));
                    aCrftMnPojo.setCraftsManReviewAvrg(aSubCatObj.getString("avg_review"));
                    aCrftMnPojo.setCraftsManReviewCount(aSubCatObj.getString("total_review")+" REVIEWS");
                    aCrftMnPojo.setCraftsManYearOfExperience(aSubCatObj.getString("experience"));
                    myCraftsManArrList.add(aCrftMnPojo);

                }
                setDataInAdapter();

            }
            else
            {

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }








    }

    private void postRequestConfirmBooking(final Context aContext, String url,String taskerid){
        myDialog = new ProgressDialogcreated(aContext);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());

        jsonParams.put("booking_source", "app");
        jsonParams.put("taskid", bookingid);
        jsonParams.put("taskerid", taskerid);

        SharedPreferences zip;
        zip = getApplicationContext().getSharedPreferences("zipcode", 0);
        jsonParams.put("location",zip.getString("address",""));
        jsonParams.put("tasklat", zip.getString("Latitude",""));
        jsonParams.put("tasklng",zip.getString("Longitude",""));



        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Confirm Booking Response----------------" + response);

                try {

                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1"))
                    {


                        JSONObject insideres = new JSONObject(object.getString("response"));


                        SharedPreferences pickup;
                        pickup = getApplicationContext().getSharedPreferences("scheduleapp", 0); // 0 - for private mode


                        Intent aSheduleAppointmntIntent = new Intent(SelectCraftManActivity.this, BookingConfirmationWithCraftsmanActivity.class);

                        aSheduleAppointmntIntent.putExtra("IntentMessage", "Your booking has been confirmed");
                        aSheduleAppointmntIntent.putExtra("IntentJobID", insideres.getString("job_id"));
                        aSheduleAppointmntIntent.putExtra("IntentOrderDate", pickup.getString("date","")+" "+pickup.getString("time",""));
                        aSheduleAppointmntIntent.putExtra("IntentServiceType", insideres.getString("note"));
                        aSheduleAppointmntIntent.putExtra("IntentDescription", insideres.getString("description"));
                        aSheduleAppointmntIntent.putExtra("Intenttell", insideres.getString("description"));


                        aSheduleAppointmntIntent.putExtra("taskername",taskername);
                        aSheduleAppointmntIntent.putExtra("taskerimage", taskerimage);
                        aSheduleAppointmntIntent.putExtra("taskerreviews", taskerreviews);
                        aSheduleAppointmntIntent.putExtra("taskexperience",taskexperience);
                        aSheduleAppointmntIntent.putExtra("taskeraddress", taskeraddress);
                        aSheduleAppointmntIntent.putExtra("taskerkm",taskerkm);
                        aSheduleAppointmntIntent.putExtra("taskeroverallreviews",taskeroverallreviews);

                        startActivityForResult(aSheduleAppointmntIntent, SHEDULE_APPOINTMENT_FLAG);

                        if (myDialog.isShowing()) {
                            myDialog.dismiss();
                        }
                    }
                    else
                    {

                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }












    public class SelectCraftsmenAdapter extends BaseAdapter {
        private Context myContext;
        private ArrayList<CraftsManDetailPojo> myCraftMenList;
        private LayoutInflater myInflater;

        public SelectCraftsmenAdapter(Context myContext, ArrayList<CraftsManDetailPojo> myCraftMenList) {
            this.myContext = myContext;
            this.myCraftMenList = myCraftMenList;
            myInflater = LayoutInflater.from(myContext);
        }

        @Override
        public int getCount() {
            return myCraftMenList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

         class ViewHolder {
            private CustomTextView myCraftManReviewCountTXT,myCraftManExperienceTXT,myCraftManAddressTXT,myCraftManJobRadiusTXT;
            RatingBar myCraftManAvrgReviewRatingBAR;
             CircleImageView myCraftProfileImageView;
            BoldCustomTextView myCraftManNameTXT;
            CustomButton mySelectCraftManBTN;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = myInflater.inflate(R.layout.layout_inflate_craft_men_list_item, parent, false);

                holder.myCraftManReviewCountTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_craft_men_review_count_textView);
                holder.myCraftManExperienceTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_craft_men_experience_textView);
                holder.myCraftManAddressTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_craft_men_address_textView);
                holder.myCraftManJobRadiusTXT = (CustomTextView) convertView.findViewById(R.id.layout_inflater_craft_men_job_radius_textView);
                holder.myCraftManAvrgReviewRatingBAR = (RatingBar) convertView.findViewById(R.id.layout_inflate_craft_men_review_ratingBar);
                holder.myCraftProfileImageView = (CircleImageView) convertView.findViewById(R.id.layout_inflater_craft_men_profile_imageView);
                holder.myCraftManNameTXT = (BoldCustomTextView) convertView.findViewById(R.id.layout_inflater_craft_men_name_textView);
                holder.mySelectCraftManBTN = (CustomButton) convertView.findViewById(R.id.layout_inflater_craft_men_select_button);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.myCraftManNameTXT.setText(myCraftMenList.get(position).getCraftsManProfileName());
            holder.myCraftManJobRadiusTXT.setText(myCraftMenList.get(position).getCraftsManJobRadius());
            holder.myCraftManAddressTXT.setText(myCraftMenList.get(position).getCraftsManAddress());
            holder.myCraftManExperienceTXT.setText(myCraftMenList.get(position).getCraftsManYearOfExperience());
            holder.myCraftManReviewCountTXT.setText(myCraftMenList.get(position).getCraftsManReviewCount());




            if(myCraftMenList.get(position).getCraftsManProfileImage().startsWith("http://")||myCraftMenList.get(position).getCraftsManProfileImage().startsWith("https://"))
            {
                Picasso.get().load(myCraftMenList.get(position).getCraftsManProfileImage())
                        .resize(100,100)
                        .placeholder(R.drawable.icon_placeholder)
                        .into(holder.myCraftProfileImageView);
            }
            else
            {
                Picasso.get().load(ServiceConstant.Base_Url + myCraftMenList.get(position).getCraftsManProfileImage())
                        .resize(100,100)
                        .placeholder(R.drawable.icon_placeholder)
                        .into(holder.myCraftProfileImageView);
            }

            holder.myCraftManAvrgReviewRatingBAR.setRating(Float.parseFloat(myCraftMenList.get(position).getCraftsManReviewAvrg()));

            holder.mySelectCraftManBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
              /*  Intent aJobDetIntent = new Intent(myContext, JobDetailActivity.class);
                myContext.startActivity(aJobDetIntent);*/
                    taskername=myCraftMenList.get(position).getCraftsManProfileName();
                    taskerimage=myCraftMenList.get(position).getCraftsManProfileImage();
                    taskerreviews=myCraftMenList.get(position).getCraftsManReviewAvrg();
                    taskexperience= myCraftMenList.get(position).getCraftsManYearOfExperience();
                    taskeraddress=myCraftMenList.get(position).getCraftsManAddress();
                    taskerkm=myCraftMenList.get(position).getCraftsManJobRadius();
                    taskeroverallreviews=myCraftMenList.get(position).getCraftsManReviewCount();


                    if (myConnectionManager.isConnectingToInternet()) {
                        postRequestConfirmBooking(SelectCraftManActivity.this, ServiceConstant.BookJob,myCraftMenList.get(position).getTaskerid());
                    } else {
                        myNoInternetLAY.setVisibility(View.VISIBLE);
                    }
                }
            });
            return convertView;
        }
    }

}
