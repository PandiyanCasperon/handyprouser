package com.handypro.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.handypro.R;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;

/**
 * Created by CAS63 on 2/12/2018.
 */

public class ScheduleCallActivity extends AppCompatActivity {
    final int PERMISSION_REQUEST_CODE = 111;
    final int PERMISSION_REQUEST_CODES = 123;
    private CustomEdittext myNameET, myEmailET, myPhoneNumberET, mySpclInstructionsET;
    private CustomButton myMakeCallBTN;
    private RelativeLayout myBackLAY;
    private String myPhNumberSTR = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shedule_call);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initClassAndWidgets();
        clickListener();
    }

    private void clickListener() {
        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myMakeCallBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myPhNumberSTR != null) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + myPhNumberSTR));
                    startActivity(callIntent);
                }

            }
        });
    }

    private void initClassAndWidgets() {
        myNameET = findViewById(R.id.activity_shedule_call_name_ET);
        myEmailET = findViewById(R.id.activity_shedule_call_email_ET);
        myPhoneNumberET = findViewById(R.id.activity_shedule_call_phone_number_ET);
        mySpclInstructionsET = findViewById(R.id.activity_shedule_call_special_instruction_ET);
        myMakeCallBTN = findViewById(R.id.activity_shedule_call_BTN);
        myBackLAY = findViewById(R.id.activity_shedule_call_password_LAY_back);
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(ScheduleCallActivity.this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:


            case PERMISSION_REQUEST_CODES:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + myPhNumberSTR));
                    startActivity(callIntent);
                }
                break;

        }
    }
}
