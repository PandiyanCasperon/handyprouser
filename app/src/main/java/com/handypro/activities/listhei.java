package com.handypro.activities;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class listhei extends ListView {

    public listhei(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public listhei(Context context) {
        super(context);
    }

    public listhei(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 1,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
