package com.handypro.activities;

/**
 * Created by user127 on 11-04-2018.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.handypro.Dialog.internetmissing;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.PaymentFareSummeryPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.adapters.PaymentFareSummeryAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.CurrencySymbolConverter;
import com.handypro.volley.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.logging.SocketHandler;
/**
 * Created by user127 on 02-04-2018.
 */


/**
 * Created by user88 on 1/8/2016.
 */
public class MoreInfoPayment extends AppCompatActivity {
    SharedPreferences pref;
    int approval_status = 0;
    PaymentFareSummeryAdapter adapter;
    TextView Tvpaymentfare_jobId;
    LinearLayout layout_fare_summery2;
    TextView hintshow;
    private ConnectionDetector cd;
    private Context context;
    private ProgressDialogcreated myDialog;
    private Boolean isInternetPresent = false;
    private boolean show_progress_status = false;
    private Handler mHandler;
    private String provider_id = "";
    private ListView fare_listview;
    private TextView Tv_JobId, Tv_JobDescription;
    private String asyntask_name = "normal", currencyCode;
    private ArrayList<PaymentFareSummeryPojo> farelist, TempFareList;
    private SharedPreference mySession;
    private String Job_id = "";
    private boolean isPaymetFare = false;
    private RelativeLayout Rl_layout_back;
    private SocketHandler socketHandler;
    private int PreAmount = 0;

    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {

        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moreinfopaymentpage);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mySession = new SharedPreference(MoreInfoPayment.this);

        pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
        Job_id = pref.getString("jobid", "");
        initialize();


        Rl_layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


    }

    private void initialize() {

        cd = new ConnectionDetector(MoreInfoPayment.this);
        mHandler = new Handler();

        isInternetPresent = cd.isConnectingToInternet();
        // get user data from session

        farelist = new ArrayList<PaymentFareSummeryPojo>();
        TempFareList = new ArrayList<PaymentFareSummeryPojo>();

        hintshow = (TextView) findViewById(R.id.hintshow);
        Tvpaymentfare_jobId = (TextView) findViewById(R.id.Tvpaymentfare_jobId);
        layout_fare_summery2 = (LinearLayout) findViewById(R.id.layout_fare_summery2);
        fare_listview = (ListView) findViewById(R.id.cancelreason_listView);
        Tv_JobId = (TextView) findViewById(R.id.paymentfare_jobId_);
        Tv_JobDescription = (TextView) findViewById(R.id.Tvpaymentfare_job_description);


        Rl_layout_back = (RelativeLayout) findViewById(R.id.layout_jobfare_back);


        if (isInternetPresent) {
            paymentPost(MoreInfoPayment.this, ServiceConstant.PAYMENT_URL);

        } else {
            final internetmissing mDialog = new internetmissing(MoreInfoPayment.this);
            mDialog.setPositiveButton(
                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );

            mDialog.show();
        }

    }

    //----------------------Post method for Payment Fare------------
    private void paymentPost(Context mContext, String url) {


        myDialog = new ProgressDialogcreated(MoreInfoPayment.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();

        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("job_id", Job_id);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "", Str_jobDescription = "", Str_NeedPayment = "", misc_approval_status = "", Str_Currency = "", Str_BtnGroup = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject object = jobject.getJSONObject("response");
                        JSONObject object2 = object.getJSONObject("job");
                        Str_jobDescription = object2.getString("job_summary");
                        Str_NeedPayment = object2.getString("need_payment");
                        Str_Currency = object2.getString("currency");


                        misc_approval_status = object2.getString("misc_approval_status");

                        // Currency currencycode = Currency.getInstance(getLocale(Str_Currency));

                        if (object2.has("user_pre_amount")) {
                            JSONArray user_pre_amount_obj = object2.getJSONArray("user_pre_amount");
                            for (int i = 0; i < user_pre_amount_obj.length(); i++) {
                                if (user_pre_amount_obj.getJSONObject(i).has("payment_status")
                                        && user_pre_amount_obj.getJSONObject(i).has("pre_amount")
                                        && user_pre_amount_obj.getJSONObject(i).getString("payment_status").equals("Accepted")) {
                                    PreAmount = PreAmount + Integer.parseInt(user_pre_amount_obj.getJSONObject(i).getString("pre_amount"));
                                }

                            }
                        }

                        currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);

                        Str_BtnGroup = object2.getString("btn_group");

                        JSONArray jarry = object2.getJSONArray("billing");


                        if (jarry.length() > 0) {

                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);
                                PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();

                                String title = jobjects_amount.getString("title");
                                String amount = jobjects_amount.getString("amount");

                                if (amount.equalsIgnoreCase("null") || amount.equalsIgnoreCase(null)) {

                                } else {
                                    pojo.setPayment_title(jobjects_amount.getString("title"));

                                    if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                        pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                    } else {
                                        pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount"));
                                    }


                                    farelist.add(pojo);
                                    TempFareList.add(pojo);
                                }


                            }
                            show_progress_status = true;
                        } else {
                            show_progress_status = false;
                        }


                    } else {
                        Str_response = jobject.getString("response");
                    }

                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("1")) {


                    System.out.println();
                    Tvpaymentfare_jobId.setVisibility(View.VISIBLE);
                    Tv_JobDescription.setText(Str_jobDescription);
                    Tv_JobId.setText(Job_id);

                    System.out.println();


                    if (PreAmount != 0 && PreAmount > 0) {
                        PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title("Prepayment");
                        pojo.setPayment_amount(currencyCode + String.valueOf(PreAmount));
                        TempFareList.set((TempFareList.size() - 1), pojo);
                        pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title(farelist.get(farelist.size() - 1).getPayment_title());
                        pojo.setPayment_amount(farelist.get(farelist.size() - 1).getPayment_amount());
                        TempFareList.add(pojo);
                        adapter = new PaymentFareSummeryAdapter(MoreInfoPayment.this, TempFareList);
                    } else if (PreAmount == 0) {
                        adapter = new PaymentFareSummeryAdapter(MoreInfoPayment.this, farelist);
                    }

                    fare_listview.setAdapter(adapter);

//                    if (Str_NeedPayment.equalsIgnoreCase("1")) {
//                        Rl_layout_farepayment_methods.setVisibility(View.VISIBLE);
//                    } else {
//                        Rl_layout_farepayment_methods.setVisibility(View.GONE);
//                    }


                } else {
                    HNDHelper.showResponseErrorAlert(MoreInfoPayment.this, Str_response);

                }

                myDialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                myDialog.dismiss();
            }
        });
    }

    private void accepthit(Context mContext, String url, final int sttaus) {


        myDialog = new ProgressDialogcreated(MoreInfoPayment.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();

        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("job_id", Job_id);
        jsonParams.put("approval_status", "" + sttaus);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "", Str_jobDescription = "", Str_NeedPayment = "", Str_Currency = "", Str_BtnGroup = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {


                        Str_response = jobject.getString("response");
                        HNDHelper.showResponseTitle(getResources().getString(R.string.success_label), MoreInfoPayment.this, Str_response);

                    } else {
                        Str_response = jobject.getString("response");
                        HNDHelper.showResponseTitle(getResources().getString(R.string.action_sorry), MoreInfoPayment.this, Str_response);
                    }


                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }


                myDialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                myDialog.dismiss();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }


}

