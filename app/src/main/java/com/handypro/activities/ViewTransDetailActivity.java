package com.handypro.activities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by CAS63 on 3/8/2018.
 */

public class ViewTransDetailActivity extends AppCompatActivity {
    String myBookingIdStr;
    String userid;
    private LinearLayout myMainLAY;
    private Animation myLayAnim;
    private RelativeLayout myBackLAY;
    private CustomTextView myBookingIDIXT, myTaskCategoryTXT, myTaskerNameTXT, myTaskAddressTXT, myTaskHoursTXT, myPaymentModeTXT, myPerHourTXT, myMinHourlyRateTxt,
            myTaskTimeTXT, myTaskAmountTXT, myMaterialFeeTXT, myServiceTaxTXT, myTotalAmountTXT;
    private ConnectionDetector myConnectionManager;
    private ProgressDialogcreated myDialog;
    private SharedPreference mySession;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_trans_detail);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mySession = new SharedPreference(getApplicationContext());
        myBookingIdStr = getIntent().getExtras().getString("BookingId");
        userid = getIntent().getExtras().getString("ProviderId");
        classAndWidgetInit();
        loadData();
    }

    private void loadData() {
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestTransactionDetail(ViewTransDetailActivity.this, ServiceConstant.TRANSACTION_DETAIL_URL);
        } else {
            HNDHelper.showErrorAlert(ViewTransDetailActivity.this, getResources().getString(R.string.nointernet_text));
        }
    }

    private void classAndWidgetInit() {
        myConnectionManager = new ConnectionDetector(ViewTransDetailActivity.this);
        myMainLAY = (LinearLayout) findViewById(R.id.activity_view_trans_detail_mainLAY);
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_view_trans_detail_LAY_back);
        myLayAnim = AnimationUtils.loadAnimation(ViewTransDetailActivity.this, R.anim.nav_show_text);
        myMainLAY.setAnimation(myLayAnim);

        myBookingIDIXT = (CustomTextView) findViewById(R.id.activity_view_trans_detail_TXT_booking_id);
        myTaskCategoryTXT = (CustomTextView) findViewById(R.id.activity_view_trans_detail_TXT_categoryname);
        myTaskerNameTXT = (CustomTextView) findViewById(R.id.screen_transaction_detail_TXT_categoryname);
        myTaskAddressTXT = (CustomTextView) findViewById(R.id.screen_transaction_detail_TXT_categoryAddress);
        myTaskHoursTXT = (CustomTextView) findViewById(R.id.screen_transaction_detail_TXT_totalHours);
        myPaymentModeTXT = (CustomTextView) findViewById(R.id.screen_transaction_detail_TXT_paymentMode);
        myPerHourTXT = (CustomTextView) findViewById(R.id.screen_transaction_detail_TXT_perHour);
        myMinHourlyRateTxt = (CustomTextView) findViewById(R.id.screen_transaction_detail_TXT_minHour_rate);
        myTaskTimeTXT = (CustomTextView) findViewById(R.id.screen_transaction_detail_TXT_task_time);
        myTaskAmountTXT = (CustomTextView) findViewById(R.id.screen_transaction_detail_TXT_task_amount);
        myMaterialFeeTXT = (CustomTextView) findViewById(R.id.screen_transaction_detail_TXT_material_fees);
        myServiceTaxTXT = (CustomTextView) findViewById(R.id.screen_transaction_detail_TXT_service_tax);
        myTotalAmountTXT = (CustomTextView) findViewById(R.id.screen_transaction_detail_TXT_total_amount);


        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void postRequestTransactionDetail(final Context aContext, String url) {
        myDialog = new ProgressDialogcreated(aContext);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", "" + userid);
        jsonParams.put("booking_id", "" + myBookingIdStr);

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Transaction detail Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {


                    } else {
                        myBookingIDIXT.setText("---");
                        myTaskCategoryTXT.setText("---");
                        myTaskerNameTXT.setText("---");
                        myTaskAddressTXT.setText("---");
                        myTaskHoursTXT.setText("---");
                        myPerHourTXT.setText("---");
                        myMinHourlyRateTxt.setText("---");
                        myTaskAmountTXT.setText("---");
                        myServiceTaxTXT.setText("---");
                        myTotalAmountTXT.setText("---");
                        myTaskTimeTXT.setText("---");
                        myPaymentModeTXT.setText("---");

                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }
}
