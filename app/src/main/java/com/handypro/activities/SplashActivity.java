package com.handypro.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.handypro.BuildConfig;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.SessionParentPojo;
import com.handypro.R;
import com.handypro.Widgets.ForceUpdateAsync;
import com.handypro.Widgets.ProgressDialogcreatedforfrontpage;
import com.handypro.activities.navigationMenu.ChatActivity;
import com.handypro.activities.navigationMenu.paymentrequest;
import com.handypro.fcm.MyFirebaseMessagingService;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.AutoStartHelper;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class SplashActivity extends BaseActivity {

    private static int SPLASH_TIME = 2500;
    private SharedPreference mySession;
    final static int REQUEST_LOCATION = 199;
    final int PERMISSION_REQUEST_CODE = 111;
    private ProgressDialogcreatedforfrontpage myDialog;
    private ConnectionDetector myConnectionManager;


    private final String BRAND_XIAOMI = "xiaomi";
    private final String BRAND_LETV = "letv";
    private final String BRAND_ASUS = "asus";
    private final String BRAND_HONOR = "honor";
    private final String BRAND_OPPO = "oppo";
    private final String BRAND_VIVO = "vivo";
    private final String BRAND_NOKIA = "nokia";

    FirebaseRemoteConfig mFirebaseRemoteConfig;
    String currentVersion = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        myDialog = new ProgressDialogcreatedforfrontpage(SplashActivity.this);
        myConnectionManager = new ConnectionDetector(SplashActivity.this);
        NotificationDB mHelper = new NotificationDB(this);


        int count = mHelper.getProfilesCount();
        if (count > 100) {

            this.deleteDatabase(mHelper.DATABASE_NAME);
            System.out.println("Sara response--->fcm-deleted");
        }

        try {
            currentVersion= SplashActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        SharedPreferences prefss = getApplicationContext().getSharedPreferences("logintoconform", 0); // 0 - for private mode
        SharedPreferences.Editor editorss = prefss.edit();
        editorss.putString("lo", "0");
        editorss.apply();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("beforedismiss", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("before", "0");
        editor.apply();

        mySession = new SharedPreference(SplashActivity.this);
        mySession.SetFlowFrom("");
        mySession.putSessionParentPojo(new ArrayList<SessionParentPojo>());
        SharedPreferences preferences = getSharedPreferences("jobdetailpage", Context.MODE_PRIVATE);
        SharedPreferences.Editor JobDetailsEditor = preferences.edit();
        JobDetailsEditor.clear();
        JobDetailsEditor.apply();

        getFCMId();
        GetVersionCode versionCode = new GetVersionCode();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            versionCode.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            versionCode.execute();

    }


    private void getFCMId() {
        mySession.putFCMRegisterId(MyFirebaseMessagingService.getRegistrationId(SplashActivity.this));
        Log.e("FCMid", mySession.getFCMId());
    }


    @Override
    protected void onStart() {
        super.onStart();

        //-------------KeyHash Start--------------
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                System.out.println("---------SHA--------" + Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.println("------------KeyHash:---------------" + Base64.encodeToString(md.digest(), Base64.DEFAULT));
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        //-------------KeyHash End--------------



    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkcamerapermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermission() {
       // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                moveToHomeActivity();
            } else {
                moveToHomeActivity();
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_LOCATION) {
            if (resultCode == Activity.RESULT_CANCELED) {
                finish();
            }
        }
    }

    private void moveToHomeActivity() {
        if (myConnectionManager.isConnectingToInternet()) {
            myDialog.show();
            /*platform: android
            app: user/craftsman*/
            HashMap<String, String> jsonParams = new HashMap<>();
            jsonParams.put("platform", "android");
            jsonParams.put("app", "user");
            ServiceRequest mServiceRequest = new ServiceRequest(SplashActivity.this);
            mServiceRequest.makeServiceRequest(ServiceConstant.App_Info, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

                @Override
                public void onCompleteListener(String response) {
                    myDialog.dismiss();
                    Log.e("mobile/appinfo", response);
                    String Str_status, google_api = "";

                    try {
                        JSONObject jobject = new JSONObject(response);
                        Str_status = jobject.getString("status");

                        if (Str_status.equalsIgnoreCase("1")) {

                            if (jobject.has("google_api"))
                                google_api = jobject.getString("google_api");

                            SharedPreferences pref = getApplicationContext().getSharedPreferences("AppInfo", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            //For the security reason, I'm here encoding the code.
                            editor.putString("code", Base64.encodeToString(google_api.getBytes("UTF-8"), Base64.DEFAULT));
                            editor.apply();

                            /*String device_company = Build.BRAND.toLowerCase();
                            if (device_company.equals(BRAND_ASUS) || device_company.equals(BRAND_HONOR) || device_company.equals(BRAND_LETV) || device_company.equals(BRAND_XIAOMI) || device_company.equals(BRAND_OPPO) || device_company.equals(BRAND_VIVO) || device_company.equals(BRAND_NOKIA))
                                AutoStartHelper.getInstance(SplashActivity.this).getAutoStartPermission(SplashActivity.this);*/

                            if (mySession.getAutoStart()) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        CheckBundle();
                                    }
                                }, SPLASH_TIME);
                            } else {
                                String device_company = Build.BRAND.toLowerCase();
                                if (device_company.equals(BRAND_ASUS) || device_company.equals(BRAND_HONOR) || device_company.equals(BRAND_LETV) || device_company.equals(BRAND_XIAOMI) || device_company.equals(BRAND_OPPO) || device_company.equals(BRAND_VIVO) || device_company.equals(BRAND_NOKIA))
                                    AutoStartHelper.getInstance(SplashActivity.this).getAutoStartPermission(SplashActivity.this);
                                else {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            CheckBundle();
                                        }
                                    }, SPLASH_TIME);
                                }
                            }


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onErrorListener() {
                    myDialog.dismiss();
                    Toast.makeText(SplashActivity.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    void CheckBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("data") && bundle.getString("data") != null) {
            JSONObject jsonObj;
            try {
                jsonObj = new JSONObject(bundle.getString("data"));
                if (jsonObj.has("action")) {
                    String Action = jsonObj.getString("action");
                    String Job_id = jsonObj.getString("key0");
                    if (Action.equals("job_reassign")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("page", "1");
                        prefeditor.apply();


                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.putString("servicetype", "");
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("Task_failed")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("page", "1");
                        prefeditor.apply();


                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.putString("servicetype", "");
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                        intent.putExtra("gotore", "goo");
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("admin_notification")) {
                        finish();
                    } else if (Action.equals("job_user_approval")) {
                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), newmoreinfo.class);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("start_off")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("page", "1");
                        prefeditor.apply();

                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.putString("servicetype", "");
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("chat")) {
                        Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("provider_reached")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("page", "1");
                        prefeditor.apply();


                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.putString("servicetype", "");
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("job_started")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("page", "1");
                        prefeditor.apply();

                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.putString("servicetype", "");
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("job_completed")) {
                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), newmoreinfo.class);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("job_rescheduled")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("page", "1");
                        prefeditor.apply();


                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.putString("servicetype", "");
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("change_order_request")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("page", "1");
                        prefeditor.apply();

                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), MoreInfoPage.class);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("estimation_received")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("page", "1");
                        prefeditor.apply();


                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), approveestimatebuilder.class);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("job_accepted")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("page", "1");
                        prefeditor.apply();

                        SharedPreferences showpopup;
                        showpopup = getApplicationContext().getSharedPreferences("showpoupup", 0); // 0 - for private mode
                        SharedPreferences.Editor editorshowpopup = showpopup.edit();
                        editorshowpopup.putString("show", "1");
                        editorshowpopup.apply();


                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.putString("servicetype", "");
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("payment_paid")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("page", "1");
                        prefeditor.apply();

                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.apply();

                        Intent intent = new Intent(SplashActivity.this, RatingPageActivity.class);
                        intent.putExtra("JobID", Job_id);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("pre_payment_paid")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("page", "1");
                        prefeditor.apply();


                        SharedPreferences showpopup;
                        showpopup = getApplicationContext().getSharedPreferences("showpoupup", 0); // 0 - for private mode
                        SharedPreferences.Editor editorshowpopup = showpopup.edit();
                        editorshowpopup.putString("show", "1");
                        editorshowpopup.apply();


                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.putString("servicetype", "");
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("requesting_payment") || Action.equalsIgnoreCase("requesting_pre_payment")) {
                        SharedPreferences prefjobid = getApplicationContext().getSharedPreferences("reload", 0);
                        SharedPreferences.Editor prefeditor = prefjobid.edit();
                        prefeditor.putString("page", "1");
                        prefeditor.apply();

                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("sendjobid", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("jobid", "" + Job_id);
                        editor.apply();

                        Intent intent = new Intent(getApplicationContext(), paymentrequest.class);
                        intent.putExtra("Type", Action);
                        startActivity(intent);
                        finish();
                    } else if (Action.equals("referer_credited")) {
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

            moveHomeIntent();


            mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
            FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                    .setMinimumFetchIntervalInSeconds(3600)
                    .build();
            mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);
            mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);

            mFirebaseRemoteConfig.fetchAndActivate()
                    .addOnCompleteListener(this, new OnCompleteListener<Boolean>() {
                        @Override
                        public void onComplete(@NonNull Task<Boolean> task) {
                            if (task.isSuccessful()) {
                                boolean updated = task.getResult();
                                Log.d("Remote Config", "Config params updated: " + updated);
                            } else {
                                Log.d("Remote Config", "Fetch failed");
                            }

                        }
                    });
//            if (!mFirebaseRemoteConfig.getString("version").equals("1.0") && !mFirebaseRemoteConfig.getString("version").equals(BuildConfig.VERSION_NAME)) {
//                if (mFirebaseRemoteConfig.getBoolean("force_update")) {
//                    displayForceUpdateDialog(false);
//                } else if (mFirebaseRemoteConfig.getBoolean("update")) {
//                    displayForceUpdateDialog(true);
//                }
//
//            } else {
//                moveHomeIntent();
//            }
        }
    }


//    @Override
//    protected void onResume() {
//        super.onResume();
//        forceUpdate();
//
//    }
//
//    public void forceUpdate(){
//        PackageManager packageManager = this.getPackageManager();
//        PackageInfo packageInfo = null;
//        try {
//            packageInfo =  packageManager.getPackageInfo(getPackageName(),0);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//        String currentVersion = packageInfo.versionName;
//        new ForceUpdateAsync(currentVersion,SplashActivity.this).execute();
//    }
//


    void displayForceUpdateDialog(final boolean check) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setCancelable(false);

        if (!SplashActivity.this.isFinishing()) {
            builder.setTitle("Update Pending...")
                    .setMessage("The new version of the app available please update for more option")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mFirebaseRemoteConfig.getString("link"))));
                            finish();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (check)
                                moveHomeIntent();
                            else
                                finish();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    void moveHomeIntent() {
        Intent aMainPageIntent = new Intent(SplashActivity.this, HomeActivity.class);
        startActivity(aMainPageIntent);
        finish();
    }
    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = "";
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" +SplashActivity.this.getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("https://www.google.com")
                        .get()
                        .select(".hAyfc .htlgb")
                        .get(7)
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (!onlineVersion.equals("")) {

                if (onlineVersion != null && !onlineVersion.isEmpty()) {
                    if (!currentVersion.equals(onlineVersion)) {

                        if (SplashActivity.this != null && !SplashActivity.this.isFinishing()) {

                            AlertPlayStore(getResources().getString(R.string.app_name), "There is newer version of this application available, click OK to upgrade now?");
                        }

                        //                Alert(getResources().getString(R.string.app_name), "There is newer version of this application available, click OK to upgrade now?");
                    } else {
                        if (Build.VERSION.SDK_INT >= 23) {
                            // Marshmallow+
                            if (!checkWriteExternalStoragePermission() || !checkcamerapermission()) {
                                requestPermission();
                            } else {
                                moveToHomeActivity();
                            }
                        } else {
                            moveToHomeActivity();
                        }
                    }
                }
            } else {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() ||
                            !checkWriteExternalStoragePermission() || !checkcamerapermission()) {
                        requestPermission();
                    } else {
                        moveToHomeActivity();
                    }
                } else {
                    moveToHomeActivity();
                }
            }
            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
        }
    }
    private void AlertPlayStore(String title, String alert) {
        try {
            final PkDialog mDialog = new PkDialog(SplashActivity.this);
            mDialog.setDialogTitle(title);
            mDialog.setDialogMessage(alert);
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                    finish();
                }
            });
            mDialog.setNegativeButton(getResources().getString(R.string.action_cancel), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    moveHomeIntent();
                    //finish();
                }
            });
            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*File dir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + SplashActivity.this.getApplicationContext().getPackageName()
                + "/Files/Compressed");*/

        File dir = new File(getExternalFilesDir("") + "/Compressed");

        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String child : children) {
                new File(dir, child).delete();
            }
        }
    }
}
