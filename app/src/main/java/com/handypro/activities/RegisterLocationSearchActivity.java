package com.handypro.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.adapters.LocationSearchAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.textview.CustomEdittext;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by CAS63 on 2/20/2018.
 */

public class RegisterLocationSearchActivity extends AppCompatActivity {
    private CustomEdittext mySearchLocET;
    private ListView myLocationListView;
    private ConnectionDetector myConnectionManager;
    private LocationSearchAdapter myAdapter;
    private ArrayList<String> myItemList_location;
    private ArrayList<String> myItemList_placeId;
    private Boolean isdataAvailable = false;
    private String mySelected_location = "", myLatitude = "", myLongitude = "", map_key = "";
    private RelativeLayout myBackLAY;
    private GeometricProgressView myProgressView;
    private Dialog myGetZipCodeDialog;
    String myStatus = "", myArea = "", myFormateedaddress = "", myLocality = "", myState_name = "", myCity_name = "", myPostalCode = "", myCountry = "", line1 = "", line2 = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_location_search);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("AppInfo", MODE_PRIVATE);
        byte[] data = Base64.decode(pref.getString("code", ""), Base64.DEFAULT);
        try {
            map_key = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        classAndWidgetsInit();
        editTextOnTextChange();
        listViewItemClick();
    }

    private void listViewItemClick() {
        myLocationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mySelected_location = myItemList_location.get(position);
                if (myConnectionManager.isConnectingToInternet()) {
                    PostLatLongRequest(ServiceConstant.GetAddressFrom_LatLong_url + map_key + "&placeid=" + myItemList_placeId.get(position));
                } else {
                    HNDHelper.showErrorAlert(RegisterLocationSearchActivity.this, getResources().getString(R.string.nointernet_text));
                }
            }
        });
    }

    private void PostLatLongRequest(String Url) {
        System.out.println("--------------LatLong url-------------------" + Url);
        ServiceRequest mRequest = new ServiceRequest(RegisterLocationSearchActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------LatLong  reponse-------------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        myStatus = object.getString("status");
                        JSONObject place_object = object.getJSONObject("result");
                        if (myStatus.equalsIgnoreCase("OK")) {
                            if (place_object.length() > 0) {
                                myArea = place_object.getString("name");
                                myFormateedaddress = place_object.getString("formatted_address");
                                JSONArray addressArray = place_object.getJSONArray("address_components");
                                if (addressArray.length() > 0) {
                                    for (int i = 0; i < addressArray.length(); i++) {
                                        JSONObject address_object = addressArray.getJSONObject(i);

                                        JSONArray typesArray = address_object.getJSONArray("types");
                                        if (typesArray.length() > 0) {
                                            for (int j = 0; j < typesArray.length(); j++) {

                                                if (typesArray.get(j).toString().equalsIgnoreCase("locality")) {
                                                    myLocality = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("administrative_area_level_2")) {
                                                    myCity_name = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("administrative_area_level_1")) {
                                                    myState_name = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("postal_code")) {
                                                    myPostalCode = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("country")) {
                                                    myCountry = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("sublocality_level_1")) {
                                                    line1 = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("sublocality_level_2")) {
                                                    line2 = address_object.getString("long_name");
                                                }


                                            }
                                            isdataAvailable = true;
                                        } else {
                                            isdataAvailable = false;
                                        }


                                    }
                                } else {
                                    isdataAvailable = false;
                                }


                                JSONObject geometry_object = place_object.getJSONObject("geometry");
                                if (geometry_object.length() > 0) {
                                    JSONObject location_object = geometry_object.getJSONObject("location");
                                    if (location_object.length() > 0) {
                                        myLatitude = location_object.getString("lat");
                                        myLongitude = location_object.getString("lng");
                                        isdataAvailable = true;
                                    } else {
                                        isdataAvailable = false;
                                    }
                                } else {
                                    isdataAvailable = false;
                                }
                            } else {
                                isdataAvailable = false;
                            }
                        } else {
                            isdataAvailable = false;
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isdataAvailable) {

                    if (!myPostalCode.isEmpty()) {

                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("line1", "" + line1);
                        editor.putString("line2", "" + line2);
                        editor.putString("city", "" + myCity_name);
                        editor.putString("state", "" + myState_name);
                        editor.putString("zipcode", "" + myPostalCode);
                        editor.putString("country", "" + myCountry);
                        editor.putString("formatted_address", "" + myFormateedaddress);

                        editor.putString("Selected_Latitude", "" + myLatitude);
                        editor.putString("Selected_Longitude", "" + myLongitude);


                        editor.commit();
                        editor.apply();


                        returnAndFinish(myPostalCode, myArea, myCity_name, myState_name, myLocality);

                    } else {
                        myGetZipCodeDialog = new Dialog(RegisterLocationSearchActivity.this);
                        myGetZipCodeDialog.getWindow();
                        myGetZipCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        myGetZipCodeDialog.setContentView(R.layout.alert_dialog_get_zipcode);
                        myGetZipCodeDialog.setCanceledOnTouchOutside(false);
                        myGetZipCodeDialog.getWindow().getAttributes().windowAnimations = R.style.animations_photo_Picker;
                        myGetZipCodeDialog.show();
                        myGetZipCodeDialog.getWindow().setGravity(Gravity.CENTER);

                        final CustomEdittext myZipCodeET = myGetZipCodeDialog.findViewById(R.id.alert_dialog_ET_get_zipcode);
                        CustomTextView aCancelDialogTXT = myGetZipCodeDialog.findViewById(R.id.alert_dialog_get_zipcode_cancel_TXT);
                        CustomTextView aSubmitZipCodeTXT = myGetZipCodeDialog.findViewById(R.id.alert_dialog_get_zipcode_ok_TXT);

                        aCancelDialogTXT.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                myGetZipCodeDialog.dismiss();
                            }
                        });

                        aSubmitZipCodeTXT.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (myZipCodeET.getText().length() != 0) {
                                    returnAndFinish(myZipCodeET.getText().toString(), myArea, myCity_name, myState_name, myLocality);
                                } else {
                                    return;
                                }

                                myGetZipCodeDialog.dismiss();
                            }
                        });
                    }

                } else {
                    HNDHelper.showResponseErrorAlert(RegisterLocationSearchActivity.this, myStatus);
                }

            }

            @Override
            public void onErrorListener() {

            }
        });
    }

    private void returnAndFinish(String aPostalCode, String sArea, String aCity_name, String aState_name, String aLocality) {

        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("getaddress", 0); // 0 - for private mode
        SharedPreferences.Editor editor = getcard.edit();

        editor.putString("line1", "" + line1);
        editor.putString("line2", "" + line2);
        editor.putString("city", "" + aCity_name);
        editor.putString("state", "" + aState_name);
        editor.putString("zipcode", "" + aPostalCode);
        editor.putString("country", "" + myCountry);
        editor.putString("formatted_address", "" + myFormateedaddress);

        editor.putString("Selected_Latitude", "" + myLatitude);
        editor.putString("Selected_Longitude", "" + myLongitude);

        editor.commit();
        editor.apply();

        onBackPressed();
        finish();
    }


    private void editTextOnTextChange() {
        mySearchLocET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (myConnectionManager.isConnectingToInternet()) {
                    String data = s.toString().replace(" ", "%20");
                    PlaceSearchRequest(ServiceConstant.place_search_url + map_key + "&input=" + data);
                } else {
                    HNDHelper.showErrorAlert(RegisterLocationSearchActivity.this, getResources().getString(R.string.nointernet_text));
                }
                if (s.length() == 0) {
                    myProgressView.setVisibility(View.GONE);
                }
            }
        });

        mySearchLocET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    if (v.getText().toString().isEmpty()) {
                        CloseKeyboard(mySearchLocET);
                    }
                }
                return false;
            }
        });

    }

    private void classAndWidgetsInit() {
        myConnectionManager = new ConnectionDetector(RegisterLocationSearchActivity.this);
        myItemList_location = new ArrayList<String>();
        myItemList_placeId = new ArrayList<String>();

        mySearchLocET = findViewById(R.id.activity_location_search_edittext);
        myLocationListView = findViewById(R.id.activity_location_search_listView);
        myBackLAY = findViewById(R.id.activity_location_search_LAY_back);
        myProgressView = findViewById(R.id.activity_location_search_dialog_progressView);

        /*
         * Show Soft keyboard when page opens
         */
        mySearchLocET.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(mySearchLocET, 0);
            }
        }, 200);

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void PlaceSearchRequest(String Url) {
        myProgressView.setVisibility(View.VISIBLE);

        ServiceRequest mRequest = new ServiceRequest(RegisterLocationSearchActivity.this);
        System.out.println("--------------Search city url-------------------" + Url);

        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Search city  response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");
                        JSONArray place_array = object.getJSONArray("predictions");
                        if (status.equalsIgnoreCase("ok")) {
                            if (place_array.length() > 0) {
                                myItemList_location.clear();
                                myItemList_placeId.clear();
                                for (int i = 0; i < place_array.length(); i++) {
                                    JSONObject place_object = place_array.getJSONObject(i);
                                    myItemList_location.add(place_object.getString("description"));
                                    myItemList_placeId.add(place_object.getString("place_id"));
                                }
                                isdataAvailable = true;
                            } else {
                                myItemList_location.clear();
                                myItemList_placeId.clear();
                                isdataAvailable = false;
                            }
                        } else {
                            myItemList_location.clear();
                            myItemList_placeId.clear();
                            isdataAvailable = false;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                myProgressView.setVisibility(View.GONE);

                myAdapter = new LocationSearchAdapter(RegisterLocationSearchActivity.this, myItemList_location);
                myLocationListView.setAdapter(myAdapter);
                myAdapter.notifyDataSetChanged();
            }

            @Override
            public void onErrorListener() {
                myProgressView.setVisibility(View.GONE);
                CloseKeyboard(mySearchLocET);
            }
        });
    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

}
