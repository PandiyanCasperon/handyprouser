package com.handypro.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.handypro.BuildConfig;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.login.LoginActivity;
import com.handypro.activities.login.SignupActivity;
import com.handypro.activities.navigationMenu.CardListActivity;
import com.handypro.activities.navigationMenu.ChatActivity;
import com.handypro.activities.navigationMenu.InviteFriendsActivity;
import com.handypro.activities.navigationMenu.MyOrdersActivity;
import com.handypro.activities.navigationMenu.NotificationActivity;
import com.handypro.activities.navigationMenu.ReviewsActivity;
import com.handypro.activities.navigationMenu.TransactionActivity;
import com.handypro.adapters.HomeMenuListAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.core.socket.ChatMessageService;
import com.handypro.core.socket.ChatMessageServicech;
import com.handypro.core.socket.SocketHandler;
import com.handypro.fragment.HomePageFragment;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.AutoStartHelper;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.HideSoftKeyboard;
import com.handypro.volley.ServiceRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by CAS61 on 2/8/2018.
 */

public class HomeActivity extends AppCompatActivity {

    public static DrawerLayout myMainDrawerLayout;
    public static RelativeLayout myDrawerLayout;
    private ListView myDrawerList;
    private String[] myTitles;
    private int[] myIcons;
    private static HomeMenuListAdapter myMenuAdapter;
    private SharedPreference mySession;
    private ConnectionDetector myConnectionManager;
    private ProgressDialogcreated myDialog;
    public static HomeActivity myHomeActivity;
    public static boolean isActivityInBG = false;
    public static final long INTERVAL = 5000;//variable to execute services every 10 second
    private Timer mTimer = null; // timer handling
    private Handler mHandler = new Handler(); // run on another Thread to avoid crash
    private String myzipcode = "";
   // private MediaPlayer aPlayer;

    PlayerView aPlayerView;
    SimpleExoPlayer aSimpleExoPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        forceUpdate();


        SharedPreferences getcard;
        getcard = HomeActivity.this.getSharedPreferences("getaddress", 0); // 0 - for private mode
        myzipcode = getcard.getString("zipcode", "");


       /* SharedPreferences.Editor editor = pref.edit();
        editor.putString("termsandconditions", myzipcode);
        editor.apply();
        editor.commit();*/


        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }

        myHomeActivity = HomeActivity.this;
        HideSoftKeyboard.setupUI(
                HomeActivity.this.getWindow().getDecorView(),
                HomeActivity.this);
        classAndWidgetInitialize();

        if (mTimer != null)
            mTimer.cancel();
        else
            mTimer = new Timer(); // recreate new timer
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, INTERVAL);//

    }

    private void classAndWidgetInitialize() {
        mySession = new SharedPreference(HomeActivity.this);
        myConnectionManager = new ConnectionDetector(HomeActivity.this);

        myMainDrawerLayout = (DrawerLayout) findViewById(R.id.activity_home_LAY_drawer_parent);
        myDrawerLayout = (RelativeLayout) findViewById(R.id.activity_home_LAY_drawer);
        myDrawerList = (ListView) findViewById(R.id.activity_home_LV_drawer);


        if (mySession.getLogInStatus() == false) {
            replaceFragment(new HomePageFragment());
            myDrawerLayout.setVisibility(View.GONE);
        } else {
            myDrawerLayout.setVisibility(View.VISIBLE);
            postMode(HomeActivity.this, ServiceConstant.Notification_mode);
            loadMenuData();
            replaceFragment(new HomePageFragment());
            listItemClick();
        }


    }

    private void getMaintenaceData() {
        HashMap<String, String> jsonParams = new HashMap<>();
        ServiceRequest mServiceRequest = new ServiceRequest(HomeActivity.this);
        mServiceRequest.makeServiceRequest(ServiceConstant.GETHMP_DETAILS, Request.Method.GET, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                Log.e("hmp", response);
                String Str_status, google_api = "";

                try {
                    JSONObject jobject = new JSONObject(response);

                    if (jobject.getString("status").equalsIgnoreCase("1")) {
                        JSONObject aResponseObject = jobject.getJSONObject("response");
                        if (aResponseObject.has("popupdescription")) {
                            showMaintenanceDialog(aResponseObject.getString("popupdescription") + "<style>img{width:100%;height:auto!important;}</style>");
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }

    private void listItemClick() {
        myDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                switch (position) {
                    case 0:
                        if (mySession.getLogInStatus()) {
                            Intent aProfIntent = new Intent(HomeActivity.this, UserProfilePageActivity.class);
                            startActivity(aProfIntent);
                        } else {
                            Intent aLoginIntent = new Intent(HomeActivity.this, LoginActivity.class);
                            startActivity(aLoginIntent);
                        }
                        break;

                    case 1:
                        replaceFragment(new HomePageFragment());
                        break;

                    case 2:
                        if (myConnectionManager.isConnectingToInternet()) {
                            Intent aOrdersIntent = new Intent(HomeActivity.this, MyOrdersActivity.class);
                            startActivity(aOrdersIntent);
                        } else {
                            HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.nointernet_text));
                        }
                        break;


                    case 3:
                        if (myConnectionManager.isConnectingToInternet()) {
                            Intent aTransIntent = new Intent(HomeActivity.this, TransactionActivity.class);
                            startActivity(aTransIntent);
                        } else {
                            HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.nointernet_text));
                        }
                        break;

                    case 4:
                        if (myConnectionManager.isConnectingToInternet()) {
                            paymentPost(HomeActivity.this, ServiceConstant.LOYALITY_URL);
                        } else {
                            HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.nointernet_text));
                        }
                        break;

                    case 5:
                        if (myConnectionManager.isConnectingToInternet()) {
                            Intent aChatIntent = new Intent(HomeActivity.this, ChatActivity.class);
                            startActivity(aChatIntent);
                        } else {
                            HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.nointernet_text));
                        }
                        break;

                    case 6:
                        if (myConnectionManager.isConnectingToInternet()) {
                            Intent aNotificationIntent = new Intent(HomeActivity.this, NotificationActivity.class);
                            startActivity(aNotificationIntent);
                        } else {
                            HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.nointernet_text));
                        }
                        break;

                    case 7:
                        if (myConnectionManager.isConnectingToInternet()) {
                            Intent aNotificationIntent = new Intent(HomeActivity.this, ReviewsActivity.class);
                            startActivity(aNotificationIntent);
                        } else {
                            HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.nointernet_text));
                        }
                        break;


                    case 8:
                        if (myConnectionManager.isConnectingToInternet()) {
                            Intent aInvtFrndIntent = new Intent(HomeActivity.this, InviteFriendsActivity.class);
                            startActivity(aInvtFrndIntent);
                        } else {
                            HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.nointernet_text));
                        }
                        break;

                    case 9:
                        if (myConnectionManager.isConnectingToInternet()) {
                            Intent aAddCardIntent = new Intent(HomeActivity.this, CardListActivity.class);
                            startActivity(aAddCardIntent);
                        } else {
                            HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.nointernet_text));
                        }
                        break;

                    case 10:
                        getMaintenaceData();

                        break;
                    case 11:
                        shareReportThrGmail("hpsupport@handypro.com", getResources().getString(R.string.navigation_label_share_to_gmail_report), getResources().getString(R.string.navigation_label_share_to_gmail_sent));
                        break;

                    case 12:
                        if (myConnectionManager.isConnectingToInternet()) {
                            String url = ServiceConstant.Aboutus_Url;
                            Intent intent = new Intent(HomeActivity.this, CommonWebView.class);
                            intent.putExtra("url", url);
                            intent.putExtra("type", "about");
                            intent.putExtra("header", getResources().getString(R.string.navigation_label_about_us));
                            startActivity(intent);
                        } else {
                            HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.nointernet_text));

                        }
                        break;

                    case 13:
                        if (myConnectionManager.isConnectingToInternet()) {
                            SharedPreferences getcard;
                            getcard = HomeActivity.this.getSharedPreferences("getaddress", 0); // 0 - for private mode
                            String zipcode = getcard.getString("zipcode", "");

                            Intent intent = new Intent(HomeActivity.this, CommonWebView.class);
                            intent.putExtra("url", zipcode);
                            intent.putExtra("type", "reg");
                            intent.putExtra("header", getResources().getString(R.string.activity_signup_TXT_signup_terms_use));
                            startActivity(intent);
                        } else {
                            HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.nointernet_text));

                        }
                        break;

                    case 14:
                        if (myConnectionManager.isConnectingToInternet()) {
                            SharedPreferences getcard;
                            getcard = HomeActivity.this.getSharedPreferences("getaddress", 0); // 0 - for private mode
                            String zipcode = getcard.getString("zipcode", "");

                            Intent intent = new Intent(HomeActivity.this, PrivacyPolicyPageActivity.class);
                            intent.putExtra("url", zipcode);
                            intent.putExtra("header", getResources().getString(R.string.privacypolicy));
                            startActivity(intent);
                        } else {
                            HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.nointernet_text));

                        }
                        break;


                    case 15:
                        if (myConnectionManager.isConnectingToInternet()) {

                            final PkDialog mDialog = new PkDialog(HomeActivity.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.navigation_label_logout));
                            mDialog.setDialogMessage(getResources().getString(R.string.activity_home_signout_text));
                            mDialog.setPositiveButton(
                                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();
                                            postRequestLogout(ServiceConstant.logout_url);
                                        }
                                    }
                            );

                            mDialog.setNegativeButton(
                                    getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();

                                        }
                                    }
                            );

                            mDialog.show();


                        } else {
                            HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.nointernet_text));
                        }
                        break;


                }
                myDrawerList.setItemChecked(position, true);
                myMainDrawerLayout.closeDrawer(myDrawerLayout);


            }
        });
    }

    private void showMaintenanceDialog(String aDescriptionStr) {
        final Dialog aPopDialog = new Dialog(HomeActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        aPopDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View aDialogView = inflater.inflate(R.layout.dialog_maintenance, null);
        aPopDialog.setContentView(aDialogView);
        aPopDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        WebView aWebview = (WebView) aPopDialog.findViewById(R.id.dialog_maintenance_WEBVW);
        LinearLayout aCloseLAY = (LinearLayout) aPopDialog.findViewById(R.id.dialog_maintenance_LAY_close);
        CustomButton aSubmitBTN = (CustomButton) aPopDialog.findViewById(R.id.dialog_maintenance_BTN_watchvideo);

        aSubmitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aPopDialog.dismiss();
                getVideoMaintainenceData();
            }
        });
        aCloseLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aPopDialog.dismiss();
            }
        });
        aWebview.loadData(aDescriptionStr, "text/html", "UTF-8");
        aPopDialog.show();
    }

    private void getVideoMaintainenceData() {
        HashMap<String, String> jsonParams = new HashMap<>();
        ServiceRequest mServiceRequest = new ServiceRequest(HomeActivity.this);
        mServiceRequest.makeServiceRequest(ServiceConstant.GETHMP_DETAILS, Request.Method.GET, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                Log.e("hmp", response);

                try {
                    JSONObject jobject = new JSONObject(response);

                    if (jobject.getString("status").equalsIgnoreCase("1")) {
                        JSONObject aResponseObject = jobject.getJSONObject("response");
                        if (aResponseObject.has("popupdescription2") && aResponseObject.has("videourl")) {
                            showVideMaintenanceDialog(aResponseObject.getString("popupdescription2"),
                                    aResponseObject.getString("videourl"));
                        } else if (aResponseObject.has("popupdescription2")) {
                            showVideMaintenanceDialog("", aResponseObject.getString("videourl"));
                        } else if (aResponseObject.has("videourl")) {
                            showVideMaintenanceDialog(aResponseObject.getString("popupdescription2"),
                                    "");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }

    private void showVideMaintenanceDialog(String aDescriptionStr, String aVideoURLStr) {

        final Dialog aPopDialog = new Dialog(HomeActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        aPopDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View aDialogView = inflater.inflate(R.layout.dialog_watchvideo_maintenance, null);
        aPopDialog.setContentView(aDialogView);
        aPopDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        WebView aWebview = (WebView) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_WEBVW);
        LinearLayout aCloseLAY = (LinearLayout) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_LAY_close);
        CustomButton aSubmitBTN = (CustomButton) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_BTN_watchvideo);
        final ImageView aPlayBTN = (ImageView) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_BTN_play);
        final RelativeLayout aVideoLAY = (RelativeLayout) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_LAY_video);
        final ProgressBar aProgressBar = (ProgressBar) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_PRG);

        aPlayerView = (PlayerView)aPopDialog.findViewById(R.id.player_view);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Uri videoUrl = Uri.parse("https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4");

        LoadControl loadControl = new DefaultLoadControl();
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
        aSimpleExoPlayer = ExoPlayerFactory.newSimpleInstance(HomeActivity.this,trackSelector,loadControl);
        DefaultHttpDataSourceFactory factory = new DefaultHttpDataSourceFactory("exoplayer_video");
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        final MediaSource mediaSource = new ExtractorMediaSource(videoUrl,factory,extractorsFactory,null,null);
        aPlayerView.setPlayer(aSimpleExoPlayer);
        aPlayerView.setKeepScreenOn(true);
        aPlayerView.hideController();


        aPlayBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aSimpleExoPlayer.prepare(mediaSource);
                aPlayBTN.setVisibility(View.GONE);
                aPlayerView.showController();
                aSimpleExoPlayer.setPlayWhenReady(true);
            }
        });

        aSimpleExoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == Player.STATE_BUFFERING){
                    aProgressBar.setVisibility(View.VISIBLE);
                }else if (playbackState == Player.STATE_READY){
                    aProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {
            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
            }

            @Override
            public void onPositionDiscontinuity(int reason) {
            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            }

            @Override
            public void onSeekProcessed() {
            }
        });


        /*final ImageView aPauseIMG = (ImageView) aPopDialog.findViewById(R.id.dialog_watchvideo_maintainance_IMG_pause);
        final ImageView aBackwardIMG = (ImageView) aPopDialog.findViewById(R.id.dialog_watchvideo_maintainance_IMG_backward);
        final ImageView aForwardIMG = (ImageView) aPopDialog.findViewById(R.id.dialog_watchvideo_maintainance_IMG_forward);
        final RelativeLayout aController = (RelativeLayout) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_LAY_controller);

        aPlayer = new MediaPlayer();
        final VideoView aVideoView = (VideoView) aPopDialog.findViewById(R.id.dialog_watchvideo_maintenance_VDVW);
        aVideoView.setVideoPath("https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4");

        aBackwardIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int aPosition = aPlayer.getCurrentPosition() - 5000;
                Log.e("aBackwardIMG", "" + aPosition);
                aVideoView.seekTo(aPosition);

            }
        });
        aPlayBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aPlayBTN.setVisibility(View.GONE);
                aProgressBar.setVisibility(View.VISIBLE);
                aVideoView.start();
                if (aVideoView.isPlaying()) {
                    aProgressBar.setVisibility(View.GONE);
                    aController.setVisibility(View.VISIBLE);
                }
            }
        });

        aPauseIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (aVideoView.isPlaying()) {
                    aVideoView.pause();
                    aPauseIMG.setImageResource(R.drawable.icon_play);
                } else {
                    aVideoView.start();
                    aPauseIMG.setImageResource(R.drawable.icon_pause);
                }
            }
        });

        aForwardIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int aPosition = aPlayer.getCurrentPosition() + 5000;
                Log.e("aForwardIMG", "" + aPosition);
                aVideoView.seekTo(aPosition);
            }
        });

        aVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                aPlayer = mediaPlayer;
                aProgressBar.setVisibility(View.GONE);
                aPlayBTN.setVisibility(View.GONE);
                aController.setVisibility(View.VISIBLE);
            }
        });

        aVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                aController.setVisibility(View.GONE);
                aPlayBTN.setVisibility(View.VISIBLE);
            }
        });*/

        // set the uri for the video view

        final String Base_Url = BuildConfig.SERVER_URL + "user_login/";
        aSubmitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Base_Url));
                startActivity(browserIntent);
            }
        });
        aCloseLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aPopDialog.dismiss();
            }
        });
        aWebview.loadData(aDescriptionStr, "text/html", "UTF-8");
        aPopDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        aSimpleExoPlayer.setPlayWhenReady(false);
      //  aSimpleExoPlayer.getPlaybackState();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
      //  aSimpleExoPlayer.setPlayWhenReady(true);
       // aSimpleExoPlayer.getPlaybackState();
    }


    private void shareReportThrGmail(String aGmail, String aMailSubjctSTR, String aMsgBodySTR) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        //  emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{aGmail});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, aMailSubjctSTR);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_TEXT, aMsgBodySTR);
        final PackageManager pm = HomeActivity.this.getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
        ResolveInfo best = null;
        for (final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                best = info;
        if (best != null)
            emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        startActivity(emailIntent);
    }


    private void loadMenuData() {
        myTitles = new String[]{"username", getResources().getString(R.string.navigation_label_home),
                getResources().getString(R.string.navigation_label_my_orders),
                getResources().getString(R.string.navigation_label_transactions),
                getResources().getString(R.string.navigation_label_loyalty),
                getResources().getString(R.string.navigation_label_chat),
                getResources().getString(R.string.navigation_label_notification),
                getResources().getString(R.string.navigation_label_reviews),
                getResources().getString(R.string.navigation_label_invite_friends),
                getResources().getString(R.string.navigation_label_add_card),
                getResources().getString(R.string.navigation_label_maintenance),
                getResources().getString(R.string.navigation_label_report_issues), getResources().getString(R.string.navigation_label_about_us), getResources().getString(R.string.termsco), getResources().getString(R.string.privacypol),
                getResources().getString(R.string.navigation_label_logout)

        };
        myIcons = new int[]{R.drawable.icon_placeholder, R.drawable.icon_home,
                R.drawable.icon_my_order, R.drawable.loyalitybank, R.drawable.icon_wallet,
                R.drawable.icon_message, R.drawable.icon_notification, R.drawable.icon_reviews,
                R.drawable.icon_invite_friends, R.drawable.icon_add_card, R.drawable.icon_maintenance,
                R.drawable.icon_report_issues, R.drawable.icon_information,
                R.drawable.privacypolicyy, R.drawable.privacypolicyy,
                R.drawable.icon_logout};

        myMenuAdapter = new HomeMenuListAdapter(HomeActivity.this, myTitles, myIcons);
        myDrawerList.setAdapter(myMenuAdapter);
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.commit();
    }

    public static void openDrawer() {
        myMainDrawerLayout.openDrawer(myDrawerLayout);
    }

    private void postRequestLogout(String url) {
        myDialog = new ProgressDialogcreated(HomeActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("device_type", "android");

        ServiceRequest mRequest = new ServiceRequest(HomeActivity.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------logout response------------" + response);

                String Str_status = "", Str_message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {
                        mySession.clearPreference();
                        isActivityInBG = false;
                        Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
                        startActivity(intent);
                        finish();


                    } else if (Str_status.equalsIgnoreCase("0")) {
                        Str_message = object.getString("response");

                        final PkDialog mDialog = new PkDialog(HomeActivity.this);
                        mDialog.setDialogTitle("Error");
                        mDialog.setDialogMessage(Str_message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                    }
                                }
                        );

                        mDialog.show();

                    }

                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    public static void NotifyChange() {
        myMenuAdapter.notifyDataSetChanged();
    }


    private void postMode(Context mContext, String url) {

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user", mySession.getUserDetails().getUserId());
        jsonParams.put("user_type", "user");
        jsonParams.put("mode", "gcm");
        jsonParams.put("type", "android");


        ServiceRequest mRequest = new ServiceRequest(mContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equals("1")) {

                        System.out.println("---------mode response------------" + response);

                    } else {


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

            }
        });
    }


    protected void onResume() {
        super.onResume();

        SharedPreferences getcard;
        getcard = HomeActivity.this.getSharedPreferences("getaddress", 0); // 0 - for private mode
        myzipcode = getcard.getString("zipcode", "");


        // Toast.makeText(HomeActivity.this,""+mySession.getLogInStatus(),Toast.LENGTH_LONG).show();

        if (mySession.getLogInStatus() == false) {
            myDrawerLayout.setVisibility(View.GONE);
        } else {


            SharedPreferences prefdd = HomeActivity.this.getSharedPreferences("termsconditionn", MODE_PRIVATE);
            if (prefdd.getString("termsandconditions", "").equals("1")) {

            } else {
                Intent intent = new Intent(HomeActivity.this, FirstTimePrivacy.class);
                intent.putExtra("url", myzipcode);
                intent.putExtra("type", "reg");
                intent.putExtra("classs", "home");
                intent.putExtra("header", getResources().getString(R.string.activity_signup_TXT_signup_terms_use));
                startActivity(intent);
                finish();
            }

            try {

                Intent intent = new Intent(HomeActivity.this, ChatMessageServicech.class);
                startService(intent);


                Intent intenzt = new Intent(HomeActivity.this, ChatMessageService.class);
                startService(intenzt);

            } catch (Exception e) {
                e.printStackTrace();
            }


            SharedPreferences pref = HomeActivity.this.getSharedPreferences("logintoconform", 0); // 0 - for private mode
            String skiplog = pref.getString("lo", "");

            if (skiplog.equalsIgnoreCase("1")) {
                replaceFragment(new HomePageFragment());
            }

            SharedPreferences.Editor editor = pref.edit();
            editor.putString("lo", "0");
            editor.commit();
            editor.apply();

            myDrawerLayout.setVisibility(View.VISIBLE);
            loadMenuData();
            listItemClick();

            try {
                Intent intent = new Intent(HomeActivity.this, ChatMessageService.class);
                startService(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    private void paymentPost(final Context mContext, String url) {


        myDialog = new ProgressDialogcreated(HomeActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();

        jsonParams.put("user_id", mySession.getUserDetails().getUserId());


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "", Str_jobDescription = "", Str_NeedPayment = "", misc_approval_status = "", Str_Currency = "", Str_BtnGroup = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        Str_response = jobject.getString("loyaltymoney");
                        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        // Get the layout inflater
                        LayoutInflater inflater = getLayoutInflater();
                        // Inflate the layout for the dialog
                        // Pass null as the parent view because its going in the dialog layout
                        View RequestPaymentView = inflater.inflate(R.layout.dialog_loyalty_cash, null);

                        // Set the dialog layout
                        builder.setView(RequestPaymentView);
                        builder.create();
                        final AlertDialog alertDialog = builder.show();
                        TextView TextViewLoyaltyCash = RequestPaymentView.findViewById(R.id.TextViewLoyaltyCash);
                        TextViewLoyaltyCash.setText(mySession.getCurrencySymbol() + Str_response);
                        TextView TextViewTermsAndCondition = RequestPaymentView.findViewById(R.id.TextViewTermsAndCondition);
                        TextViewTermsAndCondition.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String url = ServiceConstant.Terms_Conditions_Url;

                                SharedPreferences getcard;
                                getcard = HomeActivity.this.getSharedPreferences("getaddress", 0); // 0 - for private mode
                                String zipcode = getcard.getString("zipcode", "");

                                Intent intent = new Intent(HomeActivity.this, CommonWebView.class);
                                intent.putExtra("url", zipcode);
                                intent.putExtra("type", "reg");
                                intent.putExtra("header", getResources().getString(R.string.activity_signup_TXT_signup_terms_use));
                                startActivity(intent);
                            }
                        });


//                        HNDHelper.showErrorAlert(HomeActivity.this, getResources().getString(R.string.class_home_activity_money) + " " + mySession.getCurrencySymbol() + Str_response);
                    } else {
                        Str_response = jobject.getString("message");
                        HNDHelper.showErrorAlert(HomeActivity.this, Str_response);
                    }


                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }


                myDialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                myDialog.dismiss();
            }
        });
    }


    private class TimeDisplayTimerTask extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    System.out.println("Running inside project----------->");

                    mySession = new SharedPreference(HomeActivity.this);
                    if (mySession.getLogInStatus() == false) {

                    } else {
                        SocketHandler.getInstance(HomeActivity.this).getSocketManager().connect();
                        try {
                            Intent intent = new Intent(HomeActivity.this, ChatMessageServicech.class);
                            startService(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }


    public void forceUpdate() {
        /*PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        new ForceUpdateAsync(currentVersion, HomeActivity.this).execute();*/


        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        new ForceUpdateAsync(currentVersion, getApplicationContext()).execute();


    }

    public class ForceUpdateAsync extends AsyncTask<String, String, JSONObject> {

        private String latestVersion;
        private String currentVersion;
        private Context context;

        public ForceUpdateAsync(String currentVersion, Context context) {
            this.currentVersion = currentVersion;
            this.context = context;
        }

        @Override
        protected JSONObject doInBackground(String... params) {

            try {
                latestVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=com.handypro&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if (latestVersion != null) {
                if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                    showUpdateDialog();
                    // Toast.makeText(context,"update is available.", Toast.LENGTH_LONG).show();
                    // showForceUpdateDialog();
//                    AlertDialog.Builder builder;
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        builder = new AlertDialog.Builder(HomeActivity.this, R.style.AlertDialogCustom);
//                    } else {
//                        builder = new AlertDialog.Builder(HomeActivity.this);
//                    }
//                    builder.setCancelable(true);
//
//                    builder.setTitle("Update Pending...")
//                            .setMessage("New version of app available please update for more option?")
//                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//
//                                    try {
//                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.handypro")));
//                                    } catch (android.content.ActivityNotFoundException anfe) {
//                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.handypro")));
//                                    }
//                                    finish();
//
//                                }
//                            })
////                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
////                                public void onClick(DialogInterface dialog, int which) {
////                                    // do nothing
////                                    //finish();
////                                }
////                            })
//                            .setIcon(android.R.drawable.ic_dialog_alert)
//                            .show();
                }
            }
            super.onPostExecute(jsonObject);
        }

    }

    private void showUpdateDialog() {
        final Dialog aPopDialog = new Dialog(HomeActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        aPopDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View aDialogView = inflater.inflate(R.layout.dialog_update, null);
        aPopDialog.setContentView(aDialogView);
        aPopDialog.setCancelable(false);
        // aPopDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        CustomTextView aUpdateTXT = (CustomTextView) aPopDialog.findViewById(R.id.dialog_update_TXT_update);
        aUpdateTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.handypro")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.handypro")));
                }
                finish();
            }
        });
        aPopDialog.show();
    }
}
