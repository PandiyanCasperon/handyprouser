package com.handypro.activities.navigationMenu;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.ReviewInfoPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.HomeActivity;
import com.handypro.adapters.ReviewsAdapter;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CAS63 on 3/2/2018.
 */

public class ReviewsActivity extends AppCompatActivity {
    private RelativeLayout myBackLAY;
    private ListView myreviewsListVw;
    private ReviewsAdapter myReviewsAdapter;
    private ArrayList<ReviewInfoPojo> myReviewsArrList;
    private MaterialRefreshLayout myRefreshLAY;
    private ConnectionDetector myConnectionManager;
    private LinearLayout myNoInternetLAY, myNoReviewLAY;
    private CustomTextView myRetryConnTXT, myNoRevTXT;
    private ProgressDialogcreated myDialog;
    private SharedPreference mySession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInitialize();

        getReviewData();
    }


    private void getReviewData() {
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestReviewsList(ReviewsActivity.this, ServiceConstant.REVIEW_URL);
        } else {
            checkInternetConnectivity();
        }
    }

    private void classAndWidgetInitialize() {
        mySession = new SharedPreference(ReviewsActivity.this);
        myConnectionManager = new ConnectionDetector(ReviewsActivity.this);
        myReviewsArrList = new ArrayList<ReviewInfoPojo>();
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_reviews_LAY_back);
        myreviewsListVw = (ListView) findViewById(R.id.activity_reviews_listView);
        myRefreshLAY = (MaterialRefreshLayout) findViewById(R.id.activity_reviews_Swipe_layout);
        myNoInternetLAY = (LinearLayout) findViewById(R.id.activity_reviews_no_internetLAY);
        myNoReviewLAY = (LinearLayout) findViewById(R.id.activity_reviews_no_revLAY);
        myRetryConnTXT = (CustomTextView) findViewById(R.id.activity_reviews_retry_TXT);
        myNoRevTXT = (CustomTextView) findViewById(R.id.activity_reviews_no_rev_TXT);
        myNoReviewLAY.setVisibility(View.GONE);

        checkInternetConnectivity();

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        myRefreshLAY.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getReviewData();
                        myRefreshLAY.finishRefresh();
                    }
                }, 750);
            }
        });

        myRetryConnTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getReviewData();
            }
        });

    }

    private void checkInternetConnectivity() {
        if (myConnectionManager.isConnectingToInternet()) {
            myNoInternetLAY.setVisibility(View.GONE);
            myRefreshLAY.setVisibility(View.VISIBLE);
        } else {
            myNoInternetLAY.setVisibility(View.VISIBLE);
            myRefreshLAY.setVisibility(View.GONE);
        }
    }

    private void postRequestReviewsList(final Context aContext, String url) {
        myDialog = new ProgressDialogcreated(aContext);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("role", "user");

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Reviews List Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    JSONObject aDataObj = object.getJSONObject("data");
                    if (aDataObj.getString("status").equalsIgnoreCase("1")) {

                        JSONObject response_Object = aDataObj.getJSONObject("response");
                        if (response_Object.length() > 0) {
                            JSONArray aJobsArray = response_Object.getJSONArray("reviews");
                            if (aJobsArray.length() > 0) {
                                myReviewsArrList.clear();
                                for (int i = 0; i < aJobsArray.length(); i++) {
                                    JSONObject aJsonObject = aJobsArray.getJSONObject(i);


                                    ReviewInfoPojo aRevInfo = new ReviewInfoPojo();
                                    aRevInfo.setReviewBookingId(aJsonObject.getString("booking_id"));

                                    String service = "";
                                    JSONArray service_type = aJsonObject.getJSONArray("category");
                                    for (int b = 0; b < service_type.length(); b++) {

                                        String value = "" + service_type.getString(b);


                                        service += value + ",";
                                    }

                                    if (service.endsWith(",")) {
                                        service = service.substring(0, service.length() - 1);
                                    }


                                    aRevInfo.setReviewCategory(service);
                                    aRevInfo.setReviewComments(aJsonObject.getString("comments"));
                                    aRevInfo.setReviewDate(aJsonObject.getString("date"));
                                    aRevInfo.setReviewRating(aJsonObject.getString("rating"));
                                    aRevInfo.setReviewTasker(aJsonObject.getString("tasker_name"));
                                    aRevInfo.setReviewImage(aJsonObject.getString("tasker_image"));

                                    myReviewsArrList.add(aRevInfo);
                                }
                            }
                        }


                        if (myReviewsArrList.size() > 0) {
                            myReviewsAdapter = new ReviewsAdapter(ReviewsActivity.this, myReviewsArrList);
                            myreviewsListVw.setAdapter(myReviewsAdapter);
                        }


                    } else {
                        //HNDHelper.showResponseErrorAlert(aContext, aDataObj.getString("response"));
                        myreviewsListVw.setVisibility(View.GONE);
                        myNoReviewLAY.setVisibility(View.VISIBLE);
                        myNoRevTXT.setText(aDataObj.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

}

