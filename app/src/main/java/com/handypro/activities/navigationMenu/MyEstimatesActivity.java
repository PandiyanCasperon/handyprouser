package com.handypro.activities.navigationMenu;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.handypro.R;

/**
 * Created by CAS63 on 3/2/2018.
 */

public class MyEstimatesActivity extends AppCompatActivity {
    private RelativeLayout myBackLAY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_estimates);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInitialize();
    }

    private void classAndWidgetInitialize() {
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_my_estimates_LAY_back);

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
