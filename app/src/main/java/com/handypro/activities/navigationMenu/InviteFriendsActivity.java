package com.handypro.activities.navigationMenu;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.CurrencySymbolConverter;
import com.handypro.volley.ServiceRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by CAS63 on 2/26/2018.
 */

public class InviteFriendsActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout myBackLAY;
    private LinearLayout myFacebookLAY, mySMSLay, myMessengerLAY, myWhatsappLAY, myTwitterLAY, myEmailLAY, LinearLayoutReferLink;
    private ConnectionDetector myConnectionManager;
    private ProgressDialogcreated myDialog;
    private SharedPreference mySession;
    final int PERMISSION_REQUEST_CODES = 143;
    private String myCurrencySymbolSTR = "$", myEarningAmtSTR = "300", myFriendsEarningAmtSTR = "90", myReferalCodeSTR = "SFSHFDSF123", myShareContentSTR = "";
    private boolean isDataPresent = false;
    private String sCurrencySymbol = "";
    private String sStatus = "", Fburl = "", sFriend_earn_amount = "", sYou_earn_amount = "", sFriends_rides = "", sCurrencyCode = "", sReferral_code = "";
    private TextView Tv_friends_earn, Tv_you_earn, Tv_referral_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);
        classAndWidgetInitialize();
        getData();
    }

    private void getData() {
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestInviteFriends(InviteFriendsActivity.this, ServiceConstant.invite_earn_friends_url);
        } else {
            HNDHelper.showErrorAlert(InviteFriendsActivity.this, getResources().getString(R.string.nointernet_text));
        }
    }

    private void classAndWidgetInitialize() {
        /*myShareContentSTR = getResources().getString(R.string.activity_invite_friends_message1) + " " + myCurrencySymbolSTR + " " + myEarningAmtSTR +
                getResources().getString(R.string.activity_invite_friends_message2) + " " + " " + myCurrencySymbolSTR + "" + myFriendsEarningAmtSTR + " " + getResources().getString(R.string.activity_invite_friends_message3) + " " + '"' + myReferalCodeSTR + '"' + " " + getResources().getString(R.string.activity_invite_friends_in_your_wallet) + ServiceConstant.Base_Url;*/

        myConnectionManager = new ConnectionDetector(InviteFriendsActivity.this);
        mySession = new SharedPreference(InviteFriendsActivity.this);
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_invite_friends_LAY_back);
        myFacebookLAY = (LinearLayout) findViewById(R.id.activity_invite_friends_facebook_lay);
        myMessengerLAY = (LinearLayout) findViewById(R.id.activity_invite_friends_messenger_lay);
        myWhatsappLAY = (LinearLayout) findViewById(R.id.activity_invite_friends_whatsapp_lay);
        mySMSLay = (LinearLayout) findViewById(R.id.activity_invite_friends_sms_lay);
        myTwitterLAY = (LinearLayout) findViewById(R.id.activity_invite_friends_twitter_lay);
        myEmailLAY = (LinearLayout) findViewById(R.id.activity_invite_friends_mail_lay);
        Tv_friends_earn = (TextView) findViewById(R.id.Tv_friends_earn);
        Tv_you_earn = (TextView) findViewById(R.id.Tv_you_earn);
        Tv_referral_code = (TextView) findViewById(R.id.Tv_referral_code);
        LinearLayoutReferLink = (LinearLayout) findViewById(R.id.LinearLayoutReferLink);

        myBackLAY.setOnClickListener(this);
        myFacebookLAY.setOnClickListener(this);
        myMessengerLAY.setOnClickListener(this);
        myWhatsappLAY.setOnClickListener(this);
        mySMSLay.setOnClickListener(this);
        myTwitterLAY.setOnClickListener(this);
        myEmailLAY.setOnClickListener(this);

        LinearLayoutReferLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", sReferral_code);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(InviteFriendsActivity.this, "Referral code copied to Clipboard", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void postRequestInviteFriends(final Context aContext, String url) {
        myDialog = new ProgressDialogcreated(aContext);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("username", "abc");

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------invite friends Response----------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        Object check_response_object = object.get("response");
                        if (check_response_object instanceof JSONObject) {
                            JSONObject response_Object = object.getJSONObject("response");
                            if (response_Object.length() > 0) {
                                Object check_details_object = response_Object.get("details");
                                if (check_details_object instanceof JSONObject) {
                                    JSONObject detail_object = response_Object.getJSONObject("details");
                                    if (detail_object.length() > 0) {
                                        sFriend_earn_amount = detail_object.getString("friends_earn_amount");
                                        sYou_earn_amount = detail_object.getString("your_earn_amount");
                                        sFriends_rides = detail_object.getString("your_earn");
                                        sReferral_code = detail_object.getString("referral_code");
                                        sCurrencyCode = detail_object.getString("currency");
                                        Fburl = detail_object.getString("link");

                                        isDataPresent = true;
                                        /*String mapLocation = String.format(getString(R.string.link_to_google_map), 28.0500, 75.1487);
                                        Html.fromHtml(mapLocation);*/
                                        myShareContentSTR = getResources().getString(R.string.activity_invite_friends_message1) + " " + CurrencySymbolConverter.getCurrencySymbol(sCurrencyCode) + "" + sFriend_earn_amount + ". " +
                                                getResources().getString(R.string.activity_invite_friends_message2) + " " + '"' + sReferral_code + '"' + ". " + getResources().getString(R.string.activity_invite_friends_message3) + " " +
                                                ServiceConstant.AndroidReferLink + getResources().getString(R.string.activity_invite_friends_message4) + " " + ServiceConstant.IOSReferLink;
                                    } else {
                                        isDataPresent = false;
                                    }
                                } else {
                                    isDataPresent = false;
                                }
                            } else {
                                isDataPresent = false;
                            }
                        } else {
                            isDataPresent = false;
                        }

                        if (isDataPresent) {
                            sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(sCurrencyCode);

                            Tv_friends_earn.setText(getResources().getString(R.string.invite_earn_label_friends_earn) + " " + sCurrencySymbol + "" + sFriend_earn_amount);
                            Tv_you_earn.setText(getResources().getString(R.string.invite_earn_label_friends_ride) + "," + getResources().getString(R.string.invite_earn_label_friend_ride) + " " + sCurrencySymbol + "" + sYou_earn_amount);
                            Tv_referral_code.setText(sReferral_code);
                        }
                    } else {
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.activity_invite_friends_LAY_back:
                onBackPressed();
                finish();
                break;

            case R.id.activity_invite_friends_facebook_lay:
                sendFacebookInvite(myShareContentSTR);
                break;

            case R.id.activity_invite_friends_whatsapp_lay:
                sendWhatsAppInvite(myShareContentSTR);
                break;

            case R.id.activity_invite_friends_messenger_lay:
                sendMessengerInvite(myShareContentSTR);
                break;

            case R.id.activity_invite_friends_sms_lay:
                sendSMSInvite(myShareContentSTR);
                break;

            case R.id.activity_invite_friends_twitter_lay:
                Uri imageUri = null;
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                            BitmapFactory.decodeResource(getResources(), R.drawable.icon_logo_color), null, null));
                } catch (NullPointerException e) {
                }

                sendTwitterInvite(myShareContentSTR, imageUri);
                break;

            case R.id.activity_invite_friends_mail_lay:
                sendMailInvite(myShareContentSTR);
                break;

        }
    }

    private void sendMailInvite(String aContentToShare) {
        String[] TO = {""};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.activity_invite_friends_label_app_invitation));
        emailIntent.putExtra(Intent.EXTRA_TEXT, aContentToShare);
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            HNDHelper.showResponseErrorAlert(InviteFriendsActivity.this, getResources().getString(R.string.activity_invite_friends_no_mail_installed));

        }
    }

    /**
     * invite through twitter
     *
     * @param aContectToShare
     * @param aImage
     */
    private void sendTwitterInvite(String aContectToShare, Uri aImage) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, aContectToShare);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_STREAM, aImage);
        intent.setType("image/jpeg");
        intent.setPackage("com.twitter.android");

        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            HNDHelper.showResponseErrorAlert(InviteFriendsActivity.this, getResources().getString(R.string.activity_invite_friends_no_twitter_installed));
        }
    }

    /**
     * invite through facebook
     *
     * @param aContentToShare
     */
    private void sendFacebookInvite(String aContentToShare) {
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, aContentToShare);
        Intent pacakage2 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
        Intent pacakage3 = getPackageManager().getLaunchIntentForPackage("com.example.facebook");
        Intent pacakage4 = getPackageManager().getLaunchIntentForPackage("com.facebook.android");
        if (pacakage2 != null) {
            intent.setPackage("com.facebook.katana");
        } else if (pacakage3 != null) {
            intent.setPackage("com.facebook.facebook");
        } else if (pacakage4 != null) {
            intent.setPackage("com.facebook.android");
        } else {
            intent.setPackage("com.facebook.orca");
        }

        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            HNDHelper.showResponseErrorAlert(InviteFriendsActivity.this, getResources().getString(R.string.activity_invite_friends_no_facebook_installed));
        }
    }

    /**
     * invite friends through messenger if installed
     *
     * @param aContentToShare
     */
    private void sendMessengerInvite(String aContentToShare) {
        PackageManager pm = InviteFriendsActivity.this.getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.facebook.orca", PackageManager.GET_META_DATA);  //Check if package exists or not. If not then coding catch block will be called
            waIntent.setPackage("com.facebook.orca");
            waIntent.putExtra(Intent.EXTRA_TEXT, aContentToShare);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            HNDHelper.showResponseErrorAlert(InviteFriendsActivity.this, getResources().getString(R.string.activity_invite_friends_no_messenger_installed));

        }
    }

    /**
     * invite friends through whatsapp
     *
     * @param aContentToShare
     */
    private void sendWhatsAppInvite(String aContentToShare) {
        PackageManager pm = InviteFriendsActivity.this.getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);  //Check if package exists or not. If not then codein catch block will be called
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, aContentToShare);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            HNDHelper.showResponseErrorAlert(InviteFriendsActivity.this, getResources().getString(R.string.activity_invite_friends_no_whatsapp_installed));
        }
    }

    /**
     * send invite through sms
     *
     * @param aContentToShare
     */
    private void sendSMSInvite(String aContentToShare) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:"));
        sendIntent.putExtra("sms_body", aContentToShare);
        startActivity(sendIntent);
    }

    private void requestPermissionSMS() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, PERMISSION_REQUEST_CODES);
    }

    private boolean checkSmsPermission() {
        int result = ContextCompat.checkSelfPermission(InviteFriendsActivity.this, Manifest.permission.SEND_SMS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

}
