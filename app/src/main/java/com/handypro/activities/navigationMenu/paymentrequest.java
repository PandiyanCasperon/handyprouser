package com.handypro.activities.navigationMenu;

/**
 * Created by user127 on 02-04-2018.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.handypro.Dialog.internetmissing;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.PaymentFareSummeryPojo;
import com.handypro.Pojo.User_pre_amount_list;
import com.handypro.Pojo.newimte;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.PaymentPageActivity;
import com.handypro.activities.CheckedLoyalty;
import com.handypro.activities.listhei;
import com.handypro.activities.zoomMainActivity;
import com.handypro.adapters.MultiplePaymentRequestAdapter;
import com.handypro.adapters.PaymentFareSummeryAdapter;
import com.handypro.adapters.whatsadapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.CurrencySymbolConverter;
import com.handypro.volley.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.logging.SocketHandler;

import static android.view.View.VISIBLE;


/**
 * Created by user88 on 1/8/2016.
 */
public class paymentrequest extends AppCompatActivity {
    private ConnectionDetector cd;
    SharedPreferences pref;
    List<newimte> signmovieList = new ArrayList<>();
    private ProgressDialogcreated myDialog;
    private Boolean isInternetPresent = false;
    private boolean show_progress_status = false;
    private Handler mHandler;
    int approval_status = 0;
    String loyaltymoney = "0", loyaltymoneystatus = "0", loyalityadded = "0";
    String donate = "0", tips = "0", currencyCode;
    ArrayList<String> categorynameinarray;
    ArrayList<String> whatsincludedinarray;
    private String provider_id = "";
    private listhei fare_listview, servicelistview;
    private int PreAmount = 0;
    Double addallforloyality = 0.0;


    LinearLayout capturesignaturelinear, LinearLayoutPrepaymentView;
    RecyclerView caprecycler_view;
    TextView captext;


    private TextView Tv_JobId, Tv_JobDescription;
    private String asyntask_name = "normal";

    PaymentFareSummeryAdapter adapter;
    private ArrayList<PaymentFareSummeryPojo> farelist, TempFareList;
    private SharedPreference mySession;

    TextView Tvpaymentfare_jobId, TextViewPrepaymentCount;
    private String Job_id = "";
    private boolean isPaymetFare = false;
    LinearLayout LinearLayoutDonate, LinearLayoutTips;

    private RelativeLayout Rl_layout_back;
    private Button Bt_Request_payment, ButtonRejectAll;
    private SocketHandler socketHandler;
    TextView hintshow;
    CheckBox tipscheck_box, donatecheck_box;
    EditText tipsamount, donateamount;
    TextView jonspecialin, tellabout, whatsimportant, Whatsincludedtext, whats;
    RecyclerView RecyclerViewMultiplePaymentRequest;
    String refunamout = "0", Flow = "";
    private List<User_pre_amount_list> MultiplePrepaymentSelectedFromAdapterList;
    double TotalPrepaymentAmount;
    boolean isContainPendingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paymentrequestpage);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        categorynameinarray = new ArrayList<String>();
        whatsincludedinarray = new ArrayList<String>();


        SharedPreferences pref = getApplicationContext().getSharedPreferences("donatetips", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
        editor.commit();

        mySession = new SharedPreference(paymentrequest.this);
        pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
        Job_id = pref.getString("jobid", "");

        if (getIntent().hasExtra("Type")) {
            Flow = getIntent().getStringExtra("Type");
        }
        initialize();
        Rl_layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


    }

    private void initialize() {

        cd = new ConnectionDetector(paymentrequest.this);
        mHandler = new Handler();

        isInternetPresent = cd.isConnectingToInternet();
        // get user data from session

        farelist = new ArrayList<PaymentFareSummeryPojo>();
        TempFareList = new ArrayList<PaymentFareSummeryPojo>();

        hintshow = (TextView) findViewById(R.id.hintshow);


        captext = (TextView) findViewById(R.id.captext);
        capturesignaturelinear = (LinearLayout) findViewById(R.id.capturesignaturelinear);
        caprecycler_view = (RecyclerView) findViewById(R.id.caprecycler_view);

        captext.setPaintFlags(captext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        servicelistview = (listhei) findViewById(R.id.servicelistview);

        jonspecialin = (TextView) findViewById(R.id.jonspecialin);
        jonspecialin.setPaintFlags(jonspecialin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        whatsimportant = (TextView) findViewById(R.id.whatsimportant);
        tellabout = (TextView) findViewById(R.id.tellabout);
        jonspecialin.setVisibility(View.GONE);
        whatsimportant.setVisibility(View.GONE);
        tellabout.setVisibility(View.GONE);
        whats = (TextView) findViewById(R.id.whats);
        Whatsincludedtext = (TextView) findViewById(R.id.Whatsincludedtext);
        Whatsincludedtext.setPaintFlags(Whatsincludedtext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        RecyclerViewMultiplePaymentRequest = (RecyclerView) findViewById(R.id.RecyclerViewMultiplePaymentRequest);
        LinearLayoutManager llm = new LinearLayoutManager(paymentrequest.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        RecyclerViewMultiplePaymentRequest.setLayoutManager(llm);
        RecyclerViewMultiplePaymentRequest.setHasFixedSize(true);
        TextView jobinvoice = (TextView) findViewById(R.id.jobinvoice);
        jobinvoice.setPaintFlags(jobinvoice.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        Tvpaymentfare_jobId = (TextView) findViewById(R.id.Tvpaymentfare_jobId);
        fare_listview = (listhei) findViewById(R.id.cancelreason_listView);
        Tv_JobId = (TextView) findViewById(R.id.paymentfare_jobId_);
        Tv_JobDescription = (TextView) findViewById(R.id.Tvpaymentfare_job_description);

        Bt_Request_payment = (Button) findViewById(R.id.Bt_faresummery_requestpaymet);

        Rl_layout_back = (RelativeLayout) findViewById(R.id.layout_jobfare_back);

        tipscheck_box = (CheckBox) findViewById(R.id.tipscheck_box);
        donatecheck_box = (CheckBox) findViewById(R.id.donatecheck_box);

        donateamount = (EditText) findViewById(R.id.donateamount);
        tipsamount = (EditText) findViewById(R.id.tipsamount);
        LinearLayoutTips = (LinearLayout) findViewById(R.id.LinearLayoutTips);
        LinearLayoutDonate = (LinearLayout) findViewById(R.id.LinearLayoutDonate);
        LinearLayoutPrepaymentView = (LinearLayout) findViewById(R.id.LinearLayoutPrepaymentView);
        TextViewPrepaymentCount = (TextView) findViewById(R.id.TextViewPrepaymentCount);
        TextViewPrepaymentCount.setPaintFlags(TextViewPrepaymentCount.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        ButtonRejectAll = (Button) findViewById(R.id.ButtonRejectAll);

        Bt_Request_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isInternetPresent) {
                    if (Flow.equals("requesting_pre_payment")) {
                        if (MultiplePrepaymentSelectedFromAdapterList != null && MultiplePrepaymentSelectedFromAdapterList.size() > 0) {

                            ArrayList<String> IdList = new ArrayList<>();
                            ArrayList<String> ValueList = new ArrayList<>();
                            for (int i = 0; i < MultiplePrepaymentSelectedFromAdapterList.size(); i++) {
                                IdList.add(MultiplePrepaymentSelectedFromAdapterList.get(i).get_id());
                                ValueList.add(MultiplePrepaymentSelectedFromAdapterList.get(i).getPre_amount());
                                TotalPrepaymentAmount = TotalPrepaymentAmount + Double.parseDouble(MultiplePrepaymentSelectedFromAdapterList.get(i).getPre_amount());
                            }
                            Intent aAddCardIntent = new Intent(paymentrequest.this, PaymentPageActivity.class);
                            aAddCardIntent.putExtra("loyaltymoneystatus", "" + loyaltymoneystatus);
                            aAddCardIntent.putExtra("loyaltymoney", "" + loyaltymoney);
                            aAddCardIntent.putExtra("Type", "requesting_pre_payment");
                            aAddCardIntent.putStringArrayListExtra("IdList", IdList);
                            aAddCardIntent.putStringArrayListExtra("ValueList", ValueList);
                            aAddCardIntent.putExtra("TotalPrepaymentAmount", TotalPrepaymentAmount);
                            startActivity(aAddCardIntent);
                            finish();
                        } else {
                            Toast.makeText(paymentrequest.this, "Select at least single prepayment list", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        approval_status = 1;
                        if (donatecheck_box.isChecked()) {
                            if (donateamount.getText().toString().equalsIgnoreCase("") || donateamount.getText().toString().equalsIgnoreCase("0") || donateamount.getText().toString().equalsIgnoreCase(".")) {
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("donatetips", MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("donateamt", "");
                                editor.apply();
                                editor.commit();
                            } else {
                                Double dondo = Double.parseDouble(donateamount.getText().toString());
                                addallforloyality = addallforloyality + dondo;

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("donatetips", MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("donateamt", donateamount.getText().toString());
                                editor.apply();
                                editor.commit();
                            }

                        } else {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("donatetips", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("donateamt", "");
                            editor.apply();
                            editor.commit();
                        }

                        if (tipscheck_box.isChecked()) {
                            if (tipsamount.getText().toString().equalsIgnoreCase("") || tipsamount.getText().toString().equalsIgnoreCase("0") || tipsamount.getText().toString().equalsIgnoreCase(".")) {
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("donatetips", MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("tipsamt", "");
                                editor.apply();
                                editor.commit();
                            } else {
                                Double tipdo = Double.parseDouble(tipsamount.getText().toString());
                                addallforloyality = addallforloyality + tipdo;

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("donatetips", MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("tipsamt", tipsamount.getText().toString());
                                editor.apply();
                                editor.commit();
                            }
                        } else {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("donatetips", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("tipsamt", "");
                            editor.apply();
                            editor.commit();
                        }


                        if (addallforloyality > 0 || addallforloyality > 0.0) {

                            //  Toast.makeText(getApplicationContext(),"-->"+addallforloyality,Toast.LENGTH_LONG).show();

                            if (loyaltymoneystatus.equalsIgnoreCase("1")) {
                                SpannableStringBuilder builders = new SpannableStringBuilder();
                                String string1 = "Your HandyPro" + " ";
                                SpannableString whiteSpannable = new SpannableString(string1);
                                whiteSpannable.setSpan(new ForegroundColorSpan(paymentrequest.this.getResources().getColor(R.color.black_color)), 0, string1.length(), 0);
                                builders.append(whiteSpannable);

//                                String string2 = "Pro ";
//                                SpannableString redSpannable1 = new SpannableString(string2);
//                                redSpannable1.setSpan(new ForegroundColorSpan(paymentrequest.this.getResources().getColor(R.color.colorPrimary)), 0, string2.length(), 0);
//                                builders.append(redSpannable1);

                                String string3 = "Loyalty Cash of "+ mySession.getCurrencySymbol() + loyaltymoney + "  is available. Want to use it?";
                                SpannableString redSpannable2 = new SpannableString(string3);
                                redSpannable2.setSpan(new ForegroundColorSpan(paymentrequest.this.getResources().getColor(R.color.black_color)), 0, string3.length(), 0);
                                builders.append(redSpannable2);


                                final CheckedLoyalty mDialog = new CheckedLoyalty(paymentrequest.this);
                                mDialog.setDialogTitle(builders, TextView.BufferType.SPANNABLE);
                                mDialog.setPositiveButton(
                                        getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                Intent aAddCardIntent = new Intent(paymentrequest.this, PaymentPageActivity.class);
                                                aAddCardIntent.putExtra("loyaltymoneystatus", "" + loyaltymoneystatus);
                                                aAddCardIntent.putExtra("loyaltymoney", "" + loyaltymoney);
                                                startActivity(aAddCardIntent);
                                                finish();

                                                mDialog.dismiss();

                                            }
                                        }
                                );

                                mDialog.setNegativeButton(
                                        getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mDialog.dismiss();
                                                Intent aAddCardIntent = new Intent(paymentrequest.this, PaymentPageActivity.class);
                                                aAddCardIntent.putExtra("loyaltymoneystatus", "0");
                                                aAddCardIntent.putExtra("loyaltymoney", "0");
                                                startActivity(aAddCardIntent);
                                                finish();

                                            }
                                        }
                                );

                                mDialog.show();
                            } else {
                                Intent aAddCardIntent = new Intent(paymentrequest.this, PaymentPageActivity.class);
                                aAddCardIntent.putExtra("loyaltymoneystatus", "0");
                                aAddCardIntent.putExtra("loyaltymoney", "0");
                                startActivity(aAddCardIntent);
                                finish();
                            }
                        } else {
                            Intent aAddCardIntent = new Intent(paymentrequest.this, PaymentPageActivity.class);
                            aAddCardIntent.putExtra("loyaltymoneystatus", "0");
                            aAddCardIntent.putExtra("loyaltymoney", "0");
                            startActivity(aAddCardIntent);
                            finish();
                        }


//                    /*if(loyalityadded.equalsIgnoreCase("1"))
//                    {
//                        Intent aAddCardIntent = new Intent(paymentrequest.this, PaymentPageActivity.class);
//                        aAddCardIntent.putExtra("loyaltymoneystatus","0");
//                        aAddCardIntent.putExtra("loyaltymoney","0");
//                        startActivity(aAddCardIntent);
//                        finish();
//                    }
//                    else
//                    {
//                        if(loyaltymoneystatus.equalsIgnoreCase("1"))
//                        {
//                            final chekedloyality mDialog = new chekedloyality(paymentrequest.this);
//                            mDialog.setDialogTitle("Loayalty money of "+mySession.getCurrencySymbol()+loyaltymoney+" is available want to use it?");
//                            mDialog.setPositiveButton(
//                                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//
//                                            Intent aAddCardIntent = new Intent(paymentrequest.this, PaymentPageActivity.class);
//                                            aAddCardIntent.putExtra("loyaltymoneystatus",""+loyaltymoneystatus);
//                                            aAddCardIntent.putExtra("loyaltymoney",""+loyaltymoney);
//                                            startActivity(aAddCardIntent);
//                                            finish();
//
//                                            mDialog.dismiss();
//
//                                        }
//                                    }
//                            );
//
//                            mDialog.setNegativeButton(
//                                    getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//                                            mDialog.dismiss();
//                                            Intent aAddCardIntent = new Intent(paymentrequest.this, PaymentPageActivity.class);
//                                            aAddCardIntent.putExtra("loyaltymoneystatus","0");
//                                            aAddCardIntent.putExtra("loyaltymoney","0");
//                                            startActivity(aAddCardIntent);
//                                            finish();
//
//                                        }
//                                    }
//                            );
//
//                            mDialog.show();
//                        }
//                        else
//                        {
//                            Intent aAddCardIntent = new Intent(paymentrequest.this, PaymentPageActivity.class);
//                            aAddCardIntent.putExtra("loyaltymoneystatus","0");
//                            aAddCardIntent.putExtra("loyaltymoney","0");
//                            startActivity(aAddCardIntent);
//                            finish();
//                        }
//                    }
//*/
                    }
                } else {
                    final internetmissing mDialog = new internetmissing(paymentrequest.this);
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            }
                    );

                    mDialog.show();
                }
            }
        });


        if (isInternetPresent) {
            paymentPost(paymentrequest.this, ServiceConstant.PAYMENT_URL);
            if (Flow.equals("requesting_pre_payment")) {
                PrepaymentRequest(ServiceConstant.PrepaymentRequestList);
            } else {
                LinearLayoutTips.setVisibility(View.VISIBLE);
                LinearLayoutDonate.setVisibility(View.VISIBLE);
                LinearLayoutPrepaymentView.setVisibility(View.GONE);
            }

        } else {
            final internetmissing mDialog = new internetmissing(paymentrequest.this);
            mDialog.setPositiveButton(
                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    }
            );

            mDialog.show();
        }

        ButtonRejectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isContainPendingList = false;
                if (Flow.equals("requesting_pre_payment")) {
                    if (MultiplePrepaymentSelectedFromAdapterList != null && MultiplePrepaymentSelectedFromAdapterList.size() >= 2) {
                        new AlertDialog.Builder(paymentrequest.this)
                                .setTitle("Reject")
                                .setMessage("Are you sure want to reject all the prepayments?")

                                // Specifying a listener allows you to take an action before dismissing the dialog.
                                // The dialog is automatically dismissed when a dialog button is clicked.
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        startLoading();
                                        HashMap<String, String> params = new HashMap<>();
                                        params.put("job_id", Job_id);
                                        //We are going to reject all the list which are selected by user in check box.
                                        for (int i = 0; i < MultiplePrepaymentSelectedFromAdapterList.size(); i++) {
                                            if (MultiplePrepaymentSelectedFromAdapterList.get(i).getPayment_status().equals("Pending")) {
                                                isContainPendingList = true;
                                                params.put("est[" + i + "][prepayment_id]", MultiplePrepaymentSelectedFromAdapterList.get(i).get_id());
                                                params.put("est[" + i + "][prepayment_status]", "Rejected");
                                            }
                                        }
                                        if (isContainPendingList) {
                                            ServiceRequest mservicerequest = new ServiceRequest(paymentrequest.this);
                                            mservicerequest.makeServiceRequest(ServiceConstant.PrepaymentRequestListUpdate, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                                                @Override
                                                public void onCompleteListener(String response) {
                                                    stopLoading();
                                                    Log.e("pre_amount/list", response);
                                                    try {
                                                        JSONObject jobject = new JSONObject(response);
                                                        if (jobject.has("status") && jobject.getString("status").equals("1")) {
                                                            //If we get the response as success means refresh the list to get updated value from server
                                                            //So I'm using the PrepaymentRequest function to do it.
                                                            PrepaymentRequest(ServiceConstant.PrepaymentRequestList);
                                                        } else {
                                                            Toast.makeText(paymentrequest.this, "Something went wrong! Try again", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                                @Override
                                                public void onErrorListener() {
                                                    stopLoading();
                                                }
                                            });
                                        } else {
                                            Toast.makeText(paymentrequest.this, "There is no pending list to reject!!!", Toast.LENGTH_SHORT).show();
                                            stopLoading();
                                        }
                                    }
                                })

                                // A null listener allows the button to dismiss the dialog and take no further action.
                                .setNegativeButton(android.R.string.no, null)
                                .setIcon(R.mipmap.handy_app_icon)
                                .show();
                    }
                }
            }
        });

    }


    //----------------------Post method for Payment Fare------------
    private void paymentPost(Context mContext, String url) {


        myDialog = new ProgressDialogcreated(paymentrequest.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();

        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("job_id", Job_id);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "", Str_jobDescription = "", user_approval_status = "", Str_NeedPayment = "", misc_approval_status = "", Str_Currency = "", Str_BtnGroup = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject object = jobject.getJSONObject("response");
                        JSONObject object2 = object.getJSONObject("job");
                        Str_jobDescription = object2.getString("job_summary");
                        Str_NeedPayment = object2.getString("need_payment");
                        Str_Currency = object2.getString("currency");

                        loyaltymoney = object2.getString("loyaltymoney");
                        loyaltymoneystatus = object2.getString("loyaltymoneystatus");

                        if (jobject.has("loyaltyused")) {
                            loyalityadded = jobject.getString("loyaltyused");
                        } else {
                            loyalityadded = "0";
                        }


                        if (object2.has("donate")) {
                            donate = object2.getString("donate");
                            donateamount.setText(donate);
                            donatecheck_box.setChecked(true);
                        }


                        if (object2.has("tips")) {
                            tips = object2.getString("tips");
                            tipsamount.setText(tips);
                            tipscheck_box.setChecked(true);
                        }


                        signmovieList.clear();
                        if (object2.has("signatures")) {

                            if (object2.isNull("signatures")) {

                            } else {
                                String signaturesjj = object2.getString("signatures");

                                JSONObject signaturesjjobj = new JSONObject(signaturesjj);
                                if (signaturesjjobj.has("changeorder")) {
                                    String changeorder = signaturesjjobj.getString("changeorder");
                                    if (changeorder.equals("") || changeorder.equals(" ")) {
                                    } else {
                                        newimte beforemovie = new newimte(ServiceConstant.Base_Url + changeorder, "Change Order");
                                        signmovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("startjob")) {
                                    String startjob = signaturesjjobj.getString("startjob");
                                    if (startjob.equals("") || startjob.equals(" ")) {
                                    } else {
                                        newimte beforemovie = new newimte(/*ServiceConstant.Base_Url+*/startjob, "Start Job");
                                        signmovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("completejob")) {
                                    String completejob = signaturesjjobj.getString("completejob");
                                    if (completejob.equals("") || completejob.equals(" ")) {
                                    } else {
                                        newimte beforemovie = new newimte(/*ServiceConstant.Base_Url+*/completejob, "Complete Job");
                                        signmovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("estimateApproval")) {
                                    String estimateApproval = signaturesjjobj.getString("estimateApproval");
                                    if (estimateApproval.equals("") || estimateApproval.equals(" ")) {
                                    } else {
                                        newimte beforemovie = new newimte(/*ServiceConstant.Base_Url+*/estimateApproval, "Estimate Approval");
                                        signmovieList.add(beforemovie);
                                    }
                                }


                                if (signmovieList.size() > 0) {
                                    capturesignaturelinear.setVisibility(View.VISIBLE);
                                    LinearLayoutManager layoutManager
                                            = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                    neww mAdapter = new neww(signmovieList, getApplicationContext());
                                    caprecycler_view.setLayoutManager(layoutManager);
                                    caprecycler_view.setAdapter(mAdapter);
                                } else {
                                    capturesignaturelinear.setVisibility(View.GONE);
                                    caprecycler_view.setVisibility(View.GONE);
                                }


                            }
                        }


                        misc_approval_status = object2.getString("misc_approval_status");

                        if (object2.has("instructions")) {
                            whatsimportant.setText("Instruction:" + object2.getString("instructions"));
                            tellabout.setText("Description:" + object2.getString("job_description"));
                        }


                        JSONArray categoryinclude = object2.getJSONArray("category");

                        String storewhatsincluded = "";
                        for (int z = 0; z < categoryinclude.length(); z++) {
                            JSONObject getarrayvaluex = categoryinclude.getJSONObject(z);
                            String name = "" + getarrayvaluex.getString("name");

                            if (getarrayvaluex.has("whatsincluded")) {
                                String whatsincluded = "" + getarrayvaluex.getString("whatsincluded");
                               /* if(categoryinclude.length()==(z+1))
                                {
                                    storewhatsincluded += name + "-" + whatsincluded ;
                                }
                                else
                                {
                                    storewhatsincluded += name + "-" + whatsincluded + "\n";
                                }*/

                                categorynameinarray.add(name);
                                whatsincludedinarray.add(whatsincluded);

                            } else {/*
                                if(categoryinclude.length()==(z+1))
                                {
                                    storewhatsincluded += name  ;
                                }
                                else
                                {
                                    storewhatsincluded += name + "\n";
                                }*/
                                categorynameinarray.add(name);
                                whatsincludedinarray.add("");

                            }


                        }
                        // whats.setText(storewhatsincluded);


                        // Currency currencycode = Currency.getInstance(getLocale(Str_Currency));

                        if (object2.has("user_pre_amount")) {
                            JSONArray user_pre_amount_obj = object2.getJSONArray("user_pre_amount");
                            for (int i = 0; i < user_pre_amount_obj.length(); i++) {
                                if (user_pre_amount_obj.getJSONObject(i).has("payment_status")
                                        && user_pre_amount_obj.getJSONObject(i).has("pre_amount")
                                        && user_pre_amount_obj.getJSONObject(i).getString("payment_status").equals("Accepted")) {
                                    PreAmount = PreAmount + Integer.parseInt(user_pre_amount_obj.getJSONObject(i).getString("pre_amount"));
                                }

                            }
                        }

                        currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);

                        Str_BtnGroup = object2.getString("btn_group");

                        JSONArray jarry = object2.getJSONArray("billing");

                        if (object2.has("user_approval_status")) {
                            user_approval_status = object2.getString("user_approval_status");
                        }

                        if (jarry.length() > 0) {

                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);
                                PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();

                                String title = jobjects_amount.getString("title");
                                String amount = jobjects_amount.getString("amount");


                                String desc = "";
                                if (jobjects_amount.has("description")) {
                                    desc = jobjects_amount.getString("description");
                                } else {
                                    desc = "";
                                }

                                pojo.setPaymentdesc(desc);

                                if (amount.equalsIgnoreCase("null") || amount.equalsIgnoreCase(null)) {

                                } else {

                                    if (user_approval_status.equalsIgnoreCase("1")) {

                                        if (jobjects_amount.getString("title").equalsIgnoreCase("Balance amount")) {

                                            if (jobjects_amount.getString("amount").matches("[0-9.]*")) {
                                                addallforloyality = Double.parseDouble(jobjects_amount.getString("amount"));
                                            }


                                            if (jobjects_amount.getString("amount").startsWith("-")) {
                                                refunamout = "1";
                                                Bt_Request_payment.setText(getResources().getString(R.string.refudamountbutton));
                                                pojo.setPayment_title(getResources().getString(R.string.refuda));
                                                pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount").replace("-", ""));
                                            } else {
                                                pojo.setPayment_title(jobjects_amount.getString("title"));
                                                pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount"));
                                            }

                                        } else {
                                            if (jobjects_amount.getString("title").equalsIgnoreCase("Total amount")) {
                                                pojo.setPayment_title(jobjects_amount.getString("title"));

                                                if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                                    pojo.setPayment_amount("");
                                                } else {
                                                    pojo.setPayment_amount("");
                                                }
                                            } else {

                                                if (jobjects_amount.has("description")) {

                                                    pojo.setPayment_title(jobjects_amount.getString("title"));

                                                    if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                                        pojo.setPayment_amount("");
                                                    } else {
                                                        pojo.setPayment_amount("");
                                                    }

                                                } else {
                                                    pojo.setPayment_title(jobjects_amount.getString("title"));

                                                    if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                                        pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                                    } else {
                                                        pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount"));
                                                    }
                                                }

                                            }
                                        }


                                    } else {

                                        if (jobjects_amount.getString("title").equalsIgnoreCase("Balance amount")) {


                                            if (jobjects_amount.getString("amount").matches("[0-9.]*")) {
                                                addallforloyality = Double.parseDouble(jobjects_amount.getString("amount"));
                                            }


                                            if (jobjects_amount.getString("amount").startsWith("-")) {
                                                Bt_Request_payment.setText(getResources().getString(R.string.refudamountbutton));
                                                refunamout = "1";
                                                pojo.setPayment_title(getResources().getString(R.string.refuda));
                                                pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount").replace("-", ""));
                                            } else {
                                                pojo.setPayment_title(jobjects_amount.getString("title"));
                                                pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount"));
                                            }

                                        } else {
                                            pojo.setPayment_title(jobjects_amount.getString("title"));

                                            if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                                pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                            } else {
                                                pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount"));
                                            }
                                        }

                                    }


                                    farelist.add(pojo);
                                    TempFareList.add(pojo);
                                }


                            }
                            show_progress_status = true;
                        } else {
                            show_progress_status = false;
                        }


                    } else {
                        Str_response = jobject.getString("response");
                    }

                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("1")) {


                    System.out.println();
                    Tvpaymentfare_jobId.setVisibility(View.VISIBLE);
                    Tv_JobDescription.setText(Str_jobDescription);
                    Tv_JobId.setText(Job_id);

                    System.out.println();


                    fare_listview.setEnabled(false);
                    /*if (PreAmount != 0 && PreAmount > 0) {
                        PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title("Payments Received");
                        pojo.setPayment_amount(currencyCode + String.valueOf(PreAmount));
                        TempFareList.set((TempFareList.size() - 1), pojo);
                        pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title(farelist.get(farelist.size() - 1).getPayment_title());
                        pojo.setPayment_amount(farelist.get(farelist.size() - 1).getPayment_amount());
                        TempFareList.add(pojo);
                        adapter = new PaymentFareSummeryAdapter(paymentrequest.this, TempFareList);
                    } else if (PreAmount == 0) {
                        adapter = new PaymentFareSummeryAdapter(paymentrequest.this, farelist);
                    }*/

                    PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();
                    pojo.setPayment_title("Payments Received");
                    pojo.setPayment_amount(currencyCode + String.valueOf(PreAmount));
                    TempFareList.set((TempFareList.size()-1), pojo);
                    pojo = new PaymentFareSummeryPojo();
                    pojo.setPayment_title(farelist.get(farelist.size()-1).getPayment_title());
                    pojo.setPayment_amount(farelist.get(farelist.size()-1).getPayment_amount());
                    TempFareList.add(pojo);
                    adapter = new PaymentFareSummeryAdapter(paymentrequest.this, TempFareList);
                    fare_listview.setAdapter(adapter);


                    servicelistview.setEnabled(false);
                    whatsadapter adapter = new whatsadapter(paymentrequest.this, categorynameinarray, whatsincludedinarray);
                    servicelistview.setAdapter(adapter);

//                    if (Str_NeedPayment.equalsIgnoreCase("1")) {
//                        Rl_layout_farepayment_methods.setVisibility(View.VISIBLE);
//                    } else {
//                        Rl_layout_farepayment_methods.setVisibility(View.GONE);
//                    }


                } else {
                    HNDHelper.showResponseErrorAlert(paymentrequest.this, Str_response);

                }

                myDialog.dismiss();

            }

            @Override
            public void onErrorListener() {
                myDialog.dismiss();
            }
        });
    }


    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {

        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    public class neww extends RecyclerView.Adapter<neww.MyViewHolder> {

        private List<newimte> moviesList;
        Context ctxx;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView image;
            TextView textme;

            public MyViewHolder(View view) {
                super(view);
                image = (ImageView) view.findViewById(R.id.image);
                textme = (TextView) view.findViewById(R.id.textme);
            }
        }


        public neww(List<newimte> moviesList, Context ctx) {
            this.moviesList = moviesList;
            this.ctxx = ctx;
        }

        @Override
        public neww.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.imte, parent, false);

            return new neww.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(neww.MyViewHolder holder, int position) {
            final newimte movie = moviesList.get(position);

            holder.textme.setText(movie.getTextname());
            if (movie.getTitle().isEmpty()) {
                holder.image.setImageResource(R.drawable.noimageavailable);
            } else {
                Picasso.get()
                        .load(movie.getTitle())
                        .placeholder(R.drawable.noimageavailable)   // optional
                        .error(R.drawable.noimageavailable)      // optional
                        // optional
                        .into(holder.image);
            }

            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (movie.getTitle().length() > 10) {
                        Intent in = new Intent(ctxx, zoomMainActivity.class);
                        in.putExtra("imagename", "" + movie.getTitle());
                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        ctxx.startActivity(in);

                    }

                }
            });


        }

        @Override
        public int getItemCount() {
            return moviesList.size();
        }
    }

    void PrepaymentRequest(String url) {
//        startLoading();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("job_id", Job_id);

        ServiceRequest mservicerequest = new ServiceRequest(paymentrequest.this);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                LinearLayoutTips.setVisibility(View.GONE);
                LinearLayoutDonate.setVisibility(View.GONE);
                LinearLayoutPrepaymentView.setVisibility(VISIBLE);
                ButtonRejectAll.setVisibility(View.GONE);
                Log.e("pre_amount/list", response);
                stopLoading();
                String total_amount = "", balance_amount = "", pre_amount = "";
                User_pre_amount_list[] user_pre_amount_list_main = new User_pre_amount_list[0];
                try {
                    JSONObject jobject = new JSONObject(response);
                    if (jobject.has("status") && jobject.getString("status").equals("1")) {
                        String currencyCode = CurrencySymbolConverter.getCurrencySymbol(jobject.getString("currency"));
                        if (jobject.has("user_pre_amount_list")) {
                            JSONArray user_pre_amount_list = jobject.getJSONArray("user_pre_amount_list");
                            if (user_pre_amount_list.length() > 0) {
                                TextViewPrepaymentCount.setText("Prepayment Request: " + user_pre_amount_list.length());
                                user_pre_amount_list_main = new User_pre_amount_list[user_pre_amount_list.length()];
                                for (int i = 0; i < user_pre_amount_list.length(); i++) {
                                    User_pre_amount_list user_pre_amount_list1 = new User_pre_amount_list();
                                    user_pre_amount_list1.set_id(user_pre_amount_list.getJSONObject(i).getString("_id"));
                                    user_pre_amount_list1.setPre_amount(user_pre_amount_list.getJSONObject(i).getString("pre_amount"));
                                    user_pre_amount_list1.setPayment_status(user_pre_amount_list.getJSONObject(i).getString("payment_status"));
                                    user_pre_amount_list1.setTime(user_pre_amount_list.getJSONObject(i).getString("time"));
                                    user_pre_amount_list1.setDate(user_pre_amount_list.getJSONObject(i).getString("date"));
                                    user_pre_amount_list_main[i] = user_pre_amount_list1;
                                }

                                if (user_pre_amount_list_main.length > 0) {
                                    MultiplePaymentRequestAdapter multiplePaymentRequestAdapter = new MultiplePaymentRequestAdapter(
                                            paymentrequest.this, user_pre_amount_list_main, currencyCode, Job_id,
                                            new MultiplePaymentRequestAdapter.SelectedPrepaymentListID() {
                                                @Override
                                                public void SelectedList(List<User_pre_amount_list> List) {
                                                    //Store the List value which we send from adapter to global variable MultiplePrepaymentSelectedFromAdapterList
                                                    MultiplePrepaymentSelectedFromAdapterList = List;
                                                    //If user select two or more check box then visible reject all button.
                                                    if (List.size() >= 2) {
                                                        ButtonRejectAll.setVisibility(VISIBLE);
                                                    } else {
                                                        ButtonRejectAll.setVisibility(View.GONE);
                                                    }
                                                }

                                                @Override
                                                public void RefreshTheList() {
                                                    //From here I'm refreshing the prepayment list along
                                                    //So recalling the list from server.
                                                    PrepaymentRequest(ServiceConstant.PrepaymentRequestList);
                                                }
                                            });
                                    RecyclerViewMultiplePaymentRequest.setAdapter(multiplePaymentRequestAdapter);
                                    LinearLayoutPrepaymentView.setVisibility(VISIBLE);
                                } else {
                                    LinearLayoutPrepaymentView.setVisibility(View.GONE);
                                }


                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                stopLoading();
            }
        });
    }

    //Function to start the loading the Dialog.
    private void startLoading() {
        myDialog = new ProgressDialogcreated(paymentrequest.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
    }

    //Function to stop the loading the Dialog.
    private void stopLoading() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (myDialog != null) {
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                }
            }
        }, 500);
    }
}
