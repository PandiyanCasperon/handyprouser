package com.handypro.activities.navigationMenu;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.handypro.core.socket.ChatMessageService;
import com.handypro.core.socket.ChatMessageServicech;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.ChatInfoPojo;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.ChatPage;
import com.handypro.adapters.ChatListAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CAS63 on 3/2/2018.
 */

public class ChatActivity extends AppCompatActivity {
    private RelativeLayout myBackLAY;
    private MaterialRefreshLayout myRefreshLAY;
    private ListView myChatListVw;
    private ArrayList<ChatInfoPojo> myChatArrList = new ArrayList<>();
    private ChatListAdapter myChatAdapter;
    private LinearLayout myNoInternetLAY;
    private CustomTextView myRetryConnTXT;
    private ConnectionDetector myConnectionManager;
    private ProgressDialogcreated myDialog;
    private SharedPreference mySession;
    String UserID = "";
    TextView empty_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInitialize();

        postRequest_ModeChange("gcm");


        loadData();
        getChatData();
    }

    private void getChatData() {
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestChatList(ServiceConstant.GETMESSAGECHAT_URL, ChatActivity.this);
        } else {
            checkInternetConnectivity();
        }
    }

    private void postRequestChatList(String url, final Context aContext) {
        myDialog = new ProgressDialogcreated(aContext);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("userId", UserID);
        jsonParams.put("type", "1");

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String sStatus = "";
                try {
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                    JSONObject aObject = new JSONObject(response);
                    sStatus = aObject.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject response_Object = aObject.getJSONObject("response");
                        if (response_Object.length() > 0) {
                            JSONArray aMessageArray = response_Object.getJSONArray("message");
                            if (aMessageArray.length() > 0) {
                                myChatArrList.clear();
                                for (int i = 0; i < aMessageArray.length(); i++) {
                                    JSONObject aJsonObject = aMessageArray.getJSONObject(i);
                                    ChatInfoPojo aMessageInfo = new ChatInfoPojo();
                                    aMessageInfo.setMessageTaskId(aJsonObject.getString("task_id"));
                                    aMessageInfo.setMessageBookingId(aJsonObject.getString("booking_id"));
                                    aMessageInfo.setMessageTaskerNameId(aJsonObject.getString("tasker_name"));
                                    aMessageInfo.setMessageTaskerId(aJsonObject.getString("tasker_id"));
                                    aMessageInfo.setMessageTaskerImageId(aJsonObject.getString("tasker_image"));


                                   /* String service="";
                                    JSONArray service_type = aJsonObject.getJSONArray("category");
                                    for (int b = 0; b < service_type.length(); b++)
                                    {

                                        String value=""+service_type.getString(b);

                                        //JSONObject serobj = aMainArr.getJSONObject(b);
                                        service+=value+",";
                                    }

                                    if(service.endsWith(","))
                                    {
                                        service=service.substring(0,service.length()-1);
                                    }*/

                                    aMessageInfo.setCategory("");

                                    aMessageInfo.setTasker_status(aJsonObject.getString("user_status"));
                                    aMessageInfo.setMessagedate(aJsonObject.getString("created"));
                                    myChatArrList.add(aMessageInfo);
                                }
                            }
                        }
                        loadData();
                    } else {
                        String sResponse = aObject.getString("response");
                        HNDHelper.showResponseErrorAlert(aContext, sResponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void loadData() {
        if (myChatArrList.size() > 0) {
            myChatListVw.setVisibility(View.VISIBLE);
            empty_text.setVisibility(View.GONE);
            myChatAdapter = new ChatListAdapter(ChatActivity.this, myChatArrList);
            myChatListVw.setAdapter(myChatAdapter);
        } else {
            empty_text.setVisibility(View.VISIBLE);
            myChatListVw.setVisibility(View.GONE);
        }
    }

    private void classAndWidgetInitialize() {
        mySession = new SharedPreference(ChatActivity.this);
        UserID = mySession.getUserDetails().getUserId();
        myConnectionManager = new ConnectionDetector(ChatActivity.this);
        mySession = new SharedPreference(ChatActivity.this);
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_chat_LAY_back);
        myRefreshLAY = (MaterialRefreshLayout) findViewById(R.id.activity_chat_Swipe_layout);
        myChatListVw = (ListView) findViewById(R.id.activity_chat_messages_listView);
        myRetryConnTXT = (CustomTextView) findViewById(R.id.activity_chat_no_retry_TXT);
        myNoInternetLAY = (LinearLayout) findViewById(R.id.activity_chat_no_internetLAY);
        empty_text = (TextView)findViewById(R.id.empty_text);

        checkInternetConnectivity();

        myRetryConnTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getChatData();
            }
        });

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myChatListVw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ChatMessageServicech.tasker_id="";
                ChatMessageServicech.task_id="";
                Intent intent = new Intent(ChatActivity.this, ChatPage.class);
                intent.putExtra("TaskerId", myChatArrList.get(position).getMessageTaskerId());
                intent.putExtra("TaskId", myChatArrList.get(position).getMessageTaskId());

                intent.putExtra("name", myChatArrList.get(position).getMessageTaskerNameId());
                intent.putExtra("iamge", myChatArrList.get(position).getMessageTaskerImageId());
                startActivity(intent);
            }
        });

        myRefreshLAY.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getChatData();
                        myRefreshLAY.finishRefresh();
                    }
                }, 750);
            }
        });

    }

    private void checkInternetConnectivity() {
        if (myConnectionManager.isConnectingToInternet()) {
            myNoInternetLAY.setVisibility(View.GONE);
            myRefreshLAY.setVisibility(View.VISIBLE);
        } else {
            myNoInternetLAY.setVisibility(View.VISIBLE);
            myRefreshLAY.setVisibility(View.GONE);
        }
    }

    private void postRequest_ModeChange(final String aModeType) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();


        jsonParams.put("user", mySession.getUserDetails().getUserId());
        jsonParams.put("user_type", "user");
        jsonParams.put("mode", "gcm");
        jsonParams.put("type", "android");


        ServiceRequest  mRequest = new ServiceRequest(getApplicationContext());
        mRequest.makeServiceRequest(ServiceConstant.MODEUPDATE_URL, Request.Method.POST,
                jsonParams, new ServiceRequest.ServiceListener() {
                    @Override
                    public void onCompleteListener(String response) {

                        System.out.println("---------------Mode Response-----------------" + response);
                        String sStatus = "", sResponse = "";
                        try {

                            JSONObject object = new JSONObject(response);
                            sStatus = object.getString("status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (sStatus.equalsIgnoreCase("1")) {
                            //    session.logoutUser();
                            if(aModeType.equalsIgnoreCase("socket")){
//                                Log.e("MODE UPDATED","SOCKET");
                                System.out.println("---------------Mode Response-----------------SOCKET" );
                            }
                            else{
//                                Log.e("MODE UPDATED","GCM");
                            }

//                            if (!socketHandler.getSocketManager().isConnected) {
//                                socketHandler.getSocketManager().disconnect();
//                            }
                        } else {
                        }
                    }

                    @Override
                    public void onErrorListener() {
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            Intent intent = new Intent(ChatActivity.this, ChatMessageService.class);
            startService(intent);

            Intent intents = new Intent(ChatActivity.this, ChatMessageServicech.class);
            startService(intents);

            postRequest_ModeChange("gcm");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
