package com.handypro.activities.navigationMenu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;


import com.android.volley.Request;
import com.cjj.MaterialRefreshLayout;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.CardInfoPojo;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.EditCardActivity;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.BoldCustomTextView;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CAS63 on 3/6/2018.
 */

public class CardListActivity extends AppCompatActivity {
    private static ListView myCardListVw;
    private CustomButton myAddBTN;
    private static MaterialRefreshLayout myRefreshLAY;
    private static int EDIT_CARD_FLAG = 1092;
    private static ArrayList<CardInfoPojo> myCardArrList;
    private static CardListAdapter myCardAdapter;
    private static RelativeLayout myBackLAY,mySwipeNotesLAY;
    private static LinearLayout myNoCardLAY;
    private static ConnectionDetector myConnectionManager;
    private static ProgressDialogcreated myDialog;
    private static SharedPreference mySession;
   int sizeof;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_list);

        classAndWidgetInitialize();


        widgetOnClick();
    }

    private void widgetOnClick() {
        myAddBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aCardIntent = new Intent(CardListActivity.this, EditCardActivity.class);
                startActivityForResult(aCardIntent, EDIT_CARD_FLAG);

            }
        });

        myCardListVw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((SwipeLayout)(myCardListVw.getChildAt(position - myCardListVw.getFirstVisiblePosition()))).open(true);
            }
        });

    }

    /*private void showAlert(final CardInfoPojo aInfoPojo, final int position) {
        try {
            Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Medium.ttf");
            new MaterialDialog.Builder(CardListActivity.this)
                    .content(getResources().getString(R.string.activity_card_list_delete))
                    .positiveText(R.string.dialog_ok)
                    .negativeText(R.string.dialog_cancel)
                    .typeface(tf,tf)
                    .positiveColor(getResources().getColor(R.color.colorAccent))
                    .negativeColor(getResources().getColor(R.color.darkcolor_3))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {


                        }
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void postRequestDeleteCardFromList(final Context aContext, String url, CardInfoPojo aCardInfo, final int position) {
        myDialog = new ProgressDialogcreated(CardListActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("card_id",aCardInfo.getCardId());
        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------delete Stripe Card Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        showSuccessAlert(getResources().getString(R.string.activity_card_list_delete_message),position);

                    } else {
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }


    private void showSuccessAlert(String aContent, final int position) {
        try {

            final PkDialog mDialog = new PkDialog(CardListActivity.this);
            mDialog.setDialogTitle(getResources().getString(R.string.success_label));
            mDialog.setDialogMessage(aContent);
            mDialog.setPositiveButton(
                    getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                            myCardArrList.remove(position);
                            myCardAdapter.notifyDataSetChanged();
                        }
                    }
            );
            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void loadData(Context aContext) {
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestGetCardList(aContext, ServiceConstant.getCardList);
        } else {
            HNDHelper.showErrorAlert(aContext, aContext.getResources().getString(R.string.nointernet_text));
        }
    }

    public static void postRequestGetCardList(final Context aContext, String url) {
        myDialog = new ProgressDialogcreated(aContext);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------get Stripe Card Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        myCardArrList.clear();
                        JSONObject aRespObj = object.getJSONObject("response");
                        JSONArray aMainArr = aRespObj.getJSONArray("cardList");
                        for (int a = 0; a < aMainArr.length(); a++) {
                            JSONObject aRespOb = aMainArr.getJSONObject(a);
                            CardInfoPojo aCardInfoPojo = new CardInfoPojo();
                            aCardInfoPojo.setCardNumber(aRespOb.getString("last4"));
                            aCardInfoPojo.setCardValidMonth(aRespOb.getString("exp_month"));
                            aCardInfoPojo.setCardValidYear(aRespOb.getString("exp_year"));
                            aCardInfoPojo.setCardType(aRespOb.getString("brand"));
                            aCardInfoPojo.setCardId(aRespOb.getString("id"));
                            if (aRespObj.getString("default_source")
                                    .equals(aRespOb.getString("id"))) {
                                aCardInfoPojo.setCardSelected("1");
                            } else {
                                aCardInfoPojo.setCardSelected("0");
                            }

                            myCardArrList.add(aCardInfoPojo);
                        }

                        loadDataInAdapter(aContext);

                    } else {
                        mySwipeNotesLAY.setVisibility(View.GONE);
                        myCardListVw.setVisibility(View.GONE);
                        myRefreshLAY.setVisibility(View.GONE);
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }



            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void classAndWidgetInitialize() {
        myConnectionManager = new ConnectionDetector(CardListActivity.this);
        mySession = new SharedPreference(CardListActivity.this);
        myCardArrList = new ArrayList<CardInfoPojo>();
        myCardListVw = (ListView) findViewById(R.id.activity_card_list_listView);
        myAddBTN = (CustomButton) findViewById(R.id.activity_card_list_add_BTN);
        myRefreshLAY = (MaterialRefreshLayout) findViewById(R.id.activity_card_list_Swipe_layout);
        myNoCardLAY = (LinearLayout) findViewById(R.id.activity_add_card_no_cardLAY);
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_card_list_LAY_back);
        mySwipeNotesLAY = (RelativeLayout) findViewById(R.id.activity_card_list_swipe_notes_LAY);


        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                onBackPressed();
            }
        });
    }

    public static void loadDataInAdapter(Context aContext) {
        if (myCardArrList.size() > 0) {
            myNoCardLAY.setVisibility(View.GONE);
            mySwipeNotesLAY.setVisibility(View.VISIBLE);
            myCardListVw.setVisibility(View.VISIBLE);
            myRefreshLAY.setVisibility(View.VISIBLE);
            myCardAdapter = new CardListAdapter(aContext, myCardArrList);
            myCardListVw.setAdapter(myCardAdapter);
            HNDHelper.setListViewHeightBasedOnChildren(myCardListVw);
        } else {
            myNoCardLAY.setVisibility(View.VISIBLE);
            mySwipeNotesLAY.setVisibility(View.GONE);
            myCardListVw.setVisibility(View.GONE);
            myRefreshLAY.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData(CardListActivity.this);
    }












    public static class CardListAdapter extends BaseSwipeAdapter {

        private ArrayList<CardInfoPojo> myCardInfoList;
        private Context myContext;
        private SharedPreference mySession;
        private ProgressDialogcreated myDialog;
        private ConnectionDetector myConnectionManager;
        private Animation myListAnim;

        public CardListAdapter(Context mContext, ArrayList<CardInfoPojo> aCardList) {
            this.myContext = mContext;
            this.myCardInfoList = aCardList;
            mySession = new SharedPreference(myContext);
            myConnectionManager = new ConnectionDetector(myContext);
        }

        @Override
        public int getSwipeLayoutResourceId(int position) {
            return R.id.layout_inflater_card_list_swipeLAY;
        }

        @Override
        public View generateView(final int position, ViewGroup parent) {
            View v = LayoutInflater.from(myContext).inflate(R.layout.layout_inflater_card_list_item, null);
            SwipeLayout swipeLayout = (SwipeLayout) v.findViewById(getSwipeLayoutResourceId(position));
            CustomTextView aDeleteTXT = (CustomTextView) v.findViewById(R.id.layout_inflater_card_list_item_delete_TXT);
            final RadioButton aCardRadioBTN = (RadioButton) v.findViewById(R.id.layout_inflate_card_list_item_selected_radio_BTN);
            swipeLayout.addSwipeListener(new SimpleSwipeListener() {
                @Override
                public void onOpen(SwipeLayout layout) {
                    layout.findViewById(R.id.layout_inflater_card_list_item_delete_TXT).setVisibility(View.VISIBLE);
                }
            });
            myListAnim = AnimationUtils.loadAnimation(myContext, R.anim.nav_show_text);
            swipeLayout.setAnimation(myListAnim);


            aDeleteTXT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAlert(position);
                }
            });

            if (myCardInfoList.get(position).isSelected().equals("1")) {
                aCardRadioBTN.setChecked(true);
            } else {
                aCardRadioBTN.setChecked(false);
            }

            aCardRadioBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (myCardInfoList.get(position).isSelected().equals("0")) {
                        showChangeDefCard(position, aCardRadioBTN);
                    }

                }
            });

            return v;
        }

        private void showChangeDefCard(final int position, final RadioButton aRadioBTN) {
            try {

                final PkDialog mDialog = new PkDialog(myContext);
                mDialog.setDialogTitle(myContext.getResources().getString(R.string.success_label));
                mDialog.setDialogMessage(myContext.getResources().getString(R.string.activity_card_list_switch_def_card));
                mDialog.setPositiveButton(
                        myContext.getResources().getString(R.string.dialog_yes), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                if (myConnectionManager.isConnectingToInternet()) {
                                    aRadioBTN.setChecked(true);
                                    postRequestMakeCardDefault(myContext, ServiceConstant.makeCardDefault, position);
                                } else {
                                    HNDHelper.showErrorAlert(myContext, myContext.getResources().getString(R.string.nointernet_text));
                                }
                            }
                        }
                );
                mDialog.setNegativeButton(
                        myContext.getResources().getString(R.string.dialog_no), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                aRadioBTN.setChecked(false);
                            }
                        }
                );

                mDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void showAlert(final int position) {
            try {

                final PkDialog mDialog = new PkDialog(myContext);
                mDialog.setDialogTitle(myContext.getResources().getString(R.string.success_label));
                mDialog.setDialogMessage(myContext.getResources().getString(R.string.activity_card_list_delete));
                mDialog.setPositiveButton(
                        myContext.getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                if (myConnectionManager.isConnectingToInternet()) {
                                    postRequestDeleteCardFromList(myContext, ServiceConstant.deleteStripeCard, position);
                                } else {
                                    HNDHelper.showErrorAlert(myContext, myContext.getResources().getString(R.string.nointernet_text));
                                }
                            }
                        }
                );
                mDialog.setNegativeButton(
                        myContext.getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        }
                );
                mDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void postRequestDeleteCardFromList(final Context aContext, String url, final int position) {
            myDialog = new ProgressDialogcreated(aContext);
            if (!myDialog.isShowing()) {
                myDialog.show();
            }
            HashMap<String, String> jsonParams = new HashMap<>();
            jsonParams.put("user_id", mySession.getUserDetails().getUserId());
            jsonParams.put("card_id", myCardInfoList.get(position).getCardId());
            ServiceRequest mRequest = new ServiceRequest(aContext);
            mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("-------------delete Stripe Card Response----------------" + response);

                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getString("status").equalsIgnoreCase("1")) {
                            showDeleteSuccessAlert(object.getString("response"), position);

                        } else {
                            HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }

                }

                @Override
                public void onErrorListener() {
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                }
            });

        }


        @Override
        public void fillValues(int position, View convertView) {
            BoldCustomTextView aCardNumTXT = (BoldCustomTextView) convertView.findViewById(R.id.layout_inflate_card_list_item_card_nameTXT);
            aCardNumTXT.setText("**************" + myCardInfoList.get(position).getCardNumber());
        }

        @Override
        public int getCount() {
            return myCardInfoList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        private void postRequestMakeCardDefault(final Context aContext, String url, final int position) {
            myDialog = new ProgressDialogcreated(aContext);
            if (!myDialog.isShowing()) {
                myDialog.show();
            }
            HashMap<String, String> jsonParams = new HashMap<>();
            jsonParams.put("user_id", mySession.getUserDetails().getUserId());
            jsonParams.put("card_id", myCardInfoList.get(position).getCardId());
            ServiceRequest mRequest = new ServiceRequest(aContext);
            mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("-------------make default Stripe Card Response----------------" + response);

                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getString("status").equalsIgnoreCase("1")) {
                            showUpdateSuccessAlert(aContext.getResources().getString(R.string.activity_card_list_updated_label), position);
                        } else {
                            HNDHelper.showErrorAlert(aContext, object.getString("response"));
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }

                }

                @Override
                public void onErrorListener() {
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                }
            });
        }

        private void showUpdateSuccessAlert(String aContent, final int position) {
            try {

                final PkDialog mDialog = new PkDialog(myContext);
                mDialog.setDialogTitle(myContext.getResources().getString(R.string.success_label));
                mDialog.setDialogMessage(aContent);
                mDialog.setPositiveButton(
                        myContext.getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                CardListActivity.loadData(myContext);
                            }
                        }
                );

                mDialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        private void showDeleteSuccessAlert(String aContent, final int position) {
            try {

                final PkDialog mDialog = new PkDialog(myContext);
                mDialog.setDialogTitle(myContext.getResources().getString(R.string.success_label));
                mDialog.setDialogMessage(aContent);
                mDialog.setPositiveButton(
                        myContext.getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                CardListActivity.loadData(myContext);
                            }
                        }
                );

                mDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
