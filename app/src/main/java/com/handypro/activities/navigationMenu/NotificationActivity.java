package com.handypro.activities.navigationMenu;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.NotificationInfoPojo;
import com.handypro.Pojo.NotificationMessageInfoPojo;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.adapters.NotificationAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CAS63 on 3/2/2018.
 */

public class NotificationActivity extends AppCompatActivity {
    private RelativeLayout myBackLAY;
    private NotificationAdapter myNotifAdapter;
    private ExpandableListView myExpListVw;
    private MaterialRefreshLayout myRefreshLAY;
    private ArrayList<NotificationInfoPojo> myNotifArrList;
    private LinearLayout myNoInternetLAY,myEmptyNotificationsLAY;
    private CustomTextView myRetryInternetTXT,myNoNotificationTXT;
    private ConnectionDetector myConnectionManager;
    private ProgressDialogcreated myDialog;
    private SharedPreference mySession;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInitialize();

        getNotificationData();
    }



    private void classAndWidgetInitialize() {
        myConnectionManager = new ConnectionDetector(NotificationActivity.this);
        mySession = new SharedPreference(NotificationActivity.this);
        myNotifArrList = new ArrayList<NotificationInfoPojo>();
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_notification_LAY_back);
        myExpListVw = (ExpandableListView) findViewById(R.id.activity_notification_EXPLV);
        myRefreshLAY = (MaterialRefreshLayout) findViewById(R.id.activity_notification_Swipe_layout);
        myNoInternetLAY = (LinearLayout) findViewById(R.id.activity_notifications_no_internetLAY);
        myRetryInternetTXT = (CustomTextView) findViewById(R.id.activity_notifications_no_retry_TXT);
        myNoNotificationTXT = (CustomTextView) findViewById(R.id.activity_notifications_no_notification_TXT);
        myEmptyNotificationsLAY = (LinearLayout) findViewById(R.id.activity_notifications_no_notifLAY);
        myExpListVw.setVisibility(View.VISIBLE);

        checkInternetConnectivity();

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myRefreshLAY.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getNotificationData();
                        myRefreshLAY.finishRefresh();
                    }
                }, 750);
            }
        });

        myRetryInternetTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNotificationData();
            }
        });
    }

    private void checkInternetConnectivity() {
        if (myConnectionManager.isConnectingToInternet()) {
            myNoInternetLAY.setVisibility(View.GONE);
            myRefreshLAY.setVisibility(View.VISIBLE);
        } else {
            myNoInternetLAY.setVisibility(View.VISIBLE);
            myRefreshLAY.setVisibility(View.GONE);
        }
    }

    private void getNotificationData() {
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestNotificationList(NotificationActivity.this, ServiceConstant.NOTIFICATION_URL);
        } else {
            checkInternetConnectivity();
        }
    }

    private void postRequestNotificationList(final Context aContext, String url) {
        myDialog = new ProgressDialogcreated(aContext);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("role", "user");

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Notification List Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {


                        myNotifArrList.clear();
                        JSONArray aResponseArray = object.getJSONArray("response");
                        if (aResponseArray.length() > 0) {
                            for (int i = 0; i < aResponseArray.length(); i++) {
                                ArrayList<NotificationMessageInfoPojo> aNotificationMessageInfoList = new ArrayList<NotificationMessageInfoPojo>();
                                JSONObject aResponseObject = aResponseArray.getJSONObject(i);
                                NotificationInfoPojo aNotificationInfo = new NotificationInfoPojo();
                                //aNotificationInfo.setNotificationTaskId(aResponseObject.getString("task"));
                                aNotificationInfo.setNotificationBookingId(aResponseObject.getString("booking_id"));
                                String service="";
                                JSONArray service_type = aResponseObject.getJSONArray("category");
                                for (int b = 0; b < service_type.length(); b++)
                                {

                                    String value=""+service_type.getString(b);


                                    service+=value+",";
                                }

                                if(service.endsWith(","))
                                {
                                    service=service.substring(0,service.length()-1);
                                }
                                aNotificationInfo.setNotificationCategory(service);



                                if (aResponseObject.has("messages")) {
                                    JSONArray aMessageArrayInfo = aResponseObject.getJSONArray("messages");
                                    for (int j = 0; j < aMessageArrayInfo.length(); j++) {
                                        JSONObject aMessageResponseObject = aMessageArrayInfo.getJSONObject(j);


                                        NotificationMessageInfoPojo aNotificationMessageInfo = new NotificationMessageInfoPojo();
                                        aNotificationMessageInfo.setNotificationMessageCreatedAt(aMessageResponseObject.getString("createdAt"));
                                        aNotificationMessageInfo.setNotificationMessage(aMessageResponseObject.getString("message"));
                                        aNotificationMessageInfoList.add(aNotificationMessageInfo);
                                    }
                                }
                                aNotificationInfo.setNotificationMessageInfo(aNotificationMessageInfoList);

                                myNotifArrList.add(aNotificationInfo);




                            }
                        }

                    } else {
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                        myExpListVw.setVisibility(View.GONE);
                        myEmptyNotificationsLAY.setVisibility(View.VISIBLE);
                        myNoNotificationTXT.setText(object.getString("response"));
                    }

                    myEmptyNotificationsLAY.setVisibility(View.GONE);
                    myNotifAdapter = new NotificationAdapter(NotificationActivity.this, myNotifArrList);
                    myExpListVw.setAdapter(myNotifAdapter);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

}
