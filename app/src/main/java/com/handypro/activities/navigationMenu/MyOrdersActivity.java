package com.handypro.activities.navigationMenu;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.MyOrdersPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.Cancel_Job_Reason;
import com.handypro.activities.ChatPage;
import com.handypro.activities.HomeActivity;
import com.handypro.activities.OrderDetailActivity;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.core.socket.ChatMessageServicech;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

/**
 * Created by CAS63 on 3/2/2018.
 */

public class MyOrdersActivity extends AppCompatActivity {
    final int PERMISSION_REQUEST_CODE = 111;
    final int PERMISSION_REQUEST_CODES = 222;
    TextView firstline, secondline, thirdline;
    int preLast;
    String myPhoneSTR = "", myCountryCodeSTR = "";
    TextView hintshow;
    ProgressBar spinn;
    private RelativeLayout myBackLAY, myListViewMainLAY;
    private LinearLayout myOpenOrderLAY, myCompletedOrderLAY, myCancelledOrderLAY;
    private CustomTextView myOpenTXT, myCompletdTXT, myCancelledTXT, myRetryTXT, myFromDateTXT, myToDateTXT;
    private ProgressDialogcreated myDialog;
    private SharedPreference mySession;
    private LinearLayout myNoInternetLAY, myMainLAY;
    private ConnectionDetector myConnectionManager;
    private ImageView myFilterIMG;
    private Dialog mySortDialog;
    private String myOrderType = "1", myCompletedSTR = "0", myCancelledSTR = "0", mySelectedDateSTR = "1", mySelectedSortFlag = "",
            mySelectedOrderByFlag = "", myFilterBookingFlag = "0", myFilterTypeSTR = "no";
    private ListView myOrdersListVw;
    private View myOpenView, myCompletedView, myCancelledView;
    private CaldroidFragment dialogCaldroidFragment;
    private SimpleDateFormat formatter;
    final CaldroidListener caldroidListener = new CaldroidListener() {
        @Override
        public void onSelectDate(Date date, View view) {
            dialogCaldroidFragment.dismiss();
            // Tvto_date.setText(getResources().getString(R.string.appointment_label_select_time));

            if (mySelectedDateSTR.equalsIgnoreCase("1")) {
                myFromDateTXT.setText(formatter.format(date));
            } else {
                myToDateTXT.setText(formatter.format(date));
            }
        }

        @Override
        public void onChangeMonth(int month, int year) {
            String text = "month: " + month + " year: " + year;
        }

        @Override
        public void onLongClickDate(Date date, View view) {
        }

        @Override
        public void onCaldroidViewCreated() {
        }
    };
    private boolean loadingMore = false;
    private int checkPagePos = 0;
    private boolean isJobAvailable = false;
    private ArrayList<MyOrdersPojo> myOrdersArrList;
    private MyOrdersAdapter myOrderAdapter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInitialize();
        onClicksEvents();


        myFilterIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowFilterDialog(savedInstanceState);
            }
        });

        myOrderType = "1";
        myCompletedSTR = "0";
        myCancelledSTR = "0";

        postJobRequest(myOrderType);


        myOpenView.setVisibility(View.VISIBLE);
        myCompletedView.setVisibility(View.INVISIBLE);
        myCancelledView.setVisibility(View.INVISIBLE);

    }

    private void loadDataInAdapter() {
        myOrderAdapter = new MyOrdersAdapter(MyOrdersActivity.this, myOrdersArrList);
        myOrdersListVw.setAdapter(myOrderAdapter);
    }

    private void classAndWidgetInitialize() {
        myOrdersArrList = new ArrayList<MyOrdersPojo>();
        mySession = new SharedPreference(MyOrdersActivity.this);
        myConnectionManager = new ConnectionDetector(MyOrdersActivity.this);
        formatter = new SimpleDateFormat("yyyy/MM/dd");

        firstline = findViewById(R.id.firstline);
        secondline = findViewById(R.id.secondline);
        thirdline = findViewById(R.id.thirdline);


        spinn = findViewById(R.id.spinn);


        hintshow = findViewById(R.id.hintshow);
        myBackLAY = findViewById(R.id.activity_my_orders_LAY_back);
        myOpenOrderLAY = findViewById(R.id.activity_my_orders_open_layout);
        myCompletedOrderLAY = findViewById(R.id.activity_my_orders_completed_layout);
        myCancelledOrderLAY = findViewById(R.id.activity_my_orders_cancelled_layout);
        myOpenTXT = findViewById(R.id.activity_my_orders_all_textView);
        myCancelledTXT = findViewById(R.id.activity_my_orders_cancelled_textView);
        myCompletdTXT = findViewById(R.id.activity_my_orders_completed_textView);
        myRetryTXT = findViewById(R.id.activity_my_orders_retry_TXT);
        myNoInternetLAY = findViewById(R.id.activity_my_orders_internetLAY);
        myMainLAY = findViewById(R.id.activity_my_orders_main_layout);
        myFilterIMG = findViewById(R.id.activity_my_orders_IMG_filter);
        myOrdersListVw = findViewById(R.id.activity_my_orders_listView);
        myListViewMainLAY = findViewById(R.id.activity_my_orders_main_listview_layout);
        myOpenView = findViewById(R.id.activity_my_orders_open_view1);
        myCancelledView = findViewById(R.id.activity_my_orders_cancelled_view1);
        myCompletedView = findViewById(R.id.activity_my_orders_completed_view1);

        checkInternetConnectivity();
    }

    private void checkInternetConnectivity() {
        if (myConnectionManager.isConnectingToInternet()) {
            myNoInternetLAY.setVisibility(View.GONE);
            myMainLAY.setVisibility(View.VISIBLE);
            myListViewMainLAY.setVisibility(View.VISIBLE);
        } else {
            myNoInternetLAY.setVisibility(View.VISIBLE);
            myMainLAY.setVisibility(View.GONE);
            myListViewMainLAY.setVisibility(View.GONE);
        }
    }

    private void onClicksEvents() {
        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        myOpenOrderLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myOrderType = "1";
                myCompletedSTR = "0";
                myCancelledSTR = "0";
                myOpenView.setVisibility(View.VISIBLE);
                myCompletedView.setVisibility(View.INVISIBLE);
                myCancelledView.setVisibility(View.INVISIBLE);
             /*   myOpenTXT.setTextColor(0xFFF85603);
                myCompletdTXT.setTextColor(0xFF818181);
                myCancelledTXT.setTextColor(0xFF818181);*/
                firstline.setVisibility(View.VISIBLE);
                secondline.setVisibility(View.GONE);
                thirdline.setVisibility(View.GONE);

                postJobRequest(myOrderType);

            }
        });

        myCompletedOrderLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myOrderType = "4";
                myCompletedSTR = "1";
                myOpenView.setVisibility(View.INVISIBLE);
                myCompletedView.setVisibility(View.VISIBLE);
                myCancelledView.setVisibility(View.INVISIBLE);
                /*myOpenTXT.setTextColor(0xFF818181);
                myCompletdTXT.setTextColor(0xFFF85603);
                myCancelledTXT.setTextColor(0xFF818181);*/
                firstline.setVisibility(View.GONE);
                secondline.setVisibility(View.VISIBLE);
                thirdline.setVisibility(View.GONE);
                postJobRequest(myOrderType);
            }
        });

        myCancelledOrderLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myOrderType = "5";
                myCancelledSTR = "1";
                myOpenView.setVisibility(View.INVISIBLE);
                myCompletedView.setVisibility(View.INVISIBLE);
                myCancelledView.setVisibility(View.VISIBLE);
                /*myOpenTXT.setTextColor(0xFF818181);
                myCompletdTXT.setTextColor(0xFF818181);
                myCancelledTXT.setTextColor(0xFFF85603);
*/
                firstline.setVisibility(View.GONE);
                secondline.setVisibility(View.GONE);
                thirdline.setVisibility(View.VISIBLE);
                postJobRequest(myOrderType);
            }
        });

        myRetryTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postJobRequest(myOrderType);
            }
        });

        myOrdersListVw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (myConnectionManager.isConnectingToInternet()) {


                    SharedPreferences getcard;
                    getcard = getApplicationContext().getSharedPreferences("jobdeta", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = getcard.edit();
                    editor.putString("jobid", "" + myOrdersArrList.get(position).getOrderId());
                    editor.putString("servicetype", "" + myOrdersArrList.get(position).getOrderServiceType());
                    editor.commit();


                    Intent aOderDetailIntent = new Intent(MyOrdersActivity.this, OrderDetailActivity.class);
                    startActivity(aOderDetailIntent);
                } else {
                    HNDHelper.showErrorAlert(MyOrdersActivity.this, getResources().getString(R.string.nointernet_text));
                }
            }
        });


        myOrdersListVw.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int threshold = 1;
                int count = myOrdersListVw.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (myOrdersListVw.getLastVisiblePosition() >= count - threshold && !(loadingMore)) {
                        if (myConnectionManager.isConnectingToInternet()) {
                            postJobLoadMoreRequest(ServiceConstant.MyJobsList_Url, myOrderType, (checkPagePos + 1));
                        } else {
                            HNDHelper.showErrorAlert(MyOrdersActivity.this, getResources().getString(R.string.nointernet_text));
                        }
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                boolean enable = false;

            }
        });

       /* myRefreshLAY.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        postJobRequest(myOrderType);
                        myRefreshLAY.finishRefresh();
                    }
                }, 750);
            }
        });*/


    }

    private void postJobRequest(String aOrderTypeSTR) {
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestMyOrderList(MyOrdersActivity.this, ServiceConstant.MyJobsList_Url, aOrderTypeSTR);
        } else {
            checkInternetConnectivity();
        }
    }

    private void postJobLoadMoreRequest(String url, String aOrderType, int aPosition) {
        spinn.setVisibility(View.VISIBLE);


        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("type", "" + myOrderType);
        jsonParams.put("page", String.valueOf(aPosition));
        jsonParams.put("perPage", "20");

        System.out.println("---------My Jobs LoadMore Type------------" + aOrderType);
        System.out.println("---------My Jobs LoadMore Page Pos------------" + aPosition);
        System.out.println("---------My Jobs LoadMore url------------" + url);

        ServiceRequest mRequest = new ServiceRequest(MyOrdersActivity.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------post my order Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {


                        JSONObject response_Object = object.getJSONObject("response");
                        checkPagePos = response_Object.getInt("current_page");
                        JSONArray aMainArr = response_Object.getJSONArray("jobs");


                        for (int a = 0; a < aMainArr.length(); a++) {

                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);
                            MyOrdersPojo aOrderPojo = new MyOrdersPojo();
                            aOrderPojo.setContactNumber(aSubCatObj.getString("contact_number"));
                            aOrderPojo.setCountrycode(aSubCatObj.getString("country_code"));
                            aOrderPojo.setDoCall(aSubCatObj.getString("doCall"));
                            aOrderPojo.setDoCancel(aSubCatObj.getString("doCancel"));
                            aOrderPojo.setDoMsg(aSubCatObj.getString("doMsg"));
                            aOrderPojo.setIsSupport(aSubCatObj.getString("isSupport"));
                            aOrderPojo.setOrderBookingDate(aSubCatObj.getString("booking_date"));

                            aOrderPojo.setUserphoto(aSubCatObj.getString("service_icon"));

                            aOrderPojo.setTaskerid(aSubCatObj.getString("tasker_id"));


                            String service = "";
                            JSONArray service_type = aSubCatObj.getJSONArray("service_type");
                            for (int b = 0; b < service_type.length(); b++) {

                                String value = "" + service_type.getString(b);

                                //JSONObject serobj = aMainArr.getJSONObject(b);
                                service += value + ",";
                            }

                            if (service.endsWith(",")) {
                                service = service.substring(0, service.length() - 1);
                            }
                            aOrderPojo.setOrderServiceType(service);


                            aOrderPojo.setOrderStatus(aSubCatObj.getString("job_status"));
                            aOrderPojo.setOrderId(aSubCatObj.getString("job_id"));

                            aOrderPojo.setTaskid(aSubCatObj.getString("task_id"));

                            aOrderPojo.setOrderDate(aSubCatObj.getString("job_date"));
                            aOrderPojo.setOrderTime(aSubCatObj.getString("job_time"));


                            if (aSubCatObj.has("tasker_name")) {
                                aOrderPojo.setTaskimage(aSubCatObj.getString("tasker_image"));
                                aOrderPojo.setTaskname(aSubCatObj.getString("tasker_name"));
                            } else {
                                aOrderPojo.setTaskimage("");
                                aOrderPojo.setTaskname("");
                            }


                            myOrdersArrList.add(aOrderPojo);


                        }
                        myOrderAdapter.notifyDataSetChanged();
                        hintshow.setVisibility(View.GONE);


                    } else {
                        // HNDHelper.showResponseErrorAlert(MyOrdersActivity.this, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                spinn.setVisibility(View.GONE);

            }

            @Override
            public void onErrorListener() {
                spinn.setVisibility(View.GONE);
            }
        });
    }

    private void postRequestMyOrderList(final Context aContext, String url, String aOrderListType) {
        myDialog = new ProgressDialogcreated(MyOrdersActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        System.out.println("-------------post my order Response----------------" + aOrderListType);
        jsonParams.put("type", aOrderListType);
        jsonParams.put("page", "1");
        jsonParams.put("perPage", "20");

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------post my order Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        JSONObject response_Object = object.getJSONObject("response");
                        checkPagePos = response_Object.getInt("current_page");
                        JSONArray aMainArr = response_Object.getJSONArray("jobs");

                        myOrdersArrList.clear();
                        for (int a = 0; a < aMainArr.length(); a++) {

                            JSONObject aSubCatObj = aMainArr.getJSONObject(a);
                            MyOrdersPojo aOrderPojo = new MyOrdersPojo();
                            aOrderPojo.setContactNumber(aSubCatObj.getString("contact_number"));
                            aOrderPojo.setCountrycode(aSubCatObj.getString("country_code"));
                            aOrderPojo.setDoCall(aSubCatObj.getString("doCall"));
                            aOrderPojo.setDoCancel(aSubCatObj.getString("doCancel"));
                            aOrderPojo.setDoMsg(aSubCatObj.getString("doMsg"));
                            aOrderPojo.setIsSupport(aSubCatObj.getString("isSupport"));
                            aOrderPojo.setOrderBookingDate(aSubCatObj.getString("booking_date"));
                            aOrderPojo.setUserphoto(aSubCatObj.getString("service_icon"));
                            aOrderPojo.setTaskerid(aSubCatObj.getString("tasker_id"));


                            String service = "";
                            JSONArray service_type = aSubCatObj.getJSONArray("service_type");
                            for (int b = 0; b < service_type.length(); b++) {

                                String value = "" + service_type.getString(b);

                                //JSONObject serobj = aMainArr.getJSONObject(b);
                                service += value + ",";
                            }

                            if (service.endsWith(",")) {
                                service = service.substring(0, service.length() - 1);
                            }
                            aOrderPojo.setOrderServiceType(service);
                            aOrderPojo.setOrderStatus(aSubCatObj.getString("job_status"));
                            aOrderPojo.setOrderId(aSubCatObj.getString("job_id"));
                            aOrderPojo.setTaskid(aSubCatObj.getString("task_id"));
                            aOrderPojo.setOrderDate(aSubCatObj.getString("job_date"));
                            aOrderPojo.setOrderTime(aSubCatObj.getString("job_time"));

                            if (aSubCatObj.has("tasker_name")) {
                                aOrderPojo.setTaskimage(aSubCatObj.getString("tasker_image"));
                                aOrderPojo.setTaskname(aSubCatObj.getString("tasker_name"));
                            } else {
                                aOrderPojo.setTaskimage("");
                                aOrderPojo.setTaskname("");
                            }
                            myOrdersArrList.add(aOrderPojo);


                        }
                        loadDataInAdapter();
                        hintshow.setVisibility(View.GONE);

                    } else {
                        myOrdersArrList.clear();
                        loadDataInAdapter();
                        hintshow.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    private void datePicker(Bundle savedState) {

        dialogCaldroidFragment = new CaldroidFragment();
        dialogCaldroidFragment.setCaldroidListener(caldroidListener);

        // If activity is recovered from rotation
        final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
        if (savedState != null) {
            dialogCaldroidFragment.restoreDialogStatesFromKey(getSupportFragmentManager(), savedState,
                    "DIALOG_CALDROID_SAVED_STATE", dialogTag);
            Bundle args = dialogCaldroidFragment.getArguments();
            if (args == null) {
                args = new Bundle();
                dialogCaldroidFragment.setArguments(args);
            }
        } else {
            // Setup arguments
            Bundle bundle = new Bundle();
            // Setup dialogTitle
            dialogCaldroidFragment.setArguments(bundle);
        }

        Calendar cal = Calendar.getInstance();
        Date currentDate = null;
        Date maximumDate = null;
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
            String formattedDate = df.format(cal.getTime());
            currentDate = df.parse(formattedDate);

            // Max date is next 7 days
            cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 7);
            maximumDate = cal.getTime();

        } catch (ParseException e1) {
            e1.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }


        dialogCaldroidFragment.setMinDate(currentDate);
        dialogCaldroidFragment.setMaxDate(maximumDate);
        dialogCaldroidFragment.show(getSupportFragmentManager(), dialogTag);
        dialogCaldroidFragment.refreshView();
    }

    private void ShowFilterDialog(final Bundle savedInstanceState) {
        //BottomSheetDialog aDialog = new BottomSheetDialog(MyOrdersActivity.this);
//                aDialog.setContentView(R.layout.dialog_filter_orders);
//                aDialog.show();

        mySortDialog = new Dialog(MyOrdersActivity.this);
        mySortDialog.getWindow();
        mySortDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mySortDialog.setContentView(R.layout.dialog_filter_my_orders);
        mySortDialog.setCanceledOnTouchOutside(true);
        mySortDialog.getWindow().getAttributes().windowAnimations = R.style.animations_photo_Picker;
        mySortDialog.show();
        mySortDialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout aCloseLAY = mySortDialog.findViewById(R.id.dialog_filter_my_orders_closeLAY);
        RelativeLayout aApplySortLAY = mySortDialog.findViewById(R.id.dialog_filter_my_orders_apply_LAY);
        final CustomTextView aNameTXT = mySortDialog.findViewById(R.id.dialog_filter_my_order_name_TXT);
        final CustomTextView aDateTXT = mySortDialog.findViewById(R.id.dialog_filter_my_order_date_TXT);
        CustomTextView aRecentBookingTXT = mySortDialog.findViewById(R.id.dialog_filter_my_order_recent_booking_TXT);
        CustomTextView aUpcomingTXT = mySortDialog.findViewById(R.id.dialog_filter_my_order_upcoming_booking_TXT);
        CustomTextView aTodayTXT = mySortDialog.findViewById(R.id.dialog_filter_my_order_today_booking_TXT);
        final RelativeLayout aSortByNameLAY = mySortDialog.findViewById(R.id.dialog_filter_my_orders_name_layout);
        final RelativeLayout aSortByDateLAY = mySortDialog.findViewById(R.id.dialog_filter_my_orders_date_layout);
        RelativeLayout aSortByAscendingLAY = mySortDialog.findViewById(R.id.dialog_filter_my_orders_ascending_LAY);
        RelativeLayout aSortByDescendingLAY = mySortDialog.findViewById(R.id.dialog_filter_my_orders_descending_LAY);

        myFromDateTXT = mySortDialog.findViewById(R.id.dialog_filter_my_orders_from_date_TXT);
        myToDateTXT = mySortDialog.findViewById(R.id.dialog_filter_my_orders_to_date_TXT);
        final RelativeLayout aFromDateLAY = mySortDialog.findViewById(R.id.dialog_filter_my_orders_from_date_LAY);
        final RelativeLayout aToDateLAY = mySortDialog.findViewById(R.id.dialog_filter_my_orders_to_date_LAY);

        final ImageView aNameSelectIMG = mySortDialog.findViewById(R.id.dialog_filter_my_orders_name_select_IMG);
        final ImageView aDateSelectIMG = mySortDialog.findViewById(R.id.dialog_filter_my_orders_date_selectIMG);
        final ImageView aOrderByAscendingIMG = mySortDialog.findViewById(R.id.dialog_filter_my_orders_ascendingIMG);
        final ImageView aOrderByDescendingIMG = mySortDialog.findViewById(R.id.dialog_filter_my_orders_descendingIMG);
        final ImageView aTodayBookingSelectIMG = mySortDialog.findViewById(R.id.dialog_filter_my_orders_today_boooking_IMG);
        final ImageView aRecentBookingSelectIMG = mySortDialog.findViewById(R.id.dialog_filter_my_orders_recent_boooking_IMG);
        final ImageView aUpcomingBookingSelectIMG = mySortDialog.findViewById(R.id.dialog_filter_my_orders_upcoming_boooking_IMG);
        RelativeLayout aTodayBookingLAY = mySortDialog.findViewById(R.id.dialog_filter_my_orders_today_bookingLAY);
        RelativeLayout aRecentBookingLAY = mySortDialog.findViewById(R.id.dialog_filter_my_orders_recent_bookingLAY);
        RelativeLayout aUpcomingBookingLAY = mySortDialog.findViewById(R.id.dialog_filter_my_orders_upcoming_bookingLAY);

        aCloseLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mySortDialog.dismiss();
            }
        });

        if (myCompletedSTR.equals("1") || myCancelledSTR.equals("1")) {
            aTodayBookingLAY.setEnabled(false);
            aRecentBookingLAY.setEnabled(false);
            aUpcomingBookingLAY.setEnabled(false);
            aTodayTXT.setTextColor(Color.parseColor("#DCDCDC"));
            aRecentBookingTXT.setTextColor(Color.parseColor("#DCDCDC"));
            aUpcomingTXT.setTextColor(Color.parseColor("#DCDCDC"));
        }

        aFromDateLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mySelectedDateSTR = "1";
                datePicker(savedInstanceState);
            }
        });

        aToDateLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mySelectedDateSTR = "2";
                datePicker(savedInstanceState);
            }
        });


        aSortByNameLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aNameSelectIMG.setVisibility(View.VISIBLE);
                aDateSelectIMG.setVisibility(View.GONE);

                mySelectedSortFlag = "name";
            }
        });


        aSortByDateLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aNameSelectIMG.setVisibility(View.GONE);
                aDateSelectIMG.setVisibility(View.VISIBLE);

                mySelectedSortFlag = "date";
            }
        });

        aSortByAscendingLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                aOrderByDescendingIMG.setVisibility(View.GONE);
                aOrderByAscendingIMG.setVisibility(View.VISIBLE);
                mySelectedOrderByFlag = "-1";
            }
        });


        aSortByDescendingLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                aOrderByDescendingIMG.setVisibility(View.VISIBLE);
                aOrderByAscendingIMG.setVisibility(View.GONE);
                mySelectedOrderByFlag = "1";
            }
        });

        aTodayBookingLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aTodayBookingSelectIMG.setVisibility(View.VISIBLE);
                aRecentBookingSelectIMG.setVisibility(View.GONE);
                aUpcomingBookingSelectIMG.setVisibility(View.GONE);
                myFilterBookingFlag = "1";
                myFilterTypeSTR = "today";
                aSortByNameLAY.setEnabled(false);
                aSortByDateLAY.setEnabled(false);
                aFromDateLAY.setEnabled(false);
                aToDateLAY.setEnabled(false);
                aNameSelectIMG.setVisibility(View.GONE);
                aDateSelectIMG.setVisibility(View.GONE);
                aNameTXT.setTextColor(Color.parseColor("#DCDCDC"));
                aDateTXT.setTextColor(Color.parseColor("#DCDCDC"));
                myFromDateTXT.setTextColor(Color.parseColor("#DCDCDC"));
                myToDateTXT.setTextColor(Color.parseColor("#DCDCDC"));
            }
        });

        aUpcomingBookingLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aTodayBookingSelectIMG.setVisibility(View.GONE);
                aRecentBookingSelectIMG.setVisibility(View.GONE);
                aUpcomingBookingSelectIMG.setVisibility(View.VISIBLE);
                myFilterBookingFlag = "1";
                myFilterTypeSTR = "upcoming";
                aSortByNameLAY.setEnabled(false);
                aSortByDateLAY.setEnabled(false);
                aFromDateLAY.setEnabled(false);
                aToDateLAY.setEnabled(false);
                aNameSelectIMG.setVisibility(View.GONE);
                aDateSelectIMG.setVisibility(View.GONE);
                aNameTXT.setTextColor(Color.parseColor("#DCDCDC"));
                aDateTXT.setTextColor(Color.parseColor("#DCDCDC"));
                myFromDateTXT.setTextColor(Color.parseColor("#DCDCDC"));
                myToDateTXT.setTextColor(Color.parseColor("#DCDCDC"));
            }
        });

        aRecentBookingLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aTodayBookingSelectIMG.setVisibility(View.GONE);
                aRecentBookingSelectIMG.setVisibility(View.VISIBLE);
                aUpcomingBookingSelectIMG.setVisibility(View.GONE);
                myFilterBookingFlag = "1";
                myFilterTypeSTR = "recent";
                aSortByNameLAY.setEnabled(false);
                aSortByDateLAY.setEnabled(false);
                aFromDateLAY.setEnabled(false);
                aToDateLAY.setEnabled(false);
                aNameSelectIMG.setVisibility(View.GONE);
                aDateSelectIMG.setVisibility(View.GONE);
                aNameTXT.setTextColor(Color.parseColor("#DCDCDC"));
                aDateTXT.setTextColor(Color.parseColor("#DCDCDC"));
                myFromDateTXT.setTextColor(Color.parseColor("#DCDCDC"));
                myToDateTXT.setTextColor(Color.parseColor("#DCDCDC"));
            }
        });


        aApplySortLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myConnectionManager.isConnectingToInternet()) {
                    if (myFilterBookingFlag.equalsIgnoreCase("1")) {
                        postRequestSortOrderList(MyOrdersActivity.this, ServiceConstant.Filter_booking_url, myFilterTypeSTR);
                    } else {
                        postRequestSortOrderList(MyOrdersActivity.this, ServiceConstant.MyJobsList_Url, myOrderType);
                    }
                } else {
                    HNDHelper.showErrorAlert(MyOrdersActivity.this, getResources().getString(R.string.nointernet_text));
                }

                mySortDialog.dismiss();
            }
        });

    }

    private void postRequestSortOrderList(final Context aContext, String url, String aFilterTypeSTR) {
        myDialog = new ProgressDialogcreated(MyOrdersActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        if (aFilterTypeSTR.equalsIgnoreCase("today") || aFilterTypeSTR.equalsIgnoreCase("upcoming") || aFilterTypeSTR.equalsIgnoreCase("recent")) {
            jsonParams.put("user_id", mySession.getUserDetails().getUserId());
            jsonParams.put("type", aFilterTypeSTR);
            jsonParams.put("page", "0");
            jsonParams.put("perPage", "20");
            jsonParams.put("orderby", mySelectedOrderByFlag);
            jsonParams.put("sortby", "");
        } else {

            jsonParams.put("user_id", mySession.getUserDetails().getUserId());
            jsonParams.put("type", aFilterTypeSTR);
            jsonParams.put("page", "0");
            jsonParams.put("perPage", "20");
            jsonParams.put("from", myFromDateTXT.getText().toString());
            jsonParams.put("to", myToDateTXT.getText().toString());
            jsonParams.put("orderby", mySelectedOrderByFlag);
            jsonParams.put("sortby", mySelectedSortFlag);

        }

        System.out.println("---------orderby------------" + mySelectedOrderByFlag);
        System.out.println("---------to------------" + myToDateTXT.getText().toString());
        System.out.println("---------from------------" + myFromDateTXT.getText().toString());
        System.out.println("---------sortby------------" + mySelectedSortFlag);
        System.out.println("---------My Jobs user_id------------" + mySession.getUserDetails().getUserId());
        System.out.println("---------My Jobs type------------" + aFilterTypeSTR);
        System.out.println("---------My Jobs Page page------------" + "1");
        System.out.println("---------My Jobs url------------" + url);

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------post my order filter list Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {


                    } else {
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    public void callTaskerWithPosition(String isSupport, final String aCountryCode, final String aContctNo) {

        myPhoneSTR = aContctNo;
        myCountryCodeSTR = aCountryCode;

        if (isSupport.equalsIgnoreCase("Yes")) {

            try {

                final PkDialog mDialog = new PkDialog(MyOrdersActivity.this);
                mDialog.setDialogTitle(getResources().getString(R.string.activity_my_orders_no_providers));
                mDialog.setDialogMessage(getResources().getString(R.string.activity_my_orders_want_to_call_support_team));
                mDialog.setPositiveButton(
                        getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                if (aContctNo != null) {

                                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                    callIntent.setData(Uri.parse("tel:" + aCountryCode + aContctNo));
                                    startActivity(callIntent);

                                 /*   if (Build.VERSION.SDK_INT >= 23) {
                                        // Marshmallow+
                                        if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                                            requestPermission();

                                        } else {
                                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                                            callIntent.setData(Uri.parse("tel:" + aCountryCode + aContctNo));
                                            startActivity(callIntent);
                                        }
                                    } else {
                                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                                        callIntent.setData(Uri.parse("tel:" + aCountryCode + aContctNo));
                                        startActivity(callIntent);

                                    }*/
                                } else {
                                    HNDHelper.showErrorAlert(MyOrdersActivity.this, getResources().getString(R.string.activity_my_orders_error_in_contact_provider));
                                }
                            }
                        }
                );
                mDialog.setNegativeButton(
                        getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        }
                );

                mDialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + aCountryCode + aContctNo));
            startActivity(callIntent);


            /*
            if (aContctNo != null) {
                int version = Build.VERSION.SDK_INT;
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                        requestPermission();
                    } else {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + aCountryCode + aContctNo));
                        startActivity(callIntent);
                    }
                } else {

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + aCountryCode + aContctNo));
                    startActivity(callIntent);
                }
            }*/

        }

    }


    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(MyOrdersActivity.this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + myCountryCodeSTR + myPhoneSTR));
                    startActivity(callIntent);
                }
                break;


            case PERMISSION_REQUEST_CODES:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + myCountryCodeSTR + myPhoneSTR));
                    startActivity(callIntent);
                }
                break;

        }
    }

    public void CancelOrder(final String aOrderID, final int aPosition) {

        try {


            SharedPreferences getcard;
            getcard = getApplicationContext().getSharedPreferences("cancejobid", 0); // 0 - for private mode
            SharedPreferences.Editor editor = getcard.edit();
            editor.putString("jobid", "" + aOrderID);
            editor.commit();

            // Toast.makeText(getApplicationContext(),""+getcard.getString("catgeoryid",""),Toast.LENGTH_LONG).show();
            Intent aSelectCrftsMnIntent = new Intent(MyOrdersActivity.this, Cancel_Job_Reason.class);
            startActivity(aSelectCrftsMnIntent);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void postCancelJobRequest(String url, String aOrderID, final int aPosition) {
        myDialog = new ProgressDialogcreated(MyOrdersActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("job_id", aOrderID);
        jsonParams.put("reason", "er");

        System.out.println("---------My Jobss------------" + aOrderID);
        System.out.println("---------My Jobs cancel url------------" + url);

        ServiceRequest mRequest = new ServiceRequest(MyOrdersActivity.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------My Jobss------------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        JSONObject response_Object = object.getJSONObject("response");
                        myOrdersArrList.remove(aPosition);


                    } else {
                        HNDHelper.showResponseErrorAlert(MyOrdersActivity.this, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public class MyOrdersAdapter extends BaseAdapter {
        private ArrayList<MyOrdersPojo> myOrderArrList;
        private LayoutInflater myInflater;
        private Context myContext;

        public MyOrdersAdapter(Context aContext, ArrayList<MyOrdersPojo> aOrderArrList) {
            this.myOrderArrList = aOrderArrList;
            this.myContext = aContext;
            myInflater = LayoutInflater.from(aContext);
        }

        @Override
        public int getCount() {
            return myOrderArrList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MyOrdersAdapter.ViewHolder holder;
            if (convertView == null) {
                holder = new MyOrdersAdapter.ViewHolder();
                convertView = myInflater.inflate(R.layout.layout_inflate_my_orders_list_item, parent, false);

                holder.myOrderIDTXT = convertView.findViewById(R.id.layout_inflater_my_orders_id_TXT);
                holder.myOrderJobTypeTXT = convertView.findViewById(R.id.layout_inflater_my_orders_job_type_TXT);
                holder.myOrderJobIMG = convertView.findViewById(R.id.layout_inflater_my_orders_Job_ImageView);
                holder.myOrderDateTXT = convertView.findViewById(R.id.layout_inflater_my_orders_date_TXT);
                holder.myOrderTimeTXT = convertView.findViewById(R.id.layout_inflater_my_orders_time_TXT);
                holder.myChatLAY = convertView.findViewById(R.id.layout_inflater_my_orders_chat_LAY);
                holder.myOrderStatusTXT = convertView.findViewById(R.id.layout_inflater_my_orders_status_TXT);
                holder.myCallLAY = convertView.findViewById(R.id.layout_inflater_my_orders_call_LAY);
                holder.myCancelLAY = convertView.findViewById(R.id.layout_inflater_my_orders_cancel_LAY);
                holder.myOptionalLAY = convertView.findViewById(R.id.layout_inflater_my_orders_optionsLAY);

                convertView.setTag(holder);
            } else {
                holder = (MyOrdersAdapter.ViewHolder) convertView.getTag();
            }

            holder.myOrderJobTypeTXT.setText(myOrderArrList.get(position).getOrderServiceType());
            holder.myOrderTimeTXT.setText(myOrderArrList.get(position).getOrderTime());
            holder.myOrderDateTXT.setText(myOrderArrList.get(position).getOrderDate().replace("/", "."));
            holder.myOrderIDTXT.setText(myOrderArrList.get(position).getOrderId());

            if (myOrderArrList.get(position).getTaskimage().contains("http")) {
                Picasso.get().load(myOrderArrList.get(position).getUserphoto())
                        .resize(500, 500)
                        .placeholder(R.drawable.icon_placeholder)
                        .into(holder.myOrderJobIMG);
            } else {
                if (!myOrderArrList.get(position).getTaskimage().isEmpty()) {
                    Picasso.get().load(/*ServiceConstant.Base_Url +*/ myOrderArrList.get(position).getTaskimage())
                            .resize(500, 500)
                            .placeholder(R.drawable.icon_placeholder)
                            .into(holder.myOrderJobIMG);
                } else {
                    Picasso.get().load(ServiceConstant.Base_Url + myOrderArrList.get(position).getTaskimage())
                            .resize(500, 500)
                            .placeholder(R.drawable.icon_placeholder)
                            .into(holder.myOrderJobIMG);
                }

            }


            holder.myOrderStatusTXT.setText(myOrderArrList.get(position).getOrderStatus());

            //Call Layout Show/Hide Function
            if (myOrderArrList.get(position).getDoCall().equalsIgnoreCase("Yes")) {
                holder.myCallLAY.setVisibility(View.VISIBLE);
            } else {
                holder.myCallLAY.setVisibility(View.GONE);
            }

            //Chat Layout Show/Hide Function
            if (myOrderArrList.get(position).getDoMsg().equalsIgnoreCase("Yes")) {
                holder.myChatLAY.setVisibility(View.VISIBLE);
            } else {
                holder.myChatLAY.setVisibility(View.GONE);
            }

            //Cancel Layout Show/Hide Function
            if (myOrderArrList.get(position).getDoCancel().equalsIgnoreCase("Yes") && !myOrderArrList.get(position).getOrderStatus().equalsIgnoreCase("StartJob")) {
                holder.myCancelLAY.setVisibility(View.VISIBLE);
            } else {
                holder.myCancelLAY.setVisibility(View.GONE);
            }

            //Show and Hide the Option Layout
            if (myOrderArrList.get(position).getDoCall().equalsIgnoreCase("No") && myOrderArrList.get(position).getDoMsg().equalsIgnoreCase("No") && myOrderArrList.get(position).getDoCancel().equalsIgnoreCase("No")) {
                holder.myOptionalLAY.setVisibility(View.GONE);
            } else {
                holder.myOptionalLAY.setVisibility(View.VISIBLE);
            }

            holder.myCallLAY.setOnClickListener(new MyOrdersAdapter.onCallClickListener(position));
            holder.myCancelLAY.setOnClickListener(new MyOrdersAdapter.onCancelClickListener(position));
            holder.myChatLAY.setOnClickListener(new MyOrdersAdapter.onChatClickListener(position));
            return convertView;
        }

        private class ViewHolder {
            LinearLayout myChatLAY, myCallLAY, myCancelLAY;
            private TextView myOrderIDTXT, myOrderDateTXT, myOrderTimeTXT, myOrderStatusTXT;
            private TextView myOrderJobTypeTXT;
            private ImageView myOrderJobIMG;
            private LinearLayout myOptionalLAY;
        }

        private class onCallClickListener implements View.OnClickListener {
            int aPosition;

            public onCallClickListener(int position) {
                aPosition = position;
            }

            @Override
            public void onClick(View v) {
                MyOrdersActivity aOrderAct = (MyOrdersActivity) myContext;
                aOrderAct.callTaskerWithPosition(myOrderArrList.get(aPosition).getIsSupport(), myOrderArrList.get(aPosition).getCountrycode(), myOrderArrList.get(aPosition).getContactNumber());
            }
        }

        private class onChatClickListener implements View.OnClickListener {
            int aPosition;

            public onChatClickListener(int position) {
                aPosition = position;
            }

            @Override
            public void onClick(View v) {


                if (myOrdersArrList.get(aPosition).getTaskname().equalsIgnoreCase("") || (myOrdersArrList.get(aPosition).getTaskname().equalsIgnoreCase(" "))) {

                    final PkDialog mDialog = new PkDialog(MyOrdersActivity.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.dialog_sorry));
                    mDialog.setDialogMessage(getResources().getString(R.string.class_my_orders_unavaible));
                    mDialog.setPositiveButton(
                            getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                }
                            }
                    );

                    mDialog.show();


                } else {
                    ChatMessageServicech.tasker_id = "";
                    ChatMessageServicech.task_id = "";

                    Intent intent = new Intent(MyOrdersActivity.this, ChatPage.class);
                    intent.putExtra("TaskerId", myOrdersArrList.get(aPosition).getTaskerid());
                    intent.putExtra("TaskId", myOrdersArrList.get(aPosition).getTaskid());
                    intent.putExtra("name", myOrdersArrList.get(aPosition).getTaskname());
                    intent.putExtra("iamge", myOrdersArrList.get(aPosition).getTaskimage());
                    startActivity(intent);

                }


            }
        }

        private class onCancelClickListener implements View.OnClickListener {
            int aPosition;

            public onCancelClickListener(int position) {
                aPosition = position;
            }

            @Override
            public void onClick(View v) {
                MyOrdersActivity aOrderAct = (MyOrdersActivity) myContext;
                aOrderAct.CancelOrder(myOrderArrList.get(aPosition).getOrderId(), aPosition);
            }
        }
    }


}
