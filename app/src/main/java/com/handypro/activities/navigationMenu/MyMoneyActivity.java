package com.handypro.activities.navigationMenu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.HomeActivity;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.CurrencySymbolConverter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by CAS63 on 3/2/2018.
 */

public class MyMoneyActivity extends AppCompatActivity {
    private RelativeLayout myBackLAY;
    private CustomTextView myWalletAmountTXT, myRetryTXT;
    private CustomButton myMinAmountBTN, myBtwAmountBTN, myMaxAmountBTN, myProceedPaymntBTN;
    private CustomEdittext myEnterAmountET;
    private ConnectionDetector myConnectionManager;
    private ProgressDialogcreated myDialog;
    private SharedPreference mySession;
    int maxamounttocompare=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_money);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInitialize();
        LoadMoneyDetails();
    }

    private void LoadMoneyDetails() {
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestMyMoneyDetail(MyMoneyActivity.this, ServiceConstant.plumbal_money_url);
        } else {
            HNDHelper.showErrorAlert(MyMoneyActivity.this, getResources().getString(R.string.nointernet_text));
        }
    }

    private void postRequestMyMoneyDetail(final Context aContext, String url) {
        myDialog = new ProgressDialogcreated(MyMoneyActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------post my order Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {

                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            String sCurrency_code = response_object.getString("currency");
                            String  sCurrentBalance = response_object.getString("current_balance");
                            String  sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(sCurrency_code);

                            myWalletAmountTXT.setText(sCurrencySymbol+sCurrentBalance);





                            Object check_recharge_boundary_object = response_object.get("recharge_boundary");
                            if (check_recharge_boundary_object instanceof JSONObject) {
                                JSONObject recharge_object = response_object.getJSONObject("recharge_boundary");
                                if (recharge_object.length() > 0) {
                                    myMinAmountBTN.setText(sCurrencySymbol+recharge_object.getString("min_amount"));
                                    myBtwAmountBTN.setText(sCurrencySymbol+recharge_object.getString("middle_amount"));
                                    myMaxAmountBTN.setText(sCurrencySymbol+recharge_object.getString("max_amount"));
                                    if(recharge_object.has("max_amount"))
                                    {

                                        maxamounttocompare=Integer.parseInt(recharge_object.getString("max_amount"));
                                    }
                                    else
                                    {
                                        maxamounttocompare=0;
                                    }


                                } else {

                                }
                            } else {

                            }

                        }




                    } else {
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    private void classAndWidgetInitialize() {
        mySession = new SharedPreference(MyMoneyActivity.this);
        myConnectionManager = new ConnectionDetector(MyMoneyActivity.this);
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_my_money_LAY_back);
        myWalletAmountTXT = (CustomTextView) findViewById(R.id.activity_my_money_amount_textView);
        myMaxAmountBTN = (CustomButton) findViewById(R.id.activity_my_money_maximum_amt_button);
        myMinAmountBTN = (CustomButton) findViewById(R.id.activity_my_money_minimum_amt_button);
        myBtwAmountBTN = (CustomButton) findViewById(R.id.activity_my_money_between_amt_button);
        myEnterAmountET = (CustomEdittext) findViewById(R.id.activity_my_money_enter_amount_ET);
        myProceedPaymntBTN = (CustomButton) findViewById(R.id.activity_my_money_BTN_stripe);



        myEnterAmountET.addTextChangedListener(EditorWatcher);

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        myProceedPaymntBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myEnterAmountET.getText().toString().equalsIgnoreCase("")) {
                    HNDHelper.showErrorAlert(MyMoneyActivity.this, getResources().getString(R.string.activity_my_money_enter_amount_alert_label));

                }
                else
                {
                    if (myConnectionManager.isConnectingToInternet()) {
                        SharedPreferences getcard;
                        getcard = getApplicationContext().getSharedPreferences("amounttosend", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = getcard.edit();
                        editor.putString("amt", ""+myEnterAmountET.getText().toString());
                        editor.commit();


                        int enterdamount=0;
                        if(myEnterAmountET.getText().toString().matches("[0-9]+"))
                        {
                            enterdamount=Integer.parseInt(myEnterAmountET.getText().toString());
                        }
                        else
                        {
                            enterdamount=Integer.parseInt(myEnterAmountET.getText().toString().substring(1,myEnterAmountET.getText().toString().length()));
                        }





                        if(enterdamount>maxamounttocompare)
                        {
                            HNDHelper.showResponseErrorAlert(MyMoneyActivity.this, "Entered amount is too high than maximum amount.");
                        }
                        else
                        {
                            Intent aSelectCrftsMnIntent = new Intent(MyMoneyActivity.this, addmoneypayment.class);
                            startActivity(aSelectCrftsMnIntent);
                        }

                        // Toast.makeText(getApplicationContext(),""+getcard.getString("catgeoryid",""),Toast.LENGTH_LONG).show();
                       /* Intent aSelectCrftsMnIntent = new Intent(MyMoneyActivity.this, addmoneypayment.class);
                        startActivity(aSelectCrftsMnIntent);*/
                    } else {
                        HNDHelper.showErrorAlert(MyMoneyActivity.this, getResources().getString(R.string.nointernet_text));
                    }
                }

            }

        });

        myMinAmountBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myEnterAmountET.setText(myMinAmountBTN.getText().toString());
                myMinAmountBTN.setBackgroundColor(0xFFF85603);
                myMaxAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                myBtwAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                myEnterAmountET.setSelection(myEnterAmountET.getText().length());
            }
        });

        myMaxAmountBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myEnterAmountET.setText(myMaxAmountBTN.getText().toString());
                myMinAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                myMaxAmountBTN.setBackgroundColor(0xFFF85603);
                myBtwAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                myEnterAmountET.setSelection(myEnterAmountET.getText().length());
            }
        });

        myBtwAmountBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myEnterAmountET.setText(myBtwAmountBTN.getText().toString());
                myMinAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                myBtwAmountBTN.setBackgroundColor(0xFFF85603);
                myMaxAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                myEnterAmountET.setSelection(myEnterAmountET.getText().length());
            }
        });
    }

    private void postRequestAddMoney(final Context aContext, String url) {
        myDialog = new ProgressDialogcreated(MyMoneyActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("total_amount", myEnterAmountET.getText().toString().trim());

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------post add money Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {


                    } else {
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }


    private final TextWatcher EditorWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {

            String strEnteredVal = myEnterAmountET.getText().toString();
            if (!strEnteredVal.equals("")) {






                if (myEnterAmountET.getText().toString().equals(myMinAmountBTN.getText().toString())) {
                    myMinAmountBTN.setBackgroundColor(0xFFF85603);
                    myBtwAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                    myMaxAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                } else if (myEnterAmountET.getText().toString().equals(myBtwAmountBTN.getText().toString())) {
                    myBtwAmountBTN.setBackgroundColor(0xFFF85603);
                    myMinAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                    myMaxAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                } else if (myEnterAmountET.getText().toString().equals(myMaxAmountBTN.getText().toString())) {
                    myMaxAmountBTN.setBackgroundColor(0xFFF85603);
                    myMinAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                    myBtwAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                } else {
                    myMinAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                    myBtwAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                    myMaxAmountBTN.setBackground(getResources().getDrawable(R.drawable.rectanglr_border_bg));
                }
            }
        }
    };







    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

}
