package com.handypro.activities.navigationMenu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.github.badoualy.datepicker.DatePickerTimeline;
import com.github.badoualy.datepicker.MonthView;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.Pojo.DataModel;
import com.handypro.activities.OrderDetailActivity;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;


/**
 * Created by CAS63 on 2/21/2018.
 */

public class reschedulepage extends AppCompatActivity {


    private estimateacceptdb mHelper;
    private SQLiteDatabase dataBase;
    ArrayList<String> checktime = new ArrayList<String>();
    private LinearLayout activity_shedule_appointment_page_free_estimate_LAY;
    private SharedPreference mySession;
    //    private MaterialCalendarView myCalendar;
    final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    final SimpleDateFormat formatter1 = new SimpleDateFormat("dd");
    final SimpleDateFormat formatter2 = new SimpleDateFormat("MM");
    private int myDateInt, myMonthInt, myCounterInt;
    private String myDateSTR = "", myLatitude = "", myLongitude = "", myZipCodeSTR = "", myMinuteStr = "", myApprxTimeSTR = "", mySelectedTimeSTR = "";

    private GridView myTimeRecyclerView;
    private AppointmentTimeAdapter myAdapter;
    private LinearLayout myBackLAY;
    ArrayList<DataModel> dataModels;
    ProgressDialogcreated myDialog;
    private int myIntTimePos = 0;
    public static boolean myCurrDateSelected = true;
    private String myBookingIDStr = "";
    TextView higlighttext;
    String totalday;
    int estimatecount = 0;
    int estimatehour = 0;
    String estimatehours = "";

    private Boolean isInternetPresent = false;

    private ConnectionDetector cd;
    LinearLayout donetoreschedule;
    ArrayList<String> taskname = new ArrayList<String>();
    ArrayList<String> taskhours = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reschedulepage);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        cd = new ConnectionDetector(reschedulepage.this);
        mHelper = new estimateacceptdb(this);
        mySession = new SharedPreference(reschedulepage.this);
        dataModels = new ArrayList<>();
        taskname.clear();
        taskhours.clear();
        initClassAndWidgets();
        //currentdatetime();
        clickListener();
        myCalInit();


    }


    private void recyclerViewAdapter() {
        myAdapter = new AppointmentTimeAdapter(dataModels, getApplicationContext());
        myTimeRecyclerView.setAdapter(myAdapter);


    }

    private void myCalInit() {
//        myCalendar.setSelectedDate(new Date());
        myDateSTR = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
//        myCalendar.setDateSelected(new Date(), true);
//        myCalendar.state().edit()
//                .setMinimumDate(new Date())
//                .setFirstDayOfWeek(Calendar.MONDAY)
//                .commit();
    }


    private void clickListener() {

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }


    private void initClassAndWidgets() {

        SharedPreferences Job_idpref = reschedulepage.this.getSharedPreferences("sendjobid", MODE_PRIVATE);
        estimatehours = Job_idpref.getString("estimatehours", "");
        higlighttext = findViewById(R.id.activity_shedule_appointment_page_free_estimate_TXT);
        activity_shedule_appointment_page_free_estimate_LAY = findViewById(R.id.activity_shedule_appointment_page_free_estimate_LAY);


        int IntEstimateNumber = 0;
        float FloatEstimateNumber = 0;

        assert estimatehours != null;
        if (estimatehours.length() >= 1) {
            if (estimatehours.matches("[-+]?[0-9]*\\.?[0-9]+")) {
                FloatEstimateNumber = Float.parseFloat(estimatehours);
            } else if (estimatehours.matches("[0-9]+")) {
                IntEstimateNumber = Integer.parseInt(estimatehours);
            }
            if (IntEstimateNumber > 12 || FloatEstimateNumber > 12) {
                higlighttext.setText(getResources().getString(R.string.class_reschedulePage_approx) + " " + estimatehours + " " + getResources().getString(R.string.contactprocrew));
                higlighttext.setVisibility(View.GONE);
                activity_shedule_appointment_page_free_estimate_LAY.setVisibility(View.GONE);

            } else {
                higlighttext.setText(getResources().getString(R.string.class_reschedulePage_approx) + " " + estimatehours + " " + getResources().getString(R.string.class_reschedulePage_hours));
            }
        } else {
            higlighttext.setText(getResources().getString(R.string.class_reschedulePage_approx) + " " + estimatehours + " " + getResources().getString(R.string.class_reschedulePage_hours));
        }


        myTimeRecyclerView = findViewById(R.id.activity_shedule_appointment_recyclerview);
        myBackLAY = findViewById(R.id.activity_shedule_appoinment_LAY_back);

        donetoreschedule = findViewById(R.id.donetoreschedule);

        donetoreschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mySelectedTimeSTR.length() < 2) {
                    HNDHelper.showErrorAlert(reschedulepage.this, getResources().getString(R.string.bookingtime));

                } else if (totalday.length() < 2) {
                    HNDHelper.showErrorAlert(reschedulepage.this, getResources().getString(R.string.bookingdate));
                } else {

                    sendrequest(reschedulepage.this, ServiceConstant.Estimateapproval);
                    //  HNDHelper.showErrorAlert(reschedulepage.this, getResources().getString(R.string.nointernet_text));
                }
            }
        });


        DatePickerTimeline timeline = findViewById(R.id.timeline);
        timeline.setDateLabelAdapter(new MonthView.DateLabelAdapter() {
            @Override
            public CharSequence getLabel(Calendar calendar, int index) {
                return (calendar.get(Calendar.MONTH) + 1) + "/" + (calendar.get(Calendar.YEAR) % 2000);
            }
        });


        Calendar calendar = Calendar.getInstance();
        int thisYear = calendar.get(Calendar.YEAR);
        final int thisMonth = calendar.get(Calendar.MONTH);
        int thisDay = calendar.get(Calendar.DAY_OF_MONTH);


        String month = "" + (thisMonth + 1);
        String day = "" + thisDay;

        if ((thisMonth + 1) <= 9) {
            month = "0" + (thisMonth + 1);
        }
        if (thisDay <= 9) {
            day = "0" + thisDay;
        }

        totalday = month + "/" + day + "/" + thisYear;
        //Toast.makeText(reschedulepage.this,totalday,Toast.LENGTH_SHORT).show();

        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            paymentPost(reschedulepage.this, ServiceConstant.Estimateavailability);
        } else {
            HNDHelper.showErrorAlert(reschedulepage.this, getResources().getString(R.string.nointernet_text));
        }

        timeline.setFirstVisibleDate(thisYear, thisMonth, thisDay);
        timeline.setLastVisibleDate(thisYear, thisMonth + 2, thisDay);
        timeline.setOnDateSelectedListener(new DatePickerTimeline.OnDateSelectedListener() {
            @Override
            public void onDateSelected(int year, int month, int day, int index) {
                String monthss = "" + (month + 1);
                String dayss = "" + day;

                if ((month + 1) <= 9) {
                    monthss = "0" + (month + 1);
                }
                if (day <= 9) {
                    dayss = "0" + day;
                }

                totalday = monthss + "/" + dayss + "/" + year;
                dataModels.clear();
                checktime.clear();

                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                String currentdate = df.format(c);
                //   Toast.makeText(reschedulepage.this,currentdate,Toast.LENGTH_SHORT).show();
                if (totalday.equalsIgnoreCase(currentdate)) {

                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        paymentPost(reschedulepage.this, ServiceConstant.Estimateavailability);
                    } else {
                        HNDHelper.showErrorAlert(reschedulepage.this, getResources().getString(R.string.nointernet_text));
                    }
                    // currentdatetime();
                    //  Toast.makeText(reschedulepage.this,"current"+totalday,Toast.LENGTH_SHORT).show();
                } else {

                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        paymentPost(reschedulepage.this, ServiceConstant.Estimateavailability);
                    } else {
                        HNDHelper.showErrorAlert(reschedulepage.this, getResources().getString(R.string.nointernet_text));
                    }
                    // someotherdate();
                    // Toast.makeText(reschedulepage.this,totalday,Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent data = new Intent();
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, data);
        } else {
            getParent().setResult(Activity.RESULT_OK, data);
        }

        finish();
    }


    public class AppointmentTimeAdapter extends ArrayAdapter<DataModel> {

        Context mContext;


        private class ViewHolder {
            TextView layout_inflater_appointment_time_list_item_TXT;
            LinearLayout colourchange;

        }

        AppointmentTimeAdapter(ArrayList<DataModel> data, Context context) {
            super(context, R.layout.layout_inflater_appointment_time_list_item, data);
            this.mContext = context;

        }


        @NonNull
        public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
            // Get the data item for this position
            final DataModel dataModel = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            ViewHolder viewHolder; // view lookup cache stored in tag

            final View result;

            if (convertView == null) {

                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.layout_inflater_appointment_time_list_item, parent, false);
                viewHolder.layout_inflater_appointment_time_list_item_TXT = convertView.findViewById(R.id.layout_inflater_appointment_time_list_item_TXT);

                viewHolder.colourchange = convertView.findViewById(R.id.colourchange);
                result = convertView;

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
                result = convertView;
            }


            viewHolder.layout_inflater_appointment_time_list_item_TXT.setText(dataModel.getName());

            if (dataModel.getType().equalsIgnoreCase("0")) {
                viewHolder.colourchange.setBackgroundResource(R.drawable.bookingtimecolor);
            } else if (dataModel.getType().equalsIgnoreCase("2")) {
                viewHolder.colourchange.setBackgroundResource(R.drawable.alreadybooked);
            } else {
                viewHolder.colourchange.setBackgroundResource(R.drawable.bookedred);
            }
            viewHolder.layout_inflater_appointment_time_list_item_TXT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (dataModel.getType().equalsIgnoreCase("2")) {
                        HNDHelper.showErrorAlert(reschedulepage.this, getResources().getString(R.string.unavailabletime));
                    } else {
                        if (dataModel.getType().equalsIgnoreCase("0")) {

                            for (int k = 0; k < dataModels.size(); k++) {
                                String onevalue = dataModels.get(k).getType();
                                if (onevalue.equalsIgnoreCase("1")) {
                                    String name = dataModels.get(k).getName();
                                    dataModels.set(k, new DataModel(name, "0", "", ""));
                                }

                            }


                            try {
                                SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm", Locale.US);
                                SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm aa", Locale.US);
                                Date date = parseFormat.parse(dataModel.getName());
                                System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
                                mySelectedTimeSTR = displayFormat.format(date);

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            dataModels.set(position, new DataModel(dataModel.getName(), "1", "", ""));
                            myAdapter.notifyDataSetChanged();
                        } else {
                            mySelectedTimeSTR = "";
                            dataModels.set(position, new DataModel(dataModel.getName(), "0", "", ""));
                            myAdapter.notifyDataSetChanged();
                        }
                    }


                }
            });

            // Return the completed view to render on screen
            return convertView;
        }
    }


    private void paymentPost(Context mContext, String url) {

        myDialog = new ProgressDialogcreated(reschedulepage.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
        String taskerid = Job_idpref.getString("taskerid", "");

        jsonParams.put("tasker_id", taskerid);
        jsonParams.put("date", totalday);
        jsonParams.put("overall_taskhours", estimatehours);

        Log.e("payment", taskerid);
        Log.e("payment", totalday);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);


                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        dataModels.clear();

                        JSONObject response_Object = object.getJSONObject("response");
                        JSONArray service_type = response_Object.getJSONArray("schedules");
                        for (int b = 0; b < service_type.length(); b++) {
                            JSONObject aSubCatObj = service_type.getJSONObject(b);
                            String time = aSubCatObj.getString("time");
                            String available = aSubCatObj.getString("available");
                            if (available.equalsIgnoreCase("1")) {
                                available = "0";
                            } else if (available.equalsIgnoreCase("0")) {
                                available = "2";
                            }
                            dataModels.add(new DataModel(time, available, "", ""));
                        }

                        int IntEstimateNumber = 0;
                        float FloatEstimateNumber = 0;
                        if (estimatehours.matches("[-+]?[0-9]*\\.?[0-9]+")) {
                            FloatEstimateNumber = Float.parseFloat(estimatehours);
                        } else if (estimatehours.matches("[0-9]+")) {
                            IntEstimateNumber = Integer.parseInt(estimatehours);
                        }

                        if (IntEstimateNumber > 12 || FloatEstimateNumber > 12) {
                            final PkDialog pkDialog = new PkDialog(reschedulepage.this);
                            String Number = "";

                            JSONObject FranJSONObject = response_Object.getJSONObject("franchisee_phonenumber");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                                Number = PhoneNumberUtils.formatNumber(/*FranJSONObject.getString("code") + */FranJSONObject.getString("number"), "US");
                            } else {
                                //Deprecated method
                                Number = PhoneNumberUtils.formatNumber(/*FranJSONObject.getString("code") + */FranJSONObject.getString("number"));
                            }

                            pkDialog.setMultiScheduleErrorDialog(Number);


                            pkDialog.setNegativeButton(getResources().getString(R.string.action_cancel), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    pkDialog.dismiss();
                                }
                            });

                            pkDialog.setPositiveButton(getResources().getString(R.string.activity_shedule_a_call_label), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    pkDialog.dismiss();
                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                                    callIntent.setData(Uri.parse("tel:" + "9245524545"));
                                    startActivity(callIntent);
                                }
                            });

                            pkDialog.show();
                        }


                        myAdapter = new AppointmentTimeAdapter(dataModels, getApplicationContext());
                        myTimeRecyclerView.setAdapter(myAdapter);

                    } else {
                        dataModels.clear();
                        myAdapter = new AppointmentTimeAdapter(dataModels, getApplicationContext());
                        myTimeRecyclerView.setAdapter(myAdapter);
                        HNDHelper.showResponseErrorAlert(reschedulepage.this, getResources().getString(R.string.class_reschedulePage_no_bokking));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }


    private void sendrequest(final Context aContext, String url) {


        myDialog = new ProgressDialogcreated(reschedulepage.this);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", mySession.getUserDetails().getUserId());
        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        params.put("job_id", Job_id);
        params.put("approval_status", "1");
        params.put("pickup_date", "" + totalday);
        params.put("pickup_time", "" + mySelectedTimeSTR);


        int yu = 0;
        mHelper = new estimateacceptdb(this);
        dataBase = mHelper.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + estimateacceptdb.TABLE_NAME, null);
        if (mCursor.moveToFirst()) {
            do {
                String serviceid = mCursor.getString(mCursor.getColumnIndex(estimateacceptdb.KEY_SERVICEID));
                String status = mCursor.getString(mCursor.getColumnIndex(estimateacceptdb.KEY_STATUS));
                params.put("est[" + yu + "][status]", status);
                params.put("est[" + yu + "][service_id]", serviceid);
                yu++;
            } while (mCursor.moveToNext());
        }


        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------get Confirm Category list Response----------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    String message = object.getString("response");
                    if (object.getString("status").equalsIgnoreCase("1")) {


                        final PkDialog mDialog = new PkDialog(reschedulepage.this);
                        mDialog.setDialogTitle("CONGRATULATIONS!");
                        mDialog.setDialogMessage("Your Job Has Been Successfully Scheduled.\n\nWe are Excited To Serve You And Care For Your Home");
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra("EXIT", true);
                                        startActivity(intent);
                                        finish();
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    }
                                }
                        );
                        mDialog.show();


                    } else {
                        final PkDialog mDialog = new PkDialog(reschedulepage.this);
                        mDialog.setDialogTitle("SORRY");
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();
                    }
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block


                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

}