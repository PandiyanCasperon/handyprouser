package com.handypro.activities.navigationMenu;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.handypro.R;
import com.handypro.fragment.TaskTransactionFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CAS63 on 3/2/2018.
 */

public class TransactionActivity extends AppCompatActivity {
    private RelativeLayout myBackLAY;
    private ViewPager myViewPager;
    private TabLayout myTransTabLAY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInitialize();
    }

    private void classAndWidgetInitialize(){
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_transaction_LAY_back);
        myViewPager = (ViewPager) findViewById(R.id.activity_transaction_tab_viewpager);
        myTransTabLAY = (TabLayout) findViewById(R.id.activity_transaction_tabLAY);
        setupViewPager(myViewPager);
        myTransTabLAY.setupWithViewPager(myViewPager);

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setupViewPager(ViewPager aViewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new TaskTransactionFragment(), getResources().getString(R.string.activity_transaction_menu_TXT_title_task_transaction));
       // adapter.addFragment(new WalletTransactionFragment(), getResources().getString(R.string.activity_transaction_menu_TXT_title_wallet_transaction));
        aViewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
