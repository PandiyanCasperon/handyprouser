package com.handypro.activities.navigationMenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.handypro.Pojo.acceptimage;
import com.handypro.R;

import java.util.List;


public class bookimageadapter extends RecyclerView.Adapter<bookimageadapter.MyViewHolder> {

    private List<acceptimage> moviesList;
    private Context ctxx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;

        private MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);

        }
    }

    public bookimageadapter(List<acceptimage> moviesList, Context ctx) {
        this.moviesList = moviesList;
        this.ctxx = ctx;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.acceptimagelay, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final acceptimage movie = moviesList.get(position);

        if (movie.getTitle().isEmpty()) {
            holder.image.setImageResource(R.drawable.noimageavailable);
        } else {
            /*Picasso.with(ctxx)
                    .load("file://" + movie.getTitle())
                    .placeholder(R.drawable.noimageavailable)   // optional
                    .error(R.drawable.noimageavailable)      // optional
                    // optional
                    .into(holder.image);*/

            Glide.with(ctxx)
                    .load(/*"file://" + */movie.getTitle())
                    .fitCenter()
                    .centerCrop()
                    .into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
