package com.handypro.activities.navigationMenu;

/**
 * Created by user127 on 08-05-2018.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class estimateacceptdb extends SQLiteOpenHelper {

    public static String DATABASE_NAME="estimateacceptdata";
    public static final String TABLE_NAME="estimate";

    public static final String KEY_SERVICEID="fname";
    public static final String KEY_STATUS="lname";
    public static final String KEY_ID="id";

    public estimateacceptdb(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE="CREATE TABLE "+TABLE_NAME+" ("+KEY_ID+" INTEGER PRIMARY KEY, "+KEY_SERVICEID+" TEXT, "+KEY_STATUS+" TEXT)";
        db.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

}