package com.handypro.activities;

/**
 * Created by user127 on 18-04-2018.
 */

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.handypro.R;
import com.squareup.picasso.Picasso;

import java.util.List;



public class beforeimageadapter extends RecyclerView.Adapter<beforeimageadapter.MyViewHolder> {

    private List<beforeimage> moviesList;
    Context ctxx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.image);

        }
    }


    public beforeimageadapter(List<beforeimage> moviesList, Context ctx) {
        this.moviesList = moviesList;
        this.ctxx=ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.acceptimagelay, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final beforeimage movie = moviesList.get(position);

        if (movie.getTitle().isEmpty())
        {
            holder.image.setImageResource(R.drawable.noimageavailable);
        }
        else
        {
            Picasso.get().load(movie.getTitle())
                    .placeholder(R.drawable.noimageavailable).into(holder.image);


            /*Picasso.with(ctxx)
                    .load(movie.getTitle())
                    .placeholder(R.drawable.noimageavailable)   // optional
                    .error(R.drawable.noimageavailable)      // optional
                    // optional
                    .into( holder.image);*/
        }
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(movie.getTitle().length()>10)
                {
                    Intent in=new Intent(ctxx,zoomMainActivity.class);
                    in.putExtra("imagename",""+movie.getTitle());
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctxx.startActivity(in);

                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
