package com.handypro.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.Location;
import com.handypro.Pojo.LocationSavedPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.adapters.AutoCompleteAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.gps.GPSTracker;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomEdittext;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.ServiceRequest;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by CAS63 on 2/20/2018.
 */

public class LocationSearchActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap googleMap;
    String apartmentno = "";
    private ProgressDialogcreated myDialog;
    private TextView searchedplace, TextViewOR;
    private AutoCompleteTextView mySearchLocET;
    SupportMapFragment mapFragment;
    private ConnectionDetector myConnectionManager;
    private List<String> myItemList_location;
    private ArrayList<String> myItemList_placeId;
    private Boolean isdataAvailable = false;
    private String mySelected_location = "", myLatitude = "", myLongitude = "";
    private RelativeLayout myBackLAY;
    private GeometricProgressView myProgressView;
    private Dialog myGetZipCodeDialog;
    String myStatus = "", myArea = "", myLocality = "", myState_name = "", myCity_name = "", myPostalCode = "";
    int j = 0;
    private TextView donesearch;
    private SharedPreference mySession;
    private Spinner SpinnerSavedAddress;
    private ArrayList<LocationSavedPojo> locationSavedPojos = new ArrayList<>();
    private String aSendPostalCode = "", map_key = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_search);
        mySession = new SharedPreference(this);
        myDialog = new ProgressDialogcreated(LocationSearchActivity.this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("AppInfo", MODE_PRIVATE);
        byte[] data = Base64.decode(pref.getString("code", ""), Base64.DEFAULT);
        try {
            map_key = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        classAndWidgetsInit();
        editTextOnTextChange();

    }


    private void PostLatLongRequest(String Url) {
        System.out.println("--------------LatLong url-------------------" + Url);
        ServiceRequest mRequest = new ServiceRequest(LocationSearchActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------LatLong  reponse-------------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        myStatus = object.getString("status");
                        JSONObject place_object = object.getJSONObject("result");
                        if (myStatus.equalsIgnoreCase("OK")) {
                            if (place_object.length() > 0) {
                                myArea = place_object.getString("name");
                                JSONArray addressArray = place_object.getJSONArray("address_components");
                                if (addressArray.length() > 0) {
                                    for (int i = 0; i < addressArray.length(); i++) {
                                        JSONObject address_object = addressArray.getJSONObject(i);

                                        JSONArray typesArray = address_object.getJSONArray("types");
                                        if (typesArray.length() > 0) {
                                            for (int j = 0; j < typesArray.length(); j++) {

                                                if (typesArray.get(j).toString().equalsIgnoreCase("locality")) {
                                                    myLocality = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("administrative_area_level_2")) {
                                                    myCity_name = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("administrative_area_level_1")) {
                                                    myState_name = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("postal_code")) {
                                                    myPostalCode = address_object.getString("long_name");
                                                }
                                            }
                                            isdataAvailable = true;
                                        } else {
                                            isdataAvailable = false;
                                        }
                                    }
                                } else {
                                    isdataAvailable = false;
                                }

                                JSONObject geometry_object = place_object.getJSONObject("geometry");
                                if (geometry_object.length() > 0) {
                                    JSONObject location_object = geometry_object.getJSONObject("location");
                                    if (location_object.length() > 0) {
                                        myLatitude = location_object.getString("lat");
                                        myLongitude = location_object.getString("lng");


                                        mapFragment.getMapAsync(new OnMapReadyCallback() {
                                            @Override
                                            public void onMapReady(GoogleMap googleMap) {
                                                LocationSearchActivity.this.googleMap = googleMap;
                                                googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

                                                MarkerOptions markerOptions = new MarkerOptions();
                                                LatLng latLngd = new LatLng(Double.parseDouble(myLatitude), Double.parseDouble(myLongitude));
                                                // Setting the position for the marker
                                                markerOptions.position(latLngd);
                                                // Clears the previously touched position
                                                googleMap.clear();
                                                // Animating to the touched position

                                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(myLatitude), Double.parseDouble(myLongitude)), 17.0f));
                                                // Placing a marker on the touched position
                                                googleMap.addMarker(markerOptions);
                                                mySearchLocET.setText("");
                                                InputMethodManager inputMethodManager =
                                                        (InputMethodManager) getApplicationContext().getSystemService(
                                                                Activity.INPUT_METHOD_SERVICE);
                                                inputMethodManager.hideSoftInputFromWindow(
                                                        getCurrentFocus().getWindowToken(), 0);
                                                searchedplace.setText(getAddress(Double.parseDouble(myLatitude), Double.parseDouble(myLongitude)));

                                            }
                                        });


                                        isdataAvailable = true;
                                    } else {
                                        isdataAvailable = false;
                                    }
                                } else {
                                    isdataAvailable = false;
                                }
                            } else {
                                isdataAvailable = false;
                            }
                        } else {
                            isdataAvailable = false;
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {

            }
        });
    }

    private void returnAndFinish(final String aPostalCode, final String sArea, final String aCity_name, final String aState_name, final String aLocality) {


        Button Bt_action, Bt_dismiss;
        final EditText zipcodeenter;




        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen

        final Dialog dialog = new Dialog(LocationSearchActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.zipcodeenter);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        zipcodeenter = dialog.findViewById(R.id.zipcodeenter);
        Bt_action = dialog.findViewById(R.id.custom_dialog_library_ok_button);
        Bt_dismiss = dialog.findViewById(R.id.custom_dialog_library_cancel_button);


        Bt_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                aSendPostalCode = aPostalCode;
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Selected_Latitude", myLatitude);
                returnIntent.putExtra("Selected_Longitude", myLongitude);
                returnIntent.putExtra("HouseNo", sArea);
                returnIntent.putExtra("apartmetno", zipcodeenter.getText().toString());
                returnIntent.putExtra("City", aCity_name + aState_name);
                returnIntent.putExtra("ZipCode", aSendPostalCode);
                returnIntent.putExtra("Location", aLocality);
                returnIntent.putExtra("Selected_Location", mySelected_location);
                setResult(RESULT_OK, returnIntent);
                onBackPressed();
                finish();
            }
        });

        Bt_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



        dialog.show();



    }


    private void editTextOnTextChange() {
        mySearchLocET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (myConnectionManager.isConnectingToInternet()) {
                    String data = s.toString().replace(" ", "%20");
                    PlaceSearchRequest(ServiceConstant.place_search_url + map_key + "&input=" + data);
                } else {
                    HNDHelper.showErrorAlert(LocationSearchActivity.this, getResources().getString(R.string.nointernet_text));
                }
                if (s.length() == 0) {
                    myProgressView.setVisibility(View.GONE);
                }
            }
        });

        mySearchLocET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    if (v.getText().toString().isEmpty()) {
                        CloseKeyboard(mySearchLocET);
                    }
                } else if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!myItemList_location.contains(mySearchLocET.getText().toString())) {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.selected_address_not_found));
                    }
                }
                return false;
            }
        });

        /*mySearchLocET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    if (!myItemList_location.contains(mySearchLocET.getText().toString())) {
                        TypedAddressNotMatchedWithGoogle = true;
                    }
                }
            }
        });*/

    }

    private void classAndWidgetsInit() {
        myConnectionManager = new ConnectionDetector(LocationSearchActivity.this);
        myItemList_location = new ArrayList<String>();
        myItemList_placeId = new ArrayList<String>();

        SpinnerSavedAddress = findViewById(R.id.SpinnerSavedAddress);
        TextViewOR = findViewById(R.id.TextViewOR);
        donesearch = findViewById(R.id.donesearch);
        searchedplace = findViewById(R.id.searchedplace);
        mySearchLocET = findViewById(R.id.activity_location_search_edittext);
        myBackLAY = findViewById(R.id.activity_location_search_LAY_back);
        myProgressView = findViewById(R.id.activity_location_search_dialog_progressView);

        if (googleMap == null) {
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }

        SpinnerSavedAddress.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int SelectedPosition, long id) {
                if (SelectedPosition > 0) {
                    int position = SelectedPosition - 1;
                    String selectedItem = parent.getItemAtPosition(position).toString();


                    MarkerOptions markerOptions = new MarkerOptions();

                    // Setting the position for the marker
                    markerOptions.position(new LatLng(locationSavedPojos.get(position).getLocation().getLat(), locationSavedPojos.get(position).getLocation().getLng()));
                    // Clears the previously touched position
                    googleMap.clear();
                    // Animating to the touched position

                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(locationSavedPojos.get(position).getLocation().getLat(), locationSavedPojos.get(position).getLocation().getLng()), 17.0f));
                    // Placing a marker on the touched position
                    googleMap.addMarker(markerOptions);

                    searchedplace.setText(locationSavedPojos.get(position).getFormattedAddress());
                    checkingpincode(ServiceConstant.gettingdetails + locationSavedPojos.get(position).getLocation().getLat() + "," + locationSavedPojos.get(position).getLocation().getLng() + "&key=" + map_key, false);
                    /*returnIntent.putExtra("Selected_Latitude", myLatitude);
        returnIntent.putExtra("Selected_Longitude", myLongitude);
        returnIntent.putExtra("HouseNo", sArea);
        returnIntent.putExtra("City", aCity_name + aState_name);
        returnIntent.putExtra("ZipCode", aPostalCode);
        returnIntent.putExtra("Location", aLocality);
        returnIntent.putExtra("Selected_Location", mySelected_location);*/
                }

            } // to close the onItemSelected

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        donesearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isdataAvailable) {
                    if (!myPostalCode.isEmpty()) {
                        returnAndFinish(myPostalCode, myArea, myCity_name, myState_name, myLocality);
                    } else {
                        if (myConnectionManager.isConnectingToInternet()) {
                            checkingpincode(ServiceConstant.gettingdetails + myLatitude + "," + myLongitude + "&key=" + map_key, true);
                        } else {
                            HNDHelper.showErrorAlert(LocationSearchActivity.this, getResources().getString(R.string.nointernet_text));
                        }
                    }
                } else {
                    if (myConnectionManager.isConnectingToInternet()) {
                        if (!myPostalCode.isEmpty()) {
                            returnAndFinish(myPostalCode, myArea, myCity_name, myState_name, myLocality);
                        } else {
                            checkingpincode(ServiceConstant.gettingdetails + myLatitude + "," + myLongitude + "&key=" + map_key, true);
                        }
                    } else {
                        HNDHelper.showErrorAlert(LocationSearchActivity.this, getResources().getString(R.string.nointernet_text));
                    }
                }
            }
        });


        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Medium.ttf");
        mySearchLocET.setTypeface(tf);



        /*
         * Show Soft keyboard when page opens
         */
        mySearchLocET.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(mySearchLocET, 0);
            }
        }, 200);

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void PlaceSearchRequest(String Url) {
        myProgressView.setVisibility(View.VISIBLE);

        ServiceRequest mRequest = new ServiceRequest(LocationSearchActivity.this);
        System.out.println("--------------Search city url-------------------" + Url);

        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Search city  response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");
                        JSONArray place_array = object.getJSONArray("predictions");
                        if (status.equalsIgnoreCase("ok")) {
                            if (place_array.length() > 0) {
                                myItemList_location.clear();
                                myItemList_placeId.clear();
                                for (int i = 0; i < place_array.length(); i++) {
                                    JSONObject place_object = place_array.getJSONObject(i);
                                    myItemList_location.add(place_object.getString("description"));
                                    myItemList_placeId.add(place_object.getString("place_id"));
                                }
                                isdataAvailable = true;
                            } else {
                                myItemList_location.clear();
                                myItemList_placeId.clear();
                                isdataAvailable = false;
                            }
                        } else {
                            myItemList_location.clear();
                            myItemList_placeId.clear();
                            isdataAvailable = false;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                myProgressView.setVisibility(View.GONE);

               /* myAdapter = new LocationSearchAdapter(LocationSearchActivity.this, myItemList_location);
                mySearchLocET.setAdapter(myAdapter);
                myAdapter.notifyDataSetChanged();*/

                AutoCompleteAdapter adapter = new AutoCompleteAdapter(LocationSearchActivity.this,
                        R.layout.layout_inflater_location_search, myItemList_location);

                mySearchLocET.setAdapter(adapter);

                mySearchLocET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        mySelected_location = myItemList_location.get(position);
                        /*if (myConnectionManager.isConnectingToInternet()) {
                            PostLatLongRequest(ServiceConstant.GetAddressFrom_LatLong_url + myItemList_placeId.get(position));
                        } else {
                            HNDHelper.showErrorAlert(LocationSearchActivity.this, getResources().getString(R.string.nointernet_text));
                        }*/
                        PostAddressToGetLatLng(mySelected_location);
                    }
                });

            }

            @Override
            public void onErrorListener() {
                myProgressView.setVisibility(View.GONE);
                CloseKeyboard(mySearchLocET);
            }
        });
    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {

        if (myConnectionManager.isConnectingToInternet()) {
            if (mySession.getLogInStatus()) {
                myDialog.show();
                SpinnerSavedAddress.setVisibility(View.VISIBLE);
                TextViewOR.setVisibility(View.VISIBLE);
                GetSavedAddress();
            } else {
                SpinnerSavedAddress.setVisibility(View.GONE);
                TextViewOR.setVisibility(View.GONE);
            }
        } else {
            HNDHelper.showErrorAlert(LocationSearchActivity.this, getResources().getString(R.string.nointernet_text));
        }

        this.googleMap = googleMap;
        if (j == 0) {
            j++;
            GPSTracker gps = new GPSTracker(getApplicationContext());
            if (gps.isgpsenabled() && gps.canGetLocation()) {
                Double myLatitude = gps.getLatitude();
                Double myLongitude = gps.getLongitude();
                LatLng latLngd = new LatLng(myLatitude, myLongitude);


                if (myLatitude == 0.0) {

                } else {
                    MarkerOptions markerOptions = new MarkerOptions();

                    // Setting the position for the marker
                    markerOptions.position(latLngd);
                    // Clears the previously touched position
                    googleMap.clear();
                    // Animating to the touched position

                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLatitude, myLongitude), 17.0f));
                    // Placing a marker on the touched position
                    googleMap.addMarker(markerOptions);


                    searchedplace.setText(getAddress(myLatitude, myLongitude));
                    if (myConnectionManager.isConnectingToInternet()) {
                        clickPostLatLongRequest(ServiceConstant.gettingdetails + myLatitude + "," + myLongitude + "&key=" + map_key);
                    } else {
                        HNDHelper.showErrorAlert(LocationSearchActivity.this, getResources().getString(R.string.nointernet_text));
                    }
                }


            }
        }


        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions markerOptions = new MarkerOptions();
                // Setting the position for the marker
                markerOptions.position(latLng);
                // Setting the title for the marker.
                // This will be displayed on taping the marker

                // Clears the previously touched position
                googleMap.clear();
                // Animating to the touched position
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                // Placing a marker on the touched position
                googleMap.addMarker(markerOptions);

                if (myConnectionManager.isConnectingToInternet()) {
                    clickPostLatLongRequest(ServiceConstant.gettingdetails + latLng.latitude + "," + latLng.longitude + "&key=" + map_key);
                } else {
                    HNDHelper.showErrorAlert(LocationSearchActivity.this, getResources().getString(R.string.nointernet_text));
                }

            }
        });
    }


    private String getAddress(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);


                StringBuilder strReturnedAddress = new StringBuilder();

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }


                strAdd = strReturnedAddress.toString();
            } else {
                Log.e("Crnt location address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Crnt location address", "Cannot get Address!");
        }


        return strAdd;
    }

    private void clickPostLatLongRequest(String Url) {
        System.out.println("--------------LatLong url-------------------" + Url);
        ServiceRequest mRequest = new ServiceRequest(LocationSearchActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------LatLong  reponse-------------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        myStatus = object.getString("status");
                        JSONArray place_object = object.getJSONArray("results");
                        if (myStatus.equalsIgnoreCase("OK")) {

                            if (place_object.length() > 0) {
                                for (int i = 0; i < 1; i++) {

                                    JSONObject address_object = place_object.getJSONObject(i);
                                    JSONArray addressArray = address_object.getJSONArray("address_components");
                                    String formatted_address = address_object.getString("formatted_address");
                                    JSONObject gemoetryobj = address_object.getJSONObject("geometry");
                                    JSONObject location = gemoetryobj.getJSONObject("location");
                                    String lat = location.getString("lat");
                                    String lng = location.getString("lng");

                                    mySelected_location = formatted_address;
                                    myLatitude = lat;
                                    myLongitude = lng;

                                    searchedplace.setText(formatted_address);

                                    System.out.println("--------------type-------------------" + lat + "--" + lng);

                                    for (int j = 0; j < addressArray.length(); j++) {
                                        JSONObject addressArrayvalue = addressArray.getJSONObject(j);
                                        JSONArray pincode = addressArrayvalue.getJSONArray("types");
                                        String postalcode = addressArrayvalue.getString("long_name");

                                        for (int k = 0; k < pincode.length(); k++) {
                                            String pincodevalue = "" + pincode.getString(k);
                                            if (pincodevalue.equals("postal_code")) {
                                                myPostalCode = postalcode;
                                                isdataAvailable = true;
                                                System.out.println("--------------postalcode-------------------" + postalcode);
                                            }

                                        }
                                    }

                                }
                                isdataAvailable = true;
                            } else {
                                isdataAvailable = false;
                            }


                        } else {
                            isdataAvailable = false;
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {

            }
        });
    }

    private void checkingpincode(String Url, final Boolean status) {
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        System.out.println("--------------LatLong url-------------------" + Url);
        ServiceRequest mRequest = new ServiceRequest(LocationSearchActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------LatLong  reponse-------------------" + response);


                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        myStatus = object.getString("status");
                        JSONArray place_object = object.getJSONArray("results");
                        if (myStatus.equalsIgnoreCase("OK")) {

                            if (place_object.length() > 0) {
                                for (int i = 0; i < 1; i++) {

                                    JSONObject address_object = place_object.getJSONObject(i);
                                    JSONArray addressArray = address_object.getJSONArray("address_components");
                                    String formatted_address = address_object.getString("formatted_address");
                                    JSONObject gemoetryobj = address_object.getJSONObject("geometry");
                                    JSONObject location = gemoetryobj.getJSONObject("location");
                                    String lat = location.getString("lat");
                                    String lng = location.getString("lng");

                                    mySelected_location = formatted_address;
                                    myLatitude = lat;
                                    myLongitude = lng;

                                    searchedplace.setText(formatted_address);

                                    System.out.println("--------------type-------------------" + lat + "--" + lng);

                                    for (int j = 0; j < addressArray.length(); j++) {
                                        JSONObject addressArrayvalue = addressArray.getJSONObject(j);
                                        JSONArray pincode = addressArrayvalue.getJSONArray("types");
                                        String postalcode = addressArrayvalue.getString("long_name");

                                        for (int k = 0; k < pincode.length(); k++) {
                                            String pincodevalue = "" + pincode.getString(k);
                                            if (pincodevalue.equals("postal_code")) {
                                                myPostalCode = postalcode;
                                                isdataAvailable = true;
                                                System.out.println("--------------postalcode-------------------" + postalcode);
                                            }

                                        }
                                    }

                                }
                            } else {
                                isdataAvailable = false;
                            }


                        } else {
                            isdataAvailable = false;
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status) {
                    if (myLatitude.equals("") || myLatitude.equals(" ") || myLatitude.equals("0.0") || myPostalCode.equals("")) {
                        myGetZipCodeDialog = new Dialog(LocationSearchActivity.this);
                        myGetZipCodeDialog.getWindow();
                        myGetZipCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        myGetZipCodeDialog.setContentView(R.layout.alert_dialog_get_zipcode);
                        myGetZipCodeDialog.setCanceledOnTouchOutside(false);
                        myGetZipCodeDialog.getWindow().getAttributes().windowAnimations = R.style.animations_photo_Picker;
                        myGetZipCodeDialog.show();
                        myGetZipCodeDialog.getWindow().setGravity(Gravity.CENTER);

                        final CustomEdittext myZipCodeET = myGetZipCodeDialog.findViewById(R.id.alert_dialog_ET_get_zipcode);
                        CustomTextView aCancelDialogTXT = myGetZipCodeDialog.findViewById(R.id.alert_dialog_get_zipcode_cancel_TXT);
                        CustomTextView aSubmitZipCodeTXT = myGetZipCodeDialog.findViewById(R.id.alert_dialog_get_zipcode_ok_TXT);

                        aCancelDialogTXT.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                myGetZipCodeDialog.dismiss();
                            }
                        });

                        aSubmitZipCodeTXT.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (myZipCodeET.getText().length() != 0) {
                                    returnAndFinish(myZipCodeET.getText().toString(), myArea, myCity_name, myState_name, myLocality);
                                } else {
                                    return;
                                }

                                myGetZipCodeDialog.dismiss();
                            }
                        });
                    } else {
                        returnAndFinish(myPostalCode, myArea, myCity_name, myState_name, myLocality);
                    }
                }

            }

            @Override
            public void onErrorListener() {

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }
        });
    }

    void PostAddressToGetLatLng(String Address) {
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        Log.e("LocationSearch", ServiceConstant.GetLatLonFromAddress + map_key + "&address=" + (Address.replace(" ", "%20")));
        ServiceRequest mRequest = new ServiceRequest(LocationSearchActivity.this);
        mRequest.makeServiceRequest(ServiceConstant.GetLatLonFromAddress + map_key + "&address=" + (Address.replace(" ", "%20")), Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------LatLong  reponse-------------------" + response);


                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        myStatus = object.getString("status");
                        JSONArray place_object = object.getJSONArray("results");
                        if (myStatus.equalsIgnoreCase("OK")) {

                            if (place_object.length() > 0) {
                                for (int i = 0; i < 1; i++) {

                                    JSONObject address_object = place_object.getJSONObject(i);
                                    JSONArray addressArray = address_object.getJSONArray("address_components");
                                    final String formatted_address = address_object.getString("formatted_address");
                                    JSONObject gemoetryobj = address_object.getJSONObject("geometry");
                                    JSONObject location = gemoetryobj.getJSONObject("location");
                                    String lat = location.getString("lat");
                                    String lng = location.getString("lng");

                                    mySelected_location = formatted_address;
                                    myLatitude = lat;
                                    myLongitude = lng;
                                    searchedplace.setText(formatted_address);

                                    for (int j = 0; j < addressArray.length(); j++) {
                                        JSONObject addressArrayvalue = addressArray.getJSONObject(j);
                                        JSONArray pincode = addressArrayvalue.getJSONArray("types");
                                        String postalcode = addressArrayvalue.getString("long_name");

                                        for (int k = 0; k < pincode.length(); k++) {
                                            String pincodevalue = "" + pincode.getString(k);
                                            if (pincodevalue.equals("postal_code")) {
                                                myPostalCode = postalcode;
                                                isdataAvailable = true;
                                                System.out.println("--------------postalcode-------------------" + postalcode);
                                            }

                                        }
                                    }

                                    mapFragment.getMapAsync(new OnMapReadyCallback() {
                                        @Override
                                        public void onMapReady(GoogleMap googleMap) {
                                            LocationSearchActivity.this.googleMap = googleMap;
                                            googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

                                            MarkerOptions markerOptions = new MarkerOptions();
                                            LatLng latLngd = new LatLng(Double.parseDouble(myLatitude), Double.parseDouble(myLongitude));
                                            // Setting the position for the marker
                                            markerOptions.position(latLngd);
                                            // Clears the previously touched position
                                            googleMap.clear();
                                            // Animating to the touched position

                                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(myLatitude), Double.parseDouble(myLongitude)), 17.0f));
                                            // Placing a marker on the touched position
                                            googleMap.addMarker(markerOptions);
                                            mySearchLocET.setText("");
                                            InputMethodManager inputMethodManager =
                                                    (InputMethodManager) getApplicationContext().getSystemService(
                                                            Activity.INPUT_METHOD_SERVICE);
                                            inputMethodManager.hideSoftInputFromWindow(
                                                    getCurrentFocus().getWindowToken(), 0);
                                            searchedplace.setText(formatted_address);

                                        }
                                    });
                                }
                            } else {
                                isdataAvailable = false;
                            }
                        } else {
                            isdataAvailable = false;
                        }
                    } else {
                        isdataAvailable = false;
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }
        });
    }

    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(LocationSearchActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    void GetSavedAddress() {
        HashMap<String, String> jsonParam = new HashMap<>();
        mySession = new SharedPreference(this);
        jsonParam.put("user_id", mySession.getUserDetails().getUserId());

        ServiceRequest mRequest = new ServiceRequest(LocationSearchActivity.this);
        mRequest.makeServiceRequest(ServiceConstant.GET_USER_SAVED_ADDRESS, Request.Method.POST, jsonParam, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("---------Saved Address------------" + response);
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                try {
                    JSONObject object = new JSONObject(response);
                    List<String> SavedAddress = new ArrayList<>();
                    locationSavedPojos = new ArrayList<>();
                    if (object.getString("status").equals("1") && object.has("response")) {
                        SavedAddress.add("Saved Address");
                        JSONArray ResJSONArray = object.getJSONArray("response");

                        if (ResJSONArray.length() > 0) {
                            for (int i = 0; i < ResJSONArray.length(); i++) {
                                JSONObject addressJSONArray = ResJSONArray.getJSONObject(i).getJSONObject("address");
                                if (addressJSONArray.length() > 0) {
                                    LocationSavedPojo locationSavedPojo = new LocationSavedPojo();
                                    locationSavedPojo.setId("");
                                    locationSavedPojo.setLocation(new Location(Double.valueOf(addressJSONArray.getJSONObject("location").getString("lat")), Double.valueOf(addressJSONArray.getJSONObject("location").getString("lng"))));
                                    locationSavedPojo.setFormattedAddress(addressJSONArray.getString("formatted_address"));
                                    SavedAddress.add(addressJSONArray.getString("formatted_address"));
                                    locationSavedPojos.add(locationSavedPojo);
                                }

                                JSONArray addressListJSONArray = ResJSONArray.getJSONObject(i).getJSONArray("addressList");
                                if (addressListJSONArray.length() > 0) {
                                    for (int k = 0; k < addressListJSONArray.length(); k++) {
                                        LocationSavedPojo locationSavedPojo = new LocationSavedPojo();
                                        locationSavedPojo.setId(addressListJSONArray.getJSONObject(k).getString("_id"));
                                        locationSavedPojo.setLocation(new Location(Double.valueOf(addressListJSONArray.getJSONObject(k).getJSONObject("location").getString("lat")), Double.valueOf(addressListJSONArray.getJSONObject(k).getJSONObject("location").getString("lng"))));
                                        locationSavedPojo.setFormattedAddress(addressListJSONArray.getJSONObject(k).getString("formatted_address"));
                                        SavedAddress.add(addressListJSONArray.getJSONObject(k).getString("formatted_address"));
                                        locationSavedPojos.add(locationSavedPojo);
                                    }
                                }
                            }
                        }

                        /*// Create a new LinkedHashSet
                        // Add the elements to set
                        Set<String> set = new LinkedHashSet<>(SavedAddress);

                        // Clear the list
                        SavedAddress.clear();

                        // with no duplicates to the list
                        SavedAddress.addAll(set);*/

                        ArrayAdapter address_saved_adapter = new ArrayAdapter<>(
                                LocationSearchActivity.this,
                                R.layout.spinner_deselected_item, SavedAddress
                        );
                        address_saved_adapter.setDropDownViewResource(R.layout.spinner_selected_item);
                        SpinnerSavedAddress.setAdapter(address_saved_adapter);
                    } else {
                        SpinnerSavedAddress.setVisibility(View.GONE);
                        TextViewOR.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    SpinnerSavedAddress.setVisibility(View.GONE);
                    TextViewOR.setVisibility(View.GONE);
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                SpinnerSavedAddress.setVisibility(View.GONE);
                TextViewOR.setVisibility(View.GONE);
            }
        });
    }
}
