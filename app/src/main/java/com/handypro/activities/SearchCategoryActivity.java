package com.handypro.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.handypro.Pojo.WorldPopulation;
import com.handypro.R;
import com.handypro.sharedpreference.SharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class SearchCategoryActivity extends AppCompatActivity {

    // Declare Variables
    ListView list;
    ListViewAdapter adapter;
    EditText editsearch;
    private SharedPreference mySession;
    ArrayList<WorldPopulation> arraylist = new ArrayList<WorldPopulation>();
    RelativeLayout activity_shedule_appoinment_LAY_parent;
    ArrayList<WorldPopulation> fullarraylist = new ArrayList<WorldPopulation>();
    private ArrayList<String> mySelectedSubCatIdArr;
    //  ArrayList<SubCategoryPojo> mySelectedSubCatArrList;
    ArrayList<Object> subcatname, subcatid, checkedimage, uncheckedimage, parentcat;
    ArrayList<Object> selectedid;

    ArrayList<String> selectedcategory, selectedsubid;
    Button myHomePageBTN;
    String myZipCodeSTR, myLatSTR, myLongSTR, address;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //mySelectedSubCatArrList=new  ArrayList<SubCategoryPojo>();
        mySession = new SharedPreference(SearchCategoryActivity.this);
        myHomePageBTN = findViewById(R.id.fragment_home_page_bottom_next_BTN);
        activity_shedule_appoinment_LAY_parent = findViewById(R.id.activity_shedule_appoinment_LAY_parent);
        Intent intent = getIntent();
        activity_shedule_appoinment_LAY_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String aSelectedIdSTR = android.text.TextUtils.join(",", selectedsubid);
                mySession.setCategoryId(aSelectedIdSTR);
                mySession.setNewsearch(true);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        myZipCodeSTR = getIntent().getExtras().getString("myZipCodeSTR");
        myLatSTR = getIntent().getExtras().getString("myLatSTR");
        myLongSTR = getIntent().getExtras().getString("myLongSTR");
        address = getIntent().getExtras().getString("address");

        Bundle args = intent.getBundleExtra("BUNDLE");

        parentcat = (ArrayList<Object>) args.getSerializable("parentcat");
        subcatname = (ArrayList<Object>) args.getSerializable("subcatname");
        subcatid = (ArrayList<Object>) args.getSerializable("subcatid");
        checkedimage = (ArrayList<Object>) args.getSerializable("checkedimage");
        uncheckedimage = (ArrayList<Object>) args.getSerializable("uncheckedimage");
        selectedid = (ArrayList<Object>) args.getSerializable("selectedSubCategories");
        mySelectedSubCatIdArr = (ArrayList<String>) intent.getSerializableExtra("selectedSubCategories");

        selectedcategory = new ArrayList<String>();
        selectedsubid = new ArrayList<String>();
        list = findViewById(R.id.listview);

        arraylist.clear();
        fullarraylist.clear();
        for (int i = 0; i < subcatname.size(); i++) {
            WorldPopulation aSubCatPojo = new WorldPopulation();
            aSubCatPojo.setCountry("" + parentcat.get(i));
            aSubCatPojo.setSubcatid("" + selectedid.get(i));
            aSubCatPojo.setRank("" + subcatname.get(i));
            aSubCatPojo.setPopulation("" + uncheckedimage.get(i));
            aSubCatPojo.setstatus("0");
            if (mySelectedSubCatIdArr.contains(selectedid.get(i))) {
                aSubCatPojo.setstatus("1");
                selectedsubid.add("" + selectedid.get(i));
                selectedcategory.add("" + subcatname.get(i));
            } else {
                aSubCatPojo.setstatus("0");
            }
            aSubCatPojo.setparentcat("" + parentcat.get(i));
            arraylist.add(aSubCatPojo);
            fullarraylist.add(aSubCatPojo);
        }

        // Pass results to ListViewAdapter Class
        adapter = new ListViewAdapter(this, arraylist, fullarraylist);


        // Binds the Adapter to the ListView
        list.setAdapter(adapter);

        if (selectedsubid.size() == 0) {
            myHomePageBTN.setVisibility(View.GONE);
        } else {
            myHomePageBTN.setVisibility(View.VISIBLE);
        }

        // Locate the EditText in listview_main.xml
        editsearch = findViewById(R.id.search);

        // Capture Text in EditText
        editsearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editsearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });


        myHomePageBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String aSelectedIdSTR = android.text.TextUtils.join(",", selectedsubid);
                mySession.setCategoryId(aSelectedIdSTR);
                mySession.setNewsearch(true);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            String aSelectedIdSTR = android.text.TextUtils.join(",", selectedsubid);
            mySession.setCategoryId(aSelectedIdSTR);
            mySession.setNewsearch(true);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public class ListViewAdapter extends BaseAdapter {

        // Declare Variables
        Context mContext;
        LayoutInflater inflater;
        private ArrayList<WorldPopulation> worldpopulationlist;
        private ArrayList<WorldPopulation> fullarray;


        ListViewAdapter(Context context, ArrayList<WorldPopulation> worldpopulationlist, ArrayList<WorldPopulation> fullarray) {
            mContext = context;
            this.worldpopulationlist = worldpopulationlist;
            this.fullarray = fullarray;
            inflater = LayoutInflater.from(mContext);
        }

        public class ViewHolder {
            TextView rank;
            TextView country;
            TextView population;
            ImageView showimage;
        }

        @Override
        public int getCount() {
            return worldpopulationlist.size();
        }

        @Override
        public WorldPopulation getItem(int position) {
            return worldpopulationlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View view, ViewGroup parent) {
            final ListViewAdapter.ViewHolder holder;
            if (view == null) {
                holder = new ListViewAdapter.ViewHolder();
                view = inflater.inflate(R.layout.listview_item, null);
                // Locate the TextViews in listview_item.xml
                holder.showimage = view.findViewById(R.id.layout_inflate_sub_category_IMG);
                holder.rank = view.findViewById(R.id.ranklabel);
                holder.country = view.findViewById(R.id.country);
                holder.population = view.findViewById(R.id.populationlabel);
                view.setTag(holder);
            } else {
                holder = (ListViewAdapter.ViewHolder) view.getTag();
            }


            //  bookingtimecolor
            if (worldpopulationlist.get(position).getstatus().equals("1")) {
                Picasso.get()
                        .load(/*ServiceConstant.Base_Url +*/ worldpopulationlist.get(position).getPopulation())
                        .resize(120, 120)
                        .into(holder.showimage);

                holder.population.setText(getResources().getString(R.string.removee));
                holder.population.setBackgroundResource(R.drawable.selectedgreen);
            } else {
                Picasso.get().load(/*ServiceConstant.Base_Url +*/ worldpopulationlist.get(position).getPopulation())
                        .resize(120, 120)
                        .into(holder.showimage);

                holder.population.setText(getResources().getString(R.string.addee));
                holder.population.setBackgroundResource(R.drawable.straightdoublecolour);
            }
            // Set the results into TextViews
            holder.rank.setText(worldpopulationlist.get(position).getRank());
            holder.country.setText(worldpopulationlist.get(position).getparentcat());


            // Listen for ListView Item Click
            view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {

                    for (int t = 0; t < worldpopulationlist.size(); t++) {
                        worldpopulationlist.get(position).setstatus("0");
                    }

                    selectedsubid.add(worldpopulationlist.get(position).getSubcatid());
                    selectedcategory.add(worldpopulationlist.get(position).getRank());
                    worldpopulationlist.get(position).setstatus("1");
                    notifyDataSetChanged();
                    if (selectedsubid.size() == 0) {
                        myHomePageBTN.setVisibility(View.GONE);
                    } else {
                        myHomePageBTN.setVisibility(View.VISIBLE);
                    }
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("SUBCAT_ID", worldpopulationlist.get(position).getSubcatid());
                    setResult(RESULT_OK, returnIntent);
                    String aSelectedIdSTR = android.text.TextUtils.join(",", selectedsubid);
                    mySession.setCategoryId(aSelectedIdSTR);
                    mySession.setNewsearch(true);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


//                    if (worldpopulationlist.get(position).getstatus().equals("1")) {
//                        worldpopulationlist.get(position).setstatus("0");
//                        notifyDataSetChanged();
//                        if (selectedcategory.contains(worldpopulationlist.get(position).getRank())) {
//                            int index = selectedcategory.indexOf(worldpopulationlist.get(position).getRank());
//                            selectedcategory.remove(index);
//                            selectedsubid.remove(index);
//                        }
//                        if (selectedsubid.size() == 0) {
//                            myHomePageBTN.setVisibility(View.GONE);
//                        } else {
//                            myHomePageBTN.setVisibility(View.VISIBLE);
//                        }
//                    } else {
//                        selectedsubid.add(worldpopulationlist.get(position).getSubcatid());
//                        selectedcategory.add(worldpopulationlist.get(position).getRank());
//                        worldpopulationlist.get(position).setstatus("1");
//                        notifyDataSetChanged();
//
//                        if (selectedsubid.size() == 0) {
//                            myHomePageBTN.setVisibility(View.GONE);
//                        } else {
//                            myHomePageBTN.setVisibility(View.VISIBLE);
//                        }
//                    }
                }
            });
            return view;
        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            worldpopulationlist.clear();
            System.out.println(charText.length() + "--------->" + fullarray);
            if (charText.length() == 0) {
                for (int i = 0; i < fullarray.size(); i++) {
                    WorldPopulation aSubCatPojo = new WorldPopulation();
                    aSubCatPojo.setCountry(fullarray.get(i).getCountry());
                    aSubCatPojo.setRank(fullarray.get(i).getRank());
                    aSubCatPojo.setPopulation(fullarray.get(i).getPopulation());
                    aSubCatPojo.setSubcatid(fullarray.get(i).getSubcatid());
                    if (selectedcategory.contains(fullarray.get(i).getRank())) {
                        aSubCatPojo.setstatus("1");
                    } else {
                        aSubCatPojo.setstatus("0");
                    }

                    aSubCatPojo.setparentcat(fullarray.get(i).getparentcat());
                    worldpopulationlist.add(aSubCatPojo);
                }
            } else {
                for (int i = 0; i < fullarray.size(); i++) {
                    if (fullarray.get(i).getRank().toLowerCase(Locale.getDefault()).contains(charText)) {
                        WorldPopulation aSubCatPojo = new WorldPopulation();
                        aSubCatPojo.setCountry(fullarray.get(i).getCountry());
                        aSubCatPojo.setRank(fullarray.get(i).getRank());
                        aSubCatPojo.setPopulation(fullarray.get(i).getPopulation());
                        aSubCatPojo.setSubcatid(fullarray.get(i).getSubcatid());
                        if (selectedcategory.contains(fullarray.get(i).getRank())) {
                            aSubCatPojo.setstatus("1");
                        } else {
                            aSubCatPojo.setstatus("0");
                        }
                        aSubCatPojo.setparentcat(fullarray.get(i).getparentcat());
                        worldpopulationlist.add(aSubCatPojo);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }
}