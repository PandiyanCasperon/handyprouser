package com.handypro.activities;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.CancelReasonPojo;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.navigationMenu.MyOrdersActivity;
import com.handypro.adapters.CancelReasonAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by user88 on 12/17/2015.
 */
public class Cancel_Job_Reason extends AppCompatActivity {

    private ConnectionDetector cd;
    private Context context;
    private SharedPreference mySession;
    private ListView cancel_listview;
    private ProgressDialogcreated myDialog;
    private TextView Tv_Emtytxt;
    private Boolean isInternetPresent = false;
    private boolean show_progress_status = false;

    private String Str_JobId = "";
    private RelativeLayout Rl_layout_cancel_back;

    private ArrayAdapter<String> listAdapter;
    private ArrayList<CancelReasonPojo> Cancelreason_arraylist;
    private CancelReasonAdapter adapter;

    private String provider_id = "";
    private String Job_id = "";

    private Handler mHandler;
    private boolean isReasonAvailable = false;
    private String Str_reason = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancelreason_job);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initialize();

        cancel_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Str_reason = Cancelreason_arraylist.get(position).getReason();
                System.out.println("reasonm-----------" + Cancelreason_arraylist.get(position).getReason());
                cancelJobAlert();
            }
        });


        Rl_layout_cancel_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }

    private void initialize() {

        cd = new ConnectionDetector(Cancel_Job_Reason.this);
        mHandler = new Handler();

        mySession = new SharedPreference(Cancel_Job_Reason.this);
        // get user data from session
        SharedPreferences getcard;
        getcard = getApplicationContext().getSharedPreferences("cancejobid", 0); // 0 - for private mode
        Job_id=getcard.getString("jobid","");


        Cancelreason_arraylist = new ArrayList<CancelReasonPojo>();
        cancel_listview = (ListView) findViewById(R.id.cancelreason_listView);
        Tv_Emtytxt = (TextView) findViewById(R.id.emtpy_cancelreason);
        Rl_layout_cancel_back = (RelativeLayout) findViewById(R.id.layout_cancel_back);

        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            cancelreason_PostRequest(Cancel_Job_Reason.this, ServiceConstant.MyJobs_Cancel_Reason_Url);
        } else {

            final PkDialog mDialog = new PkDialog(Cancel_Job_Reason.this);
            mDialog.setDialogTitle(getResources().getString(R.string.class_booking_confirmation_internet));
            mDialog.setDialogMessage(getResources().getString(R.string.nointernet_text));
            mDialog.setPositiveButton(getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();
        }

    }

    //--------------Alert Method-----------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(Cancel_Job_Reason.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    //--------------------code for cancel reason diaolg--------------------
    public void cancelJobAlert() {
        ConnectionDetector cd = new ConnectionDetector(Cancel_Job_Reason.this);
        final boolean isInternetPresent = cd.isConnectingToInternet();
        final PkDialog mDialog = new PkDialog(Cancel_Job_Reason.this);
        mDialog.setDialogTitle(getResources().getString(R.string.confirmdelete));
        mDialog.setDialogMessage(getResources().getString(R.string.surewanttodelete));

        mDialog.setPositiveButton(getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetPresent) {
                   postRequest_Canceljob(Cancel_Job_Reason.this, ServiceConstant.MyJobs_Cancel_Url);



                } else {
                    final PkDialog mDialog = new PkDialog(Cancel_Job_Reason.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.class_booking_confirmation_internet));
                    mDialog.setDialogMessage(getResources().getString(R.string.nointernet_text));
                    mDialog.setPositiveButton(getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    });
                    mDialog.show();
                }
                mDialog.dismiss();
            }
        });

        mDialog.setNegativeButton(getResources().getString(R.string.dialog_cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.show();


    }

    //---------------------code for cancel job-----------------
    private void cancelreason_PostRequest(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());

        myDialog = new ProgressDialogcreated(Cancel_Job_Reason.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        ServiceRequest mservicerequest = new ServiceRequest(mContext);

        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Str_response = "", sResponse = "";
                System.out.println("cancelreason--------" + response);

                try {

                    // JSONObject jobject = new JSONObject(response);
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");

                        JSONArray response_array = jsonObject.getJSONArray("reason");
                        if (response_array.length() > 0) {
                            Cancelreason_arraylist.clear();

                            for (int i = 0; i < response_array.length(); i++) {
                                JSONObject reason_object = response_array.getJSONObject(i);
                                CancelReasonPojo items = new CancelReasonPojo();
                                items.setReason(reason_object.getString("reason"));
                                items.setCancelreason_id(reason_object.getString("id"));

                                Cancelreason_arraylist.add(items);

                            }

                            isReasonAvailable = true;
                        } else {
                            isReasonAvailable = false;
                        }
                    } else {
                        sResponse = object.getString("response");
                        // alert(getResources().getString(R.string.action_sorry), sResponse);
                    }


                    if (Sstatus.equalsIgnoreCase("1")) {
                        System.out.println("secnd-----------" + Cancelreason_arraylist.get(0).getReason());
                        adapter = new CancelReasonAdapter(Cancel_Job_Reason.this, Cancelreason_arraylist);
                        cancel_listview.setAdapter(adapter);

                        if (show_progress_status) {
                            Tv_Emtytxt.setVisibility(View.GONE);
                        } else {
                            Tv_Emtytxt.setVisibility(View.VISIBLE);
                            cancel_listview.setEmptyView(Tv_Emtytxt);
                        }
                    } else {
                        HNDHelper.showResponseErrorAlert(getApplicationContext(), object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }
        });

    }


    //---------------------------Code for cancelled-------------------
    private void postRequest_Canceljob(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("job_id", Job_id);
        jsonParams.put("reason", Str_reason);


        System.out.println("provider_id-----------" + provider_id);
        System.out.println("job_id-----------" + Job_id);
        System.out.println("reason-----------" + Str_reason);

        myDialog = new ProgressDialogcreated(Cancel_Job_Reason.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        ServiceRequest mservicerequest = new ServiceRequest(mContext);

        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                System.out.println("cancelled---------------" + response);

                String Str_status = "", Str_message = "", Str_response = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject jobject = object.getJSONObject("response");
                        Str_message = jobject.getString("message");

                    } else {
                        Str_response = object.getString("response");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (Str_status.equalsIgnoreCase("1")) {

                    Intent intent = new Intent(getApplicationContext(), MyOrdersActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                } else {
                    Alert(getResources().getString(R.string.alert_label_title), Str_response);
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }



    protected void onResume() {
        super.onResume();

    }


    @Override
    protected void onPause() {
        super.onPause();

    }





}