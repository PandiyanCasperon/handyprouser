package com.handypro.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.BoldCustomTextView;
import com.handypro.volley.ServiceRequest;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by CAS63 on 2/19/2018.
 */

public class FirstTimePrivacy extends AppCompatActivity {
    private RelativeLayout myBackLAY;
    private WebView myWebView;
    private String myWebUrlStr = "", myHeaderTitleStr = "", type = "", classs = "";
    private ProgressDialogcreated myDialog;
    private BoldCustomTextView myTitleTXT;
    TextView clicktoaccept;
    private Dialog myGetZipCodeDialog;
    private SharedPreference mySession;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.termsprivacyy);

        mySession = new SharedPreference(FirstTimePrivacy.this);


        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInit();
        getIntentValues();
        setSettings();


        if (type.equals("about")) {
            myWebView.getSettings().setBuiltInZoomControls(true);
            myWebView.getSettings().setDomStorageEnabled(true);

            // Zoom out the best fit your screen
            myWebView.getSettings().setLoadWithOverviewMode(true);
            myWebView.getSettings().setUseWideViewPort(true);

            myTitleTXT.setText(myHeaderTitleStr);
            startWebView(myWebUrlStr);
        } else {
            postRegisterRequest(FirstTimePrivacy.this, ServiceConstant.Terms_Conditions_Url);
        }


    }

    private void postRegisterRequest(final Context mContext, String url) {
        myDialog = new ProgressDialogcreated(FirstTimePrivacy.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();

        if (type.equals("reg")) {
            jsonParams.put("zipcode", myWebUrlStr);
            jsonParams.put("app", "user");
        } else {
            jsonParams.put("franchisee", myWebUrlStr);
            jsonParams.put("app", "user");
        }


        ServiceRequest mRequest = new ServiceRequest(mContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                System.out.println("---------register response------------" + response);
                myWebView.loadData(response, "text/html", "UTF-8");
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }


    private void getIntentValues() {
        Intent data = getIntent();
        myWebUrlStr = data.getStringExtra("url");

        myHeaderTitleStr = data.getStringExtra("header");


        type = data.getStringExtra("type");
        classs = data.getStringExtra("classs");


    }

    private void startWebView(String aURL) {
        ShowProgessBar();

        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            @Override
            public void onLoadResource(WebView view, String url) {
            }

            @Override
            public void onPageFinished(WebView view, String url) {

                if (myDialog != null && myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                System.out.println("------------url-$$$$$------" + url);

            }
        });

        //Load url in webView
        myWebView.loadUrl(aURL);
    }


    private void ShowProgessBar() {
        myDialog = new ProgressDialogcreated(FirstTimePrivacy.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
    }

    private void classAndWidgetInit() {
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_webview_LAY_back);
        myWebView = (WebView) findViewById(R.id.activity_webview_View);
        myTitleTXT = (BoldCustomTextView) findViewById(R.id.activity_webview_TXT_title);
        clicktoaccept = (TextView) findViewById(R.id.clicktoaccept);
        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        clicktoaccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postAccept(FirstTimePrivacy.this, ServiceConstant.acceptclickurl);
            }
        });

    }

    private void setSettings() {
        myWebView.getSettings().setJavaScriptEnabled(true);

        // Allow Zoom in/out controls
        myWebView.getSettings().setBuiltInZoomControls(true);
        myWebView.getSettings().setDomStorageEnabled(true);

        // Zoom out the best fit your screen
        myWebView.getSettings().setLoadWithOverviewMode(true);
        myWebView.getSettings().setUseWideViewPort(true);

        myTitleTXT.setText(myHeaderTitleStr);
        //  startWebView(myWebUrlStr);

    }


    private void postAccept(final Context mContext, String url) {
        myGetZipCodeDialog = new Dialog(FirstTimePrivacy.this);
        myGetZipCodeDialog.getWindow();
        myGetZipCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myGetZipCodeDialog.setContentView(R.layout.alert_dialog_get_zipcode);
        myGetZipCodeDialog.setCanceledOnTouchOutside(true);
        myGetZipCodeDialog.getWindow().getAttributes().windowAnimations = R.style.animations_photo_Picker;
        myGetZipCodeDialog.show();
        myGetZipCodeDialog.getWindow().setGravity(Gravity.CENTER);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());


        ServiceRequest mRequest = new ServiceRequest(mContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                myGetZipCodeDialog.dismiss();


                try {
                    JSONObject jobject = new JSONObject(response);
                    String Str_status = jobject.getString("status");

                    if (Str_status.equals("1")) {

                        if (classs.equals("home")) {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("termsconditionn", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("termsandconditions", "1");
                            editor.apply();
                            editor.commit();

                            Intent intent = new Intent(FirstTimePrivacy.this, HomeActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("termsconditionn", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("termsandconditions", "1");
                            editor.apply();
                            editor.commit();
                            finish();
                        }

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    myGetZipCodeDialog.dismiss();
                }


            }

            @Override
            public void onErrorListener() {
                myGetZipCodeDialog.dismiss();
            }
        });
    }
}
