package com.handypro.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.volley.Request;
import com.handypro.Dialog.LoadingDialog;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.EstimatedDetails;
import com.handypro.Pojo.MultiSubItemPojo;
import com.handypro.Pojo.approveeditbuilderpojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.navigationMenu.estimateacceptdb;
import com.handypro.activities.navigationMenu.reschedulepage;
import com.handypro.adapters.RecyclerViewDynamicAdapter;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.CurrencySymbolConverter;
import com.handypro.volley.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


/**
 * Created by user127 on 16-02-2018.
 */

public class approveestimatebuilder extends AppCompatActivity {


    String storetaskerid, storeestimatehours;
    TextView hintshow;
    LinearLayout imageView;
    LinearLayout displaylist, nodata, downoptions;
    SharedPreferences pref;
    String dataisthere;
    ListView listView;
    TextView done, TextViewTotal, TextViewSubTotal, TextViewHandyProSavings,
            TextViewHandyProSavingsTitle, reject, TextViewCouponNameLabel, TextViewCouponName, TextViewCouponAmountLabel, TextViewCouponAmount;
    ArrayList<approveeditbuilderpojo> arrayList = new ArrayList<approveeditbuilderpojo>();
    String currency;
    int eday, emonth, eyear;
    String grandtotal = "0", HandyProSavings = "0", estimation_itemize = "0";
    int status = 0;
    String selecteddate, selectedtime;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private SharedPreference mySession;
    private editestimationadapter adapter;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private ProgressDialogcreated myDialog;
    private estimateacceptdb mHelper;
    private SQLiteDatabase dataBase;
    private boolean SendFlowTOFranchisee = false;
    private List<MultiSubItemPojo> MainMultSubItemsList;
    RecyclerViewDynamicAdapter recyclerViewDynamicAdapter;
    private LoadingDialog dialog;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.approveestimatepage);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mHelper = new estimateacceptdb(this);
        this.deleteDatabase(mHelper.DATABASE_NAME);


        mySession = new SharedPreference(approveestimatebuilder.this);
        cd = new ConnectionDetector(approveestimatebuilder.this);

        pref = getApplicationContext().getSharedPreferences("logindetails", 0);
        currency = mySession.getCurrencySymbol();
        imageView = (LinearLayout) findViewById(R.id.backclick);
        listView = (ListView) findViewById(R.id.listview);
        displaylist = (LinearLayout) findViewById(R.id.displaylist);
        downoptions = (LinearLayout) findViewById(R.id.downoptions);
        nodata = (LinearLayout) findViewById(R.id.nodata);
        hintshow = (TextView) findViewById(R.id.hintshow);
        reject = (TextView) findViewById(R.id.reject);
        done = (TextView) findViewById(R.id.done);
        TextViewCouponNameLabel = (TextView) findViewById(R.id.TextViewCouponNameLabel);
        TextViewCouponName = (TextView) findViewById(R.id.TextViewCouponName);
        TextViewCouponAmountLabel = (TextView) findViewById(R.id.TextViewCouponAmountLabel);
        TextViewCouponAmount = (TextView) findViewById(R.id.TextViewCouponAmount);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {

                    if (SendFlowTOFranchisee) {
                        ArrayList<String> ServiceIdList = new ArrayList<>();
                        for (int jk = 0; jk < arrayList.size(); jk++) {
                            if (arrayList.get(jk).getCheckedvalue() != 0) {
                                ServiceIdList.add(arrayList.get(jk).getServiceid());
                            }
                        }
                        if (ServiceIdList.size() > 0) {
                            MakeRequestToFranchisee(ServiceIdList);
                        }
                    } else {
                        int nozero = 0;
                        for (int jk = 0; jk < arrayList.size(); jk++) {
                            String amount = "" + arrayList.get(jk).getCheckedvalue();
                            if (amount.equalsIgnoreCase("1")) {
                                nozero = 1;
                            } else {

                            }
                        }

                        if (nozero == 1) {

                            float estimatehours = 0;
                            for (int jk = 0; jk < arrayList.size(); jk++) {

                                dataBase = mHelper.getWritableDatabase();
                                ContentValues values = new ContentValues();
                                values.put(estimateacceptdb.KEY_SERVICEID, arrayList.get(jk).getServiceid());
                                if (arrayList.get(jk).getCheckedvalue() == 0) {
                                    values.put(estimateacceptdb.KEY_STATUS, "2");
                                } else {
                                    if (MainMultSubItemsList.size() > 0 && MainMultSubItemsList != null) {
                                        for (int i = 0; i < MainMultSubItemsList.size(); i++) {
                                            if (MainMultSubItemsList.get(i).getPositionForOperation() == jk &&
                                                    !MainMultSubItemsList.get(i).getStatus().equals("User Rejected")) {
                                                estimatehours = estimatehours + Float.parseFloat(MainMultSubItemsList.get(i).getHours());
                                            }
                                        }
                                    } else {
                                        estimatehours = (estimatehours + arrayList.get(jk).getVal2());
                                    }


                                    values.put(estimateacceptdb.KEY_STATUS, "" + arrayList.get(jk).getCheckedvalue());

                                }
                                dataBase.insert(estimateacceptdb.TABLE_NAME, null, values);
                            }

                            SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                            String Job_id = Job_idpref.getString("jobid", "");

                            SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("jobid", Job_id);
                            editor.putString("estimatehours", "" + estimatehours);
                            editor.putString("taskerid", storetaskerid);
                            editor.apply();
                            editor.commit();

                            Intent InfoIntent = new Intent(approveestimatebuilder.this, reschedulepage.class);
                            InfoIntent.putExtra("eday", eday);
                            InfoIntent.putExtra("emonth", emonth);
                            InfoIntent.putExtra("eyear", eyear);
                            startActivity(InfoIntent);

                        } else {
                            HNDHelper.showResponseTitle("Sorry", approveestimatebuilder.this, getResources().getString(R.string.class_approve_estimate_choose));
                        }
                    }

                } else {
                    HNDHelper.showErrorAlert(approveestimatebuilder.this, getResources().getString(R.string.nointernet_text));
                }

            }

            private void choosetime() {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(approveestimatebuilder.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                String addzerotohours = "" + hourOfDay;

                                String addzerotomin = "" + minute;

                                if (hourOfDay <= 9) {
                                    addzerotohours = "0" + hourOfDay;
                                }

                                if (minute <= 9) {
                                    addzerotomin = "0" + minute;
                                }

                                selectedtime = addzerotohours + ":" + addzerotomin;
                                sendrequest(approveestimatebuilder.this, ServiceConstant.Estimateapproval);

                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //finish();
                status = 2;
                sendrequest(approveestimatebuilder.this, ServiceConstant.Estimateapproval);
            }
        });

        TextViewTotal = (TextView) findViewById(R.id.TextViewTotal);
        TextViewSubTotal = (TextView) findViewById(R.id.TextViewSubTotal);
        TextViewHandyProSavings = (TextView) findViewById(R.id.TextViewHandyProSavings);
        TextViewHandyProSavingsTitle = (TextView) findViewById(R.id.TextViewHandyProSavingsTitle);

        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            paymentPost(approveestimatebuilder.this, ServiceConstant.ESTIMATE_URL);
        } else {
            HNDHelper.showErrorAlert(approveestimatebuilder.this, getResources().getString(R.string.nointernet_text));
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void paymentPost(Context mContext, String url) {


        myDialog = new ProgressDialogcreated(approveestimatebuilder.this);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());

        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        jsonParams.put("job_id", Job_id);


        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                MainMultSubItemsList = new ArrayList<>();
                arrayList = new ArrayList<>();
                Log.e("get-job-estimation-category", response);
                System.out.println("Estimation response" + response);

                String provide_estimation = "", Str_response = "", Str_jobDescription = "", Str_NeedPayment = "", Str_Currency = "", Str_BtnGroup = "";


                try {
                    JSONObject jobject = new JSONObject(response);

                    SendFlowTOFranchisee = jobject.has("withoutscheduling") && jobject.getString("withoutscheduling").equals("1");
                    provide_estimation = jobject.getString("provide_estimation");


                    if (jobject.has("booking_date_date")) {
                        eday = Integer.parseInt(jobject.getString("booking_date_date"));
                        emonth = Integer.parseInt(jobject.getString("booking_date_month"));
                        eyear = Integer.parseInt(jobject.getString("booking_date_year"));
                    }


                    grandtotal = jobject.getString("grandtotal");
                    if (jobject.has("handypro_plus_saving"))
                        HandyProSavings = jobject.getString("handypro_plus_saving");

                    estimation_itemize = jobject.getString("estimation_itemize");

                    if (jobject.has("coupon") && jobject.getString("coupon").length() > 0) {
                        TextViewCouponName.setText(jobject.getString("coupon"));
                    } else {
                        TextViewCouponNameLabel.setVisibility(View.GONE);
                        TextViewCouponName.setVisibility(View.GONE);
                    }

                    if (jobject.has("couponamt") && jobject.getString("couponamt").length() > 0) {
                        TextViewCouponAmount.setText(currency + jobject.getString("couponamt"));
                    } else {
                        TextViewCouponAmountLabel.setVisibility(View.GONE);
                        TextViewCouponAmount.setVisibility(View.GONE);
                    }


                    storeestimatehours = jobject.getString("overall_taskhours");
                    if (provide_estimation.equalsIgnoreCase("1")) {
                        String currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);

                        JSONArray jarry = jobject.getJSONArray("estimation_category");
                        if (jarry.length() > 0) {
                            JSONArray EstimationCategoryArry = new JSONArray();
                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);

                                String title = jobjects_amount.getString("service_type");

                                String whatsincluded = "";
                                if (jobjects_amount.has("whatsincluded")) {
                                    whatsincluded = jobjects_amount.getString("whatsincluded");
                                }


                                if (jobjects_amount.has("tasker_id")) {
                                    storetaskerid = jobjects_amount.getString("tasker_id");
                                } else {
                                    storetaskerid = "";
                                }


                                if (jobjects_amount.has("sub_items") && jobjects_amount.getJSONArray("sub_items").length() > 0) {
                                    JSONArray jsonArraySubItems = jobjects_amount.getJSONArray("sub_items");
                                    for (int j = 0; j < jsonArraySubItems.length(); j++) {
                                        JSONObject jsonObjectSubItems = jsonArraySubItems.getJSONObject(j);
                                        MultiSubItemPojo multiSubItemPojo = new MultiSubItemPojo();
                                        multiSubItemPojo.setName(jsonObjectSubItems.getString("name"));
                                        multiSubItemPojo.setHours(jsonObjectSubItems.getString("hours"));
                                        multiSubItemPojo.setDescription(jsonObjectSubItems.getString("description"));
                                        multiSubItemPojo.setPositionForOperation(i);
                                        multiSubItemPojo.setInnerPosition(j);
                                        multiSubItemPojo.setID(jsonObjectSubItems.getString("_id"));
                                        multiSubItemPojo.setStatus(jsonObjectSubItems.getString("status"));
                                        MainMultSubItemsList.add(multiSubItemPojo);
                                    }
                                }


                                if (jobjects_amount.has("estimated_details")) {

                                    JSONObject fareobj = jobjects_amount.getJSONObject("estimated_details");

                                    if (fareobj.has("estimation_images")) {
                                        EstimationCategoryArry = fareobj.getJSONArray("estimation_images");
                                    }

                                    String hours = fareobj.getString("hours");
                                    String materialamount;

                                    if (fareobj.has("material")) {
                                        materialamount = fareobj.getString("material");
                                        if (materialamount.equalsIgnoreCase("") || materialamount.equalsIgnoreCase(" ")) {
                                            materialamount = "";
                                        } else {
                                            materialamount = mySession.getCurrencySymbol() + materialamount;
                                        }
                                    } else {
                                        materialamount = "";
                                    }


                                    String material_desc;
                                    if (fareobj.has("material_desc")) {
                                        material_desc = getResources().getString(R.string.class_approve_estimate_material) + " " + fareobj.getString("material_desc");
                                    } else {
                                        material_desc = "";
                                    }


                                    String depositamount;
                                    if (fareobj.has("depositamount")) {
                                        depositamount = mySession.getCurrencySymbol() + fareobj.getString("depositamount");
                                    } else {
                                        depositamount = "";
                                    }


                                    String description = "";
                                    if (fareobj.has("description")) {
                                        if (fareobj.getString("description").equalsIgnoreCase("") || fareobj.getString("description").equalsIgnoreCase(" ")) {
                                            description = "";
                                        } else {
                                            description = "Job Description: " + fareobj.getString("description");
                                        }

                                    } else {
                                        description = "";
                                    }

                                    String materialam;
                                    if (fareobj.has("description")) {
                                        materialam = "\nJob Description: " + fareobj.getString("description");
                                    } else {
                                        materialam = "";
                                    }


                                    String singleamount;
                                    if (fareobj.has("amount")) {
                                        singleamount = fareobj.getString("amount");
                                        if (singleamount.equalsIgnoreCase("") || singleamount.equalsIgnoreCase(" ")) {
                                            singleamount = "";
                                        } else {
                                            singleamount = mySession.getCurrencySymbol() + singleamount;
                                        }

                                    } else {
                                        singleamount = "";
                                    }


                                    String helper_amount;
                                    if (fareobj.has("helper_amount")) {
                                        helper_amount = fareobj.getString("helper_amount");
                                        if (helper_amount.equalsIgnoreCase("") || helper_amount.equalsIgnoreCase(" ") || helper_amount.equalsIgnoreCase("null")) {
                                            helper_amount = "";
                                        } else {
                                            helper_amount = mySession.getCurrencySymbol() + helper_amount;
                                        }

                                    } else {
                                        helper_amount = "";
                                    }


                                    String totalamount = fareobj.getString("totalamount");


                                    String status;
                                    if (fareobj.has("status")) {
                                        status = fareobj.getString("status");
                                    } else {

                                        status = "";
                                    }


                                    String service_id = fareobj.getString("service_id");

                                    approveeditbuilderpojo atomPayment = new approveeditbuilderpojo();
                                    atomPayment.setVal2(Float.parseFloat(hours));
                                    atomPayment.setJobtype(title);
                                    atomPayment.setJobdescription(description);
                                    atomPayment.setWhatsincluded(whatsincluded);
                                    atomPayment.setTotal(Double.parseDouble(totalamount));
                                    atomPayment.setMaterial(materialamount);
                                    atomPayment.setMaterialdescription(material_desc);
                                    atomPayment.setDepositamount(depositamount);

                                    atomPayment.setCheckedvalue(0);
                                    atomPayment.setServiceid(service_id);
                                    atomPayment.setStatus(status);
                                    atomPayment.setSingleamount(singleamount);
                                    atomPayment.setHelperamount(helper_amount);

                                    List<String> ImageArray = new ArrayList<>();
                                    for (int img = 0; img < EstimationCategoryArry.length(); img++) {
                                        ImageArray.add(EstimationCategoryArry.getString(img));
                                    }
                                    EstimatedDetails estimatedDetails = new EstimatedDetails();
                                    estimatedDetails.setEstimationImages(ImageArray);
                                    atomPayment.setEstimatedDetails(estimatedDetails);

                                    arrayList.add(atomPayment);

                                }


                            }

                        }

                        if (arrayList.size() > 0) {

                            if (jobject.has("estimation_status")) {
                                String estimation_status = jobject.getString("estimation_status");
                                hintshow.setVisibility(View.VISIBLE);
                                if (estimation_status.equalsIgnoreCase("0")) {
                                    hintshow.setText(getResources().getString(R.string.class_approve_estimate_approval));
                                    done.setVisibility(View.VISIBLE);
                                    reject.setVisibility(View.VISIBLE);
                                } else if (estimation_status.equalsIgnoreCase("1")) {
                                    hintshow.setText(getResources().getString(R.string.class_approve_estimate_accepted));
                                    done.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                } else if (estimation_status.equalsIgnoreCase("2")) {
                                    hintshow.setText(getResources().getString(R.string.class_approve_estimate_rejected));
                                    done.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                }

                            } else {
                                hintshow.setVisibility(View.GONE);
                                hintshow.setText(getResources().getString(R.string.class_approve_estimate_waiting));
                                done.setVisibility(View.VISIBLE);
                                reject.setVisibility(View.VISIBLE);
                            }


                            displaylist.setVisibility(View.VISIBLE);
                            nodata.setVisibility(View.GONE);
                            downoptions.setVisibility(View.VISIBLE);

                            if (grandtotal.contains(".")) {
                                if (grandtotal.endsWith(".0") || grandtotal.endsWith(".5") || grandtotal.endsWith(".1") || grandtotal.endsWith(".2") || grandtotal.endsWith(".3") || grandtotal.endsWith(".4") || grandtotal.endsWith(".6") || grandtotal.endsWith(".7") || grandtotal.endsWith(".8") || grandtotal.endsWith(".9")) {
                                    grandtotal = grandtotal + "0";
                                }
                            } else {
                                grandtotal = grandtotal + ".00";
                            }

                            TextViewSubTotal.setText(currency + grandtotal);
                            if (HandyProSavings.equalsIgnoreCase("0")) {
                                TextViewHandyProSavingsTitle.setVisibility(View.GONE);
                                TextViewHandyProSavings.setVisibility(View.GONE);
                            } else {
                                TextViewHandyProSavingsTitle.setVisibility(View.VISIBLE);
                                TextViewHandyProSavings.setText(currency + HandyProSavings);
                            }


                            int IntSubTotalNumber = 0, IntHandyProSavings = 0;
                            double DoubleSubTotalNumber = 0.0, DoubleHandyProSavings = 0.0;

                            if (grandtotal.length() >= 1) {
                                if (grandtotal.matches("[-+]?[0-9]*\\.?[0-9]+")) {
                                    DoubleSubTotalNumber = Double.parseDouble(grandtotal);
                                } else if (grandtotal.matches("[0-9]+")) {
                                    IntSubTotalNumber = Integer.parseInt(grandtotal);
                                }
                            }

                            if (HandyProSavings.length() >= 1) {
                                if (HandyProSavings.matches("[-+]?[0-9]*\\.?[0-9]+")) {
                                    DoubleHandyProSavings = Double.parseDouble(HandyProSavings);
                                    if (DoubleSubTotalNumber > 0) {
                                        String Total = String.valueOf(DoubleSubTotalNumber - DoubleHandyProSavings);
                                        if (Total.endsWith(".0") || Total.endsWith(".5") || Total.endsWith(".1") || Total.endsWith(".2") || Total.endsWith(".3") || Total.endsWith(".4") || Total.endsWith(".6") || Total.endsWith(".7") || Total.endsWith(".8") || Total.endsWith(".9")) {
                                            TextViewTotal.setText(currency + Total + "0");
                                        } else {
                                            TextViewTotal.setText(currency + Total);
                                        }
                                    } else if (IntSubTotalNumber > 0) {
                                        String Total = String.valueOf(IntSubTotalNumber - DoubleHandyProSavings);
                                        if (Total.endsWith(".0") || Total.endsWith(".5") || Total.endsWith(".1") || Total.endsWith(".2") || Total.endsWith(".3") || Total.endsWith(".4") || Total.endsWith(".6") || Total.endsWith(".7") || Total.endsWith(".8") || Total.endsWith(".9")) {
                                            TextViewTotal.setText(currency + Total + "0");
                                        } else {
                                            TextViewTotal.setText(currency + Total);
                                        }
                                    }
                                } else if (grandtotal.matches("[0-9]+")) {
                                    IntHandyProSavings = Integer.parseInt(HandyProSavings);
                                    if (DoubleSubTotalNumber > 0) {
                                        String Total = String.valueOf(DoubleSubTotalNumber - IntHandyProSavings);
                                        if (Total.endsWith(".0") || Total.endsWith(".5") || Total.endsWith(".1") || Total.endsWith(".2") || Total.endsWith(".3") || Total.endsWith(".4") || Total.endsWith(".6") || Total.endsWith(".7") || Total.endsWith(".8") || Total.endsWith(".9")) {
                                            TextViewTotal.setText(currency + Total + "0");
                                        } else {
                                            TextViewTotal.setText(currency + Total);
                                        }
                                    } else if (IntSubTotalNumber > 0) {
                                        String Total = String.valueOf(IntSubTotalNumber - DoubleHandyProSavings);
                                        if (Total.endsWith(".0") || Total.endsWith(".5") || Total.endsWith(".1") || Total.endsWith(".2") || Total.endsWith(".3") || Total.endsWith(".4") || Total.endsWith(".6") || Total.endsWith(".7") || Total.endsWith(".8") || Total.endsWith(".9")) {
                                            TextViewTotal.setText(currency + Total + "0");
                                        } else {
                                            TextViewTotal.setText(currency + Total);
                                        }
                                    }
                                }
                            }

                            /*if (grandtotal.contains(".")) {
                                if (grandtotal.endsWith(".0") || grandtotal.endsWith(".5") || grandtotal.endsWith(".1") || grandtotal.endsWith(".2") || grandtotal.endsWith(".3") || grandtotal.endsWith(".4") || grandtotal.endsWith(".6") || grandtotal.endsWith(".7") || grandtotal.endsWith(".8") || grandtotal.endsWith(".9")) {
                                    TextViewTotal.setText(mySession.getCurrencySymbol() + grandtotal + "0");
                                } else {
                                    TextViewTotal.setText(mySession.getCurrencySymbol() + grandtotal);
                                }

                            } else {
                                TextViewTotal.setText(mySession.getCurrencySymbol() + grandtotal + ".00");
                            }*/


                            adapter = new editestimationadapter(approveestimatebuilder.this, R.layout.approvereo_item2, arrayList);
                            listView.setAdapter(adapter);

                        } else {

                            downoptions.setVisibility(View.GONE);
                            displaylist.setVisibility(View.GONE);
                            nodata.setVisibility(View.VISIBLE);
                        }

                    } else {
                        final PkDialog mDialog = new PkDialog(approveestimatebuilder.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.class_approve_estimate_failed));
                        mDialog.setDialogMessage(getResources().getString(R.string.class_approve_estimate_sorry));
                        mDialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        });
                        mDialog.show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void sendrequest(final Context aContext, String url) {


        myDialog = new ProgressDialogcreated(approveestimatebuilder.this);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", mySession.getUserDetails().getUserId());
        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        params.put("job_id", Job_id);
        params.put("approval_status", "" + status);

        if (status == 1) {
            params.put("pickup_date", "" + selecteddate);
            params.put("pickup_time", "" + selectedtime);

        }


        if (status == 2) {

            for (int jk = 0; jk < arrayList.size(); jk++) {
                params.put("est[" + jk + "][service_id]", arrayList.get(jk).getServiceid());
                if (arrayList.get(jk).getCheckedvalue() == 0) {
                    params.put("est[" + jk + "][status]", "2");
                } else {
                    params.put("est[" + jk + "][status]", "2");
                }
            }
        } else {
            for (int jk = 0; jk < arrayList.size(); jk++) {
                params.put("est[" + jk + "][service_id]", arrayList.get(jk).getServiceid());
                if (arrayList.get(jk).getCheckedvalue() == 0) {
                    params.put("est[" + jk + "][status]", "2");
                } else {
                    params.put("est[" + jk + "][status]", "" + arrayList.get(jk).getCheckedvalue());
                }
            }
        }

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------get Confirm Category list Response----------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    String message = object.getString("response");
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        if (status == 1) {
                            hintshow.setText(getResources().getString(R.string.class_approve_estimate_accepted));
                            done.setVisibility(View.GONE);
                            reject.setVisibility(View.GONE);
                        } else {

                            hintshow.setText(getResources().getString(R.string.class_approve_estimate_rejected));
                            done.setVisibility(View.GONE);
                            reject.setVisibility(View.GONE);
                        }

                        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("EXIT", true);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    } else {
                        final PkDialog mDialog = new PkDialog(approveestimatebuilder.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.rating_header_sorry_textView));
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();
                    }
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block


                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    public class editestimationadapter extends ArrayAdapter<approveeditbuilderpojo> {

        Typeface tf;
        SharedPreferences pref;
        String currency;
        private ArrayList<approveeditbuilderpojo> items;
        private int layoutResourceId;
        private Context context;
        private GridViewAdapter gridAdapter;

        public editestimationadapter(Context context, int layoutResourceId, ArrayList<approveeditbuilderpojo> items) {
            super(context, layoutResourceId, items);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.items = items;
            tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Poppins-Regular.ttf");
            pref = context.getSharedPreferences("logindetails", 0);
            currency = pref.getString("myCurrencySymbol", "");
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            editestimationadapter.Holder holder = null;

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new editestimationadapter.Holder();
            holder.bean = items.get(position);

            holder.materialdn = (EditText) row.findViewById(R.id.materialdn);
            holder.etVal1 = (EditText) row.findViewById(R.id.edVal1);
            holder.etVal2 = (EditText) row.findViewById(R.id.edValue2);

            holder.deletecraftman = (CheckBox) row.findViewById(R.id.deletecraftman);


            holder.etVal1.setTypeface(tf);
            holder.etVal2.setTypeface(tf);

            holder.materialdn.setTypeface(tf);


            holder.jobtytpeheading = (TextView) row.findViewById(R.id.jobtytpeheading);
            holder.jobdescriptionheading = (TextView) row.findViewById(R.id.jobdescriptionheading);
            holder.amountheading = (TextView) row.findViewById(R.id.amountheading);
            holder.hourheadining = (TextView) row.findViewById(R.id.hourheadining);
            holder.materialheading = (TextView) row.findViewById(R.id.materialheading);
            holder.materialdescheading = (TextView) row.findViewById(R.id.materialdescheading);
            holder.totalamount = (TextView) row.findViewById(R.id.totalamount);

            holder.jobdhead = (TextView) row.findViewById(R.id.jobdhead);

            holder.matamlin = (LinearLayout) row.findViewById(R.id.matamlin);
            holder.jobamountlin = (LinearLayout) row.findViewById(R.id.jobamountlin);
            holder.helperamountlin = (LinearLayout) row.findViewById(R.id.helperamountlin);

            holder.depositlin = (LinearLayout) row.findViewById(R.id.depositlin);
            holder.customizehide = (LinearLayout) row.findViewById(R.id.customizehide);


            holder.whatsincluded = (TextView) row.findViewById(R.id.whatsincluded);


            holder.depositamount = (TextView) row.findViewById(R.id.depositamount);
            holder.jobamount = (TextView) row.findViewById(R.id.jobamount);
            holder.matamt = (TextView) row.findViewById(R.id.matamt);
            holder.helperamount = (TextView) row.findViewById(R.id.helperamount);

            holder.materialdhead = (TextView) row.findViewById(R.id.materialdhead);
            holder.gridview = (MyGridView) row.findViewById(R.id.gridview);
            holder.RecyclerViewDynamic = (RecyclerView) row.findViewById(R.id.RecyclerViewDynamic);
            LinearLayoutManager llm = new LinearLayoutManager(approveestimatebuilder.this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            holder.RecyclerViewDynamic.setLayoutManager(llm);
            holder.RecyclerViewDynamic.setHasFixedSize(true);


            holder.jobamount.setTypeface(tf);
            holder.matamt.setTypeface(tf);
            holder.helperamount.setTypeface(tf);

            holder.jobdhead.setTypeface(tf);
            holder.materialdhead.setTypeface(tf);

            holder.depositamount.setTypeface(tf);
            holder.jobtytpeheading.setTypeface(tf);
            holder.jobdescriptionheading.setTypeface(tf);
            holder.amountheading.setTypeface(tf);
            holder.hourheadining.setTypeface(tf);
            holder.materialheading.setTypeface(tf);
            holder.materialdescheading.setTypeface(tf);
            holder.totalamount.setTypeface(tf);
            holder.totalamount.setTypeface(tf);


            holder.unhideimgae = (ImageView) row.findViewById(R.id.unhideimgae);

            holder.materialdesc = (EditText) row.findViewById(R.id.materialdesc);
            holder.materialamount = (EditText) row.findViewById(R.id.materialamount);
            holder.jobdescription = (EditText) row.findViewById(R.id.jobdescription);

            holder.materialdesc.setTypeface(tf);
            holder.jobdescription.setTypeface(tf);
            holder.materialdescheading.setTypeface(tf);
            holder.materialamount.setTypeface(tf);


            row.setTag(holder);

            setupItem(holder);

            SetUpSubItems(holder, position);

            if (items.get(position).getEstimatedDetails().getEstimationImages().size() > 0) {
                holder.gridview.setEnabled(false);
                gridAdapter = new GridViewAdapter(approveestimatebuilder.this, R.layout.estimation_approve_grid_item, items.get(position).getEstimatedDetails().getEstimationImages());
                holder.gridview.setAdapter(gridAdapter);
                holder.gridview.setVisibility(View.VISIBLE);
            } else {
                holder.gridview.setVisibility(View.GONE);
            }

            return row;
        }

        private void SetUpSubItems(final editestimationadapter.Holder holder, final int position) {
            if (MainMultSubItemsList != null && MainMultSubItemsList.size() > 0) {
                List<MultiSubItemPojo> TempMainMultSubItemsList = new ArrayList<>();
                for (int i = 0; i < MainMultSubItemsList.size(); i++) {
                    if (MainMultSubItemsList.get(i).getPositionForOperation() == position && !MainMultSubItemsList.get(i).getStatus().equals("User Rejected")) {
                        MultiSubItemPojo multiSubItemPojo = new MultiSubItemPojo();
                        multiSubItemPojo.setName(MainMultSubItemsList.get(i).getName());
                        multiSubItemPojo.setHours(MainMultSubItemsList.get(i).getHours());
                        multiSubItemPojo.setDescription(MainMultSubItemsList.get(i).getDescription());
                        multiSubItemPojo.setPositionForOperation(MainMultSubItemsList.get(i).getPositionForOperation());
                        multiSubItemPojo.setInnerPosition(MainMultSubItemsList.get(i).getInnerPosition());
                        multiSubItemPojo.setID(MainMultSubItemsList.get(i).getID());
                        multiSubItemPojo.setStatus(MainMultSubItemsList.get(i).getStatus());
                        TempMainMultSubItemsList.add(multiSubItemPojo);
                    }
                }

                recyclerViewDynamicAdapter = new RecyclerViewDynamicAdapter(
                        approveestimatebuilder.this, TempMainMultSubItemsList, position, "ApproveEstimateBuilder", new RecyclerViewDynamicAdapter.RecyclerViewDynamicAdapterInterface() {
                    @Override
                    public void Delete(int InnerPosition) {
                        if (MainMultSubItemsList.size() > 1) {
                            for (int i = 0; i < MainMultSubItemsList.size(); i++) {
                                if (MainMultSubItemsList.get(i).getInnerPosition() == InnerPosition && MainMultSubItemsList.get(i).getPositionForOperation() == position) {

                                    final int finalI = i;
                                    new AlertDialog.Builder(approveestimatebuilder.this)
                                            .setTitle("Delete")
                                            .setMessage("Are you sure want to delete this sub item?")

                                            // Specifying a listener allows you to take an action before dismissing the dialog.
                                            // The dialog is automatically dismissed when a dialog button is clicked.
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Continue with delete operation
                                                    dialog.dismiss();
                                                    loadingDialog();

                                                    HashMap<String, String> jsonParams = new HashMap<String, String>();

                                                    jsonParams.put("user_id", mySession.getUserDetails().getUserId());

                                                    SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                                                    String Job_id = Job_idpref.getString("jobid", "");
                                                    jsonParams.put("job_id", Job_id);

                                                    jsonParams.put("service_id", holder.bean.getServiceid());
                                                    jsonParams.put("subitem_id", MainMultSubItemsList.get(finalI).getID());

                                                    ServiceRequest mservicerequest = new ServiceRequest(approveestimatebuilder.this);
                                                    mservicerequest.makeServiceRequest(ServiceConstant.SubItemsDelete, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
                                                        @Override
                                                        public void onCompleteListener(String response) {
                                                            dismissDialog();
                                                            try {
                                                                JSONObject jobject = new JSONObject(response);
                                                                if (jobject.getString("status").equalsIgnoreCase("1")) {
                                                                    if (isInternetPresent) {
                                                                        paymentPost(approveestimatebuilder.this, ServiceConstant.ESTIMATE_URL);
                                                                    } else {
                                                                        final PkDialog mDialog = new PkDialog(approveestimatebuilder.this);
                                                                        mDialog.setDialogTitle(getResources().getString(R.string.class_booking_confirmation_internet));
                                                                        mDialog.setDialogMessage(getResources().getString(R.string.nointernet_text));
                                                                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                                                            @Override
                                                                            public void onClick(View v) {
                                                                                mDialog.dismiss();
                                                                            }
                                                                        });
                                                                        mDialog.show();
                                                                    }
                                                                } else if (jobject.getString("status").equalsIgnoreCase("0")) {
                                                                    final PkDialog mDialog = new PkDialog(approveestimatebuilder.this);
                                                                    mDialog.setDialogTitle(getResources().getString(R.string.dialog_sorry));
                                                                    mDialog.setDialogMessage(jobject.getString("response"));
                                                                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(View v) {
                                                                            mDialog.dismiss();
                                                                            if (isInternetPresent) {
                                                                                paymentPost(approveestimatebuilder.this, ServiceConstant.ESTIMATE_URL);
                                                                            } else {
                                                                                final PkDialog mDialog = new PkDialog(approveestimatebuilder.this);
                                                                                mDialog.setDialogTitle(getResources().getString(R.string.class_booking_confirmation_internet));
                                                                                mDialog.setDialogMessage(getResources().getString(R.string.nointernet_text));
                                                                                mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                                                                    @Override
                                                                                    public void onClick(View v) {
                                                                                        mDialog.dismiss();
                                                                                    }
                                                                                });
                                                                                mDialog.show();
                                                                            }
                                                                        }
                                                                    });
                                                                    mDialog.show();
                                                                }
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }

                                                        @Override
                                                        public void onErrorListener() {
                                                            dismissDialog();
                                                        }
                                                    });
                                                }
                                            })

                                            // A null listener allows the button to dismiss the dialog and take no further action.
                                            .setNegativeButton(android.R.string.no, null)
                                            .setIcon(R.mipmap.handy_app_icon)
                                            .show();
                                }
                            }
                        }
                    }
                });

                holder.RecyclerViewDynamic.setAdapter(recyclerViewDynamicAdapter);
                recyclerViewDynamicAdapter.notifyDataSetChanged();
                adapter.notifyDataSetChanged();

            }
        }

        private void setupItem(final editestimationadapter.Holder holder) {


            if (holder.bean.getWhatsincluded().equals("")) {
                holder.whatsincluded.setVisibility(View.GONE);
            } else {
                holder.whatsincluded.setVisibility(View.VISIBLE);
            }

            holder.whatsincluded.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HNDHelper.showErrorAlert(approveestimatebuilder.this, holder.bean.getWhatsincluded());
                }
            });


            if (estimation_itemize.equalsIgnoreCase("1")) {
                holder.customizehide.setVisibility(View.GONE);
            } else {
                holder.customizehide.setVisibility(View.VISIBLE);
            }
            holder.jobdescription.setText(holder.bean.getJobdescription());
            holder.jobtytpeheading.setText(holder.bean.getJobtype());


            if (holder.bean.getStatus().equals("2")) {
                holder.unhideimgae.setBackgroundResource(R.drawable.cancelmark);
                holder.unhideimgae.setVisibility(View.VISIBLE);
                holder.deletecraftman.setVisibility(View.GONE);
            } else if (holder.bean.getStatus().equals("1")) {
                holder.unhideimgae.setBackgroundResource(R.drawable.ticked);
                holder.unhideimgae.setVisibility(View.VISIBLE);
                holder.deletecraftman.setVisibility(View.GONE);
            } else if (holder.bean.getStatus().equals("0")) {
                holder.unhideimgae.setVisibility(View.GONE);
                holder.deletecraftman.setVisibility(View.VISIBLE);
            } else {
                holder.unhideimgae.setVisibility(View.GONE);
                holder.deletecraftman.setVisibility(View.GONE);
            }


            if (holder.bean.getCheckedvalue() == 0) {
                holder.deletecraftman.setChecked(false);

            } else {
                holder.deletecraftman.setChecked(true);
            }

            holder.deletecraftman.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                    if (buttonView.isChecked()) {
                        holder.bean.setCheckedvalue(1);
                        notifyDataSetChanged();
                    } else {
                        holder.bean.setCheckedvalue(0);
                        notifyDataSetChanged();
                    }
                }
            });

            if (holder.bean.getMaterialdescription().equalsIgnoreCase("") || holder.bean.getMaterialdescription().equalsIgnoreCase(" ")) {
                holder.materialdhead.setText(getResources().getString(R.string.activity_approve_estimate_no_material));
            } else {
                SpannableStringBuilder builders = new SpannableStringBuilder();
                String string3 = holder.bean.getMaterialdescription().split(":")[0] + ":";
                SpannableString redSpannable1 = new SpannableString(string3);
                redSpannable1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, string3.length(), 0);
                builders.append(redSpannable1);

                String string4 = holder.bean.getMaterialdescription().split(":")[1];
                SpannableString whiteSpannable1 = new SpannableString(string4);
                whiteSpannable1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black_color)), 0, string4.length(), 0);
                builders.append(whiteSpannable1);
                holder.materialdhead.setText(builders, TextView.BufferType.SPANNABLE);
            }

            if (holder.bean.getJobdescription().equalsIgnoreCase("") || holder.bean.getJobdescription().equalsIgnoreCase(" ")) {
                holder.jobdhead.setVisibility(View.GONE);
            } else {
                holder.jobdhead.setVisibility(View.VISIBLE);

                SpannableStringBuilder builders = new SpannableStringBuilder();
                String string3 = holder.bean.getJobdescription().split(":")[0] + ":";
                SpannableString redSpannable1 = new SpannableString(string3);
                redSpannable1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, string3.length(), 0);
                builders.append(redSpannable1);

                String string4 = holder.bean.getJobdescription().split(":")[1];
                SpannableString whiteSpannable1 = new SpannableString(string4);
                whiteSpannable1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black_color)), 0, string4.length(), 0);
                builders.append(whiteSpannable1);
                holder.jobdhead.setText(builders, TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getDepositamount().equalsIgnoreCase("") || holder.bean.getDepositamount().equalsIgnoreCase("0") || holder.bean.getDepositamount().equalsIgnoreCase("$0") || holder.bean.getDepositamount().equalsIgnoreCase(" ")) {
                holder.depositlin.setVisibility(View.GONE);
            } else {
                holder.depositlin.setVisibility(View.VISIBLE);
                holder.depositamount.setText(holder.bean.getDepositamount());
            }


            if (holder.bean.getSingleamount().equalsIgnoreCase("") || holder.bean.getSingleamount().equalsIgnoreCase(" ")) {
                holder.jobamountlin.setVisibility(View.GONE);
            } else {
                holder.jobamountlin.setVisibility(View.VISIBLE);
                holder.jobamount.setText(Html.fromHtml(holder.bean.getSingleamount()), TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getHelperamount().equalsIgnoreCase("")
                    || holder.bean.getHelperamount().equalsIgnoreCase(" ")
                    || holder.bean.getHelperamount().equalsIgnoreCase("null")) {
                holder.helperamountlin.setVisibility(View.GONE);
            } else {
                holder.helperamountlin.setVisibility(View.VISIBLE);
                holder.helperamount.setText(Html.fromHtml(holder.bean.getHelperamount()), TextView.BufferType.SPANNABLE);
            }


            if (holder.bean.getMaterial().equalsIgnoreCase("") || holder.bean.getMaterial().equalsIgnoreCase(" ")) {
                holder.matamlin.setVisibility(View.GONE);
            } else {
                holder.matamlin.setVisibility(View.VISIBLE);
                holder.matamt.setText(Html.fromHtml(holder.bean.getMaterial()), TextView.BufferType.SPANNABLE);
            }


            holder.etVal1.setText(String.valueOf(holder.bean.getVal1()));
            holder.etVal2.setText(String.valueOf(holder.bean.getVal2()));

            if (String.valueOf(holder.bean.getTotal()).endsWith(".0") || String.valueOf(holder.bean.getTotal()).endsWith(".5") || String.valueOf(holder.bean.getTotal()).endsWith(".1") || String.valueOf(holder.bean.getTotal()).endsWith(".2") || String.valueOf(holder.bean.getTotal()).endsWith(".3") || String.valueOf(holder.bean.getTotal()).endsWith(".4") || String.valueOf(holder.bean.getTotal()).endsWith(".6") || String.valueOf(holder.bean.getTotal()).endsWith(".7") || String.valueOf(holder.bean.getTotal()).endsWith(".8") || String.valueOf(holder.bean.getTotal()).endsWith(".9")) {
                holder.totalamount.setText(mySession.getCurrencySymbol() + String.valueOf(holder.bean.getTotal()) + "0");
            } else {
                holder.totalamount.setText(mySession.getCurrencySymbol() + String.valueOf(holder.bean.getTotal()));
            }


        }

        public class Holder {
            CheckBox deletecraftman;
            approveeditbuilderpojo bean;
            EditText etVal1;
            ImageView unhideimgae;
            LinearLayout matamlin, helperamountlin, jobamountlin, depositlin, customizehide;
            EditText etVal2, materialdn, jobdescription, materialamount, materialdesc;
            TextView materialdhead, jobamount, whatsincluded, matamt, depositamount, jobdhead, helperamount, totalamount, jobtytpeheading, jobdescriptionheading, amountheading, hourheadining, materialheading, materialdescheading;
            MyGridView gridview;
            RecyclerView RecyclerViewDynamic;

        }
    }

    public class GridViewAdapter extends ArrayAdapter {
        private Context context;
        private List<String> ListImage = new ArrayList<String>();
        private int resourceId;
        private LayoutInflater inflater;

        private GridViewAdapter(Context context, int resourceId, List<String> ListImage) {
            super(context, resourceId, ListImage);
            this.resourceId = resourceId;
            this.context = context;
            this.ListImage = ListImage;
            inflater = LayoutInflater.from(context);

        }

        //I prefer to have Holder to keep all controls
        //So that I can recycle easily in getView
        class ViewHolder {
            ImageView image, close;
            LinearLayout LinearLayoutMainHolder;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            GridViewAdapter.ViewHolder holder = null;
            if (row == null) {
                row = inflater.inflate(resourceId, parent, false);
                holder = new GridViewAdapter.ViewHolder();
                holder.image = (ImageView) row.findViewById(R.id.image);
                holder.close = (ImageView) row.findViewById(R.id.close);
                holder.LinearLayoutMainHolder = (LinearLayout) row.findViewById(R.id.LinearLayoutMainHolder);
                row.setTag(holder);
            } else {
                holder = (GridViewAdapter.ViewHolder) row.getTag(); //Easy to recycle view
            }

            holder.close.setVisibility(View.GONE);

            Picasso.get().load(ListImage.get(position))
                    .resize(160, 160)
                    .placeholder(R.drawable.nouserimg).into(holder.image);


            /*Picasso.with(context).load(ListImage.get(position))
                    .resize(160, 160)
                    .placeholder(R.drawable.nouserimg)
                    .into(holder.image);*/
            holder.LinearLayoutMainHolder.setVisibility(View.VISIBLE);
            holder.close.setVisibility(View.GONE);


            return row;
        }
    }

    void MakeRequestToFranchisee(ArrayList<String> ServiceIdList) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());

        SharedPreferences Job_idpref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
        String Job_id = Job_idpref.getString("jobid", "");
        jsonParams.put("job_id", Job_id);
        for (int i = 0; i < ServiceIdList.size(); i++) {
            jsonParams.put("service_id[" + i + "]", ServiceIdList.get(i));
        }


        ServiceRequest mservicerequest = new ServiceRequest(approveestimatebuilder.this);
        mservicerequest.makeServiceRequest(ServiceConstant.ApprovalWithoutSchedule, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("client-approval-for-withoutschedule", response);
                System.out.println("client-approval-for-withoutschedule" + response);
                try {
                    JSONObject jobject = new JSONObject(response);

                    if (jobject.has("status") && jobject.getString("status").equals("1")) {
                        final PkDialog mDialog = new PkDialog(approveestimatebuilder.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                        mDialog.setDialogMessage(jobject.getString("response"));
                        mDialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                onBackPressed();
                            }
                        });
                        mDialog.show();
                    } else {
                        final PkDialog mDialog = new PkDialog(approveestimatebuilder.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.class_approve_estimate_failed));
                        mDialog.setDialogMessage(jobject.getString("response"));
                        mDialog.setPositiveButton(getResources().getString(R.string.server_ok_lable_header), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        });
                        mDialog.show();
                    }


                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void loadingDialog() {

        dialog = new LoadingDialog(approveestimatebuilder.this);
        dialog.setLoadingTitle(getResources().getString(R.string.loading_in));
        dialog.show();
    }

    private void dismissDialog() {

        dialog.dismiss();
    }

}
