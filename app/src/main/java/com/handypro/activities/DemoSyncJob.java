package com.handypro.activities;

import android.content.Intent;
import androidx.annotation.NonNull;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;
import com.handypro.core.socket.SocketHandler;
import com.handypro.fcm.MyFirebaseMessagingService;
import com.handypro.sharedpreference.SharedPreference;

/**
 * Created by user145 on 6/23/2018.
 */
public class DemoSyncJob extends Job {

    public static final String TAG = ">>>> job_demo_tag";

    public static void scheduleJob() {
        new JobRequest.Builder(DemoSyncJob.TAG)
                .setExecutionWindow(4_000L, 4_000L)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    @Override
    @NonNull
    protected Result onRunJob(Params params) {

        Log.d(TAG, "onRunJob: ");


        try {
            SharedPreference mySession = new SharedPreference(this.getContext());
            if (mySession.getLogInStatus() == false) {
            } else {
                SocketHandler.getInstance(this.getContext()).getSocketManager().connect();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }



     /*   try {
            Intent intent1 = new Intent(this.getContext(), ChatMessageService.class);
            this.getContext().startService(intent1);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        try {
            Intent intent1 = new Intent(this.getContext(), MyFirebaseMessagingService.class);
            this.getContext().startService(intent1);
        } catch (Exception e) {
            e.printStackTrace();
        }


        scheduleJob();

        return Result.SUCCESS;
    }


}
