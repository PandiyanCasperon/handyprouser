package com.handypro.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.handypro.R;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;

/**
 * Created by CAS63 on 2/16/2018.
 */

public class ForgotPasswordOtpActivity extends AppCompatActivity implements View.OnClickListener {
    private CustomEdittext myOTPEdtTxt;
    private CustomButton mySendOTPBTN;
    private String myEmailSTR = "", myOTPStatusSTR = "", myVerfCodeSTR = "";
    private RelativeLayout myBackLAY;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_otp);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInit();
        getIntentValues();
    }

    private void getIntentValues() {
        Intent intent = getIntent();
        myEmailSTR = intent.getStringExtra("Intent_email");
        myOTPStatusSTR = intent.getStringExtra("Intent_Otp_Status");
        myVerfCodeSTR = intent.getStringExtra("Intent_verificationCode");

        setOTPInEditTxt();
    }

    private void setOTPInEditTxt() {
        if (myOTPStatusSTR.equalsIgnoreCase("development")) {
            myOTPEdtTxt.setText(myVerfCodeSTR);
        } else {
            myOTPEdtTxt.setText("");
        }
        myOTPEdtTxt.setSelection(myOTPEdtTxt.getText().length());
    }

    private void classAndWidgetInit() {
        myOTPEdtTxt = (CustomEdittext) findViewById(R.id.activity_forgot_password_otp_verification_entered_otp_ET);
        mySendOTPBTN = (CustomButton) findViewById(R.id.activity_forgot_password_otp_verification_BTN_send);
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_forgot_password_otp_verification_LAY_bsck);


        myBackLAY.setOnClickListener(this);
        mySendOTPBTN.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_forgot_password_otp_verification_LAY_bsck:
                finish();
                break;

            case R.id.activity_forgot_password_otp_verification_BTN_send:

                Intent i = new Intent(ForgotPasswordOtpActivity.this, ResetPasswordActivity.class);
                i.putExtra("Intent_verificationCode", myVerfCodeSTR);
                startActivity(i);
                finish();

                break;
        }
    }
}
