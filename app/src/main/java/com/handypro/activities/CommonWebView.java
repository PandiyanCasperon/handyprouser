package com.handypro.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.textview.BoldCustomTextView;

import java.util.HashMap;

/**
 * Created by CAS63 on 2/19/2018.
 */

public class CommonWebView extends AppCompatActivity {
    private RelativeLayout myBackLAY;
    private WebView myWebView;
    private String myWebUrlStr = "",myHeaderTitleStr = "",type="";
    private ProgressDialogcreated myDialog;
    private BoldCustomTextView myTitleTXT;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInit();
        getIntentValues();
        setSettings();


        if(type.equals("about"))
        {
            myWebView.getSettings().setBuiltInZoomControls(true);
            myWebView.getSettings().setDomStorageEnabled(true);

            // Zoom out the best fit your screen
            myWebView.getSettings().setLoadWithOverviewMode(true);
            myWebView.getSettings().setUseWideViewPort(true);

            myTitleTXT.setText(myHeaderTitleStr);
              startWebView(myWebUrlStr);
        }
        else
        {
            postRegisterRequest(CommonWebView.this, ServiceConstant.Terms_Conditions_Url);
        }



    }

    private void postRegisterRequest(final Context mContext, String url) {
        myDialog = new ProgressDialogcreated(CommonWebView.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();

        if(type.equals("reg"))
        {
            jsonParams.put("zipcode", myWebUrlStr);
            jsonParams.put("app", "user");
        }
        else
        {
            jsonParams.put("franchisee", myWebUrlStr);
            jsonParams.put("app", "user");
        }







        ServiceRequest mRequest = new ServiceRequest(mContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                System.out.println("---------register response------------" + response);
                myWebView.loadData(response, "text/html", "UTF-8");
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }


    private void getIntentValues() {
        Intent data = getIntent();
        myWebUrlStr = data.getStringExtra("url");

        myHeaderTitleStr = data.getStringExtra("header");


        type = data.getStringExtra("type");


    }
    private void startWebView(String aURL) {
        ShowProgessBar();

        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            @Override
            public void onLoadResource(WebView view, String url) {
            }

            @Override
            public void onPageFinished(WebView view, String url) {

                if (myDialog != null && myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                System.out.println("------------url-$$$$$------" + url);

            }
        });

        //Load url in webView
        myWebView.loadUrl(aURL);
    }


    private void ShowProgessBar() {
        myDialog = new ProgressDialogcreated(CommonWebView.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
    }

    private void classAndWidgetInit() {
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_webview_LAY_back);
        myWebView = (WebView) findViewById(R.id.activity_webview_View);
        myTitleTXT = (BoldCustomTextView) findViewById(R.id.activity_webview_TXT_title);

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void setSettings() {
        myWebView.getSettings().setJavaScriptEnabled(true);

        // Allow Zoom in/out controls
        myWebView.getSettings().setBuiltInZoomControls(true);
        myWebView.getSettings().setDomStorageEnabled(true);

        // Zoom out the best fit your screen
        myWebView.getSettings().setLoadWithOverviewMode(true);
        myWebView.getSettings().setUseWideViewPort(true);

        myTitleTXT.setText(myHeaderTitleStr);
      //  startWebView(myWebUrlStr);

    }
}
