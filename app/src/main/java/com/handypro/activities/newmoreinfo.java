package com.handypro.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.handypro.Dialog.LoadingDialog;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.PaymentFareSummeryPojo;
import com.handypro.Pojo.acceptimage;
import com.handypro.Pojo.afterimage;
import com.handypro.Pojo.newimte;
import com.handypro.R;
import com.handypro.adapters.AcceptImageAdapter;
import com.handypro.adapters.AfterImageAdapter;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.activities.navigationMenu.paymentrequest;
import com.handypro.adapters.PaymentFareSummeryAdapter;
import com.handypro.adapters.whatsadapter;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.ConnectionDetector;
import com.handypro.utils.CurrencySymbolConverter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user127 on 16-02-2018.
 */

public class newmoreinfo extends AppCompatActivity {

    private static final float maxHeight = 1280.0f;
    private static final float maxWidth = 1280.0f;
    private static final int PICK_CAMERA_IMAGE = 1;
    private static final int PICK_GALLERY_IMAGE = 2;
    private float PreAmount = 0;
    ImageView imageView;
    List<beforeimage> beforemovieList = new ArrayList<>();
    List<newimte> signmovieList = new ArrayList<>();
    List<afterimage> aftermovieList = new ArrayList<>();
    int count = 1;
    MyGridView gridview;
    listhei listview, servicelistview;
    TextView done, afterphoto;
    LinearLayout beforename, beforephotolay, afterphotolay;
    SharedPreferences pref;
    ConnectionDetector cd;
    LoadingDialog dialog;
    LinearLayout bacceptcall, brejectcall;
    String jobstate = "", Job_id;
    Handler mHandler;
    String service;
    String jobinstruction, description, storewhatsincluded, saveresforward, currencyCode;
    TextView custom_text_view, jobtext, jobid, whatsimportant, tellabout, jobphoto, faredetails, beforejobphoto;
    List<acceptimage> movieList = new ArrayList<>();
    PaymentFareSummeryAdapter adapter;
    LinearLayout userphoto, paynow;
    TextView whats, beforephotomain;
    RecyclerView recyclerView, beforerecyclerView, afterrecycler_view;
    String before_status = "0";
    String approvalstatus = "";
    String phototype = "";
    LinearLayout backclick;
    ArrayList<String> categorynameinarray;
    ArrayList<String> whatsincludedinarray;
    LinearLayout capturesignaturelinear;
    RecyclerView caprecycler_view;
    TextView captext;
    Runnable dialogRunnable = new Runnable() {
        @Override
        public void run() {
            dialog = new LoadingDialog(newmoreinfo.this);
            dialog.setLoadingTitle(getResources().getString(R.string.loading));
            dialog.show();
        }
    };
    private SharedPreference mySession;
    private List<String> imageItems;
    private Boolean isInternetPresent = false;
    private File savedFileDestination;
    private File compressedImage;
    private Uri fileUri; // file url to store image/video
    private ProgressDialogcreated myDialog;
    private SQLiteDatabase dataBase;
    private boolean show_progress_status = false;
    private ArrayList<PaymentFareSummeryPojo> farelist, TempFareList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newmoreinfo);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        farelist = new ArrayList<PaymentFareSummeryPojo>();
        TempFareList = new ArrayList<PaymentFareSummeryPojo>();
        mySession = new SharedPreference(newmoreinfo.this);
        pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
        Job_id = pref.getString("jobid", "");
        mHandler = new Handler();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        cd = new ConnectionDetector(newmoreinfo.this);
        isInternetPresent = cd.isConnectingToInternet();

        categorynameinarray = new ArrayList<String>();
        whatsincludedinarray = new ArrayList<String>();

        backclick = (LinearLayout) findViewById(R.id.backclick);
        paynow = (LinearLayout) findViewById(R.id.paynow);


        captext = (TextView) findViewById(R.id.captext);
        capturesignaturelinear = (LinearLayout) findViewById(R.id.capturesignaturelinear);
        caprecycler_view = (RecyclerView) findViewById(R.id.caprecycler_view);

        captext.setPaintFlags(captext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        backclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        paynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("sendjobid", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("jobid", Job_id);
                editor.apply();
                editor.commit();
                Intent InfoIntent = new Intent(newmoreinfo.this, paymentrequest.class);
                startActivity(InfoIntent);
            }
        });

        beforephotolay = (LinearLayout) findViewById(R.id.beforephotolay);
        bacceptcall = (LinearLayout) findViewById(R.id.bacceptcall);
        brejectcall = (LinearLayout) findViewById(R.id.brejectcall);

        afterphotolay = (LinearLayout) findViewById(R.id.afterphotolay);
        beforerecyclerView = (RecyclerView) findViewById(R.id.beforerecycler_view);
        afterrecycler_view = (RecyclerView) findViewById(R.id.afterrecycler_view);


        bacceptcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approvalstatus = "1";
                phototype = "1";
                sendrequest(newmoreinfo.this, ServiceConstant.beforephotoapproval);
            }
        });

        brejectcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approvalstatus = "2";
                phototype = "1";
                sendrequest(newmoreinfo.this, ServiceConstant.beforephotoapproval);
            }
        });


        userphoto = (LinearLayout) findViewById(R.id.userphoto);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        faredetails = (TextView) findViewById(R.id.faredetails);
        faredetails.setPaintFlags(faredetails.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        LinearLayout whatsincluded = (LinearLayout) findViewById(R.id.whatsincluded);
        whats = (TextView) findViewById(R.id.whats);
        beforephotomain = (TextView) findViewById(R.id.beforephotomain);
        jobtext = (TextView) findViewById(R.id.jobtext);


        TextView afterphotomain = (TextView) findViewById(R.id.afterphotomain);
        afterphotomain.setPaintFlags(afterphotomain.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        beforephotomain.setPaintFlags(beforephotomain.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
      /*  if (storewhatsincluded.length() > 0) {
            whatsincluded.setVisibility(View.VISIBLE);
        } else {
            whatsincluded.setVisibility(View.GONE);
        }*/


        TextView Whatsincludedtext = (TextView) findViewById(R.id.Whatsincludedtext);
        Whatsincludedtext.setPaintFlags(Whatsincludedtext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        TextView jonspecialin = (TextView) findViewById(R.id.jonspecialin);
        jonspecialin.setPaintFlags(jonspecialin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        jobphoto = (TextView) findViewById(R.id.jobphoto);
        jobphoto.setPaintFlags(jobphoto.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        whatsimportant = (TextView) findViewById(R.id.whatsimportant);
        tellabout = (TextView) findViewById(R.id.tellabout);

        servicelistview = (listhei) findViewById(R.id.servicelistview);

        jobid = (TextView) findViewById(R.id.jobid);
        jobid.setText("Job Id : " + Job_id);

        afterphoto = (TextView) findViewById(R.id.afterphoto);
        done = (TextView) findViewById(R.id.done);
        beforename = (LinearLayout) findViewById(R.id.beforename);
        imageView = (ImageView) findViewById(R.id.imageView);
        gridview = (MyGridView) findViewById(R.id.gridview);
        listview = (listhei) findViewById(R.id.listview);


        custom_text_view = (TextView) findViewById(R.id.custom_text_view);


        if (isInternetPresent) {
            paymentPost(newmoreinfo.this, ServiceConstant.PAYMENT_URL);

        } else {

        }


    }

    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }


    //Remove this section if you don't want camera code (End)

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    //----------------------Post method for Payment Fare------------
    private void paymentPost(Context mContext, String url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("job_id", Job_id);

        mHandler.post(dialogRunnable);

        ServiceRequest mservicerequest = new ServiceRequest(mContext);
        mservicerequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("payment", response);

                String Str_status = "", Str_response = "", Str_jobDescription = "", Str_NeedPayment = "", Str_Currency = "", Str_BtnGroup = "";


                try {
                    JSONObject jobject = new JSONObject(response);
                    Str_status = jobject.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject object = jobject.getJSONObject("response");
                        JSONObject object2 = object.getJSONObject("job");
                        Str_jobDescription = object2.getString("job_summary");
                        Str_NeedPayment = object2.getString("need_payment");
                        Str_Currency = object2.getString("currency");


                        whatsimportant.setText("Instruction: " + object2.getString("instructions"));
                        tellabout.setText("Description: " + object2.getString("job_description"));


                        signmovieList.clear();
                        if (object2.has("signatures")) {

                            if (object2.isNull("signatures")) {

                            } else {
                                String signaturesjj = object2.getString("signatures");

                                JSONObject signaturesjjobj = new JSONObject(signaturesjj);
                                if (signaturesjjobj.has("changeorder")) {
                                    String changeorder = signaturesjjobj.getString("changeorder");
                                    if (changeorder.equals("") || changeorder.equals(" ")) {
                                    } else {
                                        newimte beforemovie = new newimte(/*ServiceConstant.Base_Url +*/ changeorder, "Change Order");
                                        signmovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("startjob")) {
                                    String startjob = signaturesjjobj.getString("startjob");
                                    if (startjob.equals("") || startjob.equals(" ")) {
                                    } else {
                                        newimte beforemovie = new newimte(/*ServiceConstant.Base_Url + */startjob, "Start Job");
                                        signmovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("completejob")) {
                                    String completejob = signaturesjjobj.getString("completejob");
                                    if (completejob.equals("") || completejob.equals(" ")) {
                                    } else {
                                        newimte beforemovie = new newimte(/*ServiceConstant.Base_Url + */completejob, "Complete Job");
                                        signmovieList.add(beforemovie);
                                    }
                                }

                                if (signaturesjjobj.has("estimateApproval")) {
                                    String estimateApproval = signaturesjjobj.getString("estimateApproval");
                                    if (estimateApproval.equals("") || estimateApproval.equals(" ")) {
                                    } else {
                                        newimte beforemovie = new newimte(/*ServiceConstant.Base_Url +*/ estimateApproval, "Estimate Approval");
                                        signmovieList.add(beforemovie);
                                    }
                                }


                                if (signmovieList.size() > 0) {
                                    capturesignaturelinear.setVisibility(View.VISIBLE);
                                    LinearLayoutManager layoutManager
                                            = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                    neww mAdapter = new neww(signmovieList, getApplicationContext());
                                    caprecycler_view.setLayoutManager(layoutManager);
                                    caprecycler_view.setAdapter(mAdapter);
                                } else {
                                    capturesignaturelinear.setVisibility(View.GONE);
                                    caprecycler_view.setVisibility(View.GONE);
                                }


                            }
                        }

                        JSONArray categoryinclude = object2.getJSONArray("category");

                        storewhatsincluded = "";
                        for (int z = 0; z < categoryinclude.length(); z++) {
                            JSONObject getarrayvaluex = categoryinclude.getJSONObject(z);
                            String name = "" + getarrayvaluex.getString("name");

                            if (getarrayvaluex.has("whatsincluded")) {
                                String whatsincluded = "" + getarrayvaluex.getString("whatsincluded");
                                //  storewhatsincluded += name + "-" + whatsincluded ;

                                categorynameinarray.add(name);
                                whatsincludedinarray.add(whatsincluded);
                                /*if(categoryinclude.length()==(z+1))
                                {

                                }
                                else
                                {
                                    storewhatsincluded += name + "-" + whatsincluded + "\n";
                                }*/

                            } else {
                               /* if(categoryinclude.length()==(z+1))
                                {
                                    storewhatsincluded += name  ;
                                }
                                else
                                {
                                    storewhatsincluded += name + "\n";
                                }*/
                                categorynameinarray.add(name);
                                whatsincludedinarray.add("");
                            }


                        }
                        whats.setText(storewhatsincluded);


                        // Currency currencycode = Currency.getInstance(getLocale(Str_Currency));

                        currencyCode = CurrencySymbolConverter.getCurrencySymbol(Str_Currency);

                        if (object2.has("before_status")) {
                            before_status = object2.getString("before_status");
                        } else {

                        }


                        if (object2.has("need_payment")) {
                            String need_payment = object2.getString("need_payment");

                            if (need_payment.equalsIgnoreCase("1")) {
                                paynow.setVisibility(View.VISIBLE);
                            } else {
                                paynow.setVisibility(View.GONE);
                            }


                        } else {
                            paynow.setVisibility(View.GONE);
                        }


                        JSONArray userbookimages = object2.getJSONArray("user_taskimages");
                        if (userbookimages.length() > 0) {
                            userphoto.setVisibility(View.VISIBLE);

                            faredetails.setVisibility(View.VISIBLE);
                            faredetails.setPaintFlags(faredetails.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


                            movieList.clear();
                            for (int b = 0; b < userbookimages.length(); b++) {
                                String value = "" + userbookimages.getString(b);
                                if (value.startsWith("http://") || value.startsWith("https://")) {
                                    value = value;
                                } else {
                                    value = ServiceConstant.Base_Url + value;
                                }
                                acceptimage movie = new acceptimage(value);
                                movieList.add(movie);
                            }

                            if (movieList.size() > 0) {
                                userphoto.setVisibility(View.VISIBLE);
                                LinearLayoutManager layoutManager
                                        = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                AcceptImageAdapter mAdapter = new AcceptImageAdapter(movieList, getApplicationContext());
                                recyclerView.setLayoutManager(layoutManager);
                                recyclerView.setAdapter(mAdapter);
                            } else {
                                userphoto.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                            }


                        } else {

                            userphoto.setVisibility(View.GONE);
                        }

                        if(object2.has("user_pre_amount")){
                            JSONArray user_pre_amount_obj = object2.getJSONArray("user_pre_amount");
                            for (int i = 0; i < user_pre_amount_obj.length() ; i++) {
                                if (user_pre_amount_obj.getJSONObject(i).has("payment_status")
                                        && user_pre_amount_obj.getJSONObject(i).has("pre_amount")
                                        && user_pre_amount_obj.getJSONObject(i).getString("payment_status").equals("Accepted")){
                                    PreAmount = PreAmount + Float.parseFloat(user_pre_amount_obj.getJSONObject(i).getString("pre_amount"));
                                }

                            }
                        }

                        JSONArray jarry = object2.getJSONArray("billing");

                        JSONArray before_taskimages = object2.getJSONArray("before_taskimages");


                        beforemovieList.clear();
                        for (int b = 0; b < before_taskimages.length(); b++) {
                            String value = "" + before_taskimages.getString(b);

                            if (value.startsWith("http://") || value.startsWith("https://")) {
                                value = value;
                            } else {
                                value = ServiceConstant.Base_Url + value;
                            }
                            beforeimage beforemovie = new beforeimage(value);
                            beforemovieList.add(beforemovie);
                        }


                        if (before_taskimages.length() > 0) {
                            if (beforemovieList.size() > 0) {
                                LinearLayoutManager layoutManager
                                        = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                beforeimageadapter mAdapter = new beforeimageadapter(beforemovieList, getApplicationContext());
                                beforerecyclerView.setLayoutManager(layoutManager);
                                beforerecyclerView.setAdapter(mAdapter);
                            } else {
                                beforerecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            beforerecyclerView.setVisibility(View.GONE);
                        }


                        JSONArray after_taskimages = object2.getJSONArray("after_taskimages");

                        if (after_taskimages.length() > 0) {
                            afterphotolay.setVisibility(View.VISIBLE);

                            aftermovieList.clear();
                            for (int b = 0; b < after_taskimages.length(); b++) {
                                String value = "" + after_taskimages.getString(b);

                                if (value.startsWith("http://") || value.startsWith("https://")) {
                                    value = value;
                                } else {
                                    value = ServiceConstant.Base_Url + value;
                                }
                                afterimage beforemovie = new afterimage(value);
                                aftermovieList.add(beforemovie);
                            }


                            if (aftermovieList.size() > 0) {

                                LinearLayoutManager layoutManager
                                        = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                                AfterImageAdapter mAdapter = new AfterImageAdapter(aftermovieList, getApplicationContext());
                                afterrecycler_view.setLayoutManager(layoutManager);
                                afterrecycler_view.setAdapter(mAdapter);
                            } else {

                                afterrecycler_view.setVisibility(View.GONE);
                            }


                        } else {
                            afterphotolay.setVisibility(View.GONE);
                        }


                        if (jarry.length() > 0) {

                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjects_amount = jarry.getJSONObject(i);
                                PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();

                                String title = jobjects_amount.getString("title");
                                pojo.setPayment_title(jobjects_amount.getString("title"));

                                if (title.contains("Hours") || title.contains("Payment mode") || title.contains("Task type") || title.contains("Start Date") || title.contains("Time") || title.contains("Month") || title.contains("End Date")) {
                                    pojo.setPayment_amount(jobjects_amount.getString("amount"));
                                } else {
                                    pojo.setPayment_amount(currencyCode + jobjects_amount.getString("amount"));
                                }

                                if (jobjects_amount.has("description")) {
                                    if (jobjects_amount.getString("description").equals("") || jobjects_amount.getString("description").equals(" ")) {
                                        pojo.setPaymentdesc("");
                                    } else {
                                        pojo.setPaymentdesc("Description:" + jobjects_amount.getString("description"));
                                    }

                                } else {
                                    pojo.setPaymentdesc("");
                                }


                                /*pojo.setPayment_title(jobjects_amount.getString("title"));
                                pojo.setPayment_amount(currencyCode+jobjects_amount.getString("amount"));*/

                                farelist.add(pojo);
                                TempFareList.add(pojo);
                            }
                            show_progress_status = true;
                        } else {
                            show_progress_status = false;
                        }

                        if (before_taskimages.length() > 0) {
                            beforephotolay.setVisibility(View.VISIBLE);
                            mainjobmethod();
                        } else {
                            beforephotolay.setVisibility(View.GONE);
                        }


                    } else {
                        Str_response = jobject.getString("response");
                    }

                    System.out.println("payment1---------------------------");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
                if (Str_status.equalsIgnoreCase("1")) {
                    listview.setEnabled(false);
                    /*if (PreAmount != 0 && PreAmount > 0){
                        PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title("Payments Received");
                        pojo.setPayment_amount(currencyCode + String.valueOf(PreAmount));
                        TempFareList.set((TempFareList.size()-1), pojo);
                        pojo = new PaymentFareSummeryPojo();
                        pojo.setPayment_title(farelist.get(farelist.size()-1).getPayment_title());
                        pojo.setPayment_amount(farelist.get(farelist.size()-1).getPayment_amount());
                        TempFareList.add(pojo);
                        adapter = new PaymentFareSummeryAdapter(newmoreinfo.this, TempFareList);
                    }else if (PreAmount == 0){
                        adapter = new PaymentFareSummeryAdapter(newmoreinfo.this, farelist);
                    }*/

                    PaymentFareSummeryPojo pojo = new PaymentFareSummeryPojo();
                    pojo.setPayment_title("Payments Received");
                    pojo.setPayment_amount(currencyCode + String.valueOf(PreAmount));
                    TempFareList.set((TempFareList.size()-1), pojo);
                    pojo = new PaymentFareSummeryPojo();
                    pojo.setPayment_title(farelist.get(farelist.size()-1).getPayment_title());
                    pojo.setPayment_amount(farelist.get(farelist.size()-1).getPayment_amount());
                    TempFareList.add(pojo);
                    adapter = new PaymentFareSummeryAdapter(newmoreinfo.this, TempFareList);
                    listview.setAdapter(adapter);


                    servicelistview.setEnabled(false);
                    whatsadapter adapter = new whatsadapter(newmoreinfo.this, categorynameinarray, whatsincludedinarray);
                    servicelistview.setAdapter(adapter);


                } else {
                    //  Alert(getResources().getString(R.string.server_lable_header), Str_response);
                }


            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void mainjobmethod() {

        if (before_status.equalsIgnoreCase("0") || before_status.equalsIgnoreCase("")) {
            afterphoto.setText(getResources().getString(R.string.conformjo));
            jobtext.setText(getResources().getString(R.string.class_orderdetail_activity_job_conform));
            bacceptcall.setVisibility(View.VISIBLE);
            brejectcall.setVisibility(View.VISIBLE);
        } else if (before_status.equalsIgnoreCase("1")) {
            afterphoto.setText(getResources().getString(R.string.jobdetails));
            jobtext.setText(getResources().getString(R.string.class_orderdetail_activity_job_request));
            bacceptcall.setVisibility(View.GONE);
            brejectcall.setVisibility(View.GONE);
        } else if (before_status.equalsIgnoreCase("2")) {
            afterphoto.setText(getResources().getString(R.string.jobdetails));
            jobtext.setText(getResources().getString(R.string.class_orderdetail_activity_job_rejected));
            bacceptcall.setVisibility(View.VISIBLE);
            brejectcall.setVisibility(View.GONE);
        }
    }

    private void sendrequest(final Context aContext, String url) {


        myDialog = new ProgressDialogcreated(newmoreinfo.this);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", mySession.getUserDetails().getUserId());
        params.put("job_id", Job_id);
        params.put("status", "" + approvalstatus);
        params.put("position", "" + phototype);


        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------get Confirm Category list Response----------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    String message = object.getString("response");
                    if (object.getString("status").equalsIgnoreCase("1")) {


                        if (phototype.equalsIgnoreCase("1")) {

                            if (approvalstatus.equalsIgnoreCase("1")) {
                                before_status = "1";
                            } else if (approvalstatus.equalsIgnoreCase("2")) {
                                before_status = "2";
                            } else {
                                before_status = "0";
                            }

                        }


                        mainjobmethod();


                        final PkDialog mDialog = new PkDialog(newmoreinfo.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();

                    } else {
                        final PkDialog mDialog = new PkDialog(newmoreinfo.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.action_sorry));
                        mDialog.setDialogMessage(message);
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        mDialog.dismiss();
                                    }
                                }
                        );
                        mDialog.show();
                    }
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }


            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    public class neww extends RecyclerView.Adapter<neww.MyViewHolder> {

        Context ctxx;
        private List<newimte> moviesList;

        public neww(List<newimte> moviesList, Context ctx) {
            this.moviesList = moviesList;
            this.ctxx = ctx;
        }

        @Override
        public neww.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.imte, parent, false);

            return new neww.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(neww.MyViewHolder holder, int position) {
            final newimte movie = moviesList.get(position);

            holder.textme.setText(movie.getTextname());
            if (movie.getTitle().isEmpty()) {
                holder.image.setImageResource(R.drawable.noimageavailable);
            } else {
                Picasso.get()
                        .load(movie.getTitle())
                        .placeholder(R.drawable.noimageavailable)   // optional
                        .error(R.drawable.noimageavailable)      // optional
                        // optional
                        .into(holder.image);
            }

            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (movie.getTitle().length() > 10) {
                        Intent in = new Intent(ctxx, zoomMainActivity.class);
                        in.putExtra("imagename", "" + movie.getTitle());
                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        ctxx.startActivity(in);

                    }

                }
            });


        }

        @Override
        public int getItemCount() {
            return moviesList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView image;
            TextView textme;

            public MyViewHolder(View view) {
                super(view);
                image = (ImageView) view.findViewById(R.id.image);
                textme = (TextView) view.findViewById(R.id.textme);
            }
        }
    }
}

