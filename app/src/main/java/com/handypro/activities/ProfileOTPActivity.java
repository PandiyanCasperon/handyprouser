package com.handypro.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;


import com.android.volley.Request;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.UserInfoPojo;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;
import com.handypro.utils.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by CAS63 on 2/22/2018.
 */

public class ProfileOTPActivity extends AppCompatActivity {
    private ConnectionDetector myConnectionManager;
    private SharedPreference mySession;
    private ProgressDialogcreated myDialog;
    private CustomEdittext myOTPET;
    private CustomButton mySubmitBTN;
    private RelativeLayout myBackLAY;
    private String myOTPSTR = "", myOtpStatusSTR = "", myCountryCodeSTR = "", myPhoneNoSTR = "", myUserIdSTR = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_otp_verification);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetInitialize();
        getIntentValues();
        clickListeners();
    }

    private void clickListeners() {
        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mySubmitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myOTPET.getText().toString().length() == 0) {
                    HNDHelper.showErrorAlert(ProfileOTPActivity.this, getResources().getString(R.string.otp_label_alert_otp));
                } else if (!myOTPSTR.equals(myOTPET.getText().toString())) {
                    HNDHelper.showErrorAlert(ProfileOTPActivity.this, getResources().getString(R.string.otp_label_alert_invalid));
                } else if (myConnectionManager.isConnectingToInternet()) {
                    postRequestEditPhoneNumber(ProfileOTPActivity.this, ServiceConstant.profile_edit_mobileNo_url);
                } else {
                    HNDHelper.showErrorAlert(ProfileOTPActivity.this, getResources().getString(R.string.nointernet_text));
                }
            }
        });

    }

    private void getIntentValues() {
        Intent intent = getIntent();
        myOTPSTR = intent.getStringExtra("Otp");
        myOtpStatusSTR = intent.getStringExtra("Otp_Status");
        myCountryCodeSTR = intent.getStringExtra("CountryCode");
        myPhoneNoSTR = intent.getStringExtra("Phone");
        myUserIdSTR = intent.getStringExtra("UserID");

        // if (myOtpStatusSTR.equalsIgnoreCase("development")) {
        myOTPET.setText(myOTPSTR);
//        } else {
//            myOTPET.setText("");
//        }
    }

    private void classAndWidgetInitialize() {
        myConnectionManager = new ConnectionDetector(ProfileOTPActivity.this);
        mySession = new SharedPreference(ProfileOTPActivity.this);

        myOTPET = (CustomEdittext) findViewById(R.id.activity_profile_otp_verification_entered_otp_ET);
        myBackLAY = (RelativeLayout) findViewById(R.id.activity_profile_otp_verification_LAY_back);
        mySubmitBTN = (CustomButton) findViewById(R.id.activity_profile_otp_verification_BTN_send);
    }


    /**
     * @param aContext
     * @param url
     */
    private void postRequestEditPhoneNumber(final Context aContext, String url) {
        myDialog = new ProgressDialogcreated(aContext);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", myUserIdSTR);
        jsonParams.put("country_code", myCountryCodeSTR);
        jsonParams.put("phone_number", myPhoneNoSTR);
        jsonParams.put("otp", myOTPSTR);

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Profile Otp reponse-------------------" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {
                        UserProfilePageActivity.updateMobileDialog(object.getString("country_code"), object.getString("phone_number"));
                        updatePhoneNumberinSession("+" + object.getString("country_code"), object.getString("phone_number"));

                        final PkDialog mDialog = new PkDialog(ProfileOTPActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                        mDialog.setDialogMessage(getResources().getString(R.string.activity_profile_otp_label_mobile_success));
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                        onBackPressed();
                                        finish();
                                    }
                                }
                        );
                        mDialog.show();

                    } else {
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(myOTPET.getWindowToken(), 0);

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    private void updatePhoneNumberinSession(String aCountryCode, String aPhNo) {
        UserInfoPojo aInfoPojo = new UserInfoPojo();

        aInfoPojo.setUserProfileImage(mySession.getUserDetails().getUserProfileImage());
        aInfoPojo.setUserId(mySession.getUserDetails().getUserId());
        aInfoPojo.setUserReferalCode(mySession.getUserDetails().getUserReferalCode());
        aInfoPojo.setUserCategoryId(mySession.getUserDetails().getUserCategoryId());
        aInfoPojo.setUserCurrencyCode(mySession.getUserDetails().getUserCurrencyCode());
        aInfoPojo.setSocketKey(mySession.getUserDetails().getSocketKey());
        aInfoPojo.setUserWalletAmount(mySession.getUserDetails().getUserWalletAmount());
        aInfoPojo.setUserPhoneNumber(aPhNo);
        aInfoPojo.setUserCountryCode(aCountryCode);
        aInfoPojo.setUserEmail(mySession.getUserDetails().getUserEmail());
        aInfoPojo.setUserName(mySession.getUserDetails().getUserName());

        mySession.putUserDetails(aInfoPojo);

    }
}
