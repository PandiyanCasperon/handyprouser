package com.handypro.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.RatingPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.Widgets.RoundedImageView;
import com.handypro.adapters.RatingAdapter;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.HAppController;
import com.handypro.volley.ServiceRequest;
import com.handypro.volley.VolleyMultipartRequest;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Casperon Technology on 1/23/2016.
 */
public class RatingPageActivity extends AppCompatActivity {
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SharedPreference mySession;

    private RelativeLayout Rl_skip;
    private LinearLayout Rl_submit;
    private EditText Et_comment;
    private ImageView image_upload;
    private TextView TextViewRatingBasedText, TextViewCallButton, TextViewORText;
    String ratinggiven = "0.0";


    Dialog dialog;
    private ServiceRequest mRequest;
    private ProgressDialogcreated myDialog;

    final int PERMISSION_REQUEST_CODE = 111;
    final int CALL_PERMISSION_REQUEST_CODE = 123;

    private Dialog photo_dialog;
    private int REQUEST_TAKE_PHOTO = 1;
    private int galleryRequestCode = 2;
    private Uri camera_FileUri;
    Bitmap bitMapThumbnail;
    private byte[] byteArray;
    private static final String IMAGE_DIRECTORY_NAME = "QuickRabbit";
    private static final String TAG = "";

    private String mSelectedFilePath = "";
    ArrayList<RatingPojo> itemList;
    RatingAdapter adapter;
    RatingBar rating_single_ratingBar;
    private ExpandableHeightListView listView;
    private String sJobId_intent = "";
    private String UserID = "", User_Image;
    private boolean isDataAvailable = false;
    private TextView myTaskerTXT;
    private ImageView Iv_ratingAddImage;
    private RoundedImageView Img_ratingupload;
    private String partner_image = "";
    private String partner_name = "", code = "", number = "", ButtonGoogleURL, ButtonFaceBookURL, ButtonAngleURL;
    private LinearLayout LinearLayoutSocialButtonGroup;
    private Button ButtonGoogle, ButtonFaceBook, ButtonAngle;
    TextView ss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rating_page);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mySession = new SharedPreference(RatingPageActivity.this);
        initialize();


        Iv_ratingAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission()) {
                        requestPermission();
                    } else {
                        chooseImage();
                    }
                } else {
                    chooseImage();
                }
            }
        });


        Rl_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!ratinggiven.equals("0.0")) {

                    if (isInternetPresent) {

                        System.out.println("------------job_id-------------" + sJobId_intent);
                        System.out.println("------------comments-------------" + Et_comment.getText().toString());
                        System.out.println("------------ratingsFor-------------" + "tasker");
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("comments", Et_comment.getText().toString());
                        jsonParams.put("ratingsFor", "user");
                        jsonParams.put("job_id", sJobId_intent);
                        jsonParams.put("ratings[" + 0 + "][option_id]", "");
                        jsonParams.put("ratings[" + 0 + "][option_title]", "Good");
                        jsonParams.put("ratings[" + 0 + "][rating]", ratinggiven);
                        System.out.println("------------jsonParams-------------" + jsonParams);


                        if (byteArray != null) {
                            uploadUserImage(ServiceConstant.rating_submit_url, jsonParams);

                        } else {
                            postRequest_SubmitRating(ServiceConstant.rating_submit_url, jsonParams);

                        }
                    } else {
                        alert(getResources().getString(R.string.action_no_internet_title), getResources().getString(R.string.nointernet_text));
                    }
                } else {
                    alert(getResources().getString(R.string.rating_header_comment_textView), getResources().getString(R.string.rating_header_enter_all));
                }
            }
        });

    }

    private void initialize() {

        cd = new ConnectionDetector(RatingPageActivity.this);
        isInternetPresent = cd.isConnectingToInternet();

        itemList = new ArrayList<RatingPojo>();

        ss = findViewById(R.id.ss);
        image_upload = findViewById(R.id.image_upload);
        Rl_skip = findViewById(R.id.rating_header_skip_layout);
        listView = findViewById(R.id.rating_listView);
        TextViewRatingBasedText = findViewById(R.id.TextViewRatingBasedText);
        TextViewCallButton = findViewById(R.id.TextViewCallButton);
        TextViewORText = findViewById(R.id.TextViewORText);
        rating_single_ratingBar = findViewById(R.id.rating_single_ratingBar);
        ButtonGoogle = findViewById(R.id.ButtonGoogle);
        ButtonFaceBook = findViewById(R.id.ButtonFaceBook);
        ButtonAngle = findViewById(R.id.ButtonAngle);


        rating_single_ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if (rating <= 3) {
                    Et_comment.setVisibility(View.VISIBLE);
                    TextViewRatingBasedText.setVisibility(View.VISIBLE);
                    TextViewCallButton.setVisibility(View.VISIBLE);
                    TextViewORText.setVisibility(View.VISIBLE);
                    LinearLayoutSocialButtonGroup.setVisibility(View.GONE);
                    TextViewRatingBasedText.setText(getResources().getText(R.string.poor_review));
                } else {
                    Et_comment.setVisibility(View.VISIBLE);
                    TextViewRatingBasedText.setVisibility(View.VISIBLE);
                    TextViewCallButton.setVisibility(View.GONE);
                    TextViewORText.setVisibility(View.GONE);
                    LinearLayoutSocialButtonGroup.setVisibility(View.VISIBLE);
                    TextViewRatingBasedText.setText(getResources().getText(R.string.good_review));
                }

                ratinggiven = String.valueOf(rating);
            }
        });

        TextViewCallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ActivityCompat.requestPermissions(RatingPageActivity.this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, CALL_PERMISSION_REQUEST_CODE);
            }
        });

        Rl_submit = findViewById(R.id.rating_submit_layout);
        Et_comment = findViewById(R.id.rating_comment_editText);
        Iv_ratingAddImage = findViewById(R.id.rating_page_add_ImageView);
        Img_ratingupload = findViewById(R.id.ImageView_rating);
        myTaskerTXT = findViewById(R.id.rating_page_taskernameTXT);
        LinearLayoutSocialButtonGroup = findViewById(R.id.LinearLayoutSocialButtonGroup);


        SpannableStringBuilder builders = new SpannableStringBuilder();
        String red = "Rate Your ";
        SpannableString redSpannable = new SpannableString(red);
        redSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, red.length(), 0);
        builders.append(redSpannable);

        String white = "HANDY";
        SpannableString whiteSpannable = new SpannableString(white);
        whiteSpannable.setSpan(new ForegroundColorSpan(Color.BLUE), 0, white.length(), 0);
        builders.append(whiteSpannable);

        String blue = "PRO ";
        SpannableString blueSpannable = new SpannableString(blue);
        blueSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, blue.length(), 0);
        builders.append(blueSpannable);

        String Craftsman = "Craftsman";
        SpannableString blueSpannableCraftsman = new SpannableString(Craftsman);
        blueSpannableCraftsman.setSpan(new ForegroundColorSpan(Color.BLACK), 0, blue.length(), 0);
        builders.append(blueSpannableCraftsman);


        ss.setText(builders, TextView.BufferType.SPANNABLE);

        Rl_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        // get user data from session

        UserID = mySession.getUserDetails().getUserId();
        User_Image = mySession.getUserDetails().getUserProfileImage();


        Picasso.get().load(User_Image).error(R.drawable.placeholder_icon)
                .placeholder(R.drawable.placeholder_icon).memoryPolicy(MemoryPolicy.NO_CACHE).into(Img_ratingupload);
        Intent intent = getIntent();
        sJobId_intent = intent.getStringExtra("JobID");

        if (isInternetPresent) {
            postRequest_RatingList(ServiceConstant.rating_list_url);
            postRatingDetails();
        } else {
            alert(getResources().getString(R.string.action_no_internet_title), getResources().getString(R.string.nointernet_text));
        }

        ButtonGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(ButtonGoogleURL));
                startActivity(i);
            }
        });

        ButtonFaceBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(ButtonFaceBookURL));
                startActivity(i);
            }
        });

        ButtonAngle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(ButtonAngleURL));
                startActivity(i);
            }
        });

    }

    //--------------Alert Method-----------
    private void alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(RatingPageActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //-----------------------Rating List Post Request-----------------
    private void postRequest_RatingList(String Url) {

        myDialog = new ProgressDialogcreated(RatingPageActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        System.out.println("-------------Rating List Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("holder_type", "user");
        jsonParams.put("user", UserID);
        jsonParams.put("job_id", sJobId_intent);
        System.out.println("holder_type--------" + "user");

        mRequest = new ServiceRequest(RatingPageActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Rating List Response----------------" + response);

                String sStatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONArray payment_array = object.getJSONArray("review_options");
                        itemList.clear();
                        if (payment_array.length() > 0) {

                            for (int i = 0; i < payment_array.length(); i++) {
                                JSONObject reason_object = payment_array.getJSONObject(i);
                                RatingPojo pojo = new RatingPojo();
                                pojo.setRatingId(reason_object.getString("option_id"));
                                pojo.setRatingName(reason_object.getString("option_name"));
                                pojo.setRatingCount("");
                                partner_image = reason_object.getString("image");
                                partner_name = reason_object.getString("name");
                                itemList.add(pojo);
                            }
                            isDataAvailable = true;

                            Picasso.get().load(String.valueOf(partner_image)).placeholder(R.drawable.nouserimg).into(Img_ratingupload);
                        } else {
                            isDataAvailable = false;
                        }
                    }


                    if (sStatus.equalsIgnoreCase("1") && isDataAvailable) {
                        adapter = new RatingAdapter(RatingPageActivity.this, itemList);
                        listView.setAdapter(adapter);
                        listView.setExpanded(true);
                    }

                    if (sStatus.equalsIgnoreCase("1")) {
                        myTaskerTXT.setText(partner_name);
                    }

                } catch (JSONException e) {
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void postRatingDetails() {
        System.out.println("-------------Rating List Url----------------" + ServiceConstant.GET_RATING_DETAILS);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("holder_type", "user");
        jsonParams.put("user", UserID);
        jsonParams.put("Job_id", sJobId_intent);
        System.out.println("holder_type--------" + "user");

        mRequest = new ServiceRequest(RatingPageActivity.this);
        mRequest.makeServiceRequest(ServiceConstant.GET_RATING_DETAILS, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Rating List Response----------------" + response);

                String sStatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        code = object.getJSONObject("result").getJSONObject("phone").getString("code");
                        number = object.getJSONObject("result").getJSONObject("phone").getString("number");

                        if (object.getJSONObject("result").getJSONObject("rating_settings").getJSONObject("google").getString("status").equals("1")) {
                            ButtonGoogle.setVisibility(View.VISIBLE);
                            ButtonGoogle.setText(object.getJSONObject("result").getJSONObject("rating_settings").getJSONObject("google").getString("name"));
                            ButtonGoogleURL = object.getJSONObject("result").getJSONObject("rating_settings").getJSONObject("google").getString("url");
                        }
                        if (object.getJSONObject("result").getJSONObject("rating_settings").getJSONObject("facebook").getString("status").equals("1")) {
                            ButtonFaceBook.setVisibility(View.VISIBLE);
                            ButtonFaceBook.setText(object.getJSONObject("result").getJSONObject("rating_settings").getJSONObject("facebook").getString("name"));
                            ButtonFaceBookURL = object.getJSONObject("result").getJSONObject("rating_settings").getJSONObject("facebook").getString("url");
                        }

                        if (object.getJSONObject("result").getJSONObject("rating_settings").getJSONObject("angleslist").getString("status").equals("1")) {
                            ButtonAngle.setVisibility(View.VISIBLE);
                            ButtonAngle.setText(object.getJSONObject("result").getJSONObject("rating_settings").getJSONObject("angleslist").getString("name"));
                            ButtonAngleURL = object.getJSONObject("result").getJSONObject("rating_settings").getJSONObject("angleslist").getString("url");
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                Log.e("Error", "Some Error from response");
            }
        });
    }


    //-----------------------Submit Rating Post Request-----------------
    private void postRequest_SubmitRating(String Url, final HashMap<String, String> jsonParams) {
        myDialog = new ProgressDialogcreated(RatingPageActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        System.out.println("-------------Submit Rating Url----------------" + Url);

        mRequest = new ServiceRequest(RatingPageActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("------------Submit Rating Response----------------" + response);

                String sStatus = "";
                try {
                    JSONObject object = new JSONObject(response);


                    if (object.has("status")) {
                        sStatus = object.getString("status");
                        if (sStatus.equalsIgnoreCase("1")) {

                            final PkDialog mDialog = new PkDialog(RatingPageActivity.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.action_success));
                            mDialog.setDialogMessage(getResources().getString(R.string.rating_submit_successfully));
                            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();

                                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("EXIT", true);
                                    startActivity(intent);
                                    finish();
                                    //onBackPressed();
                                    //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                            mDialog.show();
                        } else {
                            String sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_sorry), sResponse);
                        }
                    } else {
                        final PkDialog mDialog = new PkDialog(RatingPageActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.action_success));
                        mDialog.setDialogMessage(getResources().getString(R.string.rating_submit_successfully));
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();

                                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("EXIT", true);
                                startActivity(intent);
                                finish();
                                //onBackPressed();
                                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }
                        });
                        mDialog.show();
                    }

                } catch (JSONException e) {
                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }


//-----------------rate img-------------


    private void uploadUserImage(String url, final HashMap<String, String> jsonParams) {

        dialog = new Dialog(RatingPageActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.class_ratingpage_loading));
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                System.out.println("------------- image response-----------------" + response.data);
                String resultResponse = new String(response.data);
                System.out.println("-------------  response-----------------" + resultResponse);
                String sStatus = "", sResponse = "", SUser_image = "", Smsg = "";
                try {
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    sStatus = jsonObject.getString("status");
                    sResponse = jsonObject.getString("response");

                    if (sStatus.equalsIgnoreCase("1")) {

                        final PkDialog mDialog = new PkDialog(RatingPageActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.action_success));
                        mDialog.setDialogMessage(getResources().getString(R.string.rating_submit_successfully));
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("EXIT", true);
                                startActivity(intent);
                                finish();
                                //onBackPressed();
                                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }
                        });
                        mDialog.show();
                    } else {
                        //String sResponse = object.getString("response");
                        alert(getResources().getString(R.string.action_sorry), sResponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return jsonParams;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();


                params.put("file", new VolleyMultipartRequest.DataPart("maidac.jpg", byteArray));

                System.out.println("photo--------edit------" + byteArray);

                return params;
            }
        };

        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(retryPolicy);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        multipartRequest.setShouldCache(false);
        HAppController.getInstance().addToRequestQueue(multipartRequest);

    }


    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chooseImage();
                } else {
                    finish();
                }
                break;

            case CALL_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!code.isEmpty() && !number.isEmpty()) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + code + number));
                        if (ActivityCompat.checkSelfPermission(RatingPageActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(callIntent);
                    }
                }
                break;
        }
    }


    // --------------------Method for choose image--------------------
    private void chooseImage() {
        photo_dialog = new Dialog(RatingPageActivity.this);
        photo_dialog.getWindow();
        photo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        photo_dialog.setContentView(R.layout.photo_picker_dialog);
        photo_dialog.setCanceledOnTouchOutside(true);

        photo_dialog.show();
        photo_dialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout camera = photo_dialog
                .findViewById(R.id.photo_picker_camera_layout);
        RelativeLayout gallery = photo_dialog
                .findViewById(R.id.photo_picker_gallery_layout);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
                photo_dialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                photo_dialog.dismiss();
            }
        });

    }


    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        camera_FileUri = getOutputMediaFileUri(1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, camera_FileUri);
        // start the image capture Intent
        startActivityForResult(intent, REQUEST_TAKE_PHOTO);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, galleryRequestCode);
    }


    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "eOops! Failed creat "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", camera_FileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        camera_FileUri = savedInstanceState.getParcelable("file_uri");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;

                    final Bitmap bitmap = BitmapFactory.decodeFile(camera_FileUri.getPath(), options);
                    Bitmap thumbnail = bitmap;
                    final String picturePath = camera_FileUri.getPath();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                    File curFile = new File(picturePath);
                    try {
                        ExifInterface exif = new ExifInterface(curFile.getPath());
                        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                        int rotationInDegrees = exifToDegrees(rotation);

                        Matrix matrix = new Matrix();
                        if (rotation != 0f) {
                            matrix.preRotate(rotationInDegrees);
                        }
                        thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
                    } catch (IOException ex) {
                        Log.e("TAG", "Failed to get Exif data", ex);
                    }
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    byteArray = byteArrayOutputStream.toByteArray();

                    //------------Code to update----------
                    bitMapThumbnail = thumbnail;
                    image_upload.setImageBitmap(thumbnail);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == galleryRequestCode) {

                Uri selectedImage = data.getData();
                if (selectedImage.toString().startsWith("content://com.sec.android.gallery3d.provider")) {
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    final String picturePath = c.getString(columnIndex);
                    c.close();

                    Picasso.get().load(picturePath).resize(100, 100).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            Bitmap thumbnail = bitmap;
                            mSelectedFilePath = picturePath;
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                            byteArray = byteArrayOutputStream.toByteArray();

                            //------------Code to update----------
                            bitMapThumbnail = thumbnail;
                            image_upload.setImageBitmap(thumbnail);
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                        }
                    });
                } else {
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();

                    int columnIndex = c.getColumnIndex(filePath[0]);
                    final String picturePath = c.getString(columnIndex);
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                    Bitmap thumbnail = bitmap; //getResizedBitmap(bitmap, 600);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    File curFile = new File(picturePath);

                    try {
                        ExifInterface exif = new ExifInterface(curFile.getPath());
                        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                        int rotationInDegrees = exifToDegrees(rotation);

                        Matrix matrix = new Matrix();
                        if (rotation != 0f) {
                            matrix.preRotate(rotationInDegrees);
                        }
                        thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
                    } catch (IOException ex) {
                        Log.e("TAG", "Failed to get Exif data", ex);
                    }
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    byteArray = byteArrayOutputStream.toByteArray();
                    c.close();

                    bitMapThumbnail = thumbnail;
                    image_upload.setImageBitmap(thumbnail);
                }
            }
        }
    }


    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    //-----------------Move Back on pressed phone back button------------------
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {


            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            return true;
        }
        return false;
    }
}
