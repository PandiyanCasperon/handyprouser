package com.handypro.activities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import com.android.volley.Request;
import com.handypro.Dialog.PkDialog;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.R;
import com.handypro.volley.ServiceRequest;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomButton;
import com.handypro.textview.CustomEdittext;
import com.handypro.utils.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by CAS63 on 2/21/2018.
 */

public class ChangePasswordActivity extends AppCompatActivity {
    private CustomEdittext myOldPasswordET, myNewPasswordET, myConfirmPasswordET;
    private CustomButton myChangPwdSubmitBTN;
    private ConnectionDetector myConnectionManager;
    private SharedPreference mySession;
    private ProgressDialogcreated myDialog;
    RelativeLayout activity_change_password_LAY_bsck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        classAndWidgetsInit();
        onClick();
    }

    private void classAndWidgetsInit() {
        myConnectionManager = new ConnectionDetector(ChangePasswordActivity.this);
        mySession = new SharedPreference(ChangePasswordActivity.this);
        activity_change_password_LAY_bsck= (RelativeLayout) findViewById(R.id.activity_change_password_LAY_bsck);
        myOldPasswordET = (CustomEdittext) findViewById(R.id.activity_change_password_ET_old_pwd);
        myNewPasswordET = (CustomEdittext) findViewById(R.id.activity_change_password_ET_new_pwd);
        myConfirmPasswordET = (CustomEdittext) findViewById(R.id.activity_change_password_ET_confirm_pwd);
        myChangPwdSubmitBTN = (CustomButton) findViewById(R.id.activity_change_password_BTN_submit);

        activity_change_password_LAY_bsck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void onClick() {
        myChangPwdSubmitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getEditTextValue(myOldPasswordET).length() == 0) {
                    myOldPasswordET.requestFocus();
                    HNDHelper.showErrorAlert(ChangePasswordActivity.this, getResources().getString(R.string.activity_change_password_error_empty_old_password));
                } else if (getEditTextValue(myNewPasswordET).length() == 0) {
                    myNewPasswordET.requestFocus();
                    HNDHelper.showErrorAlert(ChangePasswordActivity.this, getResources().getString(R.string.activity_change_password_error_empty_new_password));
                } else if (getEditTextValue(myConfirmPasswordET).length() == 0) {
                    myConfirmPasswordET.requestFocus();
                    HNDHelper.showErrorAlert(ChangePasswordActivity.this, getResources().getString(R.string.activity_change_password_error_empty_confirm_password));
                } else if (!isValidPassword(getEditTextValue(myNewPasswordET))) {
                    HNDHelper.showErrorAlert(ChangePasswordActivity.this, getResources().getString(R.string.activity_change_password_error_not_valid_password));
                } else if (!isValidPassword(getEditTextValue(myConfirmPasswordET))) {
                    HNDHelper.showErrorAlert(ChangePasswordActivity.this, getResources().getString(R.string.activity_change_password_error_not_valid_password));
                } else if (!getEditTextValue(myNewPasswordET).equals(getEditTextValue(myConfirmPasswordET))) {
                    HNDHelper.showErrorAlert(ChangePasswordActivity.this, getResources().getString(R.string.activity_change_password_error_password_not_match));
                } else {
                    submitValues();
                }
            }
        });

    }

    private void submitValues() {
        if (myConnectionManager.isConnectingToInternet()) {
            postRequestChangePassword(ChangePasswordActivity.this, ServiceConstant.changePassword_url);

        } else {
            HNDHelper.showErrorAlert(ChangePasswordActivity.this, getResources().getString(R.string.nointernet_text));
        }
    }


    /**
     * Change Password Post Request
     *
     * @param aContext
     * @param url
     */
    private void postRequestChangePassword(final Context aContext, String url) {
        myDialog = new ProgressDialogcreated(aContext);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("password", myOldPasswordET.getText().toString());
        jsonParams.put("new_password", myNewPasswordET.getText().toString());

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------change password Response----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {

                        final PkDialog mDialog = new PkDialog(ChangePasswordActivity.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.success_label));
                        mDialog.setDialogMessage(getResources().getString(R.string.activity_change_password_success_alert_content));
                        mDialog.setPositiveButton(
                                getResources().getString(R.string.dialog_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                        onBackPressed();
                                        finish();



                                    }
                                }
                        );
                        mDialog.show();

                    } else {
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });

    }

    /**
     * Get the Edit text values
     *
     * @param aEditText
     * @return
     */
    private String getEditTextValue(EditText aEditText) {
        return aEditText.getText().toString().trim();
    }


    /**
     * validating password with retype password
     *
     * @param pass
     * @return
     */
    private boolean isValidPassword(String pass) {
        if (pass.length() < 6) {
            return false;
        }
            /*
             * else if(!pass.matches("(.*[A-Z].*)")) { return false; }
			 */
        else if (!pass.matches("(.*[a-z].*)")) {
            return false;
        } else if (!pass.matches("(.*[0-9].*)")) {
            return false;
        }
            /*
             * else if(!pass.matches(
			 * "(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)")) {
			 * return false; }
			 */
        else {
            return true;
        }

    }

}
