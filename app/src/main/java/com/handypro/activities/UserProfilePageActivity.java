package com.handypro.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.handypro.Iconstant.ServiceConstant;
import com.handypro.Pojo.UserInfoPojo;
import com.handypro.R;
import com.handypro.Widgets.ProgressDialogcreated;
import com.handypro.commonvalues.HNDHelper;
import com.handypro.sharedpreference.SharedPreference;
import com.handypro.textview.CustomEdittext;
import com.handypro.textview.CustomTextView;
import com.handypro.utils.ConnectionDetector;
import com.handypro.volley.HAppController;
import com.handypro.volley.ServiceRequest;
import com.handypro.volley.VolleyMultipartRequest;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by CAS63 on 2/21/2018.
 */

public class UserProfilePageActivity extends AppCompatActivity {
    private static final String TAG = "";
    private static CustomTextView myPasswordTXT, myCountryCodeTXT, myChangePwdTXT, myChangeMblNoTXT;
    private static CustomEdittext myPhoneNumberET, activity_user_profile_user_first_name_ET, activity_user_profile_user_last_name_ET, activity_user_profile_user_name_ET;
    private static String IMAGE_DIRECTORY_NAME = "";
    final int PERMISSION_REQUEST_CODE = 111;
    private RelativeLayout myBackLAY;
    private EditText myEmailTXT;
    private TextView TextViewChangeEmail;
    private ConnectionDetector myConnectionManager;
    private SharedPreference mySession;
    private CircleImageView myUserProImgVw;
    private int GALLERY_REQUEST_CODE = 1;
    private int CAMERA_REQUEST_CODE = 1021;
    private Uri camera_FileUri;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private Bitmap selectedBitmap;
    private byte[] byteArray;
    private Dialog myAddPhotoDialog;
    private File myImageRoot;
    private CountryPicker picker;
    private ProgressDialogcreated myDialog;

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "eOops! Failed creat "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + System.currentTimeMillis() + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 6) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    //--------------Update Mobile Number From Profile OTP Page Method-----------
    public static void updateMobileDialog(String code, String phone) {
        myPhoneNumberET.setText(phone);
        myCountryCodeTXT.setText(code.replace("+", ""));
    }

    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        myDialog = new ProgressDialogcreated(UserProfilePageActivity.this);
        classAndWidgetsInit();
        getUserData();
        createDirectory();
        initCountryPicker();
    }

    private void createDirectory() {
        myImageRoot = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
        if (!myImageRoot.exists()) {
            myImageRoot.mkdir();
        } else if (!myImageRoot.isDirectory()) {
            myImageRoot.delete();
            myImageRoot.mkdir();
        }

    }

    private void initCountryPicker() {
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                myCountryCodeTXT.setText(dialCode);
                if (myCountryCodeTXT.getText().length() > 0) {
                    myPhoneNumberET.setError(null);
                }
                myPhoneNumberET.requestFocus();

                // close keyboard
                InputMethodManager mgr_username = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr_username.hideSoftInputFromWindow(myPhoneNumberET.getWindowToken(), 0);
            }
        });
    }

    private void getUserData() {
        if (myConnectionManager.isConnectingToInternet()) {
            postUserProfileRequest(UserProfilePageActivity.this, ServiceConstant.User_profile_Url);
        } else {
            HNDHelper.showErrorAlert(UserProfilePageActivity.this, getResources().getString(R.string.nointernet_text));
        }
    }

    private void classAndWidgetsInit() {
        mySession = new SharedPreference(UserProfilePageActivity.this);
        myConnectionManager = new ConnectionDetector(UserProfilePageActivity.this);
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_label));
        IMAGE_DIRECTORY_NAME = getResources().getString(R.string.app_name);


        myBackLAY = findViewById(R.id.activity_user_profile_LAY_back);
        myEmailTXT = findViewById(R.id.activity_user_profile_email_textview);
        myPasswordTXT = findViewById(R.id.activity_user_profile_password_textview);
        activity_user_profile_user_first_name_ET = findViewById(R.id.activity_user_profile_user_first_name_ET);
        activity_user_profile_user_last_name_ET = findViewById(R.id.activity_user_profile_user_last_name_ET);
        activity_user_profile_user_name_ET = findViewById(R.id.activity_user_profile_user_name_ET);
        myCountryCodeTXT = findViewById(R.id.activity_user_profile_phone_code_textview);
        myPhoneNumberET = findViewById(R.id.activity_user_profile_phone_ET);
        myUserProImgVw = findViewById(R.id.activity_user_profile_imageView);
        myChangePwdTXT = findViewById(R.id.activity_user_profile_change_password_textview);
        myChangeMblNoTXT = findViewById(R.id.activity_user_profile_change_number_textview);
        TextViewChangeEmail = findViewById(R.id.TextViewChangeEmail);
        activity_user_profile_user_first_name_ET.setSelection(activity_user_profile_user_first_name_ET.getText().toString().trim().length());
        activity_user_profile_user_last_name_ET.setSelection(activity_user_profile_user_last_name_ET.getText().toString().trim().length());
        activity_user_profile_user_name_ET.setSelection(activity_user_profile_user_name_ET.getText().toString().trim().length());
        myPhoneNumberET.setCursorVisible(false);

        myBackLAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextViewChangeEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.show();
                HashMap<String, String> params = new HashMap<>();

                if (EditTextValidationMail(myEmailTXT.getText().toString().trim())) {
                    params.put("email_id", myEmailTXT.getText().toString().trim());
                    params.put("user_id", mySession.getUserDetails().getUserId());
                    ServiceRequest mRequest = new ServiceRequest(UserProfilePageActivity.this);
                    mRequest.makeServiceRequest(ServiceConstant.UPDATE_USER_EMAIL_URL, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                        @Override
                        public void onCompleteListener(String response) {
                            myDialog.dismiss();
                            Log.e("EmailUpdate", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.get("status").toString().equalsIgnoreCase("1")) {
                                    getUserData();
                                } else {
                                    HNDHelper.showResponseErrorAlert(UserProfilePageActivity.this, jsonObject.getJSONObject("response").toString());
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorListener() {
                            myDialog.dismiss();
                        }
                    });
                } else {
                    HNDHelper.showResponseErrorAlert(UserProfilePageActivity.this, "Please enter valid Email");
                }

            }
        });

        myChangeMblNoTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cd = new ConnectionDetector(UserProfilePageActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    postRequest_editMobileNumber(ServiceConstant.profile_edit_mobileNo_url);
                } else {
                    HNDHelper.showErrorAlert(UserProfilePageActivity.this, getResources().getString(R.string.nointernet_text));
                }

               /* myPhoneNumberET.setCursorVisible(true);
                myPhoneNumberET.setSelection(myPhoneNumberET.getText().length());
                myPhoneNumberET.requestFocus();

                InputMethodManager inputMethodManager =  (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.toggleSoftInputFromWindow(myPhoneNumberET.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);*/
            }
        });

        myChangePwdTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aChngPwdIntent = new Intent(UserProfilePageActivity.this, ChangePasswordActivity.class);
                startActivity(aChngPwdIntent);
            }
        });

        activity_user_profile_user_first_name_ET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == activity_user_profile_user_first_name_ET.getId()) {
                    activity_user_profile_user_first_name_ET.setCursorVisible(true);
                }
            }
        });

        activity_user_profile_user_first_name_ET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;

                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (myConnectionManager.isConnectingToInternet()) {
                        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        mgr.hideSoftInputFromWindow(activity_user_profile_user_first_name_ET.getWindowToken(), 0);

                        postRequestEditUserName(UserProfilePageActivity.this);
                    } else {
                        HNDHelper.showErrorAlert(UserProfilePageActivity.this, getResources().getString(R.string.nointernet_text));
                    }
                    handled = true;
                }
                return handled;
            }
        });

        activity_user_profile_user_last_name_ET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == activity_user_profile_user_last_name_ET.getId()) {
                    activity_user_profile_user_last_name_ET.setCursorVisible(true);
                }
            }
        });

        activity_user_profile_user_last_name_ET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;

                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (myConnectionManager.isConnectingToInternet()) {
                        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        mgr.hideSoftInputFromWindow(activity_user_profile_user_last_name_ET.getWindowToken(), 0);

                        postRequestEditUserName(UserProfilePageActivity.this);
                    } else {
                        HNDHelper.showErrorAlert(UserProfilePageActivity.this, getResources().getString(R.string.nointernet_text));
                    }
                    handled = true;
                }
                return handled;
            }
        });

        activity_user_profile_user_name_ET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == activity_user_profile_user_name_ET.getId()) {
                    activity_user_profile_user_name_ET.setCursorVisible(true);
                }
            }
        });

        activity_user_profile_user_name_ET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;

                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (myConnectionManager.isConnectingToInternet()) {
                        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        mgr.hideSoftInputFromWindow(activity_user_profile_user_name_ET.getWindowToken(), 0);

                        postRequestEditUserName(UserProfilePageActivity.this);
                    } else {
                        HNDHelper.showErrorAlert(UserProfilePageActivity.this, getResources().getString(R.string.nointernet_text));
                    }
                    handled = true;
                }
                return handled;
            }
        });

        myPhoneNumberET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == myPhoneNumberET.getId()) {
                    myPhoneNumberET.setCursorVisible(true);
                }
            }
        });

        myPhoneNumberET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;

                if (actionId == EditorInfo.IME_ACTION_SEND) {

                    if (!isValidPhoneNumber(myPhoneNumberET.getText().toString())) {
                        HNDHelper.showErrorAlert(UserProfilePageActivity.this, getResources().getString(R.string.activity_user_profile_label_error_mobile));
                    } else if (myCountryCodeTXT.getText().toString().length() == 0) {
                        HNDHelper.showErrorAlert(UserProfilePageActivity.this, getResources().getString(R.string.activity_user_profile_label_error_mobileCode));
                    } else if (myConnectionManager.isConnectingToInternet()) {
                        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        mgr.hideSoftInputFromWindow(myPhoneNumberET.getWindowToken(), 0);

                        postRequestEditPhoneNumber(UserProfilePageActivity.this, ServiceConstant.profile_edit_mobileNo_url);
                    } else {
                        HNDHelper.showErrorAlert(UserProfilePageActivity.this, getResources().getString(R.string.nointernet_text));
                    }

                    handled = true;
                }
                return handled;
            }
        });

        myCountryCodeTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(myPhoneNumberET.getWindowToken(), 0);

                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        myUserProImgVw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission()) {
                        requestGalleryPermission();
                    } else {
                        chooseImage();
                    }
                } else {
                    chooseImage();
                }
            }
        });
    }

    private void chooseImage() {
        myAddPhotoDialog = new Dialog(UserProfilePageActivity.this);
        myAddPhotoDialog.getWindow();
        myAddPhotoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myAddPhotoDialog.setContentView(R.layout.alert_dialog_take_photo);
        myAddPhotoDialog.setCanceledOnTouchOutside(true);
        myAddPhotoDialog.getWindow().getAttributes().windowAnimations = R.style.animations_photo_Picker;
        myAddPhotoDialog.show();
        myAddPhotoDialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout camera = myAddPhotoDialog
                .findViewById(R.id.alert_dialog_take_photo_camera_layout);
        RelativeLayout gallery = myAddPhotoDialog
                .findViewById(R.id.alert_dialog_take_photo_gallery_layout);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeImageFromCamera();
                myAddPhotoDialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImageFromGallery();
                myAddPhotoDialog.dismiss();
            }
        });
    }

    private void openImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    private void takeImageFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        camera_FileUri = getOutputMediaFileUri(1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, camera_FileUri);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_REQUEST_CODE);
    }

    public void postUserProfileRequest(final Context aContext, String url) {

        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("userid", mySession.getUserDetails().getUserId());

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------view profile response------------" + response);

                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equals("1")) {


                        if (object.has("user_first_name"))
                            activity_user_profile_user_first_name_ET.setText(object.getString("user_first_name"));
                        if (object.has("username"))
                            activity_user_profile_user_name_ET.setText(object.getString("username"));
                        if (object.has("user_last_name"))
                            activity_user_profile_user_last_name_ET.setText(object.getString("user_last_name"));
                        myEmailTXT.setText(object.getString("email"));
                        myCountryCodeTXT.setText(object.getString("countrycode"));
                        myPhoneNumberET.setText(object.getString("number"));

                        Picasso.get()
                                .load(/*ServiceConstant.Base_Url + */object.getString("avatar"))
                                .placeholder(R.drawable.ic_no_user)
                                .resize(500, 500)
                                .error(R.drawable.ic_no_user)
                                .into(myUserProImgVw);

                    }

                    if (myDialog.isShowing()) {
                        myDialog.dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", camera_FileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        camera_FileUri = savedInstanceState.getParcelable("file_uri");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_REQUEST_CODE || requestCode == UCrop.REQUEST_CROP) {
                try {
                    if (requestCode == CAMERA_REQUEST_CODE) {

                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 8;

                        final Bitmap bitmap = BitmapFactory.decodeFile(camera_FileUri.getPath(), options);
                        Bitmap thumbnail = bitmap;
                        final String picturePath = camera_FileUri.getPath();
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                        File curFile = new File(picturePath);
                        try {
                            ExifInterface exif = new ExifInterface(curFile.getPath());
                            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                            int rotationInDegrees = exifToDegrees(rotation);

                            Matrix matrix = new Matrix();
                            if (rotation != 0f) {
                                matrix.preRotate(rotationInDegrees);
                            }
                            thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
                        } catch (IOException ex) {
                            Log.e("TAG", "Failed to get Exif data", ex);
                        }
                        thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                        byteArray = byteArrayOutputStream.toByteArray();

                        Uri picUri = Uri.fromFile(curFile);

                        UCrop.Options Uoptions = new UCrop.Options();
                        Uoptions.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
                        Uoptions.setToolbarColor(getResources().getColor(R.color.colorPrimary));
                        Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));


                        UCrop.of(picUri, picUri)
                                .withAspectRatio(4, 4)
                                .withMaxResultSize(8000, 8000)
                                .withOptions(Uoptions)
                                .start(UserProfilePageActivity.this);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else if (requestCode == GALLERY_REQUEST_CODE) {

                Uri selectedImage = data.getData();
                if (selectedImage.toString().startsWith("content://com.sec.android.gallery3d.provider")) {
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    final String picturePath = c.getString(columnIndex);
                    c.close();
                    File curFile = new File(picturePath);

                    Uri picUri = Uri.fromFile(curFile);

                    UCrop.Options Uoptions = new UCrop.Options();
                    Uoptions.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
                    Uoptions.setToolbarColor(getResources().getColor(R.color.colorPrimary));
                    Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));

                    UCrop.of(picUri, picUri)
                            .withAspectRatio(4, 4)
                            .withOptions(Uoptions)
                            .withMaxResultSize(8000, 8000)
                            .start(UserProfilePageActivity.this);

                } else {
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();

                    int columnIndex = c.getColumnIndex(filePath[0]);
                    final String picturePath = c.getString(columnIndex);
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                    Bitmap thumbnail = bitmap; //getResizedBitmap(bitmap, 600);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    File curFile = new File(picturePath);

                    try {
                        ExifInterface exif = new ExifInterface(curFile.getPath());
                        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                        int rotationInDegrees = exifToDegrees(rotation);

                        Matrix matrix = new Matrix();
                        if (rotation != 0f) {
                            matrix.preRotate(rotationInDegrees);
                        }
                        thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
                    } catch (IOException ex) {
                        Log.e("TAG", "Failed to get Exif data", ex);
                    }
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    byteArray = byteArrayOutputStream.toByteArray();
                    c.close();

                    //---new-----------new image--DIR
                    if (!myImageRoot.exists()) {
                        myImageRoot.mkdir();
                    } else if (!myImageRoot.isDirectory()) {
                        myImageRoot.delete();
                        myImageRoot.mkdir();
                    }

                    final File image = new File(myImageRoot, System.currentTimeMillis() + ".jpg");
                    camera_FileUri = Uri.fromFile(image);

                    UCrop.Options Uoptions = new UCrop.Options();
                    Uoptions.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
                    Uoptions.setToolbarColor(getResources().getColor(R.color.colorPrimary));
                    Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));

                    UCrop.of(selectedImage, camera_FileUri)
                            .withAspectRatio(4, 4)
                            .withOptions(Uoptions)
                            .withMaxResultSize(8000, 8000)
                            .start(UserProfilePageActivity.this);


                }
            }

            if (requestCode == UCrop.REQUEST_CROP) {

                final Uri resultUri = UCrop.getOutput(data);
                try {
                    selectedBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                byteArray = byteArrayOutputStream.toByteArray();


                postRequestUploadUserImage(ServiceConstant.profile_edit_photo_url);


            } else if (resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(data);
                System.out.println("========cropError===========" + cropError);
            }
        }
    }

    private void requestGalleryPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chooseImage();
                } else {
                    finish();
                }
                break;
        }
    }

    /*

     */

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private void postRequestUploadUserImage(String url) {
        myDialog = new ProgressDialogcreated(UserProfilePageActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                System.out.println("------------- image response-----------------" + response.data);

                String resultResponse = new String(response.data);
                System.out.println("-------------  user image upload response-----------------" + resultResponse);
                String sStatus = "";

                try {
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    sStatus = jsonObject.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject responseObject = jsonObject.getJSONObject("response");

                        Picasso.get()
                                .load(/*ServiceConstant.Base_Url +*/ responseObject.getString("image"))
                                .placeholder(R.drawable.ic_no_user)
                                .resize(500, 500)
                                .error(R.drawable.ic_no_user)
                                .into(myUserProImgVw);


                        UpdateUserImageInSession(responseObject.getString("image"));

                        HomeActivity.NotifyChange();

                        HNDHelper.showErrorAlert(UserProfilePageActivity.this, responseObject.getString("msg"));

                    } else {
                        HNDHelper.showResponseErrorAlert(UserProfilePageActivity.this, jsonObject.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }

                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", mySession.getUserDetails().getUserId());
                System.out.println("user_id---------------" + mySession.getUserDetails().getUserId());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("file", new DataPart("maidac.jpg", byteArray));

                System.out.println("photo--------edit------" + byteArray);

                return params;
            }
        };

        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(retryPolicy);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        multipartRequest.setShouldCache(false);
        HAppController.getInstance().addToRequestQueue(multipartRequest);
    }

    private void postRequestEditUserName(final Context aContext) {

        myDialog = new ProgressDialogcreated(UserProfilePageActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("user_first_name", activity_user_profile_user_first_name_ET.getText().toString());
        jsonParams.put("user_last_name", activity_user_profile_user_last_name_ET.getText().toString());
        jsonParams.put("user_name", activity_user_profile_user_name_ET.getText().toString());

        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(ServiceConstant.profile_edit_userName_url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------------Edit Username Response-----------------" + response);
                try {
                    JSONObject object = new JSONObject(response);

                    if (object.getString("status").equalsIgnoreCase("1")) {
                        setUserNameUpdateSession(activity_user_profile_user_name_ET.getText().toString());
                        HomeActivity.NotifyChange();
                        HNDHelper.showErrorAlert(aContext, getResources().getString(R.string.activity_user_profile_username_upload_success));
                    } else {
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }


            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void postRequestEditPhoneNumber(final Context aContext, String url) {
        myDialog = new ProgressDialogcreated(UserProfilePageActivity.this);
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("country_code", myCountryCodeTXT.getText().toString());
        jsonParams.put("phone_number", myPhoneNumberET.getText().toString());
        ServiceRequest mRequest = new ServiceRequest(aContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("---------------Edit MobileNumber Response-----------------" + response);
                try {

                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1")) {

                        Intent intent = new Intent(UserProfilePageActivity.this, ProfileOTPActivity.class);
                        intent.putExtra("Otp", object.getString("otp"));
                        intent.putExtra("Otp_Status", object.getString("otp_status"));
                        intent.putExtra("CountryCode", object.getString("country_code"));
                        intent.putExtra("Phone", object.getString("phone_number"));
                        intent.putExtra("UserID", mySession.getUserDetails().getUserId());
                        startActivity(intent);
                    } else {
                        HNDHelper.showResponseErrorAlert(aContext, object.getString("response"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    private void UpdateUserImageInSession(String aUserSTR) {
        UserInfoPojo aInfopojo = new UserInfoPojo();

        aInfopojo.setUserProfileImage(aUserSTR);
        aInfopojo.setUserId(mySession.getUserDetails().getUserId());
        aInfopojo.setUserReferalCode(mySession.getUserDetails().getUserReferalCode());
        aInfopojo.setUserCategoryId(mySession.getUserDetails().getUserCategoryId());
        aInfopojo.setUserCurrencyCode(mySession.getUserDetails().getUserCurrencyCode());
        aInfopojo.setSocketKey(mySession.getUserDetails().getSocketKey());
        aInfopojo.setUserWalletAmount(mySession.getUserDetails().getUserWalletAmount());
        aInfopojo.setUserPhoneNumber(mySession.getUserDetails().getUserPhoneNumber());
        aInfopojo.setUserCountryCode(mySession.getUserDetails().getUserCountryCode());
        aInfopojo.setUserEmail(mySession.getUserDetails().getUserEmail());
        aInfopojo.setUserName(mySession.getUserDetails().getUserName());

        mySession.putUserDetails(aInfopojo);
    }

    private void setUserNameUpdateSession(String aUserName) {
        UserInfoPojo aInfopojo = new UserInfoPojo();

        aInfopojo.setUserProfileImage((mySession.getUserDetails().getUserProfileImage()));
        aInfopojo.setUserId(mySession.getUserDetails().getUserId());
        aInfopojo.setUserReferalCode(mySession.getUserDetails().getUserReferalCode());
        aInfopojo.setUserCategoryId(mySession.getUserDetails().getUserCategoryId());
        aInfopojo.setUserCurrencyCode(mySession.getUserDetails().getUserCurrencyCode());
        aInfopojo.setSocketKey(mySession.getUserDetails().getSocketKey());
        aInfopojo.setUserWalletAmount(mySession.getUserDetails().getUserWalletAmount());
        aInfopojo.setUserPhoneNumber(mySession.getUserDetails().getUserPhoneNumber());
        aInfopojo.setUserCountryCode(mySession.getUserDetails().getUserCountryCode());
        aInfopojo.setUserEmail(mySession.getUserDetails().getUserEmail());
        aInfopojo.setUserName(aUserName);

        mySession.putUserDetails(aInfopojo);
    }

    //-----------------------Edit MobileNumber Request-----------------
    private void postRequest_editMobileNumber(String Url) {
        if (!myDialog.isShowing()) {
            myDialog.show();
        }
        System.out.println("---------------Edit MobileNumber Url-----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", mySession.getUserDetails().getUserId());
        jsonParams.put("country_code", myCountryCodeTXT.getText().toString());
        jsonParams.put("phone_number", myPhoneNumberET.getText().toString());
        //  jsonParams.put("otp", "");

        ServiceRequest mRequest = new ServiceRequest(UserProfilePageActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("---------------Edit MobileNumber Response-----------------" + response);
                String Sstatus = "", Smessage = "", Sotp = "", Sotp_status = "", Scountry_code = "", Sphone_number = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Sotp = object.getString("otp");
                        Sotp_status = object.getString("otp_status");
                        Scountry_code = object.getString("country_code");
                        Sphone_number = object.getString("phone_number");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                if (Sstatus.equalsIgnoreCase("1")) {
                    Intent intent = new Intent(UserProfilePageActivity.this, ProfileOtpPageActivity.class);
                    intent.putExtra("Otp", Sotp);
                    intent.putExtra("Otp_Status", Sotp_status);
                    intent.putExtra("CountryCode", Scountry_code);
                    intent.putExtra("Phone", Sphone_number);
                    intent.putExtra("UserID", mySession.getUserDetails().getUserId());
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    HNDHelper.showResponseErrorAlert(UserProfilePageActivity.this, getResources().getString(R.string.class_userprofilepage_number));
                }
            }

            @Override
            public void onErrorListener() {
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
            }
        });
    }

    public boolean EditTextValidationMail(String value) {
        return value.length() <= 0 || isValidEmail(value);
    }

}
