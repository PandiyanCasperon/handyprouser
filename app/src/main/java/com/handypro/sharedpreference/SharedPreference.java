package com.handypro.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.handypro.Pojo.SessionParentPojo;
import com.handypro.Pojo.UserInfoPojo;

import java.util.ArrayList;

/**
 * Created by CAS61 on 2/8/2018.
 */

public class SharedPreference {

    private SharedPreferences mySharedPreference;
    private SharedPreferences.Editor mySharedEditor;

    private SharedPreferences myLocationSharedPreference;
    private SharedPreferences.Editor myLocationSharedEditor;
    private static String FCM_ID = "fcm_id";
    private static String USERDETAILS = "User_details";
    private static String IS_LOGIN = "login";
    private static String CURRENCYDETAIL = "currency_details";
    private static String CATEGORYID = "category_id";
    private static String ISNEWSELECTION = "new_selection";
    private static String ISNEWSEARCH = "new_search";

    public static final String KEY_Chat_userid = "chatuserid";
    public static final String KEY_TASK_ID = "taskID";

    public static final String KEY_AppartmentNumber = "AppartmentNumber";

    private final String AutoStart = "false";

    private static String SESSIONPARENT_DETAILS = "seesion_parent";

    public SharedPreference(Context aContext) {
        mySharedPreference = PreferenceManager.getDefaultSharedPreferences(aContext);
        mySharedEditor = mySharedPreference.edit();

        myLocationSharedPreference = aContext.getSharedPreferences("zipcode", 0);
        myLocationSharedEditor = myLocationSharedPreference.edit();
    }

    public void clearPreference() {
        mySharedEditor.clear();
        myLocationSharedEditor.clear();
        myLocationSharedEditor.commit();
        mySharedEditor.commit();
    }

    public void putFCMRegisterId(String aFCMId) {
        mySharedEditor.putString(FCM_ID, aFCMId);
        mySharedEditor.commit();
    }


    public void setchatuserid(String chatUserId) {
        mySharedEditor.putString(KEY_Chat_userid, chatUserId);
        mySharedEditor.commit();
    }

    public String getchatuserid() {
        try {
            return mySharedPreference.getString(KEY_Chat_userid, "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void setchattaskid(String task_ID) {
        mySharedEditor.putString(KEY_TASK_ID, task_ID);
        mySharedEditor.commit();
    }

    public String getchattaskid() {
        try {
            return mySharedPreference.getString(KEY_TASK_ID, "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getFCMId() {
        try {
            return mySharedPreference.getString(FCM_ID, "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Put Userdetails
     *
     * @param aUserInfo
     */
    public void putUserDetails(UserInfoPojo aUserInfo) {

        String aUserDetailJson = null;

        Gson aGson = new Gson();

        aUserDetailJson = aGson.toJson(aUserInfo);

        mySharedEditor.putString(USERDETAILS, aUserDetailJson);

        mySharedEditor.commit();

    }

    /**
     * Get User details
     *
     * @return
     */
    public UserInfoPojo getUserDetails() {

        UserInfoPojo aUserInfo = null;

        String aUserInfoJSON = mySharedPreference.getString(USERDETAILS, null);

        if (aUserInfoJSON != null) {

            Gson aGson = new Gson();

            aUserInfo = aGson.fromJson(aUserInfoJSON, UserInfoPojo.class);

        }
        return aUserInfo;
    }

    /**
     * Get User details
     *
     * @return
     */
    public ArrayList<SessionParentPojo> getSessionParentDetails() {

        ArrayList<SessionParentPojo> aSessionParentInfo = new ArrayList<>();

        String aSessionParentInfoJSON = mySharedPreference.getString(SESSIONPARENT_DETAILS, null);

        if (aSessionParentInfoJSON != null) {

            aSessionParentInfo = new Gson().fromJson(
                    aSessionParentInfoJSON, new TypeToken<ArrayList<SessionParentPojo>>() {
                    }.getType());
        }
        return aSessionParentInfo;
    }


    /**
     * Put SessionParentPojo
     *
     * @param aSessionParentList
     */
    public void putSessionParentPojo(ArrayList<SessionParentPojo> aSessionParentList) {

        String aSessionParentJson = null;

        Gson aGson = new Gson();

        aSessionParentJson = aGson.toJson(aSessionParentList);

        mySharedEditor.putString(SESSIONPARENT_DETAILS, aSessionParentJson);

        mySharedEditor.commit();

    }


    public void SetLocationDetails(String myZipCodeSTR, String myLatSTR, String myLongSTR, String mySelectedLocation) {
        myLocationSharedEditor.putString("code", "" + myZipCodeSTR);
        myLocationSharedEditor.putString("Latitude", "" + myLatSTR);
        myLocationSharedEditor.putString("Longitude", "" + myLongSTR);
        myLocationSharedEditor.putString("address", "" + myZipCodeSTR);
        myLocationSharedEditor.putString("FullAddress", "" + mySelectedLocation);
        myLocationSharedEditor.apply();
    }

    public void SetZIPCode(String myZipCodeSTR) {
        myLocationSharedEditor.putString("code", myZipCodeSTR);
        myLocationSharedEditor.apply();
    }

    public void SetFlowFrom(String FlowScreen) {
        myLocationSharedEditor.putString("Flow", FlowScreen);
        myLocationSharedEditor.apply();
    }

    public void SetLatLong(String myLatSTR, String myLongSTR) {
        myLocationSharedEditor.putString("Latitude", "" + myLatSTR);
        myLocationSharedEditor.putString("Longitude", "" + myLongSTR);
        myLocationSharedEditor.apply();
    }

    public void SetAddress(String mySelectedLocation, String myZipCodeSTR) {
        myLocationSharedEditor.putString("auto_selected_code_from_GPS", "" + myZipCodeSTR);
        myLocationSharedEditor.putString("code", myZipCodeSTR);
        myLocationSharedEditor.putString("address", "" + myZipCodeSTR);
        myLocationSharedEditor.putString("FullAddress", "" + mySelectedLocation);
        myLocationSharedEditor.apply();
    }

    public String GetFlowFrom() {
        return myLocationSharedPreference.getString("Flow", "");
    }

    public String GetZIPCode() {
        return myLocationSharedPreference.getString("code", "");
    }

    public String GetGPSZIPCode() {
        return myLocationSharedPreference.getString("auto_selected_code_from_GPS", "");
    }

    public void SetGPSZIPCode(String myZipCodeSTR) {
        myLocationSharedEditor.putString("auto_selected_code_from_GPS", "" + myZipCodeSTR);
        myLocationSharedEditor.apply();
    }

    public String GetLatitude() {
        return myLocationSharedPreference.getString("Latitude", "");
    }

    public String GetLongitude() {
        return myLocationSharedPreference.getString("Longitude", "");
    }

    public String GetFullAddress() {
        return myLocationSharedPreference.getString("FullAddress", "");
    }


    public void setLoginStatus(Boolean val) {
        mySharedEditor.putBoolean(IS_LOGIN, val);
        mySharedEditor.commit();

    }

    // Get Login State
    public boolean getLogInStatus() {
        return mySharedPreference.getBoolean(IS_LOGIN, false);
    }

    public void putCurrencySymbol(String aCurrCode) {
        mySharedEditor.putString(CURRENCYDETAIL, aCurrCode);
        mySharedEditor.commit();
    }

    public String getCurrencySymbol() {
        try {
            return mySharedPreference.getString(CURRENCYDETAIL, "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void setCategoryId(String aCategryId) {
        mySharedEditor.putString(CATEGORYID, aCategryId);
        mySharedEditor.apply();
    }

    public String getCategoryId() {
        try {
            return mySharedPreference.getString(CATEGORYID, "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public Boolean isNewSelection() {
        return mySharedPreference.getBoolean(ISNEWSELECTION, false);
    }

    public Boolean isNewSearch() {
        return mySharedPreference.getBoolean(ISNEWSEARCH, false);
    }

    public void setNewSelection(boolean aNewSelection) {
        mySharedEditor.putBoolean(ISNEWSELECTION, aNewSelection);
        mySharedEditor.commit();
    }

    public void setNewsearch(boolean aNewSelection) {
        mySharedEditor.putBoolean(ISNEWSEARCH, aNewSelection);
        mySharedEditor.commit();
    }

    public void setAutoStart(boolean status) {
        mySharedEditor.putBoolean(AutoStart, status);
        mySharedEditor.commit();
    }

    public boolean getAutoStart() {
        return mySharedPreference.getBoolean(AutoStart, false);
    }


    public void setAppartmentnumber(String status) {
        mySharedEditor.putString(KEY_AppartmentNumber,status);
        mySharedEditor.commit();
    }

    public String getAppartmentnumber() {
        return mySharedPreference.getString(KEY_AppartmentNumber, "");

    }
}
