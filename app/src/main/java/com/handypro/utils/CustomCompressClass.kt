package com.handypro.utils

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import com.handypro.Widgets.ProgressDialogcreatedforfrontpage
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.io.File


class CustomCompressClass {
    var compressedFile = File("")
    var myDialog: ProgressDialogcreatedforfrontpage? = null
    fun constructor(context: Context,
                    imageFile: File/*, handler: Handler*/) {
        myDialog = ProgressDialogcreatedforfrontpage(context)
        GlobalScope.launch(Dispatchers.Main) {
            myDialog!!.show()
            val ephemeralKey = async(Dispatchers.IO) { myCompress(context, imageFile) }
            if (ephemeralKey.await().isFile) {
                myDialog!!.dismiss()
                compressedFile = ephemeralKey.await()
                val intent = Intent()
                intent.putExtra("Status", "Created")
                intent.action = "com.handypro.files.selected"
                context.sendBroadcast(intent)
            } else {
                val intent = Intent()
                intent.putExtra("Status", "Fail")
                intent.action = "com.handypro.files.selected"
                context.sendBroadcast(intent)
                myDialog!!.dismiss()
            }
        }
    }

    private suspend fun myCompress(context: Context,
                                   imageFile: File): File {
        return GlobalScope.async(Dispatchers.IO) {
            return@async Compressor.compress(context, imageFile) {
                val extension: String = imageFile.absolutePath.substring(imageFile.absolutePath.lastIndexOf("."))
                resolution(640, 480)
                quality(75)
                format(Bitmap.CompressFormat.WEBP)
                size(2_097_152) // 2 MB
                destination(File(context.getExternalFilesDir("").toString() + "/Compressed/" + System.currentTimeMillis() + extension))
            }
        }.await()
    }
}