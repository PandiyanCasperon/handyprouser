package com.handypro.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.List;
import java.util.Locale;

public class LocationAddress {
    private String TAG = "LocationAddress";
    public void getAddressFromLocation(
            final Double latitude, final Double longitude,
            final Context context , final Handler handler,
            final String From
    ) {
        Thread thread = new Thread(){
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                try {
                    List<Address> addressList = geocoder.getFromLocation(
                            latitude, longitude, 1
                    );
                    if (addressList != null && addressList.size() > 0) {
                        Address address = addressList.get(0);
                        /*val sb = StringBuilder()
                        for (i in 0 until address.maxAddressLineIndex) {
                            sb.append(address.getAddressLine(i)).append("\n")
                        }
                        sb.append(address.locality).append("\n")
                        sb.append(address.postalCode).append("\n")
                        sb.append(address.countryName).append("\n")
                        sb.append(address.postalCode)*/
                        switch (From) {
                            case "":
                                result = address.getAddressLine(0);
                                break;
                            case "SplashActivity":
                                result = address.getAddressLine(0) + "/" + address.getPostalCode();
                                break;
                            case "SignInActivity":
                                result = address.getCountryCode();
                                break;
                        }
                        Log.e(TAG, "Address---------" + result);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Unable connect to Geocoder", e);
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    if (result != null) {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("address", result);
                        message.setData(bundle);
                    } else {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("address", result);
                        message.setData(bundle);
                    }
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }
}
