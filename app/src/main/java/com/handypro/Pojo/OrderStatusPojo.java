package com.handypro.Pojo;

/**
 * Created by CAS63 on 3/22/2018.
 */

public class OrderStatusPojo {
    private String OrderTitle = "";
    private String OrderTime = "";
    private String OrderDate = "";

    public String getOrderTitle() {
        return OrderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        OrderTitle = orderTitle;
    }

    public String getOrderTime() {
        return OrderTime;
    }

    public void setOrderTime(String orderTime) {
        OrderTime = orderTime;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }
}
