package com.handypro.Pojo;

/**
 * Created by CAS63 on 3/5/2018.
 */

public class TransactionInfoPojo {
    private String TransactionJobId = "";
    private String TransactionCategoryName = "";
    private String TransactionTotalAmount = "";
    private String TransactionDate = "";
    private String TransactionTime = "";

    public String getTransactionJobId() {
        return TransactionJobId;
    }

    public void setTransactionJobId(String transactionJobId) {
        TransactionJobId = transactionJobId;
    }

    public String getTransactionCategoryName() {
        return TransactionCategoryName;
    }

    public void setTransactionCategoryName(String transactionCategoryName) {
        TransactionCategoryName = transactionCategoryName;
    }

    public String getTransactionTotalAmount() {
        return TransactionTotalAmount;
    }

    public void setTransactionTotalAmount(String transactionTotalAmount) {
        TransactionTotalAmount = transactionTotalAmount;
    }

    public String getTransactionDate() {
        return TransactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        TransactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return TransactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        TransactionTime = transactionTime;
    }
}
