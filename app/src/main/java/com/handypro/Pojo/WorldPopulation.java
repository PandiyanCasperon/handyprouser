package com.handypro.Pojo;

import java.io.Serializable;

public class WorldPopulation implements Serializable {
    private String rank;
    private String country;
    private String population;

    private String status;
    private String parentcat, subcatid;


    public String getRank() {
        return rank;
    }


    public void setRank(String Rankx) {
        rank = Rankx;
    }


    public String getCountry() {
        return country;
    }


    public void setCountry(String Countryx) {
        country = Countryx;
    }


    public String getSubcatid() {
        return subcatid;
    }


    public void setSubcatid(String subcatidx) {
        subcatid = subcatidx;
    }


    public String getPopulation() {
        return population;
    }


    public void setPopulation(String Populationx) {
        population = Populationx;
    }

    public String getstatus() {
        return status;
    }

    public void setstatus(String statusx) {
        status = statusx;
    }


    public String getparentcat() {
        return parentcat;
    }

    public void setparentcat(String parentcatx) {
        parentcat = parentcatx;
    }

}