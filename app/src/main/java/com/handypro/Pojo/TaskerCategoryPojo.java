package com.handypro.Pojo;

/**
 * Created by CAS63 on 3/15/2018.
 */

public class TaskerCategoryPojo {
    private String CategoryName = "";
    private String CategoryHourlyRate = "";
    private String CategoryImage = "";

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getCategoryHourlyRate() {
        return CategoryHourlyRate;
    }

    public void setCategoryHourlyRate(String categoryHourlyRate) {
        CategoryHourlyRate = categoryHourlyRate;
    }

    public String getCategoryImage() {
        return CategoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        CategoryImage = categoryImage;
    }
}
