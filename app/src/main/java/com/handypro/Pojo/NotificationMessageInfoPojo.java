package com.handypro.Pojo;

/**
 * Created by CAS63 on 3/5/2018.
 */

public class NotificationMessageInfoPojo {
    private String NotificationMessageCreatedAt = "";
    private String NotificationMessage = "";

    public String getNotificationMessageCreatedAt() {
        return NotificationMessageCreatedAt;
    }

    public void setNotificationMessageCreatedAt(String notificationMessageCreatedAt) {
        NotificationMessageCreatedAt = notificationMessageCreatedAt;
    }

    public String getNotificationMessage() {
        return NotificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        NotificationMessage = notificationMessage;
    }
}
