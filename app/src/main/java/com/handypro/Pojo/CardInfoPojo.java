package com.handypro.Pojo;

/**
 * Created by CAS63 on 3/6/2018.
 */

public class CardInfoPojo {
    private String cardId = "";
    private String cardNumber = "";
    private String cardValidMonth = "";
    private String cardType = "";
    private String cardCvvNumber = "";
    private String cardValidYear = "";
    private String cardSelected = "";

    public String getCardValidYear() {
        return cardValidYear;
    }

    public void setCardValidYear(String cardValidYear) {
        this.cardValidYear = cardValidYear;
    }

    public String getCardCvvNumber() {
        return cardCvvNumber;
    }

    public void setCardCvvNumber(String cardCvvNumber) {
        this.cardCvvNumber = cardCvvNumber;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardValidMonth() {
        return cardValidMonth;
    }

    public void setCardValidMonth(String cardValidMonth) {
        this.cardValidMonth = cardValidMonth;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String isSelected() {
        return cardSelected;
    }

    public void setCardSelected(String cardSelected) {
        this.cardSelected = cardSelected;
    }

}
