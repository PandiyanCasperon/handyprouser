package com.handypro.Pojo;

/**
 * Created by CAS63 on 2/16/2018.
 */

public class UserInfoPojo {
    private String UserName = "";
    private String UserId = "";
    private String UserProfileImage = "";
    private String UserEmail = "";
    private String UserCountryCode = "";
    private String UserPhoneNumber = "";
    private String UserCurrencyCode = "";
    private String UserWalletAmount = "";
    private String UserReferalCode = "";
    private String UserCategoryId = "";
    private String SocketKey = "";

    public String getUserReferalCode() {
        return UserReferalCode;
    }

    public void setUserReferalCode(String userReferalCode) {
        UserReferalCode = userReferalCode;
    }

    public String getUserCategoryId() {
        return UserCategoryId;
    }

    public void setUserCategoryId(String userCategoryId) {
        UserCategoryId = userCategoryId;
    }

    public String getUserWalletAmount() {
        return UserWalletAmount;
    }

    public void setUserWalletAmount(String userWalletAmount) {
        UserWalletAmount = userWalletAmount;
    }

    public String getSocketKey() {
        return SocketKey;
    }

    public void setSocketKey(String socketKey) {
        SocketKey = socketKey;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserProfileImage() {
        return UserProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        UserProfileImage = userProfileImage;
    }

    public String getUserEmail() {
        return UserEmail;
    }

    public void setUserEmail(String userEmail) {
        UserEmail = userEmail;
    }

    public String getUserCountryCode() {
        return UserCountryCode;
    }

    public void setUserCountryCode(String userCountryCode) {
        UserCountryCode = userCountryCode;
    }

    public String getUserPhoneNumber() {
        return UserPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        UserPhoneNumber = userPhoneNumber;
    }

    public String getUserCurrencyCode() {
        return UserCurrencyCode;
    }

    public void setUserCurrencyCode(String userCurrencyCode) {
        UserCurrencyCode = userCurrencyCode;
    }
}
