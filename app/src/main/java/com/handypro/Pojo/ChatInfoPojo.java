package com.handypro.Pojo;

/**
 * Created by CAS63 on 3/5/2018.
 */

public class ChatInfoPojo {
    private String MessageTaskId = "";
    private String MessageBookingId = "";
    private String MessageTaskerNameId = "";
    private String MessageTaskerId = "";
    private String MessageTaskerImageId = "";
    private String Messagedate="";
    private String Category;
    private String tasker_status="";

    public String getMessageTaskId() {
        return MessageTaskId;
    }

    public void setMessageTaskId(String messageTaskId) {
        MessageTaskId = messageTaskId;
    }

    public String getMessageBookingId() {
        return MessageBookingId;
    }

    public void setMessageBookingId(String messageBookingId) {
        MessageBookingId = messageBookingId;
    }

    public String getMessageTaskerNameId() {
        return MessageTaskerNameId;
    }

    public void setMessageTaskerNameId(String messageTaskerNameId) {
        MessageTaskerNameId = messageTaskerNameId;
    }

    public String getMessageTaskerId() {
        return MessageTaskerId;
    }

    public void setMessageTaskerId(String messageTaskerId) {
        MessageTaskerId = messageTaskerId;
    }

    public String getMessageTaskerImageId() {
        return MessageTaskerImageId;
    }

    public void setMessageTaskerImageId(String messageTaskerImageId) {
        MessageTaskerImageId = messageTaskerImageId;
    }

    public String getMessagedate() {
        return Messagedate;
    }

    public void setMessagedate(String messagedate) {
        Messagedate = messagedate;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getTasker_status() {
        return tasker_status;
    }

    public void setTasker_status(String tasker_status) {
        this.tasker_status = tasker_status;
    }
}
