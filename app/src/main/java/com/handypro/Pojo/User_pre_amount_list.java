package com.handypro.Pojo;

public class User_pre_amount_list
{
    private String date;

    private String payment_status;

    private String time;

    private String pre_amount;

    private String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDate ()
    {
        return date;
    }

    public void setDate (String date)
    {
        this.date = date;
    }

    public String getPayment_status ()
    {
        return payment_status;
    }

    public void setPayment_status (String payment_status)
    {
        this.payment_status = payment_status;
    }

    public String getTime ()
    {
        return time;
    }

    public void setTime (String time)
    {
        this.time = time;
    }

    public String getPre_amount ()
    {
        return pre_amount;
    }

    public void setPre_amount (String pre_amount)
    {
        this.pre_amount = pre_amount;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [date = "+date+", payment_status = "+payment_status+", time = "+time+", pre_amount = "+pre_amount+"]";
    }
}

