package com.handypro.Pojo;

import java.io.Serializable;

/**
 * Created by CAS63 on 2/13/2018.
 */

public class SubCategoryPojo implements Serializable {
    private String SubCategoryName = "";
    private String SubCategoryImage = "";
    private String SubCategoryMainName = "";
    private String isSubCategorySelected = "";
    private String SubCategoryActiveIcon = "";
    private String SubCategoryInactiveIcon = "";
    private String subCategoryId = "";
    private String SubCategoryJobType = "";
    private String SubCategoryHourlyRate = "";
    private String Availability = "";

    private String SubCategoryHours = "";
    private String Estimatetext = "", Joborestimate = "";
    private String userstatus = "";
    private String amount = "";

    public String getAvailability() {
        return Availability;
    }

    public void setAvailability(String availability) {
        Availability = availability;
    }

    public String getSubCategoryJobType() {
        return SubCategoryJobType;
    }

    public void setSubCategoryJobType(String subCategoryJobType) {
        SubCategoryJobType = subCategoryJobType;
    }


    public String getEstimatetext() {
        return Estimatetext;
    }

    public void setEstimatetext(String Estimatetext1) {
        Estimatetext = Estimatetext1;
    }

    public String getSubCategoryHourlyRate() {
        return SubCategoryHourlyRate;
    }

    public void setSubCategoryHourlyRate(String subCategoryHourlyRate) {
        SubCategoryHourlyRate = subCategoryHourlyRate;
    }

    public String getSubCategoryHours() {
        return SubCategoryHours;
    }

    public void setSubCategoryHours(String SubCategoryHours) {
        this.SubCategoryHours = SubCategoryHours;
    }


    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getSubCategoryActiveIcon() {
        return SubCategoryActiveIcon;
    }

    public void setSubCategoryActiveIcon(String SubCategoryActiveIcon) {
        this.SubCategoryActiveIcon = SubCategoryActiveIcon;
    }

    public String getSubCategoryInactiveIcon() {
        return SubCategoryInactiveIcon;
    }

    public void setSubCategoryInactiveIcon(String SubCategoryInactiveIcon) {
        this.SubCategoryInactiveIcon = SubCategoryInactiveIcon;
    }

    public String getIsSubCategorySelected() {
        return isSubCategorySelected;
    }

    public void setIsSubCategorySelected(String isSubCategorySelected) {
        this.isSubCategorySelected = isSubCategorySelected;
    }

    public String getSubCategoryName() {
        return SubCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        SubCategoryName = subCategoryName;
    }

    public String getSubCategoryImage() {
        return SubCategoryImage;
    }

    public void setSubCategoryImage(String subCategoryImage) {
        SubCategoryImage = subCategoryImage;
    }

    public String getSubCategoryMainName() {
        return SubCategoryMainName;
    }

    public void setSubCategoryMainName(String subCategoryMainName) {
        SubCategoryMainName = subCategoryMainName;
    }
    public String getJoborestimate() {
        return Joborestimate;
    }

    public void setJoborestimate(String joborestimate) {
        Joborestimate = joborestimate;
    }

    public String getUserstatus() {
        return userstatus;
    }

    public void setUserstatus(String userstatus) {
        this.userstatus = userstatus;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
