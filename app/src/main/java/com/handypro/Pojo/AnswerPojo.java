package com.handypro.Pojo;

public class AnswerPojo {

    String AnswerId = "";
    String Answertitle = "";
    boolean answerselected = false;

    public boolean isAnswerselected() {
        return answerselected;
    }

    public void setAnswerselected(boolean answerselected) {
        this.answerselected = answerselected;
    }



    public String getAnswerId() {
        return AnswerId;
    }

    public void setAnswerId(String answerId) {
        AnswerId = answerId;
    }

    public String getAnswertitle() {
        return Answertitle;
    }

    public void setAnswertitle(String answertitle) {
        Answertitle = answertitle;
    }


}
