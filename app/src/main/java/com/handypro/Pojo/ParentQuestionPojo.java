package com.handypro.Pojo;

import java.util.ArrayList;

public class ParentQuestionPojo {

    String QuestionId = "";
    String Questiontitle = "";
    String QuestionAnsType = "";
    String QuestionAnsView = "";
    ArrayList<AnswerPojo> answerpojo = null;

    public ArrayList<AnswerPojo> getAnswerpojo() {
        return answerpojo;
    }

    public void setAnswerpojo(ArrayList<AnswerPojo> answerpojo) {
        this.answerpojo = answerpojo;
    }


    public String getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

    public String getQuestiontitle() {
        return Questiontitle;
    }

    public void setQuestiontitle(String questiontitle) {
        Questiontitle = questiontitle;
    }

    public String getQuestionAnsType() {
        return QuestionAnsType;
    }

    public void setQuestionAnsType(String questionAnsType) {
        QuestionAnsType = questionAnsType;
    }

    public String getQuestionAnsView() {
        return QuestionAnsView;
    }

    public void setQuestionAnsView(String questionAnsView) {
        QuestionAnsView = questionAnsView;
    }


}
