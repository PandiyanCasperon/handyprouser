package com.handypro.Pojo;

import java.util.List;

public class EstimatedDetails {
    private List<String> estimationImages = null;

    public List<String> getEstimationImages() {
        return estimationImages;
    }

    public void setEstimationImages(List<String> estimationImages) {
        this.estimationImages = estimationImages;
    }
}
