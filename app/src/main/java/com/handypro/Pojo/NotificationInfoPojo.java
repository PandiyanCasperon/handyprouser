package com.handypro.Pojo;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/5/2018.
 */

public class NotificationInfoPojo {
    private String NotificationTaskId = "";
    private String NotificationBookingId = "";
    private String NotificationCategory = "";
    private ArrayList<NotificationMessageInfoPojo> NotificationMessageInfo = null;

    public String getNotificationTaskId() {
        return NotificationTaskId;
    }

    public void setNotificationTaskId(String notificationTaskId) {
        NotificationTaskId = notificationTaskId;
    }

    public String getNotificationBookingId() {
        return NotificationBookingId;
    }

    public void setNotificationBookingId(String notificationBookingId) {
        NotificationBookingId = notificationBookingId;
    }

    public String getNotificationCategory() {
        return NotificationCategory;
    }

    public void setNotificationCategory(String notificationCategory) {
        NotificationCategory = notificationCategory;
    }

    public ArrayList<NotificationMessageInfoPojo> getNotificationMessageInfo() {
        return NotificationMessageInfo;
    }

    public void setNotificationMessageInfo(ArrayList<NotificationMessageInfoPojo> notificationMessageInfo) {
        NotificationMessageInfo = notificationMessageInfo;
    }

}
