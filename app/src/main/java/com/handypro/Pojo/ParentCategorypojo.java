package com.handypro.Pojo;

import java.util.ArrayList;

/**
 * Created by CAS63 on 2/13/2018.
 */
public class ParentCategorypojo {
    private String ParentCategory_name = "";
    private String ParentCategory_id = "";
    private String ParentSubMoreCategory_item = "";
    private String ParentCategoryActiveIcon = "";
    private String ParentCategoryInactiveIcon = "";
    private ArrayList<SubCategoryPojo> SubCategoryListArr;
    private ArrayList<SubCategoryPojo> SubTempCategoryListArr;

    public String getParentCategoryActiveIcon() {
        return ParentCategoryActiveIcon;
    }

    public void setParentCategoryActiveIcon(String parentCategoryActiveIcon) {
        ParentCategoryActiveIcon = parentCategoryActiveIcon;
    }

    public String getParentCategoryInactiveIcon() {
        return ParentCategoryInactiveIcon;
    }

    public void setParentCategoryInactiveIcon(String parentCategoryInactiveIcon) {
        ParentCategoryInactiveIcon = parentCategoryInactiveIcon;
    }

    public ArrayList<SubCategoryPojo> getSubCategoryListArr() {
        return SubCategoryListArr;
    }

    public void setSubCategoryListArr(ArrayList<SubCategoryPojo> subCategoryListArr) {
        SubCategoryListArr = subCategoryListArr;
    }

    public ArrayList<SubCategoryPojo> getTempSubCategoryListArr() {
        return SubTempCategoryListArr;
    }

    public void setTempSubCategoryListArr(ArrayList<SubCategoryPojo> subCategoryListArr) {
        SubTempCategoryListArr = subCategoryListArr;
    }

    public String getParentSubMoreCategory_item() {
        return ParentSubMoreCategory_item;
    }

    public void setParentSubMoreCategory_item(String parentSubMore) {
        ParentSubMoreCategory_item = parentSubMore;
    }

    public String getParentCategory_name() {
        return ParentCategory_name;
    }

    public void setParentCategory_name(String categoryname) {
        this.ParentCategory_name = categoryname;
    }

    public String getParentCategoryID() {
        return ParentCategory_id;
    }

    public void setParentCategoryID(String categoryID) {
        this.ParentCategory_id = categoryID;
    }
}
