package com.handypro.Pojo;

public class CraftsmanReviewRatedUsersItem {
	private String rattingImage;
	private String comments;
	private String userImage;
	private String ratingTime;
	private String jobId;
	private String userName;
	private String ratings;
	private String desc;

	public CraftsmanReviewRatedUsersItem(String rattingImage, String comments, String userImage, String ratingTime, String jobId, String userName, String ratings, String desc) {
		this.rattingImage = rattingImage;
		this.comments = comments;
		this.userImage = userImage;
		this.ratingTime = ratingTime;
		this.jobId = jobId;
		this.userName = userName;
		this.ratings = ratings;
		this.desc = desc;
	}

	public void setRattingImage(String rattingImage){
		this.rattingImage = rattingImage;
	}

	public String getRattingImage(){
		return rattingImage;
	}

	public void setComments(String comments){
		this.comments = comments;
	}

	public String getComments(){
		return comments;
	}

	public void setUserImage(String userImage){
		this.userImage = userImage;
	}

	public String getUserImage(){
		return userImage;
	}

	public void setRatingTime(String ratingTime){
		this.ratingTime = ratingTime;
	}

	public String getRatingTime(){
		return ratingTime;
	}

	public void setJobId(String jobId){
		this.jobId = jobId;
	}

	public String getJobId(){
		return jobId;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setRatings(String ratings){
		this.ratings = ratings;
	}

	public String getRatings(){
		return ratings;
	}

	public void setDesc(String desc){
		this.desc = desc;
	}

	public String getDesc(){
		return desc;
	}

	@Override
 	public String toString(){
		return 
			"RatedUsersItem{" + 
			"ratting_image = '" + rattingImage + '\'' + 
			",comments = '" + comments + '\'' + 
			",user_image = '" + userImage + '\'' + 
			",rating_time = '" + ratingTime + '\'' + 
			",job_id = '" + jobId + '\'' + 
			",user_name = '" + userName + '\'' + 
			",ratings = '" + ratings + '\'' + 
			",desc = '" + desc + '\'' + 
			"}";
		}
}
