package com.handypro.Pojo;

import com.google.gson.annotations.SerializedName;

public class LocationSavedPojo {

    @SerializedName("zipcode")
    private String zipcode;

    @SerializedName("country")
    private String country;

    @SerializedName("formatted_address")
    private String formattedAddress;

    @SerializedName("city")
    private String city;

    @SerializedName("location")
    private Location location;

    @SerializedName("_id")
    private String id;

    @SerializedName("state")
    private String state;

    @SerializedName("line2")
    private String line2;

    @SerializedName("line1")
    private String line1;

    @SerializedName("status")
    private int status;

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine1() {
        return line1;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "LocationSavedPojo{" +
                        "zipcode = '" + zipcode + '\'' +
                        ",country = '" + country + '\'' +
                        ",formatted_address = '" + formattedAddress + '\'' +
                        ",city = '" + city + '\'' +
                        ",location = '" + location + '\'' +
                        ",_id = '" + id + '\'' +
                        ",state = '" + state + '\'' +
                        ",line2 = '" + line2 + '\'' +
                        ",line1 = '" + line1 + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}