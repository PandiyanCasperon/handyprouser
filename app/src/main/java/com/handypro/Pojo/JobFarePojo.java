package com.handypro.Pojo;

/**
 * Created by CAS63 on 3/22/2018.
 */

public class JobFarePojo {
    private String jobFareTitle = "";
    private String jobFareCurrency = "";
    private String jobFareAmount = "";

    public String getJobFareTitle() {
        return jobFareTitle;
    }

    public void setJobFareTitle(String jobFareTitle) {
        this.jobFareTitle = jobFareTitle;
    }

    public String getJobFareCurrency() {
        return jobFareCurrency;
    }

    public void setJobFareCurrency(String jobFareCurrency) {
        this.jobFareCurrency = jobFareCurrency;
    }

    public String getJobFareAmount() {
        return jobFareAmount;
    }

    public void setJobFareAmount(String jobFareAmount) {
        this.jobFareAmount = jobFareAmount;
    }
}
