package com.handypro.Pojo;

import java.util.ArrayList;

public class SessionParentPojo {

    String SessionParentId = "";
    ArrayList<ParentQuestionPojo> aPojo = null;

    public String getSessionParentId() {
        return SessionParentId;
    }

    public void setSessionParentId(String sessionParentId) {
        SessionParentId = sessionParentId;
    }

    public ArrayList<ParentQuestionPojo> getaPojo() {
        return aPojo;
    }

    public void setaPojo(ArrayList<ParentQuestionPojo> aPojo) {
        this.aPojo = aPojo;
    }


}
