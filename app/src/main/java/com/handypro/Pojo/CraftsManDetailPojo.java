package com.handypro.Pojo;

/**
 * Created by CAS63 on 2/12/2018.
 */

public class CraftsManDetailPojo {
    private String CraftsManProfileImage = "";
    private String CraftsManProfileName = "";
    private String CraftsManReviewCount = "";
    private String CraftsManReviewAvrg = "";
    private String CraftsManYearOfExperience = "";
    private String CraftsManAddress = "";
    private String CraftsManJobRadius = "";
    private String taskerid = "";

    public String getCraftsManProfileImage() {
        return CraftsManProfileImage;
    }

    public void setCraftsManProfileImage(String craftsManProfileImage) {
        CraftsManProfileImage = craftsManProfileImage;
    }


    public String getTaskerid() {
        return taskerid;
    }

    public void setTaskerid(String Taskerid) {
        taskerid = Taskerid;
    }

    public String getCraftsManProfileName() {
        return CraftsManProfileName;
    }

    public void setCraftsManProfileName(String craftsManProfileName) {
        CraftsManProfileName = craftsManProfileName;
    }

    public String getCraftsManReviewCount() {
        return CraftsManReviewCount;
    }

    public void setCraftsManReviewCount(String craftsManReviewCount) {
        CraftsManReviewCount = craftsManReviewCount;
    }

    public String getCraftsManReviewAvrg() {
        return CraftsManReviewAvrg;
    }

    public void setCraftsManReviewAvrg(String craftsManReviewAvrg) {
        CraftsManReviewAvrg = craftsManReviewAvrg;
    }

    public String getCraftsManYearOfExperience() {
        return CraftsManYearOfExperience;
    }

    public void setCraftsManYearOfExperience(String craftsManYearOfExperience) {
        CraftsManYearOfExperience = craftsManYearOfExperience;
    }

    public String getCraftsManAddress() {
        return CraftsManAddress;
    }

    public void setCraftsManAddress(String craftsManAddress) {
        CraftsManAddress = craftsManAddress;
    }

    public String getCraftsManJobRadius() {
        return CraftsManJobRadius;
    }

    public void setCraftsManJobRadius(String craftsManJobRadius) {
        CraftsManJobRadius = craftsManJobRadius;
    }


}
