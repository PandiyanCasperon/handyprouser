package com.handypro.Pojo;

import java.util.List;

public class CraftsmanReviewResponsePojo {
	private String avgReview;
	private int perPage;
	private String totalReview;
	private List<CraftsmanReviewRatedUsersItem> ratedUsers;
	private int currentPage;

	public void setAvgReview(String avgReview){
		this.avgReview = avgReview;
	}

	public String getAvgReview(){
		return avgReview;
	}

	public void setPerPage(int perPage){
		this.perPage = perPage;
	}

	public int getPerPage(){
		return perPage;
	}

	public void setTotalReview(String totalReview){
		this.totalReview = totalReview;
	}

	public String getTotalReview(){
		return totalReview;
	}

	public void setRatedUsers(List<CraftsmanReviewRatedUsersItem> ratedUsers){
		this.ratedUsers = ratedUsers;
	}

	public List<CraftsmanReviewRatedUsersItem> getRatedUsers(){
		return ratedUsers;
	}

	public void setCurrentPage(int currentPage){
		this.currentPage = currentPage;
	}

	public int getCurrentPage(){
		return currentPage;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"avg_review = '" + avgReview + '\'' + 
			",perPage = '" + perPage + '\'' + 
			",total_review = '" + totalReview + '\'' + 
			",rated_users = '" + ratedUsers + '\'' + 
			",current_page = '" + currentPage + '\'' + 
			"}";
		}
}