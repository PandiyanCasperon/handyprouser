package com.handypro.Pojo;

public class CraftsmanReviewPojo{
	private CraftsmanReviewResponsePojo response;
	private String status;

	public void setResponse(CraftsmanReviewResponsePojo response){
		this.response = response;
	}

	public CraftsmanReviewResponsePojo getResponse(){
		return response;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"CraftsmanReviewPojo{" + 
			"response = '" + response + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
