package com.handypro.Pojo;

/**
 * Created by user127 on 19-02-2018.
 */

import java.io.Serializable;

public class approveeditbuilderpojo implements Serializable
{
    private static final long serialVersionUID = -5435670920302756945L;
    private int val1 =0;
    private float val2 =0;

    private int Checkedvalue =0;

    private double Total = 0;
    private String jobtype,whatsincluded,Jobdescription,Materialdescription,depositamount,Material,Serviceid,Status,Singleamount,Helperamount ;

    public int getVal1() {
        return val1;
    }

    public void setVal1(int val1) {
        this.val1 = val1;
    }


    public String getWhatsincluded() {
        return whatsincluded;
    }

    public void setWhatsincluded(String whatsincluded) {
        this.whatsincluded = whatsincluded;
    }


    public int getCheckedvalue() {
        return Checkedvalue;
    }

    public void setCheckedvalue(int checkedvalue) {
        this.Checkedvalue = checkedvalue;
    }


    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }


    public void setHelperamount(String helperamount) {
        this.Helperamount = helperamount;
    }

    public String getHelperamount() {
        return Helperamount;
    }





    public void setSingleamount(String singleamount) {
        this.Singleamount = singleamount;
    }

    public String getSingleamount() {
        return Singleamount;
    }


    public float getVal2() {
        return val2;
    }

    public void setVal2(float val2) {
        this.val2 = val2;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(Double total) {
        Total = total;
    }

    public void setJobtype(String jobtype) {
        this.jobtype = jobtype;
    }

    public String getJobtype() {
        return jobtype;
    }








    public void setServiceid(String serviceid) {
        this.Serviceid = serviceid;
    }

    public String getServiceid() {
        return Serviceid;
    }




    public void setJobdescription(String Jobdescription) {
        this.Jobdescription = Jobdescription;
    }

    public String getJobdescription() {
        return Jobdescription;
    }

    public void setMaterialdescription(String Materialdescription)
    {
        this.Materialdescription = Materialdescription;
    }

    public String getMaterialdescription() {
        return Materialdescription;
    }




    public void setDepositamount(String Depositamount)
    {
        this.depositamount = Depositamount;
    }

    public String getDepositamount() {
        return depositamount;
    }



    public void setMaterial(String Material)
    {
        this.Material = Material;
    }

    public String getMaterial() {
        return Material;
    }

    private EstimatedDetails estimatedDetails;

    public EstimatedDetails getEstimatedDetails() {
        return estimatedDetails;
    }

    public void setEstimatedDetails(EstimatedDetails estimatedDetails) {
        this.estimatedDetails = estimatedDetails;
    }

}

